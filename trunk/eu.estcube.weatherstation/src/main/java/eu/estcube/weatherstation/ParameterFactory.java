/** 
 *
 */
package eu.estcube.weatherstation;

import org.hbird.exchange.core.Parameter;

/**
 *
 */
public abstract class ParameterFactory<T extends Number> implements NamedFactory<Parameter> {

    /** @{inheritDoc . */
    public Parameter createNew(Parameter base, String value) {
        Parameter param = new Parameter(base.getID(), base.getName());
        param.setDescription(base.getDescription());
        param.setUnit(base.getUnit());
        param.setValue(parse(value));
        return param;
    }

    protected abstract T parse(String value);
}
