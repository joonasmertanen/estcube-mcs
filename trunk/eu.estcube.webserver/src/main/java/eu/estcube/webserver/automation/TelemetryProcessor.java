package eu.estcube.webserver.automation;

import eu.estcube.codec.gcp.struct.GcpStruct;
import eu.estcube.common.script.ScriptParameterData;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.core.CalibratedParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by Gregor on 7/1/2015.
 */
@Component
public class TelemetryProcessor implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(TelecommandProcessor.class);

    public static final String FILTER_CALIBRATED_PARAMETERS = "${in.header.class} == 'CalibratedParameter'";

    @Autowired
    private GcpStruct struct;

    @Autowired
    private IdBuilder idBuilder;

    @Value("${gcp.entityId}")
    String entityId;

    /**
     * @{inheritDoc .
     */
    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();

        CalibratedParameter cp = in.getBody(CalibratedParameter.class);
        ScriptParameterData sdp = new ScriptParameterData(cp.getName(), cp.getValue(), cp.getRawValue());

        out.setBody(sdp);
    }
}
