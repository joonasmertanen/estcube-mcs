package eu.estcube.webserver.automation;

import eu.estcube.codec.gcp.GcpEncoder;
import eu.estcube.codec.gcp.exceptions.SubsystemNotFoundException;
import eu.estcube.codec.gcp.struct.GcpCommand;
import eu.estcube.codec.gcp.struct.GcpStruct;
import eu.estcube.codec.gcp.struct.GcpSubsystem;
import eu.estcube.codec.gcp.struct.GcpSubsystemIdProvider;
import eu.estcube.common.script.ScriptCommand;
import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.codehaus.jettison.json.JSONObject;
import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.core.CalibratedParameter;
import org.hbird.exchange.core.Command;
import org.hbird.exchange.core.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Gregor on 7/1/2015.
 */
@Component
public class TelecommandProcessor implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(TelecommandProcessor.class);

    @Autowired
    private GcpStruct struct;

    @Autowired
    private IdBuilder idBuilder;

    @Value("${gcp.entityId}")
    String entityId;

    /**
     * @{inheritDoc .
     */
    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();

        ScriptCommand input = in.getBody(ScriptCommand.class);

        String[] data = input.getCommandName().split(":", 2);
        data[0] = data[0].trim();
        data[1] = data[1].trim();

        int destinationId = -1;
        int commId = -1;
        try {
            destinationId = GcpSubsystemIdProvider.getId(data[0]);
        } catch (SubsystemNotFoundException snfe) {
            // TODO throw to log
        }
        for (GcpCommand command : struct.getCommands()) {
            if (command.getName().equals(data[1])) {
                commId = command.getId();
                break;
            }
        }
        if (destinationId == -1) {
            // TODO figure out how to send an error
        }
        if (commId == -1) {
            // TODO figure out how to send an error
        }

        String[] output = { "06", Integer.toString(destinationId),
                "0", Integer.toString(commId), "06", "0", "" };

        // TODO figure out what to do with hardcoded values
        struct.setSatelliteId("/ESTCUBE/Satellites/ESTCube-1");

        Command command = new GcpEncoder().encode(output, struct, entityId, idBuilder);
        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put(StandardArguments.GROUND_STATION_ID,
                "/ESTCUBE/GroundStations/ES5EC-lab");
        headers.put(StandardArguments.SATELLITE_ID, "/ESTCUBE/Satellites/ESTCube-1");
        System.out.println("Processed command: " + input.getCommandName());
        out.setHeaders(headers);
        out.setBody(command);
    }
}
