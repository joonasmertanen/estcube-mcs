import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import ConfigParser

#read configuration
confparser = ConfigParser.ConfigParser()
confparser.read('tests/testConf.ini')
driverpath = confparser.get('driverpath','chromedriverpath')
mcc = confparser.get('environments','mcc')

def setUpModule():
	global driver
	driver = webdriver.Chrome(driverpath)
	global wait
	wait = WebDriverWait(driver, 30)
	driver.get(mcc)
	user_elem = driver.find_element_by_name("j_username")
	user_elem.send_keys(confparser.get('admins','admin'))
	pw_elem = driver.find_element_by_name("j_password")
	pw_elem.send_keys(confparser.get('admins','adminpw'))
	pw_elem.send_keys(Keys.RETURN)
	driver.get(mcc + '/MCS/#system/dashboard')

def tearDownModule():
	driver.close()
	
#def locating(element_name):

_multiprocess_can_split_ = True

class testComponenTNC(unittest.TestCase):
	
	def setUp(self):
		self.assertTrue("User: test.admin" in driver.page_source)
		
	def tearDown(self):
		return

	def testTNC(self):
		tnc = wait.until(EC.presence_of_element_located((By.ID, "dgrid_5-row-ES5EC TNC Driver")))
		assert tnc
		
class testComponenArchiver(unittest.TestCase):
	
	def setUp(self):
		self.assertTrue("User: test.admin" in driver.page_source)
		
	def tearDown(self):
		return

	def testArchiver(self):
		archiver = wait.until(EC.presence_of_element_located((By.ID, "dgrid_5-row-Oracle Archiver")))
		assert archiver
		
class testComponenTacker(unittest.TestCase):
	
	def setUp(self):
		self.assertTrue("User: test.admin" in driver.page_source)
		
	def tearDown(self):
		return
		
	def testTarcker(self):
		tacker = wait.until(EC.presence_of_element_located((By.ID, "dgrid_5-row-TrackingAutomation")))
		assert tacker
		
class testComponenCalibrator(unittest.TestCase):
	
	def setUp(self):
		self.assertTrue("User: test.admin" in driver.page_source)
		
	def tearDown(self):
		return
		
	def testCalibrator(self):
		calibrator = wait.until(EC.presence_of_element_located((By.ID, "dgrid_5-row-MCS Calibrator")))
		assert calibrator
		
class testComponenEC1ContactPredictor(unittest.TestCase):
	
	def setUp(self):
		self.assertTrue("User: test.admin" in driver.page_source)
		
	def tearDown(self):
		return

	def testEC1ContactPredictor(self):
		ec1_contact_predictor = wait.until(EC.presence_of_element_located((By.ID, "dgrid_5-row-ESTCube-1 Contact Predictor")))
		assert ec1_contact_predictor
		
class testComponenConfigurator(unittest.TestCase):
		
	def setUp(self):
		self.assertTrue("User: test.admin" in driver.page_source)
		
	def tearDown(self):
		return
		
	def testConfigurator(self):
		configurator = wait.until(EC.presence_of_element_located((By.ID, "dgrid_5-row-Configurator")))
		assert configurator
		
class testComponenSDC(unittest.TestCase):
	
	def setUp(self):
		self.assertTrue("User: test.admin" in driver.page_source)
		
	def tearDown(self):
		return
	
	def testSDC(self):
		sdc = wait.until(EC.presence_of_element_located((By.ID, "dgrid_5-row-Space Data Chain")))
		assert sdc
	
class testComponenES5ECRadio(unittest.TestCase):
	
	def setUp(self):
		self.assertTrue("User: test.admin" in driver.page_source)
		
	def tearDown(self):
		return
	
	def testES5ECRadio(self):
		es5ec_radio = wait.until(EC.presence_of_element_located((By.ID, "dgrid_5-row-ES5EC-Radio")))
		assert es5ec_radio
	
	def testEC1OrbitPredictor(self):
		ec1_orbit_predictor = wait.until(EC.presence_of_element_located((By.ID, "dgrid_5-row-ESTCube-1 Orbit Predictor")))
		assert ec1_orbit_predictor
		
class testComponenWeaterStation(unittest.TestCase):
	
	def setUp(self):
		self.assertTrue("User: test.admin" in driver.page_source)
		
	def tearDown(self):
		return
	
	def testWeaterStation(self):
		weather_station = wait.until(EC.presence_of_element_located((By.ID, "dgrid_5-row-Weather Station")))
		assert weather_station
		
class testComponenProxyServer(unittest.TestCase):
	
	def setUp(self):
		self.assertTrue("User: test.admin" in driver.page_source)
		
	def tearDown(self):
		return
		
	def testProxyServer(self):
		proxy_server = wait.until(EC.presence_of_element_located((By.ID, "dgrid_5-row-MCS Proxy Server")))
		assert proxy_server
		
class testComponenWebServer(unittest.TestCase):
	
	def setUp(self):
		self.assertTrue("User: test.admin" in driver.page_source)
		
	def tearDown(self):
		return
	
	def testWebServer(self):
		web_server = wait.until(EC.presence_of_element_located((By.ID, "dgrid_5-row-MCS Web Server")))
		assert web_server
		
class testComponenES5ECWebcam(unittest.TestCase):
	
	def setUp(self):
		self.assertTrue("User: test.admin" in driver.page_source)
		
	def tearDown(self):
		return
	
	def testES5ECWebcam(self):
		es5ec_webcam = wait.until(EC.presence_of_element_located((By.ID, "dgrid_5-row-ES5EC Webcamera")))
		assert es5ec_webcam
		
class testComponenTLEUploader(unittest.TestCase):
	
	def setUp(self):
		self.assertTrue("User: test.admin" in driver.page_source)
		
	def tearDown(self):
		return
		
	def testTLEUploader(self):
		tle_uploader = wait.until(EC.presence_of_element_located((By.ID, "dgrid_5-row-ESTCube-1 Tle Updater")))
		assert tle_uploader
