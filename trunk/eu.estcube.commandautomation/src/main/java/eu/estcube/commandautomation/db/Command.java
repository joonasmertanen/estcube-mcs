package eu.estcube.commandautomation.db;

public class Command {

    private String name;

    private String command;

    private String description;

    private int maxExecutionTime;

    private int waitAfterEndTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMaxExecutionTime() {
        return maxExecutionTime;
    }

    public void setMaxExecutionTime(int maxExecutionTime) {
        this.maxExecutionTime = maxExecutionTime;
    }

    public int getWaitAfterEndTime() {
        return waitAfterEndTime;
    }

    public void setWaitAfterEndTime(int waitAfterEndTime) {
        this.waitAfterEndTime = waitAfterEndTime;
    }
}
