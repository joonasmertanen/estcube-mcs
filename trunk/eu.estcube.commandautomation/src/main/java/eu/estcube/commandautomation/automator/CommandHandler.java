package eu.estcube.commandautomation.automator;

import eu.estcube.commandautomation.automator.event.OnCommandHandlerFinished;
import eu.estcube.commandautomation.db.Command;

abstract public class CommandHandler {

    private Command command;

    private OnCommandHandlerFinished onFinishedHandler;

    public CommandHandler(Command command) {
        this.command = command;
    }

    abstract public void prepare();

    abstract public void start();

    abstract public void end(); // Must call onEnded after script end!

    abstract public void kill(); // Must call onEnded after script end!

    public void onEnded(String result) {
        onFinishedHandler.execute(result);
    }

    public Command getCommand() {
        return command;
    }

    public void setOnFinished(OnCommandHandlerFinished onFinishedHandler) {
        this.onFinishedHandler = onFinishedHandler;
    }

    public OnCommandHandlerFinished getOnFinished() {
        return this.onFinishedHandler;
    }
}
