package eu.estcube.webcamera;

import java.util.List;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.language.ConstantExpression;
import org.apache.camel.spring.Main;
import org.apache.commons.lang3.StringUtils;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.core.BusinessCard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import eu.estcube.common.Constants;
import eu.estcube.common.PrepareForInjection;

@SuppressWarnings("deprecation")
public class WebCamera extends RouteBuilder {

    public static final String PROPERTY_SOCKS_PROXY_HOST = "socksProxyHost";
    public static final String PROPERTY_SOCKS_PROXY_PORT = "socksProxyPort";
    public static final String WEB_CAM_REQUEST_BODY = "Image request";
    public static final String WEB_CAM_SOURCE_ID = "SourceID";

    private static final Logger LOG = LoggerFactory.getLogger(WebCamera.class);

    // TODO - 10.07.2013; kimmell - create class for config

    @Value("${heart.beat.interval}")
    private int heartBeatInterval;

    @Value("${service.id}")
    private String serviceId;

    @Value("${service.version}")
    private String serviceVersion;

    @Value("${service.name}")
    private String serviceName;

    @Value("${socks.proxy.host}")
    private String proxyHost;

    @Value("${socks.proxy.port}")
    private String proxyPort;

    @Value("${gs.id}")
    private String gsId;

    @Autowired
    private ImageTimestamper timestamper;

    @Autowired
    private PrepareForInjection preparator;

    @Autowired
    private ImageMessageCreator toBinary;

    private List<FeedSource> sources;

    private WebCamera() {
    }

    public WebCamera(List<FeedSource> sources) {
        this.sources = sources;
    }

    @Override
    public void configure() throws Exception {
        LOG.info("configure() called");

        MDC.put(StandardArguments.ISSUED_BY, serviceId);

        if (StringUtils.isNotBlank(proxyHost) && StringUtils.isNotBlank(proxyPort)) {
            // for usage with SSH tunnel, outside of UT network
            LOG.debug("Using proxy {}: {}; {}:{};", new Object[] { PROPERTY_SOCKS_PROXY_HOST, proxyHost,
                    PROPERTY_SOCKS_PROXY_PORT, proxyPort });
            System.setProperty(PROPERTY_SOCKS_PROXY_HOST, proxyHost);
            System.setProperty(PROPERTY_SOCKS_PROXY_PORT, proxyPort);
        }

        // @formatter:off
        for (final FeedSource source : sources) {
            getContext().addRouteDefinition(
                    source.getRouteAction()
                    .process(timestamper)
                    .setHeader(WEB_CAM_SOURCE_ID, new ConstantExpression(source.getID()))
                    .bean(toBinary)
                    .process(preparator)
                    .to("log:eu.estcube.webcam.stats?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
                    .to(Constants.AMQ_WEBCAM_SEND)
                    );
        }

        BusinessCard card = new BusinessCard(serviceId, serviceName);
        card.setPeriod(heartBeatInterval);
        card.setDescription(String.format("Web camera transmitter for %s; version: %s", gsId, serviceVersion));
        from("timer://heartbeat?fixedRate=true&period=" + heartBeatInterval)
            .bean(card, "touch")
            .process(preparator)
            .to("activemq:topic:hbird.monitoring");

        // @formatter:on
    }

    public static void main(String[] args) throws Exception {
        LOG.info("Starting Webcamera transmitter");
        new Main().run(args);
    }
}