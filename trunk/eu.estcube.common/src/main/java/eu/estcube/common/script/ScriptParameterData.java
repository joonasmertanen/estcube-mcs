package eu.estcube.common.script;

import java.io.Serializable;

/**
 * Created by Joonas on 1.7.2015.
 */
public class ScriptParameterData implements Serializable {
    private String parameterName;
    private Object value;
    private Object rawvalue;

    public ScriptParameterData() {
    }

    public ScriptParameterData(String parameterName, Object value, Object rawvalue) {
        this.parameterName = parameterName;
        this.value = value;
        this.rawvalue = rawvalue;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Object getRawValue() {
        return rawvalue;
    }

    public void setRawValue(Object value) {
        this.value = rawvalue;
    }
}
