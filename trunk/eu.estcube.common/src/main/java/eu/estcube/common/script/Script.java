package eu.estcube.common.script;

import java.io.Serializable;

public class Script implements Serializable {
    private String code;

    public Script(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
