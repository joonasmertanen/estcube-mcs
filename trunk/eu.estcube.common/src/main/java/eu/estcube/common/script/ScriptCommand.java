package eu.estcube.common.script;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Joonas on 1.7.2015.
 */
public class ScriptCommand implements Serializable {
    public ScriptCommand() {}
    public ScriptCommand(String commandName, Map<String, Object> arguments) {
        this.commandName = commandName;
        this.arguments = arguments;
    }

    private String commandName;
    private Map<String, Object> arguments = new HashMap<String, Object>();

    public String getCommandName() {
        return commandName;
    }

    public Map<String, Object> getArguments() {
        return arguments;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setArguments(Map<String, Object> arguments) {
        this.arguments = arguments;
    }
}
