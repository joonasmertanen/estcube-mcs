package eu.estcube.gs.tnc.processor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.io.File;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.hbird.exchange.constants.StandardArguments;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.common.ByteUtil;
import eu.estcube.domain.transport.tnc.TncFrame;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TncFrameToFileTest {

    private static final byte[] BYTES = new byte[] { 0x0C, 0x04, 0x07, 0x0F, 0x32 };

    private static final Long NOW = System.currentTimeMillis();

    @Mock
    private Exchange exchange;

    @Mock
    private Message in;

    @Mock
    private Message out;

    @Mock
    private TncFrame frame;

    private TncFrameToFile processor;

    private InOrder inOrder;

    private RuntimeException exception;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        processor = new TncFrameToFile();
        inOrder = inOrder(exchange, in, out, frame);
        exception = new RuntimeException("Mutchos problemos");
        when(exchange.getIn()).thenReturn(in);
        when(exchange.getOut()).thenReturn(out);
        when(in.getBody(TncFrame.class)).thenReturn(frame);
        when(in.getHeader(StandardArguments.TIMESTAMP, Long.class)).thenReturn(NOW);
        when(frame.getTarget()).thenReturn(12);
        when(frame.getData()).thenReturn(BYTES);
    }

    /**
     * Test method for
     * {@link eu.estcube.gs.tnc.processor.TncFrameToFile#process(org.apache.camel.Exchange)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testProcess() throws Exception {
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(in, times(1)).getBody(TncFrame.class);
        inOrder.verify(in, times(1)).getHeader(StandardArguments.TIMESTAMP, Long.class);
        inOrder.verify(frame, times(1)).getTarget();
        inOrder.verify(frame, times(1)).getData();
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        inOrder.verify(out, times(1)).setHeader(eq(Exchange.FILE_NAME), captor.capture());
        inOrder.verify(out, times(1)).setBody(ByteUtil.toHexString(BYTES));
        assertTrue(captor.getValue().startsWith(12 + File.separator));
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.gs.tnc.processor.TncFrameToFile#process(org.apache.camel.Exchange)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testProcessWithException() throws Exception {
        when(in.getHeader(StandardArguments.TIMESTAMP, Long.class)).thenThrow(exception);
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(in, times(1)).getBody(TncFrame.class);
        inOrder.verify(in, times(1)).getHeader(StandardArguments.TIMESTAMP, Long.class);
        inOrder.verify(exchange, times(1)).setException(exception);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.gs.tnc.processor.TncFrameToFile#process(org.apache.camel.Exchange)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testProcessNoTncFrame() throws Exception {
        when(in.getBody(TncFrame.class)).thenReturn(null);
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(in, times(1)).getBody(TncFrame.class);
        inOrder.verify(exchange, times(1)).setException(any(IllegalArgumentException.class));
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.gs.tnc.processor.TncFrameToFile#process(org.apache.camel.Exchange)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testProcessNoTimestamp() throws Exception {
        when(in.getHeader(StandardArguments.TIMESTAMP, Long.class)).thenReturn(null);
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(in, times(1)).getBody(TncFrame.class);
        inOrder.verify(in, times(1)).getHeader(StandardArguments.TIMESTAMP, Long.class);
        inOrder.verify(frame, times(1)).getTarget();
        inOrder.verify(frame, times(1)).getData();
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        inOrder.verify(out, times(1)).setHeader(eq(Exchange.FILE_NAME), captor.capture());
        inOrder.verify(out, times(1)).setBody(ByteUtil.toHexString(BYTES));
        assertTrue(captor.getValue().startsWith(12 + File.separator));
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.gs.tnc.processor.TncFrameToFile#createName(int, java.util.Date, java.text.SimpleDateFormat)}
     * .
     */
    @Test
    public void testCreateName() {
        assertEquals(3 + File.separator + "2013-02-08T155936-203.txt", processor.createName(3, "2013-02-08T155936-203"));
        inOrder.verifyNoMoreInteractions();
    }
}
