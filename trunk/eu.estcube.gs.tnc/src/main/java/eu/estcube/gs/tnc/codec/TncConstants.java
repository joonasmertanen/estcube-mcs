package eu.estcube.gs.tnc.codec;

/**
 * TNC KISS protocol constants.
 */
public class TncConstants {

    /** TNC Frame end byte. */
    public static final byte FEND = (byte) 0xC0;
    
    /** TNC Frame escape byte. */
    public static final byte FESC = (byte) 0xDB;
    
    /** TNC Frame escaped frame end byte. */
    public static final byte TFEND = (byte) 0xDC;
    
    /** TNC Frame escaped frame escape byte. */
    public static final byte TFESC = (byte) 0xDD;
}
