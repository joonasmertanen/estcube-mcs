package eu.estcube.calibration.data;

import org.hbird.exchange.core.CalibratedParameter;
import org.hbird.exchange.core.CalibratedParameter.Level;

/**
 * Holds data and provides methods for limit checking
 * 
 * @author Gregor
 * 
 */
public class LimitsData {
    private Double saneLower;
    private Double hardLower;
    private Double softLower;
    private Double softUpper;
    private Double hardUpper;
    private Double saneUpper;

    private Level errorLevel;

    /**
     * @return the saneLower
     */
    public Double getSaneLower() {
        return saneLower;
    }

    /**
     * @param saneLower the saneLower to set
     */
    public void setSaneLower(Double saneLower) {
        this.saneLower = saneLower;
    }

    /**
     * @return the hardLower
     */
    public Double getHardLower() {
        return hardLower;
    }

    /**
     * @param hardLower the hardLower to set
     */
    public void setHardLower(Double hardLower) {
        this.hardLower = hardLower;
    }

    /**
     * @return the softLower
     */
    public Double getSoftLower() {
        return softLower;
    }

    /**
     * @param softLower the softLower to set
     */
    public void setSoftLower(Double softLower) {
        this.softLower = softLower;
    }

    /**
     * @return the softUpper
     */
    public Double getSoftUpper() {
        return softUpper;
    }

    /**
     * @param softUpper the softUpper to set
     */
    public void setSoftUpper(Double softUpper) {
        this.softUpper = softUpper;
    }

    /**
     * @return the hardUpper
     */
    public Double getHardUpper() {
        return hardUpper;
    }

    /**
     * @param hardUpper the hardUpper to set
     */
    public void setHardUpper(Double hardUpper) {
        this.hardUpper = hardUpper;
    }

    /**
     * @return the saneUpper
     */
    public Double getSaneUpper() {
        return saneUpper;
    }

    /**
     * @param saneUpper the saneUpper to set
     */
    public void setSaneUpper(Double saneUpper) {
        this.saneUpper = saneUpper;
    }

    /**
     * @return the errorLevel
     */
    public Level getErrorLevel() {
        return errorLevel;
    }

    /**
     * 
     * @param errorLevel the errorLevel to set
     * @return the previous error level
     */
    public Level setErrorLevel(Level errorLevel) {
        Level previousErrorLevel = this.errorLevel;
        this.errorLevel = errorLevel;
        return previousErrorLevel;
    }

    private boolean inLimits(CalibratedParameter cb, double lower, double upper) {
        Number n = cb.getValue();
        if (n == null)
            return false;
        double d = n.doubleValue();
        return d >= lower && d <= upper;
    }

    /**
     * 
     * @param cb - the <code>CalibratedParameter</code> to check
     * @return <code>true</code> if is in soft limits, <code>false</code>
     *         otherwise
     */
    public boolean inSoft(CalibratedParameter cb) {
        return inLimits(cb, getSoftLower(), getSoftUpper());
    }

    /**
     * 
     * @param cb - the <code>CalibratedParameter</code> to check
     * @return <code>true</code> if is in hard limits, <code>false</code>
     *         otherwise
     */
    public boolean inHard(CalibratedParameter cb) {
        return inLimits(cb, getHardLower(), getHardUpper());
    }

    /**
     * 
     * @param cb - the <code>CalibratedParameter</code> to check
     * @return <code>true</code> if is in sane limits, <code>false</code>
     *         otherwise
     */
    public boolean inSane(CalibratedParameter cb) {
        return inLimits(cb, getSaneLower(), getSaneUpper());
    }

}
