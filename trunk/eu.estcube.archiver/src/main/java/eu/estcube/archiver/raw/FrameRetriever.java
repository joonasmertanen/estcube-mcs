package eu.estcube.archiver.raw;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import oracle.jdbc.OracleConnection;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.archiver.raw.sql.AbstractSqlFrameArchiver;
import eu.estcube.common.queryParameters.OracleQueryParameters;

/**
 * @author Manca
 * 
 * Retrieve data from different databases.
 *
 */
public abstract class FrameRetriever {

    protected static final String CREATED = "created";
    protected static final String DETAILS_FRAME_ID = "frame_id";
    protected static final String DETAILS_NAME = "name";
    protected static final String DETAILS_VALUE = "value";
    
    private DataSource dataSource;
    protected Connection c = null;
    private static final Logger LOG = LoggerFactory
            .getLogger(AbstractSqlFrameArchiver.class);

    
    public FrameRetriever(DataSource source) {
        dataSource = source;
    }

    public DataSource getDataSource() {
        return dataSource;
    }
    
    protected String createQuery(OracleQueryParameters params) {
    	boolean hasGS = !params.getGroundStationName().isEmpty();
    	boolean hasOrbit = !params.getOrbitRange().isEmpty();
        String[] orbit = params.getOrbitRange().split("/");
        StringBuilder query = new StringBuilder("Select * from ").append(getTable());
        query.append(" framesTable ");
        query.append("right join ").append(getDetailsTable());
        query.append(" on framesTable.id = frame_id");
        query.append(" WHERE reception_time BETWEEN ").append(params.getStart().toDateTimeISO().getMillis());
        query.append(" AND ").append(params.getEnd().toDateTimeISO().getMillis());
        query.append(" AND (name = 'groundStationId' OR name = 'issuedBy' OR name = 'orbitNumber')");  
          if (!params.getSatellite().isEmpty()) {
              query.append(" AND satellite = '")
                      .append(params.getSatellite())
                      .append("'");
          }
          if (!params.getDirection().isEmpty()) {
              query.append(" AND direction = '")
              .append(params.getDirection())
              .append("'");
          }
          query.append("ReplaceInSubclass");
          if(hasGS){
         	 query.append(" AND EXISTS ( SELECT id from ");
         	 query.append(getDetailsTable());
         	 query.append(" details where details.frame_id = framesTable.id AND "
         	 		+ "(name = 'groundStationId' OR name = 'issuedBy' ) AND value = '");
         	 query.append(params.getGroundStationName());
         	 query.append("')");
         }
          if(hasOrbit){
          	 query.append(" AND EXISTS ( SELECT id from ");
          	 query.append(getDetailsTable());
          	 query.append(" details where details.frame_id = framesTable.id AND "
          	 		+ "name = 'orbitNumber' and value between ");
          	 query.append(orbit[0]).append(" and ");
          	query.append(orbit[1]).append(")");
          }
        return query.toString();
    }


    public void retrieve(Exchange exchange) throws ArchivingException, SQLException {
        c = dataSource.getConnection();
        ((OracleConnection) c).setImplicitCachingEnabled(true);
                
        
    }

    protected abstract String getTable();

    protected abstract String getDetailsTable();

}
