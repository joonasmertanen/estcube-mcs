package eu.estcube.scriptengine.mcsscript.util;

import eu.estcube.scriptengine.mcsscript.groovy.util.ClosureUtils;
import groovy.lang.Closure;
import org.jdeferred.DoneCallback;
import org.jdeferred.FailCallback;
import org.jdeferred.Promise;
import org.jdeferred.impl.DefaultDeferredManager;

import java.util.concurrent.atomic.AtomicReference;

/**
 * A wrapper for {@link org.jdeferred.Promise} with some Groovy and MCSScript specific
 * utility methods
 */
public class PromiseAdapter {
    private final Promise p;

    public PromiseAdapter(Promise p) {
        this.p = p;
    }

    public PromiseAdapter then(final Closure onDone) {
        return new PromiseAdapter(p.then(new DoneCallback() {
            @Override
            public void onDone(Object o) {
                ClosureUtils.safeCallWithParams(onDone, o);
            }
        }));
    }

    public Object waitForResolve() {
        try {
            p.waitSafely();
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        // help
        final AtomicReference<Object> ref = new AtomicReference<Object>();
        p.then(new DoneCallback() {
            public void onDone(Object o) {
                ref.set(o);
            }
        }, new FailCallback() {
            @Override
            public void onFail(Object o) {
                ref.set(o);
            }
        });


        try {
            p.waitSafely();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }

        Object o = ref.get();
        if (p.isResolved()) {
            return o;
        } else if (o instanceof Throwable) {
            throw new IllegalStateException((Throwable) o);
        } else {
            throw new IllegalStateException("PromiseAdapter#waitForResolve finished without resolving (val: " + o + ")");
        }
    }

    public static PromiseAdapter all(PromiseAdapter... adapters) {
        Promise[] promiseObjs = new Promise[adapters.length];
        for (int i = 0;i < adapters.length; i++) promiseObjs[i] = adapters[i].p;

        return new PromiseAdapter(new DefaultDeferredManager().when(promiseObjs));
    }
}
