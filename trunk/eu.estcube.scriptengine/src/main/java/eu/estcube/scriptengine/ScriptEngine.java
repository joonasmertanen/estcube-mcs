package eu.estcube.scriptengine;

import eu.estcube.scriptengine.io.ScriptIO;
import eu.estcube.scriptengine.raw.camel.CamelScriptIO;
import eu.estcube.scriptengine.raw.camel.ScriptRunProcessor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.spring.Main;
import org.hbird.business.core.AddHeaders;
import org.hbird.exchange.configurator.StandardEndpoints;
import org.hbird.exchange.core.BusinessCard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Joonas on 25.6.2015.
 */
public class ScriptEngine extends RouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(ScriptEngine.class);

    public static final String SCRIPT = "activemq:queue:estcube.scriptengine.script";

    private ScriptEngineConfig config;

    @Autowired
    private AddHeaders addHeaders;

    private CamelScriptIO scriptIO;
    private ScriptRunProcessor processor;

    @Override
    public void configure() throws Exception {
        ProducerTemplate producerTemplate = new DefaultCamelContext().createProducerTemplate();
        scriptIO = new CamelScriptIO(producerTemplate);
        processor = new ScriptRunProcessor(scriptIO);

        // @formatter:off
        from(SCRIPT)
                .process(processor);

        from(CamelScriptIO.SCRIPT_IN)
                .process(scriptIO);
        // @formatter:on
    }

    public static void main(String[] args) throws Exception {
        LOG.info("Starting script engine");
        new Main().run(args);
        /*
        try {
            AbstractApplicationContext context = new ClassPathXmlApplicationContext("META-INF/spring/camel-context.xml");
            ScriptEngine scriptEngine = context.getAutowireCapableBeanFactory().createBean(ScriptEngine.class);

            Main m = new Main();
            m.setApplicationContext(context);
            m.addRouteBuilder(scriptEngine);
            m.run(args);
        } catch (Exception e) {
            LOG.error("Failed to start " + ScriptEngine.class.getName(), e);
        }
        */
    }
}
