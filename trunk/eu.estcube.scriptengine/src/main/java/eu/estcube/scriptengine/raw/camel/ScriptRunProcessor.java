package eu.estcube.scriptengine.raw.camel;

import eu.estcube.common.script.Script;
import eu.estcube.scriptengine.mcsscript.ScriptInstance;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.codehaus.groovy.control.CompilationFailedException;

/**
 * Created by Joonas on 25.6.2015.
 */
public class ScriptRunProcessor implements Processor {
    private final CamelScriptIO scriptIO;

    public ScriptRunProcessor(CamelScriptIO scriptIO) {
        this.scriptIO = scriptIO;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        Message message = exchange.getIn();

        Object body = message.getBody();

        // for debugging
        if (body instanceof String) {
            body = new Script((String) body);
        }

        Script script = (Script) body;
        System.out.println("Received script object with length " + script.getCode().length());

        ScriptInstance scriptInstance = ScriptInstance.from(script.getCode());
        try {
            scriptInstance.compile();
        } catch (CompilationFailedException e) {
            throw e;
        }
        scriptInstance.execute(scriptIO);
    }
}
