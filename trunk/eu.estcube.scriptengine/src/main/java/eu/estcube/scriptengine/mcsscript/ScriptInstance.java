package eu.estcube.scriptengine.mcsscript;

import com.google.common.io.Files;
import eu.estcube.scriptengine.io.ScriptIO;
import eu.estcube.scriptengine.mcsscript.base.Responses;
import eu.estcube.scriptengine.mcsscript.base.ScriptBase;
import eu.estcube.scriptengine.mcsscript.groovy.BytecodeSpewingClassloader;
import eu.estcube.scriptengine.mcsscript.groovy.transform.InnerClassWrapperTransformer;
import eu.estcube.scriptengine.mcsscript.groovy.transform.SimpleStateTransformer;
import eu.estcube.scriptengine.mcsscript.util.DebugScriptIO;
import groovy.lang.GroovyShell;
import groovy.lang.Script;
import groovy.transform.TypeChecked;
import org.codehaus.groovy.control.CompilationFailedException;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.codehaus.groovy.control.customizers.ASTTransformationCustomizer;
import org.codehaus.groovy.control.customizers.ImportCustomizer;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Instance of a script. Handles compilation and execution of a mcsscript file.
 */
public class ScriptInstance {
    private final String code;

    private boolean shouldBeTypeChecked = true;
    private boolean shouldSpewBytecode = false;

    private ScriptInstance(String code) {
        this.code = code;
    }

    private Script parsedScript;
    private ClassLoader scriptClassLoader;

    public boolean isCompiled() {
        return parsedScript != null;
    }
    public ScriptInstance compile() throws CompilationFailedException {
        CompilerConfiguration compilerConfiguration = new CompilerConfiguration();

        // Note: order of ast transformers matters
        compilerConfiguration.addCompilationCustomizers(new ASTTransformationCustomizer(new SimpleStateTransformer()));
        compilerConfiguration.addCompilationCustomizers(new ASTTransformationCustomizer(new InnerClassWrapperTransformer()));

        if (shouldBeTypeChecked)
            compilerConfiguration.addCompilationCustomizers(new ASTTransformationCustomizer(TypeChecked.class));

        compilerConfiguration.addCompilationCustomizers(
                new ImportCustomizer().addStarImports("eu.estcube.scriptengine.mcsscript.base")
                                      .addStaticStars(Responses.class.getName()));

        GroovyShell shell = new GroovyShell(compilerConfiguration);

        if (isShouldSpewBytecode())
            scriptClassLoader = BytecodeSpewingClassloader.enableFor(shell);
        else
            scriptClassLoader = shell.getClassLoader();

        parsedScript = shell.parse(code);

        return this;
    }

    /**
     * Executes compiled script.
     *
     * Note: script must be compiled using {@link ScriptInstance#compile()} first.
     */
    public void execute(ScriptIO scriptIO) {
        if (!isCompiled())
            throw new IllegalStateException("ScriptInstance must be compiled before execution");

        try {
            ScriptBase r = (ScriptBase) scriptClassLoader.loadClass("MCSScript").newInstance();
            r.setScriptIO(scriptIO);
            r.run();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public boolean isShouldSpewBytecode() {
        return shouldSpewBytecode;
    }

    public ScriptInstance setShouldSpewBytecode(boolean shouldSpewBytecode) {
        this.shouldSpewBytecode = shouldSpewBytecode;
        return this;
    }

    public boolean shouldBeTypeChecked() {
        return shouldBeTypeChecked;
    }

    public ScriptInstance setShouldBeTypeChecked(boolean shouldBeTypeChecked) {
        this.shouldBeTypeChecked = shouldBeTypeChecked;
        return this;
    }

    public static ScriptInstance from(String s) {
        return new ScriptInstance(s);
    }

    public static ScriptInstance from(File f) throws IOException {
        return new ScriptInstance(Files.toString(f, Charset.forName("UTF-8")));
    }
}
