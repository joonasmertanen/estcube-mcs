package eu.estcube.scriptengine.mcsscript.util;

import eu.estcube.common.script.ScriptParameterData;
import eu.estcube.scriptengine.io.ScriptIO;
import eu.estcube.scriptengine.io.ScriptIOPayload;

/**
 * Created by Joonas on 30.6.2015.
 */
public class DebugScriptIO implements ScriptIO {
    @Override
    public void send(String op, ScriptIOPayload payload) {
        // we dont actually send anything
    }

    @Override
    public ScriptParameterData pollParameter(String param) {
        // TODO implement
        return null;
    }
}
