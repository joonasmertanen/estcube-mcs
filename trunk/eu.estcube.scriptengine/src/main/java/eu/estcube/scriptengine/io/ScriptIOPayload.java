package eu.estcube.scriptengine.io;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Joonas on 30.6.2015.
 */
public class ScriptIOPayload {
    private Map<String, Object> map = new HashMap<String, Object>();

    public ScriptIOPayload() {}
    public ScriptIOPayload(Map<String, Object> arguments) {
        this.map = arguments;
    }

    public Map<String, Object> getDeserializedMap() {
        return map;
    }

    public ScriptIOPayload put(String key, Object value) {
        map.put(key, value);
        return this;
    }
}
