package eu.estcube.scriptengine.mcsscript.base;

/**
 * Created by Joonas on 30.6.2015.
 */
public class Responses {
    public static abstract class Response {
        public abstract boolean isError();
    }

    public static class OkResponse extends Response {
        private final Object obj;

        public OkResponse(Object obj) {
            this.obj = obj;
        }

        @Override
        public boolean isError() {
            return false;
        }
    }
    public static Response Ok(Object obj) {
        return new OkResponse(obj);
    }
    public static Response Ok() {
        return Ok(null);
    }

    public static class ErrorResponse extends Response {
        private final Object obj;

        public ErrorResponse(Object obj) {
            this.obj = obj;
        }

        @Override
        public boolean isError() {
            return true;
        }
    }
    public static Object Error(Object obj) {
        return new ErrorResponse(obj);
    }
}
