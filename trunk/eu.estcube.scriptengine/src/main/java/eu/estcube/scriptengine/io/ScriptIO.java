package eu.estcube.scriptengine.io;

import eu.estcube.common.script.ScriptParameterData;

/**
 * Created by Joonas on 30.6.2015.
 */
public interface ScriptIO {
    /**
     *  Sends a packet with given payload for given op.
     * @param op
     * @param payload
     */
    void send(String op, ScriptIOPayload payload);

    /**
     * Polls for parameter with given name. Returns null if there is none.
     * @param param
     * @return
     */
    ScriptParameterData pollParameter(String param);
}