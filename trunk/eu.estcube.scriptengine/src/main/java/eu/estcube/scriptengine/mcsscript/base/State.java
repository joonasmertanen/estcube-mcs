package eu.estcube.scriptengine.mcsscript.base;

/**
 * Created by test on 16.06.2015.
 */
public abstract class State {
    public abstract Object run();
}
