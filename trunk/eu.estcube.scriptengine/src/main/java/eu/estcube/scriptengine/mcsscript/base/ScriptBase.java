package eu.estcube.scriptengine.mcsscript.base;

import eu.estcube.common.script.ScriptParameterData;
import eu.estcube.scriptengine.io.ScriptIO;
import eu.estcube.scriptengine.io.ScriptIOPayload;
import eu.estcube.scriptengine.mcsscript.util.PromiseAdapter;
import groovy.lang.Script;
import org.jdeferred.AlwaysCallback;
import org.jdeferred.Deferred;
import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Joonas on 29.6.2015.
 */
public abstract class ScriptBase extends Script {
    private Map<Class<? extends State>, PromiseAdapter> promises = new HashMap<Class<? extends State>, PromiseAdapter>();

    private ExecutorService threadPool = Executors.newCachedThreadPool();
    private void runInParallel(final State state, final Deferred deferred) {
        threadPool.submit(new Runnable() {
            public void run() {
                try {
                    deferred.resolve(state.run());
                }
                catch (Exception e) {
                    deferred.reject(e);
                }
            }
        });
    }
    private PromiseAdapter runState(final Class<? extends State> cls) {
        PromiseAdapter p = null;

        // We want concurrent access to promises map, but we dont use concurrent maps because those would
        // technically allow two threads to instantiate a new state at the same time.
        // Instead we want to force sequential access to promises, which makes sure that only one instance of each state
        // can exist. Performance isn't too big of a problem, because synchronization is only done on state starting
        // methods.
        synchronized (promises) {
            p = promises.get(cls);
            if (p == null) {
                Deferred deferred = new DeferredObject();

                final State state;
                try {
                    Constructor init = cls.getConstructors()[0];
                    // We want to get the constructor with runtime-generated class param
                    // TODO what if there are multiple constructors, is constructors[0] always good

                    state = (State) init.newInstance(this);
                    runInParallel(state, deferred);
                } catch (Exception e) {
                    e.printStackTrace();
                    deferred.reject(e);
                }

                // TODO this is kind of ugly
                deferred.always(new AlwaysCallback() {
                    public void onAlways(Promise.State state, Object o, Object o2) {
                        promises.remove(cls);
                    }
                });

                p = new PromiseAdapter(deferred.promise());
                promises.put(cls, p);
            }
        }
        return p;
    }

    public Object gotoState(Class<? extends State> cls) {
        PromiseAdapter p = runState(cls);
        return p.waitForResolve();
    }

    // TODO dependency inject this
    private ScriptIO scriptIO;
    public void setScriptIO(ScriptIO scriptIO) {
        this.scriptIO = scriptIO;
    }

    public void send(String op) {
        scriptIO.send(op, new ScriptIOPayload());
    }

    public Object listenForParameter(String param) {
        // TODO add "opts" keyword which should accept a option map (for timeout etc)
        while(true) {
            ScriptParameterData pl = scriptIO.pollParameter(param);
            if (pl != null) return pl.getValue();

            Thread.yield(); // TODO is sleeping less CPU intensive?
        }
    }
}
