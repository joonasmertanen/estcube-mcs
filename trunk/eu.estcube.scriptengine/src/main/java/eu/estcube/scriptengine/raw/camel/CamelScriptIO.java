package eu.estcube.scriptengine.raw.camel;

import eu.estcube.common.script.ScriptCommand;
import eu.estcube.common.script.ScriptParameterData;
import eu.estcube.scriptengine.io.ScriptIO;
import eu.estcube.scriptengine.io.ScriptIOPayload;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;

import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Joonas on 1.7.2015.
 */
public class CamelScriptIO implements ScriptIO, Processor {
    public static final String SCRIPT_OUT = "activemq:topic:estcube.scriptengine.telecommand";
    public static final String SCRIPT_IN = "activemq:topic:estcube.scriptengine.telemetry";

    private final ProducerTemplate producerTemplate;

    public CamelScriptIO(ProducerTemplate producerTemplate) {
        this.producerTemplate = producerTemplate;
    }

    @Override
    public void send(String op, ScriptIOPayload payload) {
        ScriptCommand cmd = new ScriptCommand();
        cmd.setCommandName(op);
        cmd.setArguments(payload.getDeserializedMap());

        this.producerTemplate.sendBody(SCRIPT_OUT, cmd);
    }

    // TODO clear incoming parameters on script end
    // TODO make incomingParameters discard data older than x seconds (scripts dont necessarily listen to every parameter)
    private ConcurrentMap<String, Queue<ScriptParameterData>> incomingParameters = new ConcurrentHashMap<String, Queue<ScriptParameterData>>();

    @Override
    public ScriptParameterData pollParameter(String param) {
        Queue<ScriptParameterData> queue = incomingParameters.get(param);
        if (queue == null) return null;

        return queue.poll();
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        Message m = exchange.getIn();

        ScriptParameterData cmd = (ScriptParameterData) m.getBody();
        String commandName = cmd.getParameterName();

        // TODO this can cause a race condition
        Queue<ScriptParameterData> queue = incomingParameters.get(commandName);
        if (queue == null) {
            queue = new LinkedBlockingQueue<ScriptParameterData>();
            incomingParameters.put(commandName, queue);
        }

        queue.add(cmd);
    }
}
