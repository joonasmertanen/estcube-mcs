package eu.estcube.codec.ax25.impl;

import java.nio.ByteBuffer;

import org.springframework.stereotype.Component;

import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.domain.transport.tnc.TncFrame;
import eu.estcube.domain.transport.tnc.TncFrame.TncCommand;

/**
 * Encodes the {@link Ax25UIFrame} to {@link TncFrame}.
 * 
 * Appends prefix if set in the encoder.
 */
@Component
public class Ax25UIFrameKissEncoder {

    /** Default TNC port to use. */
    public static final int DEFAULT_TNC_TARGET_PORT = 0;
    
	/**
	 * Creates new {@link Ax25UIFrameKissEncoder}.
	 *
	 * Prefix is set to null.
	 */
	public Ax25UIFrameKissEncoder() {
	}

	/**
	 * Encodes {@link Ax25UIFrame} using default TNC port.
	 * 
	 * @param ax25Frame {@link Ax25UIFrame} to encode
	 * @return TncFrame encoded from {@link Ax25UIFrame}.
	 * @see #DEFAULT_TNC_TARGET_PORT
	 */
    public TncFrame encode(Ax25UIFrame ax25Frame) {
        return encode(ax25Frame, DEFAULT_TNC_TARGET_PORT, null);
    }

    /**
     * Encodes {@link Ax25UIFrame} using given TNC port.
     * 
     * @param ax25Frame {@link Ax25UIFrame} to encode
     * @param target TNC target port to use
     * @return TncFrame encoded from {@link Ax25UIFrame} and target port
     */
	public TncFrame encode(Ax25UIFrame ax25Frame, int target, byte[] prefix) {
	    int size = Ax25UIFrame.FRAME_CONTENTS_MAX_SIZE + (prefix == null ? 0 : prefix.length);
	    ByteBuffer buffer = ByteBuffer.allocate(size);
	    
	    if (prefix != null) {
	        buffer.put(prefix);
	    }
	    buffer.put(ax25Frame.getDestAddr());
	    buffer.put(ax25Frame.getSrcAddr());
	    buffer.put(ax25Frame.getCtrl());
	    buffer.put(ax25Frame.getPid());
	    buffer.put(ax25Frame.getInfo());
	    
	    buffer.flip();
	    byte data[] = new byte[buffer.remaining()];
	    buffer.get(data);
  	    
	    return new TncFrame(TncCommand.DATA, target, data);
	}
}
