package eu.estcube.gs.sdr;

import org.apache.camel.ExchangePattern;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.hbird.business.core.AddHeaders;
import org.hbird.exchange.configurator.StandardEndpoints;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.core.BusinessCard;
import org.hbird.exchange.groundstation.Track;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import eu.estcube.common.Headers;
import eu.estcube.domain.transport.Downlink;
import eu.estcube.gs.GroundStationAndSatelliteFilter;
import eu.estcube.gs.GsAddHeaders;
import eu.estcube.gs.contact.LocationContactEventExtractor;
import eu.estcube.gs.contact.LocationContactEventHolder;
import eu.estcube.gs.sdr.domain.SdrDriverConfiguration;
import eu.estcube.gs.sdr.processor.ValidAx25UIFrameFilter;

/**
 *
 */
public class Sdr extends RouteBuilder {

    public static final String TYPE = "AX.25";

    private static final Logger LOG = LoggerFactory.getLogger(Sdr.class);

    @Autowired
    private AddHeaders addHeaders;

    @Autowired
    private SdrDriverConfiguration config;

    @Autowired
    private GsAddHeaders gsAddHeaders;

    @Autowired
    private LocationContactEventExtractor eventExtractor;

    @Autowired
    private GroundStationAndSatelliteFilter eventFilter;

    @Autowired
    private LocationContactEventHolder eventHolder;

    @Autowired
    private ValidAx25UIFrameFilter validAx25UIFrameFilter;

    /** @{inheritDoc . */
    @Override
    public void configure() throws Exception {
        MDC.put(StandardArguments.ISSUED_BY, config.getServiceId());

        BusinessCard card = new BusinessCard(config.getServiceId(), config.getServiceName());
        card.setPeriod(config.getHeartBeatInterval());
        card.setDescription(String.format("SDR driver for %s; version: %s", config.getGroundstationId(),
                config.getServiceVersion()));

        // @formatter:off

        // send business card
        from("timer://heartbeat?fixedRate=true&period=" + config.getHeartBeatInterval())
            .bean(card, "touch")
            .process(addHeaders)
            .to(StandardEndpoints.MONITORING);

        // > Send DATA to SDR

        // TODO

        // > Handle DATA from SDR

        from("direct:toAmq")
            .to(Downlink.AX25_FRAMES)
            .end();

        // mina2 (nor netty) does not support tcp client mode
        // therefore could use this kind of hack; or udp, as below
//      from("timer://sdr?fixedRate=true&period=1000")
//          .inOut("mina2:tcp://" + config.getTcpInHost() + ":" + config.getTcpInPort() + "?codec=#ax25UIFrameCodecFactory")

        from("mina2:udp://" + config.getIpInHost() + ":" + config.getIpInPort() + "?codec=#ax25UIFrameCodecFactory&sync=false")
            .setExchangePattern(ExchangePattern.InOnly)
            .to("log:eu.estcube.gs.sdr.in?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
            .filter().method(validAx25UIFrameFilter, "valid")
            .bean(gsAddHeaders)
            .setHeader(StandardArguments.TYPE, constant(TYPE))
            .setHeader(Headers.COMMUNICATION_LINK_TYPE, constant(Downlink.class.getSimpleName()))
            .to("direct:toAmq");

        // listen for Track commands
        from(StandardEndpoints.COMMANDS + "?selector=class='" + Track.class.getSimpleName() + "'")
            .bean(eventExtractor)                     // extract LocationContactEvent
            .filter().method(eventFilter, "matches")  // filter by GS ID and SAT ID
            .bean(eventHolder);                       // update event holder;
        // GsAddHeaders will use this event holder to add contact ID to message header

        // @formatter:on
    }

    public static void main(String[] args) throws Exception {
        LOG.info("Starting SDR driver");
        // new Main().run(args);
        // TODO - 14.08.2013; kimmell - switch back when upgrading to Camel 2.12
        AbstractApplicationContext context = new ClassPathXmlApplicationContext("META-INF/spring/camel-context.xml");
        Sdr sdr = context.getAutowireCapableBeanFactory().createBean(Sdr.class);

        Main m = new Main();
        m.setApplicationContext(context);
        m.addRouteBuilder(sdr);
        m.run(args);
    }
}
