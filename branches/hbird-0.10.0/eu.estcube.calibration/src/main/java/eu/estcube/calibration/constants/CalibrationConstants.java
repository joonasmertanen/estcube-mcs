package eu.estcube.calibration.constants;

public class CalibrationConstants {

    public static final String ID = "id";
    public static final String SCRIPTINFO = "scriptInfo";
    public static final String UNIT = "unit";
    public static final String ADDITIONAL_PARAMETERS = "additionalParameters";
    public static final String OUTPUT_ID = "outputID";
    public static final String DESCRIPTION = "description";
    public static final String ISVECTOR = "isVector";
    public static final String RESULT_VARIABLE = "resultVariable";
    public static final String SCRIPT = "script";

}
