package eu.estcube.calibration.domain;

import java.util.ArrayList;

public class NeededFor {

    private String id;
    private ArrayList<String> parameters;
    private int parametersLeft;

    public NeededFor(String id) {

        this.id = id;
        this.parameters = new ArrayList<String>();
        this.parametersLeft = 0;

    }

    public String getId() {
        return id;
    }

    public ArrayList<String> getParameters() {

        return parameters;
    }

    public void addNew(String parameterId) {

        parameters.add(parameterId);
        parametersLeft++;
    }

    public void calibrated(String parameterId) {

        if (parameters.contains(parameterId)) {

            parametersLeft--;
        }

    }

    public void restart() {

        parametersLeft = parameters.size();
    }

    public boolean isUsedUp() {

        return parametersLeft == 0;
    }

}
