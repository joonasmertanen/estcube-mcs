package eu.estcube.calibration.calibrate;

import java.util.ArrayList;
import java.util.List;

import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.core.Parameter;

import bsh.EvalError;
import bsh.Interpreter;
import eu.estcube.calibration.domain.CalibrationUnit;
import eu.estcube.calibration.domain.InfoContainer;
import eu.estcube.calibration.processors.ParameterCalibrator;

public class Calibrate<T extends Number> implements ParameterCalibrator {

    @SuppressWarnings("unchecked")
    @Override
    public List<Parameter> calibrate(CalibrationUnit input, IdBuilder idBuilder) throws EvalError {

        List<Parameter> output = new ArrayList<Parameter>();

        String calibrationScript = input.getCalibrationInfo().getScript();

        calibrationScript = calibrationScript.replaceAll(input.getMain().getName(), input.getMain().getValue()
                .toString());

        for (Parameter p : input.getAuxParameters()) {

            calibrationScript = calibrationScript.replaceAll(p.getName(), p.getValue().toString());

        }

        Interpreter interpreter = new Interpreter();
        interpreter.eval(calibrationScript);

        if (input.getCalibrationInfo().isResultIsVector()) {

            T[] calibratedValues = (T[]) interpreter.get(input.getCalibrationInfo().getScriptResultVariable());

            for (T value : calibratedValues) {

                Parameter calibratedParameter = createNewParameter(input.getMain(), value, input.getCalibrationInfo());
                output.add(calibratedParameter);
            }

        } else {

            T calibratedValue = (T) interpreter.get(input.getCalibrationInfo().getScriptResultVariable());
            Parameter calibratedParameter = createNewParameter(input.getMain(), calibratedValue,
                    input.getCalibrationInfo());
            output.add(calibratedParameter);

        }

        return output;

    }

    private Parameter createNewParameter(Parameter p, T newValue, InfoContainer info) {

        Parameter result = new Parameter(p.getID(), info.getOutputId());
        result.setTimestamp(p.getTimestamp());
        result.setValue(newValue);
        result.setDescription(info.getDescription());
        result.setUnit(info.getUnit());

        return result;

    }
}
