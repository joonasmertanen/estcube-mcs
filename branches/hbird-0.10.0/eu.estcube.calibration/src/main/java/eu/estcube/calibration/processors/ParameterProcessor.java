/** 
 *
 */
package eu.estcube.calibration.processors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.core.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.calibration.calibrate.Calibrate;
import eu.estcube.calibration.domain.CalibrationUnit;
import eu.estcube.calibration.domain.InfoContainer;

/**
 *
 */
@Component
public class ParameterProcessor implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(ParameterProcessor.class);

    private Map<String, CalibrationUnit> calibrationUnits = initCalibrationUnits();

    @Autowired
    private IdBuilder idBuilder;

    /** @{inheritDoc . */
    @Override
    public void process(Exchange exchange) throws Exception {

        Message in = exchange.getIn();
        Message out = exchange.getOut();
        out.copyFrom(in);

        Parameter input = in.getBody(Parameter.class);

        if (calibrationUnits.containsKey(input.getName())) {

            calibrationUnits.get(input.getName()).setMain(input);

            List<Parameter> calibratedParameters = new ArrayList<Parameter>();

            for (CalibrationUnit cu : calibrationUnits.values()) {

                if (cu.needsThisParameter(input)) {

                    cu.addAuxParameter(input);
                }

                if (cu.isReadyForCalibration()) {

                    ParameterCalibrator pc = new Calibrate<Double>();
                    List<Parameter> calibrationResult = pc.calibrate(cu, idBuilder);
                    cu.clean();

                    calibratedParameters.addAll(calibrationResult);
                }

            }

            out.setBody(calibratedParameters);

        } else {

            // no calibrator found - stop the processing route
            LOG.debug("Skipping Parameter; ID: {}", input.getID(),
                    input.getName());
            exchange.setProperty(Exchange.ROUTE_STOP, Boolean.TRUE);

        }

    }

    private static Map<String, InfoContainer> initCalibrators() {
        Map<String, InfoContainer> map = new HashMap<String, InfoContainer>();
        return map;
    }

    private static Map<String, CalibrationUnit> initCalibrationUnits() {

        Map<String, CalibrationUnit> map = new HashMap<String, CalibrationUnit>();

        Map<String, InfoContainer> calibrationInfo = initCalibrators();

        for (InfoContainer info : calibrationInfo.values()) {

            CalibrationUnit cu = new CalibrationUnit(info);
            map.put(cu.getId(), cu);

        }

        return map;

    }

}
