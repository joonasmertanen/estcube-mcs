package eu.estcube.calibration.xmlparser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import eu.estcube.calibration.constants.CalibrationConstants;
import eu.estcube.calibration.domain.InfoContainer;

public class HashMapConverter implements Converter {

    @Override
    public boolean canConvert(Class arg0) {

        return arg0.equals(HashMap.class);

    }

    @Override
    public void marshal(Object value, HierarchicalStreamWriter writer,
            MarshallingContext context) {

        // Hashtable<String,InfoContainer> table =
        // (Hashtable<String,InfoContainer>) value;
        //
        //
        // Iterator it = table.keySet().iterator();
        //
        // while (it.hasNext()){
        //
        // String key = (String) it.next();
        // InfoContainer info = (InfoContainer) table.get(key);
        //
        //
        // writer.startNode("entry");
        // writer.startNode("id");
        // writer.setValue(key);
        // writer.endNode();
        // writer.startNode("equation");
        // writer.setValue(info.getEquation());
        // writer.endNode();
        // writer.startNode("unit");
        // writer.setValue(info.getUnit());
        // writer.endNode();
        //

        // }

    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader,
            UnmarshallingContext context) {

        HashMap<String, InfoContainer> table = new HashMap<String, InfoContainer>();

        while (reader.hasMoreChildren()) {

            reader.moveDown();

            String id = "";
            String script = "";
            String unit = "";
            String outputId = "";
            String description = "";
            boolean isVector = false;
            String resultVariable = "";
            ArrayList<String> parameters = new ArrayList<String>(0);

            while (reader.hasMoreChildren()) {
                reader.moveDown();

                if (reader.getNodeName().equals(CalibrationConstants.ID)) {

                    id = reader.getValue();

                } else if (reader.getNodeName().equals(CalibrationConstants.SCRIPTINFO)) {

                    while (reader.hasMoreChildren()) {

                        reader.moveDown();

                        if (reader.getNodeName().equals(CalibrationConstants.ISVECTOR)) {

                            isVector = reader.getValue().equalsIgnoreCase("true");

                        } else if (reader.getNodeName().equals(CalibrationConstants.RESULT_VARIABLE)) {

                            resultVariable = reader.getValue();

                        } else if (reader.getNodeName().equals(CalibrationConstants.SCRIPT)) {

                            script = reader.getValue();
                        }

                        reader.moveUp();
                    }

                } else if (reader.getNodeName().equals(CalibrationConstants.UNIT)) {

                    unit = reader.getValue();

                } else if (reader.getNodeName().equals(CalibrationConstants.ADDITIONAL_PARAMETERS)) {

                    parameters = getParameters(reader.getValue());

                } else if (reader.getNodeName().equals(CalibrationConstants.OUTPUT_ID)) {

                    outputId = reader.getValue();

                } else if (reader.getNodeName().equals(CalibrationConstants.DESCRIPTION)) {

                    description = reader.getValue();
                }

                reader.moveUp();
            }
            InfoContainer info = new InfoContainer(id, script, unit, parameters, description, outputId, isVector,
                    resultVariable);

            table.put(id, info);
            reader.moveUp();

        }

        return table;

    }

    private ArrayList<String> getParameters(String s) {

        if (!s.isEmpty()) {

            String[] paramArray = s.split(",");

            ArrayList<String> parameters = new ArrayList<String>(Arrays.asList(paramArray));

            return parameters;
        }

        else {

            return new ArrayList<String>(0);
        }

    }

}
