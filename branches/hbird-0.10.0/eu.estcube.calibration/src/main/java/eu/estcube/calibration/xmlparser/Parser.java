package eu.estcube.calibration.xmlparser;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import eu.estcube.calibration.domain.InfoContainer;

public class Parser {

	/**
	 * @param args
	 */
	@SuppressWarnings("unchecked")
	public static HashMap<String,InfoContainer> getCalibrations(File f){
		
		
		HashMap<String,InfoContainer> table;
		
		XStream xs = new XStream(new DomDriver());
		xs.alias("calibration", Hashtable.class);
		xs.registerConverter(new HashMapConverter());
		
		table = (HashMap<String, InfoContainer>) xs.fromXML(f);
		
		
		return table;

	}
	
	

}
