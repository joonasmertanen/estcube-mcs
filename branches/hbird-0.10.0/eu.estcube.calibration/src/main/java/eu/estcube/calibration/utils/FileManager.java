package eu.estcube.calibration.utils;

import java.io.File;
import java.io.FilenameFilter;


public class FileManager {
	
	
	public static File[] getFiles(){
		
		String currentDirectory = System.getProperty("user.dir");
		
		File folder = new File(currentDirectory+File.separator+"calibrate");
		
		FilenameFilter filter = new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String name) {
				
				return name.endsWith(".xml");
			}
		};
		
		
		File[] files = folder.listFiles(filter);
		
			
		return files;
	}
	
	
	
	

}
