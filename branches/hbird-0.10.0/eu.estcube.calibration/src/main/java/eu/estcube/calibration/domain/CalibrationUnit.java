package eu.estcube.calibration.domain;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.hbird.exchange.core.Parameter;

public class CalibrationUnit {

    private String id;
    private Parameter main;
    private HashMap<String, Parameter> auxParameters;
    private InfoContainer calibrationInfo;

    public CalibrationUnit(InfoContainer calibrationInfo) {

        this.calibrationInfo = calibrationInfo;
        this.id = calibrationInfo.getId();
        clean();

    }

    public String getId() {

        return id;
    }

    public List<String> neededParameters() {

        return calibrationInfo.getAdditionalParameters();

    }

    public boolean needsThisParameter(Parameter p) {

        return neededParameters().contains(p.getName()) && !auxParameters.containsKey(p.getName());
    }

    public Parameter getMain() {

        return main;
    }

    public void setMain(Parameter p) {

        main = p;
    }

    public Collection<Parameter> getAuxParameters() {

        return auxParameters.values();

    }

    public void addAuxParameter(Parameter p) {

        auxParameters.put(p.getName(), p);

    }

    public boolean isReadyForCalibration() {

        boolean isReady = false;

        if (main != null && main.getName().equals(id)) {

            isReady = true;

        }

        if (!auxParameters.keySet().containsAll(calibrationInfo.getAdditionalParameters())) {

            isReady = false;
        }

        return isReady;

    }

    public InfoContainer getCalibrationInfo() {

        return calibrationInfo;
    }

    public void clean() {

        this.main = null;
        this.auxParameters = new HashMap<String, Parameter>();

    }

}
