package eu.estcube.webcamera.sources;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.model.RouteDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.webcamera.FeedSource;

public class FileSource extends FeedSource {
    private static final Logger LOG = LoggerFactory.getLogger(FeedSource.class);

    private String path;

    public FileSource(String id, String path) throws IOException {
        super(id);

        this.path = path;

        // this.path = new File(path).getCanonicalPath();

        System.out.println(this.path);
    }

    @Override
    public RouteDefinition getRouteAction() {
        return new RouteDefinition("file://" + path
                + "?readLock=markerFile&readLockTimeout=200&readLockCheckInterval=200&filter=#empty_file_filter")
                .process(new Processor() {
                    @Override
                    public void process(Exchange ex) throws Exception {
                        // System.out.println("File input processor called " +
                        // ex.getIn().getBody(String.class));

                        byte[] body = ex.getIn().getBody(byte[].class);

                        ex.getOut().setBody(new ByteArrayInputStream(body));
                    }
                });
    }
}
