/** 
 *
 */
package eu.estcube.weatherstation;

import org.hbird.exchange.core.Label;

/**
 * 
 */
public class LabelFactory implements NamedFactory<Label> {

    /** @{inheritDoc . */
    public Label createNew(Label base, String value) {
        Label label = new Label(base.getID(), base.getName());
        label.setDescription(base.getDescription());
        label.setValue(value);
        return label;
    }
}
