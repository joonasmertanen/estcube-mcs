package eu.estcube.webserver.gcp;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.codec.gcp.struct.GcpCommand;
import eu.estcube.codec.gcp.struct.GcpParameter;
import eu.estcube.codec.gcp.struct.GcpStruct;
import eu.estcube.codec.gcp.struct.GcpSubsystem;

@SuppressWarnings("serial")
@Component
public class GcpCommandArgumentsAutoCompleteServlet extends HttpServlet {

    @Autowired
    private GcpStruct struct;

    @EndpointInject(uri = "direct:gcpCommandInput")
    ProducerTemplate producer;

    class RadioBeacon {
        protected Set<String> radioBeaconMessage = new HashSet<String>();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {

        String input = request.getParameter("command");
        int subsys = Integer.parseInt(request.getParameter("subsys"));

        try {
            JSONObject json = new JSONObject();

            GcpCommand command;
            int commandId;
            String defaultArguments = "";
            try {
                commandId = Integer.parseInt(input);
                command = struct.getCommand(commandId, subsys);

            } catch (NumberFormatException e) {
                command = struct.getCommand(input, subsys);
            }

            if (command == null) {
                json.put("status", "error");
                json.put("message", "Command not found!");

            } else {

                String data = "";
                data += "<b>Description:</b> " + command.getDescription() + "<br />";
                data += "<b>Subsystems:</b> ";
                for (GcpSubsystem sub : command.getSubsystems()) {
                    data += sub.getName() + ", ";
                }
                data += "<br />";

                // If the command has parameters/attributes then it adds them
                if (command.getParameters().isEmpty() == false) {

                    data = data.substring(0, data.length() - 2); // remove last
                    data += "<br />";
                    data += "<b>Arguments</b>";
                    data += "<table width=\"100%\" cellspacing=\"4\" cellpadding=\"0\" border=\"0\">";
                    data += "<tr>";
                    data += "<th>Nr</th>";
                    data += "<th>Type</th>";
                    data += "<th>Name</th>";
                    data += "<th>Description</th>";
                    data += "<th>Unit</th>";
                    data += "</tr>";

                    int i = 1;

                    for (GcpParameter parameter : command.getParameters()) {

                        String type = parameter.getClass().getSimpleName().replace("GcpParameter", "").toLowerCase();

                        data += "<tr>";
                        data += "<td valign='top'>" + i + ". </td>";
                        data += "<td valign='top'><b>" + type + "</b></td>";
                        data += "<td valign='top'><b>" + parameter.getName() + "</b></td>";
                        data += "<td valign='top'>" + parameter.getDescription() + "</td>";
                        data += "<td valign='top'>" + parameter.getUnit() + "</td>";
                        data += "</tr>";
                        if (parameter.getDefaultValue() != null) {
                            if (defaultArguments != "") {
                                defaultArguments += " " + parameter.getDefaultValue();
                            }
                            else {
                                defaultArguments = parameter.getDefaultValue();
                            }
                        }
                        i++;
                    }
                    data += "</table>";
                } else {
                    data += "Command has no arguments!";
                }

                json.put("status", "ok");
                json.put("message", data);
                if (defaultArguments != null) {
                    json.put("defaultValue", defaultArguments);
                }

            }
            response.getWriter().write(json.toString());
        } catch (JSONException e1) {
            throw new ServletException(e1);
        }

    }
}
