package eu.estcube.codec.gcp.struct.parameters;

import java.nio.ByteBuffer;

import org.w3c.dom.Element;

public class GcpParameterDouble extends GcpParameterNumberic {

    public final static long BYTES = 8;

    public GcpParameterDouble(String name, String description, String unit) {
        super(name, description, unit, false);
    }

    public GcpParameterDouble(Element e) {
        super(e);
    }

    @Override
    public int getLength() {
        return 64;
    }

    @Override
    public Double toValue(String input) {
        if (isHexString(input)) {
            return (Double) toValueFromHex(input);
        }
        return Double.parseDouble(input);
    }

    @Override
    public byte[] toBytes(Object value) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putDouble(toValue(String.valueOf(value)));
        byte[] bytes = { buffer.get(0), buffer.get(1), buffer.get(2), buffer.get(3), buffer.get(4), buffer.get(5),
                buffer.get(6), buffer.get(7) };
        return bytes;
    }

    @Override
    public Double toValue(byte[] bytes) {

        if (bytes.length != BYTES) {
            throw new RuntimeException("byte[] length of the argument must be " + BYTES + "! Was " + bytes.length);
        }

        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.put(bytes[0]);
        buffer.put(bytes[1]);
        buffer.put(bytes[2]);
        buffer.put(bytes[3]);
        buffer.put(bytes[4]);
        buffer.put(bytes[5]);
        buffer.put(bytes[6]);
        buffer.put(bytes[7]);

        return buffer.getDouble(0);
    }

    @Override
    public Class<?> getValueClass() {
        return Double.class;
    }

}
