package eu.estcube.codec.impl;

import eu.estcube.codec.SscProvider;

/**
 * A simple SSC provider that always starts counting from zero when initiated.
 */
public class SimpleSscProvider implements SscProvider {
	private int ssc = 1;

	@Override
	public int getNext(int count) {
		int result = ssc;
		ssc += count;
		return result;
	}

}
