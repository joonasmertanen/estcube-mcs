package eu.estcube.codec.gcp.struct;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import eu.estcube.codec.gcp.struct.parameters.GcpParameterBit;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterByteArray;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterDouble;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterFloat;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterInt16;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterInt32;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterInt8;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterString;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterUint16;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterUint32;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterUint8;

public class GcpParameterTest {

    String xml1 = "<param><name>a</name><description>b</description><unit>c</unit><type>{$type}</type><ignore>1</ignore></param>";
    String xml2 = "<param><name>a</name><description>b</description><unit>c</unit><type little_endian=\"false\">{$type}</type><ignore>true</ignore></param>";
    String xml3 = "<param><name>a</name><description>b</description><unit>c</unit><type little_endian=\"true\">{$type}</type></param>";
    String noName = "<param><name></name><description>b</description><unit>c</unit><type>string</type></param>";
    String noNameTag = "<param><description>b</description><unit>c</unit><type>string</type></param>";
    String onlyName = "<param><name>a</name><type>string</type></param>";

    @Test
    public void GcpParameter() throws Exception {

        GcpParameter parameter;

        try {
            parameter = GcpParameter.getParameterObjectFromType(parse(noName));
            fail("Must throw exception if name is not set!");
        } catch (Exception e) {
        }

        try {
            parameter = GcpParameter.getParameterObjectFromType(parse(noNameTag));
            fail("Must throw exception if tag name is not set!");
        } catch (Exception e) {
        }

        parameter = GcpParameter.getParameterObjectFromType(parse(onlyName));
        assertTrue(parameter instanceof GcpParameterString);

    }

    @Test
    public void getParameterObjectFromType() throws Exception {

        GcpParameter parameter;

        parameter = GcpParameter.getParameterObjectFromType(parse(xml1.replace("{$type}", "bit")));
        assertTrue(parameter instanceof GcpParameterBit);
        assertEquals(true, parameter.getIgnored());

        parameter = GcpParameter.getParameterObjectFromType(parse(xml1.replace("{$type}", "bytearray")));
        assertTrue(parameter instanceof GcpParameterByteArray);

        parameter = GcpParameter.getParameterObjectFromType(parse(xml3.replace("{$type}", "double")));
        assertTrue(parameter instanceof GcpParameterDouble);
        assertEquals(((GcpParameterDouble) parameter).getIsLittleEndian(), true);
        assertEquals(false, parameter.getIgnored());

        parameter = GcpParameter.getParameterObjectFromType(parse(xml3.replace("{$type}", "float")));
        assertTrue(parameter instanceof GcpParameterFloat);
        assertEquals(((GcpParameterFloat) parameter).getIsLittleEndian(), true);

        parameter = GcpParameter.getParameterObjectFromType(parse(xml2.replace("{$type}", "int16")));
        assertTrue(parameter instanceof GcpParameterInt16);
        assertEquals(((GcpParameterInt16) parameter).getIsLittleEndian(), false);
        assertEquals(true, parameter.getIgnored());

        parameter = GcpParameter.getParameterObjectFromType(parse(xml3.replace("{$type}", "int32")));
        assertTrue(parameter instanceof GcpParameterInt32);
        assertEquals(((GcpParameterInt32) parameter).getIsLittleEndian(), true);

        parameter = GcpParameter.getParameterObjectFromType(parse(xml2.replace("{$type}", "int8")));
        assertTrue(parameter instanceof GcpParameterInt8);
        assertEquals(((GcpParameterInt8) parameter).getIsLittleEndian(), false);

        parameter = GcpParameter.getParameterObjectFromType(parse(xml3.replace("{$type}", "string")));
        assertTrue(parameter instanceof GcpParameterString);

        parameter = GcpParameter.getParameterObjectFromType(parse(xml2.replace("{$type}", "uint16")));
        assertTrue(parameter instanceof GcpParameterUint16);
        assertEquals(((GcpParameterUint16) parameter).getIsLittleEndian(), false);

        parameter = GcpParameter.getParameterObjectFromType(parse(xml3.replace("{$type}", "uint32")));
        assertTrue(parameter instanceof GcpParameterUint32);
        assertEquals(((GcpParameterUint32) parameter).getIsLittleEndian(), true);

        parameter = GcpParameter.getParameterObjectFromType(parse(xml2.replace("{$type}", "uint8")));
        assertTrue(parameter instanceof GcpParameterUint8);
        assertEquals(((GcpParameterUint8) parameter).getIsLittleEndian(), false);

        try {
            parameter = GcpParameter.getParameterObjectFromType(parse(xml2.replace("{$type}", "not_existing_type")));
            fail("Must throw exception about not existing type!");
        } catch (Exception e) {

        }

    }

    private Element parse(String xml) throws Exception {
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                .parse(new InputSource(new StringReader(xml)));

        return (Element) doc.getElementsByTagName("param").item(0);
    }
}
