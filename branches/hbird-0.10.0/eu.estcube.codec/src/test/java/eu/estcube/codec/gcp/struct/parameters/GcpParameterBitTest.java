package eu.estcube.codec.gcp.struct.parameters;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilderFactory;

import org.hbird.exchange.core.Parameter;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

public class GcpParameterBitTest {

    GcpParameterBit p = new GcpParameterBit("a", "b", "c");

    @Test
    public void GcpParameterBit() throws Exception {

        String xml = "<param><name>test</name><description>a</description><unit>b</unit></param>";
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                .parse(new InputSource(new StringReader(xml)));

        GcpParameterBit s = new GcpParameterBit((Element) doc.getElementsByTagName("param").item(0));
        assertEquals("test", s.getName());
        assertEquals("a", s.getDescription());
        assertEquals("b", s.getUnit());
    }

    @Test
    public void getLength() {
        assertEquals(1, p.getLength());
    }

    @Test
    public void toValueStr() {

        assertEquals(true, p.toValue("1"));
        assertEquals(true, p.toValue("true"));
        assertEquals(false, p.toValue("0"));
        assertEquals(false, p.toValue("false"));
        assertEquals(false, p.toValue("12"));
    }

    @Test
    public void toBytes() {
        byte[] t = { 1 };
        byte[] f = { 0 };
        assertArrayEquals(t, p.toBytes(true));
        assertArrayEquals(f, p.toBytes(false));
    }

    @Test
    public void toValueByte() {
        byte[] t1 = { 1 };
        byte[] t2 = { 2 };
        byte[] t3 = { 3 };
        byte[] t4 = { 0 };
        assertEquals(true, p.toValue(t1));
        assertEquals(false, p.toValue(t2));
        assertEquals(true, p.toValue(t3));
        assertEquals(false, p.toValue(t4));
    }

    @Test
    public void getIssued() {
        Parameter parameter = p.getIssued(new byte[] { 0x01 });
        assertEquals("a", parameter.getName());
        assertNull(parameter.getID());
        assertEquals("b", parameter.getDescription());
        assertEquals(null, parameter.getIssuedBy());
        assertEquals(1, parameter.getValue());
        assertEquals("c", parameter.getUnit());
    }

    @Test
    public void getValueClass() {
        assertEquals(Boolean.class, p.getValueClass());
    }
}
