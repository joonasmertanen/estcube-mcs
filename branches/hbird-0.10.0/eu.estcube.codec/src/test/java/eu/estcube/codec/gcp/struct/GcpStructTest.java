package eu.estcube.codec.gcp.struct;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class GcpStructTest {

    GcpStruct struct;

    @Before
    public void setup() throws Exception {
        struct = new GcpStruct();
        struct.parseCommandsFromXml(struct.loadResourceXml("testCommands.xml"));
        struct.parseRepliesFromXml(struct.loadResourceXml("testReplies.xml"));
    }

    @Test
    public void testInitFromXml() throws Exception {
        GcpStruct realStruct = new GcpStruct();
        realStruct.initFromXml();
    }

    @Test
    public void testGetCommand() throws Exception {
        assertEquals("ping", struct.getCommand("ping", 0).getName());
        assertEquals("ping", struct.getCommand(0, 0).getName());

        assertEquals("getmode", struct.getCommand("getmode", 0).getName());
        assertEquals("getmode", struct.getCommand(5, 0).getName());

        assertEquals(null, struct.getCommand(12, 0));
    }

    @Test
    public void testGetReply() throws Exception {
        assertEquals("Ping", struct.getReply(0, "CDHS").getName());
        assertEquals("CDHS house", struct.getReply(4, "CDHS").getName());
    }

    @Test
    public void testGetReplies() throws Exception {
        assertEquals(2, struct.getReplies().size());
    }

}
