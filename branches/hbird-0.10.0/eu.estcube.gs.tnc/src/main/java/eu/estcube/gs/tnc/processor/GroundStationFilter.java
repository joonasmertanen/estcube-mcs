/** 
 *
 */
package eu.estcube.gs.tnc.processor;

import org.hbird.exchange.navigation.LocationContactEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.gs.tnc.domain.TncDriverConfiguration;

/**
 *
 */
@Component
public class GroundStationFilter {

    @Autowired
    private TncDriverConfiguration config;

    public boolean matches(LocationContactEvent event) {
        return config.getGroundstationId().equals(event.getGroundStationID());
    }
}
