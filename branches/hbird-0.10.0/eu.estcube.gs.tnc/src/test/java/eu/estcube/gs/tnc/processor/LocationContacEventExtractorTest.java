/** 
 *
 */
package eu.estcube.gs.tnc.processor;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import org.hbird.exchange.groundstation.Track;
import org.hbird.exchange.navigation.LocationContactEvent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class LocationContacEventExtractorTest {

    @Mock
    private LocationContactEvent event;

    @Mock
    private Track track;

    private LocationContacEventExtractor extractor;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        extractor = new LocationContacEventExtractor();
        inOrder = inOrder(event, track);
    }

    @Test
    public void testExtract() {
        when(track.getLocationContactEvent()).thenReturn(event);
        assertEquals(event, extractor.extract(track));
        inOrder.verify(track, times(1)).getLocationContactEvent();
        inOrder.verifyNoMoreInteractions();
    }
}
