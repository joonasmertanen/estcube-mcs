#!/bin/sh
kill $(ps -u mcs-test -ef | grep java | grep estcube/eu.estcube.webserver | awk '{print $2}') 2>/dev/null
kill $(ps -u mcs-test -ef | grep java | grep estcube/eu.estcube.webcamera | awk '{print $2}') 2>/dev/null
kill $(ps -u mcs-test -ef | grep java | grep estcube/eu.estcube.gs.rotator | awk '{print $2}') 2>/dev/null
kill $(ps -u mcs-test -ef | grep java | grep estcube/eu.estcube.gs.radio | awk '{print $2}') 2>/dev/null
kill $(ps -u mcs-test -ef | grep java | grep estcube/eu.estcube.weatherstation | awk '{print $2}') 2>/dev/null
killall -u mcs-test -e rotctld 2>/dev/null
killall -u mcs-test -e rigctld 2>/dev/null

cd ~/estcube/

for z in ./*.zip; do unzip -o -q $z; done

mkdir -p hamlib/logs 2>/dev/null
touch hamlib/logs/rotator.log
touch hamlib/logs/radio.log

rotctld -m 1 -t 4533 >> hamlib/logs/rotator.log 2>&1 &
rigctld -m 1901 -t 4535 >> hamlib/logs/radio.log 2>&1 &

cd ~/estcube/eu.estcube.webserver/
mkdir logs 2>/dev/null
touch logs/webServer.log
chmod +x bin/run
bin/run >> logs/webServer.log 2>&1 &

cd ~/estcube/eu.estcube.webcamera/
mkdir logs 2>/dev/null
touch logs/webCamera.log
chmod +x bin/run
bin/run >> logs/webCamera.log 2>&1 &

cd ~/estcube/eu.estcube.gs.rotator/
mkdir logs 2>/dev/null
touch logs/rotator.log
chmod +x bin/run
bin/run >> logs/rotator.log 2>&1 &

cd ~/estcube/eu.estcube.gs.radio/
mkdir logs 2>/dev/null
touch logs/radio.log
chmod +x bin/run
bin/run >> logs/radio.log 2>&1 &

cd ~/estcube/eu.estcube.weatherstation/
mkdir logs 2>/dev/null
touch logs/weatherstation.log
chmod +x bin/run
bin/run >> logs/weatherstation.log 2>&1 &
