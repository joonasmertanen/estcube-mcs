package eu.estcube.archiver.raw.tnc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.camel.Message;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import eu.estcube.archiver.raw.ArchivingException;
import eu.estcube.archiver.raw.FrameDataProvider;
import eu.estcube.archiver.raw.sql.MessageToSqlFrameDataProvider;
import eu.estcube.domain.transport.Direction;
import eu.estcube.domain.transport.tnc.TncFrame;
import eu.estcube.domain.transport.tnc.TncFrame.TncCommand;

public class OracleTncFrameArchiverTest {
	private static final long TIMESTAMP = System.currentTimeMillis();
	private static final String SAT = "SAT1";

	private static final TncCommand COMMAND = TncCommand.SET_HARDWARE;
	private static final byte[] DATA = new byte[] { 4, 5, 6 };
	private static final int TARGET = 66;

	private DataSource dataSource;
	private OracleTncFrameArchiver archiver;
	private FrameDataProvider provider;

	private Message message;
	private TncFrame frame;
	private Map<String, Object> headers;

	@Before
	public void setUp() throws Exception {
		message = Mockito.mock(Message.class);
		dataSource = Mockito.mock(DataSource.class);

		provider = new TncToSqlProvider(Direction.DOWN, message);
		archiver = new OracleTncFrameArchiver(dataSource);
		frame = new TncFrame(COMMAND, TARGET, DATA);
		headers = new HashMap<String, Object>();
		headers.put(MessageToSqlFrameDataProvider.HDR_RECEPTION_TIME, TIMESTAMP);
		headers.put(MessageToSqlFrameDataProvider.HDR_SATELLITE, SAT);

		Mockito.when(message.getBody(TncFrame.class)).thenReturn(frame);
		Mockito.when(message.getHeaders()).thenReturn(headers);

	}

	@Ignore
	public void testDataTypes() throws ArchivingException, SQLException,
			ClassNotFoundException {
		Class.forName("oracle.jdbc.OracleDriver");
		Connection c = DriverManager.getConnection(
				"jdbc:oracle:thin:@localhost:1521:devel", "mcs_user",
				"m1skiMCS");
		Mockito.when(dataSource.getConnection()).thenReturn(c);

		archiver.store(provider);
		c.close();

	}

	@Test
	public void testGetAutoIncrementFunction() {
		assertEquals("MCS.TNC_FRAMES_SEQ.NEXTVAL",
				archiver.getAutoIncrementFunction());
	}

	@Test
	public void testGetDetailsAutoIncrementFunction() {
		assertEquals("MCS.TNC_FRAME_DETAILS_SEQ.NEXTVAL",
				archiver.getDetailsAutoIncrementFunction());
	}

	@Test
	public void testGetColumns() {
		String[] c = archiver.getColumns();
		assertEquals(5, c.length);
		assertEquals(TncToSqlProvider.COL_RECEPTION_TIME, c[0]);
		assertEquals(TncToSqlProvider.COL_SATELLITE, c[1]);
		assertEquals(TncToSqlProvider.COL_COMMAND, c[2]);
		assertEquals(TncToSqlProvider.COL_DATA, c[3]);
		assertEquals(TncToSqlProvider.COL_TARGET, c[4]);
	}

	@Test
	public void testGetTable() {
		assertEquals("MCS.TNC_FRAMES", archiver.getTable());
	}

	@Test
	public void testGetDetailsTable() {
		assertEquals("MCS.TNC_FRAME_DETAILS", archiver.getDetailsTable());
	}

	@Test
	public void testGetTimestampFunction() {
		assertEquals("SYSDATE", archiver.getTimestampFunction());
	}

	@Test
	public void testOracleTncFrameArchiver() {
		assertSame(dataSource, archiver.getDataSource());
	}

}
