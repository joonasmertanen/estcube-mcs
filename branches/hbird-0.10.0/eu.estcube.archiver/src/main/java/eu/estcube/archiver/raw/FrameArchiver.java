package eu.estcube.archiver.raw;

public interface FrameArchiver {

	public void store(FrameDataProvider frameDataProvider)
			throws ArchivingException;

}
