package eu.estcube.archiver.raw.beacon;

import java.util.Map;

import org.apache.camel.Message;
import org.hbird.exchange.core.Label;

import eu.estcube.archiver.raw.sql.MessageToSqlFrameDataProvider;
import eu.estcube.domain.transport.Direction;

public class BeaconToSqlProvider extends MessageToSqlFrameDataProvider {
    public static final String COL_NAME = "name";
    public static final String COL_VALUE = "value";
    public static final String COL_INSTANCE_ID = "instance_id";
    public static final String COL_DESCRIPTION = "description";
    public static final String COL_TIMESTAMP = "timestamp";
    public static final String COL_DIRECTION = "direction";
    public static final String COL_ISSUEDBY = "issuedBy";

    public BeaconToSqlProvider(Direction direction) {
        super(direction);
    }

    @Override
    public Object getValue(String column) {
        Message m = getMessage();
        Label f = m.getBody(Label.class);
        Map<String, Object> h = m.getHeaders();

        if (COL_RECEPTION_TIME.equals(column)) {
            return h.get(HDR_RECEPTION_TIME);
        } else if (COL_SATELLITE.equals(column)) {
            return h.get(HDR_SATELLITE);
        } else if (COL_NAME.equals(column)) {
            return f.getName();
        } else if (COL_VALUE.equals(column)) {
            return f.getValue();
        } else if (COL_INSTANCE_ID.equals(column)) {
            return f.getInstanceID();
        } else if (COL_DESCRIPTION.equals(column)) {
            return f.getDescription();
        } else if (COL_TIMESTAMP.equals(column)) {
            return f.getTimestamp();
        } else if (COL_DIRECTION.equals(column)) {
            return Direction.DOWN;
        } else if (COL_ISSUEDBY.equals(column)) {
            return f.getIssuedBy();
        }
        return null;
    }
}
