define([
    "dojo/_base/declare",
    "dojo/dom-class",
    "dojo/dom-construct",
    "config/config",
    "common/store/WebCamStore",
    "common/display/Dashboard",
    "common/display/AudioContentProvider",
    "common/display/Ax25UiFramesContentProvider",		
    "dojo/Stateful",
    ],

    function(declare, DomClass, DomConstruct, Config, WebCamStore, Dashboard, 
    		AudioContentProvider,Ax25UiFramesContentProvider,Stateful) {

        return declare([], {

            constructor: function(args) {
            	
            	var audio =new AudioContentProvider({id:"settingsPortlet",horizontal:true});
            	
                var config = [    //Specifies all the containers on the dashboard
                    {
                        title: "Audio Settings",
                        contentProvider: audio,
                        col: 0,    //Specifies the position on the dashboard
                        row: 0,
                    },
                ];
                	
                this.dashboard = new Dashboard({ config: config, columns: Config.DASHBOARD.numberOfColumns });
                DomClass.add(this.dashboard.getContainer().domNode, "fill");
                
                
                
            },

            placeAt: function(container) {
                this.dashboard.getContainer().placeAt(container);
            },

        });
    }
);
