define([
    "dojo/_base/declare",
    "dojo/dom-class",
    "dojo/dom-construct",
    "config/config",
    "common/formatter/DateFormatter",
    "common/formatter/NameFromIdFormatter",
    "common/store/BusinessCardStore",
    "common/store/PageStore",
    "common/store/WebCamStore",
    "common/store/ParameterStore",
    "common/store/MissionInformationStore",
    "common/display/Dashboard",
    "common/display/GenericContentProvider",
    "common/display/GridContentProvider",
    "common/display/ListContentProvider",
    "common/display/WebCamContentProvider",
    "common/display/CompositeContentProvider",
    "common/display/GaugeContentProvider",
    "common/display/DGridTooltipSupport",
    "common/display/ContactContentProvider",
    ],

    function(declare, DomClass, DomConstruct, Config, DateFormatter, NameFromIdFormatter,
        BusinessCardStore, PageStore, WebCamStore, ParameterStore, MissionInformationStore,
        Dashboard, GenericContentProvider, GridContentProvider, ListContentProvider, WebCamContentProvider, CompositeContentProvider, GaugeContentProvider, DGridTooltipSupport, ContactContentProvider) {

        return declare([], {

            constructor: function(args) {
                var config = [
                    {
                        title: "Components",
                        settings: "MCS Components.",
                        contentProvider: new GridContentProvider({
                            columns: {
                                name: { label: "Component ID", className: "field-issuedBy" },
                                host: { label: "Host", className: "field-host" },
                                timestamp: { label: "Time", formatter: DateFormatter, className: "field-timestamp" },
                            },
                            store: BusinessCardStore,
                            onStartup: function(provider) {
                                new DGridTooltipSupport(provider.grid, function(entry) {
                                    return entry.description;
                                });
                            },
                        }),
                        col: 0,
                        row: 0,
                    },

                    {
                        title: "Host stats",
                        settings: "System recources monitoring.",
                        contentProvider: new GridContentProvider({
                            columns: {
                                name: { label: "Name", className: "field-short-name", formatter: NameFromIdFormatter },
                                value: { label: "Value", className: "field-medium-value" },
                                unit: { label: "Unit", className: "field-long-unit" },
                            },
                            store: ParameterStore,
                            order: "name",
                            query: function(message) {
                                return /^Host\/.*/.test(message.name);
                            },
                            onStartup: function(provider) {
                                new DGridTooltipSupport(provider.grid, function(entry) {
                                    return entry.description;
                                });
                            },
                        }),
                        col: 0,
                        row: 1,
                    },

                    {
                        title: "Satellites",
                        settings: "List of satellites",
                        contentProvider: new GridContentProvider({
                            columns: {
                                name: { label: "Name", className: "field-name" },
                            },
                            store: MissionInformationStore,
                            query: { class: "Satellite" },
                        }),
                        col: 2,
                        row: 0,
                    },

                    {
                        title: "ES5EC WebCam",
                        contentProvider: new WebCamContentProvider({
                            store: WebCamStore,
                            imageId: Config.DASHBOARD.imageId,
                            initialImage: Config.DASHBOARD.initialImage,
                        }),
                        col: 1,
                        row: 1,
                    },

                    {
                        title: "Contacts in ES5EC",
                        contentProvider: new ContactContentProvider(),
                        col: 2,
                        row: 1,
                    },

                    {
                        title: "Air Temperature",
                        settings: "This is composite content provider including one or more other providers",
                        contentProvider: new CompositeContentProvider({
                            margin: 6,
                            providers: [

                                new CircularLinearGaugeProvider({
                                    gaugeSettings: {
                                        value: 0,
                                        minimum: -50,
                                        maximum: 50,
                                        majorTickInterval: 10,
                                        minorTickInterval: 1,
                                        interactionArea: "none",
                                        animationDuration: 1000,
                                        style: "width: 180px; height: 200px;",
                                        title: "Meteo",
                                    },
                                    textIndicator: { x: 100, y: 170, value: "°C", },
                                    store: ParameterStore,
                                    parameterId: "/ESTCUBE/WeatherStations/meteo.physic.ut.ee/Air Temperature",
                                }),

                                new CircularLinearGaugeProvider({
                                    gaugeSettings: {
                                        value: 0,
                                        minimum: -50,
                                        maximum: 50,
                                        majorTickInterval: 10,
                                        minorTickInterval: 1,
                                        interactionArea: "none",
                                        animationDuration: 1000,
                                        style: "width: 180px; height: 200px;",
                                        title: "EMHI",
                                    },
                                    textIndicator: { x: 100, y: 170, value: "°C", },
                                    store: ParameterStore,
                                    parameterId: "/ESTCUBE/WeatherStations/emhi.ee/Air Temperature",
                                }),

                                new CircularLinearGaugeProvider({
                                    gaugeSettings: {
                                        value: 0,
                                        minimum: -50,
                                        maximum: 50,
                                        majorTickInterval: 10,
                                        minorTickInterval: 1,
                                        interactionArea: "none",
                                        animationDuration: 1000,
                                        style: "width: 180px; height: 200px;",
                                        title: "Ilm.ee",
                                    },
                                    textIndicator: { x: 100, y: 170, value: "°C", },
                                    store: ParameterStore,
                                    parameterId: "/ESTCUBE/WeatherStations/ilm.ee/Air Temperature",
                                }),
                            ],
                        }),
                        col: 1,
                        row: 2,

                    },
                ];
                this.dashboard = new Dashboard({ config: config, columns: Config.DASHBOARD.numberOfColumns });
                DomClass.add(this.dashboard.getContainer().domNode, "fill");
            },

            placeAt: function(container) {
                this.dashboard.getContainer().placeAt(container);
            },

        });
    }
);
