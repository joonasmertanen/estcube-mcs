define([
        "dojo/_base/declare",
        "dijit/form/Button",
        "dijit/form/VerticalSlider",
        "dijit/TooltipDialog",
        "dijit/popup",
        "dijit/form/CheckBox",
        "dojo/dom-construct",
        "dojo/cookie",
        "dojox/layout/TableContainer",
        "dijit/form/TextBox",
        "common/store/AudioSettingStore",  
        ],

        function(declare, Button, VerticalSlider, TooltipDialog, Popup, CheckBox, DomConstruct, Cookie, TableContainer, TextBox,AudioSettingStore) {
            declare("SoundControlButton", Button, {
                
                constructor: function(args) {
                	var d = new Date();
                	var cookieExpires=d.getDay()+100;
                    
                    if(Cookie(config.COOKIE_NOTIFICATIONS_MUTE) === undefined){
                        Cookie(config.COOKIE_NOTIFICATIONS_MUTE, "false", { expires: cookieExpires });
                    }
                    if(Cookie(config.COOKIE_STREAM_MUTE) === undefined){
                        Cookie(config.COOKIE_STREAM_MUTE, "true", { expires: cookieExpires });
                    }
                    if(Cookie(config.COOKIE_NOTIFICATIONS_VOLUME) === undefined){
                        Cookie(config.COOKIE_NOTIFICATIONS_VOLUME, 50, { expires: cookieExpires });
                    }
                    if(Cookie(config.COOKIE_STREAM_VOLUME) === undefined){
                        Cookie(config.COOKIE_STREAM_VOLUME, 50, { expires: cookieExpires });
                    }
                    
                    this.streamSoundVolume = Cookie(config.COOKIE_STREAM_VOLUME);
                    this.streamSoundMute = (Cookie(config.COOKIE_STREAM_MUTE) == "true" ? true : false);

                    this.notificationSoundVolume = Cookie(config.COOKIE_NOTIFICATIONS_VOLUME);
                    this.notificationSoundMute = (Cookie(config.COOKIE_NOTIFICATIONS_MUTE) == "true" ? true : false);
                       
                    this.title = args.tooltip == null ? args.label : args.tooltip;
                    this.iconClass = ((this.notificationSoundMute == true) && (this.streamSoundMute == true)) ? config.CLASS_SPEAKER_MUTE : config.CLASS_SPEAKER;
                    
                    this.audioContainer = DomConstruct.create("div");
                    this.tc = new TableContainer(
                            {
                              cols: 2,
                              orientation: "vert",
                              showLabels: true,
                              customClass:"sound-table",
                              labelWidth: "0",
                              spacing: "5"
                            }, this.audioContainer);
                    
                    var button = this;
                    
                    var streamSlider = new VerticalSlider({
                        label: "Beacon",
                        value: this.streamSoundVolume,
                        minimum: 0,
                        maximum: 100,
                        intermediateChanges: true,
                        baseClass: "stream-slider",
                        onChange: function(value){
                        	AudioSettingStore.put({setting: "streamSlider", value: this.value});
                            soundManager.setVolume(config.SOUND_ID_STREAM,value);
                            Cookie(config.COOKIE_STREAM_VOLUME, (value), { expires: cookieExpires });
                                
                        }
                    });
                    
                    var notificationsSlider = new VerticalSlider({
                        label: "Notifications",
                        value: this.notificationSoundVolume,
                        minimum: 0,
                        maximum: 100,
                        intermediateChanges: true,
                        baseClass: "notifications-slider",
                        onChange: function(value){
                        	AudioSettingStore.put({setting: "notificationsSlider", value: this.value});
                            soundManager.setVolume(config.SOUND_ID_AOS,value);
                            soundManager.setVolume(config.SOUND_ID_LOS,value);
                            soundManager.setVolume(config.SOUND_ID_AOSINONE,value);
                            soundManager.setVolume(config.SOUND_ID_LOSINONE,value);
                            soundManager.setVolume(config.SOUND_ID_AOSINFIVE,value);
                            soundManager.setVolume(config.SOUND_ID_LOSINFIVE,value);
                            soundManager.setVolume(config.SOUND_ID_FRAME,value);
                            Cookie(config.COOKIE_NOTIFICATIONS_VOLUME, value, { expires: cookieExpires });
                            
                        }
                    });
                    
                    var streamCheckBox = new CheckBox({
                        label: "Mute:",
                        checked: this.streamSoundMute,
                        style: "margin-left: 12px;",
                        onChange: function(b){                           
                            if(b){
                                Cookie(config.COOKIE_STREAM_MUTE, "true", { expires: cookieExpires });
                                if(Cookie(config.COOKIE_STREAM_MUTE) == "true" && Cookie(config.COOKIE_NOTIFICATIONS_MUTE) == "true"){
                                    button.set("iconClass",config.CLASS_SPEAKER_MUTE);
                                }
                            }else{
                                button.set("iconClass",config.CLASS_SPEAKER);
                                Cookie(config.COOKIE_STREAM_MUTE, "false", { expires: cookieExpires });
                                }
                            soundManager.toggleMute(config.SOUND_ID_STREAM);
                           AudioSettingStore.put({setting: "streamCheckBox", value: this.checked});
                        }
                    });
                    
                    var notificationCheckBox = new CheckBox({
                        label: "Mute:",
                        checked: this.notificationSoundMute,
                        style: "margin-left: 25px;",
                        onChange: function(b){ 
                        	AudioSettingStore.put({setting: "notificationCheckBox", value: this.checked});
                            
                            if(b){
                                Cookie(config.COOKIE_NOTIFICATIONS_MUTE, "true", { expires: cookieExpires });
                                if(Cookie(config.COOKIE_STREAM_MUTE) == "true" && Cookie(config.COOKIE_NOTIFICATIONS_MUTE) == "true"){
                                    button.set("iconClass",config.CLASS_SPEAKER_MUTE);
                                }
                            }else{
                                button.set("iconClass",config.CLASS_SPEAKER);
                                Cookie(config.COOKIE_NOTIFICATIONS_MUTE, "false", { expires: cookieExpires });
                                }
                            
                            soundManager.toggleMute(config.SOUND_ID_AOS);
                            soundManager.toggleMute(config.SOUND_ID_LOS);
                            soundManager.toggleMute(config.SOUND_ID_AOSINONE);
                            soundManager.toggleMute(config.SOUND_ID_LOSINONE);
                            soundManager.toggleMute(config.SOUND_ID_AOSINFIVE);
                            soundManager.toggleMute(config.SOUND_ID_LOSINFIVE);
                            soundManager.toggleMute(config.SOUND_ID_FRAME);
                        }
                    });
                    
                    var notificationCheckBoxFilter = AudioSettingStore.query();           	
                    var notificationCheckBoxObserve = notificationCheckBoxFilter.observe(function (object) {
                        if ((object.setting == "notificationCheckBox") && (object.value != notificationCheckBox.checked)) {
                            notificationCheckBox.set("checked", object.value);

                        }
                        else if ((object.setting == "notificationsSlider") && (object.value != notificationsSlider.value)) {
                            notificationsSlider.set("value", object.value);

                        }

                        else if ((object.setting == "streamSlider") && (object.value != streamSlider.value)) {
                            streamSlider.set("value", object.value);

                        }

                        else if ((object.setting == "streamCheckBox") && (object.value != streamCheckBox.checked)) {
                            streamCheckBox.set("checked", object.value);
                            soundManager.toggleMute(config.SOUND_ID_STREAM);
                        }
                    }, true);
                    
                    
                    
                    
                    this.closed=true;
                    
                    
                    this.tc.addChild(notificationsSlider);   
                    this.tc.addChild(streamSlider);
                    this.tc.addChild(notificationCheckBox);
                    this.tc.addChild(streamCheckBox);
                    
                    this.tc.startup();
                    
                    this.soundControlDialog = new TooltipDialog({
                          content:this.audioContainer,
                          onClose: function(){
                              button.closed=true;
                          },
                          onBlur: function(){
                              
                              Popup.close(this);
                              button.closed = true;
                          }
                      });
                    
                    
                },
                
                onClick: function(){
                    
                    if(this.closed){
                        this.closed = false;
                        Popup.open({
                            popup: this.soundControlDialog,
                            around: this.domNode,
                            orient: ["below-centered"]
                        });
                        this.soundControlDialog.focus();
                    }else{
                        this.closed = true;
                        Popup.close(this.soundControlDialog);
                    }
                }
                
            });  
        }
    );