define([
        "dojo/_base/declare",
        "dojo/cookie",
        ],

        function(declare, Cookie) {
            declare("SoundController", null, {
                
                initSound: function(){
                	var d = new Date();
                	var cookieExpires=d.getDay()+100;
                    var mySound = null;
                    soundManager.setup({
                      url: config.SOUND_URL_FLASH,
                      flashVersion: 9, // optional: shiny features (default = 8)
                      // optional: ignore Flash where possible, use 100% HTML5 mode
                      preferFlash: false,
                      onready: function() {
                        mySound = soundManager.createSound({
                              id: config.SOUND_ID_STREAM,
                              stream: true,
                              url: config.SOUND_URL_STREAM,
                              volume: (Cookie(config.COOKIE_STREAM_VOLUME)=== undefined)? 50 : Cookie(config.COOKIE_STREAM_VOLUME)
                            });
                        var notificationsVolume = (Cookie(config.COOKIE_NOTIFICATIONS_VOLUME)=== undefined)? 50 : Cookie(config.COOKIE_NOTIFICATIONS_VOLUME);
                        
                        var AOSSound = soundManager.createSound({
                              id: config.SOUND_ID_AOS,
                              url: config.SOUND_URL_AOS,
                              volume: notificationsVolume
                            });
                        var LOSSound = soundManager.createSound({
                              id: config.SOUND_ID_LOS,
                              url: config.SOUND_URL_LOS,
                              volume: notificationsVolume
                            });
                        var AOSinOne = soundManager.createSound({
                              id: config.SOUND_ID_AOSINONE,
                              url: config.SOUND_URL_AOSINONE,
                              volume: notificationsVolume
                            });
                        var LOSinOne = soundManager.createSound({
                              id: config.SOUND_ID_LOSINONE,
                              url: config.SOUND_URL_LOSINONE,
                              volume: notificationsVolume
                            });
                        var AOSinFive = soundManager.createSound({
                              id: config.SOUND_ID_AOSINFIVE,
                              url: config.SOUND_URL_AOSINFIVE,
                              volume: notificationsVolume
                            });
                        var LOSinFive = soundManager.createSound({
                              id: config.SOUND_ID_LOSINFIVE,
                              url: config.SOUND_URL_LOSINFIVE,
                              volume: notificationsVolume
                            });
                        var Frames = soundManager.createSound({
                            id: config.SOUND_ID_FRAME,
                            url: config.SOUND_URL_FRAME,
                            volume: notificationsVolume
                          });
        
                        if(Cookie(config.COOKIE_NOTIFICATIONS_MUTE) === undefined){
                            Cookie(config.COOKIE_NOTIFICATIONS_MUTE, "false", { expires: cookieExpires });
                        }
                        if(Cookie(config.COOKIE_STREAM_MUTE) === undefined){
                            Cookie(config.COOKIE_STREAM_MUTE, "true", { expires: cookieExpires });
                        }
                        
                        mySound.play();
                        
                        if(Cookie(config.COOKIE_STREAM_MUTE)=="true"){
                            soundManager.toggleMute(config.SOUND_ID_STREAM);
                        }
                        
                        if(Cookie(config.COOKIE_NOTIFICATIONS_MUTE) == "true"){
                            
                            soundManager.toggleMute(config.SOUND_ID_AOS);
                            soundManager.toggleMute(config.SOUND_ID_LOS);
                            soundManager.toggleMute(config.SOUND_ID_AOSINONE);
                            soundManager.toggleMute(config.SOUND_ID_LOSINONE);
                            soundManager.toggleMute(config.SOUND_ID_AOSINFIVE);
                            soundManager.toggleMute(config.SOUND_ID_LOSINFIVE);
                            soundManager.toggleMute(config.SOUND_ID_FRAME);
                            
                        }
                                    
                       }
                    });
                    
                    
                    
                    
                    
                }
                
            });  
        }
    );
