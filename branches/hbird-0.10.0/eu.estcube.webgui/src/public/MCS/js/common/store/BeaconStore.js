define([
    "dojo/store/Memory",
    "dojo/store/Observable",
    "dojo/_base/array",
    "config/config",
    "dojo/request",
    "./StoreMonitor"
    ],

    function(Memory, Observable, Arrays, Config, Request, StoreMonitor) {
        var store = new Observable(new Memory({ idProperty: "timestamp" }));
        
        store.update = function() {
            Request.get(Config.URL_CATALOGUE_BEACONS, {handleAs: "json"}).then(function(beacons) {
                Arrays.forEach(beacons, function(beacon) {
                    store.put(beacon);
                });
            });
        }
        
        store.update();
        
        setInterval(function() { store.update() }, Config.BEACON_UPDATE_INTERVAL);
        
        new StoreMonitor({ store: store, storeName: "BeaconStore" });
        
        return store;
    }
);