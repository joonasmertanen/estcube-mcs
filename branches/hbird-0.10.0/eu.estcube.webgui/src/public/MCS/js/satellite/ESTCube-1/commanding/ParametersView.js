define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dijit/Dialog",
    "dgrid/OnDemandGrid",
    "config/config",
    "common/formatter/DateFormatter",
    "common/store/EstCube1CommandingParametersStore",
    "common/display/ObjectToDiv",
    "common/formatter/TelemetryFormatter", 
    ],

    function(declare, Lang, DomConstruct, DomClass, Dialog, Grid, config, DateFormatter, store, ObjectToDiv,TelemetryFormatter) {
        return declare([], {
            constructor: function(args) {
                this.grid = new Grid({
                    id:"parametersView",
                    store: store,
                    columns: {
                        timestamp: { 
                            label: "Timestamp", 
                            formatter: DateFormatter, 
                            className: "field-timestamp", },
                        name: { 
                            label: "Name", 
                            className: "field-description", },
                        value: { 
                            label: "Value", 
                            className: "field-value",
                            renderCell: function (object, value, node, options) {
                                node.innerHTML = TelemetryFormatter(object);                             
                            }
                            },
                        issuedBy: { 
                            label: "Issued by", 
                            className: "field-issuedBy", },  
                        unit: { 
                            label: "Unit", 
                            className: "field-unit", },

                    }
                });
                this.grid.set("sort", "timestamp", true);
                DomClass.add(this.grid.domNode, "fill");

                var dialog = new Dialog({
                    title: "Last received parameters",
                    style: "width: 800px; background-color: #ffffff",
                });

                this.grid.startup();
    
                // XXX - workaround for grid titles not visible bug
                setTimeout(Lang.hitch(this, function() { this.grid.set("showHeader", true) }), 500);
            },

            placeAt: function(container) {
                DomConstruct.place(this.grid.domNode, container);
            },

        });
    }
);
