define(["dojo/_base/declare", 
        "dojo/_base/lang", 
        "dojo/dom-construct",
        "dojo/dom-class", 
        "dgrid/OnDemandGrid", 
        "config/config",
        "common/formatter/DateFormatter", 
        "common/store/ParameterStore", 
        "dijit/layout/TabContainer", 
        "dijit/layout/ContentPane",
        "common/display/TelemetryContentProvider", 
        "common/store/TelemetryQuery",
         ],

    function(declare, Lang, DomConstruct, DomClass, Grid, config, DateFormatter,ParameterStore,TabContainer,ContentPane,TelemetryContentProvider,TelemetryQuery) {   
        return declare([], {    
            constructor: function (args) {
                this.tabContainer = new TabContainer({
                    "class": "fill"
                });
                var telemetry = new TelemetryContentProvider({
                    columns: {
                        timestamp: {
                            label: "Timestamp",
                            formatter: DateFormatter,
                            className: "field-timestamp",
                        },
                        name: {
                            label: "ID",
                            className: "field-name",
                        },
                        name: {
                            label: "Name",
                            className: "field-description",
                        },
                        value: {
                            label: "Value",
                            className: "field-value",
                        },
                        unit: {
                            label: "Unit",
                            className: "field-unit"
                        },
                        issuedBy: {
                            label: "Issued by",
                            className: "field-issuedBy",
                        },
                    }
                });
                var cdhsQuery = Lang.partial(TelemetryQuery, ["CDHS"], undefined);
                var cdhs = new TelemetryContentProvider({
                    query: cdhsQuery
                });

                var epsQuery = Lang.partial(TelemetryQuery, ["EPS"], undefined);
                var eps = new TelemetryContentProvider({
                    query: epsQuery
                });

                var comQuery = Lang.partial(TelemetryQuery, ["COM"], undefined);
                var com = new TelemetryContentProvider({
                    query: comQuery
                });

                var camQuery = Lang.partial(TelemetryQuery, ["CAM"], undefined);
                var cam = new TelemetryContentProvider({
                    query: camQuery
                });

                var tcsQuery = Lang.partial(TelemetryQuery, ["TCS"], ["internal_raw", "rtc", "internal", "core_temp", "rtc_temp", "bat_temp_b", "bat_temp_a"]);
                var tcs = new TelemetryContentProvider({
                    query: tcsQuery
                });

                var plQuery = Lang.partial(TelemetryQuery, ["CDHS"], ["egun", "dummy", "timestamp", "tether", "anode", "rtotal", "rtime", "hivolt", "rfeg", "rspeed"]);
                var pl = new TelemetryContentProvider({
                    query: plQuery
                });

                var adcsQuery = Lang.partial(TelemetryQuery, ["CDHS"], ["timestamp", "art1", "art2", "g3t1", "g3t4", "g3t3", "g3t2", "g3t1", "at2", "gt4", "gt3", "gt2"]);
                var adcs = new TelemetryContentProvider({
                    query: adcsQuery
                });


                this.tabContainer.addChild(this.prepareGrid(telemetry.getContent(), "Telemetry"));
                this.tabContainer.addChild(this.prepareGrid(cdhs.getContent(), "CDHS"));


                this.tabContainer.addChild(this.prepareGrid(eps.getContent(), "EPS"));
                this.tabContainer.addChild(this.prepareGrid(com.getContent(), "COM"));
                this.tabContainer.addChild(this.prepareGrid(cam.getContent(), "CAM"));
                this.tabContainer.addChild(this.prepareGrid(tcs.getContent(), "TCS"));
                this.tabContainer.addChild(this.prepareGrid(pl.getContent(), "PL"));
                this.tabContainer.addChild(this.prepareGrid(adcs.getContent(), "ADCS"));


            },
            placeAt: function (container) {

                this.tabContainer.placeAt(container);
                this.tabContainer.startup();

            },
            prepareGrid: function (grid, title) {

                DomClass.add(grid.domNode, "fill");
                var gridPlaceholder = DomConstruct.create("div", {
                    "class": "fill"
                });

                DomConstruct.place(grid.domNode, gridPlaceholder);
                var contentPane = new ContentPane({
                    title: title,
                    content: gridPlaceholder
                });
                contentPane.on("show", function () {
                    grid.set("showHeader", true);
                });
                return contentPane;
            },
    
        });
    });
