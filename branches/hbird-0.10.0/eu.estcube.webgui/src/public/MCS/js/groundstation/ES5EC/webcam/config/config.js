define([
    "dojo/dom",
    "dojo/domReady!"
    ],

    function(dom) {
        return {
            routes: {
                ES5EC_WEBCAM: {
                    path: "ES5EC/webcam/",
                    defaults: {
                        controller: "ES5ECWebCam/WebCamController",
                        method: "index",
                    }
                },
            },

            ES5EC_WEBCAM: {
                initialImage: "/images/image.jpg",
                imageId: "/ESTCUBE/GroundStations/ES5EC/Image",
            }

        };
    }
);
