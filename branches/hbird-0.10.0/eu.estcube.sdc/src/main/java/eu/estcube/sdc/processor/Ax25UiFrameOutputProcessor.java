/** 
 *
 */
package eu.estcube.sdc.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.util.Dates;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.common.Headers;
import eu.estcube.common.TimestampExtractor;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.sdc.domain.Ax25UiFrameOutput;

/**
 *
 */
@Component
public class Ax25UiFrameOutputProcessor implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(Ax25UiFrameOutputProcessor.class);

    @Autowired
    private Ax25UiFrameToOutputConverter converter;

    @Autowired
    private TimestampExtractor timestampExtractor;

    /** @{inheritDoc . */
    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        out.copyFrom(in);

        try {
            Ax25UIFrame frame = in.getBody(Ax25UIFrame.class);
            if (frame == null) {
                throw new IllegalArgumentException("No Ax25UIFrame available in message body");
            }
            Ax25UiFrameOutput output = converter.convert(frame);

            long timestamp = timestampExtractor.getTimestamp(in);
            out.setHeader(StandardArguments.TIMESTAMP, timestamp);
            String timestampAsIso8601String = Dates.toIso8601DateFormat(timestamp);
            output.setTimestamp(timestampAsIso8601String);
            output.setServiceId(in.getHeader(StandardArguments.ISSUED_BY, String.class));
            output.setSerialPortName(in.getHeader(Headers.SERIAL_PORT_NAME, String.class));
            out.setBody(output);
        } catch (Throwable e) {
            LOG.error("Failed to process Ax25UIFrame to Ax25UiFrameOutput; {}", e.getMessage());
            exchange.setException(e);
        }
    }
}
