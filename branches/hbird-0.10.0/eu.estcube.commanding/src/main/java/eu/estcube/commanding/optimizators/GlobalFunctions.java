package eu.estcube.commanding.optimizators;

import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.hbird.exchange.navigation.PointingData;

import eu.estcube.commanding.calculation.Calculation;

/**
 * Functions for working with satellite trajectory optimizations.
 * 
 * @author Ivar Mahhonin
 * 
 */

public class GlobalFunctions {
	/**
	 * Invented for 360 azimuth rotators.
	 * Changing current azimuth value to the opposite.
	 * 
	 * @param azimuthValue - current azimuth value.
	 * @return azimuth value changed to 180 degrees
	 */

	public static double changeAzimuthTo180(double azimuthValue) {
		if (azimuthValue > 180) {
			azimuthValue = azimuthValue - 180;
		} else {
			azimuthValue = azimuthValue + 180;
		}
		return azimuthValue;
	}

	/**
	 * Invented for >360 azimuth rotators.
	 * Changing current azimuth value to the opposite.
	 * 
	 * @param azimuthValue - current azimuth value.
	 * @return azimuth value changed to 180 degree.
	 */

	public static double changeAzimuthTo360(double azimuthValue) {
		if (azimuthValue > 360) {
			azimuthValue = azimuthValue - 180;
		} else {
			azimuthValue = azimuthValue + 180;
		}
		return azimuthValue;

	}

	/**
	 * Finding the point, where trajectory crosses the north line.
	 * 
	 * @param coordinates - satellite trajectory
	 * @return
	 */

	public static int getCrossPoint(final List<PointingData> coordinates) {
		int crossPoint = 0;
		double lastValue = coordinates.get(0).getAzimuth();
		for (int i = 0; i < coordinates.size(); i++) {
			if (Math.abs(lastValue - coordinates.get(i).getAzimuth()) > 180) {
				crossPoint = i;
			}
			lastValue = coordinates.get(i).getAzimuth();
		}

		return crossPoint;
	}

	/**
	 * Returning degree sector, from where is current azimuth value.
	 * 
	 * @param azimuthValue - current azimuth value
	 * @return azimuth value degree sector
	 */
	public static int getDegreeSector(final double azimuthValue) {
		int sector = 0;
		if ((azimuthValue >= 0) && (azimuthValue <= 90)) {
			sector = 1;
		}
		if ((azimuthValue > 90) && (azimuthValue <= 180)) {
			sector = 2;
		}
		if ((azimuthValue > 180) && (azimuthValue <= 270)) {
			sector = 3;
		}
		if ((azimuthValue > 270) && (azimuthValue <= 359.99)) {
			sector = 4;
		}

		if ((azimuthValue > 360) && (azimuthValue <= 450)) {
			sector = 5;
		}

		return sector;

	}

	/**
	 * Checking whether trajectory movement is clockwise
	 * 
	 * @param coordinates - satellite trajectory
	 * @param crossPoint - point, where trajectory crosses north line
	 * @return
	 */
	public static boolean ifClockwise(final List<PointingData> coordinates,
			final int crossPoint) {
		boolean clockwise = false;
		if (coordinates.get(crossPoint + 1).getAzimuth() > 180) {
			clockwise = false;

		}
		if (coordinates.get(crossPoint + 1).getAzimuth() < 180) {
			clockwise = true;
		}

		Calculation.log.info("CLOCKWISE:" + clockwise);
		return clockwise;
	}

	/**
	 * Checking list for negative and greater than maximum values
	 * 
	 * @param coordinates - satellite trajectory
	 * @return checked list
	 */
	public static List<PointingData> listFinalCheck(
			List<PointingData> coordinates) {
		final List<PointingData> coordinatesTemp = new ArrayList<PointingData>();

		for (int i = 0; i < coordinates.size(); i++) {
			if (((coordinates.get(i).getElevation() > 0) && (coordinates.get(i)
					.getElevation() < 180))
					&& ((coordinates.get(i).getAzimuth() > 0) && (coordinates
							.get(i).getAzimuth() < 360))) {
				coordinatesTemp.add(coordinates.get(i));
			}

		}
		coordinates = coordinatesTemp;
		return coordinates;

	}

	/**
	 * Printing values in {@link List} satellite trajectory
	 * 
	 * @param coordinates
	 */
	public static void printValues(final List<PointingData> coordinates) {
		Date d;
		final Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
		for (int i = 0; i < coordinates.size(); i++) {
			d = new Date(coordinates.get(i).getTimestamp());
			Calculation.log.info(i + " az " + coordinates.get(i).getAzimuth()
					+ " el " + coordinates.get(i).getElevation() + " time "
					+ format.format(d).toString());

		}

	}

}
