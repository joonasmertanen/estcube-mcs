/** 
 *
 */
package eu.estcube.commanding.api;

import org.hbird.business.groundstation.configuration.RotatorDriverConfiguration;

/**
 *
 */
public interface RotatorTypeSelectorInterface {
	/**
	 * Selects {@link RotatorTypeInterface} for given Rotator.
	 * 
	 * @param rotator {@link Rotator} to select {@link RotatorTypeInterface} for
	 * @return {@link RotatorTypeInterface} for given {@link Rotator}
	 */
	public RotatorTypeInterface selectFor(RotatorDriverConfiguration rotator);
}
