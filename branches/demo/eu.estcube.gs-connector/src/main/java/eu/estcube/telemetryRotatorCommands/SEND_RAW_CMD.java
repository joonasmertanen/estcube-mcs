package eu.estcube.telemetryRotatorCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class SEND_RAW_CMD extends RotatorStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        
        
        messageString.append("+");
        messageString.append("w");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Cmd"));
        messageString.append("\n");
        
        
        return messageString;
    }
}
