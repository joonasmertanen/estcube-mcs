package eu.estcube.telemetryRadioCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class SET_MEMORY_CHANNELDATA extends RadioStringBuilder{
    private StringBuilder messageString;
    

    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        messageString.append("+");
        messageString.append("H");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Channel"));
        messageString.append("\n");
        return messageString;
    }
}
