package eu.estcube.telemetryRadioCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class SET_MODE extends RadioStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        messageString.append("+");
        messageString.append("M");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Mode"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Passband"));
        messageString.append("\n");
        return messageString;
    }
}
