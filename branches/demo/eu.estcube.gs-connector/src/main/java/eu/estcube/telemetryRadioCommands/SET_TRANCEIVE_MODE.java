package eu.estcube.telemetryRadioCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class SET_TRANCEIVE_MODE extends RadioStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        messageString.append("+");
        messageString.append("A");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Transceive"));
        messageString.append("\n");
        return messageString;
    }
}
