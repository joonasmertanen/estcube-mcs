package eu.estcube.telemetryRadioCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class SET_TRANSMIT_MODE extends RadioStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        messageString.append("+");
        messageString.append("X");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("TX Mode"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("TX Passband"));
        messageString.append("\n");
        return messageString;
    }
}
