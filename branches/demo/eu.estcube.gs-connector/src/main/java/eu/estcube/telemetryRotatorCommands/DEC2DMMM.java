package eu.estcube.telemetryRotatorCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class DEC2DMMM extends RotatorStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        
        
        messageString.append("+");
        messageString.append("e");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Dec Deg"));
        messageString.append("\n");
        
        
        return messageString;
    }
}
