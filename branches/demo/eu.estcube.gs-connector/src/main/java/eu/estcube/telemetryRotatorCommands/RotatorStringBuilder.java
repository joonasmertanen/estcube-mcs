package eu.estcube.telemetryRotatorCommands;

import eu.estcube.domain.TelemetryCommand;

public class RotatorStringBuilder {
    protected StringBuilder messageString;
    


    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        return messageString;
    }

}
