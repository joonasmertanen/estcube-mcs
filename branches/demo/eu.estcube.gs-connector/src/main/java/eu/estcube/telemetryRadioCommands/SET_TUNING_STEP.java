package eu.estcube.telemetryRadioCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class SET_TUNING_STEP extends RadioStringBuilder{
    private StringBuilder messageString;
    

    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        messageString.append("+");
        messageString.append("N");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Tuning Step"));
        messageString.append("\n");
        return messageString;
    }
}
