package eu.estcube.telemetryRotatorCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class LONLAT2LOC extends RotatorStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        
        messageString.append("+");
        messageString.append("L");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Longitude"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Latitude"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Loc Len"));
        messageString.append("\n");
        
        
        
        return messageString;
    }
}
