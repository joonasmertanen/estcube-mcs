package eu.estcube.telemetryRadioCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class RESET extends RadioStringBuilder {
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        messageString.append("+");
        messageString.append("R");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("RESET"));
        messageString.append("\n");
        return messageString;
    }
}
