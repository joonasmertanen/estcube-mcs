package eu.estcube.telemetryRotatorCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class SET_POSITION extends RotatorStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        messageString.append("+");
        messageString.append("P");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Azimuth"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Elevation"));
        messageString.append("\n");
        return messageString;
    }
}
