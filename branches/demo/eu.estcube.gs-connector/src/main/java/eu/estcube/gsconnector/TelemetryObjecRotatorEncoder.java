package eu.estcube.gsconnector;

import java.util.HashMap;
import java.util.Map;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneDecoder;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRotatorConstants;
import eu.estcube.telemetryRotatorCommands.*;

public class TelemetryObjecRotatorEncoder extends OneToOneEncoder{

    private Map<String, RotatorStringBuilder> commandsHashMap;

    private static final Logger LOG = LoggerFactory.getLogger(TelemetryObjecRotatorEncoder.class);

    public TelemetryObjecRotatorEncoder(){
        commandsHashMap = new HashMap<String, RotatorStringBuilder>();
        commandsHashMap.put("SET_POSITION", new SET_POSITION());
        commandsHashMap.put("GET_POSITION", new GET_POSITION());
        commandsHashMap.put("STOP", new STOP());
        commandsHashMap.put("PARK", new PARK());
        commandsHashMap.put("MOVE", new MOVE());
        commandsHashMap.put("CAPABILITIES", new CAPABILITIES());
        commandsHashMap.put("RESET", new RESET());
        commandsHashMap.put("SET_CONFIG", new SET_CONFIG());
        commandsHashMap.put("GET_INFO", new GET_INFO());
        commandsHashMap.put("SEND_RAW_CMD", new SEND_RAW_CMD());
        commandsHashMap.put("LONLAT2LOC", new LONLAT2LOC());
        commandsHashMap.put("LOC2LONLAT", new LOC2LONLAT());
        commandsHashMap.put("DMS2DEC", new DMS2DEC());
        commandsHashMap.put("DEC2DMS", new DEC2DMS());
        commandsHashMap.put("DMMM2DEC", new DMMM2DEC());
        commandsHashMap.put("DEC2DMMM", new DEC2DMMM());
        commandsHashMap.put("QRB", new QRB());
        commandsHashMap.put("AZIMUTH_SHORTPATH2LONGPATH", new AZIMUTH_SHORTPATH2LONGPATH());
        commandsHashMap.put("DISTANCE_SHORTPATH2LONGPATH", new DISTANCE_SHORTPATH2LONGPATH());

    }
    
    @Override
    protected Object encode(ChannelHandlerContext ctx, Channel channel, Object message) throws Exception {
        TelemetryCommand telemetryCommand = (TelemetryCommand) message;
        StringBuilder messageString=new StringBuilder();
        messageString.append(commandsHashMap.get(telemetryCommand.getName()).createMessageString(telemetryCommand));
        
        
        if(telemetryCommand.getName().equals(TelemetryRotatorConstants.SET_POSITION)){
            messageString.append("+");
            messageString.append("P");
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Azimuth"));
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Elevation"));
            messageString.append("\n");
        }
        else if(telemetryCommand.getName().equals(TelemetryRotatorConstants.GET_POSITION)){
            messageString.append("+");
            messageString.append("p");
            messageString.append("\n");
        }
        else if(telemetryCommand.getName().equals(TelemetryRotatorConstants.STOP)){
            messageString.append("+");
            messageString.append("S");
            messageString.append("\n");
        }
        else if(telemetryCommand.getName().equals(TelemetryRotatorConstants.PARK)){
            messageString.append("+");
            messageString.append("K");
            messageString.append("\n");
        }
        else if(telemetryCommand.getName().equals(TelemetryRotatorConstants.MOVE)){
            messageString.append("+");
            messageString.append("M");
            messageString.append("\n");
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Direction"));
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Speed"));
        }
        else if(telemetryCommand.getName().equals(TelemetryRotatorConstants.CAPABILITIES)){
            messageString.append("1");
            messageString.append("\n");
        }
        else if(telemetryCommand.getName().equals(TelemetryRotatorConstants.RESET)){
            messageString.append("+");
            messageString.append("R");
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get(TelemetryRotatorConstants.RESET));
            messageString.append("\n");
        }
        else if(telemetryCommand.getName().equals(TelemetryRotatorConstants.SET_CONFIG)){
            messageString.append("+");
            messageString.append("C");
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Token"));
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Value"));
            messageString.append("\n");
        }
        else if(telemetryCommand.getName().equals(TelemetryRotatorConstants.GET_INFO)){
            messageString.append("+");
            messageString.append("_");
            messageString.append("\n");
        }
        else if(telemetryCommand.getName().equals(TelemetryRotatorConstants.SEND_RAW_CMD)){
            messageString.append("+");
            messageString.append("w");
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Cmd"));
            messageString.append("\n");
        }// DELETE/CHANGE LIKE THIS? (FROM CONSTANTS AS WELL)
        else if(telemetryCommand.getName().equals(TelemetryRotatorConstants.LONLAT2LOC)){
            messageString.append("+");
            messageString.append("L");
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Longitude"));
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Latitude"));
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Loc Len"));
            messageString.append("\n");
        }
        else if(telemetryCommand.getName().equals(TelemetryRotatorConstants.LOC2LONLAT)){
            messageString.append("+");
            messageString.append("l");
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Locator"));
            messageString.append("\n");
        }
        else if(telemetryCommand.getName().equals(TelemetryRotatorConstants.DMS2DEC)){
            messageString.append("+");
            messageString.append("D");
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Degrees"));
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Minutes"));
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Seconds"));
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("S/W"));
            messageString.append("\n");
        }
        else if(telemetryCommand.getName().equals(TelemetryRotatorConstants.DEC2DMS)){
            messageString.append("+");
            messageString.append("d");
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Dec Degrees"));
            messageString.append("\n");
        }
        else if(telemetryCommand.getName().equals(TelemetryRotatorConstants.DMMM2DEC)){
            messageString.append("+");
            messageString.append("E");
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Degrees"));
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Dec Minutes"));
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("S/W"));
            messageString.append("\n");
        }
        else if(telemetryCommand.getName().equals(TelemetryRotatorConstants.DEC2DMMM)){
            messageString.append("+");
            messageString.append("e");
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Dec Deg"));
            messageString.append("\n");
        }
        else if(telemetryCommand.getName().equals(TelemetryRotatorConstants.QRB)){
            messageString.append("+");
            messageString.append("B");
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Lon 1"));
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Lat 1"));
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Lon 2"));
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Lat 2"));
            messageString.append("\n");
        }
        else if(telemetryCommand.getName().equals(TelemetryRotatorConstants.AZIMUTH_SHORTPATH2LONGPATH)){
            messageString.append("+");
            messageString.append("A");
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Short Path Deg"));
            messageString.append("\n");
        }
        else if(telemetryCommand.getName().equals(TelemetryRotatorConstants.DISTANCE_SHORTPATH2LONGPATH)){
            messageString.append("+");
            messageString.append("a");
            messageString.append(" ");
            messageString.append(telemetryCommand.getParams().get("Short Path km"));
            messageString.append("\n");
        }

        return messageString;
    }
}
