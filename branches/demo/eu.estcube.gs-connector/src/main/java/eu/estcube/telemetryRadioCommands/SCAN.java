package eu.estcube.telemetryRadioCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class SCAN extends RadioStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        messageString.append("+");
        messageString.append("g");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("SCAN Fct"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("SCAN Channel"));
        messageString.append("\n");
        return messageString;
    }
}
