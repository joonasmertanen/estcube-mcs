package eu.estcube.telemetryRadioCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class SET_LEVEL extends RadioStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        messageString.append("+");
        messageString.append("L");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Level"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Level Value"));
        messageString.append("\n");
        return messageString;
    }
}
