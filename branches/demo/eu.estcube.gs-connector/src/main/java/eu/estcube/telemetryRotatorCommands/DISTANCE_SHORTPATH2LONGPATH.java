package eu.estcube.telemetryRotatorCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class DISTANCE_SHORTPATH2LONGPATH extends RotatorStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        
        
        messageString.append("+");
        messageString.append("a");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Short Path km"));
        messageString.append("\n");
        
        
        return messageString;
    }
}
