package eu.estcube.telemetryRadioCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class SET_ANTENNA_NR extends RadioStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        messageString.append("+");
        messageString.append("Y");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Antenna"));
        messageString.append("\n");
        return messageString;
    }
}
