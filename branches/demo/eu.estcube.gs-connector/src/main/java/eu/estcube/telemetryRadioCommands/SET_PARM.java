package eu.estcube.telemetryRadioCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class SET_PARM extends RadioStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        messageString.append("+");
        messageString.append("P");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Parm"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Parm Value"));
        messageString.append("\n");
        return messageString;
    }
}
