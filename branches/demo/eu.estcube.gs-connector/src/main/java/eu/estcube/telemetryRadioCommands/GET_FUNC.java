package eu.estcube.telemetryRadioCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class GET_FUNC extends RadioStringBuilder{
    private StringBuilder messageString;


    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        messageString.append("+");
        messageString.append("u");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Func"));
        messageString.append("\n");
        return messageString;
    }
}
