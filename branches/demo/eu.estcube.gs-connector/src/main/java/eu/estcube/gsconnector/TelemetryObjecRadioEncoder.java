package eu.estcube.gsconnector;

import java.util.HashMap;
import java.util.Map;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneDecoder;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.telemetryRadioCommands.*;

public class TelemetryObjecRadioEncoder extends OneToOneEncoder{
    private Map<String, RadioStringBuilder> commandsHashMap;
    private static final Logger LOG = LoggerFactory.getLogger(TelemetryObjecRadioEncoder.class);
    
    public TelemetryObjecRadioEncoder(){
        commandsHashMap = new HashMap<String, RadioStringBuilder>();
        commandsHashMap.put("SET_FREQUENCY", new SET_FREQUENCY());
        commandsHashMap.put("GET_FREQUENCY", new GET_FREQUENCY());
        commandsHashMap.put("SET_MODE", new SET_MODE());
        commandsHashMap.put("SET_LEVEL", new SET_LEVEL());
        commandsHashMap.put("SET_VFO", new SET_VFO());
        commandsHashMap.put("SET_PTT", new SET_PTT());
        commandsHashMap.put("GET_PTT", new GET_PTT());
        commandsHashMap.put("SET_ANTENNA_NR", new SET_ANTENNA_NR());
        commandsHashMap.put("CAPABILITIES", new CAPABILITIES());
        commandsHashMap.put("CONFIGURATION", new CONFIGURATION());
        commandsHashMap.put("GET_MODE", new GET_MODE());
        commandsHashMap.put("SET_TRANSMIT_FREQUENCY", new SET_TRANSMIT_FREQUENCY());
        commandsHashMap.put("GET_TRANSMIT_FREQUENCY", new GET_TRANSMIT_FREQUENCY());
        commandsHashMap.put("SET_TRANSMIT_MODE", new SET_TRANSMIT_MODE());
        commandsHashMap.put("GET_TRANSMIT_MODE", new GET_TRANSMIT_MODE());
        commandsHashMap.put("SET_SPLIT_VFO", new SET_SPLIT_VFO());
        commandsHashMap.put("GET_SPLIT_VFO", new GET_SPLIT_VFO());
        commandsHashMap.put("SET_TUNING_STEP", new SET_TUNING_STEP());
        commandsHashMap.put("GET_TUNING_STEP", new GET_TUNING_STEP());
        commandsHashMap.put("GET_LEVEL", new GET_LEVEL());
        commandsHashMap.put("SET_FUNC", new SET_FUNC());
        commandsHashMap.put("GET_FUNC", new GET_FUNC());
        commandsHashMap.put("SET_PARM", new SET_PARM());
        commandsHashMap.put("GET_PARM", new GET_PARM());
        commandsHashMap.put("VFO_OPERATION", new VFO_OPERATION());
        commandsHashMap.put("SCAN", new SCAN());
        commandsHashMap.put("SET_TRANCEIVE_MODE", new SET_TRANCEIVE_MODE());
        commandsHashMap.put("GET_TRANCEIVE_MODE", new GET_TRANCEIVE_MODE());
        commandsHashMap.put("SET_RPTR_SHIFT", new SET_RPTR_SHIFT());
        commandsHashMap.put("GET_RPTR_SHIFT", new GET_RPTR_SHIFT());
        commandsHashMap.put("SET_RPTR_OFFSET", new SET_RPTR_OFFSET());
        commandsHashMap.put("GET_RPTR_OFFSET", new GET_RPTR_OFFSET());
        commandsHashMap.put("SET_CTCSS_TONE", new SET_CTCSS_TONE());
        commandsHashMap.put("GET_CTCSS_TONE", new GET_CTCSS_TONE());
        commandsHashMap.put("SET_DCS_CODE", new SET_DCS_CODE());
        commandsHashMap.put("GET_DCS_CODE", new GET_DCS_CODE());
        commandsHashMap.put("SET_MEMORY_CHANNELNR", new SET_MEMORY_CHANNELNR());
        commandsHashMap.put("GET_MEMORY_CHANNELNR", new GET_MEMORY_CHANNELNR());
        commandsHashMap.put("SET_MEMORY_CHANNELDATA", new SET_MEMORY_CHANNELDATA());
        commandsHashMap.put("GET_MEMORY_CHANNELDATA", new GET_MEMORY_CHANNELDATA());
        commandsHashMap.put("SET_MEMORYBANK_NR", new SET_MEMORYBANK_NR());
        commandsHashMap.put("GET_INFO", new GET_INFO());
        commandsHashMap.put("SET_RIT", new SET_RIT());
        commandsHashMap.put("GET_RIT", new GET_RIT());
        commandsHashMap.put("SET_XIT", new SET_XIT());
        commandsHashMap.put("GET_XIT", new GET_XIT());
        commandsHashMap.put("GET_ANTENNA_NR", new GET_ANTENNA_NR());
        commandsHashMap.put("RESET", new RESET());
        commandsHashMap.put("SEND_RAW_CMD", new SEND_RAW_CMD());
        commandsHashMap.put("SEND_MORSE", new SEND_MORSE());
        commandsHashMap.put("CONVERT_POWER2MW", new CONVERT_POWER2MW());
        commandsHashMap.put("CONVERT_MW2POWER", new CONVERT_MW2POWER());
        
    }

    @Override
    protected Object encode(ChannelHandlerContext ctx, Channel channel, Object message) throws Exception {
        TelemetryCommand telemetryCommand = (TelemetryCommand) message;
        StringBuilder messageString=new StringBuilder();
        messageString.append(commandsHashMap.get(telemetryCommand.getName()).createMessageString(telemetryCommand));
        
//        if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_FREQUENCY)){
//            messageString.append("+");
//            messageString.append("F");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Frequency"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_FREQUENCY)){
//            messageString.append("+");
//            messageString.append("f");
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_MODE)){
//            messageString.append("+");
//            messageString.append("M");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Mode"));
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Passband"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_LEVEL)){
//            messageString.append("+");
//            messageString.append("L");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Level"));
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Level Value"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_VFO)){
//            messageString.append("+");
//            messageString.append("V");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("VFO"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_PTT)){
//            messageString.append("+");
//            messageString.append("T");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("PTT"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_PTT)){
//            messageString.append("+");
//            messageString.append("t");
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_ANTENNA_NR)){
//            messageString.append("+");
//            messageString.append("Y");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Antenna"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.CAPABILITIES)){
//            messageString.append("+");
//            messageString.append("1");
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.CONFIGURATION)){
//            messageString.append("+");
//            messageString.append("3");
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_MODE)){
//            messageString.append("+");
//            messageString.append("m");
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_TRANSMIT_FREQUENCY)){
//            messageString.append("+");
//            messageString.append("l");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("TX Frequency"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_TRANSMIT_FREQUENCY)){
//            messageString.append("+");
//            messageString.append("i");
//            messageString.append("\n");
//        }
//        //2 nime sellel
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_TRANSMIT_MODE)){
//            messageString.append("+");
//            messageString.append("X");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("TX Mode"));
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("TX Passband"));
//            messageString.append("\n");
//        }
//        //2 nime sellel
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_TRANSMIT_MODE)){
//            messageString.append("+");
//            messageString.append("x");
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_SPLIT_VFO)){
//            messageString.append("+");
//            messageString.append("S");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Split"));
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("TX VFO"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_SPLIT_VFO)){
//            messageString.append("+");
//            messageString.append("s");
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_TUNING_STEP)){
//            messageString.append("+");
//            messageString.append("N");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Tuning Step"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_TUNING_STEP)){
//            messageString.append("+");
//            messageString.append("n");
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_LEVEL)){
//            messageString.append("+");
//            messageString.append("l");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Level"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_FUNC)){
//            messageString.append("+");
//            messageString.append("U");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Func"));
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Func Status"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_FUNC)){
//            messageString.append("+");
//            messageString.append("u");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Func"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_PARM)){
//            messageString.append("+");
//            messageString.append("P");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Parm"));
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Parm Value"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_PARM)){
//            messageString.append("+");
//            messageString.append("p");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Parm"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.VFO_OPERATION)){
//            messageString.append("+");
//            messageString.append("G");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Mem/VFO Op"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SCAN)){
//            messageString.append("+");
//            messageString.append("g");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("SCAN Fct"));
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("SCAN Channel"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_TRANCEIVE_MODE)){
//            messageString.append("+");
//            messageString.append("A");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Transceive"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_TRANCEIVE_MODE)){
//            messageString.append("+");
//            messageString.append("a");
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_RPTR_SHIFT)){
//            messageString.append("+");
//            messageString.append("R");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Rptr Shift"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_RPTR_SHIFT)){
//            messageString.append("+");
//            messageString.append("r");
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_RPTR_OFFSET)){
//            messageString.append("+");
//            messageString.append("O");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Rptr Offset"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_RPTR_OFFSET)){
//            messageString.append("+");
//            messageString.append("o");
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_CTCSS_TONE)){
//            messageString.append("+");
//            messageString.append("C");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("CTCSS Tone"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_CTCSS_TONE)){
//            messageString.append("+");
//            messageString.append("c");
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_DCS_CODE)){
//            messageString.append("+");
//            messageString.append("D");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("DCS Code"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_DCS_CODE)){
//            messageString.append("+");
//            messageString.append("d");
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_VFO)){
//            messageString.append("+");
//            messageString.append("v");
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_MEMORY_CHANNELNR)){
//            messageString.append("+");
//            messageString.append("E");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Memory#"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_MEMORY_CHANNELNR)){
//            messageString.append("+");
//            messageString.append("e");
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_MEMORY_CHANNELDATA)){
//            messageString.append("+");
//            messageString.append("H");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Channel"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_MEMORY_CHANNELDATA)){
//            messageString.append("+");
//            messageString.append("h");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Channel"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_MEMORYBANK_NR)){
//            messageString.append("+");
//            messageString.append("B");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Bank"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_INFO)){
//            messageString.append("+");
//            messageString.append("_");
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_RIT)){
//            messageString.append("+");
//            messageString.append("J");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("RIT"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_RIT)){
//            messageString.append("+");
//            messageString.append("j");
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SET_XIT)){
//            messageString.append("+");
//            messageString.append("Z");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("XIT"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_XIT)){
//            messageString.append("+");
//            messageString.append("z");
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.GET_ANTENNA_NR)){
//            messageString.append("+");
//            messageString.append("y");
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.RESET)){
//            messageString.append("+");
//            messageString.append("R");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("RESET"));
//            messageString.append("\n");
//        }
//
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SEND_RAW_CMD)){
//            messageString.append("+");
//            messageString.append("w");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Cmd"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.SEND_MORSE)){
//            messageString.append("+");
//            messageString.append("b");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Morse"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.CONVERT_POWER2MW)){
//            messageString.append("+");
//            messageString.append("2");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Power"));
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Frequency"));
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Mode"));
//            messageString.append("\n");
//        }
//        else if(telemetryCommand.getName().equals(TelemetryRadioConstants.CONVERT_MW2POWER)){
//            messageString.append("+");
//            messageString.append("4");
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Power mW"));
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Frequency"));
//            messageString.append(" ");
//            messageString.append(telemetryCommand.getParams().get("Mode"));
//            messageString.append("\n");
//        }     


        return messageString;
    }
}
