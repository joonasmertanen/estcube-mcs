package eu.estcube.telemetryRotatorCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class SET_CONFIG extends RotatorStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        
        
        
        messageString.append("+");
        messageString.append("C");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Token"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Value"));
        messageString.append("\n");
        
        return messageString;
    }
}
