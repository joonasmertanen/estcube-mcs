package eu.estcube.telemetryRotatorCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class MOVE extends RotatorStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        
        messageString.append("+");
        messageString.append("M");
        messageString.append("\n");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Direction"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Speed"));
        
        
        
        return messageString;
    }
}
