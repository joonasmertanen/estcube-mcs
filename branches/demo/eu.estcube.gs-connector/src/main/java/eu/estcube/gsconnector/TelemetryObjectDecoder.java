package eu.estcube.gsconnector;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryParameter;

public class TelemetryObjectDecoder extends OneToOneDecoder{

    private static final Logger LOG = LoggerFactory.getLogger(TelemetryObjecRotatorEncoder.class);
    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, Object message) throws Exception {
        TelemetryObject telemetryObject = new TelemetryObject("Reply");
        LOG.debug("Message recieved:\n{}", message.toString());
        
        String[] messageSplit = message.toString().split("\n");
        String[] messagePiece={null, null};
        
        for(int i=0;i<messageSplit.length;i++){

            if(messageSplit[i].contains(":\t\t")){
                messagePiece = messageSplit[i].split(":\t\t");
            }
            else if(messageSplit[i].contains(":\t")){
                messagePiece = messageSplit[i].split(":\t");
            }
            else if(messageSplit[i].contains(": ")){
                messagePiece = messageSplit[i].split(": ");
            }
            else if(messageSplit[i].contains(" ")){
                messagePiece = messageSplit[i].split(" ");
            }
            else{
                LOG.debug("Got into continue");
                continue;
            }
            telemetryObject.addParameter(new TelemetryParameter(messagePiece[0], messagePiece[1]));
            LOG.debug("Message piece 1: {}, Message piece 2: {}", messagePiece[0], messagePiece[1]);   
        }
        return telemetryObject;
//        return null;
    }

}
