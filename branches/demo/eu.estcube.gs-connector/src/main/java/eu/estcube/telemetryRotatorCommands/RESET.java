package eu.estcube.telemetryRotatorCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.domain.TelemetryRotatorConstants;

public class RESET extends RotatorStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        
        
        
        messageString.append("+");
        messageString.append("R");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get(TelemetryRotatorConstants.RESET));
        messageString.append("\n");
        
        return messageString;
    }
}
