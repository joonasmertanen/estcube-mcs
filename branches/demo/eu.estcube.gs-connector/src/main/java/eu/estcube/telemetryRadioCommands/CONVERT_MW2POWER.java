package eu.estcube.telemetryRadioCommands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class CONVERT_MW2POWER extends RadioStringBuilder{
    private StringBuilder messageString;

    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        messageString.append("+");
        messageString.append("4");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Power mW"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Frequency"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Mode"));
        messageString.append("\n");
        return messageString;
    }
}
