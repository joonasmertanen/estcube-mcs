package eu.estcube.gsconnector;



import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import eu.estcube.domain.JMSConstants;

public class Connector extends RouteBuilder{

    private static final Logger LOG = LoggerFactory.getLogger(Connector.class);

    
    @Autowired
    private TimeRequestProcessor timeRequestProcessor;
    
    @Value("${rotctldAddress}")
    private String rotAddress;
    
    @Value("${rigctldAddress}")
    private String rigAddress;
   
    @Override
    public void configure() throws Exception {
        LOG.info("Starting Connector");
        String netty_rigctld= "netty:tcp://"
                +rigAddress
                    +"?sync=true&decoders=#newLineDecoder,#stringDecoder,#hamlibDecoder&encoders=#my-encoder";
        String netty_rotctld= "netty:tcp://"
                +rotAddress
                    +"?sync=true&decoders=#newLineDecoder,#stringDecoder,#hamlibDecoder&encoders=#my-encoder";
        
        
        
        /*
         * **==========================>>TESTROUTES<<============================**
         * FROM: http://docs.jboss.org/netty/3.2/guide/html_single/index.html
         * Additionally, Netty provides out-of-the-box decoders which enables you to implement most protocols very easily and helps you 
         * avoid from ending up with a monolithic unmaintainable handler implementation. Please refer to the following packages for 
         * more detailed examples: 
         *   org.jboss.netty.example.factorial for a binary protocol, and 
         *   org.jboss.netty.example.telnet for a text line-based protocol.
         *   Camel + Netty demo routes
         */
        
        from("stream:in")
//            .to("netty:tcp://localhost:4533?sync=true&decoders=#newLineDecoder,#stringDecoder,#hamlibDecoder&encoders=#my-encoder")
//            .to("log:echo");
//            .to("netty:tcp://localhost:10111?sync=true")
//            .setBody().simple("p")
            .to("log:result")
            .to(netty_rotctld)
            .to("log:result");

        from("netty:tcp://localhost:10111")
            .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    LOG.debug("IN:" + exchange.getIn().getBody());
                    exchange.getOut().setBody("response: " + exchange.getIn().getBody());
                    LOG.debug("IN:" + exchange.getIn().getBody());
                }
            });
        
        //Time request demo route
        from("activemq:topic:time-request")
            .beanRef("aeg")
            .to("log:time-response")
            .to("activemq:topic:time-response")
            .end();
        
        /*
         * **==========================>>REAL ROUTES<<============================**
         */

        /*
         * rigctld
         */
        //Direct route
        from("direct:rigctld")
            .to(netty_rigctld)
            .to("log:DEBUG")
            .to(JMSConstants.rigSend);            
        
        from(JMSConstants.rigRecive)
            .to("log:DEBUG")
            .to("direct:rigctld");

        
        //Periodical time fire rigctld
//        from("timer://myTimer?period=2000")
//            .setBody().simple("1")
//            .to("log:DEBUG")
//            .to("direct:rotctld");
        
        /*
         * rotctld
         */
        //Direct route
        from("direct:rotctld")
            .to(netty_rotctld)
            .to("log:DEBUG")
            .to(JMSConstants.rotSend);

        //from activeMQ
        from(JMSConstants.rotRecive)
            .to("log:DEBUG")
            .to("direct:rotctld");
        
//        //Periodical time fire rotctld
//        from("timer://myTimer?period=2000")
//            .setBody().simple("1")
//            .log("log:DEBUG")
//            .to("direct:rotctld");
        
              


        
                
        //sudo
        //sudo rpc.rotd -m 1 -vvvv
        //sudo rotctld -m 101 -vvvvv  //4533
        
        //rigctld -m 1901 -vvvvv  //4532
        //rotctl --help
        //nr 2 opn net TCP
        //rotctl -m 2 -r localhost:4533
        // ENDLINE CODES:  \n ; RPTR x\n

    }
    public static void main(String[] args) throws Exception {
        LOG.info("Starting Connector");
        LOG.info("Starting Camel");
        new Main().run(args);

    }




}
