package eu.estcube.telemetryRadioCommands;

import eu.estcube.domain.TelemetryCommand;

public class RadioStringBuilder {
    protected StringBuilder messageString;
    


    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        return messageString;
    }

}
