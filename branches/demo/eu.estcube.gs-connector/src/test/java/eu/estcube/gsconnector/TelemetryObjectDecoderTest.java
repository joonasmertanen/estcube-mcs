package eu.estcube.gsconnector;

import java.nio.charset.Charset;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import junit.framework.TestCase;

public class TelemetryObjectDecoderTest extends TestCase {

    private static final Logger LOG = LoggerFactory.getLogger(TelemetryObjectDecoderTest.class);
    private TelemetryObjectDecoder decoder;
    
    @Before
    public void setUp() throws Exception {
        decoder = new TelemetryObjectDecoder();
    }

    public void testTelemetryObjectDecoder() throws Exception{
        ChannelBuffer buffer = ChannelBuffers.wrappedBuffer(new byte[] {});
        
        Object msg = new Object();
        buffer = ChannelBuffers.wrappedBuffer(new byte[] {
                'x', ':', '\n',
                'a', ':', '\t', 'b', '\n',
                'c', ':', '\t', 'd', '\n',
                'e', ':', '\t', '\t', 'f', '\n',
                'o', ' ', 'e', ':', '\t', '\t', 'f', '\n',
                'm', ':', ' ', '1', '\n',
                '\n', 
                'R', 'P', 'R', 'T', ' ', '0', '\n'});
        msg = ((ChannelBuffer) buffer).toString(Charset.forName("ASCII"));
        decoder.decode(null, null, msg);
        
    }
}
