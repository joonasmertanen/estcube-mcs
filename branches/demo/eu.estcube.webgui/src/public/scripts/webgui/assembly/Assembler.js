﻿dojo.provide("webgui.assembly.Assembler");

dojo.require("webgui.comm.ProxyBase");
dojo.require("webgui.display.MenuDisplay");
dojo.require("webgui.msgbus");
dojo.require("webgui.common.Constants");

dojo.declare("webgui.assembly.Assembler", null, {

    loadAssembly: function() {
    	 new webgui.display.MenuDisplay();
    }
});
