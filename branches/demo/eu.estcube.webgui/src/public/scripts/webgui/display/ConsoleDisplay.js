dojo.provide("webgui.contents.ConsoleDisplay");




// Console Window
var consoleWin;
consoleWin = new dijit.Dialog({
	title: "Console",
	style: "width: 500px; height: 500px;"
});


function showConsoleWin() {
		 	
		var command = dijit.byId("commandBox");
		// set the content of the dialog:
	    consoleWin.attr("content", '<div id = "logArea" ></div>'
	        	+'<div id = "commandBoxArea" float="bottom">Command:'
          		+'<input type="text" dojoType="dijit.form.TextBox"  id="commandBox" float="bottom">'
          		+'<button id = "sendButton" dojoType="dijit.form.Button" type="button" float="bottom"'
          		+'> Send </button></div>' );
	 		
	    dojo.connect(dijit.byId("sendButton"), "onClick", function() {
	    	command.attr("value", send(command.attr("value")));
	        dojo.byId("logArea").innerHTML +=  "\nsent: " + command.attr("value");
	        dojo.byId("logArea").innerHTML +=  "\nrecieved: " + listen();
	        command.attr("value", "");
	     });
	        
	    consoleWin.show();
	    
}