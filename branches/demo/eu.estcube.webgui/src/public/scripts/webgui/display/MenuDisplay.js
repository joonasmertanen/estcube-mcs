dojo.provide("webgui.display.MenuDisplay");

dojo.require("dijit.MenuBar");
dojo.require("dijit.MenuBarItem");
dojo.require("dijit.PopupMenuBarItem");
dojo.require("dijit.Menu");
dojo.require("dijit.MenuItem");
dojo.require("dijit.layout.BorderContainer");
dojo.require("dijit.layout.ContentPane");


dojo.declare("MenuDisplay", null, {

	domId: "MenuDisplay",
	
	constructor: function() {
		//------------------------------
		// Menu Content
		//------------------------------
		//Create testMenu
		
		var mainMenu = new dijit.Menu({style: "width: 100%;"});
		var menuItem1 = new dijit.MenuItem({ label: "Menu1"});
		var menuItem2 = new dijit.MenuItem({ label: "Console", onClick: showConsoleWin });
		var menuItem3 = new dijit.MenuItem({ label: "Menu3"});
		var menuItem4 = new dijit.MenuItem({ label: "Menu4"});
		
		mainMenu.addChild(menuItem1);
		mainMenu.addChild(menuItem2);
		mainMenu.addChild(menuItem3);
		mainMenu.addChild(menuItem4);
		
		
		// create a ContentPane as the left pane in the BorderContainer
		var MenuContent = new dijit.layout.ContentPane({
		   region: "left",
		   style: "height: 100%; width: 10%;",
		   content: mainMenu,
		   layoutPriority: "1;"
		});
		
		var menuPlace = dojo.doc.createElement("div");
        menuPlace.innerHTML = MenuContent;
		
	}
});

dojo.declare("webgui.contents.MenuDisplay", null, {

	constructor: function(args) {
		var menu = new MenuDisplay();
	}
});