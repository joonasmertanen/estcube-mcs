package eu.estcube.webserver;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import eu.estcube.domain.TelemetryCommand;

@Component
public class JsonToTelemetryCommand implements Processor {
	private static final Logger LOG = LoggerFactory
			.getLogger(JsonToTelemetryCommand.class);

	public void process(Exchange ex) throws Exception {
		TelemetryCommand telemetryCommand;

		String json = ex.getIn().getBody(String.class);
		JsonParser parser = new JsonParser();
		JsonObject o = (JsonObject) parser.parse(json);
		String commandName = o.get("name").getAsString();
		telemetryCommand = new TelemetryCommand(commandName);
		JsonArray params = o.getAsJsonArray("parameters");

		int paramsSize = params.size();
		for (int i = 0; i < paramsSize; i++) {
			JsonObject parameter = (JsonObject) params.get(i);
			String paramName = parameter.get("name").getAsString();
			telemetryCommand.addParameter(paramName, parameter.get("value")
					.getAsString());
		}
		ex.getOut().setBody(telemetryCommand);
	}
}
