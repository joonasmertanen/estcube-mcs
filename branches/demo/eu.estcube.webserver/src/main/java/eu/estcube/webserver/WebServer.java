/**
 * 
 */
package eu.estcube.webserver;

import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import eu.estcube.domain.TelemetryCommand;
import org.apache.camel.component.websocket.*;
import eu.estcube.domain.JMSConstants;


public class WebServer extends RouteBuilder{
    
    private static final Logger LOG = LoggerFactory.getLogger(WebServer.class);
    @Value("${webSocketAdress}")
    private String webSocketAdress;
    
    @Value("${webSocketPort}")
    private int webSocketPort;
    
    @Value("${staticResources}")
    private String staticResources;
    
    @Autowired
    private JsonToTelemetryCommand jsonToTelemetryCommand;
    @Autowired
    private TelemetryObjectToJson telemetryObjectToJson;
    
    @Autowired
    private TimeResponseHandler timeResponseHandler;
           
    @Override
    public void configure() throws Exception {
        WebsocketComponent websocketComponent = (WebsocketComponent) getContext().getComponent("websocket");
        websocketComponent.setPort(webSocketPort);
        websocketComponent.setStaticResources(staticResources);      
        Endpoint websocket = websocketComponent.createEndpoint(webSocketAdress); 
    
        
//        from(websocket)
//        	.log("${body}")
//        	.setHeader(WebsocketConstants.SEND_TO_ALL, constant(true))
//        	.log("${body}")
//        	.to(websocket);  // for GUI testing
        	
       
        from("websocket://cache").bean(CacheMessage.class, "getCache")
        	.to("websocket://cache");
        
        from(websocket).process(new Processor() {
            public void process(Exchange exchange) throws Exception {
                LOG.debug((String) exchange.getOut().getBody());
            }
        })   
        .to(JMSConstants.rotRecive);
        		
        from("stream:in")
            .choice()
                .when(body().isEqualTo("time"))
                    .process(new Processor() {
                        public void process(Exchange exchange) throws Exception {
                            exchange.getOut().setBody(new TelemetryCommand("time", null), TelemetryCommand.class);
                        }
                    })
                    .to("activemq:topic:time-request")
                .when(body().isEqualTo("1"))
                    .process(new Processor() {
                        public void process(Exchange exchange) throws Exception {
                            exchange.getOut().setBody("1");
                        }
                    })
                    .to(JMSConstants.rotRecive)
                 .when(body().isEqualTo("testserver"))
                    .process(new Processor() {
                        public void process(Exchange exchange) throws Exception {
                            exchange.getOut().setBody("1");
                        }
                    })
                    .to(JMSConstants.rotRecive) 
                    
                .otherwise()
                    .to("log:echo")
                    .end();
        
        from("activemq:topic:time-response")
            .process(timeResponseHandler)
            .end();
               
        from(JMSConstants.rotSend).setHeader(WebsocketConstants.SEND_TO_ALL, constant(true))
            .to(websocket)
            .to("log:echo");
        
    }
    
    public static void main(String[] args) throws Exception { 
        LOG.info("Starting WebServer");
        new Main().run(args);
    }
}
