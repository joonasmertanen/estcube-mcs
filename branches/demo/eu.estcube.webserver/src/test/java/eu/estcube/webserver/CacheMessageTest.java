package eu.estcube.webserver;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.eclipse.jetty.util.log.Log;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryParameter;


public class CacheMessageTest {

    private CacheMessage cacheMessage;
    private TelemetryObject telemetryObject;
    private TelemetryObject telemetryObject2;
    private String telObjName = "telObjName";
    private String telObjName2 = "telObjName2";
    private TelemetryParameter telemetryParameter;
    private TelemetryParameter telemetryParameter2;
    private String telParName = "telParName";
    private String telParName2 = "telParName2";   
    private String value = "123"; 
    private Double value2 = new Double(2.3334);
    private Exchange ex;
    private Message message;
    private Object answer;

    
    @Before
    public void setUp() throws Exception {
        cacheMessage = new CacheMessage();
          
        telemetryObject = new TelemetryObject(telObjName); 
        telemetryObject2 = new TelemetryObject(telObjName2);
        
        telemetryParameter = new TelemetryParameter(telParName, value);
        telemetryParameter2 = new TelemetryParameter(telParName2, value2);
        
        telemetryObject.addParameter(telemetryParameter);
        telemetryObject.addParameter(telemetryParameter2);
        telemetryObject2.addParameter(telemetryParameter2);
        
        ex = Mockito.mock(Exchange.class);
        message = Mockito.mock(Message.class);
        
        Mockito.when(ex.getIn()).thenReturn(message);
        Mockito.when(ex.getOut()).thenReturn(message);
        
        Mockito.doAnswer(new Answer<Object>() {
            public Object answer(InvocationOnMock invocation) {
                answer = invocation.getArguments()[0];
                return answer;
            }
        }).when(message).setBody(Mockito.any()); 
    }
    
    @Test
    public void testAddToCache() {
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObject);       
        cacheMessage.addToCache(ex);         
        cacheMessage.getCachedElement(ex, telObjName);

        assertEquals(telemetryObject, (TelemetryObject) answer); 
        assertEquals(telemetryParameter, ((TelemetryObject) answer).getParameter(telParName));
    }

    @Test
    public void testGetCachedElement() {
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObject);     
        cacheMessage.addToCache(ex);
        cacheMessage.getCachedElement(ex, telObjName); 
        
        assertEquals(telemetryObject, (TelemetryObject) answer);
        assertEquals(telemetryParameter, ((TelemetryObject) answer).getParameter(telParName));
        assertEquals(telemetryParameter2, ((TelemetryObject) answer).getParameter(telParName2));
        
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObject2);   
        cacheMessage.addToCache(ex);
        cacheMessage.getCachedElement(ex, telObjName2);
        
        assertEquals(telemetryObject2, (TelemetryObject) answer);
        assertEquals(telemetryParameter2, ((TelemetryObject) answer).getParameter(telParName2));
    }

    @Test
    public void testGetCache() {
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObject);
        cacheMessage.addToCache(ex);
      
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObject2);   
        cacheMessage.addToCache(ex);
        
        cacheMessage.getCache(ex);
        
        assertEquals(telemetryObject, ((HashMap)answer).get(telObjName));
        assertEquals(telemetryObject2, ((HashMap)answer).get(telObjName2)); 
    }

}
