package eu.estcube.domain;

public class JMSConstants {
        public static final String rigRecive = "activemq:topic:rigReceive";
        public static final String rigSend = "activemq:topic:rigSend";
        public static final String rotRecive = "activemq:topic:rotReceive";
        public static final String rotSend = "activemq:topic:rotSend";

    public JMSConstants() {

    }
}
