package eu.estcube.domain;

public class TelemetryRotatorConstants {
    public static final String SET_POSITION = "SET_POSITION";
    public static final String GET_POSITION = "GET_POSITION";
    public static final String STOP = "STOP";
    public static final String PARK = "PARK";   
    public static final String MOVE = "MOVE";
    public static final String CAPABILITIES = "CAPABILITIES";
    public static final String RESET = "RESET";
    public static final String SET_CONFIG = "SET_CONFIG";
    public static final String GET_INFO = "GET_INFO";
    public static final String SEND_RAW_CMD = "SEND_RAW_CMD";                     //?
    // public static final String Cmd = "Cmd";                 
    public static final String LONLAT2LOC = "LONLAT2LOC";   
    public static final String LOC2LONLAT = "LOC2LONLAT";
    public static final String DMS2DEC = "DMS2DEC";
    public static final String DEC2DMS = "DEC2DMS";
    public static final String DMMM2DEC = "DMMM2DEC";
    public static final String DEC2DMMM = "DEC2DMMM";
    public static final String QRB = "QRB";
    public static final String AZIMUTH_SHORTPATH2LONGPATH = "AZIMUTH_SHORTPATH2LONGPATH";   
    public static final String DISTANCE_SHORTPATH2LONGPATH = "DISTANCE_SHORTPATH2LONGPATH";

    public TelemetryRotatorConstants() {

    }
}
