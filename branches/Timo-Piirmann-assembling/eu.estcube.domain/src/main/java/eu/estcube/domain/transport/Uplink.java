/** 
 *
 */
package eu.estcube.domain.transport;

/**
 *
 */
public class Uplink {

    public static final String TNC_FRAMES = "activemq:queue:estcube.uplink.tnc";

    public static final String TNC_FRAMES_LOG = "activemq:topic:estcube.uplink.tnc.log";

    public static final String AX25_FRAMES = "activemq:queue:estcube.uplink.ax25";

    public static final String AX25_FRAMES_LOG = "activemq:topic:estcube.uplink.ax25.log";

    public static final String GCP_FRAMES = "activemq:queue:estcube.uplink.gcp";

    public static final String COMMANDS = "activemq:queue:estcube.uplink.commands";
}
