package eu.estcube.commanding.optimizators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.hbird.exchange.navigation.PointingData;
import org.junit.Test;

public class GlobalFunctionsTest {

	public static List<PointingData> coordinatesResult = new ArrayList<PointingData>();
	public static List<PointingData> coordinatesExpected = new ArrayList<PointingData>();

	@Test
	public void changeAzimuthTo360() {
		final int azimuthResult = 406;
		final int azimuthExpected = 226;
		assertTrue(azimuthExpected == GlobalFunctions
				.changeAzimuthTo360(azimuthResult));

	}

	public void fillListsSingle(final double[] azArray, final double[] elArray) {
		for (int i = 0; i < azArray.length; i++) {
			coordinatesExpected.add(new PointingData(0L, azArray[i],
					elArray[i], 123.0, "test", "test"));

		}

	}

	public void fillListsTwo(final double[] azArrayResult,
			final double[] elArrayResult, final double[] azArrayExpected,
			final double[] elArrayExpected) {
		for (int i = 0; i < azArrayExpected.length; i++) {
			coordinatesExpected.add(new PointingData(0L, azArrayExpected[i],
					elArrayExpected[i], 123.0, "test", "test"));

		}

		for (int i = 0; i < azArrayResult.length; i++) {
			coordinatesResult.add(new PointingData(0L, azArrayResult[i],
					elArrayResult[i], 123.0, "test", "test"));

		}

	}

	@Test
	public void getDegreeSector1() {
		final int azimuth = 45;
		final int sectorExpected = 1;
		assertEquals(sectorExpected, GlobalFunctions.getDegreeSector(azimuth));
	}

	@Test
	public void ifClockwise() {
		boolean clockwiseResult = false;
		final double[] azArray = { 365, 367, 1, 2 };
		final double[] elArray = { 1, 2, 3, 4 };
		final boolean clockwiseExpected = true;
		final int crossPoint = 1;
		fillListsSingle(azArray, elArray);
		clockwiseResult = GlobalFunctions.ifClockwise(coordinatesExpected, 1);
		assertTrue(GlobalFunctions.ifClockwise(coordinatesExpected, 1));
		coordinatesExpected.clear();

	}

	@Test
	public void listFinalCheck() {
		final double[] azArrayResult = { -123, 160.81, 160.73, -263, 160.52,
				159.96, 158.05, 98.6, 347.04, 344.9, 344.31, 344.1, 344.04,
				4845 };
		final double[] elArrayResult = { 25.04, 33.89, -365, 46.54, 64.87,
				87.84, 66.47, 47.6, 34.55, 25.45, 18.77, 13.59, 254, 360 };
		final double[] azArrayExpected = { 160.81, 160.52, 159.96, 158.05,
				98.6, 347.04, 344.9, 344.31, 344.1 };
		final double[] elArrayExpected = { 33.89, 64.87, 87.84, 66.47, 47.6,
				34.55, 25.45, 18.77, 13.59 };

		fillListsTwo(azArrayResult, elArrayResult, azArrayExpected,
				elArrayExpected);

		coordinatesResult = GlobalFunctions.listFinalCheck(coordinatesResult);
		for (int i = 0; i < coordinatesResult.size(); i++) {
			assertEquals(coordinatesResult.get(i).getAzimuth(),
					coordinatesExpected.get(i).getAzimuth());
			assertEquals(coordinatesResult.get(i).getElevation(),
					coordinatesExpected.get(i).getElevation());

		}

		coordinatesResult.clear();
		coordinatesExpected.clear();
	}
}
