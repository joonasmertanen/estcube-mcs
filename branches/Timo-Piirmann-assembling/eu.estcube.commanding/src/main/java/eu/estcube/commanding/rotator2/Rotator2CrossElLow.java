package eu.estcube.commanding.rotator2;

import java.util.List;

import org.hbird.exchange.navigation.PointingData;

import eu.estcube.commanding.api.OptimizeInterface;
import eu.estcube.commanding.calculation.Calculation;
import eu.estcube.commanding.optimizators.GlobalFunctions;
import eu.estcube.commanding.rotator1.Rotator1CrossElLow;

/**
 * When north line is crossed and elevation is low, class checks whether
 * satellite tracking can be supported with 360<azValue<450 range
 * If not, then it acts simillary to rator 1 type.
 * 
 * @author Ivar Mahhonin
 * 
 */
public class Rotator2CrossElLow implements OptimizeInterface {
	double receivingSector;
	double maxAz;
	double maxEl;

	public Rotator2CrossElLow(final double receivingSector, final double maxAz,
			final double maxEl) {
		super();
		this.receivingSector = receivingSector;
		this.maxAz = maxAz;
		this.maxEl = maxEl;
	}

	public List<PointingData> caseSuitable(final boolean suitable,
			final boolean clockwise, List<PointingData> coordinates,
			final int crossPoint) {

		if (suitable == true) {
			if (clockwise == true) {
				coordinates = crossElLowOptimizeClockwise(coordinates,
						crossPoint);
			}
			if (clockwise == false) {
				coordinates = crossElLowOptimizeAntiClockwise(coordinates,
						crossPoint);

			}
		}
		if (suitable == false) {
			final Rotator1CrossElLow caseToApply = new Rotator1CrossElLow(
					receivingSector, maxAz, maxEl);
			coordinates = caseToApply.optimize(coordinates);
			Calculation.log.info("APPLYING CASE AS FOR ROTATOR 1");

		}

		return coordinates;
	}

	/**
	 * If movement is anticlockwise and satellite can be tracked within <360
	 * range, then we are changing azimuth values before north line crosspoint
	 * to currentValue +360
	 * 
	 * @param coordinates - satellite trajectory
	 * @param crossPoint - point, where north line is crossed.
	 * @return
	 */
	public List<PointingData> crossElLowOptimizeAntiClockwise(
			final List<PointingData> coordinates, final int crossPoint) {
		double addValue;
		for (int i = 0; i < crossPoint; i++) {
			addValue = (coordinates.get(i).getAzimuth() + 360);
			coordinates.get(i).setAzimuth(addValue);
		}

		return coordinates;
	}

	/**
	 * If movement is clockwise and satellite can be tracked within <360
	 * range, then we are changing azimuth values after north line crosspoint
	 * to currentValue +360
	 * 
	 * @param coordinates
	 * @param crossPoint
	 * @return
	 */
	public List<PointingData> crossElLowOptimizeClockwise(
			final List<PointingData> coordinates, final int crossPoint) {
		double addValue;
		for (int i = crossPoint; i < coordinates.size(); i++) {
			addValue = (coordinates.get(i).getAzimuth() + 360);

			coordinates.get(i).setAzimuth(addValue);
		}

		return coordinates;
	}

	@Override
	public List<PointingData> optimize(List<PointingData> coordinates) {
		boolean suitable = false;
		boolean clockwise = false;
		int crossPoint = 0;
		crossPoint = GlobalFunctions.getCrossPoint(coordinates);
		clockwise = GlobalFunctions.ifClockwise(coordinates, crossPoint);
		suitable = withinMaxAzRange(clockwise, coordinates);
		coordinates = caseSuitable(suitable, clockwise, coordinates, crossPoint);

		return coordinates;
	}

	/**
	 * Checking if satellite can be supported using rotator 2 beneficial
	 * opportunities.
	 * 
	 * @param clockwise - whether satellite is moving clockwise
	 * @param coordinates - satellite trajectory.
	 * @return
	 */
	public boolean withinMaxAzRange(final boolean clockwise,
			final List<PointingData> coordinates) {
		boolean suitable = false;
		if (clockwise == true) {
			if (coordinates.get(coordinates.size() - 1).getAzimuth() < 90) {
				suitable = true;

			} else {
				suitable = false;
			}

		}
		if (clockwise == false) {
			if (coordinates.get(0).getAzimuth() < 90) {
				suitable = true;

			} else {
				suitable = false;
			}
		}

		return suitable;
	}

}