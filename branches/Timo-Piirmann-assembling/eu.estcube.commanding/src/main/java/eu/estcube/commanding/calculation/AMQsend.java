package eu.estcube.commanding.calculation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ScheduledMessage;
import org.hbird.business.groundstation.hamlib.HamlibNativeCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.common.Constants;

/**
 * Sending generated commands for rotator via ActiveMQ
 * 
 * @author Ivar Mahhonin
 * 
 */
class AMQsend {
    public static List<Double> azimuthList = new ArrayList<Double>();
    public static List<Double> elevationList = new ArrayList<Double>();
    /** Logging */
    private static final Logger LOG = LoggerFactory.getLogger(AMQsend.class);

    /** AMQ server address */
    private static String amqUrl = "tcp://localhost:61616";

    /** TOPIC name where commands will be received */
    public static String TOPIC = Constants.AMQ_GS_COMMANDS;

    /** Message header */
    public static String HEADER = Constants.HEADER_COMPONENT_ID;

    /** Header for radio messages */
    public static String HEADER_RADIO = Constants.CMDG_HEADER_RADIO;

    /** Header for rotator messages */
    public static String HEADER_ROTATOR = Constants.CMDG_HEADER_ROTATOR;

    /** Whether it is upling or downlin */
    public static boolean linkInit;

    /** List, where commands are stored */
    public static List<HamlibNativeCommand> commandsList = new ArrayList<HamlibNativeCommand>();

    /**
     * Completing List<NativeCommand> commandsList
     * 
     * @param TOPIC - topic, where list elements will be stored
     * @param destination - message destination
     * @param name - Native Command
     * @param command - command value
     * @param timeStamp - command timestamp
     * @throws Exception
     */
    public static void addCommand(final String destination, final String name,
            final String command, final long timeStamp) throws Exception {

        HamlibNativeCommand c = new HamlibNativeCommand(command, timeStamp, null, HamlibNativeCommand.STAGE_TRACKING);
        c.setDestination(destination);
        c.setIssuedBy("COMMANDING");
        c.setName(name);
        c.setTimestamp(timeStamp);
        commandsList.add(c);
        messageCounter = messageCounter + 1;
    }

    public static void endConnection() throws Exception {
        main(commandsList);

    }

    /** Azimuth parameter for command */
    double azimuth;

    /** Elevation parameter for command */
    double elevation;

    /** Elevation parameter for command */
    static int messageCounter = 0;

    /**
     * Sending commands list to ActiveMQ
     * 
     * @param commandsList
     * @throws Exception
     */

    public static void main(final List<HamlibNativeCommand> commandsList)
            throws Exception {

        final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
                amqUrl);
        final Connection connection = connectionFactory.createConnection();
        connection.start();
        final Session session = connection.createSession(false,
                Session.AUTO_ACKNOWLEDGE);
        final Destination destination = session.createQueue(TOPIC);
        final MessageProducer producer = session.createProducer(destination);
        for (int i = 0; i < commandsList.size(); i++) {
            final Date date = new Date();
            final long release = commandsList.get(i).getTimestamp()
                    - date.getTime();
            final ObjectMessage message = session
                    .createObjectMessage(commandsList.get(i));
            message.setStringProperty(HEADER, commandsList.get(i)
                    .getDestination());
            message.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_DELAY,
                    release);
            producer.send(message);

        }
        LOG.info("Message list sent to AMQ with size " + commandsList.size());
        commandsList.clear();

    }

    /**
     * Creating command for Rotator and adding it to List<NativeCommand>
     * commandsList
     * 
     * @param azimuth - azimuth
     * @param elevation -elevation
     * @param timeStamp - command timestamp
     * @throws Exception
     */
    public static void sendToRotator(final String azimuth,
            final String elevation, final long timeStamp) throws Exception {
        final String messageCommand = new String("P " + azimuth + " "
                + elevation);
        addCommand(HEADER_ROTATOR, "command", messageCommand, timeStamp);
        azimuthList.add(Double.parseDouble(azimuth));
        elevationList.add(Double.parseDouble(elevation));

    }

}