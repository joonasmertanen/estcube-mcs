package eu.estcube.webserverpublic.radiobeacon;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.camel.EndpointInject;
import org.apache.camel.Handler;
import org.apache.camel.ProducerTemplate;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.sun.mail.util.MailSSLSocketFactory;

import eu.estcube.codec.radiobeacon.RadioBeaconTranslator;

@SuppressWarnings("serial")
@Component
public class RadioBeaconSendEmailServlet extends HttpServlet {

    @EndpointInject(uri = "direct:radioBeaconInput")
    ProducerTemplate producer;

    @Value("${radiobeacon.to}")
    String toEmail;

    @Value("${radiobeacon.from}")
    String fromEmail;

    @Value("${radiobeacon.host}")
    String host;

    @Value("${radiobeacon.port}")
    String port;

    @Value("${radiobeacon.authentication.user}")
    String authUser;

    @Value("${radiobeacon.authentication.password}")
    String authPw;

    @Value("${radiobeacon.checkValue}")
    String checkValue;
    
    @Value("${radiobeacon.authentication.method}")
    String method;
    
    public static final String SMTP_HOST = "mail.smtp.host"; 
    public static final String SMTP_AUTH = "mail.smtp.auth"; 
    public static final String SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable"; 
    public static final String SMTP_PORT = "mail.smtp.port"; 
    public static final String SMTP_SSL_SOCKETFACTORY = "mail.smtp.ssl.socketFactory"; 
    
    public static final String USER = "mail.user"; 
    public static final String PASSWORD = "mail.password"; 
    
    public static final String SMTPS_HOST = "mail.smtps.host"; 
    public static final String SMTPS_AUTH = "mail.smtps.auth"; 
    public static final String SMTPS_STARTTLS_ENABLE = "mail.smtps.starttls.enable"; 
    public static final String SMTPS_PORT = "mail.smtps.port"; 
    public static final String SMTPS_SSL_SOCKETFACTORY = "mail.smtps.ssl.socketFactory";
    
    public static final String SSL = "SSL";
    public static final String TLS = "TLS";
    
    public static final String PROTOCOL_SMTP = "smtp";
    public static final String PROTOCOL_SMTPS = "smtps";

    class RadioBeacon {
        protected Set<String> radioBeaconMessage = new HashSet<String>();
    }

    @Override
    @Handler
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String datetime = request.getParameter("datetime").trim();
        String source = request.getParameter("source").trim();
        String data = request.getParameter("data").trim().toUpperCase();
        String check = request.getParameter("check").trim();

        JSONObject json = new JSONObject();

        try {
            RadioBeaconTranslator translator = new RadioBeaconTranslator();

            if (check.equals(checkValue) == false) {
                json.put("status", "error");
                json.put("message", "Invalid check value!");

            } else if (!(translator.checkBeaconMessage(translator.reformBeaconMessage(data)))) {
                json.put("status", "error");
                json.put("message", "Invalid beacon!");

            } else {
                try {
                    sendEmail("Beacon data: " + datetime + " " + source + "",
                            buildMessage(data, datetime, source));

                    json.put("status", "ok");
                    json.put("message", "Message sent!");

                } catch (Exception e) {
                    json.put("status", "error");
                    json.put("message", "Message could not be sent: " + e.getMessage());
                }
            }

        } catch (JSONException e1) {
            throw new ServletException(e1);
        }

        response.getWriter().write(json.toString());

    }

    public String buildMessage(String data, String datetime, String source) throws Exception {
        return data + ";" + source + ";" + datetime;
    }

    public Boolean sendEmail(String title, String content) throws AddressException, MessagingException, GeneralSecurityException {
        
    		Session session = getSession(getProperties());
    		
    		MimeMessage message = getMessage(session, title, content);
    		
    		Send(session, message);
    		
        return true;

    }
    

    private Properties getProperties() throws GeneralSecurityException{
    	
    	Properties props = new Properties();
    	
    	if(method.equalsIgnoreCase(TLS)){
    		
    		MailSSLSocketFactory sf = new MailSSLSocketFactory();
        	sf.setTrustAllHosts(true);
        	
    		props.put(SMTP_HOST, host);
    		props.put(SMTP_PORT, port);
    		props.put(SMTP_AUTH, "true");
    		props.put(SMTP_STARTTLS_ENABLE, "true");
            props.put(SMTP_SSL_SOCKETFACTORY, sf);
            
    	}else if(method.equalsIgnoreCase(SSL)){
    		
    		MailSSLSocketFactory sf = new MailSSLSocketFactory();
        	sf.setTrustAllHosts(true);
        	
        	props.put(SMTPS_HOST, host);
        	props.put(SMTPS_PORT, port);
        	props.put(SMTPS_AUTH, "true");
        	props.put(SMTPS_STARTTLS_ENABLE,"true");
        	props.put(SMTPS_SSL_SOCKETFACTORY, sf);
    		
    	}else{
    		
            props.put(SMTP_HOST, host);
            props.put(SMTP_PORT, port);
    		
    	}
    	
    	return props;
    }
    
    private Session getSession(Properties properties){
    	Session session = Session.getInstance(properties,
        		  new javax.mail.Authenticator() {
        			protected PasswordAuthentication getPasswordAuthentication() {
        				return new PasswordAuthentication(authUser, authPw);
        			}
        		  });
		return session;
    }
    
    private MimeMessage getMessage(Session session, String title, String content) throws AddressException, MessagingException{
    	
    	MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(fromEmail));
    	message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
        message.setSubject(title);
        message.setText(content);
        
		return message;
    	
    }
    
    private void Send(Session session, MimeMessage message) throws NumberFormatException, MessagingException{
    	
    	if(method.equalsIgnoreCase(SSL)||method.equalsIgnoreCase(TLS)){

        	Transport transport = method.equalsIgnoreCase(SSL) ? session.getTransport(PROTOCOL_SMTPS) : session.getTransport(PROTOCOL_SMTP);
        	transport.connect(host, Integer.parseInt(port), authUser, authPw);
        	transport.sendMessage(message, message.getAllRecipients());
        	transport.close();
        	
    	}else{
    		Transport.send(message);
    	}
    	
    }
}
