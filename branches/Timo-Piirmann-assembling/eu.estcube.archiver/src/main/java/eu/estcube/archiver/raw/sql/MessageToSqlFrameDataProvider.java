package eu.estcube.archiver.raw.sql;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Message;

import eu.estcube.domain.transport.Direction;

public abstract class MessageToSqlFrameDataProvider implements
		SqlFrameDataProvider {
	public static final String NULL = "<null>";

	public static final String HDR_RECEPTION_TIME = "timestamp";
	public static final String HDR_SATELLITE = "satellite";

	public static final String COL_RECEPTION_TIME = "reception_time";
	public static final String COL_SATELLITE = "satellite";

	private Message message;
	private Direction direction;

	public MessageToSqlFrameDataProvider(Direction direction) {
		this(direction, null);
	}

	public MessageToSqlFrameDataProvider(Direction direction, Message message) {
		this.message = message;
		this.direction = direction;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	@Override
	public Map<String, String> getDetails() {
		Message m = getMessage();

		Map<String, Object> h = m.getHeaders();
		Map<String, String> d = new HashMap<String, String>();

		for (Map.Entry<String, Object> e : h.entrySet()) {
			if (e.getValue() == null)
				d.put(e.getKey(), NULL);
			else
				d.put(e.getKey(), e.getValue().toString());
		}

		return d;
	}

}
