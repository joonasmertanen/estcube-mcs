/** 
 *
 */
package eu.estcube.calibration.processors;

import java.util.List;

import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.core.Parameter;

import eu.estcube.calibration.domain.CalibrationUnit;

/**
 *
 */
public interface ParameterCalibrator {

    public List<Parameter> calibrate(CalibrationUnit input, IdBuilder idBuilder) throws Exception;
}
