package eu.estcube.calibration.domain;

import java.util.ArrayList;
import java.util.List;

public class InfoContainer {

    private String id;
    private String script;
    private String unit;
    private List<String> additionalParameters;
    private String description;
    private String outputId;
    private boolean resultIsVector;
    private String scriptResultVariable;

    public InfoContainer(String id, String script, String unit, ArrayList<String> additionalParameters,
            String description, String outputId, boolean resultIsVector, String scriptResultVariable) {

        this.id = id;
        this.script = script;
        this.unit = unit;
        this.additionalParameters = additionalParameters;
        this.description = description;
        this.outputId = outputId.equals("") ? id : outputId;
        this.resultIsVector = resultIsVector;
        this.scriptResultVariable = scriptResultVariable;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOutputId() {
        return outputId;
    }

    public void setOutputId(String outputId) {
        this.outputId = outputId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public List<String> getAdditionalParameters() {
        return additionalParameters;
    }

    public void setAdditionalParameters(ArrayList<String> additionalParameters) {
        this.additionalParameters = additionalParameters;
    }

    public boolean isResultIsVector() {
        return resultIsVector;
    }

    public String getScriptResultVariable() {
        return scriptResultVariable;
    }

}
