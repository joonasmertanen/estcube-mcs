package eu.estcube.codec.gcp.struct.parameters;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilderFactory;

import org.hbird.exchange.core.Parameter;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

public class GcpParameterUint32Test {

    private final GcpParameterUint32 p = new GcpParameterUint32("a", "b", "c", false);
    private final GcpParameterUint32 a = new GcpParameterUint32("a", "b", "c", true);

    @Test
    public void GcpParameterBit() throws Exception {

        String xml = "<param><name>test</name><description>a</description><unit>b</unit></param>";
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                .parse(new InputSource(new StringReader(xml)));

        GcpParameterUint32 s = new GcpParameterUint32((Element) doc.getElementsByTagName("param").item(0));
        assertEquals("test", s.getName());
        assertEquals("a", s.getDescription());
        assertEquals("b", s.getUnit());
    }

    @Test
    public void getLength() {
        assertEquals(32, p.getLength());
    }

    @Test
    public void toValueStr() {
        assertEquals(4294967295L, (long) p.toValue("4294967295")); // max
        assertEquals(0, (long) p.toValue("0")); // min
        assertEquals(2147483648L, (long) p.toValue("2147483648")); // rnd

        assertEquals(2147483648L, (long) p.toValue("0x80000000")); // min
        assertEquals(2147483647L, (long) p.toValue("0x7FFFFFFF")); // min
        assertEquals(4294967295L, (long) p.toValue("0xFFFFFFFF")); // max
        try {
            p.toValue("4294967296"); // >max
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        try {
            p.toValue("-1"); // <min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }
    }

    @Test
    public void toBytes() {

        assertArrayEquals(new byte[] { 0, 0, 0, 0 }, p.toBytes(0)); // min
        assertArrayEquals(new byte[] { -1, -1, -1, -1 }, p.toBytes(4294967295L)); // max
        assertArrayEquals(new byte[] { -128, 0, 0, 0 }, p.toBytes(2147483648L)); // misc
        assertArrayEquals(new byte[] { 127, -1, -1, -1 }, p.toBytes(2147483647)); // misc
        assertArrayEquals(new byte[] { 0x20, 0x1B, 0x27, 0x01 }, p.toBytes(538650369)); // misc
        try {
            p.toBytes(-1); // <min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        try {
            p.toBytes(4294967296L); // >max
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        assertArrayEquals(new byte[] { 0, 0, 0, 0 }, a.toBytes(0)); // min
        assertArrayEquals(new byte[] { -1, -1, -1, -1 }, a.toBytes(4294967295L)); // max
        assertArrayEquals(new byte[] { 0, 0, 0, -128 }, a.toBytes(2147483648L)); // misc
        assertArrayEquals(new byte[] { -1, -1, -1, 127 }, a.toBytes(2147483647)); // misc

        try {
            a.toBytes(-1); // <min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        try {
            a.toBytes(4294967296L); // >max
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

    }

    @Test
    public void toValueByte() {

        assertEquals(4294967295L, (long) p.toValue(new byte[] { -1, -1, -1, -1 })); // max
        assertEquals(2147483648L, (long) p.toValue(new byte[] { -128, 0, 0, 0 })); // misc
        assertEquals(2147483647, (long) p.toValue(new byte[] { 127, -1, -1, -1 })); // misc
        assertEquals(0, (long) p.toValue(new byte[] { 0, 0, 0, 0 })); // 0
        assertEquals(538650369, (long) p.toValue(new byte[] { 0x20, 0x1B, 0x27, 0x01 })); // misc

        assertEquals(4294967295L, (long) a.toValue(new byte[] { -1, -1, -1, -1 })); // max
        assertEquals(2147483648L, (long) a.toValue(new byte[] { 0, 0, 0, -128 })); // misc
        assertEquals(2147483647, (long) a.toValue(new byte[] { -1, -1, -1, 127 })); // misc
        assertEquals(0, (long) a.toValue(new byte[] { 0, 0, 0, 0 })); // 0

        try {
            p.toValue(new byte[] { 0, 0, 0, 0, 0 }); // more bytes
            fail("Must throw RuntimeException");
        } catch (RuntimeException e) {
        }

        try {
            p.toValue(new byte[] { 0, 0, 0 }); // less bytes
            fail("Must throw RuntimeException");
        } catch (RuntimeException e) {
        }
    }

    @Test
    public void getIssued() {
        Parameter parameter = p.getIssued(new byte[] { -1, -1, -1, -1 });
        assertEquals("a", parameter.getName());
        assertNull(parameter.getID());
        assertEquals("b", parameter.getDescription());
        assertEquals(null, parameter.getIssuedBy());
        assertEquals(4294967295L, parameter.getValue());
        assertEquals("c", parameter.getUnit());
    }

    @Test
    public void getValueClass() {
        assertEquals(Long.class, p.getValueClass());
    }
}
