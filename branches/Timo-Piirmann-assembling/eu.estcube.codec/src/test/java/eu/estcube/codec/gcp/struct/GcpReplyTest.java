package eu.estcube.codec.gcp.struct;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import eu.estcube.codec.gcp.struct.parameters.GcpParameterInt16;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterString;

public class GcpReplyTest {
    @Test
    public void GcpReply() throws Exception {

        String xml = "<reply>" +
                "<name>AA</name>" +
                "<description>BB</description>" +
                "<id>123</id>" +
                "<subsystems>" +
                "<subsys>TEST</subsys>" +
                "</subsystems>" +
                "<parameters>" +
                "<param><name>a</name><description>b</description><unit>c</unit><type>string</type></param>" +
                "</parameters>" +
                "</reply>";

        GcpReply com = new GcpReply(parse(xml));

        assertEquals(123, com.getId());
        assertEquals("AA", com.getName());
        assertEquals("BB", com.getDescription());

        assertEquals(1, com.getSubsystems().size());
        assertEquals("TEST", com.getSubsystems().get(0).getName());

        assertEquals(1, com.getParameters().size());
        assertEquals("a", com.getParameters().get(0).getName());
        assertTrue(com.getParameters().get(0) instanceof GcpParameterString);
    }

    @Test
    public void taddParameter() {

        try {
            GcpReply com1 = new GcpReply(0, "", "");
            com1.addParameter(new GcpParameterString("", ""));
            com1.addParameter(new GcpParameterInt16("", "", "", false));
            fail("Must throw exception");
        } catch (Exception e) {
        }

    }

    private Element parse(String xml) throws Exception {
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                .parse(new InputSource(new StringReader(xml)));

        return (Element) doc.getElementsByTagName("reply").item(0);
    }
}
