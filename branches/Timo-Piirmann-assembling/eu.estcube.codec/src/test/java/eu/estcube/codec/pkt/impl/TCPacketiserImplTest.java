package eu.estcube.codec.pkt.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import eu.estcube.codec.SscProvider;
import eu.estcube.codec.TimestampProvider;
import eu.estcube.domain.transport.pkt.TCPacket;
import eu.estcube.domain.transport.pkt.TCPacketSecondaryFrame;

public class TCPacketiserImplTest {
	public static final int FRAMESIZE = 22; // Primary header is 20 bytes, 2
											// left for body
	public static final int PHEADER = 20;
	public static final int SHEADER = 5;

	private TCPacketiserImpl packetiser;
	private SscProvider sscProvider;
	private TimestampProvider timestampProvider;

	@Before
	public void setUp() throws Exception {
		sscProvider = Mockito.mock(SscProvider.class);
		timestampProvider = Mockito.mock(TimestampProvider.class);
		packetiser = new TCPacketiserImpl(sscProvider, timestampProvider, FRAMESIZE);
	}

	@Test
	public void testAx25FramedTCPktEncoder() {
		assertTrue(sscProvider == packetiser.getSscProvider());
		assertTrue(timestampProvider == packetiser.getTimestampProvider());
		assertEquals(FRAMESIZE, packetiser.getFrameSize());
	}

	@Test
	public void testCreateEqualToPrimary() {
		int spid = 511; // 0000000111111111
		long et = 1095233372415L; // 1111111100000000111111110000000011111111
		long ts = 4294967295L;
		int ssc = 65535;

		byte[] payload;
		TCPacket packet;

		Mockito.when(sscProvider.getNext(Mockito.anyInt())).thenReturn(ssc);
		Mockito.when(timestampProvider.getTimestamp()).thenReturn(ts);

		// Payload is 2 bytes - fits into primary frame exactly
		payload = new byte[] { (byte) 233, (byte) 255 };
		packet = packetiser.create(spid, et, payload);

		// Check structure
		assertNotNull(packet.getPrimaryFrame());
		assertNotNull(packet.getPrimaryFrame().getHeader());
		assertNotNull(packet.getPrimaryFrame().getBody());
		assertTrue(packet.getSecondaryFrames().isEmpty());

		// Check header
		assertEquals(ssc, packet.getPrimaryFrame().getHeader().getSsc());
		assertEquals(1, packet.getPrimaryFrame().getHeader().getLen());
		assertEquals(spid, packet.getPrimaryFrame().getHeader().getSpid());
		assertEquals(ts, packet.getPrimaryFrame().getHeader().getTs());
		// TODO: Check MAC once implemented
		assertEquals(et, packet.getPrimaryFrame().getHeader().getEt());

		// Check payload
		assertTrue(Arrays.equals(payload, packet.getPrimaryFrame().getBody()));
	}

	@Test
	public void testCreateTwoPlusFrames() {
		int spid = 511; // 0000000111111111
		long et = 1095233372415L; // 1111111100000000111111110000000011111111
		long ts = 4294967295L;
		int ssc = 65535;

		byte[] payload;
		TCPacket packet;

		Mockito.when(sscProvider.getNext(Mockito.anyInt())).thenReturn(ssc);
		Mockito.when(timestampProvider.getTimestamp()).thenReturn(ts);

		// Payload is 20 bytes - fits into primary frame, one secondary and 1
		// byte to another secondary
		// frame
		payload = new byte[] { (byte) 233, (byte) 255, (byte) 233, (byte) 255, (byte) 233, (byte) 255, (byte) 233,
				(byte) 255, (byte) 233, (byte) 255, (byte) 233, (byte) 255, (byte) 233, (byte) 255, (byte) 233,
				(byte) 255, (byte) 233, (byte) 255, (byte) 233, (byte) 255 };

		packet = packetiser.create(spid, et, payload);

		// Check structure
		assertNotNull(packet.getPrimaryFrame());
		assertNotNull(packet.getPrimaryFrame().getHeader());
		assertNotNull(packet.getPrimaryFrame().getBody());
		assertEquals(2, packet.getSecondaryFrames().size());

		// Check primary header
		assertEquals(ssc, packet.getPrimaryFrame().getHeader().getSsc());
		assertEquals(3, packet.getPrimaryFrame().getHeader().getLen());
		assertEquals(spid, packet.getPrimaryFrame().getHeader().getSpid());
		assertEquals(ts, packet.getPrimaryFrame().getHeader().getTs());
		// TODO: Check MAC once implemented
		assertEquals(et, packet.getPrimaryFrame().getHeader().getEt());

		// Check primary payload
		assertTrue(Arrays.equals(ArrayUtils.subarray(payload, 0, 2), packet.getPrimaryFrame().getBody()));

		// Check secondary frames
		TCPacketSecondaryFrame secondaryFrame;

		secondaryFrame = packet.getSecondaryFrames().get(0);
		assertEquals(ssc + 1, secondaryFrame.getHeader().getSsc());
		assertEquals(3, secondaryFrame.getHeader().getLen());
		assertTrue(Arrays.equals(ArrayUtils.subarray(payload, 2, 19), secondaryFrame.getBody()));

		secondaryFrame = packet.getSecondaryFrames().get(1);
		assertEquals(ssc + 2, secondaryFrame.getHeader().getSsc());
		assertEquals(3, secondaryFrame.getHeader().getLen());
		assertTrue(Arrays.equals(ArrayUtils.subarray(payload, 19, 20), secondaryFrame.getBody()));

	}

	@Test
	public void testEncodeTwoFrames() {
		int spid = 511; // 0000000111111111
		long et = 1095233372415L; // 1111111100000000111111110000000011111111
		long ts = 4294967295L;
		int ssc = 65535;

		byte[] payload;
		TCPacket packet;

		Mockito.when(sscProvider.getNext(Mockito.anyInt())).thenReturn(ssc);
		Mockito.when(timestampProvider.getTimestamp()).thenReturn(ts);

		// Payload is 19 bytes - fits into primary frame and one secondary
		// frame
		payload = new byte[] { (byte) 233, (byte) 255, (byte) 233, (byte) 255, (byte) 233, (byte) 255, (byte) 233,
				(byte) 255, (byte) 233, (byte) 255, (byte) 233, (byte) 255, (byte) 233, (byte) 255, (byte) 233,
				(byte) 255, (byte) 233, (byte) 255, (byte) 233 };

		packet = packetiser.create(spid, et, payload);

		// Check structure
		assertNotNull(packet.getPrimaryFrame());
		assertNotNull(packet.getPrimaryFrame().getHeader());
		assertNotNull(packet.getPrimaryFrame().getBody());
		assertEquals(1, packet.getSecondaryFrames().size());

		// Check primary header
		assertEquals(ssc, packet.getPrimaryFrame().getHeader().getSsc());
		assertEquals(2, packet.getPrimaryFrame().getHeader().getLen());
		assertEquals(spid, packet.getPrimaryFrame().getHeader().getSpid());
		assertEquals(ts, packet.getPrimaryFrame().getHeader().getTs());
		// TODO: Check MAC once implemented
		assertEquals(et, packet.getPrimaryFrame().getHeader().getEt());

		// Check primary payload
		assertTrue(Arrays.equals(ArrayUtils.subarray(payload, 0, 2), packet.getPrimaryFrame().getBody()));

		// Check secondary frame
		TCPacketSecondaryFrame secondaryFrame;

		secondaryFrame = packet.getSecondaryFrames().get(0);
		assertEquals(ssc + 1, secondaryFrame.getHeader().getSsc());
		assertEquals(2, secondaryFrame.getHeader().getLen());
		assertTrue(Arrays.equals(ArrayUtils.subarray(payload, 2, 19), secondaryFrame.getBody()));
	}

	@Test
	public void testEncodeLessThanPrimary() {
		int spid = 511; // 0000000111111111
		long et = 1095233372415L; // 1111111100000000111111110000000011111111
		long ts = 4294967295L;
		int ssc = 65535;

		byte[] payload;
		TCPacket packet;

		Mockito.when(sscProvider.getNext(Mockito.anyInt())).thenReturn(ssc);
		Mockito.when(timestampProvider.getTimestamp()).thenReturn(ts);

		// 1. Payload is 1 byte - less than fits into primary frame
		payload = new byte[] { (byte) 233 };
		packet = packetiser.create(spid, et, payload);

		// Check structure
		assertNotNull(packet.getPrimaryFrame());
		assertNotNull(packet.getPrimaryFrame().getHeader());
		assertNotNull(packet.getPrimaryFrame().getBody());
		assertTrue(packet.getSecondaryFrames().isEmpty());

		// Check header
		assertEquals(ssc, packet.getPrimaryFrame().getHeader().getSsc());
		assertEquals(1, packet.getPrimaryFrame().getHeader().getLen());
		assertEquals(spid, packet.getPrimaryFrame().getHeader().getSpid());
		assertEquals(ts, packet.getPrimaryFrame().getHeader().getTs());
		// TODO: Check MAC once implemented
		assertEquals(et, packet.getPrimaryFrame().getHeader().getEt());

		// Check payload
		assertTrue(Arrays.equals(payload, packet.getPrimaryFrame().getBody()));
	}

	@Test
	public void testGetPrimaryHeaderSize() {
		assertEquals(20, packetiser.getPrimaryHeaderSize());
	}

	@Test
	public void testGetSecondarHeaderNumBytes() {
		assertEquals(5, packetiser.getSecondarHeaderNumBytes());
	}

	@Test
	public void testCalculateFrameCount() {
		assertEquals(1, packetiser.calculateFrameCount(3, 2, 1, 1));
		assertEquals(2, packetiser.calculateFrameCount(3, 2, 1, 2));
		assertEquals(2, packetiser.calculateFrameCount(3, 2, 1, 3));
		assertEquals(3, packetiser.calculateFrameCount(3, 2, 1, 4));
		assertEquals(3, packetiser.calculateFrameCount(3, 2, 1, 5));

		// No room in primary header
		try {
			packetiser.calculateFrameCount(3, 3, 1, 5);
		} catch (Throwable t) {
			assertEquals(IllegalArgumentException.class, t.getClass());
		}

		// No room in secondary header
		try {
			packetiser.calculateFrameCount(3, 2, 3, 5);
		} catch (Throwable t) {
			assertEquals(IllegalArgumentException.class, t.getClass());
		}

	}

}
