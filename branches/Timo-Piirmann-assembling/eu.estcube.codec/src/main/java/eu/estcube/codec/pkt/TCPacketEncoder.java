package eu.estcube.codec.pkt;

import eu.estcube.domain.transport.pkt.TCPacket;

/**
 * Encodes the given TC packet.
 */
public interface TCPacketEncoder {

	/**
	 * Encodes the given packet as byte array.
	 * 
	 * @param packet
	 *            The packet to encode.
	 * @return The data of the TC packet as byte array.
	 */
	public byte[] encode(TCPacket packet);
}
