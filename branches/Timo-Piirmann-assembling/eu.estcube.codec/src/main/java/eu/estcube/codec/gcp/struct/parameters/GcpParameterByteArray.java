package eu.estcube.codec.gcp.struct.parameters;

import org.hbird.exchange.core.Binary;
import org.w3c.dom.Element;

import eu.estcube.codec.gcp.struct.GcpParameter;
import eu.estcube.common.ByteUtil;

public class GcpParameterByteArray extends GcpParameter implements GcpParameterMaxLength {

    public GcpParameterByteArray(String name, String description) {
        super(name, description, "");
    }

    public GcpParameterByteArray(Element e) {
        super(e);
    }

    @Override
    public int getLength() {
        return -1;
    }

    @Override
    public byte[] toValue(String input) {
        return ByteUtil.toBytesFromHexString(input);
    }

    @Override
    public byte[] toBytes(Object val) {
        return (byte[]) val;
    }

    @Override
    public Binary getIssued(byte[] value) {
        Binary param = new Binary(null, getName());
        param.setDescription(getDescription());
        param.setRawData(toValue(value));
        return param;
    }

    @Override
    public byte[] toValue(byte[] bytes) {
        return bytes;
    }

    @Override
    public Class<?> getValueClass() {
        return byte[].class;
    }

}
