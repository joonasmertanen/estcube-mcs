define([
    "dojo/_base/declare", 
    "dojo/_base/lang", 
    "dojo/_base/array", 
    "dojo/aspect", 
    "dojo/dom-class",
    "dojo/on",
    "dgrid/OnDemandGrid", 
    "dgrid/util/misc", 
    "./ContentProvider", 
    "common/store/ParameterStore", 
    "common/formatter/DateFormatter", 
    ], 
    function(declare, Lang, Arrays, Aspect, DomClass, on, Grid, Misc, ContentProvider, ParameterStore, DateFormatter) {
        return declare(ContentProvider, {        
            grid: null,
            store: ParameterStore,        
            getContent: function (args) {
                declare.safeMixin(this, args);
                
                this.grid = new Grid({
                    class: this.domClasses,
                    columns: this.columns || {
                        name: {
                            label: "Name",
                            className: "field-short-name",
                        },
                        value: {
                            label: "Value",
                        },
                        unit: {
                            label: "Unit",
                        },
                        timestamp: { 
                            label: "Time", formatter: DateFormatter
                             },
                        applicableTo: { 
                            label: "Source",
                            renderCell: function (object, value, node) {
                                var elements = object.applicableTo.split("/");
                                var id  = elements[elements.length-1];
                                node.innerHTML = id;
                            },

                             },
                    },
                    store: this.store,
                    query: this.query ||

                    function (message) {
                        var sources =[
                            "/ESTCUBE/WeatherStations/meteo.physic.ut.ee",
                            "/ESTCUBE/WeatherStations/emhi.ee", 
                            "/ESTCUBE/WeatherStations/ilm.ee"];
                        return sources.indexOf(message.applicableTo)!=-1;
                    }
                });
                return this.grid;
            },

        });
    });
    



