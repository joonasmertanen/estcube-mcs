define([
        "dojo/_base/declare", 
        "dojo/dom-construct", 
        "dojo/dom-attr", 
        "dijit/layout/ContentPane", 
        "./ContentProvider", 
        "dojo/_base/declare", 
        "dojo/dom-class", 
        "dojo/dom-construct", 
        "dojo/on", 
        "dojo/_base/lang", 
        "dojo/request", 
        "dojox/gfx",
        "dojox/gfx/fx",
        'dojo/on',
        "dojo/dom"
        
        ], 
        
        function(declare, DomConstruct, DomAttr, ContentPane, ContentProvider, declare, DomClass, DomConstruct,On, Lang, Request, gfx, fx, on,dom) {   
            return declare(ContentProvider, {getContent: function () {
                this.radarPane = new ContentPane({
                    id: "genCircleRadarView",
                    region: "center",
                    style: {
                        width: "250px",
                        height: "193px",
                        margin: "0px",
                        padding: "0px"
                    }
                });

                this.trajectory;
                this.groundStation;
                this.satellite;
                this.orbit;
                
                this.surfaceX = 230;
                this.surfaceY = 190;
                this.surface = dojox.gfx.createSurface(this.radarPane.domNode, this.surfaceX, this.surfaceY);
                this.circleX = 115;
                this.circleY = 95;
                this.circleR = 75;
                this.trackingGenerated = [];
                this.currentTracker = [];
                this.drawRadarView();
                return this.radarPane;
            },

            getSatelliteTrajectoryById: function (instanceId, orbitNumber) {
                console.log("[radarViewContentProvider]: asking server for the trajectory.")
                var startTime = 0;
                var endTime = 0;
                var trajectoryAdopted = [];

                Request.get("/getTrajectory", {
                    query: {
                        instanceId: instanceId,
                    },
                    handleAs: "json"
                }).then(Lang.hitch(this, function (response) {
                    this.trajectory = response;
                    console.log("[radarViewContentProvider]: received trajectory from the server")
                    startTime = this.getTimeLabel(this.trajectory[0].timestamp);
                    startTime = startTime.hours + ":" + startTime.minutes + ":" + startTime.seconds;
                    endTime = this.getTimeLabel(this.trajectory[this.trajectory.length - 1].timestamp);
                    endTime = endTime.hours + ":" + endTime.minutes + ":" + endTime.seconds;
                    trajectoryAdopted = this.getCoordinates(this.trajectory);
                    
                    this.orbit = orbitNumber;

                    this.emptyRadarView();

                    var gsLabel = this.surface.createText({
                        x: 0,
                        y: 10,
                        text: this.groundStation,
                    }).setFont({
                        size: this.fontSizeGroud - 5,
                        family:"Arial"
                    }).setFill("black");

                    gsLabel.rawNode.id = "gsLabel";
                    
                    var satLabel = this.surface.createText({
                        x: this.surfaceX-62,
                        y: 10,
                        text: this.satellite,
                    }).setFont({
                        size: this.fontSizeGroud - 5,
                        family:"Arial"
                    }).setFill("black");

                    satLabel.rawNode.id = "satLabel";
                    
                    var orbitLabel = this.surface.createText({
                    	 x: this.surfaceX-65,
                         y: this.surfaceY,
                        text: "orbit: "+this.orbit,
                    }).setFont({
                        size: this.fontSizeGroud - 5,
                        family:"Arial"
                    }).setFill("black");

                    orbitLabel.rawNode.id = "orbitLabel";

                    var azLabel = this.surface.createText({
                        x: 0,
                        y: this.surfaceY - 20,
                        text: "",
                    }).setFont({
                        size: this.fontSizeGroud - 5,
                        family:"Arial"
                    }).setFill("black");
                    azLabel.rawNode.id = "azLabel";

                    var middleX = trajectoryAdopted[Math.round(trajectoryAdopted.length / 2)].x;
                    var startX = trajectoryAdopted[0].x;
                    var endX = trajectoryAdopted[0].y;
                    if (middleX < this.circleX) {
                        kof = 40 / 5;
                    }
                    else {
                        kof = 50 * (-1);
                    }

                    var startTimeLabel = this.surface.createText({
                        x: startX + kof,
                        y: endX,
                        text: startTime,
                    }).setFont({
                        size: this.fontSizeGroud,
                        family:"Arial"
                    }).setFill("black");

                    startTimeLabel.rawNode.id = "startTimeLabel";
                    
                    on(startTimeLabel, "mousemove", Lang.hitch(this, function (e) {
                    	var insideRadarCircle = this.insideRadarCircle(startTimeLabel);
                    	if(insideRadarCircle){
	                        this.getMainRadarCircleLogics(true, e);
	                        on(startTimeLabel, "mouseout", Lang.hitch(this, function (e) {
	                            this.getMainRadarCircleLogics(false, e);
	                        }));
                    	}
                    }));

                    var endTimeLabel = this.surface.createText({
                        x: trajectoryAdopted[trajectoryAdopted.length - 1].x + kof,
                        y: trajectoryAdopted[trajectoryAdopted.length - 1].y,
                        text: endTime,
                    }).setFont({
                        size: this.fontSizeGroud,
                        family:"Arial"
                    }).setFill("black");
                    
                    on(endTimeLabel, "mousemove", Lang.hitch(this, function (e) {
                    	var insideRadarCircle = this.insideRadarCircle(endTimeLabel);
                    	if(insideRadarCircle){
	                        this.getMainRadarCircleLogics(true, e);
	                        on(endTimeLabel, "mouseout", Lang.hitch(this, function (e) {
	                            this.getMainRadarCircleLogics(false, e);
	                        }));
                    	}
                    }));

                    endTimeLabel.rawNode.id = "endTimeLabel";

                    var elLabel = this.surface.createText({
                        x: 0,
                        y: this.surfaceY - 5,
                        text: "",
                    }).setFont({
                        size: this.fontSizeGroud - 5,
                        family:"Arial"
                    }).setFill("black");

                    elLabel.rawNode.id = "elLabel";

                    trajectoryLine = this.surface.createPolyline(trajectoryAdopted).setStroke({
                        color: "blue",
                        width: 2
                    });
                    trajectoryLine.rawNode.id = "trajectoryLineDrawing";
                    on(trajectoryLine, "mousemove", Lang.hitch(this, function (e) {
                        this.getMainRadarCircleLogics(true, e);
                        on(trajectoryLine, "mouseout", Lang.hitch(this, function (e) {
                            this.getMainRadarCircleLogics(false, e);
                        }));
                    }));
                    this.showSatellitePosition(this.trajectory, instanceId);
                    getElementsByTagName('')

                }));

            },

            getTimeLabel: function (timestamp) {
                function ifSingle(time) {
                    if (time.toString().length == 1) {
                        time = "0" + time;
                    }
                    return time;
                }
                var date = new Date(timestamp);
                var hours = ifSingle(date.getHours());
                var minutes = ifSingle(date.getMinutes());
                var seconds = ifSingle(date.getSeconds());

                return {
                    "hours": hours,
                    "minutes": minutes,
                    "seconds": seconds
                };

            },

            showSatellitePosition: function (trajectory, instanceId) {
                var oldSpot = document.getElementById("satelliteSpot");
                if (oldSpot != null) {
                    oldSpot.parentNode.removeChild(oldSpot);
                }

                this.currentTracker = instanceId;
                if (this.trackingGenerated.indexOf(this.currentTracker) == -1) {
                    this.trackingGenerated.push(instanceId);
                    var trajeсtoryLength = trajectory.length;
                    var i = 0;
                    (Lang.hitch(this, function () {
                        if (instanceId == this.currentTracker) {
                            var oldSpot = document.getElementById("satelliteSpot");
                            if (oldSpot != null) {
                                oldSpot.parentNode.removeChild(oldSpot);
                            }
                            if (i < trajeсtoryLength) {
                                var d = new Date();
                                var when = new Date(trajectory[i].timestamp);
                                var when = when - d;
                                if (when < 0) {
                                    when = 10;
                                }
                                console.log("[RadarViewContentProvider] " + i + " next satellite position update in " + Math.round(when / 1000) + " seconds");
                                var azimuth = trajectory[i].azimuth;
                                var elevation = trajectory[i].elevation;
                                var rect = this.tranformPolarToRectangCoordinates(azimuth, elevation);
                                if (i != 0) {
                                    var circle = this.surface.createCircle({
                                        cx: rect.sphericX,
                                        cy: rect.sphericY,
                                        r: 5
                                    }).setFill("red").setStroke({
                                        color: "blue",
                                        width: 1
                                    });
                                    circle.rawNode.id = "satelliteSpot";
                                }
                                i++;
                                setTimeout(Lang.hitch(this, arguments.callee), when);
                            }
                        }
                    }))();
                }
            },
            
            insideRadarCircle: function(object){
            	var x = parseInt(object.rawNode.attributes.x.value);
            	var y = parseInt(object.rawNode.attributes.y.value);
            	
            	var distance = Math.sqrt((x-this.circleX)*(x-this.circleX)+(y-this.circleY)*(y-this.circleY));
            	
            	
            	console.log(this.circleR, distance);
            	if(distance>this.circleR){
            		return false;
            	}
            	else{
            		return true;
            	}
            	
      
            	
            	
            },

            getCoordinates: function (trajectory) {
                var adoptedTrajectory = [];
                var azimuth = 0;
                var elevation = 0;

               
                var gsRawName = trajectory[0].groundStationName;
                var gsIdParts = gsRawName.split("/");
                
             

                var satRawName = trajectory[0].satelliteName;
                var satIdParts = satRawName.split("/");

                
                this.groundStation = gsIdParts[gsIdParts.length - 1];
                this.satellite = satIdParts[satIdParts.length - 1];
         

                for (var i = 0; i < trajectory.length; i++) {
                    azimuth = trajectory[i].azimuth;
                    elevation = trajectory[i].elevation;

                    var rectCoordinates = this.tranformPolarToRectangCoordinates(azimuth, elevation);

                    adoptedTrajectory.push({
                        x: rectCoordinates.sphericX,
                        y: rectCoordinates.sphericY
                    });
                }

                return adoptedTrajectory;

            },

            tranformPolarToRectangCoordinates: function (azimuth, elevation) {

                var degrX = Math.cos(this.toRadians(azimuth - 90));
                var degrY = Math.sin(this.toRadians(azimuth - 90));
                var x = (this.circleR * degrX + this.circleX);
                var y = (this.circleR * degrY + this.circleY);
                var deltaX = this.circleX - x;
                var kofX = deltaX / 90;
                var deltaY = this.circleY - y;
                var kofY = deltaY / 90;
                var actX = x + elevation * kofX;
                var sphericX = actX;
                var actY = y + (elevation * kofY);
                var sphericY = actY;

                return {
                    "sphericX": sphericX,
                    "sphericY": sphericY
                };

            },

            updateAzElLabel: function (x, y) {
                var azLabel = dom.byId("azLabel");
                var elLabel = dom.byId("elLabel");
                var x0 = this.circleX;
                var y0 = this.circleY;
                y = y0 - y;
                x = x - x0;

                var R = Math.sqrt(y * y + x * x);

                var tanT = x / y;
                var azimuth = Math.round(this.toDegrees(Math.atan(tanT)));

                if (x < 0 && y > 0) {
                    azimuth += 360
                }
                else if (x > 0 && y < 0) {
                    azimuth += 180

                }

                else if (x < 0 && y < 0) {
                    azimuth += 180;
                }

                var elevation = (Math.round(R * 90 / this.circleR) - 90) * (-1);

                if (azimuth == "NaN") {
                    azimuth = 0;
                }

                else if (elevation < 0) {
                    elevation = elevation * (-1);
                }

                if (azLabel.textContent != null) {
                    azLabel.textContent = "az: " + azimuth + "°";
                    elLabel.textContent = "el: " + elevation + "°";
                }

            },

            emptyAzElLabels: function () {
                var azLabel = dom.byId("azLabel");
                var elLabel = dom.byId("elLabel");
                azLabel.textContent = "";
                elLabel.textContent = "";
            },

            getMainRadarCircleLogics: function (update, e) {
                if (update) {
                    var element = document.getElementById("genCircleRadarView");
                    var clientX = e.clientX - parseInt(element.x);
                    var clientY = e.clientY - parseInt(element.y);
                    this.updateAzElLabel(clientX, clientY);
                }
                else {
                    this.emptyAzElLabels();
                }
            },

            drawRadarView: function () {

                var firstCircle = this.surface.createCircle({
                    cx: this.circleX,
                    cy: this.circleY,
                    r: this.circleR
                }).setFill("white").setStroke({
                    color: "#B8BCC1",
                    width: 1.5
                });

                on(firstCircle, "mousemove", Lang.hitch(this, function (e) {
                    this.getMainRadarCircleLogics(true, e);
                    on(firstCircle, "mouseout", Lang.hitch(this, function (e) {
                        this.getMainRadarCircleLogics(false, e);
                    }));
                }));

                var secondCircle = this.surface.createCircle({
                    cx: this.circleX,
                    cy: this.circleY,
                    r: this.circleR - 26.6
                }).setFill("white").setStroke({
                    color: "#B8BCC1",
                    width: 1.5
                });

                on(secondCircle, "mousemove", Lang.hitch(this, function (e) {
                    this.getMainRadarCircleLogics(true, e);
                    on(secondCircle, "mouseout", Lang.hitch(this, function (e) {
                        this.getMainRadarCircleLogics(false, e);
                    }));
                }));

                var thirdCircle = this.surface.createCircle({
                    cx: this.circleX,
                    cy: this.circleY,
                    r: this.circleR - 53.2
                }).setFill("white").setStroke({
                    color: "#B8BCC1",
                    width: 1.5
                });

                on(thirdCircle, "mousemove", Lang.hitch(this, function (e) {
                    this.getMainRadarCircleLogics(true, e);
                    on(thirdCircle, "mouseout", Lang.hitch(this, function (e) {
                        this.getMainRadarCircleLogics(false, e);
                    }));
                }));

                var horizontalLine = this.surface.createLine({
                    x1: this.circleX - this.circleR,
                    y1: this.circleY,
                    x2: this.circleX + this.circleR,
                    y2: this.circleY
                }).setStroke({
                    color: "#B8BCC1",
                    width: 1.5
                });

                on(horizontalLine, "mousemove", Lang.hitch(this, function (e) {
                    this.getMainRadarCircleLogics(true, e);
                    on(horizontalLine, "mouseout", Lang.hitch(this, function (e) {
                        this.getMainRadarCircleLogics(false, e);
                    }));
                }));

                var verticalLine = this.surface.createLine({
                    x1: this.circleX,
                    y1: this.circleY - this.circleR,
                    x2: this.circleX,
                    y2: this.circleY + this.circleR,
                }).setStroke({
                    color: "#B8BCC1",
                    width: 1.5
                });

                on(verticalLine, "mousemove", Lang.hitch(this, function (e) {
                    this.getMainRadarCircleLogics(true, e);
                    on(verticalLine, "mouseout", Lang.hitch(this, function (e) {
                        this.getMainRadarCircleLogics(false, e);
                    }));
                }));

                var north = this.surface.createText({
                    x: this.circleX - 5,
                    y: this.circleY - this.circleR - 3,
                    text: "N",
                }).setFont({
                    size: this.fontSizeGroud,
                    family:"Arial"
                }).setFill("black");

                var east = this.surface.createText({
                    x: this.circleX + this.circleR + 5,
                    y: this.circleY + 5,
                    text: "E",
                }).setFont({
                    size: this.fontSizeGroud,
                    family:"Arial"
                }).setFill("black");

                var west = this.surface.createText({
                    x: this.circleX - this.circleR - 15,
                    y: this.circleY + 5,
                    text: "W",
                }).setFont({
                    size: this.fontSizeGroud,
                    family:"Arial"
                }).setFill("black");

                var south = this.surface.createText({
                    x: this.circleX - 3,
                    y: this.circleY + this.circleR + 13,
                    text: "S",
                }).setFont({
                    size: this.fontSizeGroud,
                    family:"Arial"
                }).setFill("black");

            },

            toRadians: function (degrees) {
                return degrees * Math.PI / 180;
            },

            toDegrees: function (radians) {
                return radians * 180 / Math.PI;
            },
            
            emptyRadarView: function(){
            	 var gsLabel = document.getElementById("gsLabel");
                 var satLabel = document.getElementById("satLabel");
                 var orbitLabel = document.getElementById("orbitLabel");
                 var oldTrajectory = document.getElementById("trajectoryLineDrawing");
                 var oldStartTime = document.getElementById("startTimeLabel");
                 var oldEndTime = document.getElementById("endTimeLabel");
                 if (gsLabel != null) {
                     gsLabel.parentNode.removeChild(gsLabel);
                     satLabel.parentNode.removeChild(satLabel);
                     orbitLabel.parentNode.removeChild(orbitLabel);
                     oldTrajectory.parentNode.removeChild(oldTrajectory);
                     oldStartTime.parentNode.removeChild(oldStartTime);
                     oldEndTime.parentNode.removeChild(oldEndTime);
                 }
            	
            }
            

            });
            });