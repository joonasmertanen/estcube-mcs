#!/bin/bash

if [[ $# < 1 ]]; then
    printf "Missing config file. Usage: %s file\n" "$0"
    exit -1
else
    file=$1

    # add new line to the end of the file in case it's missing
    sed -i -e '$a\' $file

    index=0
    while read line; do
        services[$index]="$line"
        index=$(($index+1))
    done < $file
fi

dir=$( cd "$( dirname "$0" )" && pwd )
workingDir=$(pwd)

# iterate over the list of services to stop them
# modify IFS to skip spaces in for loop
oldIFS=$IFS
IFS=$(echo -en "\n\b")

for service in ${services[@]}
do
    [ -z "$service" ] && continue # skip empty lines
    [ ! -d "$service" ] && continue # skip non directories
    [[ "$service" = "bin" ]] && continue # skip bin/
    [[ "$service" = "conf" ]] && continue # skip conf/
    [[ "$service" == \#.* ]] && continue # skip comments
    chmod +x $service/bin/run # set execution rights
    printf "%s\n" "$($dir/stop $service)"
done
# restore IFS
IFS=$oldIFS


# unzip
zipList=$(ls *.zip | grep -v eu.estcube.deploy) # XXX : exclude eu.estcube.deploy - already unzipped
for z in $zipList; do
    unzip -o -q $z;
done

# iterate over the list of services to start them 
# modify IFS to skip spaces in for loop
oldIFS=$IFS
IFS=$(echo -en "\n\b")

for service in ${services[@]}
do
    [ -z "$service" ] && continue # skip empty lines
    [ ! -d "$service" ] && continue # skip non directories
    [[ "$service" = "bin" ]] && continue # skip bin/
    [[ "$service" = "conf" ]] && continue # skip conf/
    [[ "$service" == \#.* ]] && continue # skip comments
    chmod +x $service/bin/run # set execution rights
    printf "%s\n" "$($dir/start $service)"
done
# restore IFS
IFS=$oldIFS

exit 0
