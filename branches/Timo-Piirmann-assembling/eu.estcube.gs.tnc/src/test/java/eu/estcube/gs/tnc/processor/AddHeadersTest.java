/** 
 *
 */
package eu.estcube.gs.tnc.processor;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.hbird.exchange.constants.StandardArguments;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.camel.serial.SerialConsumer;
import eu.estcube.common.Headers;
import eu.estcube.common.TimestampExtractor;
import eu.estcube.domain.transport.Downlink;
import eu.estcube.gs.tnc.Tnc;
import eu.estcube.gs.tnc.domain.TncDriverConfiguration;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class AddHeadersTest {

    private static final String SERVICE = "service";
    private static final String SATELLITE = "satellite";
    private static final String GROUND_STATION = "groundSation";
    private static final Long NOW = System.currentTimeMillis();
    private static final String SERIAL_PORT_NAME = "/dev/ttyS1";
    private static final String EVENT_ID = "GS:SAT:1234";

    @Mock
    private TncDriverConfiguration config;

    @Mock
    private Exchange exchange;

    @Mock
    private Message in;

    @Mock
    private Message out;

    @Mock
    private LocationContactEventHolder eventHolder;

    @Mock
    private TimestampExtractor timestampExtractor;

    @InjectMocks
    private AddHeaders processor;

    private Object body;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @SuppressWarnings("deprecation")
    @Before
    public void setUp() throws Exception {
        body = new Object();
        inOrder = inOrder(config, exchange, in, out, eventHolder, timestampExtractor);
        when(exchange.getIn()).thenReturn(in);
        when(exchange.getOut()).thenReturn(out);
        when(config.getServiceId()).thenReturn(SERVICE);
        when(config.getSatelliteId()).thenReturn(SATELLITE);
        when(config.getGroundstationId()).thenReturn(GROUND_STATION);
        when(in.getBody()).thenReturn(body);
        when(timestampExtractor.getTimestamp(in)).thenReturn(NOW);
        when(in.getHeader(SerialConsumer.SERIAL_PORT)).thenReturn(SERIAL_PORT_NAME);
    }

    /**
     * Test method for
     * {@link eu.estcube.gs.tnc.processor.AddHeaders#process(org.apache.camel.Exchange)}
     * .
     * 
     * @throws Exception
     */
    @SuppressWarnings("deprecation")
    @Test
    public void testProcessNoEventId() throws Exception {
        when(eventHolder.getContactId(NOW)).thenReturn(null);
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(timestampExtractor, times(1)).getTimestamp(in);
        inOrder.verify(eventHolder, times(1)).getContactId(NOW);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.TIMESTAMP, NOW);
        inOrder.verify(config, times(1)).getServiceId();
        inOrder.verify(out, times(1)).setHeader(StandardArguments.ISSUED_BY, SERVICE);
        inOrder.verify(config, times(1)).getSatelliteId();
        inOrder.verify(out, times(1)).setHeader(StandardArguments.SATELLITE_ID, SATELLITE);
        inOrder.verify(config, times(1)).getGroundstationId();
        inOrder.verify(out, times(1)).setHeader(StandardArguments.GROUND_STATION_ID, GROUND_STATION);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.CONTACT_ID, AddHeaders.DEFAULT_CONTACT_ID);
        inOrder.verify(in, times(1)).getBody();
        inOrder.verify(out, times(1)).setHeader(StandardArguments.CLASS, body.getClass().getSimpleName());
        inOrder.verify(out, times(1)).setHeader(StandardArguments.TYPE, Tnc.TNC);
        inOrder.verify(in, times(1)).getHeader(SerialConsumer.SERIAL_PORT);
        inOrder.verify(out, times(1)).setHeader(Headers.SERIAL_PORT_NAME, SERIAL_PORT_NAME);
        inOrder.verify(out, times(1)).setHeader(Headers.COMMUNICATION_LINK_TYPE, Downlink.class.getSimpleName());
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.gs.tnc.processor.AddHeaders#process(org.apache.camel.Exchange)}
     * .
     * 
     * @throws Exception
     */
    @SuppressWarnings("deprecation")
    @Test
    public void testProcess() throws Exception {
        when(eventHolder.getContactId(NOW)).thenReturn(EVENT_ID);
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(timestampExtractor, times(1)).getTimestamp(in);
        inOrder.verify(eventHolder, times(1)).getContactId(NOW);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.TIMESTAMP, NOW);
        inOrder.verify(config, times(1)).getServiceId();
        inOrder.verify(out, times(1)).setHeader(StandardArguments.ISSUED_BY, SERVICE);
        inOrder.verify(config, times(1)).getSatelliteId();
        inOrder.verify(out, times(1)).setHeader(StandardArguments.SATELLITE_ID, SATELLITE);
        inOrder.verify(config, times(1)).getGroundstationId();
        inOrder.verify(out, times(1)).setHeader(StandardArguments.GROUND_STATION_ID, GROUND_STATION);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.CONTACT_ID, EVENT_ID);
        inOrder.verify(in, times(1)).getBody();
        inOrder.verify(out, times(1)).setHeader(StandardArguments.CLASS, body.getClass().getSimpleName());
        inOrder.verify(out, times(1)).setHeader(StandardArguments.TYPE, Tnc.TNC);
        inOrder.verify(in, times(1)).getHeader(SerialConsumer.SERIAL_PORT);
        inOrder.verify(out, times(1)).setHeader(Headers.SERIAL_PORT_NAME, SERIAL_PORT_NAME);
        inOrder.verify(out, times(1)).setHeader(Headers.COMMUNICATION_LINK_TYPE, Downlink.class.getSimpleName());
        inOrder.verifyNoMoreInteractions();
    }
}
