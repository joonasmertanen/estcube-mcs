/** 
 *
 */
package eu.estcube.gs.tnc.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 */
public class TncDriverConfigurationTest {

    private TncDriverConfiguration config;

    @Before
    public void setUp() throws Exception {
        config = new TncDriverConfiguration();
    }

    @Test
    public void testGetSerialInPort() throws Exception {
        assertNull(config.getSerialInPort());
    }

    @Test
    public void testGetSerialOutPort() throws Exception {
        assertNull(config.getSerialOutPort());
    }

    @Test
    public void testGetSerialInBaud() throws Exception {
        assertNull(config.getSerialInBaud());
    }

    @Test
    public void testGetSerialInDataBits() throws Exception {
        assertNull(config.getSerialInDataBits());
    }

    @Test
    public void testGetSerialInStopBits() throws Exception {
        assertNull(config.getSerialInStopBits());
    }

    @Test
    public void testGetSerialInParity() throws Exception {
        assertNull(config.getSerialInParity());
    }

    @Test
    public void testGetSerialInFlowControl() throws Exception {
        assertNull(config.getSerialInFlowControl());
    }

    @Test
    public void testGetSerialOutBaud() throws Exception {
        assertNull(config.getSerialOutBaud());
    }

    @Test
    public void testGetSerialOutDataBits() throws Exception {
        assertNull(config.getSerialOutDataBits());
    }

    @Test
    public void testGetSerialOutStopBits() throws Exception {
        assertNull(config.getSerialOutStopBits());
    }

    @Test
    public void testGetSerialOutParity() throws Exception {
        assertNull(config.getSerialOutParity());
    }

    @Test
    public void testGetSerialOutFlowControl() throws Exception {
        assertNull(config.getSerialOutFlowControl());
    }

    @Test
    public void testGetSerialInFilters() throws Exception {
        assertNull(config.getSerialInFilters());
    }

    @Test
    public void testGetSerialOutFilters() throws Exception {
        assertNull(config.getSerialOutFlowControl());
    }

    @Test
    public void testUseFiles() throws Exception {
        assertFalse(config.useFiles());
    }

    @Test
    public void testGetSatelliteId() throws Exception {
        testSetSatelliteId();
    }

    @Test
    public void testSetSatelliteId() throws Exception {
        assertNull(config.getSatelliteId());
        config.setSatelliteId("SAT-ID");
        assertEquals("SAT-ID", config.getSatelliteId());
    }

    @Test
    public void testGetFramesInterval() throws Exception {
        assertEquals(0, config.getFramesInterval());
    }

    @Test
    public void testGetFilePollInterval() throws Exception {
        assertEquals(0, config.getFilePollInterval());
    }

    @Test
    public void testGetMaxMessagesPerPoll() throws Exception {
        assertEquals(0, config.getMaxMessagesPerPoll());
    }
}
