package eu.estcube.limitchecking.utils;

import java.io.File;
import java.io.FilenameFilter;

import eu.estcube.limitchecking.constants.LimitConstants;

public class FileManager {

    public static File[] getFiles() {

        FilenameFilter filter = new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {

                return name.endsWith(".xml");
            }
        };

        String pathToFolder = System.getProperty(LimitConstants.PATHPROPERTY);

        File folder = new File(pathToFolder);

        if (folder.isDirectory()) {

            return folder.listFiles(filter);

        } else {

            return null;
        }
    }
}
