package eu.estcube.limitchecking.domain;

import java.math.BigDecimal;

public class InfoContainer {

    private String id;
    private BigDecimal[] sanityLimits;
    private BigDecimal[] hardLimits;
    private BigDecimal[] softLimits;
    private int levels;

    public InfoContainer(String id, BigDecimal sanityLower, BigDecimal sanityUpper, BigDecimal hardLower,
            BigDecimal hardUpper,
            BigDecimal softLower, BigDecimal softUpper) {

        this.id = id;

        if (sanityUpper != null && sanityLower != null) {

            sanityLimits = new BigDecimal[2];
            sanityLimits[0] = sanityLower;
            sanityLimits[1] = sanityUpper;

            levels = 4;

        } else {

            levels = 3;

        }

        hardLimits = new BigDecimal[2];
        hardLimits[0] = hardLower;
        hardLimits[1] = hardUpper;

        softLimits = new BigDecimal[2];
        softLimits[0] = softLower;
        softLimits[1] = softUpper;

    }

    public String getId() {

        return this.id;
    }

    public BigDecimal getSanityLower() {

        return sanityLimits[0];

    }

    public BigDecimal getSanityUpper() {

        return sanityLimits[1];
    }

    public BigDecimal getHardLower() {

        return hardLimits[0];
    }

    public BigDecimal getHardUpper() {

        return hardLimits[1];
    }

    public BigDecimal getSoftLower() {

        return softLimits[0];
    }

    public BigDecimal getSoftUpper() {

        return softLimits[1];
    }

    public boolean isSanityCheckAvailable() {

        return sanityLimits != null;

    }

    public int getLevels() {

        return levels;
    }

}
