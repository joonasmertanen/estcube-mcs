package eu.estcube.archiver.raw.beacon;

import javax.sql.DataSource;

import eu.estcube.archiver.notifier.MailNotifier;
import eu.estcube.archiver.raw.ax25.Ax25ToSqlProvider;
import eu.estcube.archiver.raw.sql.AbstractSqlFrameArchiver;

public class OracleBeaconFrameArchiver extends AbstractSqlFrameArchiver {

    public OracleBeaconFrameArchiver(DataSource dataSource, MailNotifier notifier) {
        super(dataSource, notifier);
    }

    @Override
    public String getAutoIncrementFunction() {
        return "MCS.BEACON_FRAMES_SEQ.NEXTVAL";
    }

    @Override
    public String getDetailsAutoIncrementFunction() {
        return "MCS.BEACON_FRAME_DETAILS_SEQ.NEXTVAL";
    }

    @Override
    public String[] getColumns() {
        return new String[] { Ax25ToSqlProvider.COL_RECEPTION_TIME,
                BeaconToSqlProvider.COL_SATELLITE,
                BeaconToSqlProvider.COL_NAME,
                BeaconToSqlProvider.COL_VALUE,
                BeaconToSqlProvider.COL_INSTANCE_ID,
                BeaconToSqlProvider.COL_DESCRIPTION,
                BeaconToSqlProvider.COL_TIMESTAMP,
                BeaconToSqlProvider.COL_ISSUEDBY,
                BeaconToSqlProvider.COL_INSERTEDBY
        };
    }

    @Override
    public String getTable() {
        return "MCS.BEACON_FRAMES";
    }

    @Override
    public String getDetailsTable() {
        return "MCS.BEACON_FRAME_DETAILS";
    }

    @Override
    public String getTimestampFunction() {
        return "SYSDATE";
    }

    /**
     * public String createInsert(String table, String idColumn,
     * String directionColumn, String createdColumn, String[] columns,
     * String autoIncrementFunction, String timestampFunction) {
     * StringBuilder b = new StringBuilder("INSERT INTO ").append(table);
     * 
     * b.append(" (").append(idColumn);
     * // b.append(", ").append(directionColumn);
     * 
     * for (int i = 0; i < columns.length; i++) {
     * b.append(", ");
     * b.append(columns[i]);
     * }
     * b.append(", ").append(createdColumn);
     * 
     * b.append(") VALUES (").append(autoIncrementFunction);
     * b.append(", ?"); // direction
     * for (int i = 0; i < columns.length-1; i++) {
     * b.append(", ");
     * b.append("?");
     * }
     * b.append(", ").append(timestampFunction).append(")");
     * 
     * return b.toString();
     * }
     */

}
