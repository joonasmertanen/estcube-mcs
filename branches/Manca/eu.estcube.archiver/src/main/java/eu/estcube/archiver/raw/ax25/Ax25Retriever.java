package eu.estcube.archiver.raw.ax25;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;
import javax.xml.ws.Endpoint;

import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.sun.xml.bind.v2.schemagen.xmlschema.List;

import eu.estcube.archiver.notifier.MailNotifier;
import eu.estcube.archiver.raw.ArchivingException;
import eu.estcube.archiver.raw.FrameDataProvider;
import eu.estcube.archiver.raw.FrameRetriever;
import eu.estcube.common.ByteUtil;
import eu.estcube.common.queryParameters.AX25Object;
import eu.estcube.common.queryParameters.AX25QueryParameters;
import eu.estcube.common.queryParameters.QueryParameters;


public class Ax25Retriever extends FrameRetriever {

    public Ax25Retriever(DataSource source) {
        super(source);
    }

    private Logger LOG = LoggerFactory.getLogger(Ax25Retriever.class);

    private String createQuery(AX25QueryParameters params) {
        StringBuilder sb = new StringBuilder();
//        if (!params.getGroundStationVersion().isEmpty()) {
//            sb.append(" AND info LIKE '__")
//                    .append(params.getGroundStationVersion())
//                    .append("%'");
//        }
        if (params.getSubsystem() != Integer.MIN_VALUE) {
            String direction = params.getDirection();
            if(!direction.isEmpty()){
                if(direction.equals( "UP")){
                    sb.append(" AND info LIKE '060")
                    .append(params.getSubsystem())
                    .append("%'");
                }else if (direction.equals("DOWN")){
                    sb.append(" AND info LIKE '0")
                    .append(params.getSubsystem())
                    .append("06%'");
                }
                
            }else{
                sb.append(" AND (info LIKE '0")
                .append(params.getSubsystem())
                .append("06%' OR info LIKE '060")
                .append(params.getSubsystem())
                .append("%')");
            }
        }
       String query = super.createQuery(params);
       query = query.replace("ReplaceInSubclass", sb.toString());
      
        return query;
    }


    @Override
    public void retrieve(Exchange exchange) throws ArchivingException, SQLException {
        AX25QueryParameters params = (AX25QueryParameters) exchange.getIn().getBody();
        LOG.info("Ax25Retreiver in parameters" + params.toString());
        String query = createQuery(params);
        LOG.info("Ax25Retreiver query" + query);

        super.retrieve(exchange);
        Statement statement = c.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        
        //get metadata
        ResultSetMetaData meta = null;
        meta = resultSet.getMetaData();

        //get column names
        int colCount = meta.getColumnCount();
        ArrayList<String> cols = new ArrayList<String>();
        for (int i=1; i<=colCount; i++){
          cols.add(meta.getColumnName(i));
        }
        //fetch out rows
        ArrayList<AX25Object> rows = new ArrayList<AX25Object>();

        //make new object
        while (resultSet.next()) {
          AX25Object row = new AX25Object();
          for (String colName:cols) {
              if (resultSet.getObject(colName) == null);
              else if (colName.equals("ID")){
                  row.setId((resultSet.getInt(colName)));
              }else if (colName.equals("DIRECTION")){
                  row.setDirection(resultSet.getString(colName));
              }else if (colName.equals("RECEPTION_TIME")){
                  row.setReceptionTime(resultSet.getString(colName)); 
              }else if (colName.equals("SATELLITE")){
                  row.setSatellite(resultSet.getString(colName)); 
              }else if (colName.equals("DESTADDR")){
                  row.setDestAddr(ByteUtil.toHexString(resultSet.getBytes(colName))); 
              }else if (colName.equals("SRCADDR")){
                  row.setSrcAddr(ByteUtil.toHexString(resultSet.getBytes(colName))); 
              }else if (colName.equals("CTRL")){
                  row.setCtrl(Integer.parseInt(resultSet.getString(colName))); 
              }else if (colName.equals("PID")){
                  row.setPid(Integer.parseInt(resultSet.getString(colName))); 
              }else if (colName.equals("INFO")){
                  row.setInfo((ByteUtil.toHexString(resultSet.getBytes(colName)))); 
              }else if (colName.equals("FCS")){
                  row.setFcs(resultSet.getString(colName)); 
              }else if (colName.equals("ERROR_BITMASK")){
                  row.setErrorBitmask(Integer.parseInt(resultSet.getString(colName))); 
              }else if (colName.equals("CREATED")){
                  long mili= resultSet.getTimestamp(colName).getTime();
                  row.setCreated(new Date(mili)); 
              }
          }
          rows.add(row);
        }
        LOG.info("Result: " + rows.size());
        
        exchange.getOut().setBody(rows);
        c.close();

    }

    @Override
    protected String getTable() {
        return AX25QueryParameters.getTable();
    }



    @Override
    protected String getDetailsTable() {
        return AX25QueryParameters.getDetailsTable();
    }



}
