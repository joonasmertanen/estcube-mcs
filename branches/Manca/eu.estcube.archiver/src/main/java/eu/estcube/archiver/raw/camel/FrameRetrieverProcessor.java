package eu.estcube.archiver.raw.camel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultCamelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.archiver.raw.FrameRetriever;
import eu.estcube.common.queryParameters.QueryParameters;


public class FrameRetrieverProcessor implements Processor {

    private ProducerTemplate producer;
    
    private FrameRetriever frameRetriever;
    protected QueryParameters params;
    public FrameRetrieverProcessor(FrameRetriever frameRetriever) {
        this.frameRetriever = frameRetriever;
    }

    public FrameRetriever getFrameRetriever() {
        return frameRetriever;
    }


    @Override
    public void process(Exchange exchange) throws Exception {
        frameRetriever.retrieve(exchange);
        DefaultCamelContext context = new DefaultCamelContext();
        producer = context.createProducerTemplate();
        try {
            ArrayList<Object> array = (ArrayList<Object>) exchange.getOut().getBody();
            
            int partitionSize = 1000;
            ArrayList<List<Object>> partitions = new ArrayList<List<Object>>();
            for (int i = 0; i < array.size(); i += partitionSize) {
                partitions.add(array.subList(i,i + Math.min(partitionSize, array.size() - i)));
            }
            
            Iterator<List<Object>> i = partitions.iterator();
            
            while( i.hasNext()){

                producer.sendBody("activemq:queue:customQueryReturn", i.next().toArray());
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
