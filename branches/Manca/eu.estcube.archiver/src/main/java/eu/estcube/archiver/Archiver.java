package eu.estcube.archiver;

import java.sql.SQLException;

import javax.sql.DataSource;

import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.hbird.business.core.AddHeaders;
import org.hbird.business.core.EntityRouter;
import org.hbird.exchange.configurator.StandardEndpoints;
import org.hbird.exchange.core.BusinessCard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import eu.estcube.archiver.notifier.MailNotifier;
import eu.estcube.archiver.raw.ax25.Ax25Retriever;
import eu.estcube.archiver.raw.ax25.Ax25ToSqlProvider;
import eu.estcube.archiver.raw.ax25.OracleAx25FrameArchiver;
import eu.estcube.archiver.raw.beacon.BeaconRetriever;
import eu.estcube.archiver.raw.beacon.BeaconToSqlProvider;
import eu.estcube.archiver.raw.beacon.OracleBeaconFrameArchiver;
import eu.estcube.archiver.raw.camel.FrameArchiverProcessor;
import eu.estcube.archiver.raw.camel.FrameRetrieverProcessor;
import eu.estcube.archiver.raw.tnc.OracleTncFrameArchiver;
import eu.estcube.archiver.raw.tnc.TncRetriever;
import eu.estcube.archiver.raw.tnc.TncToSqlProvider;
import eu.estcube.common.Constants;
import eu.estcube.common.queryParameters.AX25QueryParameters;
import eu.estcube.common.queryParameters.TncQueryParameters;
import eu.estcube.domain.transport.Direction;
import eu.estcube.domain.transport.Downlink;
import eu.estcube.domain.transport.Uplink;

public class Archiver extends RouteBuilder {
    private static final Logger LOG = LoggerFactory.getLogger(Archiver.class);

    @Value("${service.id}")
    private String serviceId;

    // TODO - 10.07.2013; kimmell - remove when hbird ConfigurationBase has this
    // field - probably version 0.10.0
    @Deprecated
    @Value("${service.name}")
    private String serviceName;

    @Value("${heart.beat.interval}")
    private int heartBeatInterval;

    @Value("${oracle.url}")
    private String oracleUrl;

    @Autowired
    private AddHeaders addHeaders;

    @Autowired
    private EntityRouter router;

    @Autowired
    private MailNotifier emailer;

    private FrameArchiverProcessor ax25DownlinkArchiver;
    private FrameArchiverProcessor ax25UplinkArchiver;
    private FrameArchiverProcessor tncUplinkArchiver;
    private FrameArchiverProcessor tncDownlinkArchiver;
    private FrameArchiverProcessor beaconArchiver;
    
    //custom data query retrievers
    private FrameRetrieverProcessor ax25FrameRetriever;
    private FrameRetrieverProcessor beaconRetriever;
    private FrameRetrieverProcessor tncFrameRetriever;

    private DataSource dataSource;

    private void createDataSource() throws SQLException {
        PoolDataSource pds = PoolDataSourceFactory.getPoolDataSource();
        pds.setConnectionFactoryClassName("oracle.jdbc.pool.OracleDataSource");
        pds.setURL(oracleUrl);
        pds.setInitialPoolSize(1);
        pds.setMaxPoolSize(30);
        pds.setAbandonedConnectionTimeout(30); // Just for safety, if connection
                                               // is not used for
                                               // 30 seconds, it is being put
                                               // back in the pool
        LOG.info("Testing connection to database");
        pds.getConnection().close();
        LOG.info("Connection successful");

        dataSource = pds;
    }

    private void createProcessors() {
        ax25DownlinkArchiver = new FrameArchiverProcessor(
                new OracleAx25FrameArchiver(dataSource, emailer),
                new Ax25ToSqlProvider(Direction.DOWN));

        ax25UplinkArchiver = new FrameArchiverProcessor(
                new OracleAx25FrameArchiver(dataSource, emailer),
                new Ax25ToSqlProvider(Direction.UP));

        tncDownlinkArchiver = new FrameArchiverProcessor(
                new OracleTncFrameArchiver(dataSource, emailer),
                new TncToSqlProvider(Direction.DOWN));

        tncUplinkArchiver = new FrameArchiverProcessor(
                new OracleTncFrameArchiver(dataSource, emailer),
                new TncToSqlProvider(Direction.UP));

        beaconArchiver = new FrameArchiverProcessor(
                new OracleBeaconFrameArchiver(dataSource, emailer),
                new BeaconToSqlProvider(Direction.DOWN));
        
        
        //custom data query retrievers
        ax25FrameRetriever = new FrameRetrieverProcessor(
                new Ax25Retriever(dataSource));
        beaconRetriever = new FrameRetrieverProcessor(
                new BeaconRetriever(dataSource));
        tncFrameRetriever = new FrameRetrieverProcessor(
                new TncRetriever(dataSource));


    }

    @Override
    public void configure() throws Exception {
        LOG.info("Creating data source");
        createDataSource();

        LOG.info("Creating processors");
        createProcessors();

        LOG.info("Creating routes with processors");

        // @formatter:off

		from(Downlink.AX25_FRAMES).process(ax25DownlinkArchiver);
		from(Uplink.AX25_FRAMES_LOG).process(ax25UplinkArchiver);

		from(Downlink.FROM_TNC).process(tncDownlinkArchiver);
		from(Uplink.TNC_FRAMES_LOG).process(tncUplinkArchiver);

		from(StandardEndpoints.MONITORING)
				.filter(simple("${in.header.entityID} regex '\\/ESTCUBE\\/Satellites\\/.*\\/beacon\\/raw'"))
				.process(beaconArchiver);
		
		BusinessCard card = new BusinessCard(serviceId, serviceName);
		card.setPeriod(heartBeatInterval);
		card.setDescription("Frame archiver");
		from("timer://heartbeat?fixedRate=true&period=" + heartBeatInterval)
				.bean(card, "touch").process(addHeaders).process(router)
				.recipientList(header("hbird.route")).end();

        //custom data query route
        from(Constants.AMQ_RETRIEVE).choice()
            .when(simple("${in.header.table} == 'AX25'")).process(ax25FrameRetriever)
            .when(simple("${in.header.table} == 'TNC'")).process(tncFrameRetriever)
            .otherwise().process(beaconRetriever)
            .end();
        
		// @formatter:on
    }

    public static void main(String[] args) throws Exception {
        LOG.info("Starting Oracle Archiver");
        new Main().run(args);
    }
}