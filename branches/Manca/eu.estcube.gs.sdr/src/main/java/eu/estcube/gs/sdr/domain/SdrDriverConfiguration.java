package eu.estcube.gs.sdr.domain;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.estcube.gs.GsDriverConfiguration;

/**
 *
 */
@Component
public class SdrDriverConfiguration extends GsDriverConfiguration {

    private static final long serialVersionUID = -1545747881443087813L;

    @Value("${ip.in.host}")
    private String ipInHost;

    @Value("${ip.in.port}")
    private int ipInPort;

    @Value("${ip.out.host}")
    private String ipOutHost;

    @Value("${ip.out.port}")
    private int ipOutPort;

    public String getIpInHost() {
        return ipInHost;
    }

    public int getIpInPort() {
        return ipInPort;
    }

    public String getIpOutHost() {
        return ipOutHost;
    }

    public int getIpOutPort() {
        return ipOutPort;
    }
}
