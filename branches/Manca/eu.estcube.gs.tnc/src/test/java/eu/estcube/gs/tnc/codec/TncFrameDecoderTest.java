package eu.estcube.gs.tnc.codec;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.domain.transport.tnc.TncFrame;
import eu.estcube.domain.transport.tnc.TncFrame.TncCommand;

/**
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class TncFrameDecoderTest {

    @Mock
    private IoSession session;
    
    @Mock
    private ProtocolDecoderOutput out;
    
    private TncFrameDecoder decoder;
    
    private InOrder inOrder;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        decoder = new TncFrameDecoder();
        inOrder = inOrder(session, out);
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFrameDecoder#decode(org.apache.mina.core.session.IoSession, org.apache.mina.core.buffer.IoBuffer, org.apache.mina.filter.codec.ProtocolDecoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testDecode() throws Exception {
        byte[] bytes = new byte[] { 0x01, 0x03, 0x05, 0x07 };
        TncFrame tncFrame = new TncFrame(TncCommand.SET_HARDWARE, 12, bytes);
        IoBuffer in = IoBuffer.wrap(TncFrames.toBytes(tncFrame));
        decoder.decode(session, in, out);
        ArgumentCaptor<TncFrame> captor = ArgumentCaptor.forClass(TncFrame.class);
        inOrder.verify(out, times(1)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        assertEquals(TncCommand.SET_HARDWARE, captor.getValue().getCommand());
        assertEquals(12, captor.getValue().getTarget());
        assertTrue(Arrays.equals(bytes, captor.getValue().getData()));
    }
}
