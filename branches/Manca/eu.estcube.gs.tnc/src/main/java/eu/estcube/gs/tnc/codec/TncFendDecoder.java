package eu.estcube.gs.tnc.codec;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Reads TNC frames between FEND bytes.
 */
public class TncFendDecoder extends CumulativeProtocolDecoder {

    private static final Logger LOG = LoggerFactory.getLogger(TncFendDecoder.class);
    
    /** @{inheritDoc}. */
    @Override
    protected boolean doDecode(IoSession session, IoBuffer in, ProtocolDecoderOutput out) throws Exception {
        // the initial position
        int start = in.position();
        
        while (in.hasRemaining()) {
            byte current = in.get();
            if (current == TncConstants.FEND) {
                // store current values
                int position = in.position();
                int limit = in.limit();
                try {
                    // set position to the first byte
                    in.position(start);
                    // set limit to the byte before found FEND
                    in.limit(position - 1);
                    if (position - 1 > start) { // check if there are any bytes between FEND and start
                        // buffer contains one TNC frame between position and limit now
                        IoBuffer slice = in.slice();
                        LOG.trace("Decoded {}", slice.getHexDump());
                        // send it to the next decoder
                        out.write(slice);
                    }
                } finally {
                    // set the limit back to the buffer limit
                    in.limit(limit);
                    // set position right after the found FEEND
                    in.position(Math.min(position, limit));
                }
                // Decoded one TNC frame; CumulativeProtocolDecoder will
                // call me again until I return false. So just
                // return true until there are no more TNC frames in the
                // buffer.
                return true;
            }
        }
        // Could not find FEND in the buffer. Reset the initial
        // position to the one we recorded above.
        in.position(start);
        return false;
    }
}
