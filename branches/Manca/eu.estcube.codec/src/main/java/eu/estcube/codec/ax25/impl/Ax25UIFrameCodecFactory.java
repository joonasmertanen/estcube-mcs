package eu.estcube.codec.ax25.impl;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class Ax25UIFrameCodecFactory implements ProtocolCodecFactory {

    private static final Logger LOG = LoggerFactory.getLogger(Ax25UIFrameCodecFactory.class);

    private ProtocolEncoder encoder = new Ax25UIFrameEncoder();

    private ProtocolDecoder decoder = new Ax25UIFrameDecoder();

    /** @{inheritDoc . */
    @Override
    public ProtocolEncoder getEncoder(IoSession session) throws Exception {
        LOG.trace("Getting AX.25 UI frame encoder");
        return encoder;
    }

    /** @{inheritDoc . */
    @Override
    public ProtocolDecoder getDecoder(IoSession session) throws Exception {
        LOG.trace("Getting AX.25 UI frame decoder");
        return decoder;
    }
}
