package eu.estcube.codec.ax25.impl;

/**
 *
 */
public class Ax25CRC16 {
    // https://kratt.physic.ut.ee/svn/COM/trunk/tinyos/platforms/ec1/mac/ax25/ax25crc.h
    // XXX: Copied from code onboard ESTCube-1, not sure if equivalent to
    // standard AX.25 CRC-16-CCITT. Make also Ax25CRC16Test.java. Order of FCS
    // bytes?
    protected static short ec1ax25crc16(byte[] data_p) {
        int length = data_p.length;
        int ptr = 0;

        int crc = 0xffff; // uint16_t
        int crc16_table[] = { // uint16_t
        0x0000, 0x1081, 0x2102, 0x3183,
                0x4204, 0x5285, 0x6306, 0x7387,
                0x8408, 0x9489, 0xa50a, 0xb58b,
                0xc60c, 0xd68d, 0xe70e, 0xf78f
        };

        while (length-- > 0) {
            crc = (crc >> 4 & 0xffff) ^ crc16_table[(crc & 0xf) ^ (data_p[ptr] & 0xf)];
            crc = (crc >> 4 & 0xffff) ^ crc16_table[(crc & 0xf) ^ (data_p[ptr++] >> 4 & 0xf)];
        }

        crc = (crc << 8) | (crc >> 8 & 0xff); // do byte swap here that is
                                              // needed by AX25 standard
        return (short) (~crc & 0xffff);
    }
}
