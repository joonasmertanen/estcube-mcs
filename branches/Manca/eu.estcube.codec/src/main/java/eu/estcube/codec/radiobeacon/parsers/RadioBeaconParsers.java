package eu.estcube.codec.radiobeacon.parsers;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.core.EntityInstance;
import org.hbird.exchange.core.Label;
import org.hbird.exchange.core.Metadata;

import eu.estcube.codec.radiobeacon.parser.RadioBeaconMessageParser;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserTimestamp;
import eu.estcube.common.hbird.MetadataFactory;

public abstract class RadioBeaconParsers {

    protected final IdBuilder idBuilder;

    private MetadataFactory metadataFactory;

    public RadioBeaconParsers(IdBuilder idBuilder) {
        this.idBuilder = idBuilder;
    }

    abstract public List<RadioBeaconMessageParser> getParsers();

    abstract public String getEntityID();

    abstract public Label getRadioBeacon();
    
    abstract public void updateParameters();

    abstract public RadionBeaconMessageParserTimestamp getTimestampParser();

    public HashMap<String, EntityInstance> parse(String message, Long timestamp, String issuedBy, String insertedBy) {

        HashMap<String, EntityInstance> list = new LinkedHashMap<String, EntityInstance>();

        updateParameters();
        Label beacon = getRadioBeacon();
        beacon.setValue(message);
        beacon.setIssuedBy(issuedBy);
        beacon.setTimestamp(timestamp);
        String beaconId = idBuilder.buildID(getEntityID(), beacon.getName());
        beacon.setID(beaconId);
        list.put(beacon.getName(), beacon);
        
        
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("insertedBy", insertedBy);
        metadataFactory = new MetadataFactory();
        Metadata meta = metadataFactory.createMetadata(beacon, data, issuedBy);
        list.put(meta.getName(), meta);    
       

        for (RadioBeaconMessageParser parser : getParsers()) {
            EntityInstance issued = parser.parseToIssued(message);
            if (issued != null) {
                String issuedId = idBuilder.buildID(getEntityID(), issued.getName());
                issued.setID(issuedId);
                issued.setIssuedBy(issuedBy);
                issued.setTimestamp(timestamp);
                list.put(issued.getName(), issued);
                

            }
        }
        
        return list;
    }

}
