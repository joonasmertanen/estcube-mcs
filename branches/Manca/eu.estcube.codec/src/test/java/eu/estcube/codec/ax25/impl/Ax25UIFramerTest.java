package eu.estcube.codec.ax25.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Before;
import org.junit.Test;

import eu.estcube.domain.transport.ax25.Ax25UIFrame;

public class Ax25UIFramerTest {
	private static final byte[] DEST_ADDR = new byte[] { 1, 2, 3, 4, 5, 6, 7 };
	private static final byte[] SRC_ADDR = new byte[] { 8, 9, 10, 11, 12, 13, 14 };
	private static final byte CTRL = 15;
	private static final byte PID = 16;

	private Ax25UIFramer framer;

	@Before
	public void setUp() throws Exception {
		framer = new Ax25UIFramer(2, CTRL, PID, SRC_ADDR, DEST_ADDR);
	}

	@Test
	public void testCreateOneFrame() {
		byte[] info = new byte[] { 33, 34 };

		Ax25UIFrame[] frames = framer.create(info);

		assertEquals(1, frames.length);
		checkFrame(frames[0], info);
	}

	@Test
	public void testCreateTwoFrames() {
		byte[] info = new byte[] { 33, 34, 35, 36 };

		Ax25UIFrame[] frames = framer.create(info);

		assertEquals(2, frames.length);
		checkFrame(frames[0], ArrayUtils.subarray(info, 0, 2));
		checkFrame(frames[1], ArrayUtils.subarray(info, 2, 4));
	}

	@Test
	public void testCreateTwoAndHalfFrames() {
		byte[] info = new byte[] { 33, 34, 35, 36, 37 };

		Ax25UIFrame[] frames = framer.create(info);

		assertEquals(3, frames.length);
		checkFrame(frames[0], ArrayUtils.subarray(info, 0, 2));
		checkFrame(frames[1], ArrayUtils.subarray(info, 2, 4));
		checkFrame(frames[2], ArrayUtils.subarray(info, 4, 5));
	}

	@Test
	public void testCreateHalfFrames() {
		byte[] info = new byte[] { 33 };

		Ax25UIFrame[] frames = framer.create(info);

		assertEquals(1, frames.length);
		checkFrame(frames[0], info);
	}

	protected void checkFrame(Ax25UIFrame f, byte[] info) {
		assertTrue(Arrays.equals(DEST_ADDR, f.getDestAddr()));
		assertTrue(Arrays.equals(SRC_ADDR, f.getSrcAddr()));
		assertEquals(CTRL, f.getCtrl());
		assertEquals(PID, f.getPid());
		assertTrue(Arrays.equals(info, f.getInfo()));
	}

}
