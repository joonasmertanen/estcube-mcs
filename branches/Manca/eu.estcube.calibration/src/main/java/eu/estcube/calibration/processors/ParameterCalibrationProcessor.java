/** 
 *
 */
package eu.estcube.calibration.processors;


import java.io.File;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.hbird.exchange.core.CalibratedParameter;
import org.hbird.exchange.core.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

import eu.estcube.calibration.data.CalibrationExpression;

/**
 * Calibrates <code>Parameter</code>s in Camel route
 */
@Component
public class ParameterCalibrationProcessor implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(ParameterCalibrationProcessor.class);
    private XStream xstream;
    private File calibrationSettings;
    private List<CalibrationExpression> calibrationExpressions;

    /**
     * Constructs a new instance of <code>ParameterCalibrationProcessor</code>,
     * loading the calibration expressions from a file specified in VM arguments
     * ("calibration.values")
     * 
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public ParameterCalibrationProcessor() throws Exception {
        if (!System.getProperties().containsKey("calibration.values")) {
            LOG.error("Calibration file location is not set with \"calibration.values\" in VM arguments");
            return;
        }
        calibrationSettings = new File(System.getProperty("calibration.values"));
        xstream = new XStream(new StaxDriver());
        xstream.alias("parameters", List.class);
        xstream.alias("parameter", CalibrationExpression.class);
        calibrationExpressions = (List<CalibrationExpression>) xstream.fromXML(calibrationSettings);
    }

    /** @{inheritDoc . */
    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();

        Parameter input = in.getBody(Parameter.class);
        CalibratedParameter output = new CalibratedParameter(input);
        String id = output.getID().replace("//", "/");
        for (CalibrationExpression c : calibrationExpressions) {
            if (c.getID().equals(id)) {
                try {
                    c.calibrate(output);
                } catch (Exception e) {
                    LOG.error(
                            "Failed to calibrate Parameter: " + c.getID() + " (current value: " + output.asDouble()
                                    + "; expression: " + c.getExpression() + ")", e);
                }
                break;
            }
        }
        out.setBody(output);

    }

}
