package eu.estcube.commandautomation.processor;

import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.commandautomation.automator.Automator;

@Component
public class LosEventProcessor extends AbstractTrackingEventProcessor {

    @Autowired
    private Automator commandAutomation;

    @Override
    public void process(Exchange exchange) throws Exception {
        commandAutomation.end((getTrackingId(exchange)));
    }
}
