/** 
 *
 */
package eu.estcube.limitchecking.processors;

import java.util.List;

import org.hbird.exchange.core.Parameter;
import org.hbird.exchange.core.State;

import eu.estcube.limitchecking.domain.InfoContainer;

/**
 *
 */
public interface ParameterLimitChecker {

    public List<State> checkLimits(Parameter input, InfoContainer limits) throws Exception;
}
