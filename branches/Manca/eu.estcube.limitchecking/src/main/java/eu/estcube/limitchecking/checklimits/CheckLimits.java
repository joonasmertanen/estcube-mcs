package eu.estcube.limitchecking.checklimits;

import java.math.BigDecimal;
import java.util.List;

import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.core.Parameter;
import org.hbird.exchange.core.State;

import eu.estcube.limitchecking.domain.InfoContainer;
import eu.estcube.limitchecking.processors.ParameterLimitChecker;

public abstract class CheckLimits implements ParameterLimitChecker {

    protected boolean isInDiscardRegion(Parameter p, InfoContainer limits) {

        BigDecimal value = new BigDecimal(p.getValue().toString());

        return (value.compareTo(limits.getSanityLower()) < 0 || value.compareTo(limits.getSanityUpper()) > 0);

    }

    protected abstract boolean isInErrorRegion(Parameter p, InfoContainer limits);

    protected boolean isInWarningRegion(Parameter p, InfoContainer limits) {

        BigDecimal value = new BigDecimal(p.getValue().toString());

        return (value.compareTo(limits.getHardLower()) >= 0 && value.compareTo(limits.getSoftLower()) < 0)
                || (value.compareTo(limits.getSanityUpper()) > 0 && value.compareTo(limits.getHardUpper()) <= 0);

    }

    protected boolean isInOKRegion(Parameter p, InfoContainer limits) {

        BigDecimal value = new BigDecimal(p.getValue().toString());

        return (value.compareTo(limits.getSoftLower()) >= 0 && value.compareTo(limits.getSoftUpper()) <= 0);

    }

    public abstract List<State> checkLimits(Parameter p, InfoContainer limits, IdBuilder idBuilder);

}
