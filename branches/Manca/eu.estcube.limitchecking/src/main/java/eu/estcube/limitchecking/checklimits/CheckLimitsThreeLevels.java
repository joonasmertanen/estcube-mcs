package eu.estcube.limitchecking.checklimits;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.core.Parameter;
import org.hbird.exchange.core.State;

import eu.estcube.limitchecking.domain.InfoContainer;

public class CheckLimitsThreeLevels extends CheckLimits {

    @Override
    protected boolean isInErrorRegion(Parameter p, InfoContainer limits) {

        BigDecimal value = new BigDecimal(p.getValue().toString());

        return value.compareTo(limits.getHardLower()) < 0 || value.compareTo(limits.getHardUpper()) > 0;
    }

    public ArrayList<State> checkLimits(Parameter p, InfoContainer limits) {

        State s0 = new State("", "");
        s0.setValue(false);

        State s1 = new State("", "");
        s1.setValue(false);

        State s2 = new State("", "");
        s2.setValue(false);

        State s3 = new State("", "");
        s3.setValue(false);

        ArrayList<State> states = new ArrayList<State>(4);

        s1.setValue(isInErrorRegion(p, limits));

        s2.setValue(isInWarningRegion(p, limits));

        s3.setValue(isInOKRegion(p, limits));

        states.add(s0);
        states.add(s1);
        states.add(s2);
        states.add(s3);

        return states;

    }

	@Override
	public List<State> checkLimits(Parameter p, InfoContainer limits,
			IdBuilder idBuilder) {
		// TODO Auto-generated method stub
		return null;
	}

}
