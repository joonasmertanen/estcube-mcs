/** 
 *
 */
package eu.estcube.limitchecking.processors;

import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.hbird.exchange.core.Parameter;
import org.hbird.exchange.core.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.limitchecking.checklimits.CheckLimitsFourLevels;
import eu.estcube.limitchecking.checklimits.CheckLimitsThreeLevels;
import eu.estcube.limitchecking.domain.InfoContainer;
import eu.estcube.limitchecking.utils.InitLimits;

/**
 *
 */
@Component
public class ParameterProcessor implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(ParameterProcessor.class);

    private Map<String, InfoContainer> limits = InitLimits.getLimits();


    /** @{inheritDoc . */
    @Override
    public void process(Exchange exchange) throws Exception {

        Message in = exchange.getIn();
        Message out = exchange.getOut();
        out.copyFrom(in);

        Parameter input = in.getBody(Parameter.class);

        if (limits.containsKey(input.getName())) {

            InfoContainer l = limits.get(input.getName());

            switch (l.getLevels()) {

                case (3):
                    ParameterLimitChecker plcThree = new CheckLimitsThreeLevels();
                    List<State> statesThree = plcThree.checkLimits(input, l);
                    out.setBody(statesThree);

                    break;

                case (4):
                    ParameterLimitChecker plcFour = new CheckLimitsFourLevels();
                    List<State> statesFour = plcFour.checkLimits(input, l);
                    out.setBody(statesFour);

                    break;

            }

        } else {

            // no limits found - stop the processing route
            LOG.debug("Skipping Parameter; ID: {}", input.getID(),
                    input.getName());
            exchange.setProperty(Exchange.ROUTE_STOP, Boolean.TRUE);

        }

    }
}
