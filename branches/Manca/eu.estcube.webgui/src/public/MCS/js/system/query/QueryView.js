define([ "dojo/_base/declare", "dojo/dom-construct",
		"dijit/layout/BorderContainer", "dijit/layout/ContentPane",
		"dijit/form/DateTextBox", "dijit/form/Button", "dojo/on",
		"dojo/parser", "dijit/form/ValidationTextBox",
		"dijit/form/DropDownButton", "dijit/DropDownMenu", "dijit/MenuItem",
		"common/store/MissionInformationStore", "common/utils/DoIntersect",
		"dojo/_base/lang", "dojo/_base/array", "dijit/form/FilteringSelect",
		"common/store/ForEachStoreElement", "common/store/ArchiverStore",
		"dojo/store/Memory", "dojo/store/Observable", "dojo/request",
		"dijit/form/Form", "dojox/grid/DataGrid",
		"common/display/QueryContentProvider",
		"common/formatter/DateFormatterS", ],

function(declare, DomConstruct, BorderContainer, ContentPane, DateTextBox,
		Button, on, Parser, ValidationTextBox, DropDownButton, DropDownMenu,
		MenuItem, MissionInformationStore, CanSee, Lang, Arrays,
		FilteringSelect, ForEachStoreElement, ArchiverStore, Memory,
		Observable, request, Form, DataGrid, QueryContentProvider,
		DateFormatterS) {
	var form;
	return declare([], {

		placeAt : function(container) {
			DomConstruct.place(this.mainContainer, container);
		},

		addCommonFields : function(cp, ccp) {
			var self = this;
			var start = new DateTextBox({
				id : "start",
				class : "queryViewFilter",
				constraints : {
					datePattern : 'yyyy-MM-dd'
				},
				value : config.DataQuery.defaultRangeStart

			});
			var end = new DateTextBox({
				id : "end",
				class : "queryViewFilter",
				constraints : {
					datePattern : 'yyyy-MM-dd'
				},
				value : config.DataQuery.defaultRangeEnd
			});

			var tableNameStore = new Memory({
				data : [ {
					name : "AX25",
					id : "AX25"
				}, {
					name : "TNC",
					id : "TNC"
				} ]
			});
			tableName = new FilteringSelect({
				name : "tableName",
				store : tableNameStore,
				searchAttr : "name",
				required : true,
				value : tableNameStore.valueOf().data[0].id,
				onChange : function(value) {
					self.updateCustomArgumentsPane(value, ccp.valueOf().id);

				}
			// regExp: "\\d-\\d"

			}, "tableName");

			var satellite = new dijit.form.FilteringSelect({
				name : "satellite",
				placeHolder : "Select a satellite",
				store : MissionInformationStore,
				searchAttr : "name",
				query : {
					class : "Satellite"
				},
				required : false
			}, "satellite");

			var directionStore = new Memory({
				data : [ {
					name : "UP",
					id : "UP"
				}, {
					name : "DOWN",
					id : "DOWN"
				} ]
			});
			var direction = new FilteringSelect({
				name : "direction",
				placeHolder : "Select a direction",
				store : directionStore,
				searchAttr : "name",
				required : false
			// regExp: "\\d-\\d"

			}, "direction");
			cp.addChild(tableName);
			cp.addChild(start);
			cp.addChild(end);
			cp.addChild(direction);
//			cp.addChild(satellite);

		},

		addAX25Fields : function(cp) {
			var orbitRange = new ValidationTextBox({
				name : "orbitRange",
				value : "",
				placeHolder : "orbit range",
				required : false,
			// regExp: "\\d-\\d"

			}, "orbitRange");

			var subSystemStore = new Memory({
				data : [ {
					name : "EPS",
					id : 00
				}, {
					name : "COM",
					id : 01
				}, {
					name : "CDHS",
					id : 02
				}, {
					name : "ADCS",
					id : 03
				}, {
					name : "PL",
					id : 04
				}, {
					name : "CAM",
					id : 05
				}, {
					name : "GS",
					id : 06
				}, {
					name : "PC",
					id : 07
				}, {
					name : "PC2",
					id : 08
				} ]
			});

			var subSystem = new dijit.form.FilteringSelect({
				name : "subSystem",
				placeHolder : "Select a subsystem",
				searchAttr : "name",
				store : subSystemStore,
				required : false
			}, "subSystem");

			var groundStation = new dijit.form.FilteringSelect({
				name : "groundStation",
				placeHolder : "Select a ground station",
				store : MissionInformationStore,
				searchAttr : "name",
				query : {
					class : "GroundStation"
				},
				required : false
			}, "groundStation");

//			cp.addChild(groundStation);
//			cp.addChild(orbitRange);
			cp.addChild(subSystem);

		},
		addTNCFields : function(cp) {
			var testStore = new Memory({
				data : [ {
					name : "test",
					id : 0
				} ]
			});
			var command = new dijit.form.FilteringSelect({
				name : "command",
				placeHolder : "Select a command",
				searchAttr : "name",
				store : testStore,
				required : false
			}, "command");

//			cp.addChild(command);
		},
		addButton : function(cp) {
			var self = this;
			var submitFilter = new Button({
				class : "queryViewFilter",
				label : "Apply filter"
			});

			on(submitFilter, "click", Lang.hitch(this, function() {

				request.get(config.SUBMIT_URL, {
					query : form.get("value"),
					headers : {
						start : dijit.byId("start").get("value").getTime(),
						end : dijit.byId("end").get("value").getTime()
					},
					handleAs : 'json'
				}).then(function(response) {
					if (response.valueOf() == "") {
						ArchiverStore.data = [];
						self.makeGrid(gridPane);
					}

				});
			}));
			cp.addChild(submitFilter);

		},

		updateCustomArgumentsPane : function(value, name) {
			var self = this;
			var cp = dijit.byId(name);
			cp.destroyDescendants();
			if (value == "AX25") {
				self.addAX25Fields(cp);
			} else if (value == "TNC") {
				self.addTNCFields(cp);
			}

			self.addButton(cp);

		},

		makeGrid : function(gridPane) {
			var provider;
			if (tableName.value == "TNC") {
				provider = new QueryContentProvider({
					columns : {
						id : {
							label : "ID",
							field : "id",
							className : "queryField-small",

						},
						receptionTime : {
							label : "Reception Time",
							field : "receptionTime",
							className : "queryField-large",
						},
						type : {
							label : "Type",
							field : "direction",
							className : "queryField-small"
						},
						satellite : {
							label : "Satellite",
							field : "satellite",
							className : "queryField-medium"
						},
						command : {
							label : "Command",
							field : "command",
							className : "queryField-small"
						},
						data : {

							label : "Data",
							field : "data",
							className : "queryField-data"
						},
						target : {

							label : "Target",
							field : "target",
							className : "queryField-small"
						},
						created : {
							label : "Created",
							field : "created",
							className : "queryField-large"
						}
					},
				});
			}
			else if (tableName.value == "AX25") {
				provider = new QueryContentProvider({
					columns : {
						id : {
							label : "ID",
							field : "id",
							className : "queryField-small",

						},
						receptionTime : {
							label : "Reception Time",
							field : "receptionTime",
							className : "queryField-large",
						},
						type : {
							label : "Type",
							field : "direction",
							className : "queryField-small"
						},
						satellite : {
							label : "Destination",
							field : "destAddr",
							className : "queryField-medium"
						},
						src : {
							label : "Source",
							field : "srcAddr",
							className : "queryField-medium"
						},
						pid : {
							label : "Pid",
							field : "pid",
							className : "queryField-extra-small"
						},
						ctrl : {
							label : "Ctrl",
							field : "ctrl",
							className : "queryField-extra-small"
						},
						fcs : {
							label : "Fcs",
							field : "fcs",
							className : "queryField-extra-small"
						},
						data : {

							label : "Data",
							field : "info",
							className : "queryField-info"
						},
						bitmask : {

							label : "errorBitmask",
							field : "errorBitmask",
							className : "queryField-small"
						},
						created : {
							label : "Created",
							field : "created",
							className : "queryField-large"
						}
					},
				});
			}
			var grid = provider.getContent();
			gridPane.destroyDescendants();
			gridPane.addChild(grid);
			grid.startup();
		},
		constructor : function(args) {
			self = this;
			this.mainContainer = DomConstruct.create("div", {
				style : {
					width : "100%",
					height : "100%"
				}
			});
			var div = DomConstruct.create("div", {}, this.mainContainer);
			form = new Form({
				id : "ParametersForm",
				encType : "multipart/form-data",
				action : "",
				method : "",
				style : {
					width : "100%",
					height : "100%"
				}
			}, div);

			var commonArgumentsPane = new ContentPane({
				region : "left",
				style : {
					width : "100%",
					margin : "0px",
					padding : "0px",
					position : "center"
				}
			});

			var customArgumentsPane = new ContentPane({
				id : "customArgumentsPane",
				region : "center",
				style : {
					width : "100%",
					margin : "0px",
					padding : "0px",
					position : "center"
				}
			});

			gridPane = new ContentPane({
				id : "gridPane",
				region : "center",
				style : {
					width : "100%",
					margin : "0px",
					padding : "0px",
					position : "center",
					height : "90%"
				}
			});

			this.addCommonFields(commonArgumentsPane, customArgumentsPane);
			this.addAX25Fields(customArgumentsPane);
			this.addButton(customArgumentsPane);
			form.domNode.appendChild(commonArgumentsPane.domNode);
			form.domNode.appendChild(customArgumentsPane.domNode);
			form.domNode.appendChild(gridPane.domNode);
			this.bc = new BorderContainer({}, this.mainContainer);
			this.bc.addChild(form);

		}
	});
});
