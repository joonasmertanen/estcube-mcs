define([
    "dojo/dom",
    "dojo/domReady!"
    ],

    function(dom) {
        return {
            routes: {
                QUERY: {
                    path: "system/query",
                    defaults: {
                        controller: "Query/QueryController",
                        method: "index"
                    }
                }
            },
           DataQuery: {
                defaultRangeStart: new Date(Date.now() - 1000 * 60 * 60 * 24 * 7),
                defaultRangeEnd: new Date(Date.now() + 1000 * 60 * 60 * 24)
            },
            SUBMIT_URL:                 "/customQuery"
        };
    }
);
