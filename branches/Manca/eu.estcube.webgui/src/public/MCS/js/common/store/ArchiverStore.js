define([
    "dojo/store/Memory",
    "dojo/store/Observable",
    "config/config",
    "./DataHandler",
    "./StoreMonitor",
    "dojo/_base/array"
    ],

    function (Memory, Observable, Config, DataHandler, StoreMonitor, Arrays) {



        var channel = Config.WEBSOCKET_ARCHIVE;
        var storeId = "ID";
        var store = new Observable(new Memory({ idProperty: storeId }));
        new StoreMonitor({ store: store, storeName: "ArchiveStore" });


        var handler = new DataHandler({ channels: [channel], callback: function (message, channel) {

            if (!(message.value == "KeepAlive")) {
                var array = message.valueOf();
                Arrays.forEach(array, function (entry) {
                   setTimeout(function() {
                        store.put(entry)
                    }, 0);
                });


            }
        }
        });

        return store;
    }

    );