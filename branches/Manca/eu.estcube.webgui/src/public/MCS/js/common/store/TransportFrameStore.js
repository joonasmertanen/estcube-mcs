define([
    "dojo/_base/array",
    "dojo/store/Memory",
    "dojo/store/Observable",
    "dojo/topic",
    "common/messages/SystemMessage",
    "config/config",
    "./DataHandler",
    "./StoreIdGenerator",
    "./StoreLimitHandler",
    "./StoreMonitor",
    ],

    function(Arrays, Memory, Observable, Topic, SystemMessage, Config, DataHandler, IdGenerator, StoreLimitHandler, StoreMonitor) {

        var channel = Config.WEBSOCKET_TRANSPORT;
        var storeId = "storeId";

        var store = new Observable(new Memory({ idProperty: storeId }));
        new StoreMonitor({ store: store, storeName: "TransportFrameStore" });

        // two arrays for storing statistics
        store.groundStationIDs = [];
        store.frameCounts = [];
        
        var handler = new DataHandler({ channels: [channel], callback: function(message, channel) {
            // accept only messages where headers["class"] value is in the list Config.TRANSPORT_FRAME_FILTER
            // discard all others
            if (message.headers && Arrays.indexOf(Config.TRANSPORT_FRAME_FILTER, message.headers["class"]) > -1) {
                message.storeId = dojox.uuid.generateRandomUuid();
                if(message.headers.type === "AX.25" && (typeof message.frame.srcAddr == "undefined" || typeof message.frame.destAddr == "undefined")) {
                    Topic.publish(Config.TOPIC_SYSTEM_MESSAGES, new SystemMessage({
                        value: "Invalid AX.25 frame received. Possible cause: partial TNC frame.",
                        level: "WARN"
                    }));
                    
                }
                
            	// FRAME STATISTICS
            	var i = Arrays.indexOf( store.groundStationIDs, message.headers.groundStationId );
        			
    			// New Ground Station
    			if (i == -1) {
    				i = store.groundStationIDs.length;
    				store.groundStationIDs[i] = message.headers.groundStationId;
    				store.frameCounts[i] = 1;
    			}
    			// Old Ground Station
    			else {
    				store.frameCounts[i]++;
    			}
    			// Puts the frames to the store after the javascript execution stack is ended
    			setTimeout(function() {
        			// Add frame to store
        			StoreLimitHandler.put(store, IdGenerator.generate(message, storeId), Config.STORE_LIMIT_TRANSPORT_FRAMES);
    			}, 0);
            }
        }});

        return store;
    }
);