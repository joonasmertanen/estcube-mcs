package eu.estcube.proxy.model;

public class SrcDestCouplet {

	private String destination;
	private String source;

	public SrcDestCouplet(String destination, String source) {
		this.setDestination(destination);
		this.setSource(source);
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@Override
	public boolean equals(Object arg) {
		SrcDestCouplet couplet = (SrcDestCouplet) arg;
		return couplet.getDestination().equals(getDestination()) && couplet.getSource().equals(getSource());
	}
}
