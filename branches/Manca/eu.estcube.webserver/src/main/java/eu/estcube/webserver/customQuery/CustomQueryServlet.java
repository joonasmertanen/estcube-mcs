package eu.estcube.webserver.customQuery;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.activemq.transport.FutureResponse;
import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.estcube.common.Constants;
import eu.estcube.common.queryParameters.AX25QueryParameters;
import eu.estcube.common.queryParameters.QueryParameters;
import eu.estcube.common.queryParameters.TncQueryParameters;
import eu.estcube.webserver.WebServer;

@Component
public class CustomQueryServlet extends HttpServlet {

    /**
     * 
     */
    Logger log = LoggerFactory.getLogger(CustomQueryServlet.class);
    private static final long serialVersionUID = 1L;

    @EndpointInject(uri = "direct:customQuery")
    private ProducerTemplate producer;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        
        try {
            
            QueryParameters params = null;
            if (req.getParameter("tableName").equals("AX25")) {
                params = new AX25QueryParameters();
                if (req.getHeader("start") != null)
                    params.setStart(new DateTime(Long.valueOf(req.getHeader("start")).longValue()));
                if (req.getHeader("end") != null)
                    params.setEnd(new DateTime(Long.valueOf(req.getHeader("end")).longValue()));
//                params.setSatellite(req.getParameter("satellite"));
                params.setDirection(req.getParameter("direction"));
//                ((AX25QueryParameters) params).setOrbitRange(req.getParameter("orbitRange"));
//                ((AX25QueryParameters) params).setGroundStationVersion(req.getParameter("groundStation"));
                if (!req.getParameter("subSystem").isEmpty())
                    ((AX25QueryParameters) params).setSubsystem(Integer.parseInt(req.getParameter("subSystem")));
            } else if (req.getParameter("tableName").equals("TNC")) {
                params = new TncQueryParameters();
                if (req.getHeader("start") != null)
                    params.setStart(new DateTime(Long.valueOf(req.getHeader("start")).longValue()));
                if (req.getHeader("end") != null)
                    params.setEnd(new DateTime(Long.valueOf(req.getHeader("end")).longValue()));
//                params.setSatellite(req.getParameter("satellite"));
                params.setDirection(req.getParameter("direction"));
//                if (req.getParameter("command") != "")
//                    ((TncQueryParameters) params).setCommand(Integer.parseInt(req.getParameter("command")));
            }

            log.debug("Parameters: " + params.toString());
            resp.setContentType("application/json");
            producer.sendBodyAndHeader(params, "table", req.getParameter("tableName"));
            
           
        } catch (Exception e) {
            if (e.getClass() == IOException.class) {
                log.error("I/O exception: " + e.getMessage());
                e.printStackTrace();
            } else
                log.error(e.toString());
        }
    }
}
