package eu.estcube.webserver.routes;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.common.json.ToJsonProcessor;
import eu.estcube.webserver.cache.Cache;
import eu.estcube.webserver.cache.ArrayCache;
import eu.estcube.webserver.cache.guava.GuavaTimeoutStore;

@Component
public class ArchiverRouteBuilder extends WebsocketWithCacheAndKeepAliveRouteBuilder {

    public static final long CACHE_TIMEOUT_FOR_NAMED_OBJECTS = 1000L * 60 * 60 * 12;
    public static final String WEBSOCKET = "websocket://estcube.out.archive";
   
    @Autowired
    private ToJsonProcessor toJson;
    
    private final Cache<Integer, Object> cache = new ArrayCache<Object>(
            new GuavaTimeoutStore<Integer, Object>(CACHE_TIMEOUT_FOR_NAMED_OBJECTS));
    
    @Override
    protected Cache<?, ?> getCache() {
        return cache;
    }

    @Override
    protected Object getSerializer() {
        return toJson;
    }

    @Override
    protected String getSource() {
       return "activemq:queue:customQueryReturn";
    }

    @Override
    protected String getDestination() {
      return WEBSOCKET;
    }

}
