#!/bin/bash
tabular=false

# function for tabular output
# $1 - service
# $2 - status
# $3 - PID
# $4 - start date & time
function output {
    printf " %-30.30s | %-11.11s | %5s | %-23s \n" "$1" "$2" "$3" "$4"
}

# tabular flag
if [[ $1 = "-t" ]]; then
    tabular=true
    shift
fi

# Check argument
if [[ $# < 1 ]]; then
    printf "Missing service. Usage %s [-t] <service>\n" "$0"
    exit -1
fi

service=$1
shift

if [[ $service = "hamlib.radio" ]]; then
	daemon="rigctld"
fi
if [[ $service = "hamlib.rotator" ]]; then
	daemon="rotctld"
fi

# Check service
dir=$(pwd) 

if [ ! -f "$dir/$service/bin/run" ]; then
    if $tabular; then
        output $service "Not found" "-" "-"
    else
        printf "Service %s not found\n" "$service" 
    fi
    exit -2
fi

if [[ ! -x "$dir/$service/bin/run" ]]; then
    if $tabular; then
        output $service "Non exec" "-" "-"
    else
        printf "Service %s not executable\n" "$service"
    fi
    exit -3
fi

pidFile=$dir/$service/bin/service.pid
pid=$(cat $pidFile 2>/dev/null)

if [[ $? = 0 ]]; then
    if [[ $service = "hamlib.rotator" || $service = "hamlib.radio" ]]; then
	    ps -p $pid -f | grep $daemon >/dev/null 2>&1
    else
      ps -p $pid -f | grep $service >/dev/null 2>&1
    fi
    if [[ $? = 0 ]]; then
        since="$(date -u -r $pidFile +'%Y-%m-%d %H:%M:%S %Z')"
        if $tabular; then
            output $service "Running" $pid "$since"
        else
            printf "Service %s is running. PID: %s. Since: %s\n" "$service" "$pid" "$since"
        fi
        exit 0
    else
        if $tabular; then
            output $service "Not running*" "-" "-" 
        else
            printf "Service %s is not running or PID %s is used by some other process\n" "$service" "$pid"
        fi
        exit -4
    fi
else
    if $tabular; then
        output $service "Not running" "-" "-"
    else
        printf "Service %s is not running\n" "$service"
    fi
    exit 0
fi
exit -5
