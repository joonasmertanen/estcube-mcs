/**
 *
 */
package eu.estcube.gs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

/**
 *
 */
public class GsDriverConfigurationTest {

    private GsDriverConfiguration config;

    @Before
    public void setUp() throws Exception {
        config = new GsDriverConfiguration();
    }

    @Test
    public void testGetSatelliteId() throws Exception {
        testSetSatelliteId();
    }

    @Test
    public void testSetSatelliteId() throws Exception {
        assertNull(config.getSatelliteId());
        config.setSatelliteId("SAT-ID");
        assertEquals("SAT-ID", config.getSatelliteId());
    }
}
