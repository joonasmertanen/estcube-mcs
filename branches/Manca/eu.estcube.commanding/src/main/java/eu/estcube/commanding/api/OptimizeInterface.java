package eu.estcube.commanding.api;

import java.util.List;

import org.hbird.exchange.navigation.PointingData;

public interface OptimizeInterface {

	/**
	 * Optimizes antenna pointing data (azimuth & elevation).
	 * 
	 * Input is "raw" data from the Orekit.
	 * 
	 * @param input {@link List} of unoptimized {@link PointingData} objects
	 * @return list of {@link PointingData} objects with optimized values
	 */
	List<PointingData> optimize(List<PointingData> input);
}
