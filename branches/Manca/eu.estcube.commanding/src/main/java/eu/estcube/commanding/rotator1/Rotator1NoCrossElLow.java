package eu.estcube.commanding.rotator1;

import java.util.List;

import org.hbird.exchange.navigation.PointingData;

import eu.estcube.commanding.api.OptimizeInterface;

/**
 * When north line is not crossed and elevation is low, we are not affecting
 * coordinate list by optimization.
 * 
 * @author Ivar Mahhonin
 * 
 */

public class Rotator1NoCrossElLow implements OptimizeInterface {
	double receivingSector;
	double maxAz;
	double maxEl;

	public Rotator1NoCrossElLow(final double receivingSector,
			final double maxAz, final double maxEl) {
		super();
		this.receivingSector = receivingSector;
		this.maxAz = maxAz;
		this.maxEl = maxEl;

	}

	@Override
	public List<PointingData> optimize(final List<PointingData> coordinates) {
		return coordinates;
	}

}