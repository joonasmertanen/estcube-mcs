package eu.estcube.commanding.calculation;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.hbird.business.groundstation.configuration.RotatorDriverConfiguration;
import org.hbird.exchange.navigation.PointingData;

import eu.estcube.commanding.optimizators.GlobalFunctions;

public class GenerateTestData {
	public static double antennaReceivingSector;

	public static List<PointingData> chooseCase(final String caseN,
			final List<PointingData> coordinates) throws IOException {
		try {
			final String fileName = new String("src/main/resources/cases/"
					+ caseN + ".txt");
			final FileInputStream fstream = new FileInputStream(fileName);
			final DataInputStream in = new DataInputStream(fstream);
			final BufferedReader br = new BufferedReader(new InputStreamReader(
					in));
			String strLine;
			int counter = 1;
			String azimuth;
			String elevation;
			final String space = new String(" ");
			final long ts = 0L;
			while ((strLine = br.readLine()) != null) {

				counter++;
				if ((counter > 9) && (counter < 29)) {
					azimuth = strLine.substring(21, 27);
					elevation = strLine.substring(28, 34);

					if (azimuth.charAt(0) == space.charAt(0)) {
						azimuth = azimuth.substring(1, azimuth.length() - 1);

					}
					if (azimuth.charAt(0) == space.charAt(0)) {
						azimuth = azimuth.substring(1, azimuth.length() - 1);

					}

					coordinates.add(new PointingData(ts, Double
							.parseDouble(azimuth), Double
							.parseDouble(elevation), 123.0, "test", "test"));
				}
			}

			in.close();
		} catch (final Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
		return coordinates;

	}

	public static void main(final String[] args) throws IOException {
		antennaReceivingSector = 7.5;
		List<PointingData> coordinates = new ArrayList<PointingData>();
		coordinates = chooseCase("case 4.1", coordinates);
		GlobalFunctions.printValues(coordinates);

		final RotatorOptimizer optimizer = new RotatorOptimizer();
		final RotatorDriverConfiguration config = new RotatorDriverConfiguration();
		config.setMaxAzimuth(450D);
		config.setMaxElevation(180D);
		config.setDeviceName("dummy");

		// coordinates=GlobalFunctions.listFinalCheck(coordinates);
		coordinates = optimizer.doOptimize(coordinates, config);
		GlobalFunctions.printValues(coordinates);

	}
}
