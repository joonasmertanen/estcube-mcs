package eu.estcube.common.queryParameters;


public class AX25QueryParameters extends QueryParameters {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String orbitRange;
    protected String groundStationVersion;
    private int subsystem = Integer.MIN_VALUE;

    public int getSubsystem() {
        return subsystem;
    }

    public void setSubsystem(int subsystem) {
        this.subsystem = subsystem;
    }

    public void setOrbitRange(String parameter) {
        this.orbitRange = parameter;
    }

    public static String getTable() {
        return "MCS.AX25_FRAMES";
    }

    public static String getDetailsTable() {
        return "MCS.AX25_FRAME_DETAILS";
    }

    public static String getTimestampFunction() {
        return "SYSDATE";
    }

    public String getGroundStationVersion() {
        return groundStationVersion;
    }

    public void setGroundStationVersion(String groundStationVersion) {
        this.groundStationVersion = groundStationVersion;
    }

    @Override
    public String toString() {
        return "AX25QueryParameters [orbitRange=" + orbitRange + ", subsystem=" + subsystem + ", start=" + start
                + ", end=" + end + ", satelliteName=" + satelliteName + ", groundStationVersion="
                + groundStationVersion + ", direction=" + direction + "]";
    }
}
