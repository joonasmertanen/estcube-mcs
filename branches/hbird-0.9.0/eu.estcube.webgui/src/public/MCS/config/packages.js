var _siteRoot = "/MCS/";
var packages = [

    // dependencies
    { name: "dgrid",                    location: "/scripts/dgrid-0.3.6" },
    { name: "put-selector",             location: "/scripts/put-selector-0.3.2" },
    { name: "xstyle",                   location: "/scripts/xstyle-0.0.5" },
    { name: "ThreeJS",                  location: "/scripts/threejs-r58" },

    // common packages
    { name: "config",                   location: _siteRoot + "config" },
    { name: "common",                   location: _siteRoot + "js/common" },

    // System packages
    { name: "SystemComponents",         location: _siteRoot + "js/system/components", isModule: true },
    { name: "SystemLog",                location: _siteRoot + "js/system/log", isModule: true },
    { name: "Logout",                   location: _siteRoot + "js/system/logout", isModule: true },
    { name: "Dashboard",                location: _siteRoot + "js/system/dashboard", isModule: true },
    { name: "Demo",                     location: _siteRoot + "js/system/demo", isModule: true },
    { name: "Diagnostics",              location: _siteRoot + "js/system/diagnostics", isModule: true },
    { name: "Map",                      location: _siteRoot + "js/system/map", isModule: true},
    { name: "Messages",                 location: _siteRoot + "js/system/messages", isModule: true },
    { name: "Debug",                    location: _siteRoot + "js/system/debug", isModule: true },
    { name: "Contacts",                 location: _siteRoot + "js/system/contacts", isModule: true },
    { name: "SkyAtGlance",              location: _siteRoot + "js/system/skyatglance", isModule: true },

    // ESTCube-1 packages
    { name: "ESTCube-1.tle",            location: _siteRoot + "js/satellite/ESTCube-1/tle", isModule: true },
    { name: "ESTCube-1.beacon",         location: _siteRoot + "js/satellite/ESTCube-1/beacon", isModule: true },
    { name: "ESTCube-1.AX25",           location: _siteRoot + "js/satellite/ESTCube-1/ax25", isModule: true},
    { name: "ESTCube-1.commanding",     location: _siteRoot + "js/satellite/ESTCube-1/commanding", isModule: true },
    { name: "ESTCube-1.telemetry",       location: _siteRoot + "js/satellite/ESTCube-1/telemetry", isModule: true },
    { name: "ESTCube-1.beacons",        location: _siteRoot + "js/satellite/ESTCube-1/beacons", isModule: true},
    { name: "ESTCube-1.addbeacons",		location: _siteRoot + "js/satellite/ESTCube-1/addbeacons", isModule: true },
    { name: "ESTCube-1.dashboard",	location: _siteRoot + "js/satellite/ESTCube-1/dashboard", isModule: true },

    // ES5EC packages
    { name: "ES5ECAntenna",             location: _siteRoot + "js/groundstation/ES5EC/antenna", isModule: true },
    { name: "ES5ECTnc",                 location: _siteRoot + "js/groundstation/ES5EC/tnc", isModule: true },
    { name: "ES5ECWebCam",              location: _siteRoot + "js/groundstation/ES5EC/webcam", isModule: true },
    { name: "ES5ECWeather",             location: _siteRoot + "js/groundstation/ES5EC/weather", isModule: true },
    
    //Settings packages
    { name: "Audio",           location: _siteRoot + "js/settings/audio", isModule: true },
];
