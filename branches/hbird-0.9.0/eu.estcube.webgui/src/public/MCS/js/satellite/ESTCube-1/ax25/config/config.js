define([
    "config/config",
    "dojo/domReady!"
    ],

    function(ready) {
        return {
            routes: {
                ESTCube1_AX25: {
                    path: "ESTCube-1/ax25",
                    defaults: {
                        controller: "ESTCube-1.AX25/AX25Controller",
                        method: "index",
                    },
                    roles: config.ROLES_CAN_COMMAND
                },
            },

            ESTCube1_AX25: {
                defaults: {
                    destAddr: "8A A6 6A 8A 40 40 76",
                    sourceAddr: "8A A6 6A 8A 86 40 62",
                    ctrl: "03",
                    pid: "0F",
                    port: 0
                },

                constraints: {
                    destAddr: { length: 7 },
                    sourceAddr: { length: 7 },
                    ctrl: { length: 1},
                    pid: { length: 1},
                    port: { min: 0, max: 15 },
                    info: { minLength: 1, maxLength: 239 }
                },

                SUBMIT_URL : "/ax25/submit"
            }
        };
    }
);
