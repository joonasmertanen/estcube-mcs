define([
        "dojo/_base/declare", 
        "dojo/dom-class", 
        "dojo/dom-construct",
        "config/config",
        "common/formatter/DateFormatter",
        "common/formatter/NameFromIdFormatter", 
        "common/store/BusinessCardStore", 
        "common/store/PageStore", 
        "common/store/WebCamStore", 
        "common/store/ParameterStore", 
        "common/store/MissionInformationStore", 
        "common/display/Dashboard", 
        "common/display/GenericContentProvider", 
        "common/display/GridContentProvider", 
        "common/display/ListContentProvider", 
        "common/display/CompositeContentProvider", 
        "common/display/GaugeContentProvider", 
        "common/display/DGridTooltipSupport", 
        "common/display/AX25UIFramesContentProvider", 
        "common/display/WebCamContentProvider", 
        "common/display/CommandsContentProvider", 
        "common/display/TelemetryContentProvider", 
        "dojo/dom-style",
        ], 
        function(declare, DomClass, DomConstruct, Config, DateFormatter, NameFromIdFormatter, BusinessCardStore, PageStore, WebCamStore, ParameterStore, MissionInformationStore, Dashboard, GenericContentProvider, GridContentProvider, ListContentProvider, CompositeContentProvider, GaugeContentProvider, DGridTooltipSupport, AX25UIFramesContentProvider, WebCamContentProvider, CommandsContentProvider, TelemetryContentProvider, domStyle) {
            return declare([], {
                constructor: function(args) {
                    var config = [{
                        title: "Send Commands",
                        contentProvider: new CommandsContentProvider(),
                        col: 0,
                        row: 0,            
                        id: "sendCommandsViewPortlet",
                    }, {
                        title: "ES5EC WebCam",
                        contentProvider: new WebCamContentProvider({
                            store: WebCamStore,
                            imageId: Config.ESTCube1_DASHBOARD.imageId,
                            initialImage: Config.ESTCube1_DASHBOARD.initialImage,
                            width: "100%"
                            
                        }),
                        col: 0,
                        row: 1,
                        open: false,
                        id: "webCamViewPortlet",
                    }, 
                    {
                        title: "Telemetry",
                        contentProvider: new TelemetryContentProvider({id:"fillTelemetryContentProvider",name:"Telemetry"}),
                        col: 2    ,
                        row: 1,             
                        id: "telemetryViewPortlet",
                    }, 
                    
                   {
                        title: "EPS info",
                        contentProvider: new TelemetryContentProvider({
                            id:"fillEpsContentProvider",
                            filter:["mode","resetcount","version","bdstime","counter","bat_a","bat_b","mpb_ext1280","ctl_adcs_cs","ctl_cam_3v3_cs","ctl_cdhs_a_cs","ctl_cdhs_b_cs","ctl_cdhs_bsw_cs","ctl_com_cs","ctl_com_cs","ctl_pl_3v3_cs","ctl_pl_5v_cs","ctl_pl_12v_cs"],
                            subSystem:["EPS"],
                            name:"EPS"}
                            ),                        
                        col: 1,
                        row: 0,             
                        id: "EpsInfoViewPortlet",
                    },     
                    
                   {
                        title: "CDHS",
                        contentProvider: new TelemetryContentProvider({
                            id:"fillCdhsContentProvider",
                            filter:["timestamp","version","errors","resets","heap","cmds","packets","core_temp","rtc_temp","fw_id","num_resets","num_errors","num_cmds","vref","mcu_temp","rtc_temp"],
                            subSystem:["CDHS"],
                            name:"CDHS"}
                            ),                        
                        col: 1,
                        row: 1,             
                        id: "CdhsInfoViewPortlet",
                    }, 
 
                    {
                        title: "TNC     (uplink = black, downlink = blue)",
                        contentProvider: new AX25UIFramesContentProvider(),
                        col: 2,
                        row: 0,
                        id: "tncViewPortlet",
                    }, ];                 
                    this.dashboard = new Dashboard({
                        config: config,
                        columns: Config.DASHBOARD.numberOfColumns,
                    });
                    DomClass.add(this.dashboard.getContainer().domNode, "fill");
                },
                placeAt: function(container) {
                    this.dashboard.getContainer().placeAt(container);
                },
            });
        });