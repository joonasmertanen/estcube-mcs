define(["dojo/_base/declare", 
        "dojo/_base/lang", 
        "dojo/dom-construct",
        "dojo/dom-class", 
        "dgrid/OnDemandGrid", 
        "config/config",
        "common/formatter/DateFormatter", 
        "common/store/ParameterStore", 
        "dijit/layout/TabContainer", 
        "dijit/layout/ContentPane",
        "common/display/TelemetryContentProvider", 
         ],

    function(declare, Lang, DomConstruct, DomClass, Grid, config, DateFormatter,ParameterStore,TabContainer,ContentPane,TelemetryContentProvider) {   
        return declare([], {    
            constructor : function(args) {
                 this.tabContainer = new TabContainer({
                    "class": "fill"
                });                
                var telemetryProvider=new TelemetryContentProvider({
                    columns :{                         
                    timestamp: { label: "Timestamp", formatter: DateFormatter, className: "field-timestamp", },
                    description: { label: "ID", className: "field-name", },
                    description: { label: "Name", className: "field-description", },
                    value: { label: "Value", className: "field-value", },
                    unit:{label:"Unit",className: "field-unit"},
                    issuedBy: { label: "Issued by", className: "field-issuedBy", },  
                    }
                    }
                );
                               
                var CDHS=new TelemetryContentProvider({id:"fillCdhsContentProvider",subSystem:["CDHS"],name:"CDHS"});
                var EPS=new TelemetryContentProvider({id:"fillCdhsContentProvider",subSystem:["EPS"],name:"EPS"});
                var COM=new TelemetryContentProvider({id:"fillCdhsContentProvider",subSystem:["COM"],name:"COM"});
                var CAM=new TelemetryContentProvider({id:"fillCdhsContentProvider",subSystem:["CAM"],name:"CAM"});
                var TCS=new TelemetryContentProvider({id:"fillCdhsContentProvider",subSystem:["EPS","CDHS"],filter:["internal_raw","rtc","internal","core_temp","rtc_temp","bat_temp_b","bat_temp_a"],
                name:"TCS"});
                var Payload=new TelemetryContentProvider({id:"fillCdhsContentProvider",subSystem:["CDHS"],filter:["egun","dummy","timestamp","tether","anode","rtotal","rtime","hivolt","rfeg","rspeed"],
                name:"PL"});
                 var ADCS=new TelemetryContentProvider({id:"fillCdhsContentProvider",subSystem:["CDHS"],
                name:"ADCS"});
   
                this.tabContainer.addChild(this.prepareGrid(telemetryProvider.getContent(), "Telemetry"));
                this.tabContainer.addChild(this.prepareGrid(EPS.getContent(), "EPS"));             
                this.tabContainer.addChild(this.prepareGrid(CDHS.getContent(), "CDHS"));
                this.tabContainer.addChild(this.prepareGrid(COM.getContent(), "COM"));
                this.tabContainer.addChild(this.prepareGrid(CAM.getContent(), "CAM"));
                this.tabContainer.addChild(this.prepareGrid(TCS.getContent(), "TCS"));
                this.tabContainer.addChild(this.prepareGrid(Payload.getContent(), "PL"));       
                this.tabContainer.addChild(this.prepareGrid(ADCS.getContent(), "ADCS")); 
                
                                                               
            },   
            placeAt : function(container) {
                 this.tabContainer.placeAt(container);
                 this.tabContainer.startup();
            },            
            prepareGrid: function (grid, title) {
                DomClass.add(grid.domNode, "fill");
                var gridPlaceholder = DomConstruct.create("div", {
                    "class": "fill"
                });
                DomConstruct.place(grid.domNode, gridPlaceholder);
                var contentPane = new ContentPane({
                    title: title,
                    content: gridPlaceholder
                });
                // XXX - 14.03.2013, kimmell - fix for dgrid header
                // not visible bug
                contentPane.on("show", function () {
                    grid.set("showHeader", true);
                });
                return contentPane;
            },
    
        });
    });
