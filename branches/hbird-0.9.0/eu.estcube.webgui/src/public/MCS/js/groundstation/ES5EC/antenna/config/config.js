define([
    "dojo/domReady!"
    ],

    function(ready) {

        return {
            routes: {
                ANTENNA: {
                    path: "ES5EC/antenna",
                    defaults: {
                        controller: "ES5ECAntenna/AntennaController",
                        method: "index",
                    }
                },
            },

            ANTENNA: {
              canvasWidth: 640,
              canvasHeight: 480
            }

        };
    }
);