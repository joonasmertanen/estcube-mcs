define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dojo/dom-class",
    "config/config",
    "common/formatter/DateFormatter",
    "common/store/MissionInformationStore",
    "common/store/ResolveById",
    "common/display/ContactContentProvider",
    "common/formatter/TimeDiffFormatter",
    "common/formatter/DoubleFormatter",
    "common/TimeFactory",
    ],

    function(declare, Lang, DomConstruct, DomClass, config, DateFormatter, MissionInformationStore, ResolveById, ContactContentProvider, TimeDiffFormatter, DoubleFormatter, TimeFactory) {

        return declare([], {

            constructor: function(args) {
                var provider = new ContactContentProvider({
                    columns: {
                        groundStationId: {
                            label: "Ground Station",
                            className: "field-groundStation",
                            renderCell: function(object, value, node, options) {
                                ResolveById(value, node, provider.lookupStore);
                            },
                        },
                        satelliteId: {
                            label: "Satellite",
                            className: "field-satellite",
                            renderCell: function(object, value, node, options) {
                                ResolveById(value, node, provider.lookupStore);
                            },
                        },
                        orbitNumber: { label: "Orbit", className: "field-unit", },
                        startTime: { label: "Start Time", className: "field-timestamp", formatter: DateFormatter },
                        endTime: { label: "End Time", className: "field-timestamp", formatter: DateFormatter },
                        duration: {
                            label: "Duration",
                            className: "field-duration",
                            renderCell: function(object, value, node, options) {
                                node.innerHTML = TimeDiffFormatter(object.startTime, object.endTime);
                            },
                        },
                        elevation: {
                            label: "Max Elevation",
                            className: "field-short-numeric",
                            formatter: function(elevation) {
                                return DoubleFormatter(elevation.max);
                            }
                        },
                        azimuthStart: {
                            label: "Start Azimuth",
                            className: "field-short-numeric",
                            renderCell: function(object, value, node, options) {
                                node.innerHTML = DoubleFormatter(object.azimuth.start);
                            },
                        },
                        azimuthEnd: {
                            label: "End Azimuth",
                            className: "field-short-numeric",
                            renderCell: function(object, value, node, options) {
                                node.innerHTML = DoubleFormatter(object.azimuth.end);
                            },
                        },
                        inSunLigth: { label: "In Sun Light", className: "field-unit" },
                        derivedFromId: { label: "Source", className: "field-name" },
                    },
                });
                this.grid = provider.getContent();
                DomClass.add(this.grid.domNode, "fill");

                var gridR = this.grid;
                var counter = 0;
                TimeFactory.addListener (['aosLosInterval'], this, function( eventName, eventTime ) {
                    if (eventTime >= 0) {

                        var eventTimeS = Math.round(eventTime/1000.0);
                        if((counter-eventTimeS) <0){
                            gridR.refresh();
                        }
                        counter = eventTimeS;
                    }
                });

                // XXX - workaround for grid titles not visible bug
                setTimeout(Lang.hitch(this, function() { this.grid.set("showHeader", true) }), 500);
            },

            placeAt: function(container) {
                DomConstruct.place(this.grid.domNode, container)
            },

        });
    }
);
