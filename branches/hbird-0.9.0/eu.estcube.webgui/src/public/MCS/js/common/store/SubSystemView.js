define([],

    function () {
        return function (message, systemView) {
            //for all message classes, that are not belonging to the Metadata class
            if (/\/ESTCUBE\/Satellites\/ESTCube-1\/.*/.test(message.ID) && message["class"] != "Metadata") {
                //if telemetry view has no filter or subSystem ID defined, 
                //then it will show all the data from the ParameterStore               
                if ((systemView.filter == undefined) && (systemView.subSystem == undefined)) {
                    return true;
                } else {
                    var subSystem = systemView.subSystem;
                    var patt = new RegExp(subSystem, "g");
                    for (var i = 0; i < subSystem.length; i++) {
                        var subRegEx = new RegExp(subSystem[i], "g");
                        //if only sub system id is defined, then table will show all the messages containing the sub system name
                        //in their ID's
                        if ((subSystem != undefined) && (systemView.filter == undefined)) {                           
                            if (subRegEx.test(message.ID)) {
                                return true;
                            }
                            //if sub system and messages special id's are provided, then table will show messages with 
                            //exact name and with exact sub system ID.
                        } else if ((systemView.subSystem != undefined) && (systemView.filter != undefined)) {
                            if ((subRegEx.test(message.ID)) && (systemView.filter.indexOf(message.name) != -1)) {
                                return true;
                            }

                        }

                    }
                }

            }
        }

    });