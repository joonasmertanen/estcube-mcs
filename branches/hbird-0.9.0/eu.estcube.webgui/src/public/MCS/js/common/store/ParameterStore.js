define([
    "dojo/store/Memory",
    "dojo/store/Observable",
    "config/config",
    "./DataHandler",
    "./StoreMonitor",
    ],

    function(Memory, Observable, Config, DataHandler, StoreMonitor) {

        var channel = Config.WEBSOCKET_PARAMETERS;
        var storeId = "ID";

        var store = new Observable(new Memory({ idProperty: storeId }));
        new StoreMonitor({ store: store, storeName: "ParameterStore" });

        var handler = new DataHandler({ channels: [channel], callback: function(message, channel) {
            var oldValue = store.get(message[storeId]);
            if (oldValue == null) {
                // there is no value for the id in the store; add new value
                store.put(message);
            } else if (oldValue.timestamp < message.timestamp) {
                // existing value has older timestamp than message; update
                store.put(message);
            }
            // else message's timestamp is older than stored value timestamp; ignore
        }});

        return store;
    }
);