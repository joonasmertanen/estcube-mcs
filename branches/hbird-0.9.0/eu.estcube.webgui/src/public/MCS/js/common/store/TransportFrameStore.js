define([
    "dojo/_base/array",
    "dojo/store/Memory",
    "dojo/store/Observable",
    "dojo/topic",
    "common/messages/SystemMessage",
    "config/config",
    "./DataHandler",
    "./StoreIdGenerator",
    "./StoreLimitHandler",
    "./StoreMonitor",
    ],

    function(Arrays, Memory, Observable, Topic, SystemMessage, Config, DataHandler, IdGenerator, StoreLimitHandler, StoreMonitor) {

        var channel = Config.WEBSOCKET_TRANSPORT;
        var storeId = "storeId";

        var store = new Observable(new Memory({ idProperty: storeId }));
        new StoreMonitor({ store: store, storeName: "TransportFrameStore" });

        var handler = new DataHandler({ channels: [channel], callback: function(message, channel) {
            // accept only messages where headers["class"] value is in the list Config.TRANSPORT_FRAME_FILTER
            // discard all others
            if (message.headers && Arrays.indexOf(Config.TRANSPORT_FRAME_FILTER, message.headers["class"]) > -1) {
                message.storeId = dojox.uuid.generateRandomUuid();
                if(message.headers.type === "AX.25" && (typeof message.frame.srcAddr == "undefined" || typeof message.frame.destAddr == "undefined")) {
                    Topic.publish(Config.TOPIC_SYSTEM_MESSAGES, new SystemMessage({
                        value: "Invalid AX.25 frame received. Possible cause: partial TNC frame.",
                        level: "WARN"
                    }));
                    
                    console.log(message);
                }
                
                StoreLimitHandler.put(store, IdGenerator.generate(message, storeId), Config.STORE_LIMIT_TRANSPORT_FRAMES);
            }
        }});

        return store;
    }
);