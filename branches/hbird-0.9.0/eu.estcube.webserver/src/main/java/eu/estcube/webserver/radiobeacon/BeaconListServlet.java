package eu.estcube.webserver.radiobeacon;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hbird.business.archive.api.Catalogue;
import org.hbird.business.archive.api.DataAccess;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.core.Label;
import org.hbird.exchange.dataaccess.DataRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.estcube.common.hbird.HbirdApiHelper;
import eu.estcube.common.json.ToJsonProcessor;
import eu.estcube.webserver.utils.HttpResponseSupport;

@Component
public class BeaconListServlet extends HttpServlet {
    private static final long serialVersionUID = -2807674879565161210L;
    private static final Logger LOG = LoggerFactory.getLogger(BeaconListServlet.class);

    @Autowired
    private ToJsonProcessor toJson;

    @Autowired
    private HttpResponseSupport responseSupport;

    @Value("${service.id}")
    private String serviceId;

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        Catalogue catalogue = new Catalogue(serviceId);
        DataAccess dao = new DataAccess(serviceId);

        DataRequest request = new DataRequest("webserver");
        request.setClass(Label.class.getSimpleName());
        request.setSort(StandardArguments.TIMESTAMP);
        request.setEntityID("/ESTCUBE/Satellites/ESTCube-1/beacon/raw");

        try {
            responseSupport.sendAsJson(resp, toJson, dao.getData(request));
        } catch (Exception e) {
            String message = "Failed to process beacon list request";

            LOG.error(message, e);

            throw new ServletException(message, e);
        } finally {
            HbirdApiHelper.dispose(catalogue);
            HbirdApiHelper.dispose(dao);
        }
    }
}
