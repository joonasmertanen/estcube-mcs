/** 
 *
 */
package eu.estcube.webserver.catalogue;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hbird.business.archive.api.DataAccess;
import org.hbird.exchange.navigation.LocationContactEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.estcube.common.hbird.HbirdApiHelper;
import eu.estcube.common.json.ToJsonProcessor;
import eu.estcube.webserver.utils.HttpResponseSupport;

/**
 *
 */
@Component
public class SatellitesServlet extends HttpServlet {

    private static final long serialVersionUID = -2655050951676720703L;

    private static final Logger LOG = LoggerFactory.getLogger(SatellitesServlet.class);

    @Autowired
    private ToJsonProcessor toJson;

    @Autowired
    private HttpResponseSupport responseSupport;

    @Value("${service.id}")
    private String serviceId;

    /** @{inheritDoc . */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DataAccess dao = new DataAccess(serviceId);
        try {
            // LOG.debug("Request pathInfo: {}", req.getPathInfo());
            // LOG.debug("Request pathTranslated: {}", req.getPathTranslated());
            // LOG.debug("Request requestURI {}", req.getRequestURI());
            // LOG.debug("Request requestURL {}", req.getRequestURL());

            long now = System.currentTimeMillis();
            long delta = 1000L * 60 * 60 * 24 * 7; // one week

            LocationContactEvent event = dao.getNextLocationContactEventForGroundStation("ES5EC", now - delta);
            responseSupport.sendAsJson(resp, toJson, event);

            // List<OrbitalState> list = dao.retrieveOrbitalStatesFor("RAX-2",
            // now - delta, now + delta);
            // responseSupport.sendAsJson(resp, toJson, list);

            // ICatalogue catalogue = ApiFactory.getCatalogueApi("webserver");
            // // List<Command> commands = catalogue.getCommands();
            // // responseSupport.sendAsJson(resp, toJson, commands);
            //
            // List<Satellite> satellites = catalogue.getSatellites();
            // responseSupport.sendAsJson(resp, toJson, satellites);
        } catch (Exception e) {
            String message = "Failed to process Satellites request";
            LOG.error(message, e);
            throw new ServletException(message, e);
        } finally {
            HbirdApiHelper.dispose(dao);
        }
    }
}
