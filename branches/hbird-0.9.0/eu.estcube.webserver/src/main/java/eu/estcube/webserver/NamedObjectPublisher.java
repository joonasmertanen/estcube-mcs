package eu.estcube.webserver;

import java.util.UUID;

import org.apache.camel.Body;
import org.hbird.business.archive.api.Publish;
import org.hbird.exchange.core.EntityInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.estcube.common.hbird.HbirdApiHelper;

/**
 *
 */
@Component
public class NamedObjectPublisher {

    private static final Logger LOG = LoggerFactory.getLogger(NamedObjectPublisher.class);

    @Value("${service.id}")
    private String serviceId;

    public EntityInstance publishAndCommit(@Body EntityInstance entityInstance) {
        Publish publisher = new Publish(serviceId);
        try {
            publisher.publish(entityInstance);
            // XXX - 11.07.2013; kimmell - have to provide ID for the commit
            // command here :|
            // has to be fixed in Hummingbird
            publisher.commit(UUID.randomUUID().toString());
        } catch (Exception e) {
            LOG.error("Failed to publish {}", entityInstance.toString(), e);
        } finally {
            HbirdApiHelper.dispose(publisher);
        }
        return entityInstance;
    }
}
