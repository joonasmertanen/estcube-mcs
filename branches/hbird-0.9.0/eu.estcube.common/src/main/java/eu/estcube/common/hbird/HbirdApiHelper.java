package eu.estcube.common.hbird;

import org.hbird.business.api.HbirdApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Has to be removed once Hbird data access is changed. Probably in version
 * 0.10.0.
 * 
 * @deprecated
 */
// TODO - 10.07.2013; kimmell - remove once Hbird data access is changed
@Deprecated
public class HbirdApiHelper {

    private static final Logger LOG = LoggerFactory.getLogger(HbirdApiHelper.class);

    public static void dispose(HbirdApi api) {
        String apiClass = api.getClass().getName();
        try {
            LOG.debug("Disposing {}", apiClass);
            api.getContext().stop();
            LOG.debug("Stopped {}", apiClass);
        } catch (Exception e) {
            LOG.error("Failed to stop {}", apiClass, e);
        }
    }
}
