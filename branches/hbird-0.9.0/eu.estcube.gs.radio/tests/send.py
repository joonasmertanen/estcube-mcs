import time
import sys
import logging
import stomp

class MyListener(object):
    def on_error(self, headers, message):
        print('received an error %s' % message)

    def on_message(self, headers, message):
        print('received a message %s' % message)

logging.basicConfig()
conn = stomp.Connection([('localhost', 61613)])
conn.set_listener('somename', MyListener())
conn.start()

if __name__ == '__main__':
    conn.connect()
    headers = {
             'componentId':'RADIO_ES5EC_dummy',
             'type':'textMessage',
             'MessageNumber':78           
            }
    conn.send(message=' '.join(sys.argv[1:]), destination='/topic/gsReceive', 
        headers=headers, ack="auto")
    conn.disconnect()
