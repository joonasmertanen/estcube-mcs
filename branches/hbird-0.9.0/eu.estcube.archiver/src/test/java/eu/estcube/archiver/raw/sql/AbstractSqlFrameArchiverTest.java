package eu.estcube.archiver.raw.sql;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

public class AbstractSqlFrameArchiverTest {
	private AbstractSqlFrameArchiver abstractSqlFrameArchiver;
	private DataSource dataSource;
	private Connection connection;

	@Before
	public void setUp() throws Exception {
		dataSource = Mockito.mock(DataSource.class);
		connection = Mockito.mock(Connection.class);

		abstractSqlFrameArchiver = new AbstractSqlFrameArchiver(dataSource) {

			@Override
			public String getTimestampFunction() {
				return "NOW";
			}

			@Override
			public String getTable() {
				return "TABLE";
			}

			@Override
			public String getDetailsTable() {
				return "DETAILS_TABLE";
			}

			@Override
			public String getDetailsAutoIncrementFunction() {
				return "AUTO_INCR";
			}

			@Override
			public String[] getColumns() {
				return new String[] { "A", "B" };
			}

			@Override
			public String getAutoIncrementFunction() {
				return "SEQ.NEXTVAL";
			}
		};

	}

	@Test
	public void testGetDataSource() {
		assertSame(dataSource, abstractSqlFrameArchiver.getDataSource());
	}

	@Test
	public void testCreateInsert() {
		String ins = abstractSqlFrameArchiver
				.createInsert("XTABLE", "YID", "DIR", "ZCREATED", new String[] {
						"A", "B" }, "AUTOINCR", "NOW");

		assertEquals(
				"INSERT INTO XTABLE (YID, DIR, A, B, ZCREATED) VALUES (AUTOINCR, ?, ?, ?, NOW)",
				ins);
	}

	@Test
	public void testCreateInsertDetails() {
		String ins = abstractSqlFrameArchiver.createInsertDetails("XTABLE",
				"YID", "MASTER_ID", "ZCREATED", "NAMEX", "VALUEY", "AUTOINCR",
				"NOW");

		assertEquals(
				"INSERT INTO XTABLE (YID, NAMEX, VALUEY, MASTER_ID, ZCREATED) VALUES (AUTOINCR, ?, ?, ?, NOW)",
				ins);
	}

	@Test
	public void testStore() {
		// TODO: Write test if you can
	}

	@Test
	public void testGetInsertStatement() throws SQLException {
		PreparedStatement ps1;
		PreparedStatement ps2;

		Mockito.when(
				connection.prepareStatement(Mockito.anyString(),
						Mockito.any(String[].class))).thenReturn(
				Mockito.mock(PreparedStatement.class));

		InOrder io = Mockito.inOrder(connection);

		ps1 = abstractSqlFrameArchiver.getInsertStatement(connection);
		ps2 = abstractSqlFrameArchiver.getInsertStatement(connection);

		io.verify(connection).prepareStatement(Mockito.anyString(),
				Mockito.any(String[].class));
		io.verifyNoMoreInteractions();

		assertSame(ps1, ps2);

	}

	@Test
	public void testGetInsertDetailsStatement() throws SQLException {
		PreparedStatement ps1;
		PreparedStatement ps2;

		Mockito.when(connection.prepareStatement(Mockito.anyString()))
				.thenReturn(Mockito.mock(PreparedStatement.class));

		InOrder io = Mockito.inOrder(connection);

		ps1 = abstractSqlFrameArchiver.getInsertDetailsStatement(connection);
		ps2 = abstractSqlFrameArchiver.getInsertDetailsStatement(connection);

		io.verify(connection).prepareStatement(Mockito.anyString());
		io.verifyNoMoreInteractions();

		assertSame(ps1, ps2);
	}

	@Test
	public void testInsertMaster() {

	}

	@Test
	public void testInsertDetails() {
	}

}
