package eu.estcube.templating.placeholders;

public enum Placeholder {

    ROTATOR_ID("rot_id"),
    ROTATOR_AZIMUTH("azimuth"),
    ROTATOR_ELEVATION("elevation"),
    COMMAND_TARGET_ID("target_id"),
    COMMAND_TIMESTAMP("timestamp"),
    COMMAND_HEADER_TIMESTAMP("start_timestamp"),
    COMMAND_TAIL_TIMESTAMP("end_timestamp"),
    COMMAND_VALUE("value"),
    RADIO_FREQUENCY("frequency"),
    RADIO_PASSBAND("passband"),
    RADIO_MODE("mode"),
    ISSUED_BY("issued_by");

    private final String name;

    private Placeholder(String name) {
        this.name = name;
    }

    /**
     * Regex matching the placeholder strings. Placeholders look like that:
     * "${placeholder_name}", where the {@code placeholder_name} can only
     * include lowercase letters and underscores '_' and must be at least 1
     * character long.
     */
    public static final String PLACEHOLDER_REGEX = "\\$\\{([a-z_]+)\\}";

    /**
     * The capturing group for the {@code placeholder_name}
     */
    public static final int PLACEHOLDER_REGEX_NAME_GROUP = 1;

    /**
     * Finds an enum for the given placeholder
     * 
     * @param name The placeholder's name
     * @return The StringPlaceholder enum
     * @throws PlaceholderException If no such placeholder is defined
     */
    public static Placeholder fromName(String name) throws PlaceholderException {
        for (Placeholder placeholder : Placeholder.values()) {
            if (placeholder.toString().equals(name)) {
                return placeholder;
            }
        }
        throw new PlaceholderException("No placeholder defined for \"" + name + "\"");
    }

    public IPlaceholder<?> toIPlaceholder() throws PlaceholderException {
        IPlaceholder<?> result = null;

        switch (this) {
            case COMMAND_TARGET_ID:
                result = IntegerPlaceholder.COMMAND_TARGET_ID;
                break;
            case RADIO_FREQUENCY:
                result = IntegerPlaceholder.RADIO_FREQUENCY;
                break;
            case RADIO_PASSBAND:
                result = IntegerPlaceholder.RADIO_PASSBAND;
                break;
            case ROTATOR_ID:
                result = IntegerPlaceholder.ROTATOR_ID;
                break;
            case COMMAND_TIMESTAMP:
                result = LongPlaceholder.COMMAND_TIMESTAMP;
                break;
            case ROTATOR_AZIMUTH:
                result = DoublePlaceholder.ROTATOR_AZIMUTH;
                break;
            case ROTATOR_ELEVATION:
                result = DoublePlaceholder.ROTATOR_ELEVATION;
                break;
            case COMMAND_VALUE:
                result = StringPlaceholder.COMMAND_VALUE;
                break;
            case RADIO_MODE:
                result = StringPlaceholder.RADIO_MODE;
                break;
            case ISSUED_BY:
                result = StringPlaceholder.ISSUED_BY;
                break;
            case COMMAND_TAIL_TIMESTAMP:
                result = LongPlaceholder.COMMAND_TAIL_TIMESTAMP;
                break;
            case COMMAND_HEADER_TIMESTAMP:
                result = LongPlaceholder.COMMAND_HEADER_TIMESTAMP;
                break;
        }

        // Don't add as a default case, because this way a compiler warning will
        // still be given if a switch statement is missing
        if (result == null) {
            throw new PlaceholderException("Placeholder\"" + this + "\" not defined for IPlaceholders.");
        }

        return result;
    }

    @Override
    public String toString() {
        return name;
    }

}
