package eu.estcube.templating.beans;

import java.util.Collection;

import org.hbird.exchange.core.CommandBase;

public class Contact {
    private final Collection<CommandBase> header;
    private final Collection<CommandBase> body;
    private final Collection<CommandBase> tail;

    public Contact(Collection<CommandBase> header, Collection<CommandBase> body,
            Collection<CommandBase> tail) {
        this.header = header;
        this.body = body;
        this.tail = tail;
    }

    public Collection<CommandBase> getHeaderCommands() {
        return header;
    }

    public Collection<CommandBase> getBodyCommands() {
        return body;
    }

    public Collection<CommandBase> getTailCommands() {
        return tail;
    }

    @Override
    public String toString() {
        return "ContactTemplate [header=" + header + ", body=" + body
                + ", tail=" + tail + "]";
    }
}
