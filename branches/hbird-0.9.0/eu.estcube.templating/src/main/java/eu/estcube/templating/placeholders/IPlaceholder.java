package eu.estcube.templating.placeholders;

public interface IPlaceholder<T> {

    public Class<T> getType();

    @Override
    public String toString();

    public Placeholder toPlaceholder() throws PlaceholderException;

}
