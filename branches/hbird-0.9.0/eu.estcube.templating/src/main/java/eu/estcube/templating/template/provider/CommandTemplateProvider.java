package eu.estcube.templating.template.provider;

import eu.estcube.templating.template.CommandTemplate;

public class CommandTemplateProvider implements TemplateProvider<CommandTemplate> {

    private final CommandTemplate template;

    public CommandTemplateProvider(CommandTemplate template) {
        this.template = template;
    }

    @Override
    public CommandTemplate getTemplate() {
        return template;
    }

}
