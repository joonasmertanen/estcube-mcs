package eu.estcube.templating.template.provider;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import eu.estcube.templating.template.Template;

/**
 * Reads data from a file and uses gson to convert into a template of the
 * specified type.
 * 
 * @author deiwin
 * 
 * @param <T> The type of template provided
 */
public abstract class GenericFileTemplateProvider<T extends Template> implements TemplateProvider<T> {

    protected final T template;

    public GenericFileTemplateProvider(String fileName, Class<T> type) throws IOException, JsonSyntaxException {
        template = new Gson().fromJson(FileUtils.readFileToString(new File(fileName)), type);
    }

    @Override
    public T getTemplate() {
        return template;
    }
}
