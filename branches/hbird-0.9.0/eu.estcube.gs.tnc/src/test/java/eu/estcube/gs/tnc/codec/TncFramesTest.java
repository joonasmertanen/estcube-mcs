package eu.estcube.gs.tnc.codec;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import eu.estcube.domain.transport.tnc.TncFrame;
import eu.estcube.domain.transport.tnc.TncFrame.TncCommand;

/**
 *
 */
public class TncFramesTest {

    private static final byte[] DATA = new byte[] { 0x0e, 0x0f, 0x0e, 0x0c, 0x01 };
    
    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFrames#toBytes(eu.estcube.domain.transport.tnc.TncFrame)}.
     */
    @Test
    public void testToBytes() {
        for (TncCommand command : TncCommand.values()) {
            for (int i = TncFrame.TARGET_MIN_VALUE; i < TncFrame.TARGET_MAX_VALUE; i++) {
                TncFrame frame = new TncFrame(command, i, DATA);
                byte[] bytes = TncFrames.toBytes(frame);
                assertEquals(command, TncFrames.getCommand(bytes[0]));
                assertEquals(command == TncCommand.RETURN ? -1 : i, TncFrames.getTarget(bytes[0]));
                byte[] data = new byte[bytes.length - 1];
                System.arraycopy(bytes, 1, data, 0, data.length);
                assertTrue(Arrays.equals(DATA, data));
            }
        }
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFrames#getType(eu.estcube.domain.transport.tnc.TncFrame.TncCommand, int)}.
     */
    @Test
    public void testGetType() {
        for (TncCommand cmd : TncCommand.values()) {
            for (int i = TncFrame.TARGET_MIN_VALUE; i < TncFrame.TARGET_MAX_VALUE; i ++) {
                byte type = TncFrames.getType(cmd, i);
                TncCommand c = TncFrames.getCommand(type);
                int target = TncFrames.getTarget(type);
                assertEquals(cmd, c);
                assertEquals(cmd == TncCommand.RETURN ? -1 : i, target);
            }
        }
    }
    
    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFrames#getType(eu.estcube.domain.transport.tnc.TncFrame.TncCommand, int)}.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetTypeWithTooLowValue() {
        TncFrames.getType(TncCommand.FULL_DUPLEX, TncFrame.TARGET_MIN_VALUE - 1);
    }
    
    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFrames#getType(eu.estcube.domain.transport.tnc.TncFrame.TncCommand, int)}.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetTypeWithTooHighValue() {
        TncFrames.getType(TncCommand.FULL_DUPLEX, TncFrame.TARGET_MAX_VALUE + 1);
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFrames#toFrame(byte[])}.
     */
    @Test
    public void testToFrame() {
        for (TncCommand command : TncCommand.values()) {
            for (int i = TncFrame.TARGET_MIN_VALUE; i < TncFrame.TARGET_MAX_VALUE; i++) {
                byte[] bytes = new byte[DATA.length + 1];
                bytes[0] = TncFrames.getType(command, i);
                System.arraycopy(DATA, 0, bytes, 1, DATA.length);
                TncFrame frame = TncFrames.toFrame(bytes);
                assertEquals(command, frame.getCommand());
                assertEquals(command == TncCommand.RETURN ? -1 : i, frame.getTarget());
                assertTrue(Arrays.equals(DATA, frame.getData()));
            }
        }
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFrames#getCommand(byte)}.
     */
    @Test
    public void testGetCommand() {
        for (TncCommand command : TncCommand.values()) {
            assertEquals(command, TncFrames.getCommand(command.getValue()));
        }
        assertNull(TncFrames.getCommand((byte) 0x1A));
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFrames#getTarget(byte)}.
     */
    @Test
    public void testGetTarget() {
        for (int i = TncFrame.TARGET_MIN_VALUE; i < TncFrame.TARGET_MAX_VALUE; i++) {
            assertEquals(i, TncFrames.getTarget((byte)(i << 4)));
        }
        assertEquals((byte)0xFF, TncFrames.getTarget((byte) 0xFF));
    }
}
