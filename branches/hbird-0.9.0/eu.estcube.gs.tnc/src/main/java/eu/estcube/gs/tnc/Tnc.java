package eu.estcube.gs.tnc;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.core.BusinessCard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import eu.estcube.common.Headers;
import eu.estcube.common.PrepareForInjection;
import eu.estcube.common.UpdateTimestamp;
import eu.estcube.domain.transport.Downlink;
import eu.estcube.domain.transport.Uplink;
import eu.estcube.gs.tnc.domain.TncDriverConfiguration;
import eu.estcube.gs.tnc.processor.AddHeaders;
import eu.estcube.gs.tnc.processor.FileInputToBinary;
import eu.estcube.gs.tnc.processor.TncFrameFromFileCreator;
import eu.estcube.gs.tnc.processor.TncFrameToFile;

@SuppressWarnings("deprecation")
public class Tnc extends RouteBuilder {

    public static final String TNC = "TNC";

    public static final int FRAMES_PER_INTERVAL = 1;

    private static final Logger LOG = LoggerFactory.getLogger(Tnc.class);

    @Autowired
    private PrepareForInjection preparator;

    @Autowired
    private TncDriverConfiguration config;

    @Autowired
    private FileInputToBinary fileToBinary;

    @Autowired
    private TncFrameFromFileCreator frameCreator;

    @Autowired
    private TncFrameToFile toFile;

    @Autowired
    private AddHeaders addHeaders;

    @Autowired
    private UpdateTimestamp updateTimestamp;

    /** @{inheritDoc . */
    @Override
    public void configure() throws Exception {
        MDC.put(StandardArguments.ISSUED_BY, config.getServiceId());

        BusinessCard card = new BusinessCard(config.getServiceId(), config.getServiceName());
        card.setPeriod(config.getHeartBeatInterval());
        card.setDescription(String.format("TNC driver for %s; version: %s", config.getGroundstationId(),
                config.getServiceVersion()));

        // @formatter:off

        // send business card
        from("timer://heartbeat?fixedRate=true&period=" + config.getHeartBeatInterval())
            .bean(card, "touch")
            .process(preparator)
            .to("activemq:topic:hbird.monitoring");

        // > Send DATA to TNC
        
        // sends messages to serial port using TNC codecs
        from("direct:toTnc")
            .throttle(FRAMES_PER_INTERVAL).timePeriodMillis(config.getFramesInterval()) // limit messages; one per given amount of milliseconds
            .asyncDelayed() // do not block the calling thread
            .to("log:eu.estcube.gs.tnc.out?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
            .doTry()
                .to(getSerialOutUri(config))
            .doCatch(Exception.class)
                .to("log:eu.estcube.gs.tnc.to.device?level=ERROR&showException=true")
             .end();
        
        // send all incoming TNC frames to AMQ topic for logging
        from("seda:logUplinkTncFrames")
            .bean(addHeaders)
            .setHeader(Headers.COMMUNICATION_LINK_TYPE, constant(Uplink.class.getSimpleName()))
            .setHeader(Headers.SERIAL_PORT_NAME, constant(config.getSerialOutPort()))
            .bean(updateTimestamp) // set header timestamp value to current 
            .to(Uplink.TNC_FRAMES_LOG)
            .end();

        // reads TNC commands from AMQ filtered by Ground Station ID and sends to TNC
        from(String.format("%s?selector=%s='%s'", Uplink.TNC_FRAMES, StandardArguments.GROUND_STATION_ID, config.getGroundstationId()))
            .multicast()
            .to("seda:logUplinkTncFrames", "direct:toTnc");

        // reads files from dir toTnc and sends to TNC
        if (config.useFiles()) {
            from("file://toTnc?recursive=true&move=.sent&maxMessagesPerPoll=" + config.getMaxMessagesPerPoll() + "&delay=" + config.getFilePollInterval())
                .doTry()
                    .bean(fileToBinary)
                    .bean(frameCreator)
                .doCatch(Exception.class)
                    .to("log:eu.estcube.gs.from.file?level=ERROR&showException=true")
                    .stop()
                .end()
                .multicast()
                .to("seda:logUplinkTncFrames", "direct:toTnc");
        }

        // > Handle DATA from TNC
        
        // writes messages to AMQ
        from("direct:toAmq")
            .to(Downlink.FROM_TNC)
            .end();

        // writes messages to dir fromTnc
        from("direct:toFile")
            .choice()
                .when().method(config, "useFiles")
                    .doTry()
                        .process(toFile)
                        .to("file://fromTnc")
                    .doCatch(Exception.class)
                        .to("log:eu.estcube.gs.tnc.to.file?level=ERROR&showException=true")
                    .end()
                .end();
        
        // reads input from serial port using TNC codecs
        // sends to AMQ and dir fromTnc
        from(getSerialInUri(config))
            .to("log:eu.estcube.gs.tnc.in?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
            .bean(addHeaders)
            .multicast()
            .to("direct:toAmq", "direct:toFile");


        // @formatter:on
    }

    static String getSerialInUri(TncDriverConfiguration config) {
        StringBuilder sb = new StringBuilder();
        sb.append("serial://").append(config.getSerialInPort()).append("?")
                .append("baud=").append(config.getSerialInBaud())
                .append("&dataBits=").append(config.getSerialInDataBits())
                .append("&stopBits=").append(config.getSerialInStopBits())
                .append("&parity=").append(config.getSerialInParity())
                .append("&flowControl=").append(config.getSerialInFlowControl());
        if (config.getSerialInFilters() != null && !config.getSerialInFilters().trim().isEmpty()) {
            sb.append("&filters=").append(config.getSerialInFilters());
        }
        return sb.toString();
    }

    static String getSerialOutUri(TncDriverConfiguration config) {
        StringBuilder sb = new StringBuilder();
        sb.append("serial://").append(config.getSerialOutPort()).append("?")
                .append("baud=").append(config.getSerialOutBaud())
                .append("&dataBits=").append(config.getSerialOutDataBits())
                .append("&stopBits=").append(config.getSerialOutStopBits())
                .append("&parity=").append(config.getSerialOutParity())
                .append("&flowControl=").append(config.getSerialOutFlowControl());
        if (config.getSerialOutFilters() != null && !config.getSerialOutFilters().trim().isEmpty()) {
            sb.append("&filters=").append(config.getSerialOutFilters());
        }
        return sb.toString();
    }

    public static void main(String[] args) throws Exception {
        LOG.info("Starting TNC driver");
        new Main().run(args);
    }
}
