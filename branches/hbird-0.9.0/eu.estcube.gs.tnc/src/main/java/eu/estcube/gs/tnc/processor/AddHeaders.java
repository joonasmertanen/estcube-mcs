/** 
 *
 */
package eu.estcube.gs.tnc.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.hbird.exchange.constants.StandardArguments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.camel.serial.SerialConsumer;
import eu.estcube.common.Headers;
import eu.estcube.domain.transport.Downlink;
import eu.estcube.gs.tnc.Tnc;
import eu.estcube.gs.tnc.domain.TncDriverConfiguration;

/**
 *
 */
@Component
public class AddHeaders implements Processor {

    @Autowired
    private TncDriverConfiguration config;

    /** @{inheritDoc . */
    @SuppressWarnings("deprecation")
    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        out.copyFrom(in);
        out.setHeader(StandardArguments.TIMESTAMP, in.getHeader(Exchange.CREATED_TIMESTAMP));
        out.setHeader(StandardArguments.ISSUED_BY, config.getServiceId());
        out.setHeader(StandardArguments.SATELLITE_ID, config.getSatelliteId());
        out.setHeader(StandardArguments.GROUND_STATION_ID, config.getGroundstationId());
        out.setHeader(StandardArguments.CONTACT_ID, config.getContactId());
        out.setHeader(StandardArguments.CLASS, in.getBody().getClass().getSimpleName());
        out.setHeader(StandardArguments.TYPE, Tnc.TNC);
        out.setHeader(Headers.SERIAL_PORT_NAME, in.getHeader(SerialConsumer.SERIAL_PORT));
        out.setHeader(Headers.COMMUNICATION_LINK_TYPE, Downlink.class.getSimpleName());
    }
}
