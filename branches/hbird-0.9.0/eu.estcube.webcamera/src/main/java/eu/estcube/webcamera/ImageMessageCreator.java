package eu.estcube.webcamera;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.core.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class ImageMessageCreator {

    public static final String PARAMETER_NAME = "Image";
    public static final String DESCRIPTION = "Image from ground station webcamera";

    @Value("${service.id}")
    protected String serviceId; // Eg. eu.estcube.webcamera

    @Value("${gs.id}")
    protected String groundStationId; // Eg. /ESTCUBE/GroundStations/ES5EC

    @Value("${parameter.namespace}")
    protected String parameterNamespace; // Eg. /ESTCUBE/GroundStations/ES5EC

    @Autowired
    protected IdBuilder idBuilder;

    public Binary create(@Header(StandardArguments.TIMESTAMP) long timestamp,
            @Header(StandardArguments.TYPE) String type, @Body byte[] data) {
        String id = idBuilder.buildID(parameterNamespace, PARAMETER_NAME);

        Binary result = new Binary(id, PARAMETER_NAME);
        result.setDescription(DESCRIPTION);
        result.setRawData(data);
        result.setFormat(type);
        result.setIssuedBy(serviceId);
        result.setApplicableTo(groundStationId);
        result.setTimestamp(timestamp);
        return result;
    }
}
