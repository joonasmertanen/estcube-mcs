package eu.estcube.webcamera;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.core.Binary;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ImageMessageCreatorTest {

    private static final String GS_ID = "/ESTCUBE/GroundStations/ES5EC";
    private static final String SERVICE_ID = "webcam";
    private static final String PARAMETER_NAMESPACE = "/ESTCUBE/GroundStations/ES5EC/Webcam";
    private static final Long NOW = System.currentTimeMillis();
    private static final byte[] DATA = new byte[] { 0x04, 0x04, 0x04, 0x0C, 0x00, 0x01, 0x08, 0x05 };
    private static final String TYPE = "raw test data";

    @Mock
    private IdBuilder idBuilder;

    @InjectMocks
    private ImageMessageCreator imageMessageCreator;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        imageMessageCreator.groundStationId = GS_ID;
        imageMessageCreator.serviceId = SERVICE_ID;
        imageMessageCreator.parameterNamespace = PARAMETER_NAMESPACE;
        inOrder = inOrder(idBuilder);
    }

    /**
     * Test method for
     * {@link eu.estcube.webcamera.ImageMessageCreator#create(long, java.lang.String, byte[])}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testCreate() {
        when(idBuilder.buildID(PARAMETER_NAMESPACE, ImageMessageCreator.PARAMETER_NAME)).thenReturn(
                PARAMETER_NAMESPACE + "/" + ImageMessageCreator.PARAMETER_NAME);
        Binary binary = imageMessageCreator.create(NOW, TYPE, DATA);
        assertNotNull(binary);
        assertNotNull(binary.getID());
        assertEquals(PARAMETER_NAMESPACE + "/" + ImageMessageCreator.PARAMETER_NAME, binary.getID());
        assertEquals(ImageMessageCreator.PARAMETER_NAME, binary.getName());
        assertEquals(NOW, new Long(binary.getTimestamp()));
        assertEquals(ImageMessageCreator.DESCRIPTION, binary.getDescription());
        assertEquals(SERVICE_ID, binary.getIssuedBy());
        assertEquals(GS_ID, binary.getApplicableTo());
        assertTrue(Arrays.equals(DATA, binary.getRawData()));
        assertEquals(TYPE, binary.getFormat());
        inOrder.verify(idBuilder, times(1)).buildID(PARAMETER_NAMESPACE, ImageMessageCreator.PARAMETER_NAME);
        inOrder.verifyNoMoreInteractions();
    }
}
