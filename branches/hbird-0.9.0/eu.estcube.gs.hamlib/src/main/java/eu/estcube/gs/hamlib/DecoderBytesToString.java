package eu.estcube.gs.hamlib;

import java.nio.charset.Charset;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.string.StringDecoder;

import eu.estcube.common.Constants;

public class DecoderBytesToString extends StringDecoder {

    public DecoderBytesToString() {
        super(Charset.forName(Constants.ENCODING_STRING_FORMAT));
    }

    public Object decode(ChannelHandlerContext ctx, Channel evt, Object msg) throws Exception {
        // decodes from bytes to a string
        return super.decode(ctx, evt, msg);
    }
}
