package eu.estcube.camel.serial;

import java.util.List;

import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.transport.serial.SerialAddress.DataBits;
import org.apache.mina.transport.serial.SerialAddress.FlowControl;
import org.apache.mina.transport.serial.SerialAddress.Parity;
import org.apache.mina.transport.serial.SerialAddress.StopBits;

/**
 *
 */
public class SerialConfig {

    private String port;
    private Integer baud;
    private DataBits dataBits;
    private StopBits stopBits;
    private Parity parity;
    private FlowControl flowControl;
    private List<IoFilter> filters;

    /**
     * Creates new SerialConfig.
     */
    public SerialConfig() {

    }

    /**
     * Returns port.
     * 
     * @return the port
     */
    public String getPort() {
        return port;
    }

    /**
     * Sets port.
     * 
     * @param port the port to set
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * Returns baud.
     * 
     * @return the baud
     */
    public Integer getBaud() {
        return baud;
    }

    /**
     * Sets baud.
     * 
     * @param baud the baud to set
     */
    public void setBaud(Integer baud) {
        this.baud = baud;
    }

    /**
     * Returns dataBits.
     * 
     * @return the dataBits
     */
    public DataBits getDataBits() {
        return dataBits;
    }

    /**
     * Sets dataBits.
     * 
     * @param dataBits the dataBits to set
     */
    public void setDataBits(DataBits dataBits) {
        this.dataBits = dataBits;
    }

    /**
     * Returns stopBits.
     * 
     * @return the stopBits
     */
    public StopBits getStopBits() {
        return stopBits;
    }

    /**
     * Sets stopBits.
     * 
     * @param stopBits the stopBits to set
     */
    public void setStopBits(StopBits stopBits) {
        this.stopBits = stopBits;
    }

    /**
     * Returns parity.
     * 
     * @return the parity
     */
    public Parity getParity() {
        return parity;
    }

    /**
     * Sets parity.
     * 
     * @param parity the parity to set
     */
    public void setParity(Parity parity) {
        this.parity = parity;
    }

    /**
     * Returns flowControl.
     * 
     * @return the flowControl
     */
    public FlowControl getFlowControl() {
        return flowControl;
    }

    /**
     * Sets flowControl.
     * 
     * @param flowControl the flowControl to set
     */
    public void setFlowControl(FlowControl flowControl) {
        this.flowControl = flowControl;
    }

    /**
     * Returns filters.
     * 
     * @return the filters
     */
    public List<IoFilter> getFilters() {
        return filters;
    }

    /**
     * Sets filters.
     * 
     * @param filters the filters to set
     */
    public void setFilters(List<IoFilter> filters) {
        this.filters = filters;
    }
}
