package eu.estcube.commanding.rotator1;

import java.util.ArrayList;
import java.util.List;

import org.hbird.exchange.navigation.PointingData;

import eu.estcube.commanding.api.OptimizeInterface;
import eu.estcube.commanding.optimizators.GlobalFunctions;

/**
 * When satellite trajectory crosses the north line
 * and elevation is high, then antenna starts at the opposite sector
 * and tracks the satellite using elevation rotators.
 * Azimuth values are changing insignificantly.
 * 
 * @author Ivar Mahhonin
 * 
 */
public class Rotator1CrossElHigh implements OptimizeInterface {
    double receivingSector;
    double maxAz;
    double maxEl;
    List<Double> azimuthList;
    List<Double> elevationList;

    public Rotator1CrossElHigh(final double receivingSector,
            final double maxAz, final double maxEl) {
        super();
        this.receivingSector = receivingSector;
        this.maxAz = maxAz;
        this.maxEl = maxEl;
    }

    /**
     * Optimizing antenna movement.
     * Changing azimuth values to the opposite.
     * Changing elevation values from 180 to 0.
     * When there are azimuth values from the middle sector, then
     * current azimuth value gets the value of the previous one.
     * 
     * @param coordinates - satellite trajectory.
     * @return optimized coordinates for the antenna rotators.
     */

    public List<PointingData> crossElHigh(List<PointingData> coordinates) {
        double azLastValue = GlobalFunctions.changeAzimuthTo180(coordinates
                .get(0).getAzimuth().intValue());
        double elLastValue = coordinates.get(0).getElevation().intValue();
        final int coordinatesListSize = coordinates.size();
        final int azLastPointSector = GlobalFunctions
                .getDegreeSector(coordinates.get(coordinatesListSize - 1)
                        .getAzimuth().intValue());
        final int azFirstPointSector = GlobalFunctions
                .getDegreeSector(coordinates.get(0).getAzimuth().intValue());
        final List<PointingData> coordinatesTemp = new ArrayList<PointingData>();
        for (int i = 0; i < coordinatesListSize; i++) {
            final long timestamp = coordinates.get(i).getTimestamp();
            final double doppler = coordinates.get(i).getDoppler();
            final String satelliteId = coordinates.get(i).getSatelliteID();
            final String groundStationId = coordinates.get(i)
                    .getGroundStationID();

            final double currentValueSector = GlobalFunctions
                    .getDegreeSector(coordinates.get(i).getAzimuth().intValue());
            double azCurrentValue = coordinates.get(i).getAzimuth().intValue();

            double elCurrentValue = coordinates.get(i).getElevation()
                    .intValue();

            if (elLastValue <= elCurrentValue) {
                elCurrentValue = 180 - elCurrentValue;
            }
            if (currentValueSector == azFirstPointSector) {
                azCurrentValue = GlobalFunctions
                        .changeAzimuthTo180(azCurrentValue);
                if (Math.abs(azCurrentValue - azLastValue) > 5) {
                    azCurrentValue = azLastValue;
                }
            }

            if ((currentValueSector != azFirstPointSector)
                    && (currentValueSector != azLastPointSector)) {
                azCurrentValue = coordinates.get(i - 1).getAzimuth().intValue();
            }

            final PointingData optimizedElement = new PointingData(timestamp,
                    azCurrentValue, elCurrentValue, doppler, satelliteId,
                    groundStationId);
            coordinatesTemp.add(optimizedElement);
            azLastValue = coordinatesTemp.get(i).getAzimuth();
            elLastValue = coordinates.get(i).getElevation();

        }
        coordinates = coordinatesTemp;
        return coordinates;

    }

    @Override
    public List<PointingData> optimize(List<PointingData> coordinates) {
        coordinates = crossElHigh(coordinates);
        return coordinates;
    }
}