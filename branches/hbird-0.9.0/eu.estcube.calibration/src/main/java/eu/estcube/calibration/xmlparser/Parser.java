package eu.estcube.calibration.xmlparser;

import java.io.File;
import java.util.Hashtable;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import eu.estcube.calibration.domain.InfoContainer;

public class Parser {

	/**
	 * @param args
	 */
	@SuppressWarnings("unchecked")
	public static Hashtable<String,InfoContainer> getCalibrations(File f){
		
		
		Hashtable<String,InfoContainer> table;
		
		XStream xs = new XStream(new DomDriver());
		xs.alias("calibration", Hashtable.class);
		xs.registerConverter(new HashTableConverter());
		
		table = (Hashtable<String, InfoContainer>) xs.fromXML(f);
		
		
		return table;

	}

}
