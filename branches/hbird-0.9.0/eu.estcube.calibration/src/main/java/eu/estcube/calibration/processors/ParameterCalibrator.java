/** 
 *
 */
package eu.estcube.calibration.processors;

import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.core.Parameter;

/**
 *
 */
public interface ParameterCalibrator {

    public Parameter calibrate(Parameter input, IdBuilder idBuilder);
}
