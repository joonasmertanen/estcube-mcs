package eu.estcube.calibration.domain;

import java.util.ArrayList;

public class InfoContainer {
	
	
	private String id;
	private String equation;
	private String unit;
	private ArrayList<String> additionalParameters;
	
	
	
	
	public InfoContainer(String id, String equation, String unit, ArrayList<String> additionalParameters){
		
		this.id = id;
		this.equation = equation;
		this.unit = unit;
		this.additionalParameters = additionalParameters;
	}
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEquation() {
		return equation;
	}
	public void setEquation(String equation) {
		this.equation = equation;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}



	public ArrayList<String> getAdditionalParameters() {
		return additionalParameters;
	}



	public void setAdditionalParameters(ArrayList<String> additionalParameters) {
		this.additionalParameters = additionalParameters;
	}
	
	
	
	
	
	

}
