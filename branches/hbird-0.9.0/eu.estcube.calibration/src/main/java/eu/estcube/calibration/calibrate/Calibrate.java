package eu.estcube.calibration.calibrate;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.core.Parameter;

import bsh.EvalError;
import bsh.Interpreter;
import eu.estcube.calibration.domain.InfoContainer;
import eu.estcube.calibration.utils.QueueMap;

public class Calibrate {

    private final Hashtable<String, Parameter> calibrated;
    private final QueueMap<String, Parameter> nonCalibrated;

    private IdBuilder idBuilder;

    public Calibrate(QueueMap<String, Parameter> nonCalibrated, IdBuilder idBuilder) {

        calibrated = new Hashtable<String, Parameter>();
        this.nonCalibrated = nonCalibrated;
    }

    private Number changeEndianness(Number value) {

        int newValue = (value.intValue() >> 8)
                + ((value.intValue() % 256) << 8);

        return newValue;

    }

    private boolean checkAdditionalParameters(
            ArrayList<String> additionalParameters) {

        boolean found = true;
        int index = 0;

        while (found && index < additionalParameters.size()) {

            String auxParameter = additionalParameters.get(index);

            if (auxParameter.substring(auxParameter.length() - 4)
                    .equals("_cal")) {

                if (!calibrated.keySet().contains(auxParameter)) {

                    found = false;
                } else {

                    index++;
                }
            } else {

                index++;

            }
        }

        return found;

    }

    private double getCalibratedValue(String id, Number value, String equation,
            ArrayList<String> additionalParameters) {

        double calibratedValue = 0.0;

        equation = equation.replaceAll(id, String.valueOf(value.intValue()));

        for (String auxParameter : additionalParameters) {

            if (auxParameter.substring(auxParameter.length() - 4).equals("_cal")) {

                Parameter pAux = calibrated.get(auxParameter);

                equation = equation.replaceAll(auxParameter, pAux.getValue().toString());

            } else {

                Parameter pAux = nonCalibrated.get(auxParameter);
                equation = equation.replaceAll(auxParameter, pAux.getValue().toString());
            }

        }

        Interpreter interpreter = new Interpreter();
        try {
            interpreter.eval("result= " + equation);
        } catch (EvalError e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            calibratedValue = (Double) interpreter.get("result");
        } catch (EvalError e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return calibratedValue;

    }

    public Parameter getCalibratedParameter(Parameter p,
            InfoContainer calibration) {

        boolean changeEndiannes = false;
        double calibratedValue = 0.0;

        if (calibration.getAdditionalParameters().isEmpty()
                || checkAdditionalParameters(calibration
                        .getAdditionalParameters())) {

            if (changeEndiannes) {

                Number changeEndianValue = changeEndianness(p.getValue());
                calibratedValue = getCalibratedValue(p.getID(),
                        changeEndianValue, calibration.getEquation(), calibration.getAdditionalParameters());

            } else {

                calibratedValue = getCalibratedValue(p.getName(), p.getValue(),
                        calibration.getEquation(), calibration.getAdditionalParameters());

            }

            String id = idBuilder.buildID(p.getID(), "cal");
            String name = p.getName() + "_cal";
            Parameter calibratedParameter = new Parameter(id, name);
            calibratedParameter.setIssuedBy(p.getIssuedBy());
            calibratedParameter.setDescription(p.getDescription());
            calibratedParameter.setUnit(calibration.getUnit());
            calibratedParameter.setValue(calibratedValue);

            return calibratedParameter;

        } else {

            return null;
        }

    }

    public void calibrate(String subsystem) {

        Hashtable<String, InfoContainer> calibrations = Main
                .getSubsystem(subsystem);

        while (!nonCalibrated.isEmpty()) {

            Parameter pOriginal = nonCalibrated.poll();
            Parameter pCalibrated = getCalibratedParameter(pOriginal,
                    calibrations.get(pOriginal.getName()));

            if (pCalibrated != null) {

                calibrated.put(pCalibrated.getName(), pCalibrated);

            } else {

                nonCalibrated.offer(pOriginal.getName(), pOriginal);

            }

        }

    }

    public void printCalibrated() {

        Set<String> keys = calibrated.keySet();
        Iterator it = keys.iterator();

        while (it.hasNext()) {

            String key = (String) it.next();

            Parameter p = calibrated.get(key);
            System.out.println(p.getName() + ": " + p.getValue() + " " + p.getUnit());

        }

    }

}
