package eu.estcube.calibration.calibrate;

import java.io.File;
import java.util.Hashtable;

import org.hbird.business.api.IdBuilder;
import org.hbird.business.api.impl.DefaultIdBuilder;
import org.hbird.exchange.core.Parameter;

import eu.estcube.calibration.domain.InfoContainer;
import eu.estcube.calibration.utils.FileManager;
import eu.estcube.calibration.utils.QueueMap;
import eu.estcube.calibration.xmlparser.Parser;

public class Main {

    private static Hashtable<String, Hashtable<String, InfoContainer>> subsystems;

    public static Hashtable<String, InfoContainer> getSubsystem(String key) {

        return subsystems.get(key);

    }

    private static void populateSubsystems() {

        subsystems = new Hashtable<String, Hashtable<String, InfoContainer>>();

        File[] configFiles = FileManager.getFiles();

        for (File f : configFiles) {

            Hashtable<String, InfoContainer> subsystem = Parser.getCalibrations(f);

            String key = f.getName().substring(0, f.getName().lastIndexOf('.'));

            subsystems.put(key, subsystem);

        }

    }

    public static void main(String[] args) {

        populateSubsystems();

        Parameter p0 = new Parameter("cdhs", "rtc_temp");
        p0.setValue(23.75D);
        Parameter p1 = new Parameter("cdhs", "internal_raw");
        p1.setValue(1753);
        Parameter p2 = new Parameter("cdhs", "vref_raw");
        p2.setValue(1450);

        QueueMap<String, Parameter> nonCalibrated = new QueueMap<String, Parameter>();

        nonCalibrated.offer(p0.getName(), p0);
        nonCalibrated.offer(p1.getName(), p1);
        nonCalibrated.offer(p2.getName(), p2);

        IdBuilder idBuilder = new DefaultIdBuilder();
        Calibrate c = new Calibrate(nonCalibrated, idBuilder);
        c.calibrate("cdhs");
        c.printCalibrated();

    }

}
