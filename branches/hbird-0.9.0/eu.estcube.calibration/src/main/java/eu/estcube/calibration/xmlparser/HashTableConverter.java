package eu.estcube.calibration.xmlparser;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Iterator;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import eu.estcube.calibration.domain.InfoContainer;


public class HashTableConverter implements Converter {

	@Override
	public boolean canConvert(Class arg0) {
	
		return arg0.equals(Hashtable.class);
		
	}

	@Override
	public void marshal(Object value, HierarchicalStreamWriter writer,
			MarshallingContext context) {
		
	
//		Hashtable<String,InfoContainer> table = (Hashtable<String,InfoContainer>) value;
//		
//		
//		Iterator it = table.keySet().iterator();
//		
//		while (it.hasNext()){
//			
//			String key = (String) it.next();
//			InfoContainer info = (InfoContainer) table.get(key);
//								
//			
//			writer.startNode("entry");
//			writer.startNode("id");
//			writer.setValue(key);
//			writer.endNode();
//			writer.startNode("equation");
//			writer.setValue(info.getEquation());
//			writer.endNode();
//			writer.startNode("unit");
//			writer.setValue(info.getUnit());
//			writer.endNode();
//			
						
			
//		}
		
		
	
		
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader,
			UnmarshallingContext context) {
		
		
		Hashtable<String,InfoContainer> table = new Hashtable<String,InfoContainer>();
		
		
		
		while (reader.hasMoreChildren()){
			
			reader.moveDown();
			
			String id = "";
			String eq = "";
			String unit = "";
			ArrayList<String> parameters = new ArrayList<String>(0);
			
			
			while (reader.hasMoreChildren()){
				reader.moveDown();
				
				if (reader.getNodeName().equals("id")){
				
				id = reader.getValue();
				
				} else if (reader.getNodeName().equals("equation")){
					
					eq = reader.getValue();
					
				} else if (reader.getNodeName().equals("unit")){
					
					unit = reader.getValue();
				} else if (reader.getNodeName().equals("additionalParameters")){
					
					parameters = getParameters(reader.getValue());
					
				}
												
				reader.moveUp();
			}
			InfoContainer info = new InfoContainer(id, eq, unit, parameters);
			
			table.put(id, info);
			reader.moveUp();
			
		}
		
		return table;
		
		
	}
	
	private ArrayList<String> getParameters(String s){
		
		if (!s.isEmpty()){
			
			String[] paramArray = s.split(",");
			
			ArrayList<String> parameters = new ArrayList<String>(Arrays.asList(paramArray));
			
			return parameters;
		}
		
		else{
			
			return new ArrayList<String>(0);
		}
		
		
	}
	
	

	
}
