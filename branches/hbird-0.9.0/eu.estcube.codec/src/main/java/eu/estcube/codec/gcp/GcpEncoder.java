package eu.estcube.codec.gcp;

import java.util.ArrayList;
import java.util.List;

import org.hbird.business.api.IdBuilder;
import org.hbird.business.core.CommandableEntity;
import org.hbird.exchange.core.Command;
import org.hbird.exchange.core.CommandArgument;

import eu.estcube.codec.gcp.exceptions.CommandNotFoundException;
import eu.estcube.codec.gcp.exceptions.NotEnoughCommandArgumentsException;
import eu.estcube.codec.gcp.exceptions.SubsystemNotFoundException;
import eu.estcube.codec.gcp.exceptions.TooManyCommandArgumentsException;
import eu.estcube.codec.gcp.struct.GcpCommand;
import eu.estcube.codec.gcp.struct.GcpParameter;
import eu.estcube.codec.gcp.struct.GcpStruct;
import eu.estcube.codec.gcp.struct.GcpSubsystemIdProvider;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterMaxLength;

public class GcpEncoder {

    private static int ICP_HEADER_BYTES = 4;
    private static int HEADER_BYTES = 8;
    private static int ENDING_BYTES = 2;

    /**
     * Command to byte[] parser
     */
    public byte[] encode(Command command, GcpStruct struct, byte[] random2Bytes) {

        GcpCommand com = struct.getCommand(command.getArgumentValue("commandId", Integer.class));

        List<Byte> arguments = parametersToByteArray(com, command);

        int source = command.getArgumentValue("source", Integer.class);
        int destination = command.getArgumentValue("destination", Integer.class);
        int length = ICP_HEADER_BYTES + arguments.size();
        int priority = command.getArgumentValue("priority", Integer.class);
        int commandId = command.getArgumentValue("commandId", Integer.class);
        int CDHSSource = command.getArgumentValue("CDHSSource", Integer.class);
        int CDHSBlockIndex = command.getArgumentValue("CDHSBlockIndex", Integer.class);

        byte[] bytes = new byte[HEADER_BYTES + arguments.size() + ENDING_BYTES];

        bytes[0] = (byte) source;
        bytes[1] = (byte) destination;
        bytes[2] = (byte) (length >> 8);
        bytes[3] = (byte) (length);
        bytes[4] = (byte) ((priority << 6) | (commandId >> 8));
        bytes[5] = (byte) (commandId);
        bytes[6] = (byte) (CDHSSource << 4 | (CDHSBlockIndex & 0x0F));
        bytes[7] = (byte) arguments.size();

        int offset = HEADER_BYTES;

        for (byte b : arguments) {
            bytes[offset++] = b;
        }

        bytes[offset] = random2Bytes[0];
        bytes[offset + 1] = random2Bytes[1];

        return bytes;
    }

    private List<Byte> parametersToByteArray(GcpCommand com, Command command) {

        List<Byte> list = new ArrayList<Byte>();

        for (GcpParameter param : com.getParameters()) {
            byte[] bytes = param.toBytes(command.getArgumentValue(param.getName(), param.getValueClass()));
            for (byte mbyte : bytes) {
                list.add(mbyte);
            }
        }

        return list;
    }

    /**
     * User input string to Command parser
     * 
     * @throws SubsystemNotFoundException
     */
    public Command encode(String input, GcpStruct struct, String entityId, IdBuilder idBuilder)
            throws TooManyCommandArgumentsException,
            NotEnoughCommandArgumentsException, CommandNotFoundException, SubsystemNotFoundException {

        String[] inputs = input.trim().split(" ");

        int source = Integer.parseInt(inputs[0]);
        int destination = Integer.parseInt(inputs[1]);
        int priority = Integer.parseInt(inputs[2]);
        int CDHSSource = Integer.parseInt(inputs[4]);
        int CDHSBlockIndex = Integer.parseInt(inputs[5]);
        int commandId;

        try {
            commandId = Integer.parseInt(inputs[3]);
        } catch (NumberFormatException e) {
            GcpCommand com = struct.getCommand(inputs[3]);
            commandId = com.getId();
        }

        Command command = getCommand(struct, commandId, destination, idBuilder);
        if (command == null) {
            throw new CommandNotFoundException(destination, inputs[3]);
        }

        entityId = entityId.replace("{$subsystem}", GcpSubsystemIdProvider.getName(destination));

        command.setID(entityId);
        command.setArgumentValue("source", source);
        command.setArgumentValue("destination", destination);
        command.setArgumentValue("priority", priority);
        command.setArgumentValue("commandId", commandId);
        command.setArgumentValue("CDHSSource", CDHSSource);
        command.setArgumentValue("CDHSBlockIndex", CDHSBlockIndex);

        int i = 6;
        GcpCommand com = struct.getCommand(commandId);
        for (GcpParameter parameter : com.getParameters()) {
            if (i >= inputs.length) {
                throw new NotEnoughCommandArgumentsException(com.getName());
            }
            boolean isMaxLength = parameter instanceof GcpParameterMaxLength;
            String value = getNthParameterFromString(inputs, i++, isMaxLength);
            command.setArgumentValue(parameter.getName(), parameter.toValue(value));

            if (isMaxLength) {
                i = inputs.length;
            }
        }

        if (i < inputs.length) {
            throw new TooManyCommandArgumentsException(com.getName());
        }

        return command;
    }

    public static String getNthParameterFromString(String[] inputs, int i, boolean isMaxLength) {
        String value;
        if (isMaxLength) {
            value = "";
            for (int s = 0; s < inputs.length - i; s++) {
                if (s > 0) {
                    value += " ";
                }
                value += inputs[i + s];
            }
        } else {
            value = inputs[i];
        }
        return value;
    }

    /**
     * Helper method. Should be removed when command can be loaded through
     * archive.
     * 
     * @throws SubsystemNotFoundException
     */
    private static Command getCommand(GcpStruct struct, int commandId, int subsystem, IdBuilder idBuilder)
            throws SubsystemNotFoundException {

        GcpCommand com = struct.getCommand(commandId);
        if (com != null) {
            List<CommandableEntity> list = new GcpXmlCommandsParser(idBuilder).parse(struct);
            String subsystemName = GcpSubsystemIdProvider.getName(subsystem);
            for (CommandableEntity part : list) {
                if (part.getName().equals(subsystemName)) {
                    for (Command command : part.getCommands()) {
                        if (command.getName().equals(com.getName())) {
                            Command result = copyCommand(command);
                            return result;
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Helper method. Should be removed when Command can be copied through
     * constructor.
     */
    private static Command copyCommand(Command command) {
        Command result = new Command(command.getName(), command.getName());
        result.setDescription(command.getDescription());
        List<CommandArgument> arguments = new ArrayList<CommandArgument>();
        for (CommandArgument arg : command.getArgumentList()) {
            arguments.add(new CommandArgument(arg));
        }
        result.setArgumentList(arguments);

        return result;
    }
}
