package eu.estcube.codec.gcp.struct.parameters;

import java.nio.ByteBuffer;

import org.w3c.dom.Element;

public class GcpParameterFloat extends GcpParameterNumberic {

    public final static long BYTES = 4;

    public GcpParameterFloat(String name, String description, String unit) {
        super(name, description, unit, false);
    }

    public GcpParameterFloat(Element e) {
        super(e);
    }

    @Override
    public int getLength() {
        return 32;
    }

    @Override
    public Float toValue(String input) {
        if (isHexString(input)) {
            return (Float) toValueFromHex(input);
        }
        return Float.parseFloat(input);
    }

    @Override
    public byte[] toBytes(Object value) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putFloat(toValue(String.valueOf(value)));
        byte[] bytes = { buffer.get(0), buffer.get(1), buffer.get(2), buffer.get(3) };
        return bytes;
    }

    @Override
    public Float toValue(byte[] bytes) {

        if (bytes.length != BYTES) {
            throw new RuntimeException("byte[] length of the argument must be " + BYTES + "! Was " + bytes.length);
        }

        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.put(bytes[0]);
        buffer.put(bytes[1]);
        buffer.put(bytes[2]);
        buffer.put(bytes[3]);

        return buffer.getFloat(0);
    }

    @Override
    public Class<?> getValueClass() {
        return Float.class;
    }

}
