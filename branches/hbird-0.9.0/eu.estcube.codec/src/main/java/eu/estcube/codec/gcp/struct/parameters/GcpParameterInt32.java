package eu.estcube.codec.gcp.struct.parameters;

import java.nio.ByteBuffer;

import org.apache.commons.lang3.ArrayUtils;
import org.w3c.dom.Element;

public class GcpParameterInt32 extends GcpParameterNumberic {

    public final static long BYTES = 4;

    public GcpParameterInt32(String name, String description, String unit, Boolean isLittleEndian) {
        super(name, description, unit, isLittleEndian);
    }

    public GcpParameterInt32(Element e) {
        super(e);
    }

    @Override
    public int getLength() {
        return 32;
    }

    @Override
    public Integer toValue(String input) {
        if (isHexString(input)) {
            return (Integer) toValueFromHex(input);
        }
        return Integer.parseInt(input);
    }

    @Override
    public byte[] toBytes(Object value) {

        ByteBuffer buffer = ByteBuffer.allocate(4);
        buffer.putInt(toValue(String.valueOf(value)));
        byte[] bytes = { buffer.get(0), buffer.get(1), buffer.get(2), buffer.get(3) };

        if (getIsLittleEndian()) {
            ArrayUtils.reverse(bytes);
        }

        return bytes;
    }

    @Override
    public Integer toValue(byte[] bytes) {

        if (bytes.length != BYTES) {
            throw new RuntimeException("byte[] length of the argument must be " + BYTES + "! Was " + bytes.length);
        }

        byte byte1 = bytes[0];
        byte byte2 = bytes[1];
        byte byte3 = bytes[2];
        byte byte4 = bytes[3];

        if (getIsLittleEndian()) {
            byte1 = bytes[3];
            byte2 = bytes[2];
            byte3 = bytes[1];
            byte4 = bytes[0];
        }

        return (byte1 & 0xFF) << 24 | (byte2 & 0xFF) << 16 | (byte3 & 0xFF) << 8 | (byte4 & 0xFF);
    }

    @Override
    public Class<?> getValueClass() {
        return Integer.class;
    }

}
