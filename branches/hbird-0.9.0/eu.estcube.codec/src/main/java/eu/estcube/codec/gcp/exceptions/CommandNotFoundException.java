package eu.estcube.codec.gcp.exceptions;

public class CommandNotFoundException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -2786857398353700876L;

    public CommandNotFoundException(int destination, String id) {
        super("For destination:" + destination + " id:" + id);
    }
}
