package eu.estcube.codec.gcp.exceptions;

public class NotEnoughCommandArgumentsException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 4122539543777422756L;

    public NotEnoughCommandArgumentsException(String commandName) {
        super("For command " + commandName);
    }
}
