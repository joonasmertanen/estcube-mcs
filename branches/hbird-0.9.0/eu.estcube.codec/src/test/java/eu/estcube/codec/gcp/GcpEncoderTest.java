package eu.estcube.codec.gcp;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.hbird.business.api.IdBuilder;
import org.hbird.business.api.impl.DefaultIdBuilder;
import org.hbird.exchange.core.Command;
import org.junit.Before;
import org.junit.Test;

import eu.estcube.codec.gcp.exceptions.CommandNotFoundException;
import eu.estcube.codec.gcp.exceptions.NotEnoughCommandArgumentsException;
import eu.estcube.codec.gcp.exceptions.TooManyCommandArgumentsException;
import eu.estcube.codec.gcp.struct.GcpCommand;
import eu.estcube.codec.gcp.struct.GcpStruct;
import eu.estcube.codec.gcp.struct.GcpSubsystem;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterByteArray;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterUint32;

public class GcpEncoderTest {

    private GcpStruct struct;
    private final String pingCommandTest = "6 2 3 7 3 4 1234567891 0102 0304";
    private final String pingCommandNameTest = "6 2 3 ping 3 4 1234567891 0102 0304";
    private final byte[] pingCommandAsBytesTest = { 6, 2, 0, 12, -64, 7, 0x34, 8, 73, -106, 2, -45,
            1, 2, 3, 4, 2, 3 };

    private final String invalidCommandTest = "0 0 0 12 0 0";
    private final String tooManyArgumentsTest = "0 2 0 6 0 0 11 12";
    private final String notEnoughArgumentsTest = "0 2 0 6 0 0";

    private final byte[] randoms = { 2, 3 };
    private IdBuilder idBuilder;

    @Before
    public void setup() {
        struct = new GcpStruct();

        GcpSubsystem CDHS = new GcpSubsystem("CDHS");
        GcpSubsystem COM = new GcpSubsystem("COM");

        GcpCommand c2 = new GcpCommand(6, "ping2", "Ping2");
        c2.addParameter(new GcpParameterUint32("timestamp", "Timestamp", "s", false));
        c2.addSubsystem(COM);
        c2.addSubsystem(CDHS);
        struct.addCommand(c2);

        GcpCommand c1 = new GcpCommand(7, "ping", "Ping");
        c1.addParameter(new GcpParameterUint32("timestamp", "Timestamp", "s", false));
        c1.addParameter(new GcpParameterByteArray("bytearray", "ByteArray"));
        c1.addSubsystem(COM);
        c1.addSubsystem(CDHS);
        struct.addCommand(c1);
        idBuilder = new DefaultIdBuilder();
    }

    @Test
    public void testEncodeToCommand() throws Exception {
        Command command = new GcpEncoder().encode(pingCommandTest, struct, "prefix.{$subsystem}.", idBuilder);

        assertEquals("ping", command.getName());
        assertEquals("prefix.CDHS.", command.getID());
        assertEquals("Ping", command.getDescription());

        assertEquals(6, (int) command.getArgumentValue("source", Integer.class));
        assertEquals(2, (int) command.getArgumentValue("destination", Integer.class));
        assertEquals(3, (int) command.getArgumentValue("priority", Integer.class));
        assertEquals(7, (int) command.getArgumentValue("commandId", Integer.class));
        assertEquals(3, (int) command.getArgumentValue("CDHSSource", Integer.class));
        assertEquals(4, (int) command.getArgumentValue("CDHSBlockIndex", Integer.class));
        assertEquals(1234567891, (long) command.getArgumentValue("timestamp", Long.class));
        assertArrayEquals(new byte[] { 1, 2, 3, 4 }, command.getArgumentValue("bytearray", byte[].class));

        try {
            new GcpEncoder().encode(invalidCommandTest, struct, "", idBuilder);
            fail("Must throw exception for invalid command");
        } catch (Exception e) {
            assertTrue(e instanceof CommandNotFoundException);
        }

        try {
            new GcpEncoder().encode(notEnoughArgumentsTest, struct, "", idBuilder);
            fail("Must throw exception for invalid command");
        } catch (Exception e) {
            assertTrue(e instanceof NotEnoughCommandArgumentsException);
        }

        try {
            new GcpEncoder().encode(tooManyArgumentsTest, struct, "", idBuilder);
            fail("Must throw exception for invalid command");
        } catch (Exception e) {
            assertTrue(e instanceof TooManyCommandArgumentsException);
        }
    }

    @Test
    public void testEncodeToCommand2() throws Exception {
        Command command = new GcpEncoder().encode(pingCommandNameTest, struct, "prefix.{$subsystem}.", idBuilder);

        assertEquals(command.getName(), "ping");
        assertEquals(command.getID(), "prefix.CDHS.");
        assertEquals(command.getDescription(), "Ping");

        assertEquals(6, (int) command.getArgumentValue("source", Integer.class));
        assertEquals(2, (int) command.getArgumentValue("destination", Integer.class));
        assertEquals(3, (int) command.getArgumentValue("priority", Integer.class));
        assertEquals(7, (int) command.getArgumentValue("commandId", Integer.class));
        assertEquals(3, (int) command.getArgumentValue("CDHSSource", Integer.class));
        assertEquals(4, (int) command.getArgumentValue("CDHSBlockIndex", Integer.class));
        assertEquals(1234567891, (long) command.getArgumentValue("timestamp", Long.class));
        assertArrayEquals(new byte[] { 1, 2, 3, 4 }, command.getArgumentValue("bytearray", byte[].class));

    }

    @Test
    public void testGetNthParameterFromString() {
        String[] testString = new String[] { "a", "", "b", "", "c" };
        assertEquals("b", GcpEncoder.getNthParameterFromString(testString, 2, false));
        assertEquals("", GcpEncoder.getNthParameterFromString(testString, 1, false));
        assertEquals("b  c", GcpEncoder.getNthParameterFromString(testString, 2, true));
        assertEquals(" c", GcpEncoder.getNthParameterFromString(testString, 3, true));
    }

    @Test
    public void testEncodeToBytes() throws Exception {
        Command command = new GcpEncoder().encode(pingCommandTest, struct, "", idBuilder);
        byte[] bytes = new GcpEncoder().encode(command, struct, randoms);

        assertArrayEquals(pingCommandAsBytesTest, bytes);

        try {
            new GcpEncoder().encode(invalidCommandTest, struct, "", idBuilder);
            fail("Must throw exception for invalid command");
        } catch (Exception e) {
        }
    }

}
