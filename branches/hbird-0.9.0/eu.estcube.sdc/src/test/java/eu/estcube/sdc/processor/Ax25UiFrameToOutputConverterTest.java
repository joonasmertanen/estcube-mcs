/** 
 *
 */
package eu.estcube.sdc.processor;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.common.ByteUtil;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.sdc.domain.Ax25UiFrameOutput;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class Ax25UiFrameToOutputConverterTest {

    private static final byte[] DEST_ADDR = new byte[] { 0x09, 0x08, 0x07, 0x06, 0x05, 0x04, 0x03 };
    private static final byte[] SRC_ADDR = new byte[] { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07 };
    private static final byte CTRL = 0x40;
    private static final byte PID = 0x10;
    private static final byte[] INFO = new byte[] { 0x0D, 0x0E, 0x0A, 0x0D, 0x0C, 0x00, 0x0D, 0x0E };

    @Mock
    private Ax25UIFrame frame;

    private Ax25UiFrameToOutputConverter converter;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        converter = new Ax25UiFrameToOutputConverter();
        inOrder = inOrder(frame);
        when(frame.getDestAddr()).thenReturn(DEST_ADDR);
        when(frame.getSrcAddr()).thenReturn(SRC_ADDR);
        when(frame.getCtrl()).thenReturn(CTRL);
        when(frame.getPid()).thenReturn(PID);
        when(frame.getInfo()).thenReturn(INFO);
    }

    /**
     * Test method for
     * {@link eu.estcube.sdc.processor.Ax25UiFrameToOutputConverter#convert(eu.estcube.domain.transport.ax25.Ax25UIFrame)}
     * .
     */
    @Test
    public void testConvert() {
        Ax25UiFrameOutput output = converter.convert(frame);
        assertEquals(ByteUtil.toHexString(DEST_ADDR), output.getDestAddr());
        assertEquals(ByteUtil.toHexString(SRC_ADDR), output.getSrcAddr());
        assertEquals(ByteUtil.toHexString(CTRL), output.getCtrl());
        assertEquals(ByteUtil.toHexString(PID), output.getPid());
        assertEquals(ByteUtil.toHexString(INFO), output.getInfo());
        inOrder.verify(frame, times(1)).getDestAddr();
        inOrder.verify(frame, times(1)).getSrcAddr();
        inOrder.verify(frame, times(1)).getCtrl();
        inOrder.verify(frame, times(1)).getPid();
        inOrder.verify(frame, times(1)).getInfo();
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.sdc.processor.Ax25UiFrameToOutputConverter#convert(eu.estcube.domain.transport.ax25.Ax25UIFrame)}
     * .
     */
    @Test(expected = NullPointerException.class)
    public void testConvertNull() {
        converter.convert(null);
    }

    /**
     * Test method for
     * {@link eu.estcube.sdc.processor.Ax25UiFrameToOutputConverter#convert(eu.estcube.domain.transport.ax25.Ax25UIFrame)}
     * .
     */
    @Test
    public void testConvertFrameWithNullValues() {
        when(frame.getDestAddr()).thenReturn(null);
        when(frame.getSrcAddr()).thenReturn(null);
        when(frame.getCtrl()).thenReturn((byte) 0);
        when(frame.getPid()).thenReturn((byte) 0);
        when(frame.getInfo()).thenReturn(null);

        Ax25UiFrameOutput output = converter.convert(frame);
        assertEquals("", output.getDestAddr());
        assertEquals("", output.getSrcAddr());
        assertEquals("00", output.getCtrl());
        assertEquals("00", output.getPid());
        assertEquals("", output.getInfo());
        inOrder.verify(frame, times(1)).getDestAddr();
        inOrder.verify(frame, times(1)).getSrcAddr();
        inOrder.verify(frame, times(1)).getCtrl();
        inOrder.verify(frame, times(1)).getPid();
        inOrder.verify(frame, times(1)).getInfo();
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.sdc.processor.Ax25UiFrameToOutputConverter#toHexString(Byte)}
     * .
     */
    @Test
    public void testToHexStringByte() {
        assertEquals("", converter.toHexString((Byte) null));
        assertEquals("0F", converter.toHexString((byte) 0x0F));
    }

    /**
     * Test method for
     * {@link eu.estcube.sdc.processor.Ax25UiFrameToOutputConverter#toHexString(byte[])}
     * .
     */
    @Test
    public void testToHexStringBytes() {
        assertEquals("", converter.toHexString((byte[]) null));
        assertEquals("", converter.toHexString(new byte[0]));
        assertEquals(ByteUtil.toHexString(INFO), converter.toHexString(INFO));
    }
}
