/** 
 *
 */
package eu.estcube.sdc.processor;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.TypeConversionException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalMatchers;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.codec.ax25.impl.Ax25UIFrameKissEncoder;
import eu.estcube.common.Headers;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.domain.transport.tnc.TncFrame;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class Ax25UIFrameToTncFrameTest {

    private static final Integer PORT = new Integer(8);
    
    private static final String PREFIX = "0D 0A";
    
    @Mock
    private Ax25UIFrameKissEncoder encoder;
    
    @Mock
    private Exchange exchange;
    
    @Mock
    private Message in;
    
    @Mock
    private Message out;
    
    @Mock
    private Ax25UIFrame ax25Frame;
    
    @Mock
    private TncFrame tncFrame;
    
    @Mock
    private TypeConversionException typeConversionException;
    
    @InjectMocks
    private Ax25UIFrameToTncFrame toTnc;

    private InOrder inOrder;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        inOrder = inOrder(exchange, in, out, encoder, ax25Frame, tncFrame, typeConversionException);
        when(exchange.getIn()).thenReturn(in);
        when(exchange.getOut()).thenReturn(out);
        when(in.getBody(Ax25UIFrame.class)).thenReturn(ax25Frame);
        when(encoder.encode(eq(ax25Frame), anyInt(), any(byte[].class))).thenReturn(tncFrame);
        when(in.getHeader(Headers.TNC_PREFIX, String.class)).thenReturn(PREFIX);
    }

    /**
     * Test method for {@link eu.estcube.sdc.processor.Ax25UIFrameToTncFrame#process(org.apache.camel.Exchange)}.
     * @throws Exception 
     */
    @Test
    public void testProcess() throws Exception {
        when(in.getHeader(Headers.TNC_PORT, Integer.class)).thenReturn(PORT);
        toTnc.process(exchange);
        
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getHeader(Headers.TNC_PORT, Integer.class);
        inOrder.verify(in, times(1)).getHeader(Headers.TNC_PREFIX, String.class);
        inOrder.verify(in, times(1)).getBody(Ax25UIFrame.class);
        inOrder.verify(encoder, times(1)).encode(eq(ax25Frame), eq(PORT.intValue()), AdditionalMatchers.aryEq(new byte[] { 0x0D, 0x0A }));
        inOrder.verify(out, times(1)).setBody(tncFrame);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.sdc.processor.Ax25UIFrameToTncFrame#process(org.apache.camel.Exchange)}.
     * @throws Exception 
     */
    @Test
    public void testProcessWithNullPort() throws Exception {
        when(in.getHeader(Headers.TNC_PORT, Integer.class)).thenReturn(null);
        toTnc.process(exchange);
        
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getHeader(Headers.TNC_PORT, Integer.class);
        inOrder.verify(in, times(1)).getHeader(Headers.TNC_PREFIX, String.class);
        inOrder.verify(in, times(1)).getBody(Ax25UIFrame.class);
        inOrder.verify(encoder, times(1)).encode(eq(ax25Frame), eq(Ax25UIFrameKissEncoder.DEFAULT_TNC_TARGET_PORT), AdditionalMatchers.aryEq(new byte[] { 0x0D, 0x0A }));
        inOrder.verify(out, times(1)).setBody(tncFrame);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.sdc.processor.Ax25UIFrameToTncFrame#process(org.apache.camel.Exchange)}.
     * @throws Exception 
     */
    @Test
    public void testProcessWithException() throws Exception {
        doThrow(typeConversionException).when(in).getHeader(Headers.TNC_PORT, Integer.class);
        toTnc.process(exchange);
        
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getHeader(Headers.TNC_PORT, Integer.class);
        inOrder.verify(in, times(1)).getHeader(Headers.TNC_PREFIX, String.class);
        inOrder.verify(in, times(1)).getBody(Ax25UIFrame.class);
        inOrder.verify(encoder, times(1)).encode(eq(ax25Frame), eq(Ax25UIFrameKissEncoder.DEFAULT_TNC_TARGET_PORT), AdditionalMatchers.aryEq(new byte[] { 0x0D, 0x0A }));
        inOrder.verify(out, times(1)).setBody(tncFrame);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.sdc.processor.Ax25UIFrameToTncFrame#process(org.apache.camel.Exchange)}.
     * @throws Exception 
     */
    @Test
    public void testProcessWithNullPrefix() throws Exception {
        when(in.getHeader(Headers.TNC_PORT, Integer.class)).thenReturn(PORT);
        when(in.getHeader(Headers.TNC_PREFIX, String.class)).thenReturn(null);
        toTnc.process(exchange);
        
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getHeader(Headers.TNC_PORT, Integer.class);
        inOrder.verify(in, times(1)).getHeader(Headers.TNC_PREFIX, String.class);
        inOrder.verify(in, times(1)).getBody(Ax25UIFrame.class);
        inOrder.verify(encoder, times(1)).encode(eq(ax25Frame), eq(PORT.intValue()), isNull(byte[].class));
        inOrder.verify(out, times(1)).setBody(tncFrame);
        inOrder.verifyNoMoreInteractions();
    }
    
    /**
     * Test method for {@link eu.estcube.sdc.processor.Ax25UIFrameToTncFrame#process(org.apache.camel.Exchange)}.
     * @throws Exception 
     */
    @Test
    public void testProcessWithBlankPrefix() throws Exception {
        when(in.getHeader(Headers.TNC_PORT, Integer.class)).thenReturn(PORT);
        when(in.getHeader(Headers.TNC_PREFIX, String.class)).thenReturn("");
        toTnc.process(exchange);
        
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getHeader(Headers.TNC_PORT, Integer.class);
        inOrder.verify(in, times(1)).getHeader(Headers.TNC_PREFIX, String.class);
        inOrder.verify(in, times(1)).getBody(Ax25UIFrame.class);
        inOrder.verify(encoder, times(1)).encode(eq(ax25Frame), eq(PORT.intValue()), isNull(byte[].class));
        inOrder.verify(out, times(1)).setBody(tncFrame);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.sdc.processor.Ax25UIFrameToTncFrame#process(org.apache.camel.Exchange)}.
     * @throws Exception 
     */
    @Test
    public void testProcessWithEmptyPrefix() throws Exception {
        when(in.getHeader(Headers.TNC_PORT, Integer.class)).thenReturn(PORT);
        when(in.getHeader(Headers.TNC_PREFIX, String.class)).thenReturn(" ");
        toTnc.process(exchange);
        
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getHeader(Headers.TNC_PORT, Integer.class);
        inOrder.verify(in, times(1)).getHeader(Headers.TNC_PREFIX, String.class);
        inOrder.verify(in, times(1)).getBody(Ax25UIFrame.class);
        inOrder.verify(encoder, times(1)).encode(eq(ax25Frame), eq(PORT.intValue()), isNull(byte[].class));
        inOrder.verify(out, times(1)).setBody(tncFrame);
        inOrder.verifyNoMoreInteractions();
    }
    
    /**
     * Test method for {@link eu.estcube.sdc.processor.Ax25UIFrameToTncFrame#process(org.apache.camel.Exchange)}.
     * @throws Exception 
     */
    @Test
    public void testProcessWithInvlaidPrefix() throws Exception {
        when(in.getHeader(Headers.TNC_PORT, Integer.class)).thenReturn(PORT);
        when(in.getHeader(Headers.TNC_PREFIX, String.class)).thenReturn("WTF?");
        try {
            toTnc.process(exchange);
            fail("Exception expected");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
        
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getHeader(Headers.TNC_PORT, Integer.class);
        inOrder.verify(in, times(1)).getHeader(Headers.TNC_PREFIX, String.class);
        inOrder.verifyNoMoreInteractions();
    }
}
