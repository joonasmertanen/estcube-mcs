/** 
 *
 */
package eu.estcube.weatherstation;

import org.hbird.exchange.core.ApplicableTo;

/**
 * 
 */
public class NamedConfig<T extends ApplicableTo> {

    protected final T baseValue;

    protected final NamedFactory<T> factory;

    /**
     * Creates new NamedConfig.
     * 
     * @param defaultValue
     * @param factory
     */
    public NamedConfig(T baseValue, NamedFactory<T> factory) {
        this.baseValue = baseValue;
        this.factory = factory;
    }

    public T createNewValue(String value) {
        return factory.createNew(baseValue, value);
    }
}
