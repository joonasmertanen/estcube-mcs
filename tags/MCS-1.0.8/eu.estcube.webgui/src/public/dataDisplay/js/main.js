// Function for extracting & grouping websocket data

function webSocketObject2Table(webSocketObject) {

    var name;
    var description;
    var formatPattern;
    var calibratedValue;
    var value;
    var unit;
    var onOff;
    var tableRow;

    name = webSocketObject.name;
    description = webSocketObject.description;
    formatPattern = webSocketObject.formatPattern;
    calibratedValue = webSocketObject.calibratedValue;
    value = sprintf(formatPattern, calibratedValue);
    if (formatPattern == undefined) {
        value = calibratedValue;
    }
    unit = webSocketObject.unit;

    if (webSocketObject.class == "CalibratedParameter") {

        if (unit == "1=ON, 0=OFF" || unit == "ADU") {
            unit = "";
        }
        if (webSocketObject.ID
                .indexOf("/ESTCUBE/Satellites/ESTCube-1/CDHS//version") > -1) {
            value = "0x" + value.toUpperCase();
        }
        tableRow = new TableData(name, description, value, unit);

        if (webSocketObject.ID.indexOf("/ESTCUBE/Satellites/ESTCube-1/CDHS//") > -1) {

            groupVectorsXYZ(description, value);
            tableViewModel.replaceAdcsSun1Row(tableRow);
            tableViewModel.replaceAdcsSun2Row(tableRow);
            if (description.indexOf("Device type, one of the following:") > -1) {
                switch (value) {
                case 0:
                    tableRow.value = "Internal RAM";
                    break;
                case 1:
                    tableRow.value = "Internal Flash";
                    break;
                case 2:
                    tableRow.value = "External Flash";
                    break;
                case 3:
                    tableRow.value = "External FRAM";
                    break;
                }
            }
            tableViewModel.replaceCdhsRow(tableRow);

        } else if (webSocketObject.ID
                .indexOf("/ESTCUBE/Satellites/ESTCube-1/EPS//") > -1) {
            if (description.indexOf("charge") > -1) {
                batteryProgress(description, value);
            }
            if (value != "ON" && value != "OFF") {
                tableViewModel.replaceEpsCtlRow(tableRow);
            } else {

                switch2onOff(description, value);

                tableViewModel.replaceEpsSwitchRow(tableRow);
            }
            tableViewModel.replaceEpsSolarRow(tableRow);
            tableViewModel.replaceEpsCoilRow(tableRow);
            tableViewModel.replaceEpsBatteryRow(tableRow);
            tableViewModel.replaceEpsBatARow(tableRow);
            tableViewModel.replaceEpsBatBRow(tableRow);
            tableViewModel.replaceEpsRegRow(tableRow);
            tableViewModel.replaceEpsCtlPlRow(tableRow);
            tableViewModel.replaceEpsTempRow(tableRow);
            // COM forward, reflected and temperature data comes from EPS's
            // getdebug command
            tableViewModel.replaceComRow(tableRow);

        } else if (webSocketObject.ID
                .indexOf("/ESTCUBE/Satellites/ESTCube-1/COM//") > -1) {
            tableViewModel.replaceComRow(tableRow);
        } else if (webSocketObject.ID
                .indexOf("/ESTCUBE/Satellites/ESTCube-1/CAM//") > -1) {
            if (tableRow.name == "version") {
                value = (tableRow.value).toString(16);
                value = "0x" + value.toUpperCase();
                tableRow.value = value;
            }
            tableViewModel.addCamRow(tableRow);
        } else if (webSocketObject.ID
                .indexOf("/ESTCUBE/Satellites/ESTCube-1/ADCS//") > -1) {
            tableViewModel.addAcdsRow(tableRow);
        }
    }
}

var webSocket;
var webSocketMessage;
var webSocketObject;

function openSocket() {
    console.log("Websocket opened.");

    // Ensures only one connection is open at a time
    if (webSocket !== undefined && webSocket.readyState !== WebSocket.CLOSED) {
        writeResponse("WebSocket is already opened.");
        return;
    }
    // .webgui/MCS/config/config.js
    webSocket = new WebSocket("ws://localhost:1337/hbird.out.parameters");
    // Log errors
    webSocket.onerror = function(error) {
        console.log('WebSocket Error ' + error);
    };
    /**
     * Binds functions to the listeners for the websocket.
     */
    webSocket.onopen = function(event) {
        // For reasons I can't determine, onopen gets called twice
        // and the first time event.data is undefined.
        if (event.data === undefined)
            return;
    };
    // Reading messages from websocket
    webSocket.onmessage = function(event) {
        webSocketMessage = event.data;
        webSocketObject = JSON.parse(webSocketMessage);
        webSocketObject2Table(webSocketObject);
    };
    webSocket.onclose = function(event) {
        console.log("Websocket closed.");
        setTimeout(function() {
            openSocket()
        }, 15000);
    };
}

function closeSocket() {
    webSocket.close();
}

function writeResponse(text) {
    var messages = document.getElementById("messages");
    messages.innerHTML += "<br/>" + text;
}

// KNOCKOUTJS

// Class to represent a data row (same for all components)
function TableData(name, description, value, unit) {
    var self = this;
    self.name = name;
    self.description = description;
    self.value = value;
    self.unit = unit;
    self.onOff = "null";
}

// Overall viewmodel
function TableDataViewModel() {

    var self = this;
    self.batProgressA = ko.observable(); // for battery charge/discharge
    // progress bar
    self.batProgressB = ko.observable(); // for battery charge/discharge
    // progress bar

    self.pushItem = function(array, tableRow) {
        array.push(tableRow);
    }

    self.pushOrReplaceItem = function(array, tableRow) {
        var match = ko.utils.arrayFirst(array(), function(item) {
            if (tableRow.description != "") {
                return tableRow.description === item.description;
            } else {
                return tableRow.name === item.name;
            }
        });
        if (match) {
            array.replace(match, tableRow);
        } else {
            array.push(tableRow);
        }
    }

    // Replace if value is in array
    self.replaceItem = function(array, tableRow) {
        var match = ko.utils.arrayFirst(array(), function(item) {
            return tableRow.description === item.description;
        });
        if (match) {
            array.replace(match, tableRow);
        }
    }

    // CDHS

    // CDHS - data array
    self.cdhsRows = ko.observableArray([]);
    // CDHS - add data
    self.addCdhsRow = function(tableRow) {
        self.pushItem(self.cdhsRows, tableRow);
    }
    // CDHS - replace data
    self.replaceCdhsRow = function(tableRow) {
        self.replaceItem(self.cdhsRows, tableRow);
    }

    // EPS

    // SOLAR CELLS - data array
    self.epsSolarRows = ko.observableArray([]);
    // SOLAR CELLS - add data
    self.addEpsSolarRow = function(tableRow) {
        self.pushItem(self.epsSolarRows, tableRow);
    }
    // SOLAR CELLS - replace data
    self.replaceEpsSolarRow = function(tableRow) {
        self.replaceItem(self.epsSolarRows, tableRow);
    }
    // REGULATORS - data array
    self.epsRegRows = ko.observableArray([]);
    self.epsCtlRows = ko.observableArray([]);
    self.epsCtlPlRows = ko.observableArray([]);
    // REGULATORS - add data
    self.addEpsRegRow = function(tableRow) {
        self.pushItem(self.epsRegRows, tableRow);
    }
    self.addEpsCtlRow = function(tableRow) {
        self.pushItem(self.epsCtlRows, tableRow);
    }
    self.addEpsCtlPlRow = function(tableRow) {
        self.pushItem(self.epsCtlPlRows, tableRow);
    }
    // REGULATORS - replace data
    self.replaceEpsRegRow = function(tableRow) {
        self.replaceItem(self.epsRegRows, tableRow);
    }
    self.replaceEpsCtlRow = function(tableRow) {
        self.replaceItem(self.epsCtlRows, tableRow);
    }
    self.replaceEpsCtlPlRow = function(tableRow) {
        self.replaceItem(self.epsCtlPlRows, tableRow);
    }
    // COILS - data array
    self.epsCoilRows = ko.observableArray([]);
    // COILS - add data
    self.addEpsCoilRow = function(tableRow) {
        self.pushItem(self.epsCoilRows, tableRow);
    }
    // COILS - replace data
    self.replaceEpsCoilRow = function(tableRow) {
        self.replaceItem(self.epsCoilRows, tableRow);
    }
    // BATTERIES - data array
    self.epsBatteryRows = ko.observableArray([]);
    self.epsBatARows = ko.observableArray([]);
    self.epsBatBRows = ko.observableArray([]);
    // BATTERIES - add data
    self.addEpsBatteryRow = function(tableRow) {
        self.pushItem(self.epsBatteryRows, tableRow);
    }
    self.addEpsBatARow = function(tableRow) {
        self.pushItem(self.epsBatARows, tableRow);
    }
    self.addEpsBatBRow = function(tableRow) {
        self.pushItem(self.epsBatBRows, tableRow);
    }
    // BATTERIES - replace data
    self.replaceEpsBatteryRow = function(tableRow) {
        self.replaceItem(self.epsBatteryRows, tableRow);
    }
    self.replaceEpsBatARow = function(tableRow) {
        self.replaceItem(self.epsBatARows, tableRow);
    }
    self.replaceEpsBatBRow = function(tableRow) {
        self.replaceItem(self.epsBatBRows, tableRow);
    }
    // SWITCHES - data array
    self.epsSwitchRows = ko.observableArray([]);
    // SWITCHES - add data
    self.addEpsSwitchRow = function(tableRow) {
        self.pushItem(self.epsSwitchRows, tableRow);
    }
    // SWITCHES - replace data
    self.replaceEpsSwitchRow = function(tableRow) {
        self.replaceItem(self.epsSwitchRows, tableRow);
    }
    // TEMPERATURES - data array
    self.epsTempRows = ko.observableArray([]);
    // TEMPERATURES - add data
    self.addEpsTempRow = function(tableRow) {
        self.pushItem(self.epsTempRows, tableRow);
    }
    // TEMPERATURES - replace data
    self.replaceEpsTempRow = function(tableRow) {
        self.replaceItem(self.epsTempRows, tableRow);
    }

    // COM

    // COM - data array
    self.comRows = ko.observableArray([]);
    // COM - add data
    self.addComRow = function(tableRow) {
        self.pushOrReplaceItem(self.comRows, tableRow);
    }
    // COM - replace data
    self.replaceComRow = function(tableRow) {
        self.replaceItem(self.comRows, tableRow);
    }

    // CAM

    // CAM - data array
    self.camRows = ko.observableArray([]);
    // CAM - add data
    self.addCamRow = function(tableRow) {
        self.pushOrReplaceItem(self.camRows, tableRow);
    }

    // ADCS

    // GYROS - data array
    self.adcsGyroVectorRows = ko.observableArray([]);
    self.adcsGyroRows = ko.observableArray([]);
    // GYROS - add data
    self.addAdcsGyroVectorRow = function(tableRow) {
        self.pushItem(self.adcsGyroVectorRows, tableRow);
    }
    self.addAdcsGyroRow = function(tableRow) {
        self.pushItem(self.adcsGyroRows, tableRow);
    }
    // GYROS - replace data
    self.replaceAdcsGyroVectorRow = function(tableRow) {
        self.replaceItem(self.adcsGyroVectorRows, tableRow);
    }
    self.replaceAdcsGyroRow = function(tableRow) {
        removeString(tableRow, " measurement.");
        self.replaceItem(self.adcsGyroRows, tableRow);
    }
    // SUN SENSORS - data array
    self.adcsSunVectorRows = ko.observableArray([]);
    self.adcsSun1Rows = ko.observableArray([]);
    self.adcsSun2Rows = ko.observableArray([]);
    // SUN SENSORS - add data
    self.addAdcsSunVectorRow = function(tableRow) {
        self.pushItem(self.adcsSunVectorRows, tableRow);
    }
    self.addAdcsSun1Row = function(tableRow) {
        removeString(tableRow, "measurement ");
        self.pushItem(self.adcsSun1Rows, tableRow);
    }
    self.addAdcsSun2Row = function(tableRow) {
        removeString(tableRow, "measurement ");
        self.pushItem(self.adcsSun2Rows, tableRow);
    }
    // SUN SENSORS - replace data
    self.replaceAdcsSunVectorRow = function(tableRow) {
        self.replaceItem(self.adcsSunVectorRows, tableRow);
    }
    self.replaceAdcsSun1Row = function(tableRow) {
        removeString(tableRow, "measurement ");
        self.replaceItem(self.adcsSun1Rows, tableRow);
    }
    self.replaceAdcsSun2Row = function(tableRow) {
        removeString(tableRow, "measurement ");
        self.replaceItem(self.adcsSun2Rows, tableRow);
    }
    // MAGNETOMETERS - data array
    self.adcsMagnetoVectorRows = ko.observableArray([]);
    self.adcsMagnetoRows = ko.observableArray([]);
    // MAGNETOMETERS - add data
    self.addAdcsMagnetoVectorRow = function(tableRow) {
        self.pushItem(self.adcsMagnetoVectorRows, tableRow);
    }
    self.addAdcsMagnetoRow = function(tableRow) {
        self.pushItem(self.adcsMagnetoRows, tableRow);
    }
    // MAGNETOMETERS - replace data
    self.replaceAdcsMagnetoVectorRow = function(tableRow) {
        self.replaceItem(self.adcsMagnetoVectorRows, tableRow);
    }
    self.replaceAdcsMagnetoRow = function(tableRow) {
        removeString(tableRow, " measurement.");
        self.replaceItem(self.adcsMagnetoRows, tableRow);
    }
    // GETTM2 - data array
    self.adcsAngVectorRows = ko.observableArray([]);
    self.adcsTorqueRows = ko.observableArray([]);
    self.adcsMagnetMomentVectorRows = ko.observableArray([]);
    // GETTM2 - add data
    self.addAdcsAngVectorRow = function(tableRow) {
        self.pushItem(self.adcsAngVectorRows, tableRow);
    }
    self.addAdcsTorqueRow = function(tableRow) {
        self.pushItem(self.adcsTorqueRows, tableRow);
    }
    self.addAdcsMagnetMomentVectorRow = function(tableRow) {
        self.pushItem(self.adcsMagnetMomentVectorRows, tableRow);
    }
    // GETTM2 - replace data
    self.replaceAdcsAngVectorRow = function(tableRow) {
        self.replaceItem(self.adcsAngVectorRows, tableRow);
    }
    self.replaceAdcsTorqueRow = function(tableRow) {
        self.replaceItem(self.adcsTorqueRows, tableRow);
    }
    self.replaceAdcsMagnetMomentVectorRow = function(tableRow) {
        self.replaceItem(self.adcsMagnetMomentVectorRows, tableRow);
    }

    this.flashUpdatedElement = function(elem) {
        $(elem).effect("highlight", {
            backgroundColor : "#96eecd"
        }, 500);
    }

    this.loadDataFromFile = function() {

        $
                .ajax({
                    global : false,
                    type : "POST",
                    cache : false,
                    dataType : "json",
                    data : ({
                        action : 'read'
                    }),
                    url : 'dataToFile.php',
                    success : function(result) {
                        var data = JSON.parse(result);

                        /*
                         * Cannot use knockout's mapping plugin for some reason
                         * (data-binding didn't work after loading data from
                         * file). Have to manually map all arrays.
                         */

                        (self.epsSolarRows).push.apply(self.epsSolarRows,
                                data.epsSolarRows);
                        (self.epsCoilRows).push.apply(self.epsCoilRows,
                                data.epsCoilRows);
                        (self.epsBatteryRows).push.apply(self.epsBatteryRows,
                                data.epsBatteryRows);
                        (self.epsBatARows).push.apply(self.epsBatARows,
                                data.epsBatARows);
                        self.batProgressA(data.batProgressA);
                        (self.epsBatBRows).push.apply(self.epsBatBRows,
                                data.epsBatBRows);
                        self.batProgressB(data.batProgressB);
                        (self.epsRegRows).push.apply(self.epsRegRows,
                                data.epsRegRows);
                        (self.epsCtlPlRows).push.apply(self.epsCtlPlRows,
                                data.epsCtlPlRows);
                        (self.epsCtlRows).push.apply(self.epsCtlRows,
                                data.epsCtlRows);
                        (self.epsSwitchRows).push.apply(self.epsSwitchRows,
                                data.epsSwitchRows);
                        (self.epsTempRows).push.apply(self.epsTempRows,
                                data.epsTempRows);

                        (self.cdhsRows).push
                                .apply(self.cdhsRows, data.cdhsRows);

                        (self.comRows).push.apply(self.comRows, data.comRows);

                        (self.adcsAngVectorRows).push.apply(
                                self.adcsAngVectorRows, data.adcsAngVectorRows);
                        (self.adcsGyroRows).push.apply(self.adcsGyroRows,
                                data.adcsGyroRows);
                        (self.adcsGyroVectorRows).push.apply(
                                self.adcsGyroVectorRows,
                                data.adcsGyroVectorRows);
                        (self.adcsMagnetMomentVectorRows).push.apply(
                                self.adcsMagnetMomentVectorRows,
                                data.adcsMagnetMomentVectorRows);
                        (self.adcsMagnetoRows).push.apply(self.adcsMagnetoRows,
                                data.adcsMagnetoRows);
                        (self.adcsMagnetoVectorRows).push.apply(
                                self.adcsMagnetoVectorRows,
                                data.adcsMagnetoVectorRows);
                        (self.adcsSun1Rows).push.apply(self.adcsSun1Rows,
                                data.adcsSun1Rows);
                        (self.adcsSun2Rows).push.apply(self.adcsSun2Rows,
                                data.adcsSun2Rows);
                        (self.adcsSunVectorRows).push.apply(
                                self.adcsSunVectorRows, data.adcsSunVectorRows);
                        (self.adcsTorqueRows).push.apply(self.adcsTorqueRows,
                                data.adcsTorqueRows);

                        (self.camRows).push.apply(self.camRows, data.camRows);
                    }
                });
    }

}
var tableViewModel = new TableDataViewModel();
tableViewModel.loadDataFromFile();
ko.applyBindings(tableViewModel);

openSocket();

setInterval(function() {
    saveData2File();
    console.log("Saving...");
}, 15000);

function saveData2File() {
    var data = ko.toJSON(tableViewModel);/* data in JSON format */
    $.ajax({
        global : false,
        type : "POST",
        cache : false,
        dataType : "json",
        data : ({
            action : 'write',
            data : data
        }),
        url : 'dataToFile.php'
    });
}

function batteryProgress(description, value) {
    var batA = "Battery A";
    var batB = "Battery B";
    var battery;
    if (description.indexOf(batA) > -1) {
        battery = batA;
    } else {
        battery = batB;
    }
    if (description == battery + " charge") {
        if (value == "ON") {
            if (battery == batA) {
                tableViewModel
                        .batProgressA("<progress value=1 class=charge></progress>");
            } else if (battery == batB) {
                tableViewModel
                        .batProgressB("<progress value=1 class=charge></progress>");
            }
        } else {
            if (battery == batA) {
                tableViewModel
                        .batProgressA("<progress value=1 class=idle></progress>");
            } else if (battery == batB) {
                tableViewModel
                        .batProgressB("<progress value=1 class=idle></progress>");
            }
        }
    }
    if (description == battery + " discharge") {
        if (value == "ON") {
            if (battery == batA) {
                tableViewModel
                        .batProgressA("<progress value=1 class=discharge></progress>");
            } else if (battery == batB) {
                tableViewModel
                        .batProgressB("<progress value=1 class=discharge></progress>");
            }
        }
    }
}

function removeString(tableRow, remove) {
    str = tableRow.description;
    str = str.replace(remove, '');
    tableRow.description = str;
}