// Function for grouping data with X,Y,Z values into a single row

function groupVectorsXYZ(description, value) {

    var tableRow;

    if (description.indexOf("Magnetic field vector") > -1) {
        tableRow = findRow(tableViewModel.adcsMagnetoVectorRows(),
                "Magnetic field vector X/Y/Z");
        tableViewModel.replaceAdcsMagnetoVectorRow(groupXYZ(tableRow,
                description, value));
    } else if (description.indexOf("Last magnetometer 0") > -1) {
        tableRow = findRow(tableViewModel.adcsMagnetoRows(),
                "Last magnetometer 0 X/Y/Z raw");
        tableViewModel.replaceAdcsMagnetoRow(groupXYZ(tableRow, description,
                value));
    } else if (description.indexOf("Last magnetometer 1") > -1) {
        tableRow = findRow(tableViewModel.adcsMagnetoRows(),
                "Last magnetometer 1 X/Y/Z raw");
        tableViewModel.replaceAdcsMagnetoRow(groupXYZ(tableRow, description,
                value));
    } else if (description.indexOf("Gyro vector") > -1) {
        tableRow = findRow(tableViewModel.adcsGyroVectorRows(),
                "Gyro vector X/Y/Z");
        tableViewModel.replaceAdcsGyroVectorRow(groupXYZ(tableRow, description,
                value));
    } else if (description.indexOf("Last gyro 0") > -1) {
        tableRow = findRow(tableViewModel.adcsGyroRows(),
                "Last gyro 0 X/Y/Z raw");
        tableViewModel
                .replaceAdcsGyroRow(groupXYZ(tableRow, description, value));
    } else if (description.indexOf("Last gyro 1") > -1) {
        tableRow = findRow(tableViewModel.adcsGyroRows(),
                "Last gyro 1 X/Y/Z raw");
        tableViewModel
                .replaceAdcsGyroRow(groupXYZ(tableRow, description, value));
    } else if (description.indexOf("Last gyro 2") > -1) {
        tableRow = findRow(tableViewModel.adcsGyroRows(),
                "Last gyro 2 X/Y/Z raw");
        tableViewModel
                .replaceAdcsGyroRow(groupXYZ(tableRow, description, value));
    } else if (description.indexOf("Last gyro 3") > -1) {
        tableRow = findRow(tableViewModel.adcsGyroRows(),
                "Last gyro 3 X/Y/Z raw");
        tableViewModel
                .replaceAdcsGyroRow(groupXYZ(tableRow, description, value));
    } else if (description.indexOf("Sun vector") > -1) {
        tableRow = findRow(tableViewModel.adcsSunVectorRows(),
                "Sun vector X/Y/Z");
        tableViewModel.replaceAdcsSunVectorRow(groupXYZ(tableRow, description,
                value));
    } else if (description.indexOf("coordinate of the angular velocity vector") > -1) {
        tableRow = findRow(tableViewModel.adcsAngVectorRows(),
                "Angular velocity vector X/Y/Z");
        tableViewModel.replaceAdcsAngVectorRow(groupXYZ(tableRow, description,
                value));
    } else if (description.indexOf("Torque") > -1) {
        tableRow = findRow(tableViewModel.adcsTorqueRows(), "Torque X/Y/Z");
        tableViewModel.replaceAdcsTorqueRow(groupXYZ(tableRow, description,
                value));
    } else if (description.indexOf("component of the magnetic moment vector") > -1) {
        tableRow = findRow(tableViewModel.adcsMagnetMomentVectorRows(),
                "Magnetic moment vector X/Y/Z");
        tableViewModel.replaceAdcsMagnetMomentVectorRow(groupXYZ(tableRow,
                description, value));
    }
}

function groupXYZ(tableRow, description, value) {
    var previousValue;
    previousValue = tableRow.value;

    if (description.indexOf("X") > -1) {
        newValue = xyz(previousValue, value, 0);
    } else if (description.indexOf("Y") > -1) {
        newValue = xyz(previousValue, value, 1);
    } else if (description.indexOf("Z") > -1) {
        newValue = xyz(previousValue, value, 2);
    }
    newRow = new TableData(tableRow.name, tableRow.description, tableRow.value,
            tableRow.unit);
    newRow.value = newValue;
    return newRow;
}

function xyz(previousValue, value, pos) {
    var valueArray;
    var newValue;

    valueArray = previousValue.split("/");
    valueArray[pos] = value;
    newValue = valueArray.join("/");
    return newValue;
}
