// Finds corresponding table row from tableViewModel and changes
// onOff value according to data from websocket

function switch2onOff(description, value) {

    var dataRow;
    if (description == "REG SPBA") {
        dataRow = findRow(tableViewModel.epsBatteryRows(), "SPB A CS");
        tableViewModel.replaceEpsBatteryRow(createRow(dataRow, value));
    } else if (description == "REG SPBB") {
        dataRow = findRow(tableViewModel.epsBatteryRows(), "SPB B CS");
        tableViewModel.replaceEpsBatteryRow(createRow(dataRow, value));
    } else if (description == "REG 12VA") {
        dataRow = findRow(tableViewModel.epsRegRows(), "Reg 12V A CS");
        tableViewModel.replaceEpsRegRow(createRow(dataRow, value));
    } else if (description == "REG 12VB") {
        dataRow = findRow(tableViewModel.epsRegRows(), "Reg 12V B CS");
        tableViewModel.replaceEpsRegRow(createRow(dataRow, value));
    } else if (description == "REG 5VA") {
        dataRow = findRow(tableViewModel.epsRegRows(), "Reg 5V A CS");
        tableViewModel.replaceEpsRegRow(createRow(dataRow, value));
    } else if (description == "REG 5VB") {
        dataRow = findRow(tableViewModel.epsRegRows(), "Reg 5V B CS");
        tableViewModel.replaceEpsRegRow(createRow(dataRow, value));
    } else if (description == "REG 3V3A") {
        dataRow = findRow(tableViewModel.epsRegRows(), "Reg 3V3 A CS");
        tableViewModel.replaceEpsRegRow(createRow(dataRow, value));
    } else if (description == "REG 3V3B") {
        dataRow = findRow(tableViewModel.epsRegRows(), "Reg 3V3 B CS");
        tableViewModel.replaceEpsRegRow(createRow(dataRow, value));
    } else if (description == "CTL COM 3V3") {
        dataRow = findRow(tableViewModel.epsCtlRows(), "CTL COM 3V3");
        tableViewModel.replaceEpsCtlRow(createRow(dataRow, value));
    } else if (description == "CTL COM 5V") {
        dataRow = findRow(tableViewModel.epsCtlRows(), "CTL COM 5V");
        tableViewModel.replaceEpsCtlRow(createRow(dataRow, value));
    } else if (description == "CTL CDHS 3V3 BSW") {
        dataRow = findRow(tableViewModel.epsCtlRows(), "CTL CDHS bsw 3V3");
        tableViewModel.replaceEpsCtlRow(createRow(dataRow, value));
    } else if (description == "CTL CDHS 3V3 A") {
        dataRow = findRow(tableViewModel.epsCtlRows(), "CTL CDHS A 3V3");
        tableViewModel.replaceEpsCtlRow(createRow(dataRow, value));
    } else if (description == "CTL CDHS 3V3 B") {
        dataRow = findRow(tableViewModel.epsCtlRows(), "CTL CDHS B 3V3");
        tableViewModel.replaceEpsCtlRow(createRow(dataRow, value));
    } else if (description == "CTL ADCS 5V") {
        dataRow = findRow(tableViewModel.epsCtlRows(), "CTL ADCS 5v");
        tableViewModel.replaceEpsCtlRow(createRow(dataRow, value));
    } else if (description == "CTL CAM 3V3") {
        dataRow = findRow(tableViewModel.epsCtlRows(), "CTL CAM 3V3");
        tableViewModel.replaceEpsCtlRow(createRow(dataRow, value));
    } else if (description == "CTL PL5V") {
        dataRow = findRow(tableViewModel.epsCtlPlRows(), "CTL PL 5V");
        tableViewModel.replaceEpsCtlPlRow(createRow(dataRow, value));
    } else if (description == "CTL PL3V3") {
        dataRow = findRow(tableViewModel.epsCtlPlRows(), "CTL PL 3V3");
        tableViewModel.replaceEpsCtlPlRow(createRow(dataRow, value));
    }
}

function findRow(array, description) {
    var dataRow = ko.utils.arrayFirst(array, function(item) {
        return item.description === description;
    });
    return dataRow;
}

function createRow(dataRow, onOffValue) {
    newRow = new TableData(dataRow.name, dataRow.description, dataRow.value,
            dataRow.unit);
    newRow.onOff = onOffValue;
    return newRow;
}