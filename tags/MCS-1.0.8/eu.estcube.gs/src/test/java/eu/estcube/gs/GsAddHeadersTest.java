/**
 *
 */
package eu.estcube.gs;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.hbird.exchange.constants.StandardArguments;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.common.TimestampExtractor;
import eu.estcube.gs.contact.LocationContactEventHolder;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class GsAddHeadersTest {

    private static final String SERVICE = "service";
    private static final String SATELLITE = "satellite";
    private static final String GROUND_STATION = "groundSation";
    // private static final Long NOW = System.currentTimeMillis();
    private static final String EVENT_ID = "GS:SAT:1234";
    private static final long ORBIT_NR = 1234;

    @Mock
    private GsDriverConfiguration config;

    @Mock
    private Exchange exchange;

    @Mock
    private Message in;

    @Mock
    private Message out;

    @Mock
    private LocationContactEventHolder eventHolder;

    @Mock
    private TimestampExtractor timestampExtractor;

    @InjectMocks
    private GsAddHeaders processor;

    private Object body;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        body = new Object();
        inOrder = inOrder(config, exchange, in, out, eventHolder);
        when(exchange.getIn()).thenReturn(in);
        when(exchange.getOut()).thenReturn(out);
        when(config.getServiceId()).thenReturn(SERVICE);
        when(config.getGroundstationId()).thenReturn(GROUND_STATION);
        when(eventHolder.getSatelliteId()).thenReturn(SATELLITE);
        when(in.getBody()).thenReturn(body);
    }

    /**
     * Test method for
     * {@link eu.estcube.gs.GsAddHeaders#process(org.apache.camel.Exchange)}.
     *
     * @throws Exception
     */
    @Test
    public void testProcessNoEventId() throws Exception {
        when(eventHolder.getContactId(Mockito.anyLong())).thenReturn(null);
        when(eventHolder.getOrbitNumber(Mockito.anyLong())).thenReturn(ORBIT_NR);
        when(timestampExtractor.getTimestamp((Message) Mockito.any())).thenReturn(System.currentTimeMillis());
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(eventHolder, times(1)).getContactId(Mockito.anyLong());
        inOrder.verify(eventHolder, times(1)).getOrbitNumber(Mockito.anyLong());
        // inOrder.verify(out, times(1)).setHeader(StandardArguments.TIMESTAMP,
        // NOW);
        inOrder.verify(config, times(1)).getServiceId();
        inOrder.verify(out, times(1)).setHeader(StandardArguments.ISSUED_BY, SERVICE);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.SATELLITE_ID, SATELLITE);
        inOrder.verify(config, times(1)).getGroundstationId();
        inOrder.verify(out, times(1)).setHeader(StandardArguments.GROUND_STATION_ID, GROUND_STATION);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.CONTACT_ID, GsAddHeaders.DEFAULT_CONTACT_ID);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.ORBIT_NUMBER, GsAddHeaders.DEFAULT_ORBIT_NUMBER);
        inOrder.verify(in, times(1)).getBody();
        inOrder.verify(out, times(1)).setHeader(StandardArguments.CLASS, body.getClass().getSimpleName());
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.gs.GsAddHeaders#process(org.apache.camel.Exchange)}.
     *
     * @throws Exception
     */
    @Test
    public void testProcess() throws Exception {
        when(eventHolder.getContactId(Mockito.anyLong())).thenReturn(EVENT_ID);
        when(eventHolder.getOrbitNumber(Mockito.anyLong())).thenReturn(ORBIT_NR);
        when(timestampExtractor.getTimestamp((Message) Mockito.any())).thenReturn(System.currentTimeMillis());
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(eventHolder, times(1)).getContactId(Mockito.anyLong());
        inOrder.verify(eventHolder, times(1)).getOrbitNumber(Mockito.anyLong());
        // inOrder.verify(out, times(1)).setHeader(StandardArguments.TIMESTAMP,
        // NOW);
        inOrder.verify(config, times(1)).getServiceId();
        inOrder.verify(out, times(1)).setHeader(StandardArguments.ISSUED_BY, SERVICE);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.SATELLITE_ID, SATELLITE);
        inOrder.verify(config, times(1)).getGroundstationId();
        inOrder.verify(out, times(1)).setHeader(StandardArguments.GROUND_STATION_ID, GROUND_STATION);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.CONTACT_ID, EVENT_ID);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.ORBIT_NUMBER, ORBIT_NR);
        inOrder.verify(in, times(1)).getBody();
        inOrder.verify(out, times(1)).setHeader(StandardArguments.CLASS, body.getClass().getSimpleName());
        inOrder.verifyNoMoreInteractions();
    }
}
