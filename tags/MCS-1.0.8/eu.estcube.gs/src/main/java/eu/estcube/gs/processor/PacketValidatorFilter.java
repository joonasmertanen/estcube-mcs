package eu.estcube.gs.processor;

import java.util.regex.Pattern;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.codec.ax25.impl.Ax25AddressDecoder;
import eu.estcube.codec.ax25.impl.Ax25UIFrameKissDecoder;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.domain.transport.tnc.TncFrame;

/**
 * Decodes tncFrame, takes the source and destination string representations and
 * validates them against a regular expression.
 * 
 * @author Aivar Lobjakas
 * @since 24.07.2014
 */
@Component
public class PacketValidatorFilter implements Predicate {

    private static final Logger LOG = LoggerFactory.getLogger(PacketValidatorFilter.class);

    @Autowired
    private Ax25UIFrameKissDecoder decoder;

    private Ax25AddressDecoder addressDecoder = new Ax25AddressDecoder();

    @Override
    public boolean matches(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        out.copyFrom(in);
        try {
            TncFrame tncFrame = in.getBody(TncFrame.class);
            if (tncFrame == null) {
                throw new IllegalArgumentException("No TncFrame available in message body");
            }

            Ax25UIFrame ax25UIFrame = decoder.decode(tncFrame);
            byte[] destAddr = ax25UIFrame.getDestAddr();
            byte[] srcAddr = ax25UIFrame.getSrcAddr();

            String destAddrStr = addressDecoder.toString(destAddr);
            destAddrStr = destAddrStr.substring(0, destAddrStr.indexOf("-"));
            String srcAddrStr = addressDecoder.toString(srcAddr);
            srcAddrStr = srcAddrStr.substring(0, srcAddrStr.indexOf("-"));

            String regex = "\\b(([A-Z]{1,2})|([A-Z][0-9]))[0-9][A-Z]{1,3}\\b";
            if (Pattern.matches(regex, destAddrStr) && Pattern.matches(regex, srcAddrStr)) {
                return true;
            }

        } catch (Exception e) {
            LOG.error("Failed to process TncFrame to Ax25UIFrame; {}", e.getMessage());
            exchange.setException(e);
        }
        return false;
    }

}
