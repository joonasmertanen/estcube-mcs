package eu.estcube.commanding.rotator1;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.hbird.exchange.navigation.PointingData;
import org.junit.Test;

import eu.estcube.commanding.analysis.OptimizationType;
import eu.estcube.commanding.optimizators.Optimization;

public class Rotator1NoCrossElLowTest {
	public static List<PointingData> coordinatesResult = new ArrayList<PointingData>();
	public static List<PointingData> coordinatesExpected = new ArrayList<PointingData>();
	public static Rotator1 rotator1 = new Rotator1(7.5);

	public void fillLists(final double[] azArrayResult,
			final double[] elArrayResult, final double[] azArrayExpected,
			final double[] elArrayExpected) {
		for (int i = 0; i < azArrayResult.length; i++) {
			coordinatesExpected.add(new PointingData(0L, azArrayExpected[i],
					elArrayExpected[i], 123.0, "test", "test"));
			coordinatesResult.add(new PointingData(0L, azArrayResult[i],
					elArrayResult[i], 123.0, "test", "test"));
		}

	}

	@Test
	public void NoCrossElLow() {
		final double[] azArrayResult = { 159.96, 158.05, 158.05, 47.04 };
		final double[] elArrayResult = { 25.04, 33.89, 46.54, 64.87 };
		final double[] azArrayExpected = { 159.96, 158.05, 158.05, 47.04 };
		final double[] elArrayExpected = { 25.04, 33.89, 46.54, 64.87 };

		fillLists(azArrayResult, elArrayResult, azArrayExpected,
				elArrayExpected);
		final Optimization optimizationCase = new Optimization(
				OptimizationType.ROT_1_NO_CROSS_EL_LOW, rotator1.getMaxAz(),
				rotator1.getMaxEl(), rotator1.getReceivingSector());

		coordinatesResult = optimizationCase.optimize(coordinatesResult);

		for (int i = 0; i < coordinatesResult.size(); i++) {
			assertEquals(coordinatesResult.get(i).getAzimuth(),
					coordinatesExpected.get(i).getAzimuth());
			assertEquals(coordinatesResult.get(i).getElevation(),
					coordinatesExpected.get(i).getElevation());

		}
		coordinatesResult.clear();
		coordinatesExpected.clear();

	}
}
