package eu.estcube.commanding.rotator1;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.hbird.exchange.navigation.PointingData;
import org.junit.Test;

import eu.estcube.commanding.analysis.OptimizationType;
import eu.estcube.commanding.calculation.Calculation;
import eu.estcube.commanding.optimizators.GlobalFunctions;
import eu.estcube.commanding.optimizators.Optimization;

public class Rotator1CrossElLowTest {
	public static List<PointingData> coordinatesResult = new ArrayList<PointingData>();
	public static List<PointingData> coordinatesExpected = new ArrayList<PointingData>();
	public static Rotator1 rotator1 = new Rotator1(7.5);

	@Test
	public void crossElLowAntiClockwise() {
		final double[] azArrayResult = { 56.1, 38.7, 24.0, 12.9, 4.0, 358.91,
				354.43, 350.99 };
		final double[] elArrayResult = { 35.5, 33.95, 30.08, 25.28, 20.47,
				16.06, 12.13, 8.62 };
		final double[] azArrayExpected = { 236.0, 218.0, 204.0, 192.0, 184.0,
				178.0, 174.0, 170.0 };
		final double[] elArrayExpected = { 145.0, 147.0, 150.0, 155.0, 160.0,
				164.0, 168.0, 172.0 };

		fillLists(azArrayResult, elArrayResult, azArrayExpected,
				elArrayExpected);

		final Optimization optimizationCase = new Optimization(
				OptimizationType.ROT_1_CROSS_EL_LOW, rotator1.getMaxAz(),
				rotator1.getMaxEl(), rotator1.getReceivingSector());

		coordinatesResult = optimizationCase.optimize(coordinatesResult);

		for (int i = 0; i < coordinatesResult.size(); i++) {
			assertEquals(coordinatesResult.get(i).getAzimuth(),
					coordinatesExpected.get(i).getAzimuth());
			assertEquals(coordinatesResult.get(i).getElevation(),
					coordinatesExpected.get(i).getElevation());

		}

		coordinatesResult.clear();
		coordinatesExpected.clear();

	}

	@Test
	public void crossElLowAntiClockwiseEnd() {
		final double[] azArrayResult = { 56.1, 38.7, 24.0, 12.9, 4.0, 358.91 };
		final double[] elArrayResult = { 35.5, 33.95, 30.08, 25.28, 20.47,
				16.06, };
		final double[] azArrayExpected = { 56.1, 38.7, 24.0, 12.9, 4.0 };
		final double[] elArrayExpected = { 35.5, 33.95, 30.08, 25.28, 20.47 };

		fillLists(azArrayResult, elArrayResult, azArrayExpected,
				elArrayExpected);

		final Optimization optimizationCase = new Optimization(
				OptimizationType.ROT_1_CROSS_EL_LOW, rotator1.getMaxAz(),
				rotator1.getMaxEl(), rotator1.getReceivingSector());

		coordinatesResult = optimizationCase.optimize(coordinatesResult);



		for (int i = 0; i < coordinatesResult.size(); i++) {
			assertEquals(coordinatesResult.get(i).getAzimuth(),
					coordinatesExpected.get(i).getAzimuth());
			assertEquals(coordinatesResult.get(i).getElevation(),
					coordinatesExpected.get(i).getElevation());

		}

		coordinatesResult.clear();
		coordinatesExpected.clear();

	}

	public void fillLists(final double[] azArrayResult,
			final double[] elArrayResult, final double[] azArrayExpected,
			final double[] elArrayExpected) {
		for (int i = 0; i < azArrayResult.length; i++) {

			coordinatesResult.add(new PointingData(0L, azArrayResult[i],
					elArrayResult[i], 123.0, "test", "test"));
		}

		for (int i = 0; i < azArrayExpected.length; i++) {
			coordinatesExpected.add(new PointingData(0L, azArrayExpected[i],
					elArrayExpected[i], 123.0, "test", "test"));
		}

	}
}
