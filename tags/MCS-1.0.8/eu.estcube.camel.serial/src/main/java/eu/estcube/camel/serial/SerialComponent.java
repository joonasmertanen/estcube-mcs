package eu.estcube.camel.serial;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.impl.DefaultComponent;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.service.IoHandler;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.transport.serial.SerialAddress;
import org.apache.mina.transport.serial.SerialAddress.DataBits;
import org.apache.mina.transport.serial.SerialAddress.FlowControl;
import org.apache.mina.transport.serial.SerialAddress.Parity;
import org.apache.mina.transport.serial.SerialAddress.StopBits;
import org.apache.mina.transport.serial.SerialConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Accepted parameters:
 * <table>
 * <tr>
 * <td>Name</td>
 * <td>Default value</td>
 * <td>Valid values</td>
 * <td>Notes</td>
 * </tr>
 * <tr>
 * <td>{@value #BAUD}</td>
 * <td>1200</td>
 * <td>Any valid baud rate as integer</td>
 * <td>Baud rate of serial connection</td>
 * </tr>
 * <tr>
 * <td>{@value #DATA_BITS}</td>
 * <td>8</td>
 * <td>5, 6, 7, 8</td>
 * <td>Data bits in the serial protocol</td>
 * </tr>
 * <tr>
 * <td>{@value #STOP_BITS}</td>
 * <td>1</td>
 * <td>1, 1.5, 2</td>
 * <td>Stop bits in the serial protocol</td>
 * </tr>
 * <tr>
 * <td>{@value #PARITY}</td>
 * <td>NONE</td>
 * <td>EVEN, MARK, NONE, ODD, SPACE</td>
 * <td>Parity in the serial protocol</td>
 * </tr>
 * <tr>
 * <td>{@value #FLOW_CONTROL}</td>
 * <td>NONE</td>
 * <td>NONE, RTSCTS_IN, RTSCTS_IN_OUT, RTSCTS_OUT, XONXOFF_IN, XONXOFF_IN_OUT,
 * XONXOFF_OUT</td>
 * <td>Flow control in the serial protocol</td>
 * </tr>
 * <tr>
 * <td>{@value #FILTERS}</td>
 * <td>Empty list of filters</td>
 * <td>Valid reference to {@link List} of {@link IoFilter}s in
 * {@link CamelContext}.</td>
 * <td>Referencial {@link List} of {@link IoFilter}s to add to the
 * {@link IoHandler}.</td>
 * </tr>
 * </table>
 */
public class SerialComponent extends DefaultComponent {

    public static final String BAUD = "baud";

    public static final String DATA_BITS = "dataBits";
    public static final String STOP_BITS = "stopBits";
    public static final String PARITY = "parity";
    public static final String FLOW_CONTROL = "flowControl";
    public static final String FILTERS = "filters";

    public static final int DEFAULT_BAUD = 1200;
    public static final DataBits DEFAULT_DATA_BITS = DataBits.DATABITS_8;
    public static final StopBits DEFAULT_STOP_BITS = StopBits.BITS_1;
    public static final Parity DEFAULT_PARITY = Parity.NONE;
    public static final FlowControl DEFAULT_FLOW_CONTROL = FlowControl.NONE;
    public static final List<IoFilter> DEFAULT_LIST_OF_FILTERS = createDefaultFilter();

    private static final Logger LOG = LoggerFactory.getLogger(SerialComponent.class);

    private final Map<String, SerialEndpoint> endpoints = new ConcurrentHashMap<String, SerialEndpoint>();

    private final ExecutorService executor = Executors.newCachedThreadPool();

    /** @{inheritDoc . */
    @Override
    protected void doStop() throws Exception {
        super.doStop();
        doStop(executor);
    }

    /**
     * 
     */
    void doStop(ExecutorService executor) {
        LOG.debug("Stopping SerialComponent ExecutorService");
        executor.shutdown();
        try {
            executor.awaitTermination(3, TimeUnit.SECONDS);
            LOG.info("SerialComponent ExecutorService is stopped on time");
        } catch (InterruptedException e) {
            LOG.warn("SerialComponent ExecutorService is NOT stopped on time; forcing shutdown");
            executor.shutdownNow();
        }
    }

    /** @{inheritDoc . */
    @Override
    protected Endpoint createEndpoint(String uri, String remaining, Map<String, Object> parameters) throws Exception {
        synchronized (endpoints) {
            SerialEndpoint endpoint = endpoints.get(remaining);
            if (endpoint == null) {
                endpoint = createSerialEndpoint(uri, remaining, parameters);
                endpoints.put(remaining, endpoint);
            } else {
                clearParameters(parameters);
            }
            return endpoint;
        }
    }

    static DataBits getDataBits(int val) {
        switch (val) {
            case 5:
                return DataBits.DATABITS_5;
            case 6:
                return DataBits.DATABITS_6;
            case 7:
                return DataBits.DATABITS_7;
            case 8:
                return DataBits.DATABITS_8;
            default:
                return DEFAULT_DATA_BITS;
        }
    }

    static StopBits getStopBits(Double value) {
        if (value.equals(1.0D)) {
            return StopBits.BITS_1;
        }
        else if (value.equals(1.5D)) {
            return StopBits.BITS_1_5;
        }
        else if (value.equals(2.0D)) {
            return StopBits.BITS_2;
        }
        else {
            return DEFAULT_STOP_BITS;
        }
    }

    static <E extends Enum<E>> E getEnumValue(Class<E> c, String value, E defaultValue) {
        try {
            E result = Enum.valueOf(c, value);
            return result == null ? defaultValue : result;
        } catch (Exception e) {
            return defaultValue;
        }
    }

    protected void clearParameters(Map<String, Object> parameters) {
        parameters.remove(BAUD);
        parameters.remove(DATA_BITS);
        parameters.remove(STOP_BITS);
        parameters.remove(PARITY);
        parameters.remove(FLOW_CONTROL);
        parameters.remove(FILTERS);
    }

    protected SerialEndpoint createSerialEndpoint(String uri, String portName, Map<String, Object> parameters)
            throws Exception {
        SerialConfig config = new SerialConfig();
        config.setPort(portName);
        config.setFilters(resolveAndRemoveReferenceListParameter(parameters, FILTERS, IoFilter.class,
                DEFAULT_LIST_OF_FILTERS));
        config.setDataBits(getDataBits(getAndRemoveParameter(parameters, DATA_BITS, Integer.class, -1)));
        config.setStopBits(getStopBits(getAndRemoveParameter(parameters, STOP_BITS, Double.class, null)));
        config.setParity(getEnumValue(Parity.class, getAndRemoveParameter(parameters, PARITY, String.class),
                DEFAULT_PARITY));
        config.setFlowControl(getEnumValue(FlowControl.class,
                getAndRemoveParameter(parameters, FLOW_CONTROL, String.class), DEFAULT_FLOW_CONTROL));
        setProperties(config, parameters);
        return createSerialEndpoint(uri, config);
    }

    protected SerialEndpoint createSerialEndpoint(String uri, SerialConfig config) {
        LOG.info(
                "Creating new SerialEndpoint for port {} - baud: {}; dataBits: {}; stopBits: {}; parity: {}; flowControl: {}",
                new Object[] {
                        config.getPort(),
                        config.getBaud(),
                        config.getDataBits(),
                        config.getStopBits(),
                        config.getParity(),
                        config.getFlowControl()
                });
        SerialAddress address = new SerialAddress(
                config.getPort(),
                config.getBaud(),
                config.getDataBits(),
                config.getStopBits(),
                config.getParity(),
                config.getFlowControl());
        return new SerialEndpoint(uri, this, address, new SerialHandler(),
                new SessionFactory(config, executor, new SerialConnector(executor)));
    }

    static List<IoFilter> createDefaultFilter() {
        IoFilter filter = new ProtocolCodecFilter(new DefaultCodecFactory());
        return Arrays.asList(filter);
    }
}
