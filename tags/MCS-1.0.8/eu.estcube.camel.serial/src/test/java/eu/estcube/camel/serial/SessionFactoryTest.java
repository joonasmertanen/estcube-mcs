package eu.estcube.camel.serial;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

import org.apache.mina.core.filterchain.DefaultIoFilterChainBuilder;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.transport.serial.SerialAddress;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SessionFactoryTest {

    @Mock
    private IoConnector connector;

    @Mock
    private SerialAddress address;

    @Mock
    private SerialHandler handler;

    @Mock
    private ConnectFuture connectFuture;

    @Mock
    private IoSession session;

    @Mock
    private ExecutorService executor;

    @Mock
    private SerialConfig config;

    @Mock
    private IoFilter filter1;

    @Mock
    private IoFilter filter2;

    @Mock
    private DefaultIoFilterChainBuilder filterChain;

    private List<IoFilter> filters;

    private SessionFactory sessionFactory;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        sessionFactory = new SessionFactory(config, executor, connector);
        filters = new ArrayList<IoFilter>();
        filters.add(filter1);
        filters.add(filter2);
        inOrder = inOrder(connector, address, handler, connectFuture, session, executor, config, filter1, filter2,
                filterChain);
        when(connector.connect(address)).thenReturn(connectFuture);
        when(connectFuture.getSession()).thenReturn(session);
        when(config.getFilters()).thenReturn(filters);
        when(connector.getFilterChain()).thenReturn(filterChain);
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SessionFactory#openSession(org.apache.mina.transport.serial.SerialAddress, eu.estcube.camel.serial.SerialHandler)}
     * .
     * 
     * @throws InterruptedException
     */
    @Test
    public void testOpenSession() throws InterruptedException {
        assertEquals(session, sessionFactory.openSession(address, handler));
        inOrder.verify(connector, times(1)).setHandler(handler);
        inOrder.verify(config, times(1)).getFilters();
        inOrder.verify(connector, times(1)).getFilterChain();
        inOrder.verify(filterChain, times(1)).addFirst(anyString(), eq(filter1));
        inOrder.verify(filterChain, times(1)).addFirst(anyString(), eq(filter2));
        inOrder.verify(connector, times(1)).connect(address);
        inOrder.verify(connectFuture, times(1)).await();
        inOrder.verify(connectFuture, times(1)).getSession();
        inOrder.verify(session, times(1)).setAttribute(SerialEndpoint.ATTRIBUTE_SERIAL_ADDRESS, address);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SessionFactory#addIoFilters(IoConnector, List)}
     * .
     * 
     * @throws InterruptedException
     */
    @Test
    public void testAddIoFilters() throws InterruptedException {
        sessionFactory.addIoFilters(connector, filters);
        inOrder.verify(connector, times(1)).getFilterChain();
        inOrder.verify(filterChain, times(1)).addFirst(anyString(), eq(filter1));
        inOrder.verify(filterChain, times(1)).addFirst(anyString(), eq(filter2));
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SessionFactory#addIoFilters(IoConnector, List)}
     * .
     * 
     * @throws InterruptedException
     */
    @Test
    public void testAddIoNullFilters() throws InterruptedException {
        sessionFactory.addIoFilters(connector, null);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.camel.serial.SessionFactory#close()}.
     * 
     * @throws InterruptedException
     */
    @Test
    public void testClose() throws InterruptedException {
        sessionFactory.close();
        ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
        inOrder.verify(executor, times(1)).execute(captor.capture());
        captor.getValue().run();
        inOrder.verify(connector, times(1)).getDefaultRemoteAddress();
        inOrder.verify(connector, times(1)).dispose();
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.camel.serial.SessionFactory#close()}.
     * 
     * @throws InterruptedException
     */
    @Test
    public void testCloseTwoTimes() throws InterruptedException {
        sessionFactory.close();
        sessionFactory.close();
        ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
        inOrder.verify(executor, times(1)).execute(captor.capture());
        captor.getValue().run();
        inOrder.verify(connector, times(1)).getDefaultRemoteAddress();
        inOrder.verify(connector, times(1)).dispose();
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SessionFactory#createConnectorDisposingRunnable(IoConnector)}
     * .
     * 
     * @throws InterruptedException
     */
    @Test
    public void testCreateConnectorDisposingRunnable() throws InterruptedException {
        Runnable runnable = sessionFactory.createConnectorDisposingRunnable(connector);
        runnable.run();
        inOrder.verify(connector, times(1)).getDefaultRemoteAddress();
        inOrder.verify(connector, times(1)).dispose();
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SessionFactory#createDefaultFilter)} .
     */
    @Test
    public void testCreateDefaultFilter() {
        List<IoFilter> defaultFilters = SerialComponent.createDefaultFilter();
        assertNotNull(defaultFilters);
        assertEquals(1, defaultFilters.size());
    }
}
