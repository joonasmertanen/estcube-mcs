package eu.estcube.archiver.raw.sql;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import eu.estcube.archiver.notifier.MailNotifier;

public class AbstractSqlFrameArchiverTest {
	private AbstractSqlFrameArchiver abstractSqlFrameArchiver;
	private DataSource dataSource;
	private MailNotifier notifier;

	@Before
	public void setUp() throws Exception {
		dataSource = Mockito.mock(DataSource.class);
		notifier = Mockito.mock(MailNotifier.class);
		abstractSqlFrameArchiver = new AbstractSqlFrameArchiver(dataSource, notifier) {

			@Override
			public String getTimestampFunction() {
				return "NOW";
			}

			@Override
			public String getTable() {
				return "TABLE";
			}

			@Override
			public String getDetailsTable() {
				return "DETAILS_TABLE";
			}

			@Override
			public String getDetailsAutoIncrementFunction() {
				return "AUTO_INCR";
			}

			@Override
			public String[] getColumns() {
				return new String[] { "A", "B" };
			}

			@Override
			public String getAutoIncrementFunction() {
				return "SEQ.NEXTVAL";
			}
		};

	}

	@Test
	public void testGetDataSource() {
		assertSame(dataSource, abstractSqlFrameArchiver.getDataSource());
	}

	@Test
	public void testCreateInsert() {
		String ins = abstractSqlFrameArchiver
				.createInsert("XTABLE", "YID", "DIR", "ZCREATED", new String[] {
						"A", "B" }, "AUTOINCR", "NOW");

		assertEquals(
				"INSERT INTO XTABLE (YID, DIR, A, B, ZCREATED) VALUES (AUTOINCR, ?, ?, ?, NOW)",
				ins);
	}

	@Test
	public void testCreateInsertDetails() {
		String ins = abstractSqlFrameArchiver.createInsertDetails("XTABLE",
				"YID", "MASTER_ID", "ZCREATED", "NAMEX", "VALUEY", "AUTOINCR",
				"NOW");

		assertEquals(
				"INSERT INTO XTABLE (YID, NAMEX, VALUEY, MASTER_ID, ZCREATED) VALUES (AUTOINCR, ?, ?, ?, NOW)",
				ins);
	}

	@Test
	public void testStore() {
		// TODO: Write test if you can
	}

	@Test
	public void testInsertMaster() {

	}

	@Test
	public void testInsertDetails() {
	}

}
