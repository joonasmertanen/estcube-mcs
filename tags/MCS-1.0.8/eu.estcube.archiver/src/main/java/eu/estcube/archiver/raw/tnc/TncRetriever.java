package eu.estcube.archiver.raw.tnc;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import javax.sql.DataSource;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.archiver.raw.ArchivingException;
import eu.estcube.archiver.raw.FrameRetriever;
import eu.estcube.archiver.raw.ax25.Ax25Retriever;
import eu.estcube.common.ByteUtil;
import eu.estcube.common.queryParameters.TNCObject;
import eu.estcube.common.queryParameters.TncQueryParameters;

public class TncRetriever extends FrameRetriever {
    public TncRetriever(DataSource source) {
        super(source);
    }

    private Logger LOG = LoggerFactory.getLogger(Ax25Retriever.class);

    private final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    private final TimeZone utc = TimeZone.getTimeZone("UTC");

    private String createQuery(TncQueryParameters params) {
        StringBuilder sb = new StringBuilder();

        if (params.getCommand() != Integer.MIN_VALUE) {
            sb.append(" AND command = ")
                    .append(params.getCommand());
        }
        String query = super.createQuery(params);
        query = query.replace("ReplaceInSubclass", sb.toString());

        return query;
    }

    @Override
    public void retrieve(Exchange exchange) throws ArchivingException, SQLException {
        TncQueryParameters params = (TncQueryParameters) exchange.getIn().getBody();
        LOG.debug("TncRetreiver in parameters" + params.toString());
        String query = createQuery(params);
        LOG.debug("TncRetreiver query" + query);

        super.retrieve(exchange);
        Statement statement = c.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        df.setTimeZone(utc);

        // get metadata
        ResultSetMetaData meta = null;
        meta = resultSet.getMetaData();
        // get column names
        int colCount = meta.getColumnCount();
        ArrayList<String> cols = new ArrayList<String>();
        for (int i = 1; i <= colCount; i++) {
            cols.add(meta.getColumnName(i));
        }
        // fetch out rows
        HashMap<Integer, TNCObject> rows = new HashMap<Integer, TNCObject>();
        TNCObject row = null;
        while (resultSet.next()) {
            int id = resultSet.getInt("ID");
            if (!rows.containsKey(id)) {
                row = new TNCObject();
                row.setRequestId(params.getReqId());
                row.setId((resultSet.getInt("ID")));
                row.setDirection(resultSet.getString("DIRECTION"));
                row.setReceptionTime(df.format(new Date(resultSet.getLong("RECEPTION_TIME"))));
                row.setSatellite(resultSet.getString("SATELLITE"));
                row.setCommand((resultSet.getInt("COMMAND")));
                byte[] data = resultSet.getBytes("DATA");
                if (data != null)
                    row.setData((ByteUtil.toHexString(data)));
                row.setCreated(resultSet.getString("CREATED"));
                String name = resultSet.getString("NAME");

                if (name.equals("groundStationId") || name.equals("issuedBy")) {
                    String[] array = resultSet.getString("VALUE").split("/");
                    row.setTarget(array[array.length - 1]);
                } else if (name.equals("orbitNumber")) {
                    row.setOrbit(resultSet.getInt("VALUE"));
                }
                rows.put(row.getId(), row);
            } else {
                row = rows.get(id);
                String name = resultSet.getString("NAME");

                if (name.equals("groundStationId") || name.equals("issuedBy")) {
                    String[] array = resultSet.getString("VALUE").split("/");
                    row.setTarget(array[array.length - 1]);
                } else if (name.equals("orbitNumber")) {
                    row.setOrbit(resultSet.getInt("VALUE"));
                }
                rows.put(row.getId(), row);
            }
        }
        LOG.info("Result: " + rows.size());

        exchange.getOut().setHeader("RequestId", params.getReqId());
        exchange.getOut().setBody(new ArrayList<Object>(rows.values()));
        c.close();

    }

    @Override
    protected String getTable() {
        return TncQueryParameters.getTable();
    }

    @Override
    protected String getDetailsTable() {
        return TncQueryParameters.getDetailsTable();
    }

}
