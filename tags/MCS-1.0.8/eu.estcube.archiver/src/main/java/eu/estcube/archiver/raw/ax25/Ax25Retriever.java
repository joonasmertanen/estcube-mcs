package eu.estcube.archiver.raw.ax25;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.sql.DataSource;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.archiver.raw.ArchivingException;
import eu.estcube.archiver.raw.FrameRetriever;
import eu.estcube.common.ByteUtil;
import eu.estcube.common.queryParameters.AX25Object;
import eu.estcube.common.queryParameters.AX25QueryParameters;

public class Ax25Retriever extends FrameRetriever {

    public Ax25Retriever(DataSource source) {
        super(source);
    }

    private Logger LOG = LoggerFactory.getLogger(Ax25Retriever.class);

    private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    private final TimeZone utc = TimeZone.getTimeZone("UTC");

    private String createQuery(AX25QueryParameters params) {
        StringBuilder sb = new StringBuilder();
        if (params.getSubsystem() != Integer.MIN_VALUE) {
            String subSys;
            if (params.getSubsystem() < 10) {
                subSys = "0" + params.getSubsystem();
            } else
                subSys = params.getSubsystem() + "";
            String direction = params.getDirection();
            if (!direction.isEmpty()) {

                if (direction.equals("UP")) {
                    sb.append(" AND info LIKE '06")
                            .append(subSys)
                            .append("%'");
                } else if (direction.equals("DOWN")) {
                    sb.append(" AND info LIKE '")
                            .append(subSys)
                            .append("06%'");
                }

            } else {
                sb.append(" AND (info LIKE '")
                        .append(subSys)
                        .append("06%' OR info LIKE '06")
                        .append(subSys)
                        .append("%')");
            }
        }
        String query = super.createQuery(params);
        query = query.replace("ReplaceInSubclass", sb.toString());

        return query;
    }

    @Override
    public void retrieve(Exchange exchange) throws ArchivingException, SQLException {
        try {
            AX25QueryParameters params = (AX25QueryParameters) exchange.getIn().getBody();
            LOG.info("Ax25Retreiver in parameters" + params.toString());
            String query = createQuery(params);
            LOG.info("Ax25Retreiver query" + query);
            super.retrieve(exchange);
            df.setTimeZone(utc);
            Statement statement = c.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            // get metadata
            ResultSetMetaData meta = null;
            meta = resultSet.getMetaData();

            // get column names
            int colCount = meta.getColumnCount();
            ArrayList<String> cols = new ArrayList<String>();
            for (int i = 1; i <= colCount; i++) {
                cols.add(meta.getColumnName(i));
            }
            // fetch out rows
            Map<Integer, AX25Object> rows = new HashMap<Integer, AX25Object>();
            AX25Object row = null;
            // make new object
            while (resultSet.next()) {
                int id = resultSet.getInt("ID");
                if (!rows.containsKey(id)) {
                    row = new AX25Object();
                    row.setRequestId(params.getReqId());
                    row.setId(id);
                    row.setDirection(resultSet.getString("DIRECTION"));
                    row.setReceptionTime(df.format(new Date(resultSet.getLong("RECEPTION_TIME"))));
                    row.setSatellite(resultSet.getString("SATELLITE"));
                    byte[] destaddr = resultSet.getBytes("DESTADDR");
                    if (destaddr != null)
                        row.setDestAddr(ByteUtil.toHexString(destaddr));
                    byte[] srcaddr = resultSet.getBytes("SRCADDR");
                    if (srcaddr != null)
                        row.setSrcAddr(ByteUtil.toHexString(srcaddr));
                    row.setCtrl(resultSet.getInt("CTRL"));
                    row.setPid(resultSet.getInt("PID"));
                    byte[] info = resultSet.getBytes("INFO");
                    if (info != null)
                        row.setInfo((ByteUtil.toHexString(info)));
                    row.setFcs(resultSet.getString("FCS"));
                    row.setErrorBitmask(resultSet.getInt("ERROR_BITMASK"));
                    row.setCreated(resultSet.getString("CREATED"));
                    String name = resultSet.getString("NAME");

                    if (name.equals("groundStationId") || name.equals("issuedBy")) {
                        String[] array = resultSet.getString("VALUE").split("/");
                        row.setTarget(array[array.length - 1]);
                    } else if (name.equals("orbitNumber")) {
                        row.setOrbitNumber(resultSet.getInt("VALUE"));
                    }
                    rows.put(row.getId(), row);
                } else {
                    row = rows.get(id);
                    String name = resultSet.getString("NAME");

                    if (name.equals("groundStationId") || name.equals("issuedBy")) {
                        String[] array = resultSet.getString("VALUE").split("/");
                        row.setTarget(array[array.length - 1]);
                    } else if (name.equals("orbitNumber")) {
                        row.setOrbitNumber(resultSet.getInt("VALUE"));
                    }
                    rows.put(row.getId(), row);
                }
            }
            LOG.info("Result: " + rows.size());
            exchange.getOut().setHeader("RequestId", params.getReqId());
            exchange.getOut().setBody(new ArrayList<Object>(rows.values()));
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {

            c.close();

        }
    }

    @Override
    protected String getTable() {
        return AX25QueryParameters.getTable();
    }

    @Override
    protected String getDetailsTable() {
        return AX25QueryParameters.getDetailsTable();
    }

}
