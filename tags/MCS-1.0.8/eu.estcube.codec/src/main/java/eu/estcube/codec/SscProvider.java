package eu.estcube.codec;

/**
 * Provides the source sequence counter service, mainly for AX.25 frame
 * numbering.
 * 
 */
public interface SscProvider {
	/**
	 * Returns the next value from the counter.
	 * 
	 * @param count
	 *            The number of values required, the counter is increased by
	 *            this number.
	 * @return The next value from the counter.
	 */
	public int getNext(int count);
}
