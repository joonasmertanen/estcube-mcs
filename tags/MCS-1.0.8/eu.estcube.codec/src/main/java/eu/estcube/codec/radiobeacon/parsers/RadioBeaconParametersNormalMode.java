package eu.estcube.codec.radiobeacon.parsers;

import org.hbird.exchange.core.Label;
import org.hbird.exchange.core.Parameter;

public class RadioBeaconParametersNormalMode extends RadioBeaconParameters {

    // FIXME Remove hardcoded value @ Kaarel
    private final String ENTITY_ID = "/ESTCUBE/Satellites/ESTCube-1/beacon";

    private Label radioBeacon = new Label(null, "raw");
    private Label operatingMode = new Label(null, "operating.mode");
    private Parameter timestamp = new Parameter(null, "timestamp");
    private Parameter mainBusVoltage = new Parameter(null, "main.bus.voltage");
    private Parameter averagePowerBalance = new Parameter(null, "average.power.balance");
    private Parameter batteryAVoltage = new Parameter(null, "battery.A.voltage");
    private Parameter batteryBVoltage = new Parameter(null, "battery.B.voltage");
    private Parameter batteryATemperature = new Parameter(null, "battery.A.temperature");
    private Parameter spinRateZ = new Parameter(null, "spin.rate.z");
    private Parameter receivedSignalStrength = new Parameter(null, "received.signal.strength");
    private Parameter satelliteMissionPhase = new Parameter(null, "satellite.mission.phase");
    private Parameter timeSinceLastResetCDHS = new Parameter(null, "time.since.last.reset.CDHS");
    private Parameter timeSinceLastResetCOM = new Parameter(null, "time.since.last.reset.COM");
    private Parameter timeSinceLastResetEPS = new Parameter(null, "time.since.last.reset.EPS");
    private Parameter tetherCurrent = new Parameter(null, "tether.current");
    private Parameter timeSinceLastErrorADCS = new Parameter(null, "time.since.last.error.ADCS");
    private Parameter timeSinceLastErrorCDHS = new Parameter(null, "time.since.last.error.CDHS");
    private Parameter timeSinceLastErrorCOM = new Parameter(null, "time.since.last.error.COM");
    private Parameter timeSinceLastErrorEPS = new Parameter(null, "time.since.last.error.EPS");
    private Parameter CDHSSystemStatusCode = new Parameter(null, "CDHS.system.status.code");
    private Parameter CDHSSystemStatusValue = new Parameter(null, "CDHS.system.status.value");
    private Parameter EPSStatusCode = new Parameter(null, "EPS.status.code");
    private Parameter ADCSSystemStatusCode = new Parameter(null, "ADCS.system.status.code");
    private Parameter ADCSSystemStatusValue = new Parameter(null, "ADCS.system.status.value");
    private Parameter COMSystemStatusCode = new Parameter(null, "COM.system.status.code");
    private Parameter COMSystemStatusValue = new Parameter(null, "COM.system.status.value");

    public RadioBeaconParametersNormalMode() {
        getRadioBeacon().setDescription("Raw radiobeacon string");
        getOperatingMode().setDescription("Operating mode (E=normal or T=safe)");
        getTimestamp().setDescription("Timestamp");
        getTimestamp().setUnit("sec");
        getMainBusVoltage().setDescription("Main bus voltage");
        getMainBusVoltage().setUnit("V");
        getAveragePowerBalance().setDescription("Average power balance");
        getAveragePowerBalance().setUnit("W");
        getBatteryAVoltage().setDescription("Battery A voltage");
        getBatteryAVoltage().setUnit("V");
        getBatteryBVoltage().setDescription("Battery B voltage");
        getBatteryBVoltage().setUnit("V");
        getBatteryATemperature().setDescription("Battery A temperature");
        getBatteryATemperature().setUnit("C");
        getSpinRateZ().setDescription("Spin rate Z");
        getSpinRateZ().setUnit("deg/s");
        getReceivedSignalStrength().setDescription("Received signal strength");
        getReceivedSignalStrength().setUnit("3dBm");
        getSatelliteMissionPhase()
                .setDescription("0=Detumbling, 1=Nadir pointing, 2=Tether deployment, 3=E-sail force measurement");
        getSatelliteMissionPhase().setUnit("");
        getTimeSinceLastResetCDHS().setDescription("Time since last reset CDHS");
        getTimeSinceLastResetCDHS().setUnit("hour(s)");
        getTimeSinceLastResetCOM().setDescription("Time since last reset COM");
        getTimeSinceLastResetCOM().setUnit("hour(s)");
        getTimeSinceLastResetEPS().setDescription("Time since last reset EPS");
        getTimeSinceLastResetEPS().setUnit("hour(s)");
        getTetherCurrent().setDescription("Tether current");
        getTetherCurrent().setUnit("mA");
        getTimeSinceLastErrorADCS().setDescription("Time since last error ADCS");
        getTimeSinceLastErrorADCS().setUnit("hour(s)");
        getTimeSinceLastErrorCDHS().setDescription("Time since last error CDHS");
        getTimeSinceLastErrorCDHS().setUnit("hour(s)");
        getTimeSinceLastErrorCOM().setDescription("Time since last error COM");
        getTimeSinceLastErrorCOM().setUnit("hour(s)");
        getTimeSinceLastErrorEPS().setDescription("Time since last error EPS");
        getTimeSinceLastErrorEPS().setUnit("hour(s)");
        getCDHSSystemStatusCode().setDescription("CDHS system status code");
        getCDHSSystemStatusCode().setUnit("");
        getCDHSSystemStatusValue().setDescription("CDHS system status value");
        getCDHSSystemStatusValue().setUnit("");
        getEPSStatusCode().setDescription("EPS status code");
        getEPSStatusCode().setUnit("");
        getADCSSystemStatusCode().setDescription("ADCS system status code");
        getADCSSystemStatusCode().setUnit("");
        getADCSSystemStatusValue().setDescription("ADCS system status value");
        getADCSSystemStatusValue().setUnit("");
        getCOMSystemStatusCode().setDescription("COM system status code");
        getCOMSystemStatusCode().setUnit("");
        getCOMSystemStatusValue().setDescription("COM system status value");
        getCOMSystemStatusValue().setUnit("");
    }

    /**
     * Updates the varsion and timestamp of the parameters and labels
     */
    public void updateParameters() {
        long now = getCurrentTimeStamp();
        update(radioBeacon, now);
        update(operatingMode, now);
        update(timestamp, now);
        update(mainBusVoltage, now);
        update(averagePowerBalance, now);
        update(batteryAVoltage, now);
        update(batteryBVoltage, now);
        update(batteryATemperature, now);
        update(spinRateZ, now);
        update(receivedSignalStrength, now);
        update(satelliteMissionPhase, now);
        update(timeSinceLastResetCDHS, now);
        update(timeSinceLastResetCOM, now);
        update(timeSinceLastResetEPS, now);
        update(tetherCurrent, now);
        update(timeSinceLastErrorADCS, now);
        update(timeSinceLastErrorCDHS, now);
        update(timeSinceLastErrorCOM, now);
        update(timeSinceLastErrorEPS, now);
        update(CDHSSystemStatusCode, now);
        update(CDHSSystemStatusValue, now);
        update(EPSStatusCode, now);
        update(COMSystemStatusCode, now);
        update(COMSystemStatusValue, now);
        update(ADCSSystemStatusValue, now);
        update(ADCSSystemStatusCode, now);
    }

    public Label getRadioBeacon() {
        return radioBeacon;
    }

    public Label getOperatingMode() {
        return operatingMode;
    }

    public Parameter getTimestamp() {
        return timestamp;
    }

    public Parameter getMainBusVoltage() {
        return mainBusVoltage;
    }

    public Parameter getAveragePowerBalance() {
        return averagePowerBalance;
    }

    public Parameter getBatteryAVoltage() {
        return batteryAVoltage;
    }

    public Parameter getBatteryBVoltage() {
        return batteryBVoltage;
    }

    public Parameter getBatteryATemperature() {
        return batteryATemperature;
    }

    public Parameter getSpinRateZ() {
        return spinRateZ;
    }

    public Parameter getReceivedSignalStrength() {
        return receivedSignalStrength;
    }

    public Parameter getSatelliteMissionPhase() {
        return satelliteMissionPhase;
    }

    public Parameter getTimeSinceLastResetCDHS() {
        return timeSinceLastResetCDHS;
    }

    public Parameter getTimeSinceLastResetCOM() {
        return timeSinceLastResetCOM;
    }

    public Parameter getTimeSinceLastResetEPS() {
        return timeSinceLastResetEPS;
    }

    public Parameter getTetherCurrent() {
        return tetherCurrent;
    }

    public Parameter getTimeSinceLastErrorADCS() {
        return timeSinceLastErrorADCS;
    }

    public Parameter getTimeSinceLastErrorCDHS() {
        return timeSinceLastErrorCDHS;
    }

    public Parameter getTimeSinceLastErrorCOM() {
        return timeSinceLastErrorCOM;
    }

    public Parameter getTimeSinceLastErrorEPS() {
        return timeSinceLastErrorEPS;
    }

    public Parameter getCDHSSystemStatusCode() {
        return CDHSSystemStatusCode;
    }

    public Parameter getCDHSSystemStatusValue() {
        return CDHSSystemStatusValue;
    }

    public Parameter getEPSStatusCode() {
        return EPSStatusCode;
    }

    public Parameter getADCSSystemStatusCode() {
        return ADCSSystemStatusCode;
    }

    public Parameter getADCSSystemStatusValue() {
        return ADCSSystemStatusValue;
    }

    public Parameter getCOMSystemStatusCode() {
        return COMSystemStatusCode;
    }

    public Parameter getCOMSystemStatusValue() {
        return COMSystemStatusValue;
    }

    public String getEntityId() {
        return ENTITY_ID;
    }

}
