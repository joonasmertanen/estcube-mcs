package eu.estcube.codec.gcp.struct.parameters;

import java.nio.ByteBuffer;

import org.apache.commons.lang3.ArrayUtils;
import org.w3c.dom.Element;

public class GcpParameterUint16 extends GcpParameterNumberic {

    public final static long MIN_VALUE = 0;
    public final static long MAX_VALUE = 65535;
    public final static long BYTES = 2;

    public GcpParameterUint16(String name, String description, String unit, Boolean isLittleEndian) {
        super(name, description, unit, isLittleEndian);
    }

    public GcpParameterUint16(Element e) {
        super(e);
    }

    @Override
    public int getLength() {
        return 16;
    }

    @Override
    public Integer toValue(String input) {

        if (isHexString(input)) {
            return (Integer) toValueFromHex(input);
        }

        int num = Integer.parseInt(input);
        if (num < MIN_VALUE) {
            throw new NumberFormatException(getClass().getName() + " value must be bigger than " + MIN_VALUE);
        }
        if (num > MAX_VALUE) {
            throw new NumberFormatException(getClass().getName() + " value must be less than " + MAX_VALUE);
        }
        return (int) (num & 0x0000ffffL);
    }

    @Override
    public byte[] toBytes(Object value) {

        ByteBuffer buffer = ByteBuffer.allocate(4);
        buffer.putInt(toValue(String.valueOf(value)));
        byte[] bytes = { buffer.get(2), buffer.get(3) };

        if (getIsLittleEndian()) {
            ArrayUtils.reverse(bytes);
        }

        return bytes;
    }

    @Override
    public Integer toValue(byte[] bytes) {

        if (bytes.length != BYTES) {
            throw new RuntimeException("byte[] length of the argument must be " + BYTES + "! Was " + bytes.length);
        }

        byte byte1 = bytes[0];
        byte byte2 = bytes[1];

        if (getIsLittleEndian()) {
            byte1 = bytes[1];
            byte2 = bytes[0];
        }

        return (int) ((byte1 & 0xFF) << 8 | (byte2 & 0xFF));
    }

    @Override
    public Class<?> getValueClass() {
        return Integer.class;
    }

}
