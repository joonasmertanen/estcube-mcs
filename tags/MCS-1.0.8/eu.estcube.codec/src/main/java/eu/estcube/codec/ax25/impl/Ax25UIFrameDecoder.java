package eu.estcube.codec.ax25.impl;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.transport.ax25.Ax25FrameStatus;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;

/**
 * Decoder for AX.25 UI frames from a stream of bits.
 */
public final class Ax25UIFrameDecoder implements ProtocolDecoder {

    private final static Logger LOG = LoggerFactory.getLogger(Ax25UIFrameDecoder.class);

    // How many consecutive ones have we seen at present, e.g. a value of 3
    // means the last bits in the stream were 0111.
    private int consecutiveones = 0;

    // The next byte we are extracting from the stream, we put bits at their
    // correct position right away (no shifting of newbyte).
    private byte newbyte = 0;

    // How many bits have we read and put into newbyte (i.e. bits used for
    // stuffing are not counted), will be reset after newbyte is complete.
    private int bitsread = 0;

    // Points to next index in framebuf where to store newbyte, also equal to
    // the number of extracted bytes for the current frame.
    private int framebufptr = 0;

    // The buffer where the decoding process stores the bytes of the new frame.
    private byte[] framebuf = new byte[Ax25UIFrame.FRAME_CONTENTS_MAX_SIZE];

    private Ax25FrameStatus frameStatus;

    public Ax25UIFrameDecoder() {
        frameStatus = new Ax25FrameStatus();
    }

    @Override
    public void decode(IoSession session, IoBuffer in, ProtocolDecoderOutput out) throws Exception {
        while (in.hasRemaining()) {
            byte b = in.get();
            Ax25UIFrame frame = decode(b);
            if (frame != null) {
                out.write(frame);
            }
        }
    }

    @Override
    public void dispose(IoSession session) throws Exception {
    }

    @Override
    public void finishDecode(IoSession session, ProtocolDecoderOutput out) throws Exception {
    }

    /**
     * Unpacks bits from a byte and decodes them. Returns a decoded frame or
     * null, if all the bits for a frame have not been received yet.
     */
    public Ax25UIFrame decode(byte b) {
        Ax25UIFrame frame = null;

        for (int i = 0; i < 8; i++) {
            // unpack bits from byte in the order they were transmitted
            int bit = (b << i & 0x80) > 0 ? 1 : 0;

            if (consecutiveones == 5 && bit == 0) { // 0111110
                // drop 0, i.e. unstuff
            } else if (consecutiveones == 6 && bit == 0) { // 01111110
                // got flag 0x7e signifying the border of a frame
                if (framebufptr > 0) {
                    frameStatus.setErrorUnAligned(++bitsread < 8);
                    frameStatus.setErrorTooShort(framebufptr < Ax25UIFrame.NON_INFO_LENGTH);
                    frame = createFrame(framebuf, framebufptr, frameStatus);
                }
                // start decoding a new frame
                frameStatus = new Ax25FrameStatus(); // reset errors
                framebufptr = 0;
                newbyte = 0;
                bitsread = 0;
            } else {
                // bytes were transmitted LSB first
                newbyte = (byte) (newbyte | bit << bitsread++);
                if (consecutiveones > 6)
                    frameStatus.setErrorUnstuffedBits(true);
            }

            if (bit == 1)
                consecutiveones++;
            else
                consecutiveones = 0;

            if (bitsread == 8) { // newbyte complete
                if (framebufptr >= Ax25UIFrame.FRAME_CONTENTS_MAX_SIZE) {
                    frameStatus.setErrorTooLong(true);
                } else {
                    framebuf[framebufptr++] = newbyte;
                }
                // start extracting new byte
                newbyte = 0;
                bitsread = 0;
            }
        }

        return frame;
    }

    /**
     * Creates an AX.25 UI frame from a given byte buffer. Supports only 2
     * address fields (destination and source), doesn't support the extra 8
     * repeater-address subfields permitted by the standard.
     *
     * @param source
     *        The source to create from.
     * @param len
     *        The length of data or exclusive end index of data in source.
     * @param fs
     *        The validity of the source.
     * @return
     *         New AX.25 UI frame.
     */
    protected Ax25UIFrame createFrame(byte[] source, int len, Ax25FrameStatus fs) {
        Ax25UIFrame f = new Ax25UIFrame();
        f.setStatus(fs);

        int ptr = 0;

        if (len >= (ptr += Ax25UIFrame.DEST_ADDR_LEN)) {
            f.setDestAddr(ArrayUtils.subarray(source, ptr - Ax25UIFrame.DEST_ADDR_LEN, ptr));
        }

        if (len >= (ptr += Ax25UIFrame.SRC_ADDR_LEN)) {
            f.setSrcAddr(ArrayUtils.subarray(source, ptr - Ax25UIFrame.SRC_ADDR_LEN, ptr));
        }

        if (len >= ++ptr) {
            f.setCtrl(source[ptr - 1]);
        }

        if (len >= ++ptr) {
            f.setPid(source[ptr - 1]);
        }

        if (len >= ptr + Ax25UIFrame.FCS_LEN) {
            f.setInfo(ArrayUtils.subarray(source, ptr, len - Ax25UIFrame.FCS_LEN));
            f.setFcs(ArrayUtils.subarray(source, len - Ax25UIFrame.FCS_LEN, len));
            // check frame by comparing the CRC computed from frame Info field
            // with the CRC present in the 2 byte FCS field
            short crc = Ax25CRC16.ec1ax25crc16(ArrayUtils.subarray(source, 0, len - Ax25UIFrame.FCS_LEN));
            fs.setErrorFcs((crc >>> 8 & 0xff) != (f.getFcs()[0] & 0xff) || (crc & 0xff) != (f.getFcs()[1] & 0xff));
        }

        // Short invalid frames with null info bytes can be produced after every
        // frame when using scrambling. The sender can send multiple 0x7e flags
        // between frames. It resets scrambling in the beginning of every frame,
        // but the descrambler on the receiving end does not, thus some number
        // of those 0x7e flags will be garbled and might erroneously end up as a
        // short invalid frame before descrambling starts producing valid flags.
        if (f.getInfo() != null) {
            LOG.debug("Got " + f.getInfo().length + " info bytes; frame " + (fs.isValid() ? "is" : "not") + " valid");
        } else {
            LOG.debug("Got null info bytes; frame " + (fs.isValid() ? "is" : "not") + " valid");
        }

        return f;
    }
}
