package eu.estcube.codec.pkt.impl;

import java.nio.ByteBuffer;

import eu.estcube.codec.pkt.TCPacketEncoder;
import eu.estcube.domain.transport.pkt.TCPacket;
import eu.estcube.domain.transport.pkt.TCPacketFrameHeader;
import eu.estcube.domain.transport.pkt.TCPacketFramePrimaryHeader;
import eu.estcube.domain.transport.pkt.TCPacketPrimaryFrame;
import eu.estcube.domain.transport.pkt.TCPacketSecondaryFrame;

/**
 * An ESTCube-1 specific packet encoder that follows the agreed packet format
 * here: http://tudengisatelliit.ut.ee:8080/display/MCS/TC+Packet+Encoder
 * 
 * Note that it already takes into account that the packets are sent using AX.25
 * and therefore needs to know the size of the AX.25 that will be used. This can
 * be given as parameter.
 */
public class TCPacketEncoderImpl implements TCPacketEncoder {
	private int frameSize;

	public TCPacketEncoderImpl(int frameSize) {
		super();
		this.frameSize = frameSize;
	}

	public int getFrameSize() {
		return frameSize;
	}

	@Override
	public byte[] encode(TCPacket packet) {
		TCPacketPrimaryFrame primaryFrame = packet.getPrimaryFrame();
		TCPacketFramePrimaryHeader primaryHeader = primaryFrame.getHeader();

		byte[] sscEncoded = encodeSsc(primaryHeader.getSsc(), TCPacketFrameHeader.SSC_MAX,
				TCPacketFrameHeader.SSC_BYTES, true);

		byte[] lenEncoded = encodeUnsignedInteger(primaryHeader.getLen(), TCPacketFrameHeader.LEN_MAX,
				TCPacketFrameHeader.LEN_BYTES);

		byte[] spidEncoded = encodeUnsignedInteger(primaryHeader.getSpid(), TCPacketFramePrimaryHeader.SPID_MAX,
				TCPacketFramePrimaryHeader.SPID_BYTES);
		byte[] tsEncoded = encodeUnsignedInteger(primaryHeader.getTs(), TCPacketFramePrimaryHeader.TS_MAX,
				TCPacketFramePrimaryHeader.TS_BYTES);
		byte[] macEncoded = primaryHeader.getMac();
		byte[] etEncoded = encodeUnsignedInteger(primaryHeader.getEt(), TCPacketFramePrimaryHeader.ET_MAX,
				TCPacketFramePrimaryHeader.ET_BYTES);

		ByteBuffer bf = ByteBuffer.allocate(frameSize * primaryHeader.getLen());

		// Primary header
		bf.put(sscEncoded);
		bf.put(lenEncoded);
		bf.put(spidEncoded);
		bf.put(tsEncoded);
		bf.put(macEncoded);
		bf.put(etEncoded);

		// Primary body
		bf.put(primaryFrame.getBody());

		// Secondary frames
		for (TCPacketSecondaryFrame secondaryFrame : packet.getSecondaryFrames()) {
			TCPacketFrameHeader h = secondaryFrame.getHeader();
			bf.put(encodeSsc(h.getSsc(), TCPacketFrameHeader.SSC_MAX, TCPacketFrameHeader.SSC_BYTES, false));
			bf.put(encodeUnsignedInteger(h.getLen(), TCPacketFrameHeader.LEN_MAX, TCPacketFrameHeader.LEN_BYTES));
			bf.put(secondaryFrame.getBody());
		}

		// Create and return byte array from buffer
		byte[] result = new byte[bf.position()];
		bf.position(0);
		bf.get(result, 0, result.length);

		return result;
	}

	protected byte[] encodeSsc(long value, long maxValue, int numBytes, boolean primaryFrame) {
		byte[] sscEncoded = encodeUnsignedInteger(value, maxValue, numBytes);

		// Set the 2highest bit to 00 if primary and 01 if secondary
		if (primaryFrame) {
			sscEncoded[0] &= 0x3F; // 00111111
		} else {
			sscEncoded[0] &= 0x7F; // 01111111
			sscEncoded[0] |= 0x40; // 01000000
		}

		return sscEncoded;

	}

	protected byte[] encodeUnsignedInteger(long value, long maxValue, int numBytes) {
		if (value < 0) {
			throw new IllegalArgumentException(String.format("The value %d is is less then zero", value));
		}

		if (value > maxValue) {
			throw new IllegalArgumentException(String.format("The value %d is greater than the allowed maximum %d",
					value, maxValue));
		}

		byte[] encoded = new byte[numBytes];
		for (int i = 0; i < encoded.length; i++) {
			encoded[encoded.length - i - 1] = (byte) ((value >> i * 8) & 0xFF);
		}
		return encoded;

	}
}
