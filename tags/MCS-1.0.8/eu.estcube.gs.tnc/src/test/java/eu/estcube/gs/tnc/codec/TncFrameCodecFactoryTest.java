package eu.estcube.gs.tnc.codec;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.inOrder;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TncFrameCodecFactoryTest {

    @Mock
    private IoSession session;
    
    private TncFrameCodecFactory factory;
    
    private InOrder inOrder;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        factory = new TncFrameCodecFactory();
        inOrder = inOrder(session);
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncEscapeCodecFactory#getEncoder(org.apache.mina.core.session.IoSession)}.
     * @throws Exception 
     */
    @Test
    public void testGetEncoder() throws Exception {
        ProtocolEncoder encoder1 = factory.getEncoder(session); 
        ProtocolEncoder encoder2 = factory.getEncoder(session); 
        assertNotNull(encoder1);
        assertNotNull(encoder2);
        assertEquals(TncFrameEncoder.class, encoder1.getClass());
        assertEquals(TncFrameEncoder.class, encoder2.getClass());
        assertSame(encoder1, encoder2);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncEscapeCodecFactory#getDecoder(org.apache.mina.core.session.IoSession)}.
     * @throws Exception 
     */
    @Test
    public void testGetDecoder() throws Exception {
        ProtocolDecoder decoder1 = factory.getDecoder(session);
        ProtocolDecoder decoder2 = factory.getDecoder(session);
        assertNotNull(decoder1);
        assertNotNull(decoder2);
        assertEquals(TncFrameDecoder.class, decoder1.getClass());
        assertEquals(TncFrameDecoder.class, decoder2.getClass());
        assertSame(decoder1, decoder2);
        inOrder.verifyNoMoreInteractions();
    }
}
