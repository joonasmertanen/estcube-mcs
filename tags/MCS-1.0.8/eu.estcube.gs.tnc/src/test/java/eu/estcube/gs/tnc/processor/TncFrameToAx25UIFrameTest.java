/** 
 *
 */
package eu.estcube.gs.tnc.processor;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.hbird.exchange.constants.StandardArguments;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.codec.ax25.impl.Ax25UIFrameKissDecoder;
import eu.estcube.common.Headers;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.domain.transport.tnc.TncFrame;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TncFrameToAx25UIFrameTest {

    private static final int TNC_PORT = 6;

    @Mock
    private Ax25UIFrameKissDecoder decoder;

    @Mock
    private Exchange exchange;

    @Mock
    private Message in;

    @Mock
    private Message out;

    @Mock
    private TncFrame tncFrame;

    private Ax25UIFrame ax25Frame;

    @InjectMocks
    private TncFrameToAx25UIFrame processor;

    private RuntimeException exception;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        ax25Frame = new Ax25UIFrame();
        exception = new RuntimeException("Muchos problemos");
        inOrder = inOrder(decoder, exchange, in, out, tncFrame);
        when(exchange.getIn()).thenReturn(in);
        when(exchange.getOut()).thenReturn(out);
        when(in.getBody(TncFrame.class)).thenReturn(tncFrame);
        when(decoder.decode(tncFrame)).thenReturn(ax25Frame);
        when(tncFrame.getTarget()).thenReturn(TNC_PORT);
    }

    /**
     * Test method for
     * {@link eu.estcube.sdc.processor.TncFrameToAx25UIFrame#process(org.apache.camel.Exchange)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testProcess() throws Exception {
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getBody(TncFrame.class);
        inOrder.verify(decoder, times(1)).decode(tncFrame);
        inOrder.verify(out, times(1)).setBody(ax25Frame);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.CLASS, Ax25UIFrame.class.getSimpleName());
        inOrder.verify(out, times(1)).setHeader(StandardArguments.TYPE, TncFrameToAx25UIFrame.TYPE);
        inOrder.verify(tncFrame, times(1)).getTarget();
        inOrder.verify(out, times(1)).setHeader(Headers.TNC_PORT, TNC_PORT);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.sdc.processor.TncFrameToAx25UIFrame#process(org.apache.camel.Exchange)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testProcessWithException() throws Exception {
        when(decoder.decode(tncFrame)).thenThrow(exception);
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getBody(TncFrame.class);
        inOrder.verify(decoder, times(1)).decode(tncFrame);
        inOrder.verify(exchange, times(1)).setException(exception);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.sdc.processor.TncFrameToAx25UIFrame#process(org.apache.camel.Exchange)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testProcessNoTncFrame() throws Exception {
        when(in.getBody(TncFrame.class)).thenReturn(null);
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getBody(TncFrame.class);
        inOrder.verify(exchange, times(1)).setException(any(IllegalArgumentException.class));
        inOrder.verifyNoMoreInteractions();
    }
}
