package eu.estcube.gs.tnc.codec;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.common.ByteUtil;
import eu.estcube.domain.transport.tnc.TncFrame;
import eu.estcube.domain.transport.tnc.TncFrame.TncCommand;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TncFrameEncoderTest {

    @Mock
    private IoSession session;
    
    @Mock
    private ProtocolEncoderOutput out;
    
    private TncFrameEncoder encoder;
    
    private InOrder inOrder;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        encoder = new TncFrameEncoder();
        inOrder = inOrder(session, out);
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFrameEncoder#encode(org.apache.mina.core.session.IoSession, java.lang.Object, org.apache.mina.filter.codec.ProtocolEncoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testEncode() throws Exception {
        byte[] bytes = new byte[] { 0x01, 0x02, 0x03, 0x05, 0x07, 0x11 };
        TncFrame frame = new TncFrame(TncCommand.FULL_DUPLEX, 3, bytes);
        encoder.encode(session, frame, out);
        ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        inOrder.verify(out, times(1)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        byte[] result = captor.getValue();
        assertTrue(ByteUtil.toHexString(result), Arrays.equals(TncFrames.toBytes(frame), result));
    }
    
    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFrameEncoder#encode(org.apache.mina.core.session.IoSession, java.lang.Object, org.apache.mina.filter.codec.ProtocolEncoderOutput)}.
     * @throws Exception 
     */
    @Test(expected = RuntimeException.class)
    public void testEncodeWithException() throws Exception {
        encoder.encode(session, new Object(), out);
    }
}
