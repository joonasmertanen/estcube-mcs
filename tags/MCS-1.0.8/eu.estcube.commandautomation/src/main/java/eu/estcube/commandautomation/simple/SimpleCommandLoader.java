package eu.estcube.commandautomation.simple;

import java.util.List;

import org.hbird.exchange.core.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SimpleCommandLoader {

    @Autowired
    SimpleCommandParser parser;

    public List<Command> load(String trackingId) {

        // @TODO load data
        String commands = "";

        return parser.parse(commands);
    }
}
