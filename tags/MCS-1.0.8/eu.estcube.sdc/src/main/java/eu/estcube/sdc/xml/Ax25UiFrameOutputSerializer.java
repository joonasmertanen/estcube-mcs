/** 
 *
 */
package eu.estcube.sdc.xml;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Body;
import org.apache.camel.Handler;
import org.springframework.stereotype.Component;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.StaxDriver;

import eu.estcube.sdc.domain.Ax25UiFrameOutput;

/**
 *
 */
@Component
public class Ax25UiFrameOutputSerializer {

    public static final String ENCODING = "UTF-8";
    
    public static final String XML_HEADER_LINE = String.format("<?xml version=\"1.0\" encoding=\"%s\" ?>\n", ENCODING);
    
    private XStream xstream = new XStream(new StaxDriver());
    
    public Ax25UiFrameOutputSerializer() {
        xstream.alias("frame", Ax25UiFrameOutput.class);
        xstream.alias("frames", List.class);
    }
 
    @Handler
    public byte[] serialize(@Body Ax25UiFrameOutput output) throws Exception {
        List<Ax25UiFrameOutput> frames = new ArrayList<Ax25UiFrameOutput>(1);
        frames.add(output);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        OutputStreamWriter writer = new OutputStreamWriter(stream, ENCODING);
        HierarchicalStreamWriter xmlWriter = new PrettyPrintWriter(writer);
        writer.write(XML_HEADER_LINE);
        xstream.marshal(frames, xmlWriter);
        return stream.toByteArray();
    }
}
