/** 
 *
 */
package eu.estcube.calibration.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.hbird.exchange.core.CalibratedParameter;
import org.hbird.exchange.core.CalibratedParameter.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.calibration.Calibrator;
import eu.estcube.calibration.data.CalibrationExpression;
import eu.estcube.calibration.data.LimitsData;
import eu.estcube.calibration.notifiers.EmailNotifier;

/**
 * Calibrates <code>Parameter</code>s in Camel route
 */
@Component
public class ParameterLimitCheckingProcessor implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(ParameterLimitCheckingProcessor.class);

    @Autowired
    private EmailNotifier notifier;

    /** @{inheritDoc . */
    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();

        CalibratedParameter input = in.getBody(CalibratedParameter.class);
        String id = input.getID().replace("//", "/");
        Level currLevel = input.getLevel();
        if (currLevel == null) {
            currLevel = Level.NOT_CHECKED;
        }
        for (CalibrationExpression c : Calibrator.getCalibrationExpressions()) {
            if (c.getID().equals(id)) {
                if (c.hasLimits()) {
                    LimitsData ld = c.getLimits();
                    if (ld.inSoft(input)) {
                        // ELSE IN SOFT LIMITS
                        currLevel = Level.SOFT;
                    } else if (ld.inHard(input)) {
                        // IN HARD LIMITS
                        currLevel = Level.HARD;
                    } else if (ld.inSane(input)) {
                        // IN SANITY LIMITS
                        currLevel = Level.SANE;
                    } else {
                        // OUT OF LIMITS
                        currLevel = Level.OUT;
                    }
                    Level prevErrLevel = ld.setErrorLevel(currLevel);
                    if (prevErrLevel != null && !prevErrLevel.equals(currLevel)) {
                        notifier.notify(c, input, prevErrLevel, currLevel);
                    }
                }
                break;
            }
        }
        input.setLevel(currLevel);
        out.setBody(input);
    }

}
