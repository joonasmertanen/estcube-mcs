package eu.estcube.commanding.rotator2;

import java.util.List;

import org.hbird.exchange.navigation.PointingData;

import eu.estcube.commanding.api.OptimizeInterface;
import eu.estcube.commanding.calculation.Calculation;
import eu.estcube.commanding.optimizators.GlobalFunctions;
import eu.estcube.commanding.rotator1.Rotator1CrossElHigh;
import eu.estcube.commanding.rotator1.Rotator1NoCrossElHigh;

/**
 * When north line is crossed , elevation is high and we are dealing with 2
 * rotator type.
 * 
 * @author Ivar Mahhonin
 * 
 */
public class Rotator2CrossElHigh implements OptimizeInterface {
	double receivingSector;
	double maxAz;
	double maxEl;

	public Rotator2CrossElHigh(final double receivingSector,
			final double maxAz, final double maxEl) {
		super();
		this.receivingSector = receivingSector;
		this.maxAz = maxAz;
		this.maxEl = maxEl;
	}

	public boolean caseSuitable(boolean suitable,
			final List<PointingData> coordinates, final int crossPoint,
			final boolean clockwise) {

		suitable = withinMaxAzRange(suitable, coordinates, clockwise);

		return suitable;
	}

	public List<PointingData> changeAzimuthList(
			final List<PointingData> coordinates, final int crossPoint) {
		double addValue = 0;
		for (int i = 0; i < crossPoint; i++) {
			addValue = (coordinates.get(i).getAzimuth() + 360);
			coordinates.get(i).setAzimuth(addValue);
		}

		return coordinates;
	}

	@Override
	public List<PointingData> optimize(List<PointingData> coordinates) {
		boolean suitable = false;
		boolean clockwise = false;
		int crossPoint = 0;
		crossPoint = GlobalFunctions.getCrossPoint(coordinates);
		clockwise = GlobalFunctions.ifClockwise(coordinates, crossPoint);
		suitable = caseSuitable(suitable, coordinates, crossPoint, clockwise);

		if (suitable == true) {
			if (clockwise == true) {
				final Rotator1NoCrossElHigh caseToApply1 = new Rotator1NoCrossElHigh(
						receivingSector, 360, maxEl);
				coordinates = caseToApply1.optimize(coordinates);
				Calculation.log.info("APPLYING CASE AS FOR ROTATOR 1 CASE 3");
			}

			if (clockwise == false) {
				final Rotator1NoCrossElHigh caseToApply1 = new Rotator1NoCrossElHigh(
						receivingSector, maxAz, maxEl);
				coordinates = changeAzimuthList(coordinates, crossPoint);
				coordinates = caseToApply1.optimize(coordinates);
			}

		}
		if (suitable == false) {
			final Rotator1CrossElHigh caseToApply = new Rotator1CrossElHigh(
					receivingSector, maxAz, maxEl);
			coordinates = caseToApply.optimize(coordinates);
			Calculation.log.info("APPLYING CASE AS FOR ROTATOR 1 CASE 4");
		}

		return coordinates;
	}

	/**
	 * If trajectory points are within maximum azimuth value, then rotator can
	 * support movement at the 360<azValue<450 distance.
	 * 
	 * @param suitable
	 * @param coordinates
	 * @param clockwise
	 * @return
	 */
	public boolean withinMaxAzRange(boolean suitable,
			final List<PointingData> coordinates, final boolean clockwise) {
		if (clockwise) {
			if (coordinates.get(coordinates.size() - 1).getAzimuth() < 90) {
				suitable = true;

			} else {
				suitable = false;
			}
		} else {
			if (coordinates.get(0).getAzimuth() < 90) {
				suitable = true;

			} else {
				suitable = false;
			}

		}

		return suitable;
	}

}