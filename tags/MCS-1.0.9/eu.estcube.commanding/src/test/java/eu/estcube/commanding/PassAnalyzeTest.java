package eu.estcube.commanding;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.hbird.exchange.navigation.PointingData;
import org.junit.Test;

import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.analysis.PassAnalyze;

public class PassAnalyzeTest {
	List<PointingData> coordinates = new ArrayList<PointingData>();

	@Test
	public void crossElevationHighTest() {
		final double[] azArray = { 357, 358, 359, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		final double[] elArray = { 20, 30, 40, 45, 55, 67, 75, 80, 90, 80, 75,
				60 };
		fillLists(elArray, azArray);
		final PassAnalyze analyze = new PassAnalyze();
		final OverPassType type = analyze.calculateType(coordinates);
		assertEquals(OverPassType.CROSS_ELEVATION_HIGH, type);
	}

	@Test
	public void crossElevationLowTest() {
		final double[] azArray = { 357, 358, 359, 1, 2, 3 };
		final double[] elArray = { 20, 30, 40, 45, 55, 67 };
		fillLists(elArray, azArray);
		final PassAnalyze analyze = new PassAnalyze();
		final OverPassType type = analyze.calculateType(coordinates);
		assertEquals(OverPassType.CROSS_ELEVATION_LOW, type);

	}

	public void fillLists(final double[] elArray, final double[] azArray) {
		for (int i = 0; i < elArray.length; i++) {
			coordinates.add(new PointingData(0L, azArray[i], elArray[i], 123.0,
					"test", "test"));
		}

	}

	@Test
	public void noCrossElevationHighTest() {
		final double[] azArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
		final double[] elArray = { 20, 30, 40, 45, 55, 67, 75, 80, 90, 80, 75,
				60 };
		fillLists(elArray, azArray);
		final PassAnalyze analyze = new PassAnalyze();
		final OverPassType type = analyze.calculateType(coordinates);
		assertEquals(OverPassType.NO_CROSS_ELEVATION_HIGH, type);
	}

	@Test
	public void noCrossElevationLowTest() {
		final double[] azArray = { 20, 30, 40, 45, 55, 67 };
		final double[] elArray = { 10, 20, 30, 40, 50, 60 };
		fillLists(elArray, azArray);
		final PassAnalyze analyze = new PassAnalyze();
		final OverPassType type = analyze.calculateType(coordinates);
		assertEquals(OverPassType.NO_CROSS_ELEVATION_LOW, type);

	}

}
