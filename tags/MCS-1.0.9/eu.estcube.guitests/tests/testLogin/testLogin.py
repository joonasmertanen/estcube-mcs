import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import ConfigParser

#read configuration
confparser = ConfigParser.ConfigParser()
confparser.read('tests/testConf.ini')
driverpath = confparser.get('driverpath','chromedriverpath')
mcc = confparser.get('environments','mcc')

#Test if user authentication requred
class testLoginSecurity(unittest.TestCase):
	
	def setUp(self):
		self.driver = webdriver.Chrome(driverpath)
		
	def tearDown(self):
		self.driver.close()

	def testLoginSecurity(self):
		driver = self.driver
		driver.get(mcc + '/MCS/#ESTCube-1/dashboard')
		assert "ESTCube MCS Login" in driver.title

#Test invalid login credentials
class testInvalidLogin(unittest.TestCase):
	
	def setUp(self):
		self.driver = webdriver.Chrome(driverpath)
		
	def tearDown(self):
		self.driver.close()
		
	def testInvalidLogin(self):
		driver = self.driver
		driver.get(mcc)
		user_elem = driver.find_element_by_name("j_username")
		user_elem.send_keys("test.admin")
		pw_elem = driver.find_element_by_name("j_password")
		pw_elem.send_keys("test.admin")
		pw_elem.send_keys(Keys.RETURN)
		assert "Invalid username or password" in driver.page_source

# Test if admin can log in	
class testAdminValidLogin(unittest.TestCase):
	
	def setUp(self):
		self.driver = webdriver.Chrome(driverpath)
		
	def tearDown(self):
		self.driver.close()
		
	def testAdminValidLogin(self):
		driver = self.driver
		driver.get(mcc)
		user_elem = driver.find_element_by_name("j_username")
		user_elem.send_keys(confparser.get('admins','admin'))
		pw_elem = driver.find_element_by_name("j_password")
		pw_elem.send_keys(confparser.get('admins','adminpw'))
		pw_elem.send_keys(Keys.RETURN)
		assert "User: test.admin" in driver.page_source

#Test if user can login	
class testUserValidLogin(unittest.TestCase):
	
	def setUp(self):
		self.driver = webdriver.Chrome(driverpath)
		
	def tearDown(self):
		self.driver.close()
		
	def testUserValidLogin(self):
		driver = self.driver
		driver.get(mcc)
		user_elem = driver.find_element_by_name("j_username")
		user_elem.send_keys(confparser.get('users','user'))
		pw_elem = driver.find_element_by_name("j_password")
		pw_elem.send_keys(confparser.get('users','userpw'))
		pw_elem.send_keys(Keys.RETURN)
		assert "User: test.user" in driver.page_source
