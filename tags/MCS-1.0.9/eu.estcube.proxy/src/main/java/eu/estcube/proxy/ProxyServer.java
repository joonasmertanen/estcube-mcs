package eu.estcube.proxy;

import java.net.InetSocketAddress;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.resource.ResourceCollection;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.hbird.business.core.AddHeaders;
import org.hbird.exchange.configurator.StandardEndpoints;
import org.hbird.exchange.core.BusinessCard;
import org.hbird.exchange.groundstation.Track;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import eu.estcube.domain.transport.Downlink;
import eu.estcube.gs.contact.LocationContactEventExtractor;
import eu.estcube.gs.contact.LocationContactEventHolder;
import eu.estcube.proxy.processor.Ax25UiFrameRemoteProcessor;
import eu.estcube.proxy.servlet.RemotePacketServlet;

/**
 * Proxy server is used for accepting packets from 3rd party groundstations.
 * Direct access from outside network to ActiveMQ is prohibited from 3rd party
 * groundstations for security reasons.
 * 
 * 1) Proxy server captures the packet via POST request and extracts the data
 * from the request.
 * 
 * 2) Received data is converted into and instance of AX25UIFrame and is checked
 * whether the packet is correct.
 * 
 * 3) Correct packet is sent to Dowlink.AX25_FRAMES ActiveMQ topic and is
 * processed by SDC.
 * 
 * @author Kaarel Hanson
 * @since 10.03.2014
 */
public class ProxyServer extends RouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(ProxyServer.class);

    @Value("${heart.beat.interval}")
    private int heartBeatInterval = 3000;

    @Value("${service.id}")
    private String serviceId;

    @Value("${service.version}")
    private String proxyServerVersion;

    @Value("${service.name}")
    private String serviceName;

    @Value("${proxy.port}")
    private int proxyPort;

    @Value("${proxy.host}")
    private String proxyHost;

    @Autowired
    private AddHeaders headers;

    @Autowired
    private RemotePacketServlet remotePacketServlet;

    @Autowired
    private Ax25UiFrameRemoteProcessor remoteProcessor;

    @Autowired
    private LocationContactEventExtractor eventExtractor;

    @Autowired
    private LocationContactEventHolder eventHolder;

    @Override
    public void configure() throws Exception {
        setupJetty();
        setupCamelRoute();
    }

    private void setupCamelRoute() {
        // @formatter:off

        from("direct:frame-submit")
            .bean(remoteProcessor)
            .filter(remoteProcessor)
            .to(Downlink.AX25_FRAMES)
            .end();

        /* HEART BEAT */
        BusinessCard card = new BusinessCard(serviceId, serviceName);
        card.setPeriod(heartBeatInterval);
        card.setDescription(String.format("Proxy server; version: %s", proxyServerVersion));
        from("timer:heartbeat?fixedRate=true&period=" + heartBeatInterval)
            .bean(card, "touch")
            .bean(headers)
            .to(StandardEndpoints.MONITORING);

        // listen for Track commands
        from(StandardEndpoints.COMMANDS + "?selector=class='" + Track.class.getSimpleName() + "'")
            .bean(eventExtractor)
            .bean(eventHolder);

        // @formatter:on
    }

    /**
     * Sets up, configures and starts Jetty on the host and port as describe in
     * the service.properties configuration file
     * 
     * @throws Exception
     */
    private void setupJetty() throws Exception {
        InetSocketAddress address = new InetSocketAddress(proxyHost, proxyPort);
        Server server = new Server(address);
        server.setThreadPool(new QueuedThreadPool());
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        context.addServlet(new ServletHolder(remotePacketServlet), "/submitFrame");
        context.addServlet(new ServletHolder(new DefaultServlet()), "/html/*");
        ResourceCollection collection = new ResourceCollection(new String[] { "." });
        context.setBaseResource(collection);
        server.setHandler(context);
        server.start();
    }

    public static void main(String[] args) {
        LOG.info("Proxy Server Started");
        try {
            AbstractApplicationContext context = new ClassPathXmlApplicationContext("META-INF/spring/camel-context.xml");
            ProxyServer proxy = context.getAutowireCapableBeanFactory().createBean(ProxyServer.class);

            Main m = new Main();
            m.setApplicationContext(context);

            m.addRouteBuilder(proxy);
            m.run(args);
        } catch (Exception e) {
            LOG.error("Proxy failed to start", e);
        }
    }

}
