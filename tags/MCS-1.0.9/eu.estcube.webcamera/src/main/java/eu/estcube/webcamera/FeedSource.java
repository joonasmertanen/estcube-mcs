package eu.estcube.webcamera;

import org.apache.camel.model.RouteDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class FeedSource {
    private static final Logger LOG = LoggerFactory.getLogger(FeedSource.class);

    private String id;

    public abstract RouteDefinition getRouteAction();

    public FeedSource(String id) {
        this.id = id;
    }

    public String getID() {
        return id;
    }
}
