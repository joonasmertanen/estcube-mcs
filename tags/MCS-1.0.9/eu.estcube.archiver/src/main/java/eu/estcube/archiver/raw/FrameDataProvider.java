package eu.estcube.archiver.raw;

import eu.estcube.domain.transport.Direction;

public interface FrameDataProvider {
	public Direction getDirection();
}
