package eu.estcube.archiver.notifier;

import java.security.GeneralSecurityException;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.estcube.common.utils.Emailer;

/**
 * Sends an e-mail to a configured recipient about the status/failure
 * 
 * @author Kaarel Hanson
 * 
 */
@Component
public class MailNotifier extends Emailer {

    private static final Logger LOG = LoggerFactory.getLogger(MailNotifier.class);

    @Value("${email.to}")
    private String toEmail;

    @Value("${email.title}")
    private String title;

    @Value("${email.content}")
    private String content;

    public void sendNotification() {
        try {
            // Send email only, if recipient has been declared in the properties
            // file
            if (toEmail.length() > 0) {
                sendEmail(toEmail, title, content);
            }
        } catch (GeneralSecurityException e) {
            LOG.error("Notification e-mail sending failed", e);
        } catch (AddressException e) {
            LOG.error("Notification e-mail sending failed", e);
        } catch (MessagingException e) {
            LOG.error("Notification e-mail sending failed", e);
        }
    }

}
