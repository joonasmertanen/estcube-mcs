package eu.estcube.archiver.raw.tnc;

import javax.sql.DataSource;

import eu.estcube.archiver.notifier.MailNotifier;
import eu.estcube.archiver.raw.sql.AbstractSqlFrameArchiver;

public class OracleTncFrameArchiver extends AbstractSqlFrameArchiver {

    public OracleTncFrameArchiver(DataSource dataSource, MailNotifier notifier) {
        super(dataSource, notifier);
    }

    @Override
    public String getAutoIncrementFunction() {
        return "MCS.TNC_FRAMES_SEQ.NEXTVAL";
    }

    @Override
    public String getDetailsAutoIncrementFunction() {
        return "MCS.TNC_FRAME_DETAILS_SEQ.NEXTVAL";
    }

    @Override
    public String[] getColumns() {
        return new String[] { TncToSqlProvider.COL_RECEPTION_TIME,
                TncToSqlProvider.COL_SATELLITE, TncToSqlProvider.COL_COMMAND,
                TncToSqlProvider.COL_DATA, TncToSqlProvider.COL_TARGET };
    }

    @Override
    public String getTable() {
        return "MCS.TNC_FRAMES";
    }

    @Override
    public String getDetailsTable() {
        return "MCS.TNC_FRAME_DETAILS";
    }

    @Override
    public String getTimestampFunction() {
        return "SYSDATE";
    }
}
