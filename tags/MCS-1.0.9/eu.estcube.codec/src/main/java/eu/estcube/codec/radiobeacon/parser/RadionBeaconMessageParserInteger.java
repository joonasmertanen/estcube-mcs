package eu.estcube.codec.radiobeacon.parser;

import org.hbird.exchange.core.Parameter;

import eu.estcube.common.Constants;

public class RadionBeaconMessageParserInteger extends RadioBeaconMessageParser {

    protected Integer startIndex;

    protected Integer endIndex;

    protected Parameter parameterBase;

    private Integer result;

    public RadionBeaconMessageParserInteger(Integer startIndex, Integer length, Parameter parameterBase) {
        this.startIndex = startIndex;
        this.endIndex = startIndex + length;
        this.parameterBase = parameterBase;
    }

    @Override
    public String parse(String message) {
        String part = getValidStringPart(message, this.startIndex, this.endIndex);
        if (part == null) {
            result = null;
            return null;
        }
        result = Integer.parseInt(cwToHex(part), Constants.HEX_BASE);
        return String.valueOf(result);
    }

    @Override
    public Parameter parseToIssued(String message) {
        parse(message);
        if (result == null) {
            return null;
        }
        Parameter parameter = parameterBase.cloneEntity();
        parameter.setValue(result);
        return parameter;
    }
}
