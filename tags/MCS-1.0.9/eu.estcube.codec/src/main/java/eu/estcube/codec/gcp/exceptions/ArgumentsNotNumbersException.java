package eu.estcube.codec.gcp.exceptions;

public class ArgumentsNotNumbersException extends NumberFormatException {

    /**
     * 
     */
    private static final long serialVersionUID = -2786857398353700876L;

    public ArgumentsNotNumbersException(String commandName) {
        super("Some of the arguments are not numbers!!");
    }
}
