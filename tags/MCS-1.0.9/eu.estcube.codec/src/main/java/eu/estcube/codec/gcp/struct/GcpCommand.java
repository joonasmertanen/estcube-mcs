package eu.estcube.codec.gcp.struct;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import eu.estcube.codec.gcp.struct.parameters.GcpParameterMaxLength;

public class GcpCommand {

    private String name;

    private String description;

    private Integer id;

    private final List<GcpSubsystem> subsystems = new ArrayList<GcpSubsystem>();

    private final List<GcpParameter> parameters = new ArrayList<GcpParameter>();

    public GcpCommand(Integer id, String name, String description) {
        setId(id);
        setName(name);
        setDescription(description);
    }

    public GcpCommand(Element node) {

        int id = Integer.valueOf(node.getElementsByTagName("id").item(0).getTextContent());
        String name = node.getElementsByTagName("name").item(0).getTextContent();
        String description = node.getElementsByTagName("description").item(0).getTextContent();

        setId(id);
        setName(name);
        setDescription(description);

        NodeList parametersList = node.getElementsByTagName("param");
        for (int p = 0; p < parametersList.getLength(); p++) {
            addParameter(GcpParameter.getParameterObjectFromType((Element) parametersList.item(p)));
        }

        NodeList subsystemsList = node.getElementsByTagName("subsys");
        for (int p = 0; p < subsystemsList.getLength(); p++) {
            addSubsystem(new GcpSubsystem((Element) subsystemsList.item(p)));

        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void addSubsystem(GcpSubsystem subsystem) {
        subsystems.add(subsystem);
    }

    public List<GcpSubsystem> getSubsystems() {
        return subsystems;
    }

    public void addParameter(GcpParameter parameter) {

        if (parameters.size() > 0 && parameters.get(parameters.size() - 1) instanceof GcpParameterMaxLength) {
            throw new RuntimeException(
                    "Can\'t add any more parameters. MaxLength parameter is included to the list. After that there can not be any more parameters!");
        }

        if (parameter.getLength() != -1 && parameter.getLength() < 8) {
            throw new RuntimeException("Command can only deal with parameters that work with full bytes.");
        }

        parameters.add(parameter);
    }

    public List<GcpParameter> getParameters() {
        return parameters;
    }

}
