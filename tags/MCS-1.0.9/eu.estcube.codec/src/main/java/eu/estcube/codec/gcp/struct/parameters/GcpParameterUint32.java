package eu.estcube.codec.gcp.struct.parameters;

import java.nio.ByteBuffer;

import org.apache.commons.lang3.ArrayUtils;
import org.w3c.dom.Element;

public class GcpParameterUint32 extends GcpParameterNumberic {

    public final static long MIN_VALUE = 0;
    public final static long MAX_VALUE = 4294967295L;
    public final static long BYTES = 4;

    public GcpParameterUint32(String name, String description, String unit, Boolean isLittleEndian) {
        super(name, description, unit, isLittleEndian);
    }

    public GcpParameterUint32(Element e) {
        super(e);
    }

    @Override
    public int getLength() {
        return 32;
    }

    @Override
    public Long toValue(String input) {

        if (isHexString(input)) {
            return (Long) toValueFromHex(input);
        }

        long num = Long.parseLong(input);
        if (num < MIN_VALUE) {
            throw new NumberFormatException(getClass().getName() + " value must be bigger than " + MIN_VALUE);
        }
        if (num > MAX_VALUE) {
            throw new NumberFormatException(getClass().getName() + " value must be less than " + MAX_VALUE);
        }
        return num;
    }

    @Override
    public byte[] toBytes(Object value) {
        ByteBuffer buffer = ByteBuffer.allocate(8);

        buffer.putLong(toValue(String.valueOf(value)));
        byte[] bytes = { buffer.get(4), buffer.get(5), buffer.get(6), buffer.get(7) };

        if (getIsLittleEndian()) {
            ArrayUtils.reverse(bytes);
        }

        return bytes;
    }

    @Override
    public Long toValue(byte[] bytes) {

        if (bytes.length != BYTES) {
            throw new RuntimeException("byte[] length of the argument must be " + BYTES + "! Was " + bytes.length);
        }

        byte byte1 = bytes[0];
        byte byte2 = bytes[1];
        byte byte3 = bytes[2];
        byte byte4 = bytes[3];

        if (getIsLittleEndian()) {
            byte1 = bytes[3];
            byte2 = bytes[2];
            byte3 = bytes[1];
            byte4 = bytes[0];
        }

        return ((byte1 & 0xFF) << 24 | (byte2 & 0xFF) << 16 | (byte3 & 0xFF) << 8 | (byte4 & 0xFF)) & 0xffffffffL;
    }

    @Override
    public Class<?> getValueClass() {
        return Long.class;
    }

}
