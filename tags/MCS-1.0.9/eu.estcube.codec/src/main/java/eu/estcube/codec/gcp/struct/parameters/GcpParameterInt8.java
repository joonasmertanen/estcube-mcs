package eu.estcube.codec.gcp.struct.parameters;

import org.w3c.dom.Element;

public class GcpParameterInt8 extends GcpParameterNumberic {

    public final static long MIN_VALUE = 0;
    public final static long MAX_VALUE = 65535;
    public final static long BYTES = 1;

    public GcpParameterInt8(String name, String description, String unit) {
        super(name, description, unit, false);
    }

    public GcpParameterInt8(Element e) {
        super(e);
    }

    @Override
    public int getLength() {
        return 8;
    }

    @Override
    public Byte toValue(String input) {
        if (isHexString(input)) {
            return (Byte) toValueFromHex(input);
        }
        return Byte.parseByte(input);
    }

    @Override
    public byte[] toBytes(Object value) {
        return new byte[] { toValue(String.valueOf(value)) };
    }

    @Override
    public Byte toValue(byte[] bytes) {

        if (bytes.length != BYTES) {
            throw new RuntimeException("byte[] length of the argument must be " + BYTES + "! Was " + bytes.length);
        }

        return (byte) bytes[0];
    }

    @Override
    public Class<?> getValueClass() {
        return Byte.class;
    }

}
