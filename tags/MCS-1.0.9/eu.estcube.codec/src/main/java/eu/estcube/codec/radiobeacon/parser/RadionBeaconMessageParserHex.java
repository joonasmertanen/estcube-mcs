package eu.estcube.codec.radiobeacon.parser;

import org.hbird.exchange.core.Label;

public class RadionBeaconMessageParserHex extends RadioBeaconMessageParser {

    protected Integer startIndex;

    protected Integer endIndex;

    protected Label parameterBase;

    public RadionBeaconMessageParserHex(Integer startIndex, Integer length, Label parameterBase) {
        this.startIndex = startIndex;
        this.endIndex = startIndex + length;
        this.parameterBase = parameterBase;
    }

    @Override
    public String parse(String hex) {
        String part = getValidStringPart(hex, this.startIndex, this.endIndex);
        if (part == null) {
            return null;
        }
        return part;
    }

    @Override
    public Label parseToIssued(String message) {
        String part = parse(message);
        if (part == null) {
            return null;
        }
        Label parameter = parameterBase.cloneEntity();
        parameter.setValue(part);
        return parameter;
    }

}
