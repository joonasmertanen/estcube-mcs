package eu.estcube.codec.gcp.struct;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import eu.estcube.codec.gcp.exceptions.SubsystemNotFoundException;

public class GcpSubsystemIdProviderTest {

    @Test
    public void getId() throws SubsystemNotFoundException {
        assertEquals(0, (int) GcpSubsystemIdProvider.getId("EPS"));
        assertEquals(1, (int) GcpSubsystemIdProvider.getId("COM"));
        assertEquals(2, (int) GcpSubsystemIdProvider.getId("CDHS"));
        assertEquals(3, (int) GcpSubsystemIdProvider.getId("ADCS"));
        assertEquals(4, (int) GcpSubsystemIdProvider.getId("PL"));
        assertEquals(5, (int) GcpSubsystemIdProvider.getId("CAM"));
        assertEquals(6, (int) GcpSubsystemIdProvider.getId("GS"));
        assertEquals(7, (int) GcpSubsystemIdProvider.getId("PC"));
        assertEquals(8, (int) GcpSubsystemIdProvider.getId("PC2"));

        try {
            GcpSubsystemIdProvider.getId("NOT_EXISTING");
            fail("Should have thrown exception ");
        } catch (SubsystemNotFoundException e) {

        }
    }

    @Test
    public void getName() throws SubsystemNotFoundException {
        assertEquals("EPS", GcpSubsystemIdProvider.getName(0));
        assertEquals("COM", GcpSubsystemIdProvider.getName(1));
        assertEquals("CDHS", GcpSubsystemIdProvider.getName(2));
        assertEquals("ADCS", GcpSubsystemIdProvider.getName(3));
        assertEquals("PL", GcpSubsystemIdProvider.getName(4));
        assertEquals("CAM", GcpSubsystemIdProvider.getName(5));
        assertEquals("GS", GcpSubsystemIdProvider.getName(6));
        assertEquals("PC", GcpSubsystemIdProvider.getName(7));
        assertEquals("PC2", GcpSubsystemIdProvider.getName(8));

        try {
            GcpSubsystemIdProvider.getName(12345);
            fail("Should have thrown exception ");
        } catch (SubsystemNotFoundException e) {

        }
    }
}
