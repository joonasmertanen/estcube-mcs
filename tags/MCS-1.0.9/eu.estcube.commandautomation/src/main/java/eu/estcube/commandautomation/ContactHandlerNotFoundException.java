package eu.estcube.commandautomation;

public class ContactHandlerNotFoundException extends Exception {

    private static final long serialVersionUID = -8850944382422710668L;

    public ContactHandlerNotFoundException(String trackingId) {
        super("Contact handler not set for tracking id: " + trackingId
                + ". Did the system just rest or did it not receive prepare event?");
    }

}
