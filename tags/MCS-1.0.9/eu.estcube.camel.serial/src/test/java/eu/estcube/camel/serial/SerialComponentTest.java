package eu.estcube.camel.serial;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.TypeConverter;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.transport.serial.SerialAddress.DataBits;
import org.apache.mina.transport.serial.SerialAddress.FlowControl;
import org.apache.mina.transport.serial.SerialAddress.Parity;
import org.apache.mina.transport.serial.SerialAddress.StopBits;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SerialComponentTest {

    private static final String URI = "NOT USED ACTUALLY";
    private static final String PORT = "/dev/ttyS818";

    @Mock
    private ExecutorService executor;

    @Mock
    private InterruptedException interruptedException;

    private Map<String, Object> parameters;

    @Mock
    private CamelContext camelContext;

    @Mock
    private TypeConverter converter;

    private SerialComponent component;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        component = new SerialComponent();
        component.setCamelContext(camelContext);
        parameters = new HashMap<String, Object>();
        fillParameters();

        inOrder = inOrder(executor, interruptedException, camelContext, converter);
    }

    /**
     * 
     */
    void fillParameters() {
        parameters.put(SerialComponent.BAUD, "9600");
        parameters.put(SerialComponent.DATA_BITS, "6");
        parameters.put(SerialComponent.STOP_BITS, "1.5");
        parameters.put(SerialComponent.PARITY, "MARK");
        parameters.put(SerialComponent.FLOW_CONTROL, "NONE");
        parameters.put(SerialComponent.FILTERS, Collections.EMPTY_LIST);
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialComponent#doStop(ExecutorService)}.
     * 
     * @throws Exception
     */
    @Test
    public void testDoStopExecutor() throws Exception {
        component.doStop(executor);
        inOrder.verify(executor, times(1)).shutdown();
        inOrder.verify(executor, times(1)).awaitTermination(3, TimeUnit.SECONDS);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialComponent#doStop(ExecutorService)}.
     * 
     * @throws Exception
     */
    @Test
    public void testDoStop() throws Exception {
        component.doStop();
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialComponent#doStop(ExecutorService)}.
     * 
     * @throws Exception
     */
    @Test
    public void testDoStopExecutorWithException() throws Exception {
        doThrow(interruptedException).when(executor).awaitTermination(3, TimeUnit.SECONDS);
        component.doStop(executor);
        inOrder.verify(executor, times(1)).shutdown();
        inOrder.verify(executor, times(1)).awaitTermination(3, TimeUnit.SECONDS);
        inOrder.verify(executor, times(1)).shutdownNow();
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialComponent#createEndpoint(java.lang.String, java.lang.String, java.util.Map)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testCreateEndpointStringStringMapOfStringObject() throws Exception {
        when(camelContext.getTypeConverter()).thenReturn(converter);
        // when(converter.convertTo(Integer.class, "9600")).thenReturn(9600);
        when(converter.mandatoryConvertTo(Integer.class, "9600")).thenReturn(9600);
        when(converter.convertTo(Integer.class, "6")).thenReturn(6);
        when(converter.convertTo(Double.class, "1.5")).thenReturn(1.5D);
        when(converter.convertTo(String.class, "MARK")).thenReturn("MARK");
        when(converter.convertTo(String.class, "NONE")).thenReturn("NONE");

        Endpoint ep1 = component.createEndpoint(URI, PORT, parameters);
        fillParameters();
        Endpoint ep2 = component.createEndpoint(URI, PORT + "1", parameters);
        fillParameters();
        Endpoint ep3 = component.createEndpoint(URI, PORT, parameters);
        assertNotNull(ep1);
        assertNotNull(ep2);
        assertNotNull(ep3);
        assertNotSame(ep1, ep2);
        assertSame(ep1, ep3);
        assertTrue(parameters.isEmpty());

        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).convertTo(Integer.class, "6");
        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).convertTo(Double.class, "1.5");
        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).convertTo(String.class, "MARK");
        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).convertTo(String.class, "NONE");
        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).mandatoryConvertTo(Integer.class, "9600");

        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).convertTo(Integer.class, "6");
        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).convertTo(Double.class, "1.5");
        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).convertTo(String.class, "MARK");
        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).convertTo(String.class, "NONE");
        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).mandatoryConvertTo(Integer.class, "9600");

        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialComponent#getDataBits(int)}.
     */
    @Test
    public void testGetDataBits() {
        assertEquals(SerialComponent.DEFAULT_DATA_BITS, SerialComponent.getDataBits(0));
        assertEquals(SerialComponent.DEFAULT_DATA_BITS, SerialComponent.getDataBits(1));
        assertEquals(SerialComponent.DEFAULT_DATA_BITS, SerialComponent.getDataBits(2));
        assertEquals(SerialComponent.DEFAULT_DATA_BITS, SerialComponent.getDataBits(3));
        assertEquals(SerialComponent.DEFAULT_DATA_BITS, SerialComponent.getDataBits(4));
        assertEquals(DataBits.DATABITS_5, SerialComponent.getDataBits(5));
        assertEquals(DataBits.DATABITS_6, SerialComponent.getDataBits(6));
        assertEquals(DataBits.DATABITS_7, SerialComponent.getDataBits(7));
        assertEquals(DataBits.DATABITS_8, SerialComponent.getDataBits(8));
        assertEquals(SerialComponent.DEFAULT_DATA_BITS, SerialComponent.getDataBits(9));
        assertEquals(SerialComponent.DEFAULT_DATA_BITS, SerialComponent.getDataBits(10));
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialComponent#getStopBits(java.lang.Double)}
     * .
     */
    @Test
    public void testGetStopBits() {
        assertEquals(SerialComponent.DEFAULT_STOP_BITS, SerialComponent.getStopBits(0.0D));
        assertEquals(SerialComponent.DEFAULT_STOP_BITS, SerialComponent.getStopBits(0.5D));
        assertEquals(SerialComponent.DEFAULT_STOP_BITS, SerialComponent.getStopBits(1.01D));
        assertEquals(SerialComponent.DEFAULT_STOP_BITS, SerialComponent.getStopBits(1.1D));
        assertEquals(SerialComponent.DEFAULT_STOP_BITS, SerialComponent.getStopBits(2.2D));
        assertEquals(SerialComponent.DEFAULT_STOP_BITS, SerialComponent.getStopBits(2.02D));
        assertEquals(SerialComponent.DEFAULT_STOP_BITS, SerialComponent.getStopBits(51.112D));
        assertEquals(StopBits.BITS_1, SerialComponent.getStopBits(1.0D));
        assertEquals(StopBits.BITS_1_5, SerialComponent.getStopBits(1.5D));
        assertEquals(StopBits.BITS_2, SerialComponent.getStopBits(2.0D));
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialComponent#getEnumValue(java.lang.Class, java.lang.String, java.lang.Enum)}
     * .
     */
    @Test
    public void testGetEnumValue() {
        for (Parity p : Parity.values()) {
            assertEquals(p, SerialComponent.getEnumValue(Parity.class, p.name(), SerialComponent.DEFAULT_PARITY));
        }
        for (FlowControl f : FlowControl.values()) {
            assertEquals(f,
                    SerialComponent.getEnumValue(FlowControl.class, f.name(), SerialComponent.DEFAULT_FLOW_CONTROL));
        }
        assertEquals(SerialComponent.DEFAULT_FLOW_CONTROL, SerialComponent.getEnumValue(FlowControl.class,
                Parity.MARK.name(), SerialComponent.DEFAULT_FLOW_CONTROL));
        assertEquals(SerialComponent.DEFAULT_PARITY, SerialComponent.getEnumValue(Parity.class,
                FlowControl.XONXOFF_IN_OUT.name(), SerialComponent.DEFAULT_PARITY));
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialComponent#clearParameters(java.util.Map)}
     * .
     */
    @Test
    public void testClearParameters() {
        component.clearParameters(parameters);
        assertTrue(parameters.isEmpty());
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialComponent#createSerialEndpoint(java.lang.String, java.lang.String, java.util.Map)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testCreateSerialEndpointStringStringMapOfStringObject() throws Exception {
        when(camelContext.getTypeConverter()).thenReturn(converter);
        when(converter.mandatoryConvertTo(Integer.class, "9600")).thenReturn(9600);
        when(converter.convertTo(Integer.class, "6")).thenReturn(6);
        when(converter.convertTo(Double.class, "1.5")).thenReturn(1.5D);
        when(converter.convertTo(String.class, "MARK")).thenReturn("MARK");
        when(converter.convertTo(String.class, "NONE")).thenReturn("NONE");

        SerialEndpoint ep1 = component.createSerialEndpoint(URI, PORT, parameters);
        assertTrue(parameters.isEmpty());
        fillParameters();
        SerialEndpoint ep2 = component.createSerialEndpoint(URI, PORT, parameters);
        assertTrue(parameters.isEmpty());
        assertNotNull(ep1);
        assertNotNull(ep2);
        assertNotSame(ep1, ep2);

        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).convertTo(Integer.class, "6");
        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).convertTo(Double.class, "1.5");
        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).convertTo(String.class, "MARK");
        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).convertTo(String.class, "NONE");
        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).mandatoryConvertTo(Integer.class, "9600");

        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).convertTo(Integer.class, "6");
        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).convertTo(Double.class, "1.5");
        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).convertTo(String.class, "MARK");
        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).convertTo(String.class, "NONE");
        inOrder.verify(camelContext, times(1)).getTypeConverter();
        inOrder.verify(converter, times(1)).mandatoryConvertTo(Integer.class, "9600");

        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialComponent#createSerialEndpoint(java.lang.String, java.lang.String, int, org.apache.mina.transport.serial.SerialAddress.DataBits, org.apache.mina.transport.serial.SerialAddress.StopBits, org.apache.mina.transport.serial.SerialAddress.Parity, org.apache.mina.transport.serial.SerialAddress.FlowControl)}
     * .
     */
    @Test
    public void testCreateSerialEndpointStringStringIntDataBitsStopBitsParityFlowControl() {
        SerialConfig config = new SerialConfig();
        config.setPort(PORT);
        config.setBaud(1200);
        config.setDataBits(DataBits.DATABITS_5);
        config.setStopBits(StopBits.BITS_1_5);
        config.setParity(Parity.SPACE);
        config.setFlowControl(FlowControl.RTSCTS_OUT);
        config.setFilters(Collections.<IoFilter> emptyList());
        SerialEndpoint ep1 = component.createSerialEndpoint(URI, config);
        SerialEndpoint ep2 = component.createSerialEndpoint(URI, config);
        assertNotNull(ep1);
        assertNotNull(ep2);
        assertNotSame(ep1, ep2);
    }
}
