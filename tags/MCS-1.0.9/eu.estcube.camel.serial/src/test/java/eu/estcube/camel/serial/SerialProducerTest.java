package eu.estcube.camel.serial;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.mina.core.session.IoSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SerialProducerTest {

    private static final byte[] BYTES = new byte[] { 0x01, 0x00, 0x01, 0x0C, 0x00, 0x0D, 0x0E };

    @Mock
    private Endpoint endpoint;

    @Mock
    private IoSession session;

    @Mock
    private Exchange exchange;

    @Mock
    private Message in;

    private InOrder inOrder;

    private SerialProducer serialProducer;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        serialProducer = new SerialProducer(endpoint, session);
        inOrder = inOrder(endpoint, session, exchange, in);
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialProducer#process(org.apache.camel.Exchange)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testProcess() throws Exception {
        when(exchange.getIn()).thenReturn(in);
        when(in.getBody()).thenReturn(BYTES);
        serialProducer.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(in, times(1)).getBody();
        ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        inOrder.verify(session, times(1)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        assertTrue(Arrays.equals(BYTES, captor.getValue()));
    }
}
