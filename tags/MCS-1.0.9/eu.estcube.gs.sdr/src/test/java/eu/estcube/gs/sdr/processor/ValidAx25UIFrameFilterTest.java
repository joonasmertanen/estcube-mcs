/**
 *
 */
package eu.estcube.gs.sdr.processor;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.domain.transport.ax25.Ax25FrameStatus;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ValidAx25UIFrameFilterTest {

    @InjectMocks
    private Ax25UIFrame frame;

    @InjectMocks
    private Ax25FrameStatus status;

    @InjectMocks
    private ValidAx25UIFrameFilter filter;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        frame.setStatus(status);
    }

    @Test
    public void testValid() throws Exception {
        assertTrue(filter.valid(frame));
    }

    @Test
    public void testNotValid() throws Exception {
        status.setErrorFcs(true);
        assertFalse(filter.valid(frame));
    }
}
