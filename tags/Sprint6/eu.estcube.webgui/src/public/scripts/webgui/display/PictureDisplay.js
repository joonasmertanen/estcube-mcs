dojo.provide("webgui.display.PictureDisplay");

dojo.require("dijit.form.TextBox");
dojo.require("dijit.form.Button");
dojo.require("dijit.layout.ContentPane");
dojo.require("dijit.layout.StackContainer");
dojo.require("dojox.socket");
dojo.require("dojox.gfx");

dojo.declare("webgui.display.PictureDisplay", null, {
	
	constructor: function() {
		console.log("PictureDisplay!");
				
		var pictureTab = new dijit.layout.ContentPane({
        	id: "pictureTab",
        	title: "Picture",
        	preventCache: true      		
       });
		
        var tempCont;
        tempCont = dijit.byId("PictureView");        
        tempCont.addChild(pictureTab);
        		
		dojo.subscribe("fromWebCam", function(message) {
			pictureTab.containerNode.innerHTML ="<img src=\"data:image/jpeg;base64," + message + "\" alt=\"Picture from webcamera.\"/>" ;
		});
	}
});