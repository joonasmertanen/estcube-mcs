package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;
import eu.estcube.gsconnector.commands.radio.Configuration;

public class ConfigurationTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand = new TelemetryCommand("CONFIGURATION");
    private Configuration createMessage = new Configuration();
    private static final Logger LOG = LoggerFactory.getLogger(HamlibDecoderTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @Test
    public void testCreateMessageString() {
        string.append("+3\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
