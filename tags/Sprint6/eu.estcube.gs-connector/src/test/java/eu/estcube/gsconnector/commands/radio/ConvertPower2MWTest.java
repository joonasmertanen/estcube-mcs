package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class ConvertPower2MWTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private ConvertPower2MW createMessage = new ConvertPower2MW();
    private static final Logger LOG = LoggerFactory.getLogger(ConvertPower2MWTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("CONVERT_POWER2MW");
        telemetryCommand.addParameter("Power", 0.0);
        telemetryCommand.addParameter("Frequency", 0.0);
        telemetryCommand.addParameter("Mode", "..");
        string.append("+2 0.0 0.0 ..\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
