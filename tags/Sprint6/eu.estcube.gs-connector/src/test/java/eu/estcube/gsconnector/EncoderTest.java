package eu.estcube.gsconnector;

import java.nio.charset.Charset;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.MessageEvent;

import junit.framework.TestCase;


import static org.junit.Assert.*;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.MessageEvent;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EncoderTest extends TestCase {

    

    private static final Logger LOG = LoggerFactory.getLogger(EncoderTest.class);
    private Encoder encoder;
    private MessageEvent message;
    
    @Before
    public void setUp() throws Exception {
        encoder = new Encoder();
        message = Mockito.mock(MessageEvent.class);
        
    }

    
    /**
     * Test method for {@link eu.estcube.gsconnector.Encoder#checkEndLine(org.jboss.netty.channel.ChannelHandlerContext, org.jboss.netty.channel.Channel, java.lang.Object)}.
     * @throws Exception 
     */
    @Test
    public void testcheckEndLineChannelHandlerContextMessageEvent() {
        Mockito.when(message.getMessage()).thenReturn("1");
        String str = "1\n";
        byte[] bytes=str.getBytes(Charset.forName("ASCII"));
        assertTrue(Arrays.equals(bytes, encoder.checkEndLine(message)));
        
        Mockito.when(message.getMessage()).thenReturn("1\n");
        str = "1\n";
        bytes=str.getBytes(Charset.forName("ASCII"));
        assertTrue(Arrays.equals(bytes, encoder.checkEndLine(message)));
        
        Mockito.when(message.getMessage()).thenReturn("");
        str = null;
        bytes=null;
        assertTrue(Arrays.equals(bytes, encoder.checkEndLine(message)));

        Mockito.when(message.getMessage()).thenReturn("1\\n");
        str = "1\\n\n";
        bytes=str.getBytes(Charset.forName("ASCII"));
        assertTrue(Arrays.equals(bytes, encoder.checkEndLine(message)));
        
        Mockito.when(message.getMessage()).thenReturn("11\n11");
        str = "11\n11\n";
        bytes=str.getBytes(Charset.forName("ASCII"));
        assertTrue(Arrays.equals(bytes, encoder.checkEndLine(message)));
        
    }

}
