package eu.estcube.gsconnector.commands;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;
import eu.estcube.gsconnector.commands.SendRawCmd;

public class SendRawCmdTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SendRawCmd createMessage = new SendRawCmd();
    private static final Logger LOG = LoggerFactory.getLogger(SendRawCmdTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("SEND_RAW_CMD");
        telemetryCommand.addParameter("Cmd", "/x95");
        string.append("+w /x95\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
