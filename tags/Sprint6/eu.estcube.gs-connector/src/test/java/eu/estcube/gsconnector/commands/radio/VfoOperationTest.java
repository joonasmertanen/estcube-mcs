package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class VfoOperationTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private VfoOperation createMessage = new VfoOperation();
    private static final Logger LOG = LoggerFactory.getLogger(VfoOperationTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("VFO_OPERATION");
        telemetryCommand.addParameter("Mem/VFO Op", "CPY");
        string.append("+G CPY\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
