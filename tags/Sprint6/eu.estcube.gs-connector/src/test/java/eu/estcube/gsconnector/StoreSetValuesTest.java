package eu.estcube.gsconnector;

import static org.junit.Assert.*;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultExchange;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;

public class StoreSetValuesTest {

    private static final Logger LOG = LoggerFactory.getLogger(StoreSetValuesTest.class);
    private StoreSetValues storeSetValues;
    private DefaultExchange exchange;
    private CamelContext ctx;

    @Before
    public void setUp() throws Exception {
        storeSetValues = new StoreSetValues();
        ctx = Mockito.mock(CamelContext.class);
        exchange = new DefaultExchange(ctx);
    }

    @Test
    public void testStoreData() {
        TelemetryCommand command = new TelemetryCommand("SET_POSITION");
        command.addParameter("Elevation", "10");
        exchange.getIn().setBody(command);
        storeSetValues.storeData(exchange);
        assertTrue(storeSetValues.getRequiredParameterValueDataStore().get("SET_POSITION").equals(command));
        assertTrue(storeSetValues.getRequiredParameterValueDataStore().size() == 1);
        assertTrue(exchange.getIn().getHeader("polling").equals("setCommand"));
        storeSetValues.getRequiredParameterValueDataStore().remove("SET_POSITION");

        command = new TelemetryCommand("GET_POSITION");
        command.addParameter("Elevation", "10");
        exchange.getIn().setHeader("polling", "getCommand");
        exchange.getIn().setBody(command);
        storeSetValues.storeData(exchange);
        assertTrue(storeSetValues.getRequiredParameterValueDataStore().size() == 0);
        assertTrue(exchange.getIn().getHeader("polling").equals("getCommand"));

        command = new TelemetryCommand("AnyCommand");
        command.addParameter("Elevation", "10");
        exchange.getIn().removeHeader("polling");
        exchange.getIn().setBody(command);
        storeSetValues.storeData(exchange);
        assertTrue(storeSetValues.getRequiredParameterValueDataStore().size() == 0);
        assertTrue(exchange.getIn().getHeader("polling").equals("notRequired"));

        exchange.getIn().removeHeader("polling");
        exchange.getIn().setBody("SomeKindOfWrongMessage");
        storeSetValues.storeData(exchange);
        assertTrue(exchange.getIn().getHeaders().isEmpty());
        assertTrue(exchange.getIn().getBody() == null);
    }

}
