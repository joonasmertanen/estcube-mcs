package eu.estcube.gsconnector.commands;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class HelpTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private Help createMessage = new Help();
    private static final Logger LOG = LoggerFactory.getLogger(HelpTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("HELP");
        string.append("??+_\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
