package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class SetRprtShiftTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SetRprtShift createMessage = new SetRprtShift();
    private static final Logger LOG = LoggerFactory.getLogger(SetRprtShiftTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("SET_RPTR_SHIFT");
        telemetryCommand.addParameter("Rptr Shift", "+");
        string.append("+R +\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
