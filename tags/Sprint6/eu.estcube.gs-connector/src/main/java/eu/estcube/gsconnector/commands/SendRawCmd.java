package eu.estcube.gsconnector.commands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class SendRawCmd extends CommandStringBuilder {
    private StringBuilder messageString;

    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString = new StringBuilder();
        messageString.append("+");
        messageString.append("w");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Cmd"));
        messageString.append("\n");
        return messageString;
    }
}
