package eu.estcube.gsconnector;

import java.util.HashMap;
import java.util.Map;

import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneDecoder;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.domain.TelemetryRotatorConstants;
import eu.estcube.gsconnector.commands.Capabilities;
import eu.estcube.gsconnector.commands.CommandStringBuilder;
import eu.estcube.gsconnector.commands.GetInfo;
import eu.estcube.gsconnector.commands.Help;
import eu.estcube.gsconnector.commands.SendRawCmd;
import eu.estcube.gsconnector.commands.radio.RadioStatus;
import eu.estcube.gsconnector.commands.rotator.*;

@Component("rotatorEncoder")
public class TelemetryObjecRotatorEncoder extends OneToOneEncoder {

    private Map<String, CommandStringBuilder> commandsHashMap;

    private static final Logger LOG = LoggerFactory.getLogger(TelemetryObjecRotatorEncoder.class);

    public TelemetryObjecRotatorEncoder() {
        commandsHashMap = new HashMap<String, CommandStringBuilder>();
        commandsHashMap.put(TelemetryRotatorConstants.SET_POSITION, new SetPosition());
        commandsHashMap.put(TelemetryRotatorConstants.GET_POSITION, new GetPosition());
        commandsHashMap.put(TelemetryRotatorConstants.STOP, new Stop());
        commandsHashMap.put(TelemetryRotatorConstants.PARK, new Park());
        commandsHashMap.put(TelemetryRotatorConstants.MOVE, new Move());
        commandsHashMap.put(TelemetryRotatorConstants.CAPABILITIES, new Capabilities());
        commandsHashMap.put(TelemetryRotatorConstants.RESET, new Reset());
        commandsHashMap.put(TelemetryRotatorConstants.SET_CONFIG, new SetConfig());
        commandsHashMap.put(TelemetryRotatorConstants.GET_INFO, new GetInfo());
        commandsHashMap.put(TelemetryRotatorConstants.SEND_RAW_CMD, new SendRawCmd());
        commandsHashMap.put(TelemetryRotatorConstants.LONLAT2LOC, new LonLat2Loc());
        commandsHashMap.put(TelemetryRotatorConstants.LOC2LONLAT, new Loc2LonLat());
        commandsHashMap.put(TelemetryRotatorConstants.DMS2DEC, new Dms2Dec());
        commandsHashMap.put(TelemetryRotatorConstants.DEC2DMS, new Dec2Dms());
        commandsHashMap.put(TelemetryRotatorConstants.DMMM2DEC, new Dmm2Dec());
        commandsHashMap.put(TelemetryRotatorConstants.DEC2DMMM, new Dec2Dmm());
        commandsHashMap.put(TelemetryRotatorConstants.QRB, new QRB());
        commandsHashMap.put(TelemetryRotatorConstants.AZIMUTH_SHORTPATH2LONGPATH, new AzimuthShortPath2LongPath());
        commandsHashMap.put(TelemetryRotatorConstants.DISTANCE_SHORTPATH2LONGPATH, new DistanceShortPath2LongPath());
        commandsHashMap.put(TelemetryRadioConstants.HELP, new Help());
        commandsHashMap.put(TelemetryRotatorConstants.ROTATOR_STATUS, new RotatorStatus());

    }

    @Override
    protected Object encode(ChannelHandlerContext ctx, Channel channel, Object message) throws Exception {
        if (message == null) {
            return ChannelBuffers.EMPTY_BUFFER;
        }
        TelemetryCommand telemetryCommand = (TelemetryCommand) message;
        StringBuilder messageString = new StringBuilder();
        messageString.append(commandsHashMap.get(telemetryCommand.getName()).createMessageString(telemetryCommand));

        return messageString;
    }
}
