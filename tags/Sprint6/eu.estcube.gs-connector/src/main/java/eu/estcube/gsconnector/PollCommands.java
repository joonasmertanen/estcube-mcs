package eu.estcube.gsconnector;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.impl.DefaultMessage;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.domain.TelemetryRotatorConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;
import eu.estcube.gsconnector.commands.rotator.*;

@Component("pollCommands")
public final class PollCommands {

    private Map<String, String> commandsSetToGet;
    private Map<String, String> commandsGetToSet;

    private static final Logger LOG = LoggerFactory.getLogger(PollCommands.class);

    public PollCommands() {
        commandsSetToGet = new HashMap<String, String>();
        commandsGetToSet = new HashMap<String, String>();
        commandsSetToGet.put(TelemetryRotatorConstants.SET_POSITION, TelemetryRotatorConstants.GET_POSITION);
        commandsSetToGet.put(TelemetryRadioConstants.SET_ANTENNA_NR, TelemetryRadioConstants.GET_ANTENNA_NR);
        commandsSetToGet.put(TelemetryRadioConstants.SET_CTCSS_TONE, TelemetryRadioConstants.GET_CTCSS_TONE);
        commandsSetToGet.put(TelemetryRadioConstants.SET_DCS_CODE, TelemetryRadioConstants.GET_DCS_CODE);
        commandsSetToGet.put(TelemetryRadioConstants.SET_FREQUENCY, TelemetryRadioConstants.GET_FREQUENCY);
        commandsSetToGet
                .put(TelemetryRadioConstants.SET_MEMORY_CHANNELNR, TelemetryRadioConstants.GET_MEMORY_CHANNELNR);
        commandsSetToGet.put(TelemetryRadioConstants.SET_MODE, TelemetryRadioConstants.GET_MODE);
        commandsSetToGet.put(TelemetryRadioConstants.SET_PTT, TelemetryRadioConstants.GET_PTT);
        commandsSetToGet.put(TelemetryRadioConstants.SET_RIT, TelemetryRadioConstants.GET_RIT);
        commandsSetToGet.put(TelemetryRadioConstants.SET_RPTR_OFFSET, TelemetryRadioConstants.GET_RPTR_OFFSET);
        commandsSetToGet.put(TelemetryRadioConstants.SET_RPTR_SHIFT, TelemetryRadioConstants.GET_RPTR_SHIFT);
        commandsSetToGet.put(TelemetryRadioConstants.SET_SPLIT_VFO, TelemetryRadioConstants.GET_SPLIT_VFO);
        commandsSetToGet.put(TelemetryRadioConstants.SET_TRANCEIVE_MODE, TelemetryRadioConstants.GET_TRANCEIVE_MODE);
        commandsSetToGet.put(TelemetryRadioConstants.SET_TRANSMIT_FREQUENCY,
                TelemetryRadioConstants.GET_TRANSMIT_FREQUENCY);
        commandsSetToGet.put(TelemetryRadioConstants.SET_TRANSMIT_MODE, TelemetryRadioConstants.GET_TRANSMIT_MODE);
        commandsSetToGet.put(TelemetryRadioConstants.SET_TUNING_STEP, TelemetryRadioConstants.GET_TUNING_STEP);
        commandsSetToGet.put(TelemetryRadioConstants.SET_VFO, TelemetryRadioConstants.GET_VFO);

        commandsGetToSet.put(TelemetryRotatorConstants.GET_POSITION, TelemetryRotatorConstants.SET_POSITION);
        commandsSetToGet.put(TelemetryRadioConstants.GET_ANTENNA_NR, TelemetryRadioConstants.SET_ANTENNA_NR);
        commandsSetToGet.put(TelemetryRadioConstants.GET_CTCSS_TONE, TelemetryRadioConstants.SET_CTCSS_TONE);
        commandsSetToGet.put(TelemetryRadioConstants.GET_DCS_CODE, TelemetryRadioConstants.SET_DCS_CODE);
        commandsSetToGet.put(TelemetryRadioConstants.GET_FREQUENCY, TelemetryRadioConstants.SET_FREQUENCY);
        commandsSetToGet
                .put(TelemetryRadioConstants.GET_MEMORY_CHANNELNR, TelemetryRadioConstants.SET_MEMORY_CHANNELNR);
        commandsSetToGet.put(TelemetryRadioConstants.GET_MODE, TelemetryRadioConstants.SET_MODE);
        commandsSetToGet.put(TelemetryRadioConstants.GET_PTT, TelemetryRadioConstants.SET_PTT);
        commandsSetToGet.put(TelemetryRadioConstants.GET_RIT, TelemetryRadioConstants.SET_RIT);
        commandsSetToGet.put(TelemetryRadioConstants.GET_RPTR_OFFSET, TelemetryRadioConstants.SET_RPTR_OFFSET);
        commandsSetToGet.put(TelemetryRadioConstants.GET_RPTR_SHIFT, TelemetryRadioConstants.SET_RPTR_SHIFT);
        commandsSetToGet.put(TelemetryRadioConstants.GET_SPLIT_VFO, TelemetryRadioConstants.SET_SPLIT_VFO);
        commandsSetToGet.put(TelemetryRadioConstants.GET_TRANCEIVE_MODE, TelemetryRadioConstants.SET_TRANCEIVE_MODE);
        commandsSetToGet.put(TelemetryRadioConstants.GET_TRANSMIT_FREQUENCY,
                TelemetryRadioConstants.SET_TRANSMIT_FREQUENCY);
        commandsSetToGet.put(TelemetryRadioConstants.GET_TRANSMIT_MODE, TelemetryRadioConstants.SET_TRANSMIT_MODE);
        commandsSetToGet.put(TelemetryRadioConstants.GET_TUNING_STEP, TelemetryRadioConstants.SET_TUNING_STEP);
        commandsSetToGet.put(TelemetryRadioConstants.GET_VFO, TelemetryRadioConstants.SET_VFO);
    }

    public List<Message> checkPolling(Exchange exchange) {
        List<Message> messageList = new ArrayList<Message>();
        TelemetryObject incomingMessage = (TelemetryObject) exchange.getIn().getBody();
        createOriginalMessage(exchange, messageList);

        checkPollingMessageType(exchange, messageList, incomingMessage);
//        long t0, t1;
//        t0 = System.currentTimeMillis();
//        do {
//            t1 = System.currentTimeMillis();
//        } while (t1 - t0 < 1000);
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
             // TODO Auto-generated catch block
            LOG.debug("SLeep failed", e);
        }
       
        
        
        return messageList;
    }

    /*
     * Kas siin peaks try-catch spetsiifilisemaks tegema?
     */
    private void checkPollingMessageType(Exchange exchange, List<Message> messageList, TelemetryObject incomingMessage) {
        String checkPollingExists = exchange.getIn().getHeaders().toString();
        try {
            if (checkPollingExists.contains("polling")) {
                if (exchange.getIn().getHeader("polling").equals("setCommand")) {

                    if (incomingMessage.getParameter("RPRT").getValue().equals("0")) {
                        createPollingMessage(exchange, messageList, incomingMessage);
                    } else {
                        StoreSetValues.getRequiredParameterValueDataStore().remove(incomingMessage.getName());
                    }
                }
                if (exchange.getIn().getHeader("polling").equals("getCommand")) {
                    if (incomingMessage.getParameter("RPRT").getValue().equals("0")) {
                        checkStatusUpdate(exchange, messageList, incomingMessage);
                    } else {
                        StoreSetValues.getRequiredParameterValueDataStore().remove(
                                commandsGetToSet.get(incomingMessage.getName()));

                    }
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return;
        }
    }

    private void checkStatusUpdate(Exchange exchange, List<Message> messageList, TelemetryObject incomingMessage) {
        boolean statusUpdateParametersCorrect = true;
        String dataStoreKey;
        try {
            dataStoreKey = commandsGetToSet.get(incomingMessage.getName());
        } catch (NullPointerException e) {
            return;
        }
        for (int i = 1; i < incomingMessage.getParams().size() - 1; i++) {
            try {
                BigDecimal currentParameterValue = NumberUtils.createBigDecimal(incomingMessage.getParams().get(i)
                        .getValue().toString());
                BigDecimal requiredParameterValue = NumberUtils.createBigDecimal(StoreSetValues
                        .getRequiredParameterValueDataStore().get(dataStoreKey)
                        .getParameter(incomingMessage.getParams().get(i).getName()).toString());
                if ((currentParameterValue.compareTo(requiredParameterValue)) != 0) {
                    createPollingMessage(exchange, messageList, incomingMessage);
                    statusUpdateParametersCorrect = false;
                    break;
                }
            } catch (NumberFormatException e) {
                String currentParameterValue = incomingMessage.getParams().get(i).getValue().toString();
                String requiredParameterValue = StoreSetValues.getRequiredParameterValueDataStore().get(dataStoreKey)
                        .getParameter(incomingMessage.getParams().get(i).getName()).toString();
                if (!currentParameterValue.equals(requiredParameterValue)) {
                    createPollingMessage(exchange, messageList, incomingMessage);
                    statusUpdateParametersCorrect = false;
                    break;
                }
            }
        }
        if (statusUpdateParametersCorrect == true) {
            StoreSetValues.getRequiredParameterValueDataStore().remove(dataStoreKey);
        }
    }

    private void createOriginalMessage(Exchange exchange, List<Message> messageList) {
        DefaultMessage originalMessage = new DefaultMessage();
        originalMessage.setBody(exchange.getIn().getBody());
        originalMessage.setHeader("device", exchange.getIn().getHeader("device"));
        originalMessage.setHeader("groundStationID", exchange.getIn().getHeader("groundStationID"));
        originalMessage.setHeader("forward", "direct:gsSend");
        messageList.add(originalMessage);
    }

    private void createPollingMessage(Exchange exchange, List<Message> messageList, TelemetryObject incomingMessage) {
        DefaultMessage pollingMessage = new DefaultMessage();

        if (exchange.getIn().getHeader("polling").equals("getCommand")) {
            pollingMessage.setBody(new TelemetryCommand(incomingMessage.getName()));
        } else {
            pollingMessage.setBody(new TelemetryCommand(commandsSetToGet.get(incomingMessage.getName())));
        }
        pollingMessage.setHeader("device", exchange.getIn().getHeader("device"));
        pollingMessage.setHeader("groundStationID", exchange.getIn().getHeader("groundStationID"));
        pollingMessage.setHeader("polling", "getCommand");
        pollingMessage.setHeader("forward", "direct:chooseDevice");
        messageList.add(pollingMessage);
    }
}
