package eu.estcube.gsconnector;

import java.util.ArrayList;
import java.util.List;
import org.apache.camel.Body;
import org.apache.camel.Message;
import org.apache.camel.impl.DefaultMessage;
import org.springframework.stereotype.Component;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.domain.TelemetryRotatorConstants;

@Component
public class RotatorStatusSplitter {

    private List<String> commands;

    public RotatorStatusSplitter() {
        this.commands = new ArrayList<String>();
        commands.add(TelemetryRotatorConstants.GET_POSITION);
    }

    public List<Message> splitMessage() {
        List<Message> answer = new ArrayList<Message>();

        for (int i = 0; i < commands.size(); i++) {
            DefaultMessage message = new DefaultMessage();
            message.setBody(new TelemetryCommand(commands.get(i)));
            answer.add(message);
        }
        return answer;
    }
}