package eu.estcube.gsconnector;



import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jms.JmsMessageType;
import org.apache.camel.spring.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import eu.estcube.domain.JMSConstants;
import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.domain.TelemetryRotatorConstants;

public class Connector extends RouteBuilder{

    private static final Logger LOG = LoggerFactory.getLogger(Connector.class);

//    @Autowired
//    Processor getStatusRotator;
    
    @Autowired
    private RadioStatusSplitter radioStatusSplitter;

    @Autowired
    private RadioStatusSplitter rotatorStatusSplitter;
    
    @Autowired
    private StoreSetValues storeSetValues;
    
    @Autowired
    private PollCommands pollCommands;
    
    @Value("${rotctldAddress}")
    private String rotAddress;
    
    @Value("${rigctldAddress}")
    private String rigAddress;
   
    @Value("${gsName}")
    private String gsName;

    
    @Value("${timerFireInterval}")
    private String timerFireInterval;
    
    @Override
    public void configure() throws Exception {
        LOG.info("Starting Connector");
        String netty_rigctld= "netty:tcp://"
                +rigAddress
                    +"?sync=true&decoders=#newLineDecoder,#stringDecoder,#hamlibDecoder,#radioDecoder&encoders=#my-encoder,#radioEncoder";
        
        String netty_rotctld= "netty:tcp://"
                +rotAddress
                    +"?sync=true&decoders=#newLineDecoder,#stringDecoder,#hamlibDecoder,#rotatorDecoder&encoders=#my-encoder,#rotatorEncoder";
        
        
        /*
         * **==========================>>REAL ROUTES<<============================**
         */

        /*
         * Universal tunnels
         */
        //Incoming messages
        from(JMSConstants.GS_RECEIVE)
            .log("log:DebugRecievedMessage")
            .choice()
                .when(header("groundStationID").isEqualTo(gsName)) 
                    .to("direct:chooseDevice")
                .when(header("groundStationID").isEqualTo("*")) 
                    .to("direct:chooseDevice")
                .otherwise()
                    .log("log:WrongStation");
        
        from("direct:chooseDevice")
            .beanRef("storeSetValues")
            .log("log:ChoosingDevice")
            .choice()
                .when(header("device").isEqualTo("rigctld"))
                    .to("direct:rigctld", "direct:pollCommand")
                .when(header("device").isEqualTo("rotctld"))
                    .to("direct:rotctld", "direct:pollCommand")      
                .when(header("device").isEqualTo("status"))
                    .to("direct:status")
                .otherwise()
                    .log("log:InvalidDevice");
                    
        //monitors status updates
        from("direct:pollCommand")
            .split().method("pollCommands", "checkPolling")
            .recipientList(header("forward"));
        
        //Outgoing messages
        from("direct:gsSend")
            .to("log:DEBUGgsSEND")
            .to(JMSConstants.GS_SEND);
        
        //status
        from("direct:status")
            .multicast()
            .parallelProcessing()
            .to("direct:rigStatus")
            .to("direct:rotStatus"); 
       
        /*
         * rigctld
         */
        //Direction to the device
        from("direct:rigctld")
            .log("log:DebugRigDeviceBeforeResponse")
            .to(netty_rigctld)
            .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    exchange.getIn().setHeader("groundStationID", gsName);
                    exchange.getIn().setHeader("device", "rigctld");
                }
             });
        
        //Periodical time fire rigctld
        from("timer://rigTimer?period="+timerFireInterval)
            .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRotatorConstants.CAPABILITIES));
                }
             })
            .to("log:DebugRigPeriodicalTimeFire")
            .to("direct:rigctld", "direct:gsSend");
        
        
        //STATUS ROUTES
        from("direct:rigStatus")
            .split().method("radioStatusSplitter", "splitMessage")
            .to("direct:rigctld", "direct:gsSend");
        

        
        /*
         * rotctld
         */
        //Direction to the device
        from("direct:rotctld")
            .log("log:DebugRotDeviceBeforeResponse")
            .to(netty_rotctld)
            .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    exchange.getIn().setHeader("groundStationID", gsName);
                    exchange.getIn().setHeader("device", "rotctld");
                }
             });

        
//        //Periodical time fire rotctld
        from("timer://rotTimer?period="+timerFireInterval)
            .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRotatorConstants.CAPABILITIES));
                }
             })
            .to("log:DebugRotPeriodicalTimeFire")
            .to("direct:rotctld", "direct:gsSend");
        
        //STATUS ROUTES
        from("direct:rotStatus")
            .split().method("rotatorStatusSplitter", "splitMessage")
            .to("direct:rotctld", "direct:gsSend");
//                
//        //Hamlib info
//        //sudo rpc.rotd -m 1 -vvvv
//        //sudo rotctld -m 1 -vvvvv  //4533
//        //sudo rpc.rigd -m 1 -vvvv
//        //sudo rigctld -m 1901 -vvvvv  //4532
//        rigctld -m 1901
//        rpc.rigd -m 1
        
//        //101
//        //rigctld -m 1901 -vvvvv  //4532
//        //rotctl --help
//        //nr 2 opn net TCP
//        //rotctl -m 2 -r localhost:4533
//        // ENDLINE CODES:  \n ; RPTR x\n

        /*
         * **==========================>>TESTROUTES<<============================**
         * FROM: http://docs.jboss.org/netty/3.2/guide/html_single/index.html
         * Additionally, Netty provides out-of-the-box decoders which enables you to implement most protocols very easily and helps you 
         * avoid from ending up with a monolithic unmaintainable handler implementation. Please refer to the following packages for 
         * more detailed examples: 
         *   org.jboss.netty.example.factorial for a binary protocol, and 
         *   org.jboss.netty.example.telnet for a text line-based protocol.
         *   Camel + Netty demo routes
         */
        
        from("stream:in")
                    .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    exchange.getIn().setHeader("device", "rotctld");
                    TelemetryCommand telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.SET_POSITION);
                    telemetryCommand.addParameter("Azimuth", 89);
                    telemetryCommand.addParameter("Elevation", 89);
                    exchange.getIn().setBody(telemetryCommand);
                }
             })
            .to("direct:chooseDevice");

//            .beanRef("getStatusRotator")
//            .to("netty:tcp://localhost:4533?sync=true&decoders=#newLineDecoder,#stringDecoder,#hamlibDecoder&encoders=#my-encoder")
//            .to("log:echo");
//            .to("netty:tcp://localhost:10111?sync=true")
//            .setBody().simple("p")

//            .to(netty_rotctld)
//            .to("log:result");
//
//        from("netty:tcp://localhost:10111")
//            .process(new Processor() {
//                public void process(Exchange exchange) throws Exception {
//                    LOG.debug("IN:" + exchange.getIn().getBody());
//                    exchange.getOut().setBody("response: " + exchange.getIn().getBody());
//                    LOG.debug("IN:" + exchange.getIn().getBody());
//                }
//            });
        

    }
    public static void main(String[] args) throws Exception {
        LOG.info("Starting Connector");
        LOG.info("Starting Camel");
        new Main().run(args);

    }




}
