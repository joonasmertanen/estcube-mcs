package eu.estcube.webserver;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.net.util.Base64;
import org.springframework.stereotype.Component;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thoughtworks.xstream.core.util.Base64Encoder;


@Component
public class InputStreamToBase64 implements Processor{

    public void process(Exchange ex) throws Exception {
        InputStream is = ex.getIn().getBody(InputStream.class);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        int next = is.read();
        while (next > -1) {
          bos.write(next);
          next = is.read();
        }
        bos.flush();        
        byte[] result = bos.toByteArray();

        String base64image = Base64.encodeBase64String(result);
        ex.getOut().setBody(base64image, String.class);
    }
}
