package eu.estcube.domain;

public class JMSConstants {
        public static final String GS_RECEIVE = "activemq:topic:gsReceive";
        public static final String GS_SEND = "activemq:topic:gsSend";
        public static final String PS_QUEUE = "activemq:queue:psQueue";
        public static final String GS_ID_HEADER = "groundStationID";
        public static final String DEVICE_HEADER = "device";
        public static final String WEBCAM_SEND ="activemq:topic:webcamSend";
        
    public JMSConstants() {

    }
}
