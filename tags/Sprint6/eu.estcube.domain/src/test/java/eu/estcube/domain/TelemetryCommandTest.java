/**
 * 
 */
package eu.estcube.domain;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;


public class TelemetryCommandTest {

    private static final String NAME = "A";
    private static final Map<String, Object> PARAMS = new HashMap<String, Object>();
    private String name = "name";
    private Object value = "value";

    private TelemetryCommand tc, tc2;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        tc = new TelemetryCommand(NAME);
        tc2 = new TelemetryCommand(NAME, PARAMS);
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryCommand#TelemetryCommand(java.lang.String)}.
     */
    @Test
    public void testTelemetryCommandString() {
        assertNotNull(tc);
        assertNotNull(tc.getParams());
        assertEquals(0, tc.getParams().size());
        assertEquals(NAME, tc.getName());
        assertNotNull(tc.toString());
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryCommand#TelemetryCommand(java.lang.String, java.util.Map)}.
     */
    @Test
    public void testTelemetryCommandStringMapOfStringObject() {
        assertNotNull(tc2);
        assertNotNull(tc2.getParams());
        assertEquals(0, tc2.getParams().size());
        assertEquals(NAME, tc2.getName());
        assertNotNull(tc2.toString());
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryCommand#getName()}.
     */
    @Test
    public void testGetName() {
        assertEquals(NAME, tc.getName());
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryCommand#getParams()}.
     */
    @Test
    public void testGetParams() {
        assertEquals(PARAMS, tc.getParams());
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryCommand#addParameter(java.lang.String, java.lang.Object)}.
     */
    @Test
    public void testAddParameter() {
        tc.addParameter("a", "b");
        assertEquals("b", tc.getParameter("a"));
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryCommand#getParameter(java.lang.String)}.
     */
    @Test
    public void testGetParameter() {
        tc.addParameter(name, value);
        assertEquals(value, tc.getParameter(name));
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryCommand#getParameter()}.
     */
    @Test
    public void getParameters() {
        tc.addParameter(name, value);
        tc.addParameter("a", "b");
        Map<String, Object> map = tc.getParams();
        assertEquals(2, map.size());
        assertEquals(value, map.get(name));
        assertEquals("b", map.get("a"));        
    }
}
