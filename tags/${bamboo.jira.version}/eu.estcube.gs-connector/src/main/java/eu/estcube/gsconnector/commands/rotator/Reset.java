package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.domain.TelemetryRotatorConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class Reset extends CommandStringBuilder {

    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString = new StringBuilder();

        messageString.append("+R " + telemetryCommand.getParams().get("Reset") + "\n");

        return messageString;
    }
}
