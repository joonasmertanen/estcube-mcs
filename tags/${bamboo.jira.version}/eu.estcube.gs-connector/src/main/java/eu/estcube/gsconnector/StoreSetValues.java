package eu.estcube.gsconnector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.estcube.domain.JMSConstants;
import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.domain.TelemetryRotatorConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;
import eu.estcube.gsconnector.commands.radio.*;
import eu.estcube.gsconnector.commands.rotator.SetPosition;

@Component("storeSetValues")
public final class StoreSetValues {
    private static Map<String, TelemetryCommand> requiredParameterValueDataStore;

    public static Map<String, TelemetryCommand> getRequiredParameterValueDataStore() {
        return requiredParameterValueDataStore;
    }

    public static void setRequiredParameterValueDataStore(Map<String, TelemetryCommand> requiredParameterValueDataStore) {
        StoreSetValues.requiredParameterValueDataStore = requiredParameterValueDataStore;
    }

    private static final Logger LOG = LoggerFactory.getLogger(StoreSetValues.class);
    private Map<String, CommandStringBuilder> commandsHashMap;

    public StoreSetValues() {
        requiredParameterValueDataStore = new HashMap<String, TelemetryCommand>();
        commandsHashMap = new HashMap<String, CommandStringBuilder>();
        commandsHashMap.put(TelemetryRotatorConstants.SET_POSITION, new SetPosition());
        commandsHashMap.put(TelemetryRadioConstants.SET_ANTENNA_NR, new SetAntennaNr());
        commandsHashMap.put(TelemetryRadioConstants.SET_CTCSS_TONE, new SetCTCSSTone());
        commandsHashMap.put(TelemetryRadioConstants.SET_DCS_CODE, new SetDCSCode());
        commandsHashMap.put(TelemetryRadioConstants.SET_FREQUENCY, new SetFrequency());
        commandsHashMap.put(TelemetryRadioConstants.SET_MEMORY_CHANNELNR, new SetMemoryChannelNr());
        commandsHashMap.put(TelemetryRadioConstants.SET_MODE, new SetMode());
        commandsHashMap.put(TelemetryRadioConstants.SET_PTT, new SetPTT());
        commandsHashMap.put(TelemetryRadioConstants.SET_RIT, new SetRIT());
        commandsHashMap.put(TelemetryRadioConstants.SET_RPTR_OFFSET, new SetRptrOffset());
        commandsHashMap.put(TelemetryRadioConstants.SET_RPTR_SHIFT, new SetRprtShift());
        commandsHashMap.put(TelemetryRadioConstants.SET_SPLIT_VFO, new SetSplitVFO());
        commandsHashMap.put(TelemetryRadioConstants.SET_TRANCEIVE_MODE, new SetTranceiveMode());
        commandsHashMap.put(TelemetryRadioConstants.SET_TRANSMIT_FREQUENCY, new SetTransmitFrequency());
        commandsHashMap.put(TelemetryRadioConstants.SET_TRANSMIT_MODE, new SetTransmitMode());
        commandsHashMap.put(TelemetryRadioConstants.SET_TUNING_STEP, new SetTuningStep());
        commandsHashMap.put(TelemetryRadioConstants.SET_VFO, new SetVFO());
    }

    public void storeData(Exchange exchange) {
        TelemetryCommand command = null;
        try {
            command = (TelemetryCommand) exchange.getIn().getBody();
        } catch (ClassCastException e) {
            exchange.getIn().setHeaders(null);
            exchange.getIn().setBody(null);
            return;
        }
        if (commandsHashMap.get(command.getName()) != null) {
            requiredParameterValueDataStore.put(command.getName(), command);
            exchange.getIn().setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_SET_COMMAND);
        }

        if (exchange.getIn().getHeader(JMSConstants.HEADER_POLLING) == null) {
            exchange.getIn().setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_NOT_REQUIRED);
        }
    }
}