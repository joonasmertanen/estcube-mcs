package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class ConvertPower2MW extends CommandStringBuilder {

    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString = new StringBuilder();
        messageString
                .append("+2 " + telemetryCommand.getParams().get("Power") + " "
                        + telemetryCommand.getParams().get("Frequency") + " "
                        + telemetryCommand.getParams().get("Mode") + "\n");
        return messageString;
    }
}
