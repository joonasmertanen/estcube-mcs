package eu.estcube.gsconnector.commands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class SendRawCmd extends CommandStringBuilder {

    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString = new StringBuilder();
        messageString.append("+w " + telemetryCommand.getParams().get("Cmd") + "\n");
        return messageString;
    }
}
