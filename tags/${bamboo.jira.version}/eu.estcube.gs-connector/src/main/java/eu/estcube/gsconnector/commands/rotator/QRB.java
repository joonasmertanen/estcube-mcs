package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class QRB extends CommandStringBuilder {

    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString = new StringBuilder();

        messageString.append("+B " + telemetryCommand.getParams().get("Lon 1") + " "
                + telemetryCommand.getParams().get("Lat 1") + " " + telemetryCommand.getParams().get("Lon 2") + " "
                + telemetryCommand.getParams().get("Lat 2") + "\n");

        return messageString;
    }
}
