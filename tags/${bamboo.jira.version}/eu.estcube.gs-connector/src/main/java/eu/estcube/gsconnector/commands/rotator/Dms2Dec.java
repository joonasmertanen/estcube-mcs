package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class Dms2Dec extends CommandStringBuilder {

    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString = new StringBuilder();

        messageString.append("+D " + telemetryCommand.getParams().get("Degrees") + " "
                + telemetryCommand.getParams().get("Minutes") + " " + telemetryCommand.getParams().get("Seconds") + " "
                + telemetryCommand.getParams().get("S/W") + "\n");

        return messageString;
    }
}
