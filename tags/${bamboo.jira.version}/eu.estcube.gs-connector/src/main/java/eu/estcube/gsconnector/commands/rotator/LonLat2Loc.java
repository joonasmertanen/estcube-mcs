package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class LonLat2Loc extends CommandStringBuilder {

    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString = new StringBuilder();

        messageString.append("+L " + telemetryCommand.getParams().get("Longitude") + " "
                + telemetryCommand.getParams().get("Latitude") + " " + telemetryCommand.getParams().get("Loc Len")
                + "\n");

        return messageString;
    }
}
