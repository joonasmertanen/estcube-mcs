package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class ConvertMW2Power extends CommandStringBuilder {

    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString = new StringBuilder();
        messageString
                .append("+4 " + telemetryCommand.getParams().get("Power mW") + " "
                        + telemetryCommand.getParams().get("Frequency") + " "
                        + telemetryCommand.getParams().get("Mode") + "\n");
        return messageString;
    }
}
