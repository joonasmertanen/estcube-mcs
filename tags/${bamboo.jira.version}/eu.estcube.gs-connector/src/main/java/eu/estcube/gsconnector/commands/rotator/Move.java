package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class Move extends CommandStringBuilder {
    private StringBuilder messageString;

    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString = new StringBuilder();

        messageString.append("+M " + telemetryCommand.getParams().get("Direction") + " "
                + telemetryCommand.getParams().get("Speed") + "\n");

        return messageString;
    }
}
