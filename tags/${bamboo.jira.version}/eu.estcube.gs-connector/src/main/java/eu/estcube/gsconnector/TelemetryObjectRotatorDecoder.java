package eu.estcube.gsconnector;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.estcube.domain.JMSConstants;
import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryParameter;
import eu.estcube.domain.TelemetryRotatorConstants;

@Component("rotatorDecoder")
public class TelemetryObjectRotatorDecoder extends OneToOneDecoder {

    @Value("${gsName}")
    private String gsName;

    private static final Logger LOG = LoggerFactory.getLogger(TelemetryObjectRotatorDecoder.class);

    private Map<String, String> hamlibToProtocolMap;

    public TelemetryObjectRotatorDecoder() {
        hamlibToProtocolMap = new HashMap<String, String>();
        hamlibToProtocolMap.put("a_sp2a_lp", TelemetryRotatorConstants.AZIMUTH_SHORTPATH2LONGPATH);
        hamlibToProtocolMap.put("set_pos", TelemetryRotatorConstants.SET_POSITION);
        hamlibToProtocolMap.put("get_pos", TelemetryRotatorConstants.GET_POSITION);
        hamlibToProtocolMap.put("stop", TelemetryRotatorConstants.STOP);
        hamlibToProtocolMap.put("park", TelemetryRotatorConstants.PARK);
        hamlibToProtocolMap.put("move", TelemetryRotatorConstants.MOVE);
        hamlibToProtocolMap.put("dump_caps", TelemetryRotatorConstants.CAPABILITIES);
        hamlibToProtocolMap.put("reset", TelemetryRotatorConstants.RESET);
        hamlibToProtocolMap.put("set_conf", TelemetryRotatorConstants.SET_CONFIG);
        hamlibToProtocolMap.put("get_info", TelemetryRotatorConstants.GET_INFO);
        hamlibToProtocolMap.put("send_cmd", TelemetryRotatorConstants.SEND_RAW_CMD);
        hamlibToProtocolMap.put("Lonlat2loc", TelemetryRotatorConstants.LONLAT2LOC);
        hamlibToProtocolMap.put("Loc2lonlat", TelemetryRotatorConstants.LOC2LONLAT);
        hamlibToProtocolMap.put("d_sp2d_lp", TelemetryRotatorConstants.DISTANCE_SHORTPATH2LONGPATH);
        hamlibToProtocolMap.put("Dms2dec", TelemetryRotatorConstants.DMS2DEC);
        hamlibToProtocolMap.put("Dec2dms", TelemetryRotatorConstants.DEC2DMS);
        hamlibToProtocolMap.put("Dmmm2dec", TelemetryRotatorConstants.DMMM2DEC);
        hamlibToProtocolMap.put("dec2dmmm", TelemetryRotatorConstants.DEC2DMMM);
        hamlibToProtocolMap.put("Qrv", TelemetryRotatorConstants.QRB);
    }

    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, Object message) throws Exception {

        String[] messageSplit = message.toString().split("\n");
        TelemetryObject telemetryObject;
        LOG.debug("RotatorMessage: {}", message.toString());

        if (messageSplit[0].equals("Commands (some may not be available for this rig):")) {
            RotatorHelpCommand rotatorHelp = new RotatorHelpCommand();
            telemetryObject = new TelemetryObject("Help", new Date());
            telemetryObject = rotatorHelp.createHelpList(telemetryObject, messageSplit);
        } else {
            String[] name = messageSplit[0].split(":");

            telemetryObject = new TelemetryObject(hamlibToProtocol(name[0]), new Date());
            addParametersToTelemetryObject(messageSplit, telemetryObject);
        }

        telemetryObject.setSource(gsName);
        telemetryObject.setDevice(JMSConstants.GS_ROT_CTLD);

        telemetryObject.setTime(new Date());
//        LOG.debug("{}", telemetryObject.getTime());
        return telemetryObject;
    }

    private void addParametersToTelemetryObject(String[] messageSplit, TelemetryObject telemetryObject) {
        String[] messagePiece = { null, null };

        for (int i = 0; i < messageSplit.length; i++) {

            if (messageSplit[i].contains(":\t\t")) {
                messagePiece = messageSplit[i].split(":\t\t");
            } else if (messageSplit[i].contains(":\t")) {
                messagePiece = messageSplit[i].split(":\t");
            } else if (messageSplit[i].contains(": ")) { // /////////ohukohad
                messagePiece = messageSplit[i].split(": ");

            } else if (messageSplit[i].contains(":")) { // /////////ohukohad
                messagePiece = messageSplit[i].split(":");
            } else if (messageSplit[i].contains(" ")) {
                messagePiece = messageSplit[i].split(" ");
            } else {
                continue;
            }
            messagePiece[0] = hamlibToProtocol(messagePiece[0]);
            if (messagePiece.length == 1) {
                telemetryObject.addParameter(new TelemetryParameter(messagePiece[0], ""));
                continue;
            }
            telemetryObject.addParameter(new TelemetryParameter(messagePiece[0], messagePiece[1]));
        }
    }

    protected String hamlibToProtocol(String hamlibName) {
        String protocolName = hamlibToProtocolMap.get(hamlibName);
        if (protocolName != null) {
            return protocolName;
        }
        return hamlibName;
    }

}
