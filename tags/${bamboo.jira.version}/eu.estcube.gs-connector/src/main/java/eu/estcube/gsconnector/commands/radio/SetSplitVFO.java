package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SetSplitVFO extends CommandStringBuilder {
 
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString = new StringBuilder();
        messageString.append("+S " + telemetryCommand.getParams().get("Split") + " "
                + telemetryCommand.getParams().get("TX VFO") + "\n");
        return messageString;
    }
}
