package eu.estcube.gsconnector.commands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class GetInfo extends CommandStringBuilder {

    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString = new StringBuilder();
        messageString.append("+_\n");
        return messageString;
    }
}
