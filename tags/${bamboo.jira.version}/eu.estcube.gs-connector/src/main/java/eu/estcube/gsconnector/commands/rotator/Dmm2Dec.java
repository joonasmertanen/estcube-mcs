package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class Dmm2Dec extends CommandStringBuilder {

    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString = new StringBuilder();

        messageString.append("+E " + telemetryCommand.getParams().get("Degrees") + " "
                + telemetryCommand.getParams().get("Dec Minutes") + " " + telemetryCommand.getParams().get("S/W")
                + "\n");

        return messageString;
    }
}
