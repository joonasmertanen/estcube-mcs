package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class SetTransmitFrequencyTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SetTransmitFrequency createMessage = new SetTransmitFrequency();
    private static final Logger LOG = LoggerFactory.getLogger(SetTransmitFrequencyTest.class);

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("SET_TRANSMIT_FREQUENCY");
        telemetryCommand.addParameter("TX Frequency", 0);
        string.append("+l 0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
