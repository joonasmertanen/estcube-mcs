package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class ScanTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private Scan createMessage = new Scan();
    private static final Logger LOG = LoggerFactory.getLogger(ScanTest.class);

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("SCAN");
        telemetryCommand.addParameter("Scan Fct", "STOP");
        telemetryCommand.addParameter("Scan Channel", "DELTA");
        string.append("+g STOP DELTA\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
