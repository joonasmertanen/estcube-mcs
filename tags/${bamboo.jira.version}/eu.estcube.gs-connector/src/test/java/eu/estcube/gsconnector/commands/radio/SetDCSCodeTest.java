package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class SetDCSCodeTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SetDCSCode createMessage = new SetDCSCode();
    private static final Logger LOG = LoggerFactory.getLogger(SetDCSCodeTest.class);
 
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("SET_DCS_CODE");
        telemetryCommand.addParameter("DCS Code", 6);
        string.append("+D 6\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
