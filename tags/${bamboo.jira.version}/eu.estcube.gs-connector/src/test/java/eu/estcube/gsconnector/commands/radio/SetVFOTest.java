package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class SetVFOTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SetVFO createMessage = new SetVFO();
    private static final Logger LOG = LoggerFactory.getLogger(SetVFOTest.class);
 
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("SET_VFO");
        telemetryCommand.addParameter("VFO", "VFO");
        string.append("+V VFO\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
