package eu.estcube.gsconnector.commands.rotator;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class QRBTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private QRB createMessage = new QRB();
    private static final Logger LOG = LoggerFactory.getLogger(QRBTest.class);

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("QRB");
        telemetryCommand.addParameter("Lon 1", 0.0);
        telemetryCommand.addParameter("Lat 1", 0.0);
        telemetryCommand.addParameter("Lon 2", 0.0);
        telemetryCommand.addParameter("Lat 2", 0.0);
        string.append("+B 0.0 0.0 0.0 0.0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
