package eu.estcube.gsconnector.commands.rotator;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class DistanceShortPath2LongPathTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private DistanceShortPath2LongPath createMessage = new DistanceShortPath2LongPath();
    private static final Logger LOG = LoggerFactory.getLogger(DistanceShortPath2LongPathTest.class);

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("DISTANCE_SHORTPATH2LONGPATH");
        telemetryCommand.addParameter("Short Path km", 0.0);
        string.append("+a 0.0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
        
        string = new StringBuilder();
        telemetryCommand = new TelemetryCommand("DISTANCE_SHORTPATH2LONGPATH");
        string.append("+a null\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
        
    }

}
