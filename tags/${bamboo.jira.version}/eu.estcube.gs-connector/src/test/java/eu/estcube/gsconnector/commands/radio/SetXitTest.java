package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class SetXitTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SetXit createMessage = new SetXit();
    private static final Logger LOG = LoggerFactory.getLogger(SetXitTest.class);

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("SET_XIT");
        telemetryCommand.addParameter("XIT", 0);
        string.append("+Z 0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
