package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class GetFrequencyTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private GetFrequency createMessage = new GetFrequency();
    private static final Logger LOG = LoggerFactory.getLogger(GetFrequencyTest.class);
    
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("GET_FREQUENCY");
        string.append("+f\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
