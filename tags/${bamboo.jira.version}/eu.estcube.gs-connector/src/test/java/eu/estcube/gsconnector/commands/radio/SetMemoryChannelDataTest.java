package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class SetMemoryChannelDataTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SetMemoryChannelData createMessage = new SetMemoryChannelData();
    private static final Logger LOG = LoggerFactory.getLogger(SetMemoryChannelDataTest.class);

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("SET_MEMORY_CHANNELDATA");
        telemetryCommand.addParameter("Channel", "..");
        string.append("+H ..\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
