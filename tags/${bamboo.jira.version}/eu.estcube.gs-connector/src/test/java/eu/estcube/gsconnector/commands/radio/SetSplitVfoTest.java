package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class SetSplitVfoTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SetSplitVFO createMessage = new SetSplitVFO();
    private static final Logger LOG = LoggerFactory.getLogger(SetSplitVfoTest.class);

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("SET_SPLIT_VFO");
        telemetryCommand.addParameter("Split", 0);
        telemetryCommand.addParameter("TX VFO", "VFO");
        string.append("+S 0 VFO\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
