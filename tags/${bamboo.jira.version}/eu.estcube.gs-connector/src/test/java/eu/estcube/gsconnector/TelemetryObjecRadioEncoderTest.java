package eu.estcube.gsconnector;


import static org.junit.Assert.assertEquals;

import org.jboss.netty.buffer.ChannelBuffers;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.domain.TelemetryRotatorConstants;

public class TelemetryObjecRadioEncoderTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private TelemetryObjecRadioEncoder encode = new TelemetryObjecRadioEncoder();
    

    private static final Logger LOG = LoggerFactory.getLogger(TelemetryObjecRadioEncoderTest.class);

    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        
    }

    @Test
    public void testEncode() throws Exception
    {
        telemetryCommand = null;
        Object test = encode.encode(null, null, telemetryCommand);
        assertEquals(ChannelBuffers.EMPTY_BUFFER, test);

        telemetryCommand = new TelemetryCommand(TelemetryRadioConstants.GET_MODE);
        string.append("+m\n");
        assertEquals(string.toString(), encode.encode(null, null, telemetryCommand).toString());
    }

}
