package eu.estcube.gsconnector;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.impl.DefaultExchange;
import org.apache.camel.impl.DefaultMessage;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.JMSConstants;
import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryParameter;
import eu.estcube.domain.TelemetryRotatorConstants;

public class PollCommandsTest {

    private static final Logger LOG = LoggerFactory.getLogger(PollCommandsTest.class);
    private PollCommands pollCommands;
    private DefaultExchange exchange;
    private CamelContext ctx;
    StoreSetValues storeSetValues;

    @Before
    public void setUp() throws Exception {
        pollCommands = new PollCommands();
        ctx = Mockito.mock(CamelContext.class);
        exchange = new DefaultExchange(ctx);
        storeSetValues = new StoreSetValues();
    }

    @Test
    public void testCheckPolling() {

        // if RPRT is wrong with set_command
        exchange.getIn().setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_SET_COMMAND);
        TelemetryObject telemetryObject = new TelemetryObject(TelemetryRotatorConstants.SET_POSITION, new Date());
        telemetryObject.addParameter(new TelemetryParameter("RPRT", "1"));
        exchange.getIn().setBody(telemetryObject);
        TelemetryCommand telemetryCommandForDataStore = new TelemetryCommand(TelemetryRotatorConstants.SET_POSITION);
        telemetryCommandForDataStore.addParameter("Azimuth", "10");
        telemetryCommandForDataStore.addParameter("Elevation", "10");
        StoreSetValues.getRequiredParameterValueDataStore().put(telemetryCommandForDataStore.getName(),
                telemetryCommandForDataStore);
        List<Message> messageList = pollCommands.checkPolling(exchange);
        assertTrue(messageList.size() == 1);
        assertEquals(exchange.getIn().toString(), messageList.get(0).toString());
        assertTrue(StoreSetValues.getRequiredParameterValueDataStore().size() == 0);

        // if RPRT is wrong with get_command
        exchange.getIn().setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_GET_COMMAND);
        telemetryObject = new TelemetryObject(TelemetryRotatorConstants.GET_POSITION, new Date());
        telemetryObject.addParameter(new TelemetryParameter("RPRT", "1"));
        exchange.getIn().setBody(telemetryObject);
        telemetryCommandForDataStore = new TelemetryCommand(TelemetryRotatorConstants.SET_POSITION);
        telemetryCommandForDataStore.addParameter("Azimuth", "10");
        telemetryCommandForDataStore.addParameter("Elevation", "10");
        StoreSetValues.getRequiredParameterValueDataStore().put(telemetryCommandForDataStore.getName(),
                telemetryCommandForDataStore);
        messageList = pollCommands.checkPolling(exchange);
        assertTrue(messageList.size() == 1);
        assertEquals(exchange.getIn().toString(), messageList.get(0).toString());
        assertTrue(StoreSetValues.getRequiredParameterValueDataStore().size() == 0);

        // if set command is received
        exchange.getIn().setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_SET_COMMAND);
        telemetryObject = new TelemetryObject(TelemetryRotatorConstants.SET_POSITION, new Date());
        telemetryObject.addParameter(new TelemetryParameter("Azimuth", "10"));
        telemetryObject.addParameter(new TelemetryParameter("Elevation", "10"));
        telemetryObject.addParameter(new TelemetryParameter("RPRT", "0"));
        exchange.getIn().setBody(telemetryObject);
        messageList = pollCommands.checkPolling(exchange);
        DefaultMessage pollingMessage = new DefaultMessage();
        TelemetryCommand telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.GET_POSITION);
        pollingMessage.setBody(telemetryCommand);
        pollingMessage.setHeader(JMSConstants.HEADER_DEVICE, exchange.getIn().getHeader(JMSConstants.HEADER_DEVICE));
        pollingMessage.setHeader(JMSConstants.HEADER_GROUNDSTATIONID, exchange.getIn().getHeader(JMSConstants.HEADER_GROUNDSTATIONID));
        pollingMessage.setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_GET_COMMAND);
        pollingMessage.setHeader(JMSConstants.HEADER_FORWARD, JMSConstants.DIRECT_CHOOSE_DEVICE);
        TelemetryCommand testTelemetryCommand = (TelemetryCommand) messageList.get(1).getBody();
        assertTrue(messageList.size() == 2);
        assertEquals(exchange.getIn().toString(), messageList.get(0).toString());
        assertEquals(telemetryCommand.getName(), testTelemetryCommand.getName());
        assertEquals(telemetryCommand.getParams(), testTelemetryCommand.getParams());
        assertEquals(pollingMessage.getHeaders(), messageList.get(1).getHeaders());

        // if StoreSetValues cache is empty
        telemetryObject = new TelemetryObject(TelemetryRotatorConstants.GET_POSITION, new Date());
        telemetryObject.addParameter(new TelemetryParameter("Azimuth", "10"));
        telemetryObject.addParameter(new TelemetryParameter("Elevation", "10"));
        telemetryObject.addParameter(new TelemetryParameter("RPRT", "0"));
        exchange.getIn().setBody(telemetryObject);
        exchange.getIn().setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_GET_COMMAND);
        messageList = pollCommands.checkPolling(exchange);
        pollingMessage = new DefaultMessage();
        telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.GET_POSITION);
        pollingMessage.setBody(telemetryCommand);
        pollingMessage.setHeader(JMSConstants.HEADER_DEVICE, exchange.getIn().getHeader(JMSConstants.HEADER_DEVICE));
        pollingMessage.setHeader(JMSConstants.HEADER_GROUNDSTATIONID, exchange.getIn().getHeader(JMSConstants.HEADER_GROUNDSTATIONID));
        pollingMessage.setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_GET_COMMAND);
        pollingMessage.setHeader(JMSConstants.HEADER_FORWARD, JMSConstants.DIRECT_CHOOSE_DEVICE);
        assertTrue(messageList.size() == 1);
        assertEquals(exchange.getIn().toString(), messageList.get(0).toString());
        assertEquals(telemetryCommand.getName(), testTelemetryCommand.getName());

        // If parameters decimals and true
        telemetryObject = new TelemetryObject(TelemetryRotatorConstants.GET_POSITION, new Date());
        exchange.getIn().setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_GET_COMMAND);
        telemetryObject.addParameter(new TelemetryParameter("Azimuth", "10"));
        telemetryObject.addParameter(new TelemetryParameter("Elevation", "10"));
        telemetryObject.addParameter(new TelemetryParameter("RPRT", "0"));
        telemetryCommandForDataStore = new TelemetryCommand(TelemetryRotatorConstants.SET_POSITION);
        telemetryCommandForDataStore.addParameter("Azimuth", "10");
        telemetryCommandForDataStore.addParameter("Elevation", "10");
        StoreSetValues.getRequiredParameterValueDataStore().put(telemetryCommandForDataStore.getName(),
                telemetryCommandForDataStore);
        exchange.getIn().setBody(telemetryObject);
        messageList = pollCommands.checkPolling(exchange);
        pollingMessage = new DefaultMessage();
        telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.GET_POSITION);
        pollingMessage.setBody(telemetryCommand);
        pollingMessage.setHeader(JMSConstants.HEADER_DEVICE, exchange.getIn().getHeader(JMSConstants.HEADER_DEVICE));
        pollingMessage.setHeader(JMSConstants.HEADER_GROUNDSTATIONID, exchange.getIn().getHeader(JMSConstants.HEADER_GROUNDSTATIONID));
        pollingMessage.setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_GET_COMMAND);
        pollingMessage.setHeader(JMSConstants.HEADER_FORWARD, JMSConstants.DIRECT_CHOOSE_DEVICE);
        assertTrue(messageList.size() == 1);
        assertEquals(exchange.getIn().toString(), messageList.get(0).toString());

        // if parameters are decimals and false
        telemetryObject = new TelemetryObject(TelemetryRotatorConstants.GET_POSITION, new Date());
        exchange.getIn().setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_GET_COMMAND);
        telemetryObject.addParameter(new TelemetryParameter("Azimuth", "20"));
        telemetryObject.addParameter(new TelemetryParameter("Elevation", "20"));
        telemetryObject.addParameter(new TelemetryParameter("RPRT", "0"));
        telemetryCommandForDataStore = new TelemetryCommand(TelemetryRotatorConstants.SET_POSITION);
        telemetryCommandForDataStore.addParameter("Azimuth", "10");
        telemetryCommandForDataStore.addParameter("Elevation", "10");
        StoreSetValues.getRequiredParameterValueDataStore().put(telemetryCommandForDataStore.getName(),
                telemetryCommandForDataStore);
        exchange.getIn().setBody(telemetryObject);
        messageList = pollCommands.checkPolling(exchange);
        pollingMessage = new DefaultMessage();
        telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.GET_POSITION);
        pollingMessage.setBody(telemetryCommand);
        pollingMessage.setHeader(JMSConstants.HEADER_DEVICE, exchange.getIn().getHeader(JMSConstants.HEADER_DEVICE));
        pollingMessage.setHeader(JMSConstants.HEADER_GROUNDSTATIONID, exchange.getIn().getHeader(JMSConstants.HEADER_GROUNDSTATIONID));
        pollingMessage.setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_GET_COMMAND);
        pollingMessage.setHeader(JMSConstants.HEADER_FORWARD, JMSConstants.DIRECT_CHOOSE_DEVICE);
        assertTrue(messageList.size() == 2);
        testTelemetryCommand = (TelemetryCommand) messageList.get(1).getBody();
        assertEquals(exchange.getIn().toString(), messageList.get(0).toString());
        assertEquals(telemetryCommand.getName(), testTelemetryCommand.getName());
        assertEquals(telemetryCommand.getParams(), testTelemetryCommand.getParams());
        assertEquals(pollingMessage.getHeaders(), messageList.get(1).getHeaders());

        // If parameters are Strings and false
        telemetryObject = new TelemetryObject(TelemetryRotatorConstants.GET_POSITION, new Date());
        exchange.getIn().setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_GET_COMMAND);
        telemetryObject.addParameter(new TelemetryParameter("Azimuth", "Hello"));
        telemetryObject.addParameter(new TelemetryParameter("Elevation", "VAK"));
        telemetryObject.addParameter(new TelemetryParameter("RPRT", "0"));
        telemetryCommandForDataStore = new TelemetryCommand(TelemetryRotatorConstants.SET_POSITION);
        telemetryCommandForDataStore.addParameter("Azimuth", "OMG");
        telemetryCommandForDataStore.addParameter("Elevation", "ZOMG");
        StoreSetValues.getRequiredParameterValueDataStore().put(telemetryCommandForDataStore.getName(),
                telemetryCommandForDataStore);
        exchange.getIn().setBody(telemetryObject);
        messageList = pollCommands.checkPolling(exchange);
        pollingMessage = new DefaultMessage();
        telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.GET_POSITION);
        pollingMessage.setBody(telemetryCommand);
        pollingMessage.setHeader(JMSConstants.HEADER_DEVICE, exchange.getIn().getHeader(JMSConstants.HEADER_DEVICE));
        pollingMessage.setHeader(JMSConstants.HEADER_GROUNDSTATIONID, exchange.getIn().getHeader(JMSConstants.HEADER_GROUNDSTATIONID));
        pollingMessage.setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_GET_COMMAND);
        pollingMessage.setHeader(JMSConstants.HEADER_FORWARD, JMSConstants.DIRECT_CHOOSE_DEVICE);
        assertTrue(messageList.size() == 2);
        testTelemetryCommand = (TelemetryCommand) messageList.get(1).getBody();
        assertEquals(exchange.getIn().toString(), messageList.get(0).toString());
        assertEquals(telemetryCommand.getName(), testTelemetryCommand.getName());
        assertEquals(telemetryCommand.getParams(), testTelemetryCommand.getParams());
        assertEquals(pollingMessage.getHeaders(), messageList.get(1).getHeaders());

        // if parameters are Strings and true
        telemetryObject = new TelemetryObject(TelemetryRotatorConstants.GET_POSITION, new Date());
        exchange.getIn().setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_GET_COMMAND);
        telemetryObject.addParameter(new TelemetryParameter("Azimuth", "a"));
        telemetryObject.addParameter(new TelemetryParameter("Elevation", "b"));
        telemetryObject.addParameter(new TelemetryParameter("RPRT", "0"));
        telemetryCommandForDataStore = new TelemetryCommand(TelemetryRotatorConstants.SET_POSITION);
        telemetryCommandForDataStore.addParameter("Azimuth", "a");
        telemetryCommandForDataStore.addParameter("Elevation", "b");
        StoreSetValues.getRequiredParameterValueDataStore().put(telemetryCommandForDataStore.getName(),
                telemetryCommandForDataStore);
        exchange.getIn().setBody(telemetryObject);
        messageList = pollCommands.checkPolling(exchange);
        pollingMessage = new DefaultMessage();
        telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.GET_POSITION);
        pollingMessage.setBody(telemetryCommand);
        pollingMessage.setHeader(JMSConstants.HEADER_DEVICE, exchange.getIn().getHeader(JMSConstants.HEADER_DEVICE));
        pollingMessage.setHeader(JMSConstants.HEADER_GROUNDSTATIONID, exchange.getIn().getHeader(JMSConstants.HEADER_GROUNDSTATIONID));
        pollingMessage.setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_GET_COMMAND);
        pollingMessage.setHeader(JMSConstants.HEADER_FORWARD, JMSConstants.DIRECT_CHOOSE_DEVICE);
        assertTrue(messageList.size() == 1);
        assertEquals(exchange.getIn().toString(), messageList.get(0).toString());

        // if one parameter is decimal and one is String and both are true
        telemetryObject = new TelemetryObject(TelemetryRotatorConstants.GET_POSITION, new Date());
        exchange.getIn().setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_GET_COMMAND);
        telemetryObject.addParameter(new TelemetryParameter("Azimuth", "a"));
        telemetryObject.addParameter(new TelemetryParameter("Elevation", "1"));
        telemetryObject.addParameter(new TelemetryParameter("RPRT", "0"));
        telemetryCommandForDataStore = new TelemetryCommand(TelemetryRotatorConstants.SET_POSITION);
        telemetryCommandForDataStore.addParameter("Azimuth", "a");
        telemetryCommandForDataStore.addParameter("Elevation", "1");
        StoreSetValues.getRequiredParameterValueDataStore().put(telemetryCommandForDataStore.getName(),
                telemetryCommandForDataStore);
        exchange.getIn().setBody(telemetryObject);
        messageList = pollCommands.checkPolling(exchange);
        pollingMessage = new DefaultMessage();
        telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.GET_POSITION);
        pollingMessage.setBody(telemetryCommand);
        pollingMessage.setHeader(JMSConstants.HEADER_DEVICE, exchange.getIn().getHeader(JMSConstants.HEADER_DEVICE));
        pollingMessage.setHeader(JMSConstants.HEADER_GROUNDSTATIONID, exchange.getIn().getHeader(JMSConstants.HEADER_GROUNDSTATIONID));
        pollingMessage.setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_GET_COMMAND);
        pollingMessage.setHeader(JMSConstants.HEADER_FORWARD, JMSConstants.DIRECT_CHOOSE_DEVICE);
        assertTrue(messageList.size() == 1);
        assertEquals(exchange.getIn().toString(), messageList.get(0).toString());

        // If first parameters are String and one is true and other is false
        telemetryObject = new TelemetryObject(TelemetryRotatorConstants.GET_POSITION, new Date());
        exchange.getIn().setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_GET_COMMAND);
        telemetryObject.addParameter(new TelemetryParameter("Azimuth", "Hello"));
        telemetryObject.addParameter(new TelemetryParameter("Elevation", "VAK"));
        telemetryObject.addParameter(new TelemetryParameter("RPRT", "0"));
        telemetryCommandForDataStore = new TelemetryCommand(TelemetryRotatorConstants.SET_POSITION);
        telemetryCommandForDataStore.addParameter("Azimuth", "Hello");
        telemetryCommandForDataStore.addParameter("Elevation", "goodbye");
        StoreSetValues.getRequiredParameterValueDataStore().put(telemetryCommandForDataStore.getName(),
                telemetryCommandForDataStore);
        exchange.getIn().setBody(telemetryObject);
        messageList = pollCommands.checkPolling(exchange);
        pollingMessage = new DefaultMessage();
        telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.GET_POSITION);
        pollingMessage.setBody(telemetryCommand);
        pollingMessage.setHeader(JMSConstants.HEADER_DEVICE, exchange.getIn().getHeader(JMSConstants.HEADER_DEVICE));
        pollingMessage.setHeader(JMSConstants.HEADER_GROUNDSTATIONID, exchange.getIn().getHeader(JMSConstants.HEADER_GROUNDSTATIONID));
        pollingMessage.setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_GET_COMMAND);
        pollingMessage.setHeader(JMSConstants.HEADER_FORWARD, JMSConstants.DIRECT_CHOOSE_DEVICE);
        assertTrue(messageList.size() == 2);
        testTelemetryCommand = (TelemetryCommand) messageList.get(1).getBody();
        assertEquals(exchange.getIn().toString(), messageList.get(0).toString());
        assertEquals(telemetryCommand.getName(), testTelemetryCommand.getName());
        assertEquals(telemetryCommand.getParams(), testTelemetryCommand.getParams());
        assertEquals(pollingMessage.getHeaders(), messageList.get(1).getHeaders());
    }

}
