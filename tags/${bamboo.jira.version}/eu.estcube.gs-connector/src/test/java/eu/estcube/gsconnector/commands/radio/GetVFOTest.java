package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class GetVFOTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private GetVFO createMessage = new GetVFO();
    private static final Logger LOG = LoggerFactory.getLogger(GetVFOTest.class);

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("GET_VFO");
        string.append("+v\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
