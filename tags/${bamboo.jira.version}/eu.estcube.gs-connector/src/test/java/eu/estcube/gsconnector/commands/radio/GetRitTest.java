package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class GetRitTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private GetRit createMessage = new GetRit();
    private static final Logger LOG = LoggerFactory.getLogger(GetRitTest.class);

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("GET_RIT");
        string.append("+j\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
