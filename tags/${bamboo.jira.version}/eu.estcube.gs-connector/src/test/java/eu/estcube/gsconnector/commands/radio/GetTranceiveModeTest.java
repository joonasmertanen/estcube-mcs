package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class GetTranceiveModeTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private GetTranceiveMode createMessage = new GetTranceiveMode();
    private static final Logger LOG = LoggerFactory.getLogger(GetTranceiveModeTest.class);

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("GET_TRANCEIVE_MODE");
        string.append("+a\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
