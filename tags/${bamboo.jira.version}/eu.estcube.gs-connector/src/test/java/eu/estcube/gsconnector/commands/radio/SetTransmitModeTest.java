package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class SetTransmitModeTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SetTransmitMode createMessage = new SetTransmitMode();
    private static final Logger LOG = LoggerFactory.getLogger(SetTransmitModeTest.class);

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("SET_TRANSMIT_MODE");
        telemetryCommand.addParameter("TX Mode", "AM");
        telemetryCommand.addParameter("TX Passband", 0);
        string.append("+X AM 0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
