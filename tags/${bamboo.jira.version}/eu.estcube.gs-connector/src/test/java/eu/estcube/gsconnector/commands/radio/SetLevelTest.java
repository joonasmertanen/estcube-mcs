package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class SetLevelTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SetLevel createMessage = new SetLevel();
    private static final Logger LOG = LoggerFactory.getLogger(SetLevelTest.class);

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("SET_LEVEL");
        telemetryCommand.addParameter("Level", "ATT");
        telemetryCommand.addParameter("Level Value", 0);
        string.append("+L ATT 0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
