package eu.estcube.gsconnector.commands.rotator;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class GetPositionTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private GetPosition createMessage = new GetPosition();
    private static final Logger LOG = LoggerFactory.getLogger(GetPositionTest.class);
  
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("GET_POSITION");
        string.append("+p\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
