package eu.estcube.gsconnector;

import java.nio.charset.Charset;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.JMSConstants;
import junit.framework.TestCase;

public class HamlibDecoderTest extends TestCase {

    private static final Logger LOG = LoggerFactory.getLogger(HamlibDecoderTest.class);
    private HamlibDecoder decoder;

    @Before
    public void setUp() throws Exception {
        decoder = new HamlibDecoder();
    }
    
    /**
     * Test method for {@link eu.estcube.gsconnector.HamlibDecoder#decode(org.jboss.netty.channel.ChannelHandlerContext, org.jboss.netty.channel.Channel, java.lang.Object)}.
     * @throws Exception 
     */
    @Test
    public void testHamlibDecoder()throws Exception{
        ChannelBuffer buffer = ChannelBuffers.wrappedBuffer(new byte[] {});
        Object msg = new Object();
        
        String input = "a";
        buffer = ChannelBuffers.wrappedBuffer(input.getBytes());
        msg = ((ChannelBuffer) buffer).toString(Charset.forName(JMSConstants.ENCODING_STRING_FORMAT));
        assertEquals(null, decoder.decode(null, null, msg));
        
        input = "abc";
        buffer = ChannelBuffers.wrappedBuffer(input.getBytes());
        msg = ((ChannelBuffer) buffer).toString(Charset.forName(JMSConstants.ENCODING_STRING_FORMAT));
        assertEquals(null, decoder.decode(null, null, msg));     

        input = JMSConstants.GS_DEVICE_END_MESSAGE+" \n";
        buffer = ChannelBuffers.wrappedBuffer(input.getBytes());
        msg = ((ChannelBuffer) buffer).toString(Charset.forName(JMSConstants.ENCODING_STRING_FORMAT));
        //System.out.println(msg);
        assertEquals("aabcRPRT \n", decoder.decode(null, null, msg));  
        
        input = " \n\nR\nPRT";
        buffer = ChannelBuffers.wrappedBuffer(new byte[] {});
        buffer = ChannelBuffers.wrappedBuffer(input.getBytes());
        msg = ((ChannelBuffer) buffer).toString(Charset.forName(JMSConstants.ENCODING_STRING_FORMAT));
        assertEquals(null, decoder.decode(null, null, msg));  
        
        input = "rprt \n";
        buffer = ChannelBuffers.wrappedBuffer(input.getBytes());
        msg = ((ChannelBuffer) buffer).toString(Charset.forName(JMSConstants.ENCODING_STRING_FORMAT));
        assertEquals(null, decoder.decode(null, null, msg));  
        
        input = "RPR  T\n";
        buffer = ChannelBuffers.wrappedBuffer(input.getBytes());
        msg = ((ChannelBuffer) buffer).toString(Charset.forName(JMSConstants.ENCODING_STRING_FORMAT));
        assertEquals(null, decoder.decode(null, null, msg));  
    }

}
