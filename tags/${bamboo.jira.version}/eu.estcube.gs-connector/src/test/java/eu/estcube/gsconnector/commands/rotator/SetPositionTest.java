package eu.estcube.gsconnector.commands.rotator;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class SetPositionTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SetPosition createMessage = new SetPosition();
    private static final Logger LOG = LoggerFactory.getLogger(SetPositionTest.class);
    
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("SET_POSITION");
        telemetryCommand.addParameter("Azimuth", 0);
        telemetryCommand.addParameter("Elevation", 0);
        string.append("+P 0 0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
