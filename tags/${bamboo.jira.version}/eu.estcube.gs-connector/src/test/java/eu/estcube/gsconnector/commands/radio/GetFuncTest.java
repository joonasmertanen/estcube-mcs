package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class GetFuncTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private GetFunc createMessage = new GetFunc();
    private static final Logger LOG = LoggerFactory.getLogger(GetFuncTest.class);
 
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("GET_FUNC");
        telemetryCommand.addParameter("Func", "FAGC");
        string.append("+u FAGC\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
