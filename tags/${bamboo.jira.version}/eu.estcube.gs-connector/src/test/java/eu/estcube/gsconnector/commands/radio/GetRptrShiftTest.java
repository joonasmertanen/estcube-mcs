package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class GetRptrShiftTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private GetRptrShift createMessage = new GetRptrShift();
    private static final Logger LOG = LoggerFactory.getLogger(GetRptrShiftTest.class);
    
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("GET_RPTR_SHIFT");
        string.append("+r\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
