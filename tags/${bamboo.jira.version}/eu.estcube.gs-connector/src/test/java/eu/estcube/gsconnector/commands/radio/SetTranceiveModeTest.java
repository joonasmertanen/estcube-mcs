package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class SetTranceiveModeTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SetTranceiveMode createMessage = new SetTranceiveMode();
    private static final Logger LOG = LoggerFactory.getLogger(SetTranceiveModeTest.class);

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("CONVERT_MW2POWER");
        telemetryCommand.addParameter("Transceive", "OFF");
        string.append("+A OFF\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
