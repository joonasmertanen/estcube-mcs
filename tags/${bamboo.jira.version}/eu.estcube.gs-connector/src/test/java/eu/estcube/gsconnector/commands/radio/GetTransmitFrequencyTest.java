package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class GetTransmitFrequencyTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private GetTransmitFrequency createMessage = new GetTransmitFrequency();
    private static final Logger LOG = LoggerFactory.getLogger(GetTransmitFrequencyTest.class);
 
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("GET_TRANSMIT_FREQUENCY");
        string.append("+i\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
