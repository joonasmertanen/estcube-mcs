dojo.provide("webgui.display.StatusesGraphDisplayGS");
dojo.require("dijit.layout.StackContainer");
dojo.require("dojox.gfx");
dojo.require("dojox.gfx.Moveable");

dojo.declare("webgui.display.StatusesGraphDisplayGS", null, {

	constructor : function() {

		console.log("StatusesGraphDisplayGS activated!!!");

		/*
		 * Function to make Nodes
		 * name - name of the node
		 * id - id of the node
		 * size - length and width of the node
		 * xPos - x position
		 * yPos - y position
		 */
		function createNode(name, newId, size, xPos, yPos, subcomponents) {

			var grpName = surface.createGroup();

			grpName.createRect({
				id : newId,
				x : xPos,
				y : yPos,
				width : size,
				height : size
			}).setFill("#F0F0F0").setStroke({
				width : 5,
				color : "green"
			});

			grpName.createText({
				x : xPos + 10,
				y : yPos + nameSize + 10,
				text : name,
				align : "start"
			}).setFont({
				family : "Arial",
				size : nameSize + "pt",
				weight : "bold"
			}).setFill("black");

			for(var i = 0; i < subcomponents.length; i++) {

				grpName.createText({
					id : subcomponents[i] + "Id",
					x : xPos + 10,
					y : yPos + 3 * nameSize + 10 + (subComponentTextSize + 10 ) * i,
					text : subcomponents[i],
					align : "start"
				}).setFont({
					family : "Arial",
					size : subComponentTextSize + "pt",
					weight : "bold"
				}).setFill("black");

			}

			return grpName;

		}

		/*
		 * Function to make Connections
		 * startX - X coordinate of line start
		 * startY - Y coordinate of line start
		 * endX - X coordinate of line end
		 * endY - Y coordinate of line end
		 */

		function createConnection(startX, startY, endX, endY) {
			var grpName = surface.createGroup();

			grpName.createLine({
				x1 : startX,
				y1 : startY,
				x2 : endX,
				y2 : endY
			}).setStroke({
				width : 5,
				color : "black"
			});

			return grpName;
		}

		var subComponentTextSize = 12;
		var nodeSize = 200;
		var nameSize = 20;
		var subComponentTextSize = 12;

		var radioComponents = new Array(4);
		radioComponents[0] = "Frequency: ";
		radioComponents[1] = "Mode: ";
		radioComponents[2] = "VFO: ";
		radioComponents[3] = "Model info: ";

		var rotatorComponents = new Array(3);
		rotatorComponents[0] = "Azimuth: ";
		rotatorComponents[1] = "Elevation: ";
		rotatorComponents[2] = "Model info: ";

		var statusesGraphContent = new dijit.layout.ContentPane({
			id : "statusesGraphTab",
			title : "Graph",
			preventCache : true
		});

		var tempCont;
		tempCont = dijit.byId("CenterContainer");

		tempCont.addChild(statusesGraphContent);
		tempCont.selectChild(statusesGraphContent);

		var surfaceVar = dojo.create("div", {
			id : "surfaceElement",
			region : "center"
		}, "statusesGraphTab");

		var contentBox = dojo.contentBox("CenterContainer");
		var surface = dojox.gfx.createSurface("surfaceElement", contentBox.w, contentBox.h);

		var grpAllObjects = surface.createGroup();
		var radioGRP;
		var rotatorGRP;
		var connectionRadioRotator;
		radioGRP = createNode("RADIO", "radioNode", nodeSize, 60, 60, radioComponents);
		rotatorGRP = createNode("ROTATOR", "rotatorNode", nodeSize, 60 + 2 * nodeSize, 60, rotatorComponents);
		connectionRadioRotator = createConnection(60 + nodeSize, 60 + nodeSize / 2, 60 + 2 * nodeSize, 60 + nodeSize / 2);

		radioGRP.connect("onclick", function(e) {
			var tc = dijit.byId("componentTabContainer");
			tc.selectChild(dijit.byId("radioTab"));
		});

		rotatorGRP.connect("onclick", function(e) {
			var tc = dijit.byId("componentTabContainer");
			tc.selectChild(dijit.byId("rotatorTab"));
		});

		grpAllObjects.add(radioGRP);
		grpAllObjects.add(rotatorGRP);
		grpAllObjects.add(connectionRadioRotator);

		// FOR TESTING!
		var shrink = surface.createRect({
			x : 0,
			y : 0,
			width : 15,
			height : 15
		}).setFill("red");
		shrink.connect("onclick", function(e) {
			grpAllObjects.applyTransform(dojox.gfx.matrix.scale({
				x : 0.75,
				y : 0.75
			}));
			var contentBox = dojo.contentBox("CenterContainer");
			surface.setDimensions(contentBox.w, contentBox.h);
		});
		var scale = surface.createRect({
			x : 15,
			y : 0,
			width : 15,
			height : 15
		}).setFill("blue");
		scale.connect("onclick", function(e) {
			grpAllObjects.applyTransform(dojox.gfx.matrix.scale({
				x : 1.25,
				y : 1.25
			}));
			var contentBox = dojo.contentBox("CenterContainer");
			surface.setDimensions(contentBox.w, contentBox.h);
		});
	}
});
