/**
 * Created by JetBrains WebStorm.
 * User: Stenver
 * Date: 4.11.11
 * Time: 16:35
 * To change this template use File | Settings | File Templates.
 */

dojo.provide("webgui.display.MainTabDisplay");

dojo.require("dijit.layout.TabContainer");
dojo.require("dijit.layout.StackContainer");
dojo.require("dijit.layout.ContentPane");
dojo.require("dojox.charting.Chart");
dojo.require("dojox.charting.themes.Claro");
dojo.require("dojo.store.Observable");
dojo.require("dojo.store.Memory");
dojo.require("dojox.charting.StoreSeries");
dojo.require("dojox.charting.action2d.Tooltip");
dojo.require("dojox.charting.action2d.MoveSlice");


dojo.require("dojox.charting.widget.Legend");
dojo.require("dojox.charting.action2d.Tooltip");
dojo.require("dojox.charting.action2d.Magnify");
dojo.require("dojox.grid.DataGrid");
dojo.require("dojox.charting.DataChart");

dojo.require("dojo.parser");
dojo.require("dojox.data.PersevereStore");
dojo.require("dojox.cometd.RestChannels");
dojo.require("dojo.cookie");
dojo.require("dojo.date.locale");

dojo.require("dojox.charting.themes.PlotKit.red");

dojo.declare("webgui.display.graph", null,{

    updateInterval: 500, // update interval in milliseconds
        
	constructor : function(chartArea) {
            var key = 1;
            var storedata = { identifier: "key", items: [] };
            var store = new dojo.data.ItemFileWriteStore({ data: storedata });
            this.getStore = function() {
                return store;
            };

            var chart = new dojox.charting.Chart("chartArea");
	        chart.setTheme(dojox.charting.themes.Claro);
            var getChart = function() {
                return chart;
            };
            chart.addPlot("default", {
                type: "Lines", // type of chart
                markers: true,
                lines: true,
                labelOffset: -30,
                shadows: { dx:2, dy:2, dw:2 }
            });
            chart.addAxis("x", {
                labelFunc: function(str, value){
                    return dojo.date.locale.format(new Date(value), {datePattern: "HH:mm:ss", selector: "date"});
                },
                majorTickStep: 60000,
                majorLabels: true,
                minorTicks: false,
                minorLabels: true,
                microTicks: false
            });
            chart.addAxis("y", { vertical: true, min: 0, max: 90 });
            new dojox.charting.action2d.Magnify(chart, "default");
            new dojox.charting.action2d.Tooltip(chart, "default");

            // for removing from store TODO refactor
            var counter = 0;
            var limit = 10;

            var handleParameter = function (myObject) {
                //firstParameter
                var storeElem = {};
                storeElem.Value = myObject.params[1].value;
                storeElem.Name = myObject.params[1].name;
                storeElem.Timestamp = myObject.time;
                storeElem.key = key;
                store.newItem(storeElem);
                key++;
                counter++;
                //second parameter
                storeElem = {};
                storeElem.Value = myObject.params[2].value;
                storeElem.Name = myObject.params[2].name;
                storeElem.Timestamp = myObject.time;
                storeElem.key = key;
                store.newItem(storeElem);
                key++;
                counter++;
                if (counter > limit) {
                    // getting the size of the store
                        console.log(counter + " "+ size+" "+ limit+ " ");
                    var size = function(size, request) {
                        // remove excess elements
                        store.fetch({ count: (size - limit),
                            onItem: function(item) {
                                store.deleteItem(item);
                            }
                        });
                    };
                    store.fetch({ query: {}, onBegin: size, start: 0, count: 0 });
                }
            };

            // connect browser resize to chart
            dojo.connect(dijit.byId("chartPane"), "resize", this, function(evt) {
                var dim = dijit.byId("chartPane")._contentBox;
                chart.resize(dim.w, dim.h);
            });
            // update View at set interval
            var legend;
            var chartContainer = dojo.byId("ChartContainer");


            var updateView = function() {
                chart.render();
                // legend
                if (legend) {
                    legend.destroy();
                }
                chartArea.appendChild(dojo.create("div", { id: "legend" }));
                legend = new dojox.charting.widget.Legend({ chart:chart }, "legend");
            };

            setInterval(updateView, this.updateInterval);

//        /* get the right plot of the series, also truncates the series for scrolling */
            var historyLimit = 5; // the limit of datapoints TODO should be a mixinvariable, but then this function can't access this
            /*** This function should be refactored to access/retrieve data from abstraction ****/
            var getSeriesData = function(itemName) {
                var seriesExists = false;
                var seriesArrayData;
                dojo.forEach(getChart().series, function(entry, key) {
                    if (entry.name == itemName) {
                        seriesExists = true;
                        seriesArrayData = entry.data;
                    }
                });
                if (!seriesExists) {
                    return false;
                }
                if (seriesArrayData.length >= historyLimit) {
                    seriesArrayData.splice(0, seriesArrayData.length - historyLimit);
                }
                //TODO opt should be changed
                //TODO labels always appear to be 1 step behind
                return seriesArrayData;
            }


            var count = 0;
            // to change chart as new items are added
            var updateViewCallback = function(item) {
                var seriesArrayData = getSeriesData(item.Name);
                var jsdate = dojo.date.locale.parse(item["Timestamp"], {datePattern: "yyyy-DD HH:mm:ss.SSSZ", selector: "date"});
                if (seriesArrayData === false) {
                    getChart().addSeries(item.Name, [{ x: jsdate, y: item["Value"], tooltip: "hello" }]);
                } else {
                    seriesArrayData.push({ x: jsdate, y: item["Value"], tooltip: "hello"});
                    getChart().updateSeries(item.Name, seriesArrayData);
                    getChart().getAxis("x").opt.majorTickStep = (seriesArrayData[seriesArrayData.length-1].x.getTime() - seriesArrayData[0].x.getTime())/5;
                }
            };
            // TODO not a good idea to update the view constantly
            dojo.connect(this.getStore(), "newItem", updateViewCallback);
            dojo.subscribe("GetPositionAzimuth", handleParameter);
        }
        
    }
);