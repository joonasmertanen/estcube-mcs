dojo.provide("webgui.display.PictureDisplay");

dojo.require("dijit.form.TextBox");
dojo.require("dijit.form.Button");
dojo.require("dijit.layout.ContentPane");
dojo.require("dijit.layout.StackContainer");
dojo.require("dojox.socket");
dojo.require("dojox.gfx");

dojo.declare("webgui.display.PictureDisplay", null, {
	
	constructor: function() {
		console.log("PictureDisplay!");
		
		var pictureTab = new dijit.layout.ContentPane({
        	id: "pictureTab",
        	title: "Picture",
        	preventCache: true
        		
       });
		
        var tempCont;
        tempCont = dijit.byId("PictureView");        
        tempCont.addChild(pictureTab);
        
		var surfaceVar = dojo.create("div", {
			id : "surfaceElement",
			region : "center"
		}, "pictureTab");
		
		
		var surface = dojox.gfx.createSurface("surfaceElement",640,480);
		
		dojo.subscribe("fromWebCam", function(message) {
		var staticImage = surface.createImage({ 
		    x: 0, y: 0, width: 640, height: 480, src: "data:image/jpeg;base64," + message + ""
		});
		});			
	}
});