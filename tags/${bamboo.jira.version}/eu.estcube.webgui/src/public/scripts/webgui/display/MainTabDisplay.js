dojo.provide("webgui.display.MainTabDisplay");

dojo.require("dijit.layout.TabContainer");
dojo.require("dijit.layout.StackContainer");
dojo.require("dijit.layout.ContentPane");
dojo.require("dojox.charting.Chart");
dojo.require("dojox.charting.themes.Claro");
dojo.require("dojo.store.Observable");
dojo.require("dojo.store.Memory");
dojo.require("dojox.charting.StoreSeries");
dojo.require("dojox.charting.action2d.Tooltip");
dojo.require("dojox.charting.action2d.MoveSlice");


dojo.require("dojox.charting.widget.Legend");
dojo.require("dojox.charting.action2d.Tooltip");
dojo.require("dojox.charting.action2d.Magnify");
dojo.require("dojox.grid.DataGrid");
dojo.require("dojox.charting.DataChart");

dojo.require("dojo.parser");
dojo.require("dojox.data.PersevereStore");
dojo.require("dojox.cometd.RestChannels");
dojo.require("dojo.cookie");
dojo.require("dojo.date.locale");

dojo.require("dojox.charting.themes.PlotKit.red");

dojo.declare("webgui.display.MainTabDisplay", null, {

	constructor : function(view) {

		console.log("TabDisplay");

		var data = [
			{ id: 1, value: 0, parameter: "Azimuth" },
			{ id: 2, value: 0, parameter: "Elevation" }
			];
		
		var telemetryTab = new dijit.layout.ContentPane({
			id: "chartTab",
			title : "Telemetry",
			preventCache: true
				
		});

		var gsTab = new dijit.layout.ContentPane({
			title : "GS",
			content : "Groundstation online!",
			closable : "true",
			preventCache: true
		});
		
		var commandTab = new dijit.layout.BorderContainer({
			id : "commandTab",
			title : "Command",
			preventCache: true
		});		
		
		var commandLogA = new dijit.layout.ContentPane({
			id : "commandLogA",
			title : "LogArea",
			region : "center",
			preventCache: true
		});	

		var commandA = new dijit.layout.ContentPane({
			id : "commandA",
			region : "bottom",
			title : "CmdArea",
			content: "Insert Commands:",
			preventCache: true
		});	
		
		commandTab.addChild(commandLogA);
		commandTab.addChild(commandA);
		

		var tabs = new dijit.layout.TabContainer({
			region : "center",
			tabPosition : "bottom",
			id : "mainTabContainer",
			preventCache: true
		});

		var tempCont;
		tempCont = dijit.byId("CenterContainer");

		tabs.addChild(telemetryTab);
		tabs.addChild(gsTab);
		tabs.addChild(commandTab);
		tempCont.addChild(tabs);
		tempCont.selectChild(tabs);
		tabs.startup();
		
		var chartArea = dojo.create("div", {
			id : "chartArea",
			region : "center",
			width: 600,
			height: 600
		}, "chartTab");	
		

		

		var cmdLogArea = dojo.create("div", {
			id : "commandLogArea2",
			region : "center",
			style : "width:100%; height:90; overflow:auto;"
		}, "commandLogA");
		
		var someData = [];
		var store3 = new dojo.data.ItemFileWriteStore({
			data : {
				items : someData
			}
		});
		
        function createChart() {
            var key = 1;
            var storedata = { identifier: "key", items: [] };
            var store = new dojo.data.ItemFileWriteStore({ data: storedata });
            this.getStore = function() {
                return store;
            };
            
            var chart = new dojox.charting.Chart("chartArea");
	        chart.setTheme(dojox.charting.themes.Claro);
            var getChart = function() {
                return chart;
            };
            chart.addPlot("default", {
                type: "Lines", // type of chart
                markers: true,
                lines: true,
                labelOffset: -30,
                shadows: { dx:2, dy:2, dw:2 }
            });
            chart.addAxis("x", {
                labelFunc: function(str, value){
                    return dojo.date.locale.format(new Date(value), {datePattern: "HH:mm:ss", selector: "date"});
                },
                majorTickStep: 60000,
                majorLabels: true,
                minorTicks: false,
                minorLabels: true,
                microTicks: false
            });
            chart.addAxis("y", { vertical: true, min: 0, max: 90 });
            new dojox.charting.action2d.Magnify(chart, "default");
            new dojox.charting.action2d.Tooltip(chart, "default");
            
            // for removing from store TODO refactor
            var counter = 0;
            var limit = 10;

            var handleParameter = function (myObject) {
                //firstParameter
                var storeElem = {};
                storeElem.Value = myObject.params[1].value;
                storeElem.Name = myObject.params[1].name;
                storeElem.Timestamp = myObject.time;
                storeElem.key = key;
                store.newItem(storeElem);
                key++;
                counter++;
                //second parameter
                storeElem = {};
                storeElem.Value = myObject.params[2].value;
                storeElem.Name = myObject.params[2].name;
                storeElem.Timestamp = myObject.time;
                storeElem.key = key;
                store.newItem(storeElem);
                key++;
                counter++;
                if (counter > limit) {
                    // getting the size of the store
                    var size = function(size, request) {
                        // remove excess elements
                        store.fetch({ count: (size - limit),
                            onItem: function(item) {
                                store.deleteItem(item);
                            }
                        });
                    };
                    store.fetch({ query: {}, onBegin: size, start: 0, count: 0 });
                }
            };

            // connect browser resize to chart
            dojo.connect(dijit.byId("chartPane"), "resize", this, function(evt) {
                var dim = dijit.byId("chartPane")._contentBox;
                chart.resize(dim.w, dim.h);
            });
            // update View at set interval
            var legend;
            var chartContainer = dojo.byId("ChartContainer");


            var updateView = function() {
                chart.render();
                // legend
                if (legend) {
                    legend.destroy();
                }
                chartArea.appendChild(dojo.create("div", { id: "legend" }));
                legend = new dojox.charting.widget.Legend({ chart:chart }, "legend");
            };

            setInterval(updateView, this.updateInterval);

//        /* get the right plot of the series, also truncates the series for scrolling */
            var historyLimit = 5; // the limit of datapoints TODO should be a mixinvariable, but then this function can't access this
            /*** This function should be refactored to access/retrieve data from abstraction ****/
            var getSeriesData = function(itemName) {
                var seriesExists = false;
                var seriesArrayData;
                dojo.forEach(getChart().series, function(entry, key) {
                    if (entry.name == itemName) {
                        seriesExists = true;
                        seriesArrayData = entry.data;
                    }
                });
                if (!seriesExists) {
                    return false;
                }
                if (seriesArrayData.length >= historyLimit) {
                    seriesArrayData.splice(0, seriesArrayData.length - historyLimit);
                }
                //TODO opt should be changed
                //TODO labels always appear to be 1 step behind
                return seriesArrayData;
            }


            var count = 0;
            // to change chart as new items are added
            var updateViewCallback = function(item) {
                var seriesArrayData = getSeriesData(item.Name);
                var jsdate = dojo.date.locale.parse(item["Timestamp"], {datePattern: "yyyy-DD HH:mm:ss.SSSZ", selector: "date"});
                if (seriesArrayData === false) {
                    getChart().addSeries(item.Name, [{ x: jsdate, y: item["Value"], tooltip: "hello" }]);
                } else {
                    seriesArrayData.push({ x: jsdate, y: item["Value"], tooltip: "hello"});
                    getChart().updateSeries(item.Name, seriesArrayData);
                    getChart().getAxis("x").opt.majorTickStep = (seriesArrayData[seriesArrayData.length-1].x.getTime() - seriesArrayData[0].x.getTime())/5;
                }
            };
            // TODO not a good idea to update the view constantly
            dojo.connect(this.getStore(), "newItem", updateViewCallback);
            dojo.subscribe("GetPositionAzimuth", handleParameter);
        }

        createChart();
		
		var tableHeader = [{
			field: "name",
			name: "Telemetry Command",
			width: 11
		},{
			//field: "_params",
			field:  "params",
			name: "Parameters",
			// formatter: function (params) {
				// console.log("444" + JSON.stringify(params));
				// return JSON.stringify(params);
			// },		
			formatter: function(params, rowIndex) {
			    var item = this.grid.getItem(rowIndex);
			    return JSON.stringify(item.params);
			},			
			width: 17
		}];
		
		var cmdLogArea = new dojox.grid.DataGrid({
			id: "commandLogArea",
			title: "Telemetry Commands",
			store: store3,
			structure: tableHeader
            }, "commandLogArea2");

		cmdLogArea.startup();

		var cmdArea = dojo.create("div", {
			id : "commandArea",
			region : "center",
			style : "border: none",
		}, "commandA");
		
		var commandBox = new dijit.form.TextBox({
			id : "commandTextBox",
			value : "GET_POSITION"
		}).placeAt("commandArea");

		var buttonSend = new dijit.form.Button({
			label : "Send",
			onClick : function() {
				var muutuja = dijit.byId("commandTextBox");
				var split = muutuja.get('value').split(" ");

				var params = new Array();
				console.log(split.length % 2);
				
				if ((split.length - 1) % 2 == 0) {
					for (i = 1; i < split.length - 1; i = i + 2) {
						params.push(new TelemetryParameter(split[i],
								split[i + 1]));
					}
				}
// 
				var telemetryCommand = new TelemetryCommand(split[0], params);
				store3.newItem(telemetryCommand);
				
				cmdLogArea.scrollToRow(cmdLogArea.rowCount);
					
				
				dojo.publish("toWS", [JSON.stringify(telemetryCommand)]);
				
			}
		}).placeAt("commandArea");
		
		var buttonClear = new dijit.form.Button({
			label : "Clear",
			onClick : function() {
				cmdLogArea._clearData();
			}
		}).placeAt("commandArea");
		
		function TelemetryParameter(name, value) {
			this.name = name;
			this.value = value;
		}

		function TelemetryCommand(name, params) {
			this.name = name;
			this.params = params;
		}	

	}
});