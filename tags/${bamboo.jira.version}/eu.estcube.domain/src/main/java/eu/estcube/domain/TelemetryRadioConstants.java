package eu.estcube.domain;

public class TelemetryRadioConstants {
    public static final String SET_FREQUENCY = "SET_FREQUENCY";
    public static final String GET_FREQUENCY = "GET_FREQUENCY";
    public static final String SET_MODE = "SET_MODE";
    public static final String SET_LEVEL = "SET_LEVEL";
    public static final String SET_VFO = "SET_VFO";
    public static final String SET_PTT = "SET_PTT";
    public static final String GET_PTT = "GET_PTT";
    public static final String SET_ANTENNA_NR = "SET_ANTENNA_NR";
    public static final String CAPABILITIES = "CAPABILITIES";
    public static final String CONFIGURATION = "CONFIGURATION";
    public static final String GET_MODE = "GET_MODE";
    public static final String SET_TRANSMIT_FREQUENCY = "SET_TRANSMIT_FREQUENCY";
    public static final String GET_TRANSMIT_FREQUENCY = "GET_TRANSMIT_FREQUENCY";
    public static final String SET_TRANSMIT_MODE = "SET_TRANSMIT_MODE";
    public static final String GET_TRANSMIT_MODE = "GET_TRANSMIT_MODE";
    public static final String SET_SPLIT_VFO = "SET_SPLIT_VFO";   
    public static final String GET_SPLIT_VFO = "GET_SPLIT_VFO";
    public static final String SET_TUNING_STEP = "SET_TUNING_STEP";
    public static final String GET_TUNING_STEP = "GET_TUNING_STEP";
    public static final String GET_LEVEL = "GET_LEVEL";
    public static final String SET_FUNC = "SET_FUNC";
    public static final String GET_FUNC = "GET_FUNC";
    public static final String SET_PARM = "SET_PARM";
    public static final String GET_PARM = "GET_PARM";
    public static final String VFO_OPERATION = "VFO_OPERATION";
    public static final String SCAN = "SCAN";
    public static final String SET_TRANCEIVE_MODE = "SET_TRANCEIVE_MODE";
    public static final String GET_TRANCEIVE_MODE = "GET_TRANCEIVE_MODE";
    public static final String SET_RPTR_SHIFT = "SET_RPTR_SHIFT";
    public static final String GET_RPTR_SHIFT = "GET_RPTR_SHIFT";
    public static final String SET_RPTR_OFFSET = "SET_RPTR_OFFSET";
    public static final String GET_RPTR_OFFSET = "GET_RPTR_OFFSET"; 
    public static final String SET_CTCSS_TONE = "SET_CTCSS_TONE";
    public static final String GET_CTCSS_TONE = "GET_CTCSS_TONE";
    public static final String SET_DCS_CODE = "SET_DCS_CODE";
    public static final String GET_DCS_CODE = "GET_DCS_CODE";
    public static final String GET_VFO = "GET_VFO";
    public static final String SET_MEMORY_CHANNELNR = "SET_MEMORY_CHANNELNR";
    public static final String GET_MEMORY_CHANNELNR = "GET_MEMORY_CHANNELNR";
    public static final String SET_MEMORY_CHANNELDATA = "SET_MEMORY_CHANNELDATA";
    public static final String GET_MEMORY_CHANNELDATA = "GET_MEMORY_CHANNELDATA";
    public static final String SET_MEMORYBANK_NR = "SET_MEMORYBANK_NR";
    public static final String GET_INFO = "GET_INFO";
    public static final String SET_RIT = "SET_RIT";
    public static final String GET_RIT = "GET_RIT";
    public static final String SET_XIT = "SET_XIT";
    public static final String GET_XIT = "GET_XIT";
    public static final String GET_ANTENNA_NR = "GET_ANTENNA_NR";   
    public static final String RESET = "RESET";
    public static final String SEND_RAW_CMD = "SEND_RAW_CMD";
    public static final String SEND_MORSE = "SEND_MORSE";
    public static final String CONVERT_POWER2MW = "CONVERT_POWER2MW";
    public static final String CONVERT_MW2POWER = "CONVERT_MW2POWER";
    public static final String HELP = "HELP";
    public static final String RADIO_STATUS = "RadioStatus";


    public TelemetryRadioConstants() {

    }
}
