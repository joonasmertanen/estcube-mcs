/**
 * 
 */
package eu.estcube.domain;

import java.io.Serializable;

public class TelemetryParameter implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1340205089337640173L;
    private final String name;
    private Object value;

    /**
     * Creates new TelemetryParameter, with only a name.
     * 
     * @param name
     */
    public TelemetryParameter(String name) {
        this.name = name;
    }

    /**
     * Creates new TelemetryParameter.
     * 
     * @param name
     * @param value
     */
    public TelemetryParameter(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    /**
     * Returns name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns value.
     * 
     * @return the value
     */
    public Object getValue() {
        return value;
    }

    /**
     * Sets the value of parameter.
     * 
     * @param value
     *            the value to set
     */
    public void setValue(Object value) {
        this.value = value;
    }
}
