package eu.estcube.domain;

public class JMSConstants {
        public static final String AMQ_GS_RECEIVE = "activemq:topic:gsReceive";
        public static final String AMQ_GS_SEND = "activemq:topic:gsSend";
        public static final String AMQ_PS_QUEUE = "activemq:queue:psQueue";        
        public static final String AMQ_WEBCAM_SEND ="activemq:topic:webcamSend"; 
        
        public static final String GS_ALL_NAMES = "*";
        public static final String GS_ID_HEADER = "groundStationID";
        public static final String GS_RIG_CTLD = "rigctld";
        public static final String GS_ROT_CTLD = "rotctld";
        public static final String GS_DEVICE_END_MESSAGE = "RPRT";
        public static final int GS_MAX_FRAME_LENGTH = 10240; 
        
        public static final String DIRECT_CHOOSE_DEVICE = "direct:chooseDevice";
        public static final String DIRECT_SEND = "direct:gsSend";
        public static final String DIRECT_RIG_CTLD = "direct:rigctld";
        public static final String DIRECT_ROT_CTLD = "direct:rotctld";
        public static final String DIRECT_POLL_COMMAND = "direct:pollCommand";
        public static final String DIRECT_RIG_STATUS = "direct:rigStatus"; 
        public static final String DIRECT_ROT_STATUS = "direct:rotStatus";  
        public static final String DIRECT_STATUS = "direct:status";     
        public static final String DEVICE_HEADER = "device";
        
        public static final int POLL_COMMAND_TIMEOUT = 1000;
        public static final String POLL_SET_COMMAND = "setCommand";
        public static final String POLL_GET_COMMAND = "getCommand";
        public static final String POLL_NOT_REQUIRED = "notRequired";
        
        public static final String ENCODING_STRING_FORMAT = "ASCII";
        
        public static final String HEADER_POLLING = "polling";
        public static final String HEADER_FORWARD = "forward";
        public static final String HEADER_DEVICE = "device";
        public static final String HEADER_GROUNDSTATIONID = "groundStationID";
        
        public static final String CLASS_POLL_COMMANDS = "pollCommands";
        
    public JMSConstants() {

    }
}
