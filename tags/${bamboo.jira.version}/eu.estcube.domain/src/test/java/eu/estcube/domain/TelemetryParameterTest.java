/**
 * 
 */
package eu.estcube.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TelemetryParameterTest {

    private static final String NAME = "A";
    private static final Object VALUE = "Value";


    private TelemetryParameter tp, tp2; 
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        tp = new TelemetryParameter("B");
        tp2 = new TelemetryParameter(NAME, VALUE);
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryParameter#TelemetryParameter(java.lang.String)}.
     */
    @Test
    public void testTelemetryParameterString() {
        assertNotNull(tp);
        assertEquals(null, tp.getValue());
        assertEquals("B", tp.getName());
        assertNotNull(tp.toString());
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryParameter#TelemetryParameter(java.lang.String, java.lang.Object)}.
     */
    @Test
    public void testTelemetryParameterStringObject() {
        assertNotNull(tp2);
        assertNotNull(tp2.getValue());
        assertEquals(VALUE, tp2.getValue());
        assertEquals(NAME, tp2.getName());
        assertNotNull(tp2.toString());
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryParameter#getName()}.
     */
    @Test
    public void testGetName() {
        assertEquals("B", tp.getName());
        assertEquals(NAME, tp2.getName());
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryParameter#getValue()}.
     */
    @Test
    public void testGetValue() {
        assertEquals(null, tp.getValue());
        assertEquals(VALUE, tp2.getValue());
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryParameter#setValue(java.lang.Object)}.
     */
    @Test
    public void testSetValue() {
        tp.setValue("B-Val");
        assertEquals("B-Val", tp.getValue());
        tp2.setValue("New-Val");
        assertEquals("New-Val", tp2.getValue());
    }

}
