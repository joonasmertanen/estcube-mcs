package eu.estcube.sdc.domain;

/**
 *
 */
public class Ax25UiFrameInput {

    private String destAddr;
    private String srcAddr;
    private String ctrl;
    private String pid;
    private String info;
    private String prefix;
    private int tncTargetPort;
    private String satelliteId;
    private String groundStationId;

    /**
     * Creates new Ax25UiFrameInput.
     */
    public Ax25UiFrameInput() {
    }

    /**
     * Returns destAddr.
     *
     * @return the destAddr
     */
    public String getDestAddr() {
        return destAddr;
    }

    /**
     * Sets destAddr.
     *
     * @param destAddr the destAddr to set
     */
    public void setDestAddr(String destAddr) {
        this.destAddr = destAddr;
    }

    /**
     * Returns srcAddr.
     *
     * @return the srcAddr
     */
    public String getSrcAddr() {
        return srcAddr;
    }

    /**
     * Sets srcAddr.
     *
     * @param srcAddr the srcAddr to set
     */
    public void setSrcAddr(String srcAddr) {
        this.srcAddr = srcAddr;
    }

    /**
     * Returns ctrl.
     *
     * @return the ctrl
     */
    public String getCtrl() {
        return ctrl;
    }

    /**
     * Sets ctrl.
     *
     * @param ctrl the ctrl to set
     */
    public void setCtrl(String ctrl) {
        this.ctrl = ctrl;
    }

    /**
     * Returns pid.
     *
     * @return the pid
     */
    public String getPid() {
        return pid;
    }

    /**
     * Sets pid.
     *
     * @param pid the pid to set
     */
    public void setPid(String pid) {
        this.pid = pid;
    }

    /**
     * Returns info.
     *
     * @return the info
     */
    public String getInfo() {
        return info;
    }

    /**
     * Sets info.
     *
     * @param info the info to set
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     * Returns prefix.
     *
     * @return the prefix
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Sets prefix.
     *
     * @param prefix the prefix to set
     */
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    /**
     * Returns tncTargetPort.
     *
     * @return the tncTargetPort
     */
    public int getTncTargetPort() {
        return tncTargetPort;
    }

    /**
     * Sets tncTargetPort.
     *
     * @param tncTargetPort the tncTargetPort to set
     */
    public void setTncTargetPort(int tncTargetPort) {
        this.tncTargetPort = tncTargetPort;
    }

    /**
     * Returns satelliteId.
     * 
     * @return the satelliteId
     */
    public String getSatelliteId() {
        return satelliteId;
    }

    /**
     * Sets satelliteId.
     * 
     * @param satelliteId the satelliteId to set
     */
    public void setSatelliteId(String satelliteId) {
        this.satelliteId = satelliteId;
    }

    /**
     * Returns groundStationId.
     * 
     * @return the groundStationId
     */
    public String getGroundStationId() {
        return groundStationId;
    }

    /**
     * Sets groundStationId.
     * 
     * @param groundStationId the groundStationId to set
     */
    public void setGroundStationId(String groundStationId) {
        this.groundStationId = groundStationId;
    }
}
