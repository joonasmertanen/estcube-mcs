package eu.estcube.gs.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Predicate;
import org.hbird.exchange.constants.StandardArguments;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.codec.ax25.impl.Ax25UIFrameKissDecoder;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;

/**
 * Takes ax25UIFrame from exchange and checks if the frame is valid.
 * 
 * @author Aivar Lobjakas
 * @since 31.07.2014
 */
@Component
public class IncompletePacketFilter implements Predicate {

    private static final Logger LOG = LoggerFactory.getLogger(IncompletePacketFilter.class);

    @Autowired
    private Ax25UIFrameKissDecoder decoder;

    @Override
    public boolean matches(Exchange exchange) {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        out.copyFrom(in);
        try {
            Ax25UIFrame ax25UIFrame = in.getBody(Ax25UIFrame.class);
            if (ax25UIFrame == null) {
                throw new IllegalArgumentException("No AX.25 frame available in message body");
            }

            if (ax25UIFrame.getStatus().isValid()) {
                return true;
            }

        } catch (Exception e) {
            LOG.error("Failed to process TncFrame to Ax25UIFrame; {}", e.getMessage());
            exchange.setException(e);
        }
        LOG.info(String.format("Dropped packet from %s with timestamp %d received at %s",
                in.getHeader(StandardArguments.SATELLITE_ID), in.getHeader(StandardArguments.TIMESTAMP),
                in.getHeader(StandardArguments.GROUND_STATION_ID)));
        return false;
    }

}
