package eu.estcube.gs.tnc.domain;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.estcube.gs.GsDriverConfiguration;

/**
 *
 */
@Component
public class TncDriverConfiguration extends GsDriverConfiguration {

    private static final long serialVersionUID = 6642171032950558614L;

    @Value("${serial.in.port}")
    private String serialInPort;

    @Value("${serial.in.baud}")
    private String serialInBaud;

    @Value("${serial.in.dataBits}")
    private String serialInDataBits;

    @Value("${serial.in.stopBits}")
    private String serialInStopBits;

    @Value("${serial.in.parity}")
    private String serialInParity;

    @Value("${serial.in.flowControl}")
    private String serialInFlowControl;

    @Value("${serial.in.filters}")
    private String serialInFilters;

    @Value("${serial.out.port}")
    private String serialOutPort;

    @Value("${serial.out.baud}")
    private String serialOutBaud;

    @Value("${serial.out.dataBits}")
    private String serialOutDataBits;

    @Value("${serial.out.stopBits}")
    private String serialOutStopBits;

    @Value("${serial.out.parity}")
    private String serialOutParity;

    @Value("${serial.out.flowControl}")
    private String serialOutFlowControl;

    @Value("${serial.out.filters}")
    private String serialOutFilters;

    @Value("${tnc.use.files}")
    private boolean useFiles;

    @Value("${tnc.frameInterval}")
    private int frameInterval;

    @Value("${tnc.filePollInterval}")
    private int filePollInterval;

    @Value("${tnc.maxMessagesPerPoll}")
    private int maxMessagesPerPoll;

    public String getSerialInPort() {
        return serialInPort;
    }

    public String getSerialOutPort() {
        return serialOutPort;
    }

    public String getSerialInBaud() {
        return serialInBaud;
    }

    public String getSerialInDataBits() {
        return serialInDataBits;
    }

    public String getSerialInStopBits() {
        return serialInStopBits;
    }

    public String getSerialInParity() {
        return serialInParity;
    }

    public String getSerialInFlowControl() {
        return serialInFlowControl;
    }

    public String getSerialOutBaud() {
        return serialOutBaud;
    }

    public String getSerialOutDataBits() {
        return serialOutDataBits;
    }

    public String getSerialOutStopBits() {
        return serialOutStopBits;
    }

    public String getSerialOutParity() {
        return serialOutParity;
    }

    public String getSerialOutFlowControl() {
        return serialOutFlowControl;
    }

    public String getSerialInFilters() {
        return serialInFilters;
    }

    public String getSerialOutFilters() {
        return serialOutFilters;
    }

    public boolean useFiles() {
        return useFiles;
    }

    public int getFramesInterval() {
        return frameInterval;
    }

    public int getFilePollInterval() {
        return filePollInterval;
    }

    public int getMaxMessagesPerPoll() {
        return maxMessagesPerPoll;
    }

    public boolean isUplink() {
        return (getSerialOutPort() != null && getSerialOutPort().length() > 0);
    }

    public boolean isDownlink() {
        return (getSerialInPort() != null && getSerialInPort().length() > 0);
    }
}
