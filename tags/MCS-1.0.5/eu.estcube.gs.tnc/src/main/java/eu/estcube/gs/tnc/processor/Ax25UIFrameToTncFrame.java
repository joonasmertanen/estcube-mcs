/**
 *
 */
package eu.estcube.gs.tnc.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.TypeConversionException;
import org.apache.commons.lang3.StringUtils;
import org.hbird.exchange.constants.StandardArguments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.codec.ax25.impl.Ax25UIFrameKissEncoder;
import eu.estcube.common.ByteUtil;
import eu.estcube.common.Headers;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.domain.transport.tnc.TncFrame;

/**
 *
 */
@Component
public class Ax25UIFrameToTncFrame implements Processor {

    public static final String TYPE = "TNC";

    @Autowired
    private Ax25UIFrameKissEncoder encoder;

    /** @{inheritDoc . */
    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        out.copyFrom(in);

        Integer targetPort = null;
        try {
            targetPort = in.getHeader(Headers.TNC_PORT, Integer.class);
        } catch (TypeConversionException tce) {
            // ignore
        }

        String prefixHeaderValue = in.getHeader(Headers.TNC_PREFIX, String.class);
        byte[] prefix = null;
        if (StringUtils.isNotBlank(prefixHeaderValue)) {
            prefix = ByteUtil.toBytesFromHexString(prefixHeaderValue);
        }

        Ax25UIFrame ax25Frame = in.getBody(Ax25UIFrame.class);
        TncFrame tncFrame = encoder.encode(ax25Frame,
                targetPort == null ? Ax25UIFrameKissEncoder.DEFAULT_TNC_TARGET_PORT : targetPort, prefix);
        out.setBody(tncFrame);
        out.setHeader(StandardArguments.TYPE, TYPE);
    }
}
