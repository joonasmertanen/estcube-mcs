package eu.estcube.gs.tnc.codec;

import org.apache.commons.lang3.Validate;

import eu.estcube.domain.transport.tnc.TncFrame;
import eu.estcube.domain.transport.tnc.TncFrame.TncCommand;

/**
 * Helper methods to encode and decode {@link TncFrame}s.
 */
public class TncFrames {
    
    /** 
     * Converts {@link TncFrame} to byte array.
     * 
     * @param frame {@link TncFrame} to convert 
     */
    public static byte[] toBytes(TncFrame frame) {
        byte[] data = frame.getData();
        byte type = getType(frame.getCommand(), frame.getTarget());
        byte[] result = new byte[data.length + 1];
        result[0] = type;
        System.arraycopy(data, 0, result, 1, data.length);
        return result;
    }

    /**
     * Returns type byte for {@link TncFrame}.
     * 
     * http://www.ka9q.net/papers/kiss.html
     * 
     * @param command TncCommand to use in type byte
     * @param target target identifier to use in type byte
     * @return type byte for {@link TncFrame}
     */
    static byte getType(TncCommand command, int target) {
        Validate.isTrue(target >= TncFrame.TARGET_MIN_VALUE, "Minimum value for target is " + TncFrame.TARGET_MIN_VALUE);
        Validate.isTrue(target <= TncFrame.TARGET_MAX_VALUE, "Maximum value for target is " + TncFrame.TARGET_MAX_VALUE);
        // This type indicator byte is broken into two 4-bit nibbles so that the 
        // low-order nibble indicates the command number 
        // and the high-order nibble indicates the port number for that particular command.
        int tmp = ((byte)(target << 4)) | command.getValue();
        return (byte) tmp;
    }
    
    /**
     * Reads TncFrame from byte array.
     * 
     * @param bytes byte array to process
     * @return new {@link TncFrame} object
     */
    public static TncFrame toFrame(byte[] bytes) {
        byte type = bytes[0];
        TncCommand command = getCommand(type);
        int target = getTarget(type);
        byte data[] = new byte[bytes.length - 1];
        System.arraycopy(bytes, 1, data, 0, data.length);
        return new TncFrame(command, target, data);
    }
    
    /**
     * Returns {@link TncCommand} from type byte.
     * 
     * Reads command from lowest 4 bits in the type byte.
     * 
     * @param type type byte to process
     * @return
     */
    static TncCommand getCommand(byte type) {
        int tmp = type & 0xFF;
        byte b = tmp == 0xFF ? type : (byte) (type & 0x0F);
        return TncCommand.fromByte(b);
    }

    /**
     * Returns target identifier from type byte.
     * 
     * Reads target identifier from highest four bits in type byte.
     * Possible values are 0 - 15 or -1 (0xFF) in case of {@link TncCommand#RETURN}.
     * 
     * @param type type byte to process
     * @return target identifier from type byte
     */
    static byte getTarget(byte type) {
        int tmp = type & 0xFF;
        return tmp == 0xFF ? (byte) tmp : (byte)(tmp >>> 4);
    }
}
