package eu.estcube.gs.tnc.codec;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TncFendEncoderTest {
    
    private static final byte[] BYTES = new byte[] { 0x0C, 0x00, 0x0B, 0x0A };

    @Mock
    private IoSession session;
    
    @Mock
    private ProtocolEncoderOutput out;
    
    private TncFendEncoder encoder;
    
    private InOrder inOrder;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        encoder = new TncFendEncoder();
        inOrder = inOrder(session, out);
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFendEncoder#encode(org.apache.mina.core.session.IoSession, java.lang.Object, org.apache.mina.filter.codec.ProtocolEncoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testEncode() throws Exception {
        encoder.encode(session, BYTES, out);
        ArgumentCaptor<IoBuffer> captor = ArgumentCaptor.forClass(IoBuffer.class);
        inOrder.verify(out, times(1)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        IoBuffer buffer = captor.getValue();
        byte[] result = new byte[buffer.remaining()];
        buffer.get(result);
        assertEquals(BYTES.length + 2, result.length);
        assertEquals(TncConstants.FEND, result[0]);
        for (int i = 0; i < BYTES.length; i++) {
            assertEquals(BYTES[i], result[i + 1]);
        }
        assertEquals(TncConstants.FEND, result[result.length - 1]);
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFendEncoder#encode(org.apache.mina.core.session.IoSession, java.lang.Object, org.apache.mina.filter.codec.ProtocolEncoderOutput)}.
     * @throws Exception 
     */
    @Test(expected = RuntimeException.class)
    public void testEncodeWithWrongMessageType() throws Exception {
        encoder.encode(session, new Object(), out);
    }
}
