package eu.estcube.archiver.raw.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;

import eu.estcube.archiver.raw.FrameArchiver;
import eu.estcube.archiver.raw.sql.MessageToSqlFrameDataProvider;

public class FrameArchiverProcessor implements Processor {
	private FrameArchiver frameArchiver;
	private MessageToSqlFrameDataProvider messageToSqlFrameDataProvider;

	public FrameArchiverProcessor(FrameArchiver frameArchiver,
			MessageToSqlFrameDataProvider messageToSqlFrameDataProvider) {
		super();
		this.frameArchiver = frameArchiver;
		this.messageToSqlFrameDataProvider = messageToSqlFrameDataProvider;
	}

	public FrameArchiver getFrameArchiver() {
		return frameArchiver;
	}

	public MessageToSqlFrameDataProvider getMessageToSqlFrameDataProvider() {
		return messageToSqlFrameDataProvider;
	}

	@Override
	public void process(Exchange exchange) throws Exception {
		Message message = exchange.getIn();

		messageToSqlFrameDataProvider.setMessage(message);
		frameArchiver.store(messageToSqlFrameDataProvider);
	}
}
