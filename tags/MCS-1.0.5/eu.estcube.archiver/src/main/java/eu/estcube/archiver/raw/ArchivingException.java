package eu.estcube.archiver.raw;

public class ArchivingException extends Exception {
	private static final long serialVersionUID = -2808077791771105244L;

	public ArchivingException(String message, Throwable cause) {
		super(message, cause);
	}

	public ArchivingException(String message) {
		super(message);
	}

	public ArchivingException(Throwable cause) {
		super(cause);
	}

}
