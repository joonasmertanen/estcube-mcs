

Instance pannakse käima. 
Ette on antud satelliitide id-d millega tegeleb.

Satelliidi id-d oleks vaja unikaalsuse tagamiseks, et mitu instanci ei suhtleks ühe sateliidiga. 
Peaks olema süsteem, mis tagab ka confi vead - näiteks kui kasuaja paneb kahele instancile sama satelliidi.
Samas, kuidas teha fallbacki kui ühe instanci server kokku kukub?

Ground stationite id-sid on vaja?



FLOW
Tegelased:
instance A
instance B
GS1
GS2 (asub GS1 läheduses, kontakti ajad kattuvad, korraga ei saa suhelda ühe satelliidiga)
SAT1
SAT2

ID formaat: ground station : satellite : orbit number
- Tuleb prepare event GS1-SAT1-1
  --> Saadetakse välja queue kes tegeleb GS1-SAT1-1
  - A võtab vastu
- Tuleb start event GS1-SAT1-1
- Tuleb prepare event GS2-SAT1-2
  --> Saadetakse välja queue kes tegeleb GS2-SAT1-1
  - B võtab vastu
- Tuleb start event GS2-SAT1-2 - blocked, sest SAT1-ga juba suheldakse.
- Tuleb end event GS1-SAT1-1
- Tuleb end event GS2-SAT1-2



- Tuleb prepare event GS1-SAT1-1
- Tuleb start event GS1-SAT1-1
- Tuleb prepare event GS1-SAT2-2
- Tuleb start event GS1-SAT2-2 - blocked, sest GS1 on juba kasutusel. Priority satelliitidel???
- Tuleb end event GS1-SAT1-1
- Tuleb end event GS1-SAT2-2