package eu.estcube.commandautomation;

import org.apache.camel.builder.RouteBuilder;
import org.hbird.exchange.configurator.StandardEndpoints;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.core.BusinessCard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import eu.estcube.commandautomation.automator.Automator;
import eu.estcube.commandautomation.processor.AosEventProcessor;
import eu.estcube.commandautomation.processor.LosEventProcessor;
import eu.estcube.commandautomation.processor.PrepareEventProcessor;
import eu.estcube.common.PrepareForInjection;

@Component
public class Main extends RouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    @Autowired
    private Automator automator;

    @Autowired
    private Config config;

    @Autowired
    private PrepareForInjection preparator;

    @Autowired
    private PrepareEventProcessor prepareEventProcessor;

    @Autowired
    private AosEventProcessor aosEventProcessor;

    @Autowired
    private LosEventProcessor losEventProcessor;

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        LOG.info("Starting command automation");
        AbstractApplicationContext context = new ClassPathXmlApplicationContext("META-INF/spring/camel-context.xml");
        Main sca = context.getAutowireCapableBeanFactory().createBean(Main.class);

        org.apache.camel.spring.Main m = new org.apache.camel.spring.Main();
        m.setApplicationContext(context);
        m.addRouteBuilder(sca);
        m.run(args);
    }

    @Override
    public void configure() throws Exception {

        MDC.put(StandardArguments.ISSUED_BY, config.getServiceId());
        BusinessCard card = new BusinessCard(config.getServiceId(), config.getServiceName());
        card.setPeriod(config.getHeartBeatInterval());
        card.setDescription(String.format("Space Data Chain; version: %s", config.getServiceVersion()));

        from("timer://heartbeat?fixedRate=true&period=" + config.getHeartBeatInterval())
                .bean(card, "touch")
                .process(preparator)
                .to(StandardEndpoints.MONITORING);

        from(StandardEndpoints.COMMANDS) // Siit tuleb Track klassi object. Seal
                                         // igasugu head infot. Vaata edasi
                                         // LocationContactEvent
                .choice()
                .when(header("id").isEqualTo("/ESTCUBE/Events/PREPARE"))
                .process(prepareEventProcessor);

        from(StandardEndpoints.EVENTS) // org.hbird.exchange.core.Event.
                                       // applicableTo on kontakti ID. See osa
                                       // tõenäoliset muutub, sest ID-st on halb
                                       // võtta sat id-d ja gs id-d

                .choice()
                .when(header("id").isEqualTo("/ESTCUBE/Events/AOS"))
                .process(aosEventProcessor)
                .when(header("id").isEqualTo("/ESTCUBE/Events/LOS"))
                .process(losEventProcessor);

    }
}
