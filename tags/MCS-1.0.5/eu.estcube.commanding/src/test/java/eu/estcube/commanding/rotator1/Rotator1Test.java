package eu.estcube.commanding.rotator1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.commanding.analysis.OptimizationType;
import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.optimizators.Optimization;

public class Rotator1Test {
	Rotator1 rotator1 = new Rotator1(7.5);

	@Test
	public void crossElevationHighTest() {

		final OverPassType type = OverPassType.CROSS_ELEVATION_HIGH;
		final Optimization optimizationCase = rotator1.getOptimizer(type);
		assertEquals(OptimizationType.ROT_1_CROSS_EL_HIGH,
				optimizationCase.getOptType());
	}

	@Test
	public void crossElevationLowTest() {

		final OverPassType type = OverPassType.CROSS_ELEVATION_LOW;
		final Optimization optimizationCase = rotator1.getOptimizer(type);
		assertEquals(OptimizationType.ROT_1_CROSS_EL_LOW,
				optimizationCase.getOptType());
	}

	@Test
	public void noCrossElevationHighTest() {

		final OverPassType type = OverPassType.NO_CROSS_ELEVATION_HIGH;
		final Optimization optimizationCase = rotator1.getOptimizer(type);
		assertEquals(OptimizationType.ROT_1_NO_CROSS_EL_HIGH,
				optimizationCase.getOptType());
	}

	@Test
	public void noCrossElevationLowTest() {

		final OverPassType type = OverPassType.NO_CROSS_ELEVATION_LOW;
		final Optimization optimizationCase = rotator1.getOptimizer(type);
		assertEquals(OptimizationType.ROT_1_NO_CROSS_EL_LOW,
				optimizationCase.getOptType());
	}

}
