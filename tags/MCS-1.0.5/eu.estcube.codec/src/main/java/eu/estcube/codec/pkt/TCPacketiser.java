package eu.estcube.codec.pkt;

import eu.estcube.domain.transport.pkt.TCPacket;

/**
 * Creates a TC packet from the given payload (command).
 */
public interface TCPacketiser {

	/**
	 * Creates a TC packet from the given payload (command).
	 * 
	 * @param spid
	 *            The source packet identifier (0-65535).
	 * @param et
	 *            The execution time, should be given as 1/10 seconds since
	 *            01.01.1970 00:00 GMT.
	 * @return The data of the TC packet as source array.
	 */
	public TCPacket create(int spid, long et, byte[] payload);
}
