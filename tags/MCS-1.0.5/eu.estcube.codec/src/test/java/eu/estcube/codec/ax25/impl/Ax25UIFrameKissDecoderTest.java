/** 
 *
 */
package eu.estcube.codec.ax25.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.domain.transport.tnc.TncFrame;
import eu.estcube.domain.transport.tnc.TncFrame.TncCommand;

/**
 *
 */
public class Ax25UIFrameKissDecoderTest {

    private final byte[] destAddress = new byte[Ax25UIFrame.DEST_ADDR_LEN];
    private final byte[] srcAddress = new byte[Ax25UIFrame.DEST_ADDR_LEN];
    private byte ctrl;
    private byte pid;
    private byte[] info;

    private static final Random RND = new Random();

    private TncFrame tncFrame;

    private Ax25UIFrameKissDecoder decoder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        decoder = new Ax25UIFrameKissDecoder();
        RND.nextBytes(destAddress);
        RND.nextBytes(srcAddress);
        byte[] tmp = new byte[2];
        RND.nextBytes(tmp);
        ctrl = tmp[0];
        pid = tmp[0];
        info = new byte[RND.nextInt(1024 + 1)];
        RND.nextBytes(info);
        ByteBuffer buffer = ByteBuffer.allocate(destAddress.length + srcAddress.length + tmp.length + info.length);
        buffer.put(destAddress).put(srcAddress).put(ctrl).put(pid).put(info);
        buffer.flip();
        byte[] bytes = new byte[buffer.remaining()];
        buffer.get(bytes);
        tncFrame = new TncFrame(TncCommand.DATA, 0, bytes);
    }

    /**
     * Test method for
     * {@link eu.estcube.codec.ax25.impl.Ax25UIFrameKissDecoder#decode(eu.estcube.domain.transport.tnc.TncFrame)}
     * .
     */
    @Test
    public void testDecode() {
        Ax25UIFrame ax25Frame = decoder.decode(tncFrame);
        assertTrue(Arrays.equals(destAddress, ax25Frame.getDestAddr()));
        assertTrue(Arrays.equals(srcAddress, ax25Frame.getSrcAddr()));
        assertEquals(ctrl, ax25Frame.getCtrl());
        assertEquals(pid, ax25Frame.getPid());
        assertTrue(Arrays.equals(info, ax25Frame.getInfo()));
        assertNull(ax25Frame.getFcs());
        assertFalse(ax25Frame.isErrorFcs());
        assertFalse(ax25Frame.isErrorTooLong());
        assertFalse(ax25Frame.isErrorTooShort());
        assertFalse(ax25Frame.isErrorUnAligned());
        assertFalse(ax25Frame.isErrorUnstuffedBits());
    }

    @Test
    public void testDecodeNoBytes() {
        tncFrame = new TncFrame(TncCommand.DATA, 0, new byte[0]);
        Ax25UIFrame ax25Frame = decoder.decode(tncFrame);
        assertNull(ax25Frame.getDestAddr());
        assertNull(ax25Frame.getSrcAddr());
        assertEquals(0, ax25Frame.getCtrl());
        assertEquals(0, ax25Frame.getPid());
        assertNull(ax25Frame.getInfo());
        assertNull(ax25Frame.getFcs());
        assertFalse(ax25Frame.isErrorFcs());
        assertFalse(ax25Frame.isErrorTooLong());
        assertTrue(ax25Frame.isErrorTooShort());
        assertFalse(ax25Frame.isErrorUnAligned());
        assertFalse(ax25Frame.isErrorUnstuffedBits());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDecodeNullBytes() {
        tncFrame = new TncFrame(TncCommand.DATA, 0, null);
        decoder.decode(tncFrame);
    }

    @Test
    public void testDecodeDstAddressOnly() {
        tncFrame = new TncFrame(TncCommand.DATA, 0, destAddress);
        Ax25UIFrame ax25Frame = decoder.decode(tncFrame);
        assertTrue(Arrays.equals(destAddress, ax25Frame.getDestAddr()));
        assertNull(ax25Frame.getSrcAddr());
        assertEquals(0, ax25Frame.getCtrl());
        assertEquals(0, ax25Frame.getPid());
        assertNull(ax25Frame.getInfo());
        assertNull(ax25Frame.getFcs());
        assertFalse(ax25Frame.isErrorFcs());
        assertFalse(ax25Frame.isErrorTooLong());
        assertTrue(ax25Frame.isErrorTooShort());
        assertFalse(ax25Frame.isErrorUnAligned());
        assertFalse(ax25Frame.isErrorUnstuffedBits());
    }

    @Test
    public void testDecodeDstAddressSrcAddress() {
        ByteBuffer buffer = ByteBuffer.allocate(destAddress.length + srcAddress.length);
        buffer.put(destAddress);
        buffer.put(srcAddress);
        tncFrame = new TncFrame(TncCommand.DATA, 0, buffer.array());
        Ax25UIFrame ax25Frame = decoder.decode(tncFrame);
        assertTrue(Arrays.equals(destAddress, ax25Frame.getDestAddr()));
        assertTrue(Arrays.equals(srcAddress, ax25Frame.getSrcAddr()));
        assertEquals(0, ax25Frame.getCtrl());
        assertEquals(0, ax25Frame.getPid());
        assertNull(ax25Frame.getInfo());
        assertNull(ax25Frame.getFcs());
        assertFalse(ax25Frame.isErrorFcs());
        assertFalse(ax25Frame.isErrorTooLong());
        assertTrue(ax25Frame.isErrorTooShort());
        assertFalse(ax25Frame.isErrorUnAligned());
        assertFalse(ax25Frame.isErrorUnstuffedBits());
    }

    @Test
    public void testDecodeDstAddressSrcAddressCtrl() {
        ByteBuffer buffer = ByteBuffer.allocate(destAddress.length + srcAddress.length + 1);
        buffer.put(destAddress);
        buffer.put(srcAddress);
        buffer.put(ctrl);
        tncFrame = new TncFrame(TncCommand.DATA, 0, buffer.array());
        Ax25UIFrame ax25Frame = decoder.decode(tncFrame);
        assertTrue(Arrays.equals(destAddress, ax25Frame.getDestAddr()));
        assertTrue(Arrays.equals(srcAddress, ax25Frame.getSrcAddr()));
        assertEquals(ctrl, ax25Frame.getCtrl());
        assertEquals(0, ax25Frame.getPid());
        assertNull(ax25Frame.getInfo());
        assertNull(ax25Frame.getFcs());
        assertFalse(ax25Frame.isErrorFcs());
        assertFalse(ax25Frame.isErrorTooLong());
        assertTrue(ax25Frame.isErrorTooShort());
        assertFalse(ax25Frame.isErrorUnAligned());
        assertFalse(ax25Frame.isErrorUnstuffedBits());
    }

    @Test
    public void testDecodeHeaderOnly() {
        ByteBuffer buffer = ByteBuffer.allocate(destAddress.length + srcAddress.length + 1 + 1);
        buffer.put(destAddress);
        buffer.put(srcAddress);
        buffer.put(ctrl);
        buffer.put(pid);
        tncFrame = new TncFrame(TncCommand.DATA, 0, buffer.array());
        Ax25UIFrame ax25Frame = decoder.decode(tncFrame);
        assertTrue(Arrays.equals(destAddress, ax25Frame.getDestAddr()));
        assertTrue(Arrays.equals(srcAddress, ax25Frame.getSrcAddr()));
        assertEquals(ctrl, ax25Frame.getCtrl());
        assertEquals(pid, ax25Frame.getPid());
        assertTrue(Arrays.equals(new byte[0], ax25Frame.getInfo()));
        assertNull(ax25Frame.getFcs());
        assertFalse(ax25Frame.isErrorFcs());
        assertFalse(ax25Frame.isErrorTooLong());
        assertFalse(ax25Frame.isErrorTooShort());
        assertFalse(ax25Frame.isErrorUnAligned());
        assertFalse(ax25Frame.isErrorUnstuffedBits());
    }
}
