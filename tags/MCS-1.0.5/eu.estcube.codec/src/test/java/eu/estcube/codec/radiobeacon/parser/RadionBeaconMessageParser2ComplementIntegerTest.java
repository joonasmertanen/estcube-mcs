package eu.estcube.codec.radiobeacon.parser;

import static org.junit.Assert.assertEquals;

import org.hbird.exchange.core.Parameter;
import org.junit.Before;
import org.junit.Test;

public class RadionBeaconMessageParser2ComplementIntegerTest {

    private static final String ID = "ID";
    private static final String NAME = "NAME";

    private RadionBeaconMessageParser2ComplementInteger parser1;
    @SuppressWarnings("unused")
    private RadionBeaconMessageParser2ComplementInteger parser2;

    @Before
    public void setUp() throws Exception {
        parser1 = new RadionBeaconMessageParser2ComplementInteger(0, 2, new Parameter(ID, NAME));
        parser2 = new RadionBeaconMessageParser2ComplementInteger(0, 4, new Parameter(ID, NAME));
    }

    @Test
    public void parseTest() throws Exception {
        assertEquals(parser1.parse("NC"), "-100");
        assertEquals(parser1.parse("ZU"), "-126");
        assertEquals(parser1.parse("WB"), "27");
    }
}
