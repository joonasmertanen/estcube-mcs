/** 
 *
 */
package eu.estcube.codec.ax25.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import eu.estcube.common.ByteUtil;

/**
 *
 */
public class Ax25AddressDecoderTest {

    private Ax25AddressDecoder decoder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        decoder = new Ax25AddressDecoder();
    }

    @Test
    public void testToString() throws Exception {
        String[] input = {
                "8A A6 6A 8A 40 40 77",
                "8A A6 6A 8A 40 40 76",
                "8A A6 6A 8A 86 40 62",
                "96 70 9A 9A 9E 40 E0",
                "AE 84 68 94 8C 92 61",
                "8A A6 6A 8A 40 40 6D",
        };
        String[] output = {
                "ES5E-11",
                "ES5E-11",
                "ES5EC-1",
                "K8MMO-0",
                "WB4JFI0",
                "ES5E-6",
        };

        // String[] unknown = {
        // "17 F3 2C 3D E6 C6 96",
        // "24 E9 B6 E0 16 7A 58",
        // "0B 4D 3A D3 71 1D 18",
        // "22 EC 0B 25 5D D9 4E",
        // "9E B7 58 C6 B8 29 C5",
        // "08 46 70 61 63 CB 1C",
        // "8E C9 2F 68 89 50 18",
        // "7E ED AE 85 6D B0 C1",
        // "EA FA CD 63 CA 7A 0C",
        // "2C 86 F8 89 B3 E3 6B",
        // };

        for (int i = 0; i < input.length; i++) {
            byte[] bytes = ByteUtil.toBytesFromHexString(input[i]);
            assertEquals("Failed to decode input " + input[i], output[i], decoder.toString(bytes));
        }

        // for (String s : unknown) {
        // byte[] bytes = ByteUtil.toBytesFromHexString(s);
        // String result = decoder.toString(bytes);
        // System.out.printf("%s -> %s%n", s, result);
        // }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testToStringNotEnoughBytes() throws Exception {
        decoder.toString(new byte[Ax25AddressDecoder.NUMBER_OF_BYTES - 1]);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testToStringToManyBytes() throws Exception {
        decoder.toString(new byte[Ax25AddressDecoder.NUMBER_OF_BYTES + 1]);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testToStringNoBytes() throws Exception {
        decoder.toString(new byte[0]);
    }

    @Test(expected = NullPointerException.class)
    public void testToStringNull() throws Exception {
        decoder.toString(null);
    }
}
