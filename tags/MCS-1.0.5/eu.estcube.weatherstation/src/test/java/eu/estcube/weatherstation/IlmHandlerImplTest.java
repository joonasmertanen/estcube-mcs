package eu.estcube.weatherstation;

import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_PRESSURE;
import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_PRESSURE_TREND;
import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_TEMPERATURE;
import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_TEMPERATURE_TREND;
import static eu.estcube.weatherstation.DefaultWeatherParameters.PHENOMENON;
import static eu.estcube.weatherstation.DefaultWeatherParameters.RELATIVE_HUMIDITY;
import static eu.estcube.weatherstation.DefaultWeatherParameters.RELATIVE_HUMIDITY_TREND;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WIND_DIRECTION;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WIND_SPEED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.hbird.business.api.IdBuilder;
import org.hbird.business.api.impl.DefaultIdBuilder;
import org.hbird.exchange.core.Label;
import org.hbird.exchange.core.Parameter;
import org.hbird.exchange.interfaces.IApplicableTo;
import org.hbird.exchange.interfaces.IEntityInstance;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

public class IlmHandlerImplTest {

    private static final String SERVICE_ID = "/TEST/WeatherStation";
    private static final String PARAMETER_NAMESPACE = "/TEST/WeaterStatsions";

    private IlmHandlerImpl handler;
    private List<IEntityInstance> weatherParams;
    private IdBuilder idBuilder;

    @Before
    public void setUp() throws Exception {
        SAXBuilder builder = new SAXBuilder();
        Document document = builder.build(getClass().getResourceAsStream("/ilm.ee.xml"));
        idBuilder = new DefaultIdBuilder();
        handler = new IlmHandlerImpl(idBuilder, SERVICE_ID, PARAMETER_NAMESPACE);
        weatherParams = handler.parseDocument(document);
    }

    /**
     * Test method for {@link IlmPars#getResult()}.
     * 
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws JDOMException
     */
    @Test
    public final void testParameters() {

        String applicableTo = idBuilder.buildID(PARAMETER_NAMESPACE, handler.getDataSourceName());

        assertEquals(9, weatherParams.size());

        assertNotNull(weatherParams.get(0).getTimestamp());
        assertEquals(SERVICE_ID, weatherParams.get(0).getIssuedBy());
        assertEquals(AIR_TEMPERATURE.getName(), weatherParams.get(0).getName());
        assertEquals(AIR_TEMPERATURE.getDescription(), weatherParams.get(0).getDescription());
        assertEquals(AIR_TEMPERATURE.getUnit(), ((Parameter) weatherParams.get(0)).getUnit());
        assertEquals(22.0D, ((Parameter) weatherParams.get(0)).getValue());
        assertEquals(applicableTo, ((IApplicableTo) weatherParams.get(0)).getApplicableTo());
        assertEquals(idBuilder.buildID(applicableTo, AIR_TEMPERATURE.getName()), weatherParams.get(0).getID());

        assertNotNull(weatherParams.get(1).getTimestamp());
        assertEquals(SERVICE_ID, weatherParams.get(1).getIssuedBy());
        assertEquals(AIR_TEMPERATURE_TREND.getName(), weatherParams.get(1).getName());
        assertEquals(AIR_TEMPERATURE_TREND.getDescription(), weatherParams.get(1).getDescription());
        assertEquals("up", ((Label) weatherParams.get(1)).getValue());
        assertEquals(applicableTo, ((IApplicableTo) weatherParams.get(1)).getApplicableTo());
        assertEquals(idBuilder.buildID(applicableTo, AIR_TEMPERATURE_TREND.getName()), weatherParams.get(1).getID());

        assertNotNull(weatherParams.get(2).getTimestamp());
        assertEquals(SERVICE_ID, weatherParams.get(2).getIssuedBy());
        assertEquals(AIR_PRESSURE.getName(), weatherParams.get(2).getName());
        assertEquals(AIR_PRESSURE.getDescription(), weatherParams.get(2).getDescription());
        assertEquals(AIR_PRESSURE.getUnit(), ((Parameter) weatherParams.get(2)).getUnit());
        assertEquals(758.0D, ((Parameter) weatherParams.get(2)).getValue());
        assertEquals(applicableTo, ((IApplicableTo) weatherParams.get(2)).getApplicableTo());
        assertEquals(idBuilder.buildID(applicableTo, AIR_PRESSURE.getName()), weatherParams.get(2).getID());

        assertNotNull(weatherParams.get(3).getTimestamp());
        assertEquals(SERVICE_ID, weatherParams.get(3).getIssuedBy());
        assertEquals(AIR_PRESSURE_TREND.getName(), weatherParams.get(3).getName());
        assertEquals(AIR_PRESSURE_TREND.getDescription(), weatherParams.get(3).getDescription());
        assertEquals("down", ((Label) weatherParams.get(3)).getValue());
        assertEquals(applicableTo, ((IApplicableTo) weatherParams.get(3)).getApplicableTo());
        assertEquals(idBuilder.buildID(applicableTo, AIR_PRESSURE_TREND.getName()), weatherParams.get(3).getID());

        assertNotNull(weatherParams.get(4).getTimestamp());
        assertEquals(SERVICE_ID, weatherParams.get(4).getIssuedBy());
        assertEquals(RELATIVE_HUMIDITY.getName(), weatherParams.get(4).getName());
        assertEquals(RELATIVE_HUMIDITY.getDescription(), weatherParams.get(4).getDescription());
        assertEquals(RELATIVE_HUMIDITY.getUnit(), ((Parameter) weatherParams.get(4)).getUnit());
        assertEquals(67.0D, ((Parameter) weatherParams.get(4)).getValue());
        assertEquals(applicableTo, ((IApplicableTo) weatherParams.get(4)).getApplicableTo());
        assertEquals(idBuilder.buildID(applicableTo, RELATIVE_HUMIDITY.getName()), weatherParams.get(4).getID());

        assertNotNull(weatherParams.get(5).getTimestamp());
        assertEquals(SERVICE_ID, weatherParams.get(5).getIssuedBy());
        assertEquals(RELATIVE_HUMIDITY_TREND.getName(), weatherParams.get(5).getName());
        assertEquals(RELATIVE_HUMIDITY_TREND.getDescription(), weatherParams.get(5).getDescription());
        assertEquals("up", ((Label) weatherParams.get(5)).getValue());
        assertEquals(applicableTo, ((IApplicableTo) weatherParams.get(5)).getApplicableTo());
        assertEquals(idBuilder.buildID(applicableTo, RELATIVE_HUMIDITY_TREND.getName()), weatherParams.get(5).getID());

        assertNotNull(weatherParams.get(6).getTimestamp());
        assertEquals(SERVICE_ID, weatherParams.get(6).getIssuedBy());
        assertEquals(WIND_SPEED.getName(), weatherParams.get(6).getName());
        assertEquals(WIND_SPEED.getDescription(), weatherParams.get(6).getDescription());
        assertEquals(WIND_SPEED.getUnit(), ((Parameter) weatherParams.get(6)).getUnit());
        assertEquals(2.2D, ((Parameter) weatherParams.get(6)).getValue());
        assertEquals(applicableTo, ((IApplicableTo) weatherParams.get(6)).getApplicableTo());
        assertEquals(idBuilder.buildID(applicableTo, WIND_SPEED.getName()), weatherParams.get(6).getID());

        assertNotNull(weatherParams.get(7).getTimestamp());
        assertEquals(SERVICE_ID, weatherParams.get(7).getIssuedBy());
        assertEquals(WIND_DIRECTION.getName(), weatherParams.get(7).getName());
        assertEquals(WIND_DIRECTION.getDescription(), weatherParams.get(7).getDescription());
        assertEquals("S", ((Label) weatherParams.get(7)).getValue());
        assertEquals(applicableTo, ((IApplicableTo) weatherParams.get(7)).getApplicableTo());
        assertEquals(idBuilder.buildID(applicableTo, WIND_DIRECTION.getName()), weatherParams.get(7).getID());

        assertNotNull(weatherParams.get(8).getTimestamp());
        assertEquals(SERVICE_ID, weatherParams.get(8).getIssuedBy());
        assertEquals(PHENOMENON.getName(), weatherParams.get(8).getName());
        assertEquals(PHENOMENON.getDescription(), weatherParams.get(8).getDescription());
        assertEquals("vihm", ((Label) weatherParams.get(8)).getValue());
        assertEquals(applicableTo, ((IApplicableTo) weatherParams.get(8)).getApplicableTo());
        assertEquals(idBuilder.buildID(applicableTo, PHENOMENON.getName()), weatherParams.get(8).getID());
    }
}
