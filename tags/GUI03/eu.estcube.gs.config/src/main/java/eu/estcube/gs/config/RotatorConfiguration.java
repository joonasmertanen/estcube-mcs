package eu.estcube.gs.config;

import org.springframework.beans.factory.annotation.Value;

public class RotatorConfiguration extends Configuration {
	
	public String getDriverType() {
		return driverType;
	}
	
	public int getRotatorPort() {
		return rotatorPort;
	}

	public String getRotatorHost() {
		return rotatorHost;
	}

	public String getRotatorAddress() {
		return rotatorAddress;
	}
	
	public String getTimerFireInterval() {
		return timerFireInterval;
	}

	public int getPollDelayInterval() {
		return pollDelayInterval;
	}

	public int getPollRoundingScale() {
		return pollRoundingScale;
	}
	
	@Value("${driverType}")
	private String driverType;
	
	@Value("${rotatorPort}")
	private int rotatorPort;

	@Value("${rotatorHost}")
	private String rotatorHost;

	@Value("${rotatorAddress}")
	private String rotatorAddress;
	
	@Value("${timerFireInterval}")
	private String timerFireInterval;

	@Value("${pollDelayInterval}")
	private int pollDelayInterval;

	@Value("${pollRoundScale}")
	private int pollRoundingScale;
}
