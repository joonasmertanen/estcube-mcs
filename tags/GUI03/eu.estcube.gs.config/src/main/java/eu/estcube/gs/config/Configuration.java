package eu.estcube.gs.config;

import java.util.Map;

import org.apache.camel.Endpoint;
import org.apache.camel.impl.DefaultComponent;
import org.springframework.beans.factory.annotation.Value;

public class Configuration extends DefaultComponent {

	public String getJmsUrl() {
		return jmsUrl;
	}

	public String getGroundStationName() {
		return groundStationName;
	}

	public String getDriverVersion() {
		return driverVersion;
	}

	@Override
	protected Endpoint createEndpoint(String arg0, String arg1,
			Map<String, Object> arg2) {
		return null;
	}

	@Value("${jms.url}")
	private String jmsUrl;

	@Value("${gsName}")
	private String groundStationName;

	@Value("${version}")
	private String driverVersion;
}
