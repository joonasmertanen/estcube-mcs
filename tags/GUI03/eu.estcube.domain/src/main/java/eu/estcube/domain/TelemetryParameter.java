/**
 * 
 */
package eu.estcube.domain;

import java.io.Serializable;
import org.hbird.exchange.core.Parameter;

public class TelemetryParameter extends Parameter implements Serializable {

    private static final long serialVersionUID = 1340205089337640173L;

    public TelemetryParameter(String name) {
    	super(null, name, null, null, null);
    }

    public TelemetryParameter(String name, Object value) {
    	super(null, name, null, value, null);
    }
}
