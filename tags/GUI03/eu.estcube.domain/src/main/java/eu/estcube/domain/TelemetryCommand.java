/** 
 *
 */
package eu.estcube.domain;

import java.io.Serializable;
import java.util.List;

import org.hbird.exchange.core.Command;
import org.hbird.exchange.core.Parameter;

public class TelemetryCommand extends Command implements Serializable {

    private static final long serialVersionUID = 2092380317315772625L;
    private String commandName;
    private String device;

    public TelemetryCommand(String name) {
        super(null, name, null);
        this.commandName = name;
    }

    public TelemetryCommand(String name, List<Parameter> params) {
        super(null, name, null);
        this.commandName = name;
        setArguments(params);
    }

    public String getCommandName() {
        return commandName;
    }

    public List<Parameter> getParams() {
        return getArguments();
    }

    public void addParameter(Parameter parameter) {
        List<Parameter> params = getArguments();
        params.add(parameter);
        setArguments(params);
    }

    public Object getParameter(String name) {
        Parameter parToGet = null;
        for (Parameter par : getArguments()) {
            if (par.getName().equals(name)) {
                parToGet = par;
                break;
            }
        }
        return parToGet != null ? parToGet.getValue() : null;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }
}
