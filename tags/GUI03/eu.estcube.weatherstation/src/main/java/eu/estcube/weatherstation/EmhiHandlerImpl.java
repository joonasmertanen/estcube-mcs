package eu.estcube.weatherstation;

import java.util.ArrayList;
import java.util.List;

import org.hbird.exchange.core.Parameter;
import org.jdom2.Document;
import org.jdom2.Element;

public class EmhiHandlerImpl extends AbstractDataHandler {

	protected String getDataSourceName() {
		return "emhi.ee";
	}

	protected  List<Parameter> parseDocument(Document document){

		List<Parameter> weatherParams = new ArrayList <Parameter>();
		Element rootNode = document.getRootElement();		
		List<Element> list = rootNode.getChildren("station");
		
		
		for(int u = 0; u < list.size(); u++){
			Element node = (Element) list.get(u);
			if(node.getChildText("wmocode").equals("26242")) {
				weatherParams.add(new Parameter(getDataSourceName(),"phenomen","phenomen description",node.getChildText("phenomenon"),""));
				weatherParams.add(new Parameter(getDataSourceName(),"visibility","visibility description",node.getChildText("visibility"),""));
				weatherParams.add(new Parameter(getDataSourceName(),"precipitations","precipitations description",node.getChildText("precipitations"),""));
				weatherParams.add(new Parameter(getDataSourceName(),"airpressure","airpressure description",node.getChildText("airpressure"),""));
				weatherParams.add(new Parameter(getDataSourceName(),"relativehumidity","relativehumidity description",node.getChildText("relativehumidity"),""));
				weatherParams.add(new Parameter(getDataSourceName(),"airtemperature","airtemperature description",node.getChildText("airtemperature"),""));
				weatherParams.add(new Parameter(getDataSourceName(),"winddirection","winddirection description",node.getChildText("winddirection"),""));
				weatherParams.add(new Parameter(getDataSourceName(),"windspeed","windspeed description",node.getChildText("windspeed"),""));
				weatherParams.add(new Parameter(getDataSourceName(),"windspeedmax","windspeedmax description",node.getChildText("windspeedmax"),""));
			
			}
		}
		
		return weatherParams;
		
	}
}
