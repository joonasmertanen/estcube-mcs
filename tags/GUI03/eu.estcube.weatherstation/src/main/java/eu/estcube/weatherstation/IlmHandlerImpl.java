package eu.estcube.weatherstation;

import java.util.ArrayList;
import java.util.List;

import org.hbird.exchange.core.Parameter;
import org.jdom2.Document;
import org.jdom2.Element;

public class IlmHandlerImpl extends AbstractDataHandler {
	
	protected String getDataSourceName(){
		return "ilm.ee";
	}
	
	protected List<Parameter> parseDocument(Document document){
		
		Element rootNode = document.getRootElement();
		Element rubriik = rootNode.getChild("rubriik");
		Element sisestus = rubriik.getChild("sisestus");
		List<Element> list = sisestus.getChildren("blokk");
		
		List<Parameter> weatherParams = new ArrayList <Parameter>();
		
		for(int u = 0; u < list.size(); u++){
			Element node = (Element) list.get(u);	
			
			weatherParams.add(new Parameter(getDataSourceName(),"temp","temp description",node.getChildText("temp"),""));
			weatherParams.add(new Parameter(getDataSourceName(),"temptrend","temptrend description",node.getChildText("temptrend"),""));
			weatherParams.add(new Parameter(getDataSourceName(),"rohk","rohk description",node.getChildText("rohk"),""));
			weatherParams.add(new Parameter(getDataSourceName(),"rohktrend","rohktrend description",node.getChildText("rohktrend"),""));
			weatherParams.add(new Parameter(getDataSourceName(),"niiskus","niiskus description",node.getChildText("niiskus"),""));
			weatherParams.add(new Parameter(getDataSourceName(),"niiskustrend","niiskustrend description",node.getChildText("niiskustrend"),""));
			weatherParams.add(new Parameter(getDataSourceName(),"tuul","tuul description",node.getChildText("tuul"),""));
			weatherParams.add(new Parameter(getDataSourceName(),"suund","suund description",node.getChildText("suund"),""));
			weatherParams.add(new Parameter(getDataSourceName(),"nahtus","nahtus description",node.getChildText("nahtus"),""));		
		}
		return weatherParams;
	}
}
