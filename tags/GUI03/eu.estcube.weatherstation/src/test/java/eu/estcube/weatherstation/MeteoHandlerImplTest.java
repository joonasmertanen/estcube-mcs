package eu.estcube.weatherstation;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.hbird.exchange.core.Parameter;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.junit.Before;
import org.junit.Test;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

public class MeteoHandlerImplTest {

  @Before
  public void setUp() throws Exception {
  }

  /**
   * Test method for {@link IlmPars#getResult()}.
   * 
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws IOException
   * @throws JDOMException
   */


  @Test
  public final void testParameters() throws ParserConfigurationException, SAXException, IOException, JDOMException {

    SAXBuilder builder = new SAXBuilder();
    Document document = builder.build(getClass().getResourceAsStream("/meteo.physics.xml"));

    MeteoHandlerImpl test = new MeteoHandlerImpl();
    List<Parameter> weatherParams = test.parseDocument(document);

    assertEquals(8, weatherParams.size());

    assertEquals("temp", weatherParams.get(0).getName());
    assertEquals("temp description", weatherParams.get(0).getDescription());
    assertEquals("21.6466262090116", weatherParams.get(0).getValue());
    assertEquals("", weatherParams.get(0).getUnit());
    assertEquals("meteo.physic.ut.ee", weatherParams.get(0).getIssuedBy());

    assertEquals("humid", weatherParams.get(1).getName());
    assertEquals("humid description", weatherParams.get(1).getDescription());
    assertEquals("70.0695341688312", weatherParams.get(1).getValue());
    assertEquals("", weatherParams.get(1).getUnit());
    assertEquals("meteo.physic.ut.ee", weatherParams.get(0).getIssuedBy());

    assertEquals("wind_dir", weatherParams.get(2).getName());
    assertEquals("wind_dir description", weatherParams.get(2).getDescription());
    assertEquals("-", weatherParams.get(2).getValue());
    assertEquals("", weatherParams.get(2).getUnit());
    assertEquals("meteo.physic.ut.ee", weatherParams.get(0).getIssuedBy());

    assertEquals("wind_len", weatherParams.get(3).getName());
    assertEquals("wind_len description", weatherParams.get(3).getDescription());
    assertEquals("-", weatherParams.get(3).getValue());
    assertEquals("", weatherParams.get(3).getUnit());
    assertEquals("meteo.physic.ut.ee", weatherParams.get(0).getIssuedBy());

    assertEquals("lux", weatherParams.get(4).getName());
    assertEquals("lux description", weatherParams.get(4).getDescription());
    assertEquals("10056.0792521323", weatherParams.get(4).getValue());
    assertEquals("", weatherParams.get(4).getUnit());
    assertEquals("meteo.physic.ut.ee", weatherParams.get(0).getIssuedBy());

    assertEquals("sole", weatherParams.get(5).getName());
    assertEquals("sole description", weatherParams.get(5).getDescription());
    assertEquals("-", weatherParams.get(5).getValue());
    assertEquals("", weatherParams.get(5).getUnit());
    assertEquals("meteo.physic.ut.ee", weatherParams.get(0).getIssuedBy());

    assertEquals("gamma", weatherParams.get(6).getName());
    assertEquals("gamma description", weatherParams.get(6).getDescription());
    assertEquals("0.0675110184290355", weatherParams.get(6).getValue());
    assertEquals("", weatherParams.get(6).getUnit());
    assertEquals("meteo.physic.ut.ee", weatherParams.get(0).getIssuedBy());

    assertEquals("precip", weatherParams.get(7).getName());
    assertEquals("precip description", weatherParams.get(7).getDescription());
    assertEquals("0", weatherParams.get(7).getValue());
    assertEquals("", weatherParams.get(7).getUnit());
    assertEquals("meteo.physic.ut.ee", weatherParams.get(0).getIssuedBy());

  }

}
