#!/bin/bash

function killjava()
{
  kill `ps -eo pid,command | grep java | grep $1 | sed -e 's/^\s*//' | cut -f 1 '--delimiter= '` 2>/dev/null
}

killjava apache-activemq
sleep 5
/home/mcs-test/apache-activemq-5.6.0/bin/activemq start xbean:/home/mcs-test/estcube/gs.rotator/tests/amq-conf.xml
sleep 5

cd /home/mcs-test/estcube/gs.rotator/tests

killjava gs.listen
sleep 5
OUTFILE=`tempfile`
java -jar gs.listen-0.0.1-SNAPSHOT-jar-with-dependencies.jar | tee $OUTFILE &
sleep 10

SENDCMD='python send.py'
$SENDCMD P 10 10
sleep 5
$SENDCMD +p
$SENDCMD 1
$SENDCMD abcd
$SENDCMD S
sleep 5
killjava gs.listen

diff expectedTestOutput $OUTFILE
RESULT=$?
rm $OUTFILE

killjava apache-activemq
sleep 5
/home/mcs-test/apache-activemq-5.6.0/bin/activemq start

exit $RESULT
