#!/bin/sh

#kill `ps -eo pid,command | grep java | grep activemq | cut -f 1 '--delimiter= '`
#/home/mcs-test/apache-activemq-5.6.0/bin/run.jar start xbean:/home/mcs-test/gs.rotator/bin/amq-conf.xml

kill `ps -eo pid,command | grep java | grep gs.listen | cut -f 1 '--delimiter= '` 2>/dev/null

#cd /home/mcs-test/gs.rotator/bin

OUTFILE=`tempfile`
java -jar gs.listen-0.0.1-SNAPSHOT-jar-with-dependencies.jar >$OUTFILE 2>/dev/null &
LISTENPID=$!
sleep 5

SENDCMD='python tests/send.py'
$SENDCMD P 10 10
sleep 5
$SENDCMD +p
$SENDCMD 1
$SENDCMD abcd
$SENDCMD S
sleep 5
kill $LISTENPID

diff expectedTestOutput $OUTFILE
RESULT=$?
rm $OUTFILE

#kill `ps -eo pid,command | grep java | grep activemq | cut -f 1 '--delimiter= '`
#/home/mcs-test/apache-activemq-5.6.0/bin/run.jar start

exit $RESULT
