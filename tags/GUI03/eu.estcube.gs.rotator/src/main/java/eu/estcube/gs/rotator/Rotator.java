package eu.estcube.gs.rotator;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.util.Properties;

import org.apache.camel.CamelExchangeException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import eu.estcube.domain.JMSConstants;
import eu.estcube.gs.config.RotatorConfiguration;
import eu.estcube.gs.hamlib.DeviceType;
import eu.estcube.gs.hamlib.HamlibIO;

public class Rotator extends RouteBuilder {

    private static final String CAPABILITIES_COMMAND = "1";
    private static final Logger LOG = LoggerFactory.getLogger(Rotator.class);

    @Autowired
    private SplitterRotatorStatus rotatorStatusSplitter;
    private boolean online = true;

    @Value("#{config}")
    private RotatorConfiguration config;

    @Override
    public void configure() {
        conf();
        test();
    }

    @SuppressWarnings("unchecked")
    private void conf() {

        final Properties properties = new Properties();
        try {
            FileInputStream in = new FileInputStream("src/main/resources/metainfo.json");
            properties.load(in);
            for (String key : properties.stringPropertyNames()) {
                String value = properties.getProperty(key);
                LOG.info(key + " => " + value);
            }

        } catch (IOException e) {
            LOG.info("Exception reading metainfo.json");
            LOG.info(e.toString());
        }

        // Outgoing to AMQ metadata topic
        from("timer://metaTimer?period=10000000").process(new Processor() {

            @Override
            public void process(Exchange exchange) throws Exception {
                exchange.getOut().setBody(properties);

            }
        }).log("log:metainfo").to(JMSConstants.AMQ_GS_META_SEND);

//        // Incoming from AMQ
//        from(JMSConstants.AMQ_GS_RECEIVE).log("log: Received message in AMQ queue")/*.choice()
//            .when(header(JMSConstants.HEADER_GROUNDSTATIONID).isEqualTo(config.getGroundStationName()))*/
//            .doTry().to(JMSConstants.DIRECT_ROT_CTLD).recipientList(header(JMSConstants.HEADER_FORWARD)).endDoTry()
//            .doCatch(IOException.class, Exception.class, CamelExchangeException.class, ClosedChannelException.class)
//            .log("AMQ receive -> rotator: failed").end();
////            .when(header(JMSConstants.HEADER_GROUNDSTATIONID).isEqualTo(JMSConstants.GS_ALL_NAMES))
////            .to(JMSConstants.DIRECT_ROT_CTLD).otherwise().log("log:WrongStation");
        
        // Incoming from AMQ
        from(JMSConstants.AMQ_GS_RECEIVE).log("log: Received message in AMQ queue").choice()
            .when(header(JMSConstants.HEADER_GROUNDSTATIONID).isEqualTo(config.getGroundStationName()))
                .to(JMSConstants.DIRECT_ROT_CTLD_INTERMEDIATE)
            .when(header(JMSConstants.HEADER_GROUNDSTATIONID).isEqualTo(JMSConstants.GS_ALL_NAMES))
                .to(JMSConstants.DIRECT_ROT_CTLD_INTERMEDIATE)
            .otherwise().log("log:WrongStation");
        
        // From AMQ to rotctld
        from(JMSConstants.DIRECT_ROT_CTLD_INTERMEDIATE).log("log: Sending from intermediate to rotator")
            .doTry().to(JMSConstants.DIRECT_ROT_CTLD).recipientList(header(JMSConstants.HEADER_FORWARD)).endDoTry()
            .doCatch(IOException.class, Exception.class, CamelExchangeException.class, ClosedChannelException.class)
            .log("Log: Sending message from the AMQ receiving queue to the rotator driver failed").end();
        
        // Outgoing to AMQ
        from(JMSConstants.DIRECT_SEND)
        .log("Log: Sending message from " + JMSConstants.DIRECT_SEND + " to " + JMSConstants.AMQ_GS_SEND)
        .to(JMSConstants.AMQ_GS_SEND);

        // Outgoing to the rotator
        if (config.getDriverType().equals("hamlib")) {
            
            String nettyRotctld = HamlibIO.getDeviceDriverUrl(DeviceType.ROTATOR, config);
            from(JMSConstants.DIRECT_ROT_CTLD).log("log: Sending message to rotctld")
            .doTry()
            .inOut(nettyRotctld)
                .process(new Processor() {
                    public void process(Exchange exchange) {
                        
                        // Designate an endpoint to which to forward the response from Hamlib.
                        // To enable forwarding, JMSConstants.HEADER_FORWARD needs to be specified
                        // as the recipientList() when sending a message to JMSConstants.DIRECT_ROT_CTLD.
                        // See other routes in this file for examples on how to do that.
                        String forwardRoute = JMSConstants.DIRECT_SEND;
                            // TODO: Send status?
//                            if (online) {
//                                forwardRoute += "," + JMSConstants.DIRECT_ROT_STATUS;
//                                online = false;
//                            }
                        exchange.getIn().setHeader(JMSConstants.HEADER_FORWARD, forwardRoute);

                        exchange.getIn().setHeader(JMSConstants.HEADER_GROUNDSTATIONID, config.getGroundStationName());
                        exchange.getIn().setHeader(JMSConstants.HEADER_DEVICE, JMSConstants.GS_ROT_CTLD);
                    }
                })
            .doCatch(IOException.class, Exception.class, CamelExchangeException.class, ClosedChannelException.class)
                .process(new Processor() {
                    public void process(Exchange exchange) {
                        online = true;
                    }
                })
            .end();
            
        } else {
            throw new UnsupportedOperationException("The driver type you have specified in the " +
            		"driver.properties file in unsupported.");
        }

//        // Periodically check if rotator online and get capabilities
//        from("timer://rotTimer?period=" + config.getTimerFireInterval()).process(new Processor() {
//            public void process(Exchange exchange) {
//                exchange.getIn().setBody(CAPABILITIES_COMMAND);
//            }
//        }).log("log:DebugRotPeriodicalTimeFire").doTry().to(JMSConstants.DIRECT_ROT_CTLD)
//            .recipientList(header(JMSConstants.HEADER_FORWARD)).endDoTry()
//            .doCatch(IOException.class, Exception.class, CamelExchangeException.class, ClosedChannelException.class)
//            .log("Timerfire failed, probably channel closed").end();
    }

    @SuppressWarnings("unchecked")
    private void test() {
        
        // Send the message in process() when the user enters input (the input itself is discarded) 
        from("stream:in").process(new Processor() {
            public void process(Exchange exchange) {

                exchange.getIn().setHeader(JMSConstants.HEADER_DEVICE, JMSConstants.GS_ROT_CTLD);
                exchange.getIn().setBody("+p");
                
            }
        }).log("log: Sending message triggered by user input").doTry().to(JMSConstants.DIRECT_ROT_CTLD)
            .recipientList(header(JMSConstants.HEADER_FORWARD)).endDoTry()
            .doCatch(IOException.class, Exception.class, CamelExchangeException.class, ClosedChannelException.class)
            .log("Sending of input failed").end();
        ;

        from(JMSConstants.AMQ_VERSIONS_REQUEST).process(new Processor() {
            public void process(Exchange ex) {
                ex.getOut().setHeader(JMSConstants.HEADER_GROUNDSTATIONID, config.getGroundStationName());
                ex.getOut().setHeader(JMSConstants.HEADER_COMPONENT, JMSConstants.COMPONENT_ROTATOR);
                ex.getOut().setBody(config.getDriverVersion(), String.class);
            }
        }).to(JMSConstants.AMQ_VERSIONS_RECEIVE);

        from("timer://sendVersion?repeatCount=1").process(new Processor() {
            public void process(Exchange ex) {
                ex.getOut().setHeader(JMSConstants.HEADER_GROUNDSTATIONID, config.getGroundStationName());
                ex.getOut().setHeader(JMSConstants.HEADER_COMPONENT, JMSConstants.COMPONENT_ROTATOR);
                ex.getOut().setBody(config.getDriverVersion(), String.class);
            }
        }).to(JMSConstants.AMQ_VERSIONS_RECEIVE);
    }

    public static void main(String[] args) throws Exception {
        LOG.info("Starting driver");

        new Main().run(args);
    }
}
