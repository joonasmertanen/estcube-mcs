package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class AzimuthShortPath2LongPath implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand command) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+A ");
        messageString.append(command.getParameter("Short Path Deg"));
        messageString.append("\n");
        return messageString;
    }
}
