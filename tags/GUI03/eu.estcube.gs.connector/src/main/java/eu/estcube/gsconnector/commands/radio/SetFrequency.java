package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SetFrequency implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand command) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+F ");
        messageString.append(command.getParameter("Frequency"));
        messageString.append("\n");
        return messageString;
    }
}
