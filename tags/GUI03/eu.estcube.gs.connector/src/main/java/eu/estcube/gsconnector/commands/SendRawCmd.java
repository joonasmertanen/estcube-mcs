package eu.estcube.gsconnector.commands;

import eu.estcube.domain.TelemetryCommand;

public class SendRawCmd implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand command) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+w ")
            .append(command.getParameter("Cmd"))
            .append("\n");
        return messageString;
    }
}
