package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SetPosition implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand command) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+P ");
        messageString.append(command.getParameter("Azimuth"));
        messageString.append(" ");
        messageString.append(command.getParameter("Elevation"));
        messageString.append("\n");
        return messageString;
    }
}
