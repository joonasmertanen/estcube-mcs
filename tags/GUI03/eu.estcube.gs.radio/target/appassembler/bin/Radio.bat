@REM ----------------------------------------------------------------------------
@REM  Copyright 2001-2006 The Apache Software Foundation.
@REM
@REM  Licensed under the Apache License, Version 2.0 (the "License");
@REM  you may not use this file except in compliance with the License.
@REM  You may obtain a copy of the License at
@REM
@REM       http://www.apache.org/licenses/LICENSE-2.0
@REM
@REM  Unless required by applicable law or agreed to in writing, software
@REM  distributed under the License is distributed on an "AS IS" BASIS,
@REM  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@REM  See the License for the specific language governing permissions and
@REM  limitations under the License.
@REM ----------------------------------------------------------------------------
@REM
@REM   Copyright (c) 2001-2006 The Apache Software Foundation.  All rights
@REM   reserved.

@echo off

set ERROR_CODE=0

:init
@REM Decide how to startup depending on the version of windows

@REM -- Win98ME
if NOT "%OS%"=="Windows_NT" goto Win9xArg

@REM set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" @setlocal

@REM -- 4NT shell
if "%eval[2+2]" == "4" goto 4NTArgs

@REM -- Regular WinNT shell
set CMD_LINE_ARGS=%*
goto WinNTGetScriptDir

@REM The 4NT Shell from jp software
:4NTArgs
set CMD_LINE_ARGS=%$
goto WinNTGetScriptDir

:Win9xArg
@REM Slurp the command line arguments.  This loop allows for an unlimited number
@REM of arguments (up to the command line limit, anyway).
set CMD_LINE_ARGS=
:Win9xApp
if %1a==a goto Win9xGetScriptDir
set CMD_LINE_ARGS=%CMD_LINE_ARGS% %1
shift
goto Win9xApp

:Win9xGetScriptDir
set SAVEDIR=%CD%
%0\
cd %0\..\.. 
set BASEDIR=%CD%
cd %SAVEDIR%
set SAVE_DIR=
goto repoSetup

:WinNTGetScriptDir
set BASEDIR=%~dp0\..

:repoSetup


if "%JAVACMD%"=="" set JAVACMD=java

if "%REPO%"=="" set REPO=%BASEDIR%\lib

set CLASSPATH="%BASEDIR%"\etc;"%REPO%"\netty-3.2.7.Final.jar;"%REPO%"\camel-netty-2.10.0.jar;"%REPO%"\netty-3.5.1.Final.jar;"%REPO%"\camel-core-2.10.0.jar;"%REPO%"\slf4j-api-1.6.1.jar;"%REPO%"\camel-jms-2.10.0.jar;"%REPO%"\spring-jms-3.0.7.RELEASE.jar;"%REPO%"\aopalliance-1.0.jar;"%REPO%"\spring-core-3.0.7.RELEASE.jar;"%REPO%"\spring-asm-3.0.7.RELEASE.jar;"%REPO%"\spring-context-3.0.7.RELEASE.jar;"%REPO%"\spring-expression-3.0.7.RELEASE.jar;"%REPO%"\spring-tx-3.0.7.RELEASE.jar;"%REPO%"\camel-spring-2.10.0.jar;"%REPO%"\spring-aop-3.0.7.RELEASE.jar;"%REPO%"\camel-stream-2.10.0.jar;"%REPO%"\activemq-camel-5.6.0.jar;"%REPO%"\activemq-core-5.6.0.jar;"%REPO%"\geronimo-jms_1.1_spec-1.1.1.jar;"%REPO%"\kahadb-5.6.0.jar;"%REPO%"\activemq-protobuf-1.1.jar;"%REPO%"\fusemq-leveldb-1.1.jar;"%REPO%"\hawtbuf-proto-1.9.jar;"%REPO%"\hawtdispatch-scala-1.9.jar;"%REPO%"\hawtdispatch-1.9.jar;"%REPO%"\leveldb-0.2.jar;"%REPO%"\leveldb-api-0.2.jar;"%REPO%"\guice-3.0.jar;"%REPO%"\javax.inject-1.jar;"%REPO%"\guice-multibindings-3.0.jar;"%REPO%"\guava-10.0.1.jar;"%REPO%"\jsr305-1.3.9.jar;"%REPO%"\leveldbjni-osx-1.2.jar;"%REPO%"\leveldbjni-1.2.jar;"%REPO%"\hawtjni-runtime-1.5.jar;"%REPO%"\leveldbjni-linux32-1.2.jar;"%REPO%"\leveldbjni-linux64-1.2.jar;"%REPO%"\snappy-java-1.0.3.jar;"%REPO%"\jackson-core-asl-1.9.2.jar;"%REPO%"\jackson-mapper-asl-1.9.2.jar;"%REPO%"\hadoop-core-1.0.0.jar;"%REPO%"\commons-configuration-1.6.jar;"%REPO%"\commons-lang-2.4.jar;"%REPO%"\commons-digester-1.8.jar;"%REPO%"\commons-beanutils-1.7.0.jar;"%REPO%"\commons-beanutils-core-1.8.0.jar;"%REPO%"\jetty-6.1.26.jar;"%REPO%"\servlet-api-2.5-20081211.jar;"%REPO%"\jetty-util-6.1.26.jar;"%REPO%"\jsp-api-2.1-6.1.14.jar;"%REPO%"\servlet-api-2.5-6.1.14.jar;"%REPO%"\jsp-2.1-6.1.14.jar;"%REPO%"\ant-1.6.5.jar;"%REPO%"\scala-library-2.9.1.jar;"%REPO%"\mqtt-client-1.0.jar;"%REPO%"\hawtdispatch-transport-1.9.jar;"%REPO%"\hawtbuf-1.9.jar;"%REPO%"\org.osgi.core-4.1.0.jar;"%REPO%"\geronimo-j2ee-management_1.1_spec-1.0.1.jar;"%REPO%"\commons-net-2.2.jar;"%REPO%"\jasypt-1.8.jar;"%REPO%"\xbean-spring-3.11.1.jar;"%REPO%"\commons-logging-1.0.3.jar;"%REPO%"\slf4j-log4j12-1.6.6.jar;"%REPO%"\log4j-1.2.17.jar;"%REPO%"\domain-0.0.1-SNAPSHOT.jar;"%REPO%"\gs.config-0.0.1-SNAPSHOT.jar;"%REPO%"\mockito-all-1.9.0.jar;"%REPO%"\spring-beans-3.0.6.RELEASE.jar;"%REPO%"\commons-lang3-3.1.jar;"%REPO%"\commons-collections-3.2.1.jar;"%REPO%"\core-0.0.1-SNAPSHOT.jar;"%REPO%"\gs.radio-0.0.1-SNAPSHOT.jar
goto endInit

@REM Reaching here means variables are defined and arguments have been captured
:endInit

%JAVACMD% %JAVA_OPTS%  -classpath %CLASSPATH_PREFIX%;%CLASSPATH% -Dapp.name="Radio" -Dapp.repo="%REPO%" -Dapp.home="%BASEDIR%" -Dbasedir="%BASEDIR%" eu.estcube.gs.Radio %CMD_LINE_ARGS%
if ERRORLEVEL 1 goto error
goto end

:error
if "%OS%"=="Windows_NT" @endlocal
set ERROR_CODE=%ERRORLEVEL%

:end
@REM set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" goto endNT

@REM For old DOS remove the set variables from ENV - we assume they were not set
@REM before we started - at least we don't leave any baggage around
set CMD_LINE_ARGS=
goto postExec

:endNT
@REM If error code is set to 1 then the endlocal was done already in :error.
if %ERROR_CODE% EQU 0 @endlocal


:postExec

if "%FORCE_EXIT_ON_ERROR%" == "on" (
  if %ERROR_CODE% NEQ 0 exit %ERROR_CODE%
)

exit /B %ERROR_CODE%
