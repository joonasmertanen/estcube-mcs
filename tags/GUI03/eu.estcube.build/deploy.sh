#!/bin/sh
kill $(ps -u mcs-test -ef | grep java | grep estcube/webserver | awk '{print $2}')
kill $(ps -u mcs-test -ef | grep java | grep estcube/webcamera | awk '{print $2}')
kill $(ps -u mcs-test -ef | grep java | grep estcube/gs.rotator | awk '{print $2}')
killall -u mcs-test -e rotctld
killall -u mcs-test -e rigctld

cd /home/mcs-test/estcube/

for z in ./*.zip; do unzip -o -f -qq $z; done

rotctld -m 1 -t 4533 &
rigctld -m 1901 -t 4535 &

cd /home/mcs-test/estcube/webserver/bin/
touch logWebServer
chmod +x WebServer
./WebServer >logWebServer &

cd /home/mcs-test/estcube/webcamera/bin/
touch logWebCamera
chmod +x WebCamera
./WebCamera >logWebCamera &

cd /home/mcs-test/estcube/gs.rotator/bin/
touch logRotator
chmod +x Rotator
./Rotator >logRotator &
