package eu.estcube.gs.listen;

import javax.jms.Connection;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.hbird.exchange.core.Parameter;

import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryParameter;


public class Listen implements MessageListener {
 
    public static String brokerURL = "tcp://localhost:61616";
 
    public ConnectionFactory factory;
    public Connection connection;
    public Session session;
    public MessageConsumer consumer;
 
    public static void main( String[] args )
    {
        System.out.println("entered main function");
        Listen app = new Listen();
        app.run();
    }
 
    public void run()
    {
        try
        {
            ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(brokerURL);
            connection = factory.createConnection();
            connection.start();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = session.createTopic("gsSend");
            consumer = session.createConsumer(destination);
            consumer.setMessageListener(this);
        }
        catch (Exception e)
        {
            System.out.println("Caught:" + e);
            e.printStackTrace();
        }
    }
 
    public void onMessage(Message message)
    {
        try
        {
            if (message instanceof ObjectMessage)
            {
                ObjectMessage obMessage = (ObjectMessage)message;
                TelemetryObject ob =  (TelemetryObject) obMessage.getObject();
//                System.out.println("Message received: " + ob.toString());
                System.out.println("Message received:");
                System.out.println("Device: " + ob.getDevice());
                System.out.println("Name: " + ob.getName());
                for (Parameter par : ob.getParams()) {
                	System.out.println(par.getName() + ": " + par.getValue());
                }
                System.out.println();
            }
            else
            {	
            	TextMessage textMessage = (TextMessage)message;
                System.out.println("Text message: " + textMessage);                
                System.out.println("\n");

            }
        }
        catch (JMSException e)
        {
            System.out.println("Caught:" + e);
            e.printStackTrace();
        }
    }
}