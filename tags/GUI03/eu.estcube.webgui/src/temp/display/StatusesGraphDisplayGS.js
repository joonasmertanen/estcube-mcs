dojo.provide("webgui.display.StatusesGraphDisplayGS");
dojo.require("dijit.layout.StackContainer");
dojo.require("dojox.gfx");
dojo.require("dojox.gfx.Moveable");
dojo.require("webgui.display.StatusesTableDisplay");

dojo.declare("webgui.display.StatusesGraphDisplayGS", null, {

	constructor : function() {

		console.log("StatusesGraphDisplayGS activated!!!");

		/*
		 * Function to make Nodes
		 * name - name of the node
		 * id - id of the node
		 * size - length and width of the node
		 * xPos - x position
		 * yPos - y position
		 */
		function createNode(name, newId, size, xPos, yPos, subcomponents) {

			var grpName = surface.createGroup();

			grpName.createRect({
				id : newId,
				x : xPos,
				y : yPos,
				width : size,
				height : size
			}).setFill("#F0F0F0").setStroke({
				width : 5,
				color : "green"
			});

			grpName.createText({
				x : xPos + 10,
				y : yPos + nameSize + 10,
				text : name,
				align : "start"
			}).setFont({
				family : "Arial",
				size : nameSize + "pt",
				weight : "bold"
			}).setFill("black");

			for(var i = 0; i < subcomponents.length; i++) {

				grpName.createText({
					id : subcomponents[i] + "Id",
					x : xPos + 10,
					y : yPos + 3 * nameSize + 10 + (subComponentTextSize + 10 ) * i,
					text : subcomponents[i],
					align : "start"
				}).setFont({
					family : "Arial",
					size : subComponentTextSize + "pt",
					weight : "bold"
				}).setFill("black");

			}

			return grpName;

		}

		/*
		 * Function to make Connections
		 * startX - X coordinate of line start
		 * startY - Y coordinate of line start
		 * endX - X coordinate of line end
		 * endY - Y coordinate of line end
		 */

		function createConnection(startX, startY, endX, endY) {
			var grpName = surface.createGroup();

			grpName.createLine({
				x1 : startX,
				y1 : startY,
				x2 : endX,
				y2 : endY
			}).setStroke({
				width : 5,
				color : "black"
			});

			return grpName;
		}

		var subComponentTextSize = 12;
		var nodeSize = 200;
		var nameSize = 20;
		var subComponentTextSize = 12;
		var scrollerHider = 25;

		var radioComponents = new Array(4);
		radioComponents[0] = "Frequency: ";
		radioComponents[1] = "Mode: ";
		radioComponents[2] = "VFO: ";
		radioComponents[3] = "Model info: ";

		var rotatorComponents = new Array(3);
		rotatorComponents[0] = "Azimuth: ";
		rotatorComponents[1] = "Elevation: ";
		rotatorComponents[2] = "Model info: ";

		var statusesGraphContent = new dijit.layout.ContentPane({
			id : "statusesGraphTab",
			title : "Graph",
			preventCache : true
		});

		// Send values to rotator node
		putRotatorData = function(value, i) {
			if(i == 0)
				rotatorComponents[i] = "Azimuth: " + value;
			else if(i == 1)
				rotatorComponents[i] = "Elevation: " + value;
			else if(i == 2)
				rotatorComponents[i] = "Model info: " + value;

			reDraw();
		}
		// Send values to radio node
		putRadioData = function(value, i) {
			if(i == 0)
				radioComponents[i] = "Frequency: " + value;
			else if(i == 1)
				radioComponents[i] = "Mode: " + value;
			else if(i == 2)
				radioComponents[i] = "VFO: " + value;
			else if(i == 3)
				radioComponents[i] = "Model info: " + value;

			reDraw();
		}
		var tempCont;
		tempCont = dijit.byId("CenterContainer");

		tempCont.addChild(statusesGraphContent);
		tempCont.selectChild(statusesGraphContent);

		var surfaceVar = dojo.create("div", {
			id : "surfaceElement",
			region : "center"
		}, "statusesGraphTab");

		var contentBox = dojo.contentBox("CenterContainer");
		var surface = dojox.gfx.createSurface("surfaceElement", contentBox.w - 50, contentBox.h - 50);

		// for drawing objects with updated data
		reDraw = function() {

			surface.clear();

			var grpAllObjects = surface.createGroup();
			var grpShrink = surface.createGroup();
			var grpScale = surface.createGroup();
			var radioGRP;
			var rotatorGRP;
			var connectionRadioRotator;
			radioGRP = createNode("RADIO", "radioNode", nodeSize, 60, 60, radioComponents);
			rotatorGRP = createNode("ROTATOR", "rotatorNode", nodeSize, 60 + 2 * nodeSize, 60, rotatorComponents);
			//connectionRadioRotator = createConnection(60 + nodeSize, 60 + nodeSize / 2, 60 + 2 * nodeSize, 60 + nodeSize / 2);

			// circle movers
			var m1 = new dojox.gfx.Moveable(radioGRP), m2 = new dojox.gfx.Moveable(rotatorGRP);

			radioGRP.connect("onclick", function(e) {
				var tc = dijit.byId("componentTabContainer");

				tc.selectChild(dijit.byId("radioTab"));
				filterRadio();

			});

			rotatorGRP.connect("onclick", function(e) {
				var tc = dijit.byId("componentTabContainer");
				filterRotator();
				tc.selectChild(dijit.byId("rotatorTab"));
			});
			var connectionLine = surface.createLine({
				x1 : 60 + nodeSize,
				y1 : 60 + nodeSize / 2,
				x2 : 60 + 2 * nodeSize,
				y2 : 60 + nodeSize / 2
			}).setStroke({
				width : 5,
				color : "black"
			});

			// For movable connection line
			dojo.connect(m1, "onMoved", function(mover, shift) {
				var o = connectionLine.getShape();
				connectionLine.setShape({
					x1 : o.x1 + shift.dx,
					y1 : o.y1 + shift.dy,
					x2 : o.x2,
					y2 : o.y2
				});
			});
			dojo.connect(m2, "onMoved", function(mover, shift) {
				var o = connectionLine.getShape();
				connectionLine.setShape({
					x1 : o.x1,
					y1 : o.y1,
					x2 : o.x2 + shift.dx,
					y2 : o.y2 + shift.dy
				});
			});

			grpAllObjects.add(connectionLine);
			grpAllObjects.add(radioGRP);
			grpAllObjects.add(rotatorGRP);
			//grpAllObjects.add(connectionRadioRotator);

			// FOR TESTING!
			var shrink = surface.createRect({
				x : 0,
				y : 0,
				width : 15,
				height : 15
			}).setFill("blue");

			var shrinkSymbol = surface.createText({
				x : 3,
				y : 15,
				text : "-",
				align : "start"
			}).setFont({
				family : "Arial",
				size : "20pt",
				weight : "bold"
			}).setFill("white");
			grpShrink.add(shrink);
			grpShrink.add(shrinkSymbol);
			grpShrink.connect("onclick", function(e) {
				grpAllObjects.applyTransform(dojox.gfx.matrix.scale({
					x : 0.75,
					y : 0.75
				}));
				var contentBox = dojo.contentBox("CenterContainer");
				surface.setDimensions(contentBox.w - scrollerHider, contentBox.h - scrollerHider);
			});
			var scale = surface.createRect({
				x : 15,
				y : 0,
				width : 15,
				height : 15
			}).setFill("blue");
			var scaleSymbol = surface.createText({
				x : 16,
				y : 15,
				text : "+",
				align : "start"
			}).setFont({
				family : "Arial",
				size : "18pt",
				weight : "bold"
			}).setFill("white");
			grpScale.add(scale);
			grpScale.add(scaleSymbol);
			grpScale.connect("onclick", function(e) {
				grpAllObjects.applyTransform(dojox.gfx.matrix.scale({
					x : 1.25,
					y : 1.25
				}));
				var contentBox = dojo.contentBox("CenterContainer");
				surface.setDimensions(contentBox.w - scrollerHider, contentBox.h - scrollerHider);
			});
		}
		reDraw();

	}
});
