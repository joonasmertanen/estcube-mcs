dojo.provide("webgui.common.CommandSplitter");

splitCommand = function(selectedTarget, commandString, split) {

	var params = new Array();

	if((split.length - 1) % 2 == 0) {
		for( i = 1; i < split.length - 1; i = i + 2) {
			params.push(new TelemetryParameter(split[i], split[i + 1]));
		}
	}

	var telemetryCommand = new TelemetryCommand(split[0], params, selectedTarget);
	dojo.publish("toWS", [JSON.stringify(telemetryCommand)]);

	return telemetryCommand;
}
function TelemetryParameter(name, value) {
	this.name = name;
	this.value = value;
}

function TelemetryCommand(name, params, device) {
	this.device = device;
	this.name = name;
	this.params = params;
}