﻿dojo.provide("webgui.assembly.Assembler");

dojo.require("webgui.display.InfoDisplay");
dojo.require("webgui.display.MenuDisplay");
dojo.require("webgui.display.PictureDisplay");
dojo.require("webgui.display.MainTabDisplay");
dojo.require("webgui.display.StatusesGraphDisplayGS");
dojo.require("webgui.common.Constants");
dojo.require("webgui.net.WebSocketProxy");

dojo.declare("webgui.assembly.Assembler", null, {
	loadAssembly : function() {

		new webgui.net.WebSocketProxy();
		new webgui.display.MenuDisplay();
		new webgui.display.InfoDisplay();
		new webgui.display.PictureDisplay();
		new webgui.display.StatusesTableDisplay();
		new webgui.display.StatusesInfoDisplay();
		//new webgui.display.StatusesGraphDisplay();
		new webgui.display.StatusesGraphDisplayGS();
		new webgui.display.MainTabDisplay({minY: -180, maxY: 180});


		console.log("loadAssembly done!!");
	}
});
