define(
	["dojo/_base/declare",
	 "dojo/Stateful",
	 "../common/Constants",
	 "./AssemblerBase",
	 "../display/MapView",
     "dojo/_base/xhr"
	 ],
	 
	 function(declare, Stateful, Constants, base, view, xhr) {
		
		
		return [
		    
	        declare("MapAssembler", AssemblerBase, {
	        	
	        	build: function() {
	        		this.setupCacheRequest({ delay: 1500, channels: ["/orbitalpredictions"] });
	        		this.setupCommunication();
	        		var geoLocation = new Stateful();
					xhr.get({
	                    url: "../data/groundStations.json",
	                    handleAs: "json",
	                    load: function(data) {
	                    	var mapController = new MapController({ id: "Map Controller", channels: ["/orbitalpredictions"], geoLocation: geoLocation });
	                        var mapView = new MapView({ divId: "map", geoLocation: geoLocation, groundStationInfo: data});
	                    }
					});
	        	}

	        })
	        
		];
	}
);