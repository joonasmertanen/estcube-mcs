define([
    "dojo/_base/declare",
	"dojo/dom",
	"dojo/date/locale",
	"dojo/_base/event",
	"dojo/_base/lang",
	"./Constants"
], 
function(declare, dom, locale, event, lang, Constants) {
	
	var current_time = new Date();
	var text_time = null;
	
    return declare("Clock", null, {

    	reflectTime : function(time) {
			if(!time) {
				time = current_time;
			}
			var h = time.getUTCHours();
			var m = time.getUTCMinutes();
			var s = time.getUTCSeconds();
			text_time.innerHTML = locale.format(time, {
				selector: "time", 
				timePattern: Constants.DEFAULT_DATE_FORMAT
			});
		},
		
		tick : function() {
			current_time.setSeconds(current_time.getSeconds() + 1);
			this.reflectTime();
		},
		
		createClock : function() {
			text_time = dom.byId("time");
			window.setInterval(lang.hitch(this, "tick"), 1000);
		}
		
    });

});