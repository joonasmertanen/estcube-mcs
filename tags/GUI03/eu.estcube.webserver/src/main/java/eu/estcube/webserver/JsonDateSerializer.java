package eu.estcube.webserver;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class JsonDateSerializer implements JsonSerializer<Date>{

    public JsonElement serialize(Date date, Type arg1, JsonSerializationContext arg2) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-DD HH:mm:ss.SSSZ");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateString = formatter.format(date.getTime());
        return new JsonPrimitive(dateString);
    }

}
