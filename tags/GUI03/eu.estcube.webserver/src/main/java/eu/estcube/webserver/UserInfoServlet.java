/** 
 *
 */
package eu.estcube.webserver;

import java.io.IOException;
import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.security.MappedLoginService;
import org.eclipse.jetty.security.authentication.SessionAuthentication;

import eu.estcube.crowd.CrowdLoginService.CrowdUser;

@SuppressWarnings("serial")
public class UserInfoServlet extends HttpServlet {

    public static final String ATTRIBUT_USER_IDENTITY = "org.eclipse.jetty.security.UserIdentity";

    class UserInfo {
        protected String username;
        protected Set<String> roles = new HashSet<String>();
    }

    private ToJsonProcessor toJson = new ToJsonProcessor();

    /** @{inheritDoc}. */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserInfo userInfo = new UserInfo();
        SessionAuthentication auth = (SessionAuthentication) req.getSession().getAttribute(ATTRIBUT_USER_IDENTITY);
        if (auth != null) {
            for (Principal p : auth.getUserIdentity().getSubject().getPrincipals()) {
                if (p.getClass().equals(CrowdUser.class)) {
                    userInfo.username = p.getName();
                } else if (p.getClass().equals(MappedLoginService.RolePrincipal.class)) {
                    userInfo.roles.add(p.getName());
                }
            }
        }

        try {
            String json = toJson.process(userInfo);
            resp.setContentType("application/json");
            resp.getOutputStream().println(json);
        } catch (Exception e) {
            throw new ServletException("Failed to serialize UserInfo (username:" + userInfo.username + ")to JSON", e);
        }
    }
}
