package eu.estcube.templating.placeholders;

/**
 * An exception signaling a failure somewhere in the placeholder replacement.
 * 
 * @author deiwin
 */
public class PlaceholderException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public PlaceholderException(String message) {
        super(message);
    }

}
