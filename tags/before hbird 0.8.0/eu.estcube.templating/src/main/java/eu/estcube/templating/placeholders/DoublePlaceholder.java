package eu.estcube.templating.placeholders;

public enum DoublePlaceholder implements IPlaceholder<Double> {

    ROTATOR_AZIMUTH("azimuth"),
    ROTATOR_ELEVATION("elevation");

    private final String name;

    private DoublePlaceholder(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public Class<Double> getType() {
        return Double.class;
    }

    @Override
    public Placeholder toPlaceholder() throws PlaceholderException {
        Placeholder result = null;

        switch (this) {
            case ROTATOR_AZIMUTH:
                result = Placeholder.ROTATOR_AZIMUTH;
                break;
            case ROTATOR_ELEVATION:
                result = Placeholder.ROTATOR_ELEVATION;
                break;
        }

        // Don't add as a default case, because this way a compiler warning will
        // still be given if a switch statement is missing
        if (result == null) {
            throw new PlaceholderException("Double placeholder \"" + this + "\" not defined in Placeholders.");
        }

        return result;
    }

}
