package eu.estcube.templating.beans;

import java.util.Collection;

import org.hbird.exchange.core.Command;

public class Contact {
    private Collection<Command> header;
    private Collection<Command> body;
    private Collection<Command> tail;

    public Contact(Collection<Command> header, Collection<Command> body,
            Collection<Command> tail) {
        this.header = header;
        this.body = body;
        this.tail = tail;
    }

    public Collection<Command> getHeaderCommands() {
        return header;
    }

    public Collection<Command> getBodyCommands() {
        return body;
    }

    public Collection<Command> getTailCommands() {
        return tail;
    }

    @Override
    public String toString() {
        return "ContactTemplate [header=" + header + ", body=" + body
                + ", tail=" + tail + "]";
    }
}
