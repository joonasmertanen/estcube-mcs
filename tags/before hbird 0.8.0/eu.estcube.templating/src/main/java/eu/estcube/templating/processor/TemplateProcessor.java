package eu.estcube.templating.processor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eu.estcube.templating.placeholders.IPlaceholder;
import eu.estcube.templating.placeholders.Placeholder;
import eu.estcube.templating.placeholders.PlaceholderException;
import eu.estcube.templating.placeholders.PlaceholderMap;
import eu.estcube.templating.template.Template;
import eu.estcube.templating.template.provider.TemplateProvider;

/**
 * Processes templates by inserting data into them.
 * 
 * @author deiwin
 * 
 * @param <T> Template used for processing
 * @param <D> The output type
 * @param <P> Template provider used
 */
public abstract class TemplateProcessor<T extends Template, D, P extends TemplateProvider<T>> {

    /**
     * Fills the given string template with the given values.
     * 
     * @param template The template.
     * @param placeholderReplacements The values linked to specific
     *        placeholders.
     * @return
     * @throws PlaceholderException If any of the placeholder replacements
     *         fails.
     */
    public static String processString(String template, PlaceholderMap placeholderReplacements)
            throws PlaceholderException
    {

        Matcher m = Pattern.compile(Placeholder.PLACEHOLDER_REGEX).matcher(template);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            IPlaceholder<?> placeholder = Placeholder.fromName(m
                    .group(Placeholder.PLACEHOLDER_REGEX_NAME_GROUP)).toIPlaceholder();
            Object value = placeholderReplacements.get(placeholder);
            m.appendReplacement(sb, String.valueOf(value));
        }
        m.appendTail(sb);

        return sb.toString();
    }

    /**
     * @param template The template to process.
     * @param placeholderReplacements The values to use for processing.
     * @return The processed object.
     * @throws PlaceholderException If a required placeholder value is not set.
     */
    public abstract D process(T template, PlaceholderMap placeholderReplacements) throws PlaceholderException;

    /**
     * Same as {@link #process(Template, PlaceholderMap)} but uses a template
     * provider.
     */
    public D process(P templateProvider, PlaceholderMap placeholderReplacements) throws PlaceholderException {
        return process(templateProvider.getTemplate(), placeholderReplacements);
    }

}
