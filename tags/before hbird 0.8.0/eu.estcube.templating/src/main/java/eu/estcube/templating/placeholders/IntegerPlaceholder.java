package eu.estcube.templating.placeholders;

public enum IntegerPlaceholder implements IPlaceholder<Integer> {
    ROTATOR_ID("rot_id"),
    COMMAND_TARGET_ID("target_id"),
    RADIO_FREQUENCY("frequency"),
    RADIO_PASSBAND("passband");

    private final String name;

    private IntegerPlaceholder(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public Class<Integer> getType() {
        return Integer.class;
    }

    @Override
    public Placeholder toPlaceholder() throws PlaceholderException {
        Placeholder result = null;

        switch (this) {
            case COMMAND_TARGET_ID:
                result = Placeholder.COMMAND_TARGET_ID;
                break;
            case RADIO_FREQUENCY:
                result = Placeholder.RADIO_FREQUENCY;
                break;
            case RADIO_PASSBAND:
                result = Placeholder.RADIO_PASSBAND;
                break;
            case ROTATOR_ID:
                result = Placeholder.ROTATOR_ID;
                break;
        }

        // Don't add as a default case, because this way a compiler warning will
        // still be given if a switch statement is missing
        if (result == null) {
            throw new PlaceholderException("Integer placeholder \"" + this + "\" not defined in Placeholders.");
        }

        return result;
    }

}
