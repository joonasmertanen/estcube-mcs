package eu.estcube.templating.processor;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import eu.estcube.templating.placeholders.DoublePlaceholder;
import eu.estcube.templating.placeholders.PlaceholderException;
import eu.estcube.templating.placeholders.PlaceholderMap;

public class TemplateProcessorTest {

    private PlaceholderMap rotValues;

    private String rotTemp;

    @Before
    public void setUp() throws Exception {

        rotValues = new PlaceholderMap();
        rotTemp = "P ${azimuth} ${elevation}";
    }

    @Test
    public void testProcessCommandValue() throws PlaceholderException {
        rotValues.put(DoublePlaceholder.ROTATOR_AZIMUTH, 30.5);
        rotValues.put(DoublePlaceholder.ROTATOR_ELEVATION, 70D);

        String expected = "P 30.5 70.0";
        String actual = TemplateProcessor.processString(rotTemp, rotValues);

        assertEquals(expected, actual);

    }

    @Test(expected = PlaceholderException.class)
    public void testProcessCommandValueFail() throws PlaceholderException {
        rotValues.put(DoublePlaceholder.ROTATOR_AZIMUTH, 30D);

        TemplateProcessor.processString(rotTemp, rotValues);
    }

    @Test(expected = PlaceholderException.class)
    public void testProcessCommandValueFail2() throws PlaceholderException {
        rotValues.put(DoublePlaceholder.ROTATOR_AZIMUTH, 30.5);
        rotValues.put(DoublePlaceholder.ROTATOR_ELEVATION, 70D);

        rotTemp = "P ${azimuth} ${something_undefined} ${elevation}";

        TemplateProcessor.processString(rotTemp, rotValues);
    }
}
