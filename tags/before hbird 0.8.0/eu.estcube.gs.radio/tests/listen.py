import logging
import stomp


class MyListener(object):
    def on_error(self, headers, message):
        print 'received an error %s' % message

    def on_message(self, headers, message):
        print 'received a message %s' % message

logging.basicConfig()
conn = stomp.Connection(host_and_ports=[('localhost', 61616)])
conn.set_listener('', MyListener())
conn.start()
conn.connect()
conn.subscribe(destination='/topic/gsSend', ack='auto')
raw_input()
