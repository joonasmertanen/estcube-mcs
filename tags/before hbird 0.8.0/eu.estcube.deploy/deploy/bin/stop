#!/bin/bash
quiet=false

# quiet flag
if [[ $1 = "-q" ]]; then
    quiet=true
    shift
fi

# Check argument
if [[ $# < 1 ]]; then
    printf "Missing service. Usage: %s [-q] <service>\n" "$0"
    exit -1
fi

service=$1
shift

# Check service
dir=$(pwd)

if [ ! -f "$dir/$service/bin/run" ]; then
    printf "Service %s not found\n" "$service"
    exit -2
fi

if [[ ! -x "$dir/$service/bin/run" ]]; then
    printf "Service %s not executable\n" "$service"
    exit -3
fi

pidFile=$dir/$service/bin/service.pid

#Check to see if process running.
pid=$(cat $pidFile 2>/dev/null)
if [[ $? = 0 ]]; then
    ps -p $pid -f | grep $service >/dev/null 2>&1
    if [[ $? = 0 ]]; then
        kill -9 $pid
        rm $pidFile
        logFile=$dir/$service/logs/std.log
        message="$(date -u +'%Y-%m-%d %H:%M:%S %Z') - $service stopped; PID: $pid"
        echo $message >> $logFile
        if ! $quiet ; then
            printf "%s\n" "$message"
        fi
        exit 0
    else
        printf "Service %s not running. Process with PID: %s not found or not valid\n" "$service" "$pid"
        rm $pidFile
        exit -1
    fi
else
    printf "Service %s not running. PID file not found\n" "$service"
fi
