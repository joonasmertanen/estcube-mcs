package eu.estcube.contact;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math.geometry.Vector3D;
import org.orekit.bodies.CelestialBodyFactory;
import org.orekit.bodies.GeodeticPoint;
import org.orekit.bodies.OneAxisEllipsoid;
import org.orekit.errors.OrekitException;
import org.orekit.frames.Frame;
import org.orekit.frames.FramesFactory;
import org.orekit.frames.LocalOrbitalFrame;
import org.orekit.frames.LocalOrbitalFrame.LOFType;
import org.orekit.frames.TopocentricFrame;
import org.orekit.orbits.CartesianOrbit;
import org.orekit.orbits.Orbit;
import org.orekit.propagation.Propagator;
import org.orekit.propagation.SpacecraftState;
import org.orekit.propagation.analytical.KeplerianPropagator;
import org.orekit.propagation.events.ElevationDetector;
import org.orekit.propagation.events.EventDetector;
import org.orekit.time.AbsoluteDate;
import org.orekit.time.TimeScalesFactory;
import org.orekit.tle.TLE;
import org.orekit.tle.TLEPropagator;
import org.orekit.utils.Constants;
import org.orekit.utils.PVCoordinates;
import org.orekit.utils.PVCoordinatesProvider;


public class ContactCalculation 
{	
	public static final double EQUATORIAL_RADIUS_OF_THE_EARTH  = Constants.WGS84_EARTH_EQUATORIAL_RADIUS;             // Equatorial radius of Earth
	public static final double FLATTEING_OF_THE_ERATH_ON_POLES = Constants.WGS84_EARTH_FLATTENING;   // Earth flattening
	public static final double MU = Constants.WGS84_EARTH_MU;
    public static final double SPEED_OF_LIGHT = Constants.SPEED_OF_LIGHT;                            // Speed of light
	public static final double MINUTES_PER_DAY = 1.44E3;          						// Minutes per day 
	public static final int SECONDS_PER_DAY = 86400;									// Seconds per day
	
	//CONSTATS FOR TLE CALCUALTIONS
	public static final double TWO_PI = 6.2831853071796;								// 2*Pi 
	private static final double ONE_THIRD = 1.0 / 3.0;
	private static final double TWO_THIRD = 2.0 / 3.0;
	private static final double NORMALIZED_EQUATORIAL_RADIUS = 1.0;
	private static final double XKE    = 0.0743669161331734132; // mu = 3.986008e+14;
	private static final double XJ2    = 1.082616e-3;
	private static final double CK2    = 0.5 * XJ2 * NORMALIZED_EQUATORIAL_RADIUS * NORMALIZED_EQUATORIAL_RADIUS;
	
	final OneAxisEllipsoid earth; 
	PVCoordinatesProvider occulting = CelestialBodyFactory.getEarth();					//earth PVCoordinatesProvider
	PVCoordinatesProvider occulted = CelestialBodyFactory.getSun();						//sun PVCoordinatesProvider
	boolean inSunlight;																	//is satellite eclipsed
	SpacecraftState startingState;														//SpacecraftState in contact starting point
	
	
	public ContactCalculation() throws OrekitException{
		earth = new OneAxisEllipsoid(EQUATORIAL_RADIUS_OF_THE_EARTH, FLATTEING_OF_THE_ERATH_ON_POLES, FramesFactory.getITRF2005());
	}
	/**
	 * Method for calculating satellites PVCoordinates
	 * @param tle - satellites tle
	 * @param absoluteDate - given date, when calculations are started
	 * @return PVCoordinates - satellite coordinates at given time
	 * @throws OrekitException
	 */
	public static PVCoordinates calculateOrbitalState(TLE tle, AbsoluteDate absoluteDate) throws OrekitException {
	       TLEPropagator propagator = TLEPropagator.selectExtrapolator(tle);
	       PVCoordinates result = propagator.getPVCoordinates(absoluteDate);
	       return result;
	}
	/**
	 * Calculates doppler
	 * @param initialOrbit - satellites orbit
	 * @param locationOnEarth - GS location
	 * @param absoluteDate - time when doppler will be calculated
	 * @return
	 * @throws OrekitException
	 */
	public double calculateDoppler(Orbit initialOrbit, TopocentricFrame locationOnEarth, AbsoluteDate absoluteDate, double frequency) throws OrekitException {
	       Frame inertialFrame = FramesFactory.getCIRF2000();//an inertial frame.
	       Propagator propagator = new KeplerianPropagator(initialOrbit);//as a propagator, we consider a simple KeplerianPropagator.  
	       LocalOrbitalFrame lof = new LocalOrbitalFrame(inertialFrame, LOFType.QSW, propagator, "QSW"); //local orbital frame.
	       PVCoordinates origin = PVCoordinates.ZERO;
	       PVCoordinates pv = locationOnEarth.getTransformTo(lof, absoluteDate).transformPVCoordinates(origin);
	       double dp = Vector3D.dotProduct(pv.getPosition(), pv.getVelocity()) / pv.getPosition().getNorm();
	       double doppler = ((1 - (dp / SPEED_OF_LIGHT)) * frequency) - frequency;
	       return doppler;
	}
	/**
	 * Calculates azimuth in given time
	 * @param satellite - satellite coordinates
	 * @param locationOnEarth - GS location
	 * @param inertialFrame - an inertial frame
	 * @param startTime - given time for calculations
	 * @return
	 * @throws OrekitException
	 */
	public double calculateAzimuth(PVCoordinates satellite,TopocentricFrame locationOnEarth,Frame inertialFrame, AbsoluteDate time) throws OrekitException{
		double azimuth = locationOnEarth.getAzimuth(satellite.getPosition(), inertialFrame, time);
		double azimuthInDegree = Math.toDegrees(azimuth);
		return azimuthInDegree;
	}
	/**
	 * Calculates satellite elevation in given time
	 * @param satellite - satellite coordinates
	 * @param locationOnEarth - GS location
	 * @param absoluteDate - given time for calculations
	 * @return
	 * @throws OrekitException
	 */
	public double calculateElevation(PVCoordinates satellite, TopocentricFrame locationOnEarth, AbsoluteDate absoluteDate) throws OrekitException {
        return Math.toDegrees(locationOnEarth.getElevation(satellite.getPosition(), FramesFactory.getCIRF2000(), absoluteDate));
    }
	/**
	 * Calculates maximal elevation during contact time
	 * @param satellite
	 * @param locationOnEarth
	 * @param start - contact start time
	 * @param end - contact end time
	 * @return maximal elevation
	 * @throws OrekitException
	 */
	public double calculateMaxElevation(SpacecraftState s, TopocentricFrame locationOnEarth, AbsoluteDate start, AbsoluteDate end) throws OrekitException{
		double maxElevation = 0;
		while(start.compareTo(end) < 0){
			double elev = calculateElevation(s.getPVCoordinates(), locationOnEarth, start);
			if(elev > maxElevation){
				maxElevation = elev;
			}
			start = start.shiftedBy(30);
			s = s.shiftedBy(30);
		}
		return maxElevation;
	}
	/**
	 * Calcualtes if spacecraft is in eclipse or not
	 * @param s - SpacecraftState: the current state information: date, kinematics, attitude
	 * @return
	 * @throws OrekitException
	 */
	public double isEclipse(SpacecraftState s) throws OrekitException{
		double occultedRadius = 696000000.;
		double occultingRadius = Constants.WGS84_EARTH_EQUATORIAL_RADIUS;
		
		final Vector3D pted = occulted.getPVCoordinates(s.getDate(), s.getFrame()).getPosition();
		final Vector3D ping = occulting.getPVCoordinates(s.getDate(), s.getFrame()).getPosition();
		final Vector3D psat = s.getPVCoordinates().getPosition();
		final Vector3D ps   = pted.subtract(psat);
		final Vector3D po   = ping.subtract(psat);
		final double angle  = Vector3D.angle(ps, po);
		final double rs     = Math.asin(occultedRadius / ps.getNorm());
		final double ro     = Math.asin(occultingRadius / po.getNorm());
		return (angle- ro -rs);
	}
	/**
	 * 
	 * @param s - SpacecraftState: the current state information: date, kinematics, attitude
	 * @param location
	 * @param frame
	 * @return Range in kilometers
	 * @throws OrekitException
	 */
	public double calculateRange(SpacecraftState s, TopocentricFrame location, Frame frame) throws OrekitException{
		return location.getRange(s.getPVCoordinates().getPosition(), frame, s.getDate());
	}
	/**
	 * Calculates signal loss at given range and radio frequency
	 * @param range - range from GS to sat
	 * @param freq - radio frequency in MHz
	 * @return
	 */
	public double calculateSignalLoss(double range, double freq, double gsLoss, double satLoss){
		double loss = 20*Math.log10(range) + 20*Math.log10(freq) - 27.5 + gsLoss + satLoss;
		//TODO liita maajaama ja satelliidi konstandid
		return loss;
	}
	/**
	 * 
	 * @param s - SpacecraftState: the current state information: date, kinematics, attitude
	 * @param location
	 * @param frame
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws OrekitException
	 */
	public double calculateMinRange(SpacecraftState s, TopocentricFrame location, Frame frame, AbsoluteDate startDate, AbsoluteDate endDate) throws OrekitException{
		double minRange = calculateRange(s, location, frame);
		while(startDate.compareTo(endDate) < 0){
			double range = calculateRange(s, location, frame);
			if(range < minRange){
				minRange = range;
			}
			startDate = startDate.shiftedBy(30);
			s = s.shiftedBy(30);
		}
		return minRange;	
	}
	/**
	 * 
	 * @param s - SpacecraftState: the current state information: date, kinematics, attitude
	 * @param location
	 * @param frame
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws OrekitException
	 */
	public double calculateMaxRange(SpacecraftState s, TopocentricFrame location, Frame frame, AbsoluteDate startDate, AbsoluteDate endDate) throws OrekitException{
		double maxRange = calculateRange(s, location, frame);
		while(startDate.compareTo(endDate) < 0){
			double range = calculateRange(s, location, frame);
			if(range > maxRange){
				maxRange = range;
			}
			startDate = startDate.shiftedBy(30);
			s = s.shiftedBy(30);
		}
		return maxRange;	
	}
	/**
	 * 
	 * @param range
	 * @return
	 */
	public double calculateSignalDelay(double range){
		return range / SPEED_OF_LIGHT ;
	}
	/**
	 * 
	 * @param tle
	 * @param date
	 * @return
	 * @throws OrekitException
	 */
	// arvutada gs poolusel, erinevad satelliidid, järgmine päev
	public int calculateOrbitNumber(TLE tle, AbsoluteDate date) throws OrekitException{
		//calculations for original recovered mean motion. 
		final double a1 = Math.pow(XKE / (tle.getMeanMotion() * 60.0), TWO_THIRD);
        final double cosi0 = Math.cos(tle.getI());
        final double theta2 = cosi0 * cosi0;
        final double x3thm1 = 3.0 * theta2 - 1.0;
        final double e0sq = tle.getE() * tle.getE();
        final double beta02 = 1.0 - e0sq;
        final double beta0 = Math.sqrt(beta02);
        final double tval = CK2 * 1.5 * x3thm1 / (beta0 * beta02);
        final double delta1 = tval / (a1 * a1);
        final double a0 = a1 * (1.0 - delta1 * (ONE_THIRD + delta1 * (1.0 + 134.0 / 81.0 * delta1)));
        final double delta0 = tval / (a0 * a0);

	    // recover original mean motion :
	    final double xn0dp = tle.getMeanMotion() * 60.0 / (delta0 + 1.0);
	    
	    double age = date.durationFrom(tle.getDate()) / SECONDS_PER_DAY;

		int orbitNum = (int) (Math.floor((xn0dp* MINUTES_PER_DAY/TWO_PI + age * tle.getBStar() * 1.0 ) * age + tle.getMeanAnomaly()/TWO_PI) +tle.getRevolutionNumberAtEpoch() -1);
		return orbitNum;
	}
	//TODO Orekit-datazip uurida kuna ja mis uuendatakse
	/**
	 * 
	 * @param tle
	 * @param startDate
	 * @param endDate
	 * @param gsLocation
	 * @param elevation
	 * @return
	 * @throws OrekitException
	 */
	//elevation - nurk mille pealt maajaam nägema hakkab
	public List<ContactEventData> calculateContact(final TLE tle, AbsoluteDate startDate, AbsoluteDate endDate, GeodeticPoint gsLocation, double elevation, final String gsName, final String satName, final double uplinkFreq, final double downlinkFreq, final double gsLoss, final double satLoss) throws OrekitException{
		long start = System.currentTimeMillis();
		final List<ContactEventData> contactDataList = new ArrayList<ContactEventData>();
		//satellite
		final PVCoordinates satellite = calculateOrbitalState(tle, startDate);
		final Frame inertialFrame = FramesFactory.getCIRF2000();	//an inertial frame.
		final Orbit initalOrbit = new CartesianOrbit(satellite, inertialFrame, startDate, MU);
		final Propagator propagator = new KeplerianPropagator(initalOrbit);
		//GS
		final TopocentricFrame locationOnEarth = new TopocentricFrame(earth, gsLocation, gsName);
		final double maxCheck  = 10;
		
		
		EventDetector visibilityDetector = new ElevationDetector(maxCheck, elevation, locationOnEarth){
			private static final long serialVersionUID = 1L;
			
			@Override
			public int eventOccurred(final SpacecraftState s, final boolean increasing) throws OrekitException {
				if (increasing) {
					startingState = s;
					return ElevationDetector.CONTINUE;
				} else{
					if(startingState != null){
						ContactEventData contactEventData = new ContactEventData();
					
						contactEventData.setStartTime(startingState.getDate());
						contactEventData.setEndTime(s.getDate());
						
						if(isEclipse(startingState) < 0 || isEclipse(s) < 0){
							inSunlight = false;
						} else {
							inSunlight = true;
						}
						double rangeAtStart = calculateRange(startingState, locationOnEarth, inertialFrame);
						double maxRange = calculateMaxRange(startingState, locationOnEarth, inertialFrame, startingState.getDate(), s.getDate());
						double minRange = calculateMinRange(startingState, locationOnEarth, inertialFrame, startingState.getDate(), s.getDate());
						double rangeAtEnd = calculateRange(startingState, locationOnEarth, inertialFrame);
					
						contactEventData.setInSunlight(inSunlight);
						contactEventData.setStartAzimuth(calculateAzimuth(startingState.getPVCoordinates(), locationOnEarth, inertialFrame, startingState.getDate()));
						contactEventData.setEndAzimuth(calculateAzimuth(s.getPVCoordinates(),locationOnEarth, inertialFrame, s.getDate()));
					
						contactEventData.setStartElevation(calculateElevation(startingState.getPVCoordinates(), locationOnEarth, startingState.getDate()));
						contactEventData.setEndElevation(calculateElevation(s.getPVCoordinates(), locationOnEarth, s.getDate()));
						contactEventData.setMaxElevation(calculateMaxElevation(startingState, locationOnEarth, startingState.getDate(), s.getDate()));	
					
						contactEventData.setStartDopplerUp(calculateDoppler(initalOrbit, locationOnEarth, startingState.getDate(), uplinkFreq));
						contactEventData.setStartDopplerDown(calculateDoppler(initalOrbit, locationOnEarth, startingState.getDate(), downlinkFreq));
						contactEventData.setEndDopplerUp(calculateDoppler(initalOrbit, locationOnEarth, s.getDate(), uplinkFreq));
						contactEventData.setEndDopplerDown(calculateDoppler(initalOrbit, locationOnEarth, s.getDate(), downlinkFreq));
						
						contactEventData.setStartSigLossUp(calculateSignalLoss(rangeAtStart, uplinkFreq, gsLoss, satLoss));
						contactEventData.setStartSigLossDown(calculateSignalLoss(rangeAtStart, downlinkFreq, gsLoss, satLoss));
						contactEventData.setMaxSigLossUp(calculateSignalLoss(maxRange, uplinkFreq, gsLoss, satLoss));
						contactEventData.setMaxSigLossDown(calculateSignalLoss(maxRange, downlinkFreq, gsLoss, satLoss));
						contactEventData.setMinSigLossUp(calculateSignalLoss(minRange, uplinkFreq, gsLoss, satLoss));
						contactEventData.setMinSigLossDown(calculateSignalLoss(minRange, downlinkFreq, gsLoss, satLoss));
						contactEventData.setEndSigLossUp(calculateSignalLoss(rangeAtEnd, uplinkFreq, gsLoss, satLoss));
						contactEventData.setEndSigLossDown(calculateSignalLoss(rangeAtEnd, downlinkFreq, gsLoss, satLoss));
						
						contactEventData.setRangeAtStart(rangeAtStart);
						contactEventData.setMaxRange(maxRange);
						contactEventData.setMinRange(minRange);
						contactEventData.setRangeAtEnd(rangeAtEnd);
						
						contactEventData.setStartSignalDelay(calculateSignalDelay(rangeAtStart));
						contactEventData.setMaxSignalDelay(calculateSignalDelay(maxRange));
						contactEventData.setMinSignalDelay(calculateSignalDelay(minRange));
						contactEventData.setEndSignalDelay(calculateSignalDelay(rangeAtEnd));
						
						contactEventData.setSatellite(satName);
						contactEventData.setGroundStation(gsName);
						
						contactEventData.setOrbitNumber(calculateOrbitNumber(tle, startingState.getDate()));
						
						contactDataList.add(contactEventData);
					
					}
					return ElevationDetector.CONTINUE;//STOP;
				}	
			}
			
		};	
		
		propagator.addEventDetector(visibilityDetector);
		//Uurida kuidas saaks arvestada, et kontakti ei toimu, aga satelliit on kuulatav
		SpacecraftState finalState = propagator.propagate(endDate);
		
		double endElev = calculateElevation(finalState.getPVCoordinates(), locationOnEarth, endDate);
		if(endElev > 0){
			ContactEventData noContactData = new ContactEventData();
			noContactData.setStartTime(startDate);
			noContactData.setEndTime(endDate);
			double range = calculateRange(finalState, locationOnEarth, inertialFrame);
			noContactData.setRangeAtEnd(range);
			if(isEclipse(finalState) < 0 ){
				inSunlight = false;
			} else {
				inSunlight = true;
			}
			noContactData.setInSunlight(inSunlight);
			noContactData.setEndElevation(endElev);
			noContactData.setEndSigLossDown(calculateSignalLoss(range, downlinkFreq, gsLoss, satLoss));
			noContactData.setEndSigLossUp(calculateSignalLoss(range, uplinkFreq, gsLoss, satLoss));
			noContactData.setStartDopplerUp(calculateDoppler(initalOrbit, locationOnEarth, startDate, uplinkFreq));
			noContactData.setStartDopplerDown(calculateDoppler(initalOrbit, locationOnEarth, startDate, downlinkFreq));
			noContactData.setEndDopplerUp(calculateDoppler(initalOrbit, locationOnEarth, endDate, uplinkFreq));
			noContactData.setEndDopplerDown(calculateDoppler(initalOrbit, locationOnEarth, endDate, downlinkFreq));
			noContactData.setEndSignalDelay(calculateSignalDelay(range));
			noContactData.setOrbitNumber(calculateOrbitNumber(tle, endDate));
			contactDataList.add(noContactData);
		}

		for(ContactEventData data : contactDataList){
			AbsoluteDate startTime = data.getStartTime();
			AbsoluteDate endTime = data.getEndTime();
			
			System.out.println("Starttime: " + startTime + " ; End time: " +endTime + " ; Start azimuth: " +data.getStartAzimuth() 
					+ " ; End azimuth: " +data.getEndAzimuth() + " ; Start elevation: " +data.getStartElevation() + " ; End elevation: " 
					+ data.getEndElevation() + " ; Max elevation: " +data.getMaxElevation() +" ; Start doppler up: "+data.getStartDopplerUp()
					+ " ; Start doppler down: " +data.getStartDopplerDown() + " ; End doppler up: " +data.getEndDopplerUp()
					+ " ; End doppler down: " +data.getEndDopplerDown() + " ; Start sig loss up: " +data.getStartSigLossUp()
					+ " ; Start sig loss down: " +data.getStartSigLossDown() +" ; End sig loss up: " +data.getEndSigLossUp()
					+ " ; End sig loss down: " +data.getEndSigLossDown() + " ; Max sig loss up: "+data.getMaxSigLossUp()
					+ " ; Max sig loss down: " +data.getMaxSigLossDown() + " ; Min sig loss up: " +data.getMinSigLossUp()
					+ " ; Min sig loss down: " +data.getMinSigLossDown() + " ; Range at start: " +data.getRangeAtStart()
					+ " ; Max range: " +data.getMaxRange() +" ; Min range: " +data.getMinRange() +" ; Range at end: " +data.getRangeAtEnd()
					+ " ; Start signal delay: " +data.getStartSignalDelay() +" ; Max signal delay: " +data.getMaxSignalDelay()
					+ " ; Min signal delay: " +data.getMinSignalDelay() + " ; End signal delay: " +data.getEndSignalDelay()
					+ " ; Is in sunlight: " +data.isInSunlight() + " ; Orbit number: " +data.getOrbitNumber()
					+ " ; Satellite: " + data.getSatellite() + " ; GroundStation: " +data.getGroundStation());
		}
		System.out.println(" Final state : " + finalState.getDate().durationFrom(startDate));
		long elapsedTimeMillis = System.currentTimeMillis()-start;
		float elapsedTimeSec = elapsedTimeMillis/1000F;
		System.out.println("TIME: "+ elapsedTimeSec);
		return contactDataList;	
	}
	//sig.loss linkbudget free-space loss(kindlal raadiosagedusel, eraldi uplink ja downlink)
	//maajaama ja sat vaheline kaugus + mingi valem
	
    public static void main( String[] args ) throws OrekitException {
    	System.setProperty("orekit.data.path", "src/main/resources/orekit-data.zip"); // same as -Dorekit.data.pah=src/main/resources/orekit-data.zip
    	
    	// Geolocation of ground station ES5EC - on roof top of the building in TĆ¤he 4, Tartu, Estonia
        GeodeticPoint es5ec = new GeodeticPoint(Math.toRadians(58.3000D), Math.toRadians(26.7330D), 59.0D);
        TLE tle = new TLE(
                "1 27842U 03031C   13055.03472196  .00000066  00000-0  50307-4 0  6185",
                "2 27842  98.6925  65.9850 0009459 185.3751 237.0812 14.21401313500787");
        TLE so67 = new TLE("1 35870U 09049F   13056.87492103  .00002389  00000-0  95667-4 0  8262",
        					"2 35870  97.2042  89.6221 0002537  38.0311  89.1259 15.25988409191275");
        
        TLE compass1 = new TLE("1 32787U 08021E   13105.71801885  .00002263  00000-0  26947-3 0  5963",
				"2 32787  97.7577 165.8927 0016016 145.4217   1.1445 14.85219091268693");
        
        //Manuaalselt muudetud TLE võrdlus
        TLE katse = new TLE("1 32788U 08021F   13090.17084175  .00002446  00000-0  28856-3 0  5708",
        		"2 32788  97.7611 151.3121 0014595 194.0219 226.9472 14.85563977266422");
        TLE katse2 = new TLE("1 32788U 08021F   13090.18473064  .00002446  00000-0  28856-3 0  5708",
        		"2 32788  97.7611 151.3121 0014595 194.0219 226.9472 14.85563977266422");
        double uplinkFreq=100; //uplink frequency in MHz
        double downlinkFreq=437; //downlink frequency in MHz
        double gsLoss = 0; //signal loss constant for groundstation in db
        double satLoss = 0; //signal loss constant for satellite in db
        
        //compass-1
        final double elevation = Math.toRadians(0.26);
        //start date
        AbsoluteDate startDate = new AbsoluteDate(2013, 4, 9, 6, 16, 00, TimeScalesFactory.getUTC());
        //end date 
        AbsoluteDate endDate = new AbsoluteDate(2013, 4, 19, 13, 35, 00, TimeScalesFactory.getUTC());
        ContactCalculation calc = new ContactCalculation();
        
        calc.calculateContact(compass1, startDate, endDate, es5ec, elevation, "ES5EC", "COMPASS-1", uplinkFreq, downlinkFreq, gsLoss, satLoss);    
    }
    

}
