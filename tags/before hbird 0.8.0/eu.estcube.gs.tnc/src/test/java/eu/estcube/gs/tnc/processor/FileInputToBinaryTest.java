package eu.estcube.gs.tnc.processor;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import eu.estcube.common.ByteUtil;

/**
 *
 */
public class FileInputToBinaryTest {
    
    private FileInputToBinary converter;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        converter = new FileInputToBinary();
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.processor.FileInputToBinary#fileContentToBinary(java.io.File)}.
     * @throws Exception 
     * @throws IOException 
     */
    @Test
    public void testFileContentToBinaryHexDump() throws Exception {
        File file = new File(getClass().getResource("/hex.txt").toURI());
        byte[] bytes = converter.fileContentToBinary(file);
        String fileAsString = FileUtils.readFileToString(file);
        assertEquals(fileAsString.replaceAll("\\s", ""), ByteUtil.toHexString(bytes).replaceAll("\\s", ""));
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.processor.FileInputToBinary#fileContentToBinary(java.io.File)}.
     * @throws Exception 
     */
    @Test
    public void testFileContentToBinaryNotHexDump() throws Exception {
        File file = new File(getClass().getResource("/ascii.txt").toURI());
        byte[] bytes = converter.fileContentToBinary(file);
        byte[] fileAsBytes = FileUtils.readFileToByteArray(file);
        assertTrue(Arrays.equals(fileAsBytes, bytes));
    }
}
