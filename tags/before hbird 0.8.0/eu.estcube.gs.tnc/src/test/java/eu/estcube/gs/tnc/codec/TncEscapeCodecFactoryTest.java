package eu.estcube.gs.tnc.codec;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TncEscapeCodecFactoryTest {

    @Mock
    private IoSession session;
    
    private TncEscapeCodecFactory factory;
    
    private InOrder inOrder;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        factory = new TncEscapeCodecFactory();
        inOrder = inOrder(session);
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncEscapeCodecFactory#getEncoder(org.apache.mina.core.session.IoSession)}.
     * @throws Exception 
     */
    @Test
    public void testGetEncoder() throws Exception {
        ProtocolEncoder encoder1 = factory.getEncoder(session); 
        ProtocolEncoder encoder2 = factory.getEncoder(session); 
        assertNotNull(encoder1);
        assertNotNull(encoder2);
        assertEquals(TncEscapeEncoder.class, encoder1.getClass());
        assertEquals(TncEscapeEncoder.class, encoder2.getClass());
        assertSame(encoder1, encoder2);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncEscapeCodecFactory#getDecoder(org.apache.mina.core.session.IoSession)}.
     * @throws Exception 
     */
    @Test
    public void testGetDecoder() throws Exception {
        ProtocolDecoder decoder1 = factory.getDecoder(session);
        ProtocolDecoder decoder2 = factory.getDecoder(session);
        assertNotNull(decoder1);
        assertNotNull(decoder2);
        assertEquals(TncEscapeDecoder.class, decoder1.getClass());
        assertEquals(TncEscapeDecoder.class, decoder2.getClass());
        assertSame(decoder1, decoder2);
        inOrder.verifyNoMoreInteractions();
    }
}
