package eu.estcube.gs.tnc.codec;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.transport.tnc.TncFrame;

/**
 *
 */
public class TncFrameEncoder extends ProtocolEncoderAdapter {

    private static final Logger LOG = LoggerFactory.getLogger(TncFrameEncoder.class);
    
    /** @{inheritDoc}. */
    @Override
    public void encode(IoSession session, Object message, ProtocolEncoderOutput out) throws Exception {
        if (!(message instanceof TncFrame)) {
            LOG.error("Unsupported message type in {} - {}. Only supported type is TncFrame. Throwing exception", getClass().getSimpleName(), message.getClass().getName());
            throw new RuntimeException("Unsupported message type in " + getClass().getName() + " - " + message.getClass().getName());
        }
        
        TncFrame frame = (TncFrame) message;
        byte[] bytes = TncFrames.toBytes(frame);
        // Can't send bytes as IoBuffer - Mina will skip other downstream codecs in this case
        // sending as byte array
        out.write(bytes);
    }
}
