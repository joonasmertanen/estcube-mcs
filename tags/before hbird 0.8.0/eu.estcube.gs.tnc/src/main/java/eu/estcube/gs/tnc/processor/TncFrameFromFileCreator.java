package eu.estcube.gs.tnc.processor;

import java.io.File;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.estcube.domain.transport.tnc.TncFrame;
import eu.estcube.domain.transport.tnc.TncFrame.TncCommand;

/**
 *
 */
@Component
public class TncFrameFromFileCreator {

    public static final int DEFAULT_PORT = 0;
    public static final int MIN_PORT = 0;
    public static final int MAX_PORT = 15;
    
    private static final Logger LOG = LoggerFactory.getLogger(TncFrameFromFileCreator.class);
    
    public TncFrame createFrame(@Header("CamelFileParent") String parentPath, @Body byte[] bytes) throws Exception {
        int port = DEFAULT_PORT;
        String parent = new File(parentPath).getName();
        try {
            port = Integer.parseInt(parent);
        } catch (NumberFormatException nfe) {
            LOG.warn("Failed to parse target port name from folder name: {}. Using default value: {}", parent, port);
        }
        if (port < MIN_PORT || port > MAX_PORT) {
            LOG.warn("Invalid value for port {}. Valid values are from {} to {}. Using default value: {}", new Object[] { parent, MIN_PORT, MAX_PORT, DEFAULT_PORT });
            port = DEFAULT_PORT;
        }
        TncFrame frame = new TncFrame(TncCommand.DATA, port, bytes);
        return frame;
    }
}
