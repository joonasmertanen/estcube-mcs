/** 
 *
 */
package eu.estcube.domain.hbird.extension;

import org.hbird.exchange.core.Binary;

/**
 * Class to extend {@link Binary} by adding type field.
 * 
 * This class is missing in Hbird 0.7.0.
 */
public class ExctendedBinary extends Binary {

    private static final long serialVersionUID = 5724344711102544081L;

    /** Field to store image type information. */
    protected final String type;

    /**
     * Creates new WebcamImage.
     * 
     * @param issuedBy
     * @param name
     * @param description
     * @param rawData
     */
    public ExctendedBinary(String issuedBy, String name, String description, byte[] rawData, String type) {
        super(issuedBy, name, description, rawData);
        this.type = type;
    }

    /**
     * Returns the type of the image.
     * 
     * @return type of the image
     */
    public String getType() {
        return type;
    }
}
