package eu.estcube.domain.transport;

public enum Direction {
    UP("UP"),
    DOWN("DOWN");

    private final String name;

    Direction(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
