/** 
 *
 */
package eu.estcube.domain.hbird.extension;

import org.hbird.exchange.core.Command;

/**
 * Extending the {@link Command} class. Adding field commandToExecute.
 * 
 * This class is missing in Hbird 0.7.0.
 */
public class ExtendedNativeCommand extends Command {

    private static final long serialVersionUID = 4882872382860139036L;

    public static final String DESCRIPTION = "Native command with destination";
    public static final String DEFAULT_NAME = ExtendedNativeCommand.class.getSimpleName();

    protected String commandToExecute;

    /**
     * Creates new NativeCommandWithDestination.
     * 
     * @param issuedBy
     * @param destination
     * @param name
     * @param description
     */
    public ExtendedNativeCommand(String issuedBy, String destination, String name, String commandToExecute) {
        super(issuedBy, destination, name, DESCRIPTION);
        this.commandToExecute = commandToExecute;
    }

    public ExtendedNativeCommand(String issuedBy, String destination, String commandToExecute) {
        this(issuedBy, destination, DEFAULT_NAME, commandToExecute);
    }

    /**
     * Returns command.
     * 
     * @return the command
     */
    public String getCommandToExecute() {
        return commandToExecute;
    }

    /**
     * Sets command.
     * 
     * @param command the command to set
     */
    public void setCommandToExecute(String commandToExecute) {
        this.commandToExecute = commandToExecute;
    }
}
