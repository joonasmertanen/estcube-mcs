package eu.estcube.archiver;

import java.sql.SQLException;

import javax.sql.DataSource;

import oracle.jdbc.pool.OracleDataSource;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.hbird.exchange.core.BusinessCard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import eu.estcube.archiver.raw.ax25.Ax25ToSqlProvider;
import eu.estcube.archiver.raw.ax25.OracleAx25FrameArchiver;
import eu.estcube.archiver.raw.camel.FrameArchiverProcessor;
import eu.estcube.archiver.raw.tnc.OracleTncFrameArchiver;
import eu.estcube.archiver.raw.tnc.TncToSqlProvider;
import eu.estcube.common.PrepareForInjection;
import eu.estcube.domain.transport.Direction;
import eu.estcube.domain.transport.Downlink;
import eu.estcube.domain.transport.Uplink;

public class Archiver extends RouteBuilder {
	private static final Logger LOG = LoggerFactory.getLogger(Archiver.class);

	@Value("${service.id}")
	private String serviceId;

	@Value("${heart.beat.interval}")
	private int heartBeatInterval;

	@Value("${oracle.url}")
	private String oracleUrl;

	@Autowired
	private PrepareForInjection preparator;

	private FrameArchiverProcessor ax25DownlinkArchiver;
	private FrameArchiverProcessor ax25UplinkArchiver;
	private FrameArchiverProcessor tncUplinkArchiver;
	private FrameArchiverProcessor tncDownlinkArchiver;

	private DataSource dataSource;

	private void createDataSource() throws SQLException {
		dataSource = new OracleDataSource();
		((OracleDataSource) dataSource).setURL(oracleUrl);

		LOG.info("Testing connection to database");
		dataSource.getConnection();
		LOG.info("Connection successful");
	}

	private void createProcessors() {
		ax25DownlinkArchiver = new FrameArchiverProcessor(
				new OracleAx25FrameArchiver(dataSource), new Ax25ToSqlProvider(
						Direction.DOWN));

		ax25UplinkArchiver = new FrameArchiverProcessor(
				new OracleAx25FrameArchiver(dataSource), new Ax25ToSqlProvider(
						Direction.UP));

		tncDownlinkArchiver = new FrameArchiverProcessor(
				new OracleTncFrameArchiver(dataSource), new TncToSqlProvider(
						Direction.DOWN));

		tncUplinkArchiver = new FrameArchiverProcessor(
				new OracleTncFrameArchiver(dataSource), new TncToSqlProvider(
						Direction.UP));
	}

	@Override
	public void configure() throws Exception {
		LOG.info("Creating data source");
		createDataSource();

		LOG.info("Creating processors");
		createProcessors();

		LOG.info("Creating routes with processors");
		from(Downlink.AX25_FRAMES).process(ax25DownlinkArchiver);
		from(Uplink.AX25_FRAMES_LOG).process(ax25UplinkArchiver);

		from(Downlink.FROM_TNC).process(tncDownlinkArchiver);
		from(Uplink.TNC_FRAMES_LOG).process(tncUplinkArchiver);

		BusinessCard card = new BusinessCard(serviceId, heartBeatInterval);
		card.setDescription("Frame archiver");
		from("timer://heartbeat?fixedRate=true&period=" + heartBeatInterval)
				.bean(card, "touch").process(preparator)
				.to("activemq:topic:hbird.monitoring");
	}

	public static void main(String[] args) throws Exception {
		LOG.info("Starting Archiver");
		new Main().run(args);
	}
}