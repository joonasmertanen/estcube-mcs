package eu.estcube.archiver.raw.ax25;

import javax.sql.DataSource;

import eu.estcube.archiver.raw.sql.AbstractSqlFrameArchiver;

public class OracleAx25FrameArchiver extends AbstractSqlFrameArchiver {

	public OracleAx25FrameArchiver(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	public String getAutoIncrementFunction() {
		return "MCS.AX25_FRAMES_SEQ.NEXTVAL";
	}

	@Override
	public String getDetailsAutoIncrementFunction() {
		return "MCS.AX25_FRAME_DETAILS_SEQ.NEXTVAL";
	}

	@Override
	public String[] getColumns() {
		return new String[] { Ax25ToSqlProvider.COL_RECEPTION_TIME,
				Ax25ToSqlProvider.COL_SATELLITE,
				Ax25ToSqlProvider.COL_DESTADDR, Ax25ToSqlProvider.COL_SRCADDR,
				Ax25ToSqlProvider.COL_CTRL, Ax25ToSqlProvider.COL_PID,
				Ax25ToSqlProvider.COL_INFO, Ax25ToSqlProvider.COL_FCS,
				Ax25ToSqlProvider.COL_ERROR_BITMASK };
	}

	@Override
	public String getTable() {
		return "MCS.AX25_FRAMES";
	}

	@Override
	public String getDetailsTable() {
		return "MCS.AX25_FRAME_DETAILS";
	}

	@Override
	public String getTimestampFunction() {
		return "SYSDATE";
	}
}
