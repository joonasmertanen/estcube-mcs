package eu.estcube.archiver.raw.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.archiver.raw.ArchivingException;
import eu.estcube.archiver.raw.FrameArchiver;
import eu.estcube.archiver.raw.FrameDataProvider;

public abstract class AbstractSqlFrameArchiver implements FrameArchiver {
	private static final Logger LOG = LoggerFactory
			.getLogger(AbstractSqlFrameArchiver.class);

	public static final String ID = "id";
	public static final String CREATED = "created";
	public static final String DIRECTION = "direction";

	public static final String DETAILS_FRAME_ID = "frame_id";
	public static final String DETAILS_NAME = "name";
	public static final String DETAILS_VALUE = "value";

	private DataSource dataSource;
	private PreparedStatement psInsert;
	private PreparedStatement psInsertDetails;

	public AbstractSqlFrameArchiver(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public String createInsert(String table, String idColumn,
			String directionColumn, String createdColumn, String[] columns,
			String autoIncrementFunction, String timestampFunction) {
		StringBuilder b = new StringBuilder("INSERT INTO ").append(table);

		b.append(" (").append(idColumn);
		b.append(", ").append(directionColumn);

		for (int i = 0; i < columns.length; i++) {
			b.append(", ");
			b.append(columns[i]);
		}
		b.append(", ").append(createdColumn);

		b.append(") VALUES (").append(autoIncrementFunction);
		b.append(", ?"); // direction
		for (int i = 0; i < columns.length; i++) {
			b.append(", ");
			b.append("?");
		}
		b.append(", ").append(timestampFunction).append(")");

		return b.toString();
	}

	public String createInsertDetails(String table, String idColumn,
			String masterIdColumn, String createdColumn, String nameColumn,
			String valueColumn, String autoIncrementFunction,
			String timestampFunction) {
		return new StringBuilder("INSERT INTO ").append(table).append(" (")
				.append(idColumn).append(", ").append(nameColumn).append(", ")
				.append(valueColumn).append(", ").append(masterIdColumn)
				.append(", ").append(createdColumn).append(") VALUES (")
				.append(autoIncrementFunction).append(", ").append("?, ?, ?, ")
				.append(timestampFunction).append(")").toString();
	}

	@Override
	public void store(FrameDataProvider frameDataProvider)
			throws ArchivingException {

		SqlFrameDataProvider sqlFrameDataProvider = (SqlFrameDataProvider) frameDataProvider;

		LOG.info("Storing frame to {}, direction {}", getTable(),
				frameDataProvider.getDirection());

		try {
			Connection c = dataSource.getConnection();

			PreparedStatement psInsert = getInsertStatement(c);
			PreparedStatement psInsertDetails = getInsertDetailsStatement(c);

			Long masterId = insertMaster(psInsert, sqlFrameDataProvider);
			insertDetails(psInsertDetails, sqlFrameDataProvider, masterId);

		} catch (SQLException e) {
			LOG.error("Storage failed with '{}'", e.getMessage());
			throw new ArchivingException(
					"SQLException while storing data to database", e);
		}
	}

	protected PreparedStatement getInsertStatement(Connection connection)
			throws SQLException {

		if (psInsert == null) {
			String sql = createInsert(getTable(), ID, DIRECTION, CREATED,
					getColumns(), getAutoIncrementFunction(),
					getTimestampFunction());
			psInsert = connection.prepareStatement(sql, new String[] { ID });
		}

		return psInsert;

	}

	protected PreparedStatement getInsertDetailsStatement(Connection connection)
			throws SQLException {

		if (psInsertDetails == null) {
			psInsertDetails = connection.prepareStatement(createInsertDetails(
					getDetailsTable(), ID, DETAILS_FRAME_ID, CREATED,
					DETAILS_NAME, DETAILS_VALUE,
					getDetailsAutoIncrementFunction(), getTimestampFunction()));
		}

		return psInsertDetails;

	}

	protected Long insertMaster(PreparedStatement psInsert,
			SqlFrameDataProvider sqlFrameDataProvider) throws SQLException {

		Long id = null;

		int i = 1;
		psInsert.setObject(i++, sqlFrameDataProvider.getDirection().getName());
		for (String column : getColumns()) {
			psInsert.setObject(i++, sqlFrameDataProvider.getValue(column));
		}

		psInsert.executeUpdate();

		ResultSet rsId = psInsert.getGeneratedKeys();
		if (rsId.next()) {
			id = rsId.getLong(1);
		}
		rsId.close();

		return id;
	}

	protected void insertDetails(PreparedStatement psInsertDetails,
			SqlFrameDataProvider sqlFrameDataProvider, Long masterId)
			throws SQLException {

		psInsertDetails.setLong(3, masterId);
		Map<String, String> detailsMap = sqlFrameDataProvider.getDetails();

		for (Map.Entry<String, String> entry : detailsMap.entrySet()) {
			if (entry.getValue() != null) {
				psInsertDetails.setString(1, entry.getKey());
				psInsertDetails.setString(2, entry.getValue());
				psInsertDetails.addBatch();
			}
		}

		psInsertDetails.executeBatch();

	}

	public abstract String getAutoIncrementFunction();

	public abstract String getDetailsAutoIncrementFunction();

	public abstract String[] getColumns();

	public abstract String getTable();

	public abstract String getDetailsTable();

	public abstract String getTimestampFunction();

}
