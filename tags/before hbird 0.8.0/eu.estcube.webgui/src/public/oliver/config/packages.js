var _siteRoot = "/oliver/"
var packages = [
	{ name : "config", 		location : _siteRoot+"config"},
	
	{ name : "system",		location : _siteRoot+"js/system"},
	
	{ name : "log",			location : _siteRoot+"js/module/log", isModule:true},
	{ name : "messages",			location : _siteRoot+"js/module/messages", isModule:true},
	{ name : "exampleModule",			location : _siteRoot+"js/module/exampleModule", isModule:true},
	
	{ name : "components",			location : _siteRoot+"js/module/components", isModule:true},
	
	// ES5EC packages
	{ name : "ES5ECWebCam",			location : _siteRoot+"js/groundstation/ES5EC/webcam", isModule:true},
	{ name : "ES5ECWebCamTmp",			location : _siteRoot+"js/groundstation/ES5EC/webcam_tmp", isModule:true},
];