define(["dojo/dom", "dojo/domReady!"], function( dom ) {
	return {
		routes: {
			"components": {
				"path": "components/",
				"defaults": {
					"controller": "components/controller/ComponentsController",
					"method": "index",
					"killMethod": "removed"
				}
			},
		}
	};
});
