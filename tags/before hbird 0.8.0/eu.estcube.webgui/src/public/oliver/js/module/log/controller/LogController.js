define([
	"dojo/_base/declare",
	"system/Controller", 
	"./../view/LogView",
], 
function( 
	declare,
	Controller,
	LogWidget 
) {
	var s = declare( [Controller], {
		display: function( params ) {
			this.placeWidget( LogWidget() );
		},
		
		element: function( params ) {
			this.placeHtml( "element "+params.id );
		},
		
		setup: function() {
			console.log("Log setup");
		},
		
		destroy: function() {
			console.log("Log destroyed");
		}
	});
	return new s();
});
