define([], function() {
	return {
		routes: {
			"messages": {
				"path": "messages/",
				"defaults": {
					"controller": "messages/controller/MessagesController",
					"method": "display"
				}
			},
		}
	};
});
