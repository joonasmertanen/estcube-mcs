define([], function() {
	return {
		
		routes: {
			"exampleModule": {
				"path": "example_module/",
				"defaults": {
					"controller": "exampleModule/controller/ExampleController",
					"method": "index",
					"killMethod": "kill",
					"setupMethod": "setup",
				},
				"childRoutes": {
					"item":{
						"path": ":id",
						"defaults": {
							"method": "element"
						}
					},
				}
			},
		},
		EXAMPLE_MODULE_SPECIFIC_CONFIG: "haha"
	};
});
