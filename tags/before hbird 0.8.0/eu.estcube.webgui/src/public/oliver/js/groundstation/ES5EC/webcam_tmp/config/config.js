define(["dojo/dom", "dojo/domReady!"], function( dom ) {
	return {
		routes: {
			"ES5EC_webcam_tmp": {
				"path": "webcam_tmp/",
				"defaults": {
					"controller": "ES5ECWebCamTmp/controller/WebCamController",
					"method": "index",
				}
			},
		}
	};
});
