define([
	"config/config",
	"system/net/WebSocketProxy",
	"dojo/store/Memory",
    "dojo/store/Observable",
	],
	function(config, WebSocketProxy, Memory, Observable ){
		var channel = config.ES5EC.webcamChannel;	
		var proxy = new WebSocketProxy({ delay: 1000, channels: [channel] });
		return proxy;
	}
);