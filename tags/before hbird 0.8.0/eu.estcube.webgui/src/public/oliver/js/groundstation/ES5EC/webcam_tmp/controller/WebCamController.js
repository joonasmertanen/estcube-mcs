define([
	"dojo/_base/declare",
	"system/Controller", 
	"./../view/WebCamView",
], 
function( 
	declare,
	Controller,
	WebCamView
) {
	var s = declare( [Controller], {
		
		index: function( params ) {	
			this.placeWidget( this.view );			
		},
		
		setup: function() {
			if( !this.view ) {
				this.view = new WebCamView();
			}
		},
		
		destroy: function() {
			//this.view.destroy();
		}
		
	});
	return new s();
})
