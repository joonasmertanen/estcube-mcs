define([
	"dojo/_base/declare",
	"dojo/_base/array",
	"dojo/_base/lang",
	"dojo/on",
	"dojo/request",
	"dijit/Toolbar",
	"dijit/DropDownMenu",
	"dijit/form/DropDownButton",
	"dijit/form/Button",
	"dijit/MenuItem",
	"dijit/PopupMenuItem",
	"dijit/MenuSeparator",
	"dijit/ToolbarSeparator",
	],
	
	function(declare, Arrays, Lang, on, Request, Toolbar, DropDownMenu, DropDownButton, Button, MenuItem, PopupMenuItem, MenuSeparator, ToolbarSeparator ) {
 
		this.init = function( tree, container ) {
			
			console.log("Initializing menu...");
			
			this.toolbar = new Toolbar({});
			toolbar.placeAt( container );
			
			for( var i in tree ) {
				this.buildToolbar( tree[i], this.toolbar );
			}
			
			console.log("Menu initialized");
		}
		
		this.getTooltip = function(item) {
            return item.tooltip == null ? item.label : item.tooltip;
        }
		
		this.createButton = function( item ) {
			return new Button({
				label: item.label,
				showLabel: true,
                title: this.getTooltip(item),
                style: item.style,
			});
		}
		
		this.createDropDownButton = function( item ) {
			return new DropDownButton({
				label: item.label,
				dropDown: new DropDownMenu({
					label: item.label,
					showLabel: true,
               		title: this.getTooltip(item),
				}),
			});	
		}
		
		this.createMenuItem = function( item ) {
			return new MenuItem({
				label: item.label
			});
		}
		
		this.createDropDownMenuItem = function( item ) {
			return new PopupMenuItem({
				label: item.label,
				popup: new DropDownMenu({
					label: item.label
				}),
			});
		}
		
		this.bind = function(node, url) {
            on(node, "click", function() {
                document.location.href = url;
            });
        }
		
		
		this.buildToolbar = function( item, toolbar ) {	
			var element = null;
			
			if( item.type == "separator" ) {
				element = new ToolbarSeparator({
					
				});
			} else if( item.children ) {			
				element = this.createDropDownButton( item );		
				for( var i in item.children ) {
					this.buildSubMenu( item.children[i], element.dropDown );
				}	
						
			} else {
				element = this.createButton( item );
				this.bind( element, item.url )	
			}
				
			toolbar.addChild( element );
		};
		
		this.buildSubMenu = function( item, parent ) {
			var element = null;
			
			if( item.type == "separator") {
				element = new MenuSeparator({
					
				});
				
			} else if( item.children ) {				
				element = this.createDropDownMenuItem( item );
				for( var i in item.children ) {
					this.buildSubMenu( item.children[i], element.popup );		
				}
				
			} else {
				element = this.createMenuItem( item );
				this.bind( element, item.url )
			}
			
			parent.addChild( element );
			
		}
		
		return this;
		
	}
);