require([
	"dojo/dom",
	"system/ConfigLoader",
	"system/Router", 
	"dojo/domReady!"], 
function( 
	dom, 
	ConfigLoader, 
	router,
	StackContainer,
	parser
) {
	ConfigLoader.load( packages, function() { 
	
		router.init( ConfigLoader.getConfig2().routes, dom.byId("content") );
		router.goToCurrent();
		
		require(["system/Menu", config.MENU_STRUCTURE_CONFIG_MODULE ], function( menu, menuConfig ) {
			console.log("Menu config loaded");
			menu.init( menuConfig.menu, dom.byId("menu") );
		});
	
	});
});