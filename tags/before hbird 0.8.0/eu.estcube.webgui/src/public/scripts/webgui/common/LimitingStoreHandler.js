define([
    "dojo/_base/declare",
    ],

    function(declare) {
        return [

            declare("LimitingStoreHandler", null, {

                handle: function(store, item, limit) {
                    store.put(item);
                    var overLimit = store.data.length - limit;
                    if (overLimit > 0) {
                        store.query({}, { start: 0, count: overLimit }).forEach(function(item) {
                            var id = item[store.idProperty];
                            store.remove(id);
                        });
                    }
                },

            }),
        ];
    }
);
