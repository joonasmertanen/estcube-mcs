define([
    "dojo/_base/declare",
	"dojo/dom",
	"dojo/date/locale",
	"dojo/_base/event",
	"dojo/_base/lang",
	"./Constants",
	"dijit/form/Button"
], 
function(declare, dom, locale, event, lang, Constants, Button) {
	
	var current_time = new Date();
	var text_time = null;
	
    return [
           
	    declare("Clock", null, {
	    	
	    	build : function() {
	    		
		    	reflectTime = function(time) {
					if(!time) {
						time = current_time;
					}
					text_time.innerHTML = locale.format(time, {
						selector: "time",
						timePattern: Constants.DEFAULT_DATE_FORMAT
					});
					
				};
				
				tick = function() {
					current_time.setSeconds(current_time.getSeconds() + 1);
					this.reflectTime();
				};
				
				createClock = function() {
					text_time = dom.byId("time");
					this.checkCookie();
					this.recoverTimeFormatBySignUp();
					window.setInterval(lang.hitch(this, "tick"), 1000);
					this.createTimeFormatButton();
				};
				
				getCurrentTimeFormatFromCookie = function() {
					return this.getCookie("timeFormat");
				};
				
				recoverTimeFormatBySignUp = function() {
					var format = getCurrentTimeFormatFromCookie();
					if (format == "local") {
						this.setTimeToLocal();
					}
					else {
						this.setTimeToUTC();
					}
					this.displayCurrentTimeFormat();
				};
				
				formatTime = function() {
					var format = getCurrentTimeFormatFromCookie();
					if (format == "local") {
						this.setTimeToUTC();
						this.setCookie("timeFormat", "utc", Constants.EXPIRATION_DATE);
					}
					else {
						this.setTimeToLocal();
						this.setCookie("timeFormat", "local", Constants.EXPIRATION_DATE);
					}
					this.displayCurrentTimeFormat();
				};
				
				setTimeToLocal = function() {
					current_time = new Date();
				};
				
				setTimeToUTC = function() {
					var utc = new Date(new Date().getTime() + new Date().getTimezoneOffset() * 60000);
					current_time = utc;
				};
				
				createTimeFormatButton = function() {
					var self = this;
					var createTimeFormatButton = new Button({
						label : "Change time format",
						onClick : function() {
							self.formatTime();
						}
					}, "changeTimeFormatButton");
		    	};		    

		    	checkCookie = function() {
			    	var format = getCookie("timeFormat");
			    	if (format == null || format == "") {
			    		this.setCookie("timeFormat", "utc", Constants.EXPIRATION_DATE);
			    	}
		    	};
		    	
		    	setCookie = function(cookie_name, value, exdays) {
			    	var exdate = new Date();
			    	exdate.setDate(exdate.getDate() + exdays);
			    	var cookie_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString()) + "; path=/MCS/";
			    	document.cookie = cookie_name + "=" + cookie_value;
		    	};
		    	
		    	getCookie = function(cookie_name) {
			    	var i, x, y, ARRcookies = document.cookie.split(";");
			    	for (i = 0; i < ARRcookies.length; i++) {
			    	  x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
			    	  y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
			    	  x = x.replace(/^\s+|\s+$/g,"");
			    	  if (x == cookie_name) {
			    		  return unescape(y);
			    	  }
			    	}
		    	};
		    	
		    	displayCurrentTimeFormat = function() {
		    		currentTimeFormat = dom.byId("timeFormat");
		    		var format = getCurrentTimeFormatFromCookie();
		    		currentTimeFormat.innerHTML = format.toUpperCase();
		    	};
		    	
		    	createClock();
	    	}
			
	    })
	    
	];

});