define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dojo/on",
    "dijit/registry",
    "dijit/form/Form",
    "dijit/form/Button",
    "dijit/form/ValidationTextBox",
    "dijit/form/SimpleTextarea",
    "dijit/layout/ContentPane",
    "dojox/layout/TableContainer",
    "dgrid/OnDemandList",
    "../common/Constants",
    "../common/Utils",
    "./ViewBase"
    ],

    function(declare, Arrays, Lang, DomConstruct, On, Registry, Form, Button, ValidationTextBox, SimpleTextarea, ContentPane, TableContainer, List, Constants, Utils, ViewBase) {

        return [


            declare("TleView", View, {

                build: function() {

                    var div = DomConstruct.create("div", { });
                    var form = new Form({ encType: "multipart/form-data", action: "", method: "" }, div);

                    var table = new TableContainer({
                        cols: 1,
                        labelWidth: "100px",
                    });

                    var textSource = new ValidationTextBox({ name: "tleSource", label: "TLE Source", placeHolder: "Source of TLE", required: true });
                    var textTleValue = new SimpleTextarea({ name: "tleText", label: "TLE Value", cols: 70, rows: 2 });
                    var buttonWrapper = new ContentPane({ style: "margin: 0px; padding: 0px;" });
                    var buttonSubmit = new Button({ label: "Submit", title: "Upload TLE" });
                    buttonWrapper.addChild(buttonSubmit);

                    table.addChild(textSource);
                    table.addChild(textTleValue);
                    table.addChild(buttonWrapper);

                    form.domNode.appendChild(table.domNode);

                    var tleList = new List({
                        store: this.store,
                        query: {},
                        renderRow: function(tle) {
                            var div = DomConstruct.create("div", { style: "padding: 3px; margin: 1px;" });
                            var spanDate = DomConstruct.create("span", { innerHTML: Utils.formatDate(tle.timestamp), style: "font-weight: bold; margin-right: 2em"}, div);
                            var spanDate = DomConstruct.create("span", { innerHTML: tle.satellite, style: "font-style: italic; margin-right: 2em"}, div);
                            var spanDate = DomConstruct.create("span", { innerHTML: "Uploader: TODO", style: "font-style: italic; margin-right: 2em; color: #dddddd"}, div);
                            var line1 = DomConstruct.create("p", { innerHTML: tle.tleLine1, style: "padding: 1px; margin: 1px; font-weight: normal;" }, div);
                            var line2 = DomConstruct.create("p", { innerHTML: tle.tleLine2, style: "padding: 1px; margin: 1px; font-weight: normal;" }, div);
                            return div;
                        },
                    });
                    tleList.set("sort", "timestamp", true);

                    var container = Registry.byId(this.parentDivId);
                    container.domNode.appendChild(form.domNode);
                    container.domNode.appendChild(tleList.domNode);

                    table.startup();
                    tleList.startup();

                    On(buttonSubmit, "click", function() {
                        if (form.validate()) {
                            var value = form.get("value");
                            dojo.publish("tle/submit", [value]);
                            form.reset();
                        } else {
                            alert("Invalid data! Fix errors and resubmit!");
                        }
                    });
                },

            }),

            declare("TleController", Controller, {

                eventHandlers: [{ event: "tle/submit", handler: "onTleSubmit" }],

                constructor: function(args) {
                    this.loadData();
                },

                channelHandler: function(message, channel) {
                    if (message.type === "TleOrbitalParameters") {
                        this.store.put(message);
                    }
                },

                onTleSubmit: function(tle) {
                    console.log("Submitting TLE: " + JSON.stringify(tle));
                    var data = {
                        url: "/tle/submit",
                        content: tle,
                        handleAs: "json",
                        error: Lang.hitch(this, this.onServerError),
                        load: Lang.hitch(this, this.onServerSuccess),
                    }
                    dojo.xhrPost(data);
                },

                onServerError: function(error) {
                    // TODO - 26.02.2013 kimmell - report error
                    alert(error);
                    cosole.error(error);
                },

                onServerSuccess: function (data) {
                    // TODO - 26.02.2013 kimmell - report success
                    console.log("TLE submitted successfully to web server");
                },

                loadData: function() {
                    var request = {
                        url: "/tle",
                        handleAs: "json",
                        error: Lang.hitch(this, this.onServerError),
                        load: Lang.hitch(this, this.handleData),
                    };
                    dojo.xhrGet(request);
                },

                handleData: function(data) {
                    Arrays.forEach(data, function(item, index) {
                        this.store.put(item);
                    }, this);
                },
            }),

        ];
});