define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/_base/lang",
    "dojo/on",
    "dojo/request",
    "dijit/Toolbar",
    "dijit/DropDownMenu",
    "dijit/form/DropDownButton",
    "dijit/form/Button",
    "dijit/MenuItem",
    "dijit/PopupMenuItem",
    "./ViewBase",
    ],

    /**
     * Builds menu based on data in store.
     * Listen changes in store and adjust menu accordingly.
     *
     * TODO "priority" property is not used on sub menu building
     * TODO is not reacting on data removal from store
     */
    function(declare, Arrays, Lang, on, Request, Toolbar, DropDownMenu, DropDownButton, Button, MenuItem, PopupMenuItem, ViewBase) {

        function getTooltip(item) {
            return item.tooltip == null ? item.label : item.tooltip;
        }

        function bind(node, url) {
            on(node, "click", function() {
                document.location.href = url;
            });
        }

        function createButton(item, isPrimary) {
            return new Button({
                label: item.label,
                showLabel: true,
                title: getTooltip(item),
                style: isPrimary === false ? "float:right" : "",
                menuId: item.id,
            });
        }

        function createDropDownMenu(item, isPrimary) {
            var menu = new DropDownMenu({});
            var button = new DropDownButton({
                label: item.label,
                title: getTooltip(item),
                style: isPrimary === false ? "float:right" : "",
                menuId: item.id,
                dropDown: menu,
            });
            return button;
        }

        function createMenuItem(item) {
            return new MenuItem({
                label: item.label,
                title: getTooltip(item),
                menuId: item.id,
            });
        }

        function createPopupMenuItem(item) {
            var menu = new DropDownMenu({});
            var popup = new PopupMenuItem({
                label: item.label,
                title: getTooltip(item),
                menuId: item.id,
                popup: menu,
            });
            return popup;
        }

        function remove(parent, child) {
            var index = parent.getIndexOfChild(child);
            parent.removeChild(child);
            child.destroyRecursive(false);
            return index;
        }

        function findWidget(parent, childId) {
            var result;
            Arrays.forEach(parent.getChildren(), function(child, index) {
                if (child.menuId === childId) {
                    result = child;
                }
            });
            return result;
        }

        function buildMenuBar(menubar, store, item, isPrimary, idx) {
            var button = createButton(item, isPrimary);
            bind(button, item.url);
            menubar.addChild(button, idx);

            store.query({ parentId: item.id }).observe(function(child, removedFrom, insertedInto) {
                var widget = findWidget(menubar, item.id);
                if (typeof(widget.addChild) != "function") {
                    var index = remove(menubar, widget);
                    widget = createDropDownMenu(item, isPrimary);
                    menubar.addChild(widget, index);
                }
                buildMenuItem(widget.dropDown, store, child);
            }, true);
        }

        function buildMenuItem(parent, store, item) {
            var menuItem = createMenuItem(item);
            bind(menuItem, item.url);
            parent.addChild(menuItem, item.priority); // FIXME - 04.03.2013, kimmell - not working

            store.query({ parentId: item.id }).observe(function(child, removedFrom, insertedInto) {
                var widget = findWidget(parent, item.id);
                if (!widget.popup) {
                    var index = remove(parent, widget);
                    widget = createPopupMenuItem(item);
                    parent.addChild(widget, index);
                }
                buildMenuItem(widget.popup, store, child);
            }, true);
        }


        return [

            declare("MenuBarView", View, {

                build: function() {
                    var bar = new Toolbar({});
                    bar.placeAt(this.parentDivId);

                    // add primary main menu items (left side)
                    this.store.query(Lang.hitch(this, function(item) {
                        return Arrays.indexOf(this.primaryItems, item.id) > -1;
                    })).observe(Lang.hitch(this, function(item, removedFrom, insertedInto) {
                        var index = Arrays.indexOf(this.primaryItems, item.id);
                        buildMenuBar(bar, this.store, item, true, index);
                    }), true);

                    // add secondary main menu items (right side)
                    this.store.query(Lang.hitch(this, function(item) {
                        return Arrays.indexOf(this.secondaryItems, item.id) > -1;
                    })).observe(Lang.hitch(this, function(item, removedFrom, insertedInto) {
                        var index = this.primaryItems.length + Arrays.indexOf(this.secondaryItems, item.id);
                        buildMenuBar(bar, this.store, item, false, index);
                    }), true);
                },
            }),

        ];
    }
);
