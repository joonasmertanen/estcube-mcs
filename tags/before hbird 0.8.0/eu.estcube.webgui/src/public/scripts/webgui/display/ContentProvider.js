define([
    "dojo/_base/declare",
    "dijit/layout/ContentPane",
    ],

    function(declare, ContentPane) {
        return [
            declare("ContentProvider", null, {

                constructor: function(args) {
                    declare.safeMixin(this, args);
                    console.log("[ContentProvider] init " + this.declaredClass);
                },

                getContent: function() {
                    var result = new ContentPane({ content: "Function getContent() not implemented in " + this.declaredClass + "!" });
                    return result;
                },

                startup: function() {
                    // can be implemented in sub-classes
                },

            }),

        ];
    }
);