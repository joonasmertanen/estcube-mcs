define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dijit/layout/ContentPane",
    "./ContentProvider",
    "dojox/dgauges/components/default/CircularLinearGauge",
    "dojox/dgauges/TextIndicator",
    ],

    function(declare, Lang, DomConstruct, ContentPane, ContentProvider, CircularLinearGauge, TextIndicator) {

        return [

            declare("CircularLinerGaugeProvider", ContentProvider, {

                gauge: null,

                getContent: function() {

                    var div = DomConstruct.create("div", { });
                    this.gauge = new CircularLinearGauge(this.gaugeSettings, div);

                    if (this.textIndicator != null) {
                        var text = new TextIndicator(this.textIndicator);
                        this.gauge.addElement("text", text);
                    }

                    var title = DomConstruct.create("div", {
                        innerHTML: this.gaugeSettings.title || " ",
                        style: this.gaugeSettings.titleStyle || "font-weight: bold;",
                    }, div, this.gaugeSettings.titlePlacement || "last" );

                    var pane = new ContentPane({ content: div });

                    this.store.query({ name: this.parameterName }).observe(Lang.hitch(this, function(item, removedFrom, insertedInto) {
                        var val = parseFloat(item.value);
                        val = Math.round(val * 100) / 100; // round to 2 decimal places
                        this.gauge.set("value", val);
                    }), true);

                    return pane;
                },

                startup: function() {
                    this.gauge.startup();
                },
            }),

        ];
    }
);