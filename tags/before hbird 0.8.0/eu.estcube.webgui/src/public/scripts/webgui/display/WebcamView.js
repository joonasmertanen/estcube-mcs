define([
    "dojo/_base/declare",
    "dojo/dom-construct",
    "dojo/Stateful",
    "../common/Constants",
    "../common/Utils",
    "./ViewBase",
    "../common/Filter",
    ],

    function(declare, construct, Stateful, Constants, Utils, ViewBase, Filter) {

        return [

            declare("WebcamController", Controller, {
                channelHandler: function(message, channel) {

                    // apply filter if exists
                    if (this.filter != null) {
                        if (Filter.reject(this.filter, message)) {
                            return;
                        }
                    }

                    message.storeId = this.imageId;
                    this.store.put(message);
                },
            }),

            declare("WebcamView", View, {

                build: function() {

                    // create img element
                    var img = construct.create("img", { alt: "Webcam image",  src: this.initialImage }, this.parentDivId, "first");

                    this.store.query({ storeId: this.imageId }).observe(function(value, removedFrom, insertedInto) {
                        if (!/image\//.test(value.type)) {
                            // no type set; return
                            return;
                        }
                        dojo.attr(img, { src : "data:" + value.type + ";base64," + value.rawdata, alt: value.name });
                    }, true);
                },
            }),

        ];
    }
);