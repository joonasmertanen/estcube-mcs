define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "dijit/registry",
    "dojox/layout/GridContainer",
    "dojox/widget/Portlet",
    "dojox/widget/PortletSettings",
    "dgrid/OnDemandGrid",
    "../common/Constants",
    "../common/Utils",
    "./ViewBase",
    "./ContentProvider",
    ],

    function(declare, Arrays, Registry, GridContainer, Portlet, PortletSettings, Grid, Constants, Utils, ViewBase, Provider) {
        return [

            declare("DashboardController", Controller, {
                channelHandler: function(message, channel) {
                    message.storeId = Utils.getParameterId(message);
                    var oldValue = this.store.get(message.storeId);
                    if (oldValue == null) {
                        // there is no value for the id in the store; add new value
                        this.store.put(message);
                    } else if (oldValue.timestamp < message.timestamp) {
                        // existing value has older timestamp than message; update
                        this.store.put(message);
                    }
                    // else message's timestamp is older than stored value timestamp; ignore
                }
            }),

            declare("DashboardView", View, {

                divId: "DashboardView",

                build: function() {
                    var gridContainer = new dojox.layout.GridContainer({
                            id: this.divId,
                            nbZones: this.columns,
                            opacity: .5,
                            minColWidth: "30%",
                            hasResizableColumns: false,
                            allowAutoScroll: true,
                            withHandles: true,
                            dragHandleClass: "dijitTitlePaneTitle",
                            style: {
                                width:"100%",
                            },
                            acceptTypes: ["Portlet"],
                            isOffset: true,
                        });

                    var store = this.store;

                    Arrays.forEach(this.config, function(item, index) {
                        var portlet = new dojox.widget.Portlet({
                            id: item.title,
                            dndType: "Portlet",
                            title: item.title,
                            closable: item.closable,
                        });

                        if (item.settings != null) {
                            var settings = new dojox.widget.PortletSettings({ innerHTML: item.settings });
                            portlet.addChild(settings);
                        }

                        var provider = item.contentProvider == null ? new ContentProvider() : item.contentProvider;
                        var content = provider.getContent();
                        portlet.addChild(content);
                        if (item.col != null && item.row != null) {
                            gridContainer.addChild(portlet, item.col, item.row);
                        } else {
                            gridContainer.addChild(portlet);
                        }
                        provider.startup();
                    });

                    var container = Registry.byId(this.parentDivId);
                    container.addChild(gridContainer);
                    gridContainer.startup();
                }
            })
        ];
    }
);

