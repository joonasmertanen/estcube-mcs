define([
    "dojo/_base/declare",
    "dojo/dom-construct",
    "dojo/store/Memory",
    "dojo/store/Observable",
    "./AssemblerBase",
    "../net/WebSocketProxy",
    "../display/DashboardView",
    "../display/WebcamView",
    "../common/Constants",
    "../common/Utils",
    "../common/DataLoader",
    "../display/MenuView",
    "../display/GridContentProvider",
    "../display/WebCamContentProvider",
    "../display/GenericContentProvider",
    "../display/ListContentProvider",
    "../display/GaugeContentProvider",
    "../display/CompositeContentProvider",
    ],

    function(declare, DomConstruct, Memory, Observable, base, proxy, view, WebcamView, Constants, Utils, DataLoader, MenuView, GridProvider, WebCamProvider, GenericProvider, ListProvider, GaugeProvider, CompositeProvider) {
        return [

            declare("DashboardAssembler", AssemblerBase, {

            	build: function(components) {
//                    this.setupCacheRequest({ delay: 1500, channels: ["/parameters"], });
                    this.setupCommunication();


                    var store = new Observable(new Memory({ idProperty: "storeId" }));
                    var pageStore = new Observable(new Memory( { idProperty: "id" }));

                    var menubar = new MenuBarView({
                        parentDivId: "menu",
                        store: pageStore,
                        primaryItems: ["system", "satellites", "groundStations"],
                        secondaryItems: ["logout"],
                    });

                    var controller = new DashboardController({ id: "Parameters Dashboard", channels: ["/hbird.out.all"], store: store });
                    var webcamController = new WebcamController({ id: "webcam", channels: ["/hbird.out.binary"], store: store, imageId: "GroundStation/ES5EC/webcam" });


                    var config = [

                        {
                            title: "Components",
                            settings: "MCS Components.",
                            contentProvider: new GridContentProvider({
                                columns: {
                                    issuedBy: { label: "Component ID" },
                                    host: { label: "Host" },
                                    timestamp: { label: "Time", formatter: Utils.formatDate },
                                },
                                store: store,
                                query: { type: "BusinessCard" },
                            }),
                            col: 0,
                            row: 0,
                        },

                        {
                            title: "ETC",
                            closable: true,
                            contentProvider: new GenericContentProvider({ content: "none" }),
                            col: 0,
                            row: 2,
                        },


                        {
                            title: "MCC stats",
                            settings: "System recources monitoring.",
                            contentProvider: new GridContentProvider({
                                columns: {
                                    name: { label: "Name" },
                                    value: { label: "Value" },
                                    unit: { label: "Unit" },
                                    timestamp: { label: "Time", formatter: Utils.formatDate },
                                },
                                store: store,
                                query: function(message) {
                                    return /^Host\/mcc\/.*/.test(message.name);
                                },
                            }),
                            col: 0,
                            row: 1,
                        },

                        {
                            title: "Satellites",
                            settings: "List of satellites",
                            contentProvider: new ListContentProvider({
                                store: pageStore,
                                query: { parentId: "satellites" },
                                formatter: function(item) {
                                    return DomConstruct.create("div", { innerHTML: item.label, style: "padding: 3px; margin: 1px" });
                                },
                            }),
                            col: 2,
                            row: 0,
                        },


                        {
                            title: "ES5EC WebCam",
                            contentProvider: new WebCamContentProvider({
                                store: store,
                                imageId: "GroundStation/ES5EC/webcam",
                                initialImage: "/images/image.jpg",
                            }),
                            col: 1,
                            row: 1,
                        },

                        {
                            title: "Temperatures",
                            settings: "Temperatures from different sources.",
                            contentProvider: new GridContentProvider({
                                columns: {
                                    issuedBy: { label: "Source" },
                                    value: { label: "°C"},
                                    timestamp: { label: "Time", formatter: Utils.formatDate },
                                },
                                store: store,
                                query: function(message) {
                                         return /.*\.temperature$/.test(message.name);
                                },
                            }),
                            col: 2,
                            row: 1,
                        },

                        {
                            title: "Air Temperature",
                            settings: "This is composite content provider including one or more other providers",
                            contentProvider: new CompositeContentProvider({
                                margin: 6,
                                providers: [
                                    new CircularLinerGaugeProvider({
                                        gaugeSettings: {
                                            value: 0,
                                            minimum: -50,
                                            maximum: 50,
                                            majorTickInterval: 10,
                                            minorTickInterval: 1,
                                            interactionArea: "none",
                                            animationDuration: 1000,
                                            style: "width: 180px; height: 200px;",
                                            title: "Meteo",
                                        },
                                        textIndicator: { x: 100, y: 170, value: "°C", },
                                        store: store,
                                        parameterName: "WeatherStation/meteo.physic.ut.ee/air.temperature",
                                    }),

                                    new CircularLinerGaugeProvider({
                                        gaugeSettings: {
                                            value: 0,
                                            minimum: -50,
                                            maximum: 50,
                                            majorTickInterval: 10,
                                            minorTickInterval: 1,
                                            interactionArea: "none",
                                            animationDuration: 1000,
                                            style: "width: 180px; height: 200px;",
                                            title: "EMHI",
                                        },
                                        textIndicator: { x: 100, y: 170, value: "°C", },
                                        store: store,
                                        parameterName: "WeatherStation/emhi.ee/air.temperature",
                                    }),

                                    new CircularLinerGaugeProvider({
                                        gaugeSettings: {
                                            value: 0,
                                            minimum: -50,
                                            maximum: 50,
                                            majorTickInterval: 10,
                                            minorTickInterval: 1,
                                            interactionArea: "none",
                                            animationDuration: 1000,
                                            style: "width: 180px; height: 200px;",
                                            title: "Ilm.ee",
                                        },
                                        textIndicator: { x: 100, y: 170, value: "°C", },
                                        store: store,
                                        parameterName: "WeatherStation/ilm.ee/air.temperature",
                                    }),
                                ],
                            }),
                            col: 1,
                            row: 2,

                        },

                    ];

                    var view = new DashboardView({ parentDivId: "CenterContainer", store: store, config: config, columns: 3, });

                    new UrlDataLoader().loadData("../data/db.json", pageStore);

                }

            })

        ];
    }
);