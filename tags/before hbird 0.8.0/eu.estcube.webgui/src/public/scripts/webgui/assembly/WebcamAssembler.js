define([
    "dojo/_base/declare",
    "dojo/store/Memory",
    "dojo/store/Observable",
    "../common/Constants",
    "../common/DataLoader",
    "./AssemblerBase",
    "../display/WebcamView",
    "../display/MenuView",
    ],

    function(declare, Memory, Observable, Constants, DataLoader, base, view, MenuView) {
        return [

            declare("WebcamAssembler", AssemblerBase, {

                build: function() {
                    this.setupCommunication();

                    var pageStore = new Observable(new Memory( { idProperty: "id" }));
                    var menubar = new MenuBarView({
                        parentDivId: "menu",
                        store: pageStore,
                        primaryItems: ["ES5EC", "system", "satellites", "groundStations"],
                        secondaryItems: ["logout"],
                    });

                    var store = new Observable(new Memory({ idProperty: "storeId" }));
                    this.view = new WebcamView({ parentDivId: "webcam", initialImage: "/images/image.jpg", store: store, imageId: "GroundStation/ES5EC/webcam", });
                    this.controller = new WebcamController({ id: "webcam", channels: ["/hbird.out.binary"], store: store, imageId: "GroundStation/ES5EC/webcam" });

                    new UrlDataLoader().loadData("../../data/db.json", pageStore);

                }
            })

        ];
});