define([
    "dojo/_base/declare",
    "dojo/store/Memory",
    "dojo/store/Observable",
    "./AssemblerBase",
    "../display/TleView"
    ],

    function(declare, Memory, Observable, base, view) {
        return [

            declare("TleAssembler", AssemblerBase, {

                build: function() {
                    this.setupCacheRequest({ delay: 1500, channels: ["/hbird.out.all"] });
                    this.setupCommunication();
                    var store = new Observable(new Memory({ idProperty: "timestamp" }));
                    var controller = new TleController({ id: "tle", channels: ["/hbird.out.all"], store: store });
                    var view = new TleView({ parentDivId: "CenterContainer", store: store });
                },

            })

        ];
    });
