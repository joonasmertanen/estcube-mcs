define([
    "dojo/_base/declare",
    "dojo/store/Memory",
    "dojo/store/Observable",
    "../common/Constants",
    "../assembly/AssemblerBase",
    "../display/SystemLogView",
    ],

    function(declare, Memory, Observable, Constants, base, view) {
        return [

            declare("SystemLogAssembler", AssemblerBase, {

                build: function() {
//                    this.setupCacheRequest({ delay: 1500, channels: ["/hbird.out.parameters"], });
                    this.setupCommunication();

                    var store = new Observable(new Memory({ idProperty : "storeId", }));
                    var view = new SystemLogView({ store: store, parentDivId: "CenterContainer" });
                    var controller = SystemLogController({ id: "SysLog", channels: ["/hbird.out.systemlog"], store: store});
                },

            }),

        ];
    }
);