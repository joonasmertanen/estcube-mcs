define([], function() {

    return {

        routes: {},
        SERVICE_ID:                         "eu.estcube.webgui",
        MENU_STRUCTURE_CONFIG_MODULE:       "config/menu",
        DEFAULT_MODULE:                     "DASHBOARD",
        LOGOUT_PATH:                        "/logout",

        DEFAULT_DATE_FORMAT:                "yyyy-MM-dd (DDD) HH:mm:ss.SSS",
        EXPIRATION_DATE:                    "365",

        WEB_SOCKET_PORT:                    "1337",

        TOPIC_CHANNEL_REQUEST:              "/channel/request",
        TOPIC_CHANNEL_EVENT:                "/channel/event",
        TOPIC_CHANNEL_SEND_MESSAGE:         "/channel/message",

        TOPIC_SELECTION_PARAMETER:          "/selection/parameter",
        TOPIC_SELECTION_COMMAND:            "/selection/command",

        TOPIC_PARAMETER_SHOW:               "/parameter/show",
        TOPIC_PARAMETER_HIDE:               "/parameter/hide",

        TOPIC_STORE_EVENT:                  "/store/event",

        TOPIC_SYSTEM_MESSAGES:              "/system/message",

        URL_CATALOGUE_SATELLITES:           "/catalogue/satellites",
        URL_CATALOGUE_ORBITAL_STATES:       "/catalogue/orbitalstates/",

        DND_TYPE_PARAMETER:                 "/dnd/type/parameter",
        DND_TYPE_COMMAND:                   "/dnd/type/command",

        WEBSOCKET_ALL:                      "/hbird.out.all",
        WEBSOCKET_BINARY:                   "/hbird.out.binary",
        WEBSOCKET_BUSINESS_CARDS:           "/hbird.out.businesscards",
        WEBSOCKET_PARAMETERS:               "/hbird.out.parameters",
        WEBSOCKET_SYSTEM_LOG:               "/hbird.out.systemlog",
        WEBSOCKET_ORBITAL_PREDICTIONS:      "/orbital.predictions",
        WEBSOCKET_SYSTEM_MESSAGES:          "/hbird.out.events",
        WEBSOCKET_TRANSPORT:                "/estcube.out.transport",

        STORE_LIMIT_SYSTEM_LOG:             100,
        STORE_LIMIT_TRANSPORT_FRAMES:       100,
        STORE_LIMIT_SYSTEM_MESSAGES:        100,

        TRANSPORT_FRAME_FILTER:             ["TncFrame", "Ax25UIFrame"], // Frame classes accepted by TransportFrameStore

    };
});
