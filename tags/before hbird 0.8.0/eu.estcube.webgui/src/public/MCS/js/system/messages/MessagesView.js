define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dgrid/OnDemandGrid",
    "config/config",
    "common/formatter/DateFormatter",
    "common/store/SystemMessageStore",
    ],

    function(declare, Lang, DomConstruct, DomClass, Grid, config, DateFormatter, store) {

        return declare([], {

            constructor: function(args) {
                this.grid = new Grid({
                    store: store,
                    query: {},
                    columns: {
                        timestamp: { label: "Timestamp", formatter: DateFormatter, className: "field-timestamp", },
                        issuedBy: { label: "Component Name", className: "field-issuedBy", },
                        level: { label: "Level", className: "field-level", renderCell: function(object, value, node, options) {
                                node.innerHTML = value;
                                if (value == "TRACE" || value == "DEBUG" || value == "INFO") {
                                    DomClass.add(node, "value-ok");
                                }
                                else if (value == "WARN") {
                                    DomClass.add(node, "value-warning");
                                }
                                else {
                                    DomClass.add(node, "value-error");
                                }
                        }},
                        value: { label: "Message", className: "field-value", },
                    },
                });
                this.grid.set("sort", "timestamp", true);
                DomClass.add(this.grid.domNode, "fill");
                this.grid.startup();

                // XXX - workaround for grid titles not visible bug
                setTimeout(Lang.hitch(this, function() { this.grid.set("showHeader", true) }), 500);
            },

            placeAt: function(container) {
                DomConstruct.place(this.grid.domNode, container);
            },

        });
    }
);
