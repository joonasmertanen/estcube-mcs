define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/dom-construct",
    "dijit/layout/BorderContainer",
    "dijit/layout/ContentPane",
    "dijit/TitlePane",
    "dojox/layout/ExpandoPane",
    "dijit/form/CheckBox",
    "config/config",
    "common/store/OrbitalPredictionsStore",
    ],

    function(declare, Lang, Arrays, DomConstruct, BorderContainer, ContentPane, TitlePane, ExpandoPane, CheckBox, config, store) {

        return declare([], {
            constructor: function(args) {
            	var visible = false;
            	
            	this.setVisible = function(vis) {
            	    visible = vis;
            	}
            	
            	this.isVisible = function() {
            	    return visible;
            	}
            	
            	var map = new OpenLayers.Map({
                    controls: [
                        new OpenLayers.Control.Navigation({ zoomWheelEnabled: true }),
                        new OpenLayers.Control.PanZoomBar(),
                        new OpenLayers.Control.ScaleLine(),
                        new OpenLayers.Control.MousePosition(),
                        new OpenLayers.Control.KeyboardDefaults()
                    ],
                    numZoomLevels: 10
                });
            	
            	var groundStations = config.MAP.groundStations;
            	var renderer = OpenLayers.Layer.Vector.prototype.renderers;
            	
            	var mainLayer = new OpenLayers.Layer.WMS(
            			"Global Imagery",
            			"http://maps.opengeo.org/geowebcache/service/wms",
            			{ layers: "bluemarble" },
            			{ tileOrigin: new OpenLayers.LonLat(-180, -90) }
            	);            	
            	
            	var groundStationLocationLayer = new OpenLayers.Layer.Vector("Ground Station Location Geometry Layer", {
            	    styleMap: new OpenLayers.StyleMap(config.MAP.styles.gsLayer),
            	    rendereres: renderer
            	});
            	
            	var groundStationCoverageAreaLayer = new OpenLayers.Layer.Vector("Ground Station Coverage Area Geometry Layer", {
                    styleMap: new OpenLayers.StyleMap(config.MAP.styles.gsCoverageAreaLayer),
                    renderers: renderer
                });
            	
            	var satelliteLocationLayer = new OpenLayers.Layer.Vector("Satellite Location Prediction Layer", {
            		styleMap: new OpenLayers.StyleMap(config.MAP.styles.satLocationLayer),
                	renderers : renderer
                });
            	
            	var orbitalTraceLayer = new OpenLayers.Layer.Vector("Orbital Trace Layer", {
            	   styleMap: new OpenLayers.StyleMap(config.MAP.styles.orbitalTraceLayer),
            	   renderers: renderer
            	});
            	
            	// XXX: This is for debugging - marking the two points that the point is currently
            	// interpolating between
            	var debugLayer = new OpenLayers.Layer.Vector("Debug Layer", {
            	    styleMap: new OpenLayers.StyleMap({
            	        pointRadius: 5,
            	        fillOpacity: 0.5
            	    }),
            	    renderers: renderer
            	});
            	
            	var control = new OpenLayers.Control.SelectFeature(satelliteLocationLayer, {
            	    multiple: false,
            	    toggle: true
            	});
            	
            	map.addControl(control);
            	
            	var satPopup = null;
            	satelliteLocationLayer.events.on({
            	    "featureselected": function(element) {
            	        // TODO: If another feature is selected, does "unselected" event fire first?
            	        satPopup = new OpenLayers.Popup("Satellite popup", 
            	                new OpenLayers.LonLat(element.feature.geometry.x, element.feature.geometry.y),
            	                new OpenLayers.Size(200, 200), element.feature.attributes.label, true,
            	                function() { control.unselect(element.feature) });
            	        map.addPopup(satPopup);
            	    },
            	    
            	    "featureunselected": function(element) {
            	        satPopup.destroy();
            	    }
            	});
            	
            	control.activate();
            	
            	groundStationLocationLayer.addFeatures(Arrays.map(groundStations, function(gs) {
            		var point = new OpenLayers.Geometry.Point(gs.initialLocation.lon, gs.initialLocation.lat);
	                var feature = new OpenLayers.Feature.Vector(point);
	                
	                feature.attributes = { label : gs.abbreviation };
	                
	                return feature;
            	}, this));
            	
            	gsCoverageAreaFeatures = {}
            	
            	groundStationCoverageAreaLayer.addFeatures(Arrays.map(groundStations, function(gs) {
            		gsCoverageAreaFeatures[gs.id] = new OpenLayers.Feature.Vector(OpenLayers.Geometry.fromWKT(gs.coverageArea));
            		return gsCoverageAreaFeatures[gs.id];
            	}, this));            	
            	
            	map.addLayer(mainLayer);
            	map.addLayer(groundStationLocationLayer);
            	map.addLayer(groundStationCoverageAreaLayer);
            	map.addLayer(orbitalTraceLayer);
            	map.addLayer(satelliteLocationLayer);
            	map.addLayer(debugLayer);
            	
            	
            	var center = null;
            	if(groundStations.length > 0) {
            		var gs = groundStations[0];
            		center = new OpenLayers.LonLat(gs.initialLocation.lon, gs.initialLocation.lat);
            	}
            	map.setCenter(center, 5);
            	
            	this.map = map;
            	
            	// Inserts <x> to <arr> at position <n> inplace, shifting tail
            	function InsertIntoArray(arr, x, n) {
            	    for(var i = arr.length; i > n; i--) {
            	        arr[i] = arr[i-1];
            	    }
            	    
            	    arr[n] = x;
            	}
            	
            	// id => {showing, pos = {lat, long, timestamp}, nextPos = {lat, long, timestamp}, feature}, 
            	var satellitesTracked = {};
                
                Arrays.forEach(config.MAP.satellites, function(sat) {
                    satellitesTracked[sat.id] = {showing: true, pos: null, nextPos: null, feature: null};
                    SetupUpdater(sat.id);
                }, this);
                
                // Node: If state at a different time will be needed, change Date.now() calls to some function, that
                // returns the timestamp with some delta
                function SetupUpdater(satelliteId) {
                    var futurePositions = [];
                    
                    function MessageToPosition(msg) {
                        return {
                            lon: msg.lon,
                            lat: msg.lat,
                            timestamp: msg.timestamp
                       };
                    }
                    
                    // XXX TODO: Major. When creating the view, fetch all points already in the store
                    store.query({satellite_id : satelliteId}).observe(Lang.hitch(this, function(msg) {
                        console.log("[map] Received message");
                        
                        var trackingData = satellitesTracked[satelliteId];
                        
                        var now = Date.now();
                        
                        if(trackingData.pos != null) {
                            if(msg.timestamp > trackingData.pos.timestamp) {
                                console.log("[map] Point is later than the current position");
                                
                                if(msg.timestamp > now) {
                                    console.log("[map] Point is in future");
                                    // Future point
                                    if(trackingData.nextPos != null && msg.timestamp > trackingData.nextPos.timestamp) {
                                        console.log("[map] Point is later than current nearest future point");
                                        // Place <msg> in PQ <futurePositions>
                                        var index;
                                        for(index = 0; index < futurePositions.length && futurePositions[index].timestamp < msg.timestamp; index++);
                                        
                                        InsertIntoArray(futurePositions, MessageToPosition(msg), index);
                                    } else {
                                        console.log("[map] Point is the nearest future point");
                                        // Point between LKP and NP
                                        
                                        // Put NP into PQ
                                        if(trackingData.nextPos != null) {
                                            futurePositions.unshift(trackingData.nextPos);
                                        }
                                        
                                        // Set NP to <msg>
                                        trackingData.nextPos = MessageToPosition(msg);
                                        
                                        // Recalculate interpolation
                                        UpdateInterpolation();
                                    }
                                } else {
                                    console.log("[map] Point is in the past");
                                    // Update last known position and interpolation
                                    UpdateLastKnownPosition(msg);
                                }
                            }
                        } else { // TODO: noticed the bug too late, this is the easiest way to fix, but there should be a way to refactor it later
                            if(msg.timestamp > now) {
                                console.log("[map] Point in the future for satellite with unknown current position")
                                var index;
                                for(index = 0; index < futurePositions.length && futurePositions[index].timestamp < msg.timestamp; index++);
                                
                                InsertIntoArray(futurePositions, MessageToPosition(msg), index);
                            } else {
                                console.log("[map] Received first present/past message for " + satelliteId);

                                trackingData.pos = {lon: msg.lon, lat: msg.lat, timestamp: msg.timestamp};
                                trackingData.nextPos = null;

                                var point = new OpenLayers.Geometry.Point(msg.lon, msg.lat)
                                trackingData.feature = new OpenLayers.Feature.Vector(point);
                                trackingData.feature.attributes = { label: satelliteId };

                                trackingData.debugFeature1 = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(msg.lon, msg.lat));
                                trackingData.debugFeature2 = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(msg.lon, msg.lat));

                                satelliteLocationLayer.addFeatures([trackingData.feature]);
                                debugLayer.addFeatures([trackingData.debugFeature1, trackingData.debugFeature2]);
                                
                                if(futurePositions.length > 0) {
                                    UpdateNearestPoint();
                                }
                            }
                        }
                    }));
                    
                    function UpdateInterpolation() {
                        var data = satellitesTracked[satelliteId];
                        
                        if(data.pos != null && data.nextPos != null) {
                            var lerpCoeff = (Date.now() - data.pos.timestamp) / (data.nextPos.timestamp - data.pos.timestamp);
                            
                            if(lerpCoeff > 1) {
                                console.error("[map] UpdateInterpolation: lerp coefficient > 1");
                            }
                            
                            data.feature.geometry.x = data.pos.lon + lerpCoeff * (data.nextPos.lon - data.pos.lon);
                            data.feature.geometry.y = data.pos.lat + lerpCoeff * (data.nextPos.lat - data.pos.lat);
                            
                            data.debugFeature1.geometry.x = data.pos.lon;
                            data.debugFeature1.geometry.y = data.pos.lat;
                            data.debugFeature2.geometry.x = data.nextPos.lon;
                            data.debugFeature2.geometry.y = data.nextPos.lat;
                            
                            if(visible && data.showing) {
                                debugLayer.redraw();
                            } 
                            
                            // TODO: This should not take those coordinates
                            UpdateFeature(data.feature.geometry.x, data.feature.geometry.y);
                        }
                    }
                    
                    // Set LKP
                    // If NP is not null
                    //   If NP.timestamp < LKP.timestamp
                    //      Update NP
                    //   Else:
                    //      Recalculate interpolation (case: LKP changed)
                    function UpdateLastKnownPosition(lkp) {
                        console.info("UpdateLastKnownPosition()");
                        var data = satellitesTracked[satelliteId];
                        
                        var oldPos = data.pos;
                        
                        console.log("Old position: ", oldPos);
                        console.log("New position: ", lkp);
                        
                        data.pos = {
                            lat: lkp.lat,
                            lon: lkp.lon,
                            timestamp: lkp.timestamp
                        };
                        
                        data.feature.geometry.x = lkp.lon;
                        data.feature.geometry.y = lkp.lat;
                        
                        if(oldPos != null && data.nextPos != null && data.nextPos ) {
                            if(data.nextPos.timestamp <= lkp.timestamp) {
                                UpdateNearestPoint();
                            }
                        }
                        
                        UpdateInterpolation();
                        
                        // TODO: DRY
                        if(satellitesTracked[satelliteId].showing && visible) {
                            satelliteLocationLayer.redraw();
                        }
                    }
                    
                    // NB: Assumes that NP needs to be switched (checks are on caller site)
                    function UpdateNearestPoint() {
                        console.log("[map] UpdateNearestPoint()");
                        var data = satellitesTracked[satelliteId]; // TODO: Just move it one level up, everybody uses it
                        
                        if(futurePositions.length > 0) {
                            console.log("[map] UpdateNearestPoint(): fetched next NP")
                            var nearest = futurePositions.shift();
                            
                            // XXX: Debug
                            if(nearest.timestamp <= Date.now()) {
                                console.error("[map] Nearest point is in in the past/present");
                            }
                            
                            data.nextPos = nearest;
                        } else {
                            console.log("[map] UpdateNearestPoint(): futureValues is empty")
                            data.nextPos = null;
                        }
                    }
                    
                    function UpdateFeature(x, y) {
                        var data = satellitesTracked[satelliteId];
                        
                        data.feature.geometry.x = x;
                        data.feature.geometry.y = y;
                        
                        if(satellitesTracked[satelliteId].showing && visible) {
                            satelliteLocationLayer.redraw();
                        }
                    }
                    
                    
                    
                    var updater = setInterval(function() {
                        var data = satellitesTracked[satelliteId];
                        
                        if(data.nextPos !== null) {
                            var now = Date.now();
                            
                            if(now < data.nextPos.timestamp) {
                                UpdateInterpolation();
                            } else {
                                console.info('Switching to the next position');
                                // Update LKP, NP
                                UpdateLastKnownPosition(data.nextPos);
                            }
                        }
                    }, config.MAP.updateInterval);
                }
                
                
                
                // sat_id => trace feature
                var traceFeatures = {};
                
                // TODO: add controls for limiting the trace
                Arrays.forEach(config.MAP.satellites, function(sat) {
                    var points = store.query({ satellite_id: sat.id }, {
                        sort: [{ attribute: 'timestamp', descending: false}]
                    });
                    
                    var lineString = new OpenLayers.Geometry.LineString(Arrays.map(points, function(p) {
                        return new OpenLayers.Geometry.Point(p.lon, p.lat);
                    }));
                    
                    traceFeatures[sat.id] = new OpenLayers.Feature.Vector(lineString);
                    
                    orbitalTraceLayer.addFeatures([traceFeatures[sat.id]]);
                    
                    store.query({ satellite_id : sat.id}, {
                        sort: [{ attribute: 'timestamp', descending: false}]
                    }).observe(Lang.hitch(this, function(p, removedFrom, insertedTo) {
                        var point = new OpenLayers.Geometry.Point(p.lon, p.lat)
                        traceFeatures[sat.id].geometry.addPoint(point, insertedTo);
                        traceFeatures[sat.id].geometry.clearBounds();
                        
                        if(visible) {
                            orbitalTraceLayer.redraw();
                        }
                    }));
                }, this);

                this.createUI(groundStationCoverageAreaLayer, gsCoverageAreaFeatures, satelliteLocationLayer, 
                        satellitesTracked);
            },
            
            createUI: function(gsCoverageLayer, gsCoverageFeatures, satLocationLayer, satellitesTracked) {
				this.mainContainer = DomConstruct.create("div", {style: {width: "100%", height: "100%"}});
            	
            	this.layout = new BorderContainer({}, this.mainContainer);
            	this.mapPane = new ContentPane({region : "center"});
            	this.layout.addChild(this.mapPane);
            	
            	var listPane = new ExpandoPane({id : "lists", region : "right", style : {width : "200px", height : "100%"}});
            	var gsListPane = new TitlePane({id : "gslist", title: "Ground stations", style : {"margin-bottom" : "10px"}});
            	var satListPane = new TitlePane({id : "satlist", title: "Satellites", style : {}});
            	listPane.addChild(gsListPane);
            	listPane.addChild(satListPane);
            	
            	this.layout.addChild(listPane);
            	
            	function CreateCheckboxes(source, parent, callback) {
            		Arrays.forEach(source, function(s) {
            		    var cbPane = new ContentPane();
            		    
                		var checkBox = new CheckBox({id : (s.id + "-cb"), checked : true})
                		checkBox.onChange = function(value) {
                			callback(value, s.id);
                		}
                		
                		cbPane.addChild(checkBox);
                		cbPane.domNode.appendChild(DomConstruct.create("label", {"for" : (s.id + "-cb"), innerHTML : s.abbreviation}));

                		parent.addChild(cbPane);
            		}, this);
            	}
            	
            	CreateCheckboxes(config.MAP.groundStations, gsListPane, Lang.hitch(this, function(value, id) {
            		if(value) {
            			gsCoverageLayer.addFeatures([gsCoverageFeatures[id]]);
            		} else {
            			gsCoverageLayer.removeFeatures([gsCoverageFeatures[id]]);
            		}
            	}));
            	
            	CreateCheckboxes(config.MAP.satellites, satListPane, Lang.hitch(this, function(value, id) {
            		satellitesTracked[id].showing = value;
            		
            		if(value) {
        				satLocationLayer.addFeatures([satellitesTracked[id].feature]);
        			} else {
        				satLocationLayer.removeFeatures([satellitesTracked[id].feature]);
        			}
            	}));
            },

            placeAt: function(container) {            	
            	DomConstruct.place(this.mainContainer, container);
            	this.layout.startup();
            	
            	this.setVisible(true);

            	this.map.render(this.mapPane.domNode);
            },
            
            destroy: function() {
            	this.setVisible(false);
            }

        });
    }
);
