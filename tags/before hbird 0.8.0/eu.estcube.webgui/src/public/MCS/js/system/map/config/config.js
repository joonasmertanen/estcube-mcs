define([
    "dojo/dom",
    "dojo/domReady!"
    ],

    function(dom) {
        return {
            routes: {
                MAP: {
                    path: "system/map",
                    defaults: {
                        controller: "Map/MapController",
                        method: "index",
                        destroyMethod : "clear"
                    }
                },
            },

            MAP: {
            	satellites: [
            	             {
            	            	 id : "ESTCube-1",
            	            	 abbreviation : "ESTCube-1"
            	             }
            	],
            	
            	groundStations : [
            	                  {
            	                	  "abbreviation" : "ES5EC",
            	                	  "initialLocation" : { 
            	                		  "lon" : "26.7147224", 
            	                		  "lat" : "58.3708465" 
            	                	  },
            	                	  "coverageArea" : "POLYGON((13.798828125 59.326171875, 13.359375 58.271484375, 13.18359375 57.48046875, 13.095703125 56.513671875, 13.095703125 55.72265625, 13.53515625 55.01953125, 13.974609375 54.228515625, 14.765625 53.525390625, 15.46875 52.91015625, 16.787109375 52.3828125, 18.193359375 51.943359375, 20.126953125 51.6796875, 21.09375 51.591796875, 22.412109375 51.50390625, 24.521484375 51.50390625, 26.3671875 51.50390625, 28.388671875 51.591796875, 31.025390625 52.03125, 32.431640625 52.470703125, 33.92578125 52.998046875, 35.68359375 53.96484375, 36.9140625 54.931640625, 37.705078125 55.72265625, 38.232421875 56.953125, 38.408203125 57.919921875, 38.583984375 59.4140625, 38.583984375 60.29296875, 38.583984375 61.69921875, 38.583984375 62.9296875, 38.14453125 63.6328125, 37.353515625 64.51171875, 36.5625 65.478515625, 35.419921875 66.181640625, 34.365234375 66.62109375, 33.486328125 67.060546875, 32.431640625 67.32421875, 30.146484375 67.939453125, 29.091796875 67.939453125, 27.509765625 67.939453125, 26.015625 67.939453125, 24.2578125 67.939453125, 23.291015625 68.02734375, 22.1484375 67.763671875, 20.7421875 67.32421875, 19.86328125 66.884765625, 18.984375 66.26953125, 17.9296875 65.478515625, 16.962890625 64.6875, 16.171875 63.80859375, 13.798828125 59.326171875))",
            	                	  "id" : "esc5ec"
            	                  }

            	],
            	
            	updateInterval: 100,
            	
            	styles: {
            	    gsLayer: {
                        'default': {
                            strokeOpacity: 1,
                            strokeWidth: 2,
                            fillColor: "#FFFF00",
                            fillOpacity: 0.5,
                            pointRadius: 4,
                            strokeColor: "#000000",
                            cursor: "pointer",
                            label : "${label}",
                            fontSize: "12px",
                            fontFamily: "Courier New, monospace",
                            fontColor : "#FFFF00",
                            labelYOffset: -16
                        },
                        'select': {
                            strokeOpacity: 1,
                            strokeWidth: 2,
                            fillColor: "#FFA500",
                            fillOpacity: 0.5,
                            pointRadius: 4,
                            strokeColor: "#000000",
                            cursor: "pointer"
                        }
                    },
                    
                    gsCoverageAreaLayer: {
                        'default': {
                            strokeOpacity: 1,
                            strokeWidth: 2,
                            fillColor: "#FFFF00",
                            fillOpacity: 0.15,
                            strokeColor: "#FF7F00"
                        }
                    },
                    
                    satLocationLayer: {
                        'default': {
                            strokeOpacity: 1,
                            strokeWidth: 2,
                            fillColor: "#FF0000",
                            fillOpacity: 0.5,
                            pointRadius: 10,
                            strokeColor: "#000000",
                            cursor: "pointer",
                            label : "${label}",
                            fontSize: "16px",
                            fontFamily: "Courier New, monospace",
                            fontColor : "#FF0000",
                            labelYOffset: -20
                        },
                        'select': {
                            strokeOpacity: 1,
                            strokeWidth: 2,
                            fillColor: "#FFFF00",
                            fillOpacity: 0.5,
                            pointRadius: 10,
                            strokeColor: "#000000",
                            cursor: "pointer",
                            label : "${label}",
                            fontSize: "16px",
                            fontFamily: "Courier New, monospace",
                            fontColor : "#FF0000",
                            labelYOffset: -20
                        }
                    },
                    
                    orbitalTraceLayer: {
                        'default': {
                            strokeColor: "#FFFFFF",
                            strokeWidth: 2,
                            fillColor: "#FFFFFF",
                            pointRadius: 3
                        }
                    }
            	}
            }

        };
    }
);
