define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/dom-construct",
    "dijit/form/Form",
    "dojox/layout/TableContainer",
    "dijit/form/SimpleTextarea",
    "dijit/layout/ContentPane",
    "dijit/form/Button",
    "dijit/form/NumberSpinner",
    "dijit/form/ValidationTextBox",
    "dojo/on",
    "dojo/request",
    "dijit/Dialog",
    "dijit/ProgressBar",
    "config/config",
    ],
    
    function(declare, Lang, Array, DomConstruct, Form, TableContainer, SimpleTextarea, ContentPane, Button, 
    		NumberSpinner, ValidationTextBox, on, request, Dialog, ProgressBar, config) {
		function Bytestring(min, max) {
			return "([0-9a-fA-F]{2}(\\s)*){" + min + "," + max + "}";
		}
		
		var ax25conf = config.ESTCube1_AX25;
		
		return declare([], {
			constructor: function(args) {
				this.mainContainer = DomConstruct.create("div", { "class": "ax25-container", "style": { margin: "5px"} });
                var div = DomConstruct.create("div", {}, this.mainContainer);
                var form = new Form({ encType: "multipart/form-data", action: "", method: "" }, div);

                var table = new TableContainer({
                    cols: 1,
                    labelWidth: 150,
                    spacing : 5
                });
                
                var validatedFieldsData = [
                    {name: "destAddr", label: "Destination address"},
                    {name: "sourceAddr", label: "Source address"},
                    {name: "ctrl", label: "CTRL"},
                    {name: "pid", label: "PID"}
                ];
                
                Array.forEach(validatedFieldsData, function(data) {
                   var length = ax25conf.constraints[data.name].length;
                   var value = ax25conf.defaults[data.name];
                   
                   var field = new ValidationTextBox({ name: data.name, label: data.label,
                       pattern: Bytestring(length, length), value: value, required: true
                   });  
                   
                   table.addChild(field);
                });
                
                var portField = new NumberSpinner({ name: "port", label: "TNC port", 
                	constraints : {min: ax25conf.constraints.port.min, max: ax25conf.constraints.port.max }, 
                	value: ax25conf.defaults.port, required: true
                });
                var infoField = new SimpleTextarea({ name: "info", label: "Info", cols: 60, rows: 4});
                var infoPattern = new RegExp("^" + Bytestring(ax25conf.constraints.info.minLength, ax25conf.constraints.info.maxLength) + "$");
                
                var buttonWrapper = new ContentPane({ style: "margin: 0px; padding: 0px;" });
                var buttonSubmit = new Button({ label: "Submit", title: "Submit frame" });
                buttonWrapper.addChild(buttonSubmit);
                
                var sendingDialog = new Dialog({ 
                	title: "Sending frame", 
                	content: "", 
                	style: "width: 200px"
                });
                
                var progressBar = new ProgressBar({ value: Infinity, style: "width: 100%"});
                
                on(buttonSubmit, "click", function(evt) {
                	sendingDialog.show();

                	if(!form.validate()) {
                		sendingDialog.set("content", "Invalid field values");
                	} else if(!infoPattern.test(infoField.value)) {
                	    var infoMin = ax25conf.constraints.info.minLength;
                	    var infoMax = ax25conf.constraints.info.maxLength;
                	    
                		sendingDialog.set("content", "Invalid info size: must be " + infoMin + "-" + infoMax + "bytes");
                	} else {
	                	sendingDialog.set("content", progressBar);
	                	
	                	request.post(ax25conf.SUBMIT_URL, {
	                	    data: form.getValues(),
	                		handleAs: 'json'
	                	}).then(function(response) {
	                		if(response.status == "OK") {
	                			sendingDialog.set("content", "Completed successfully");
	                		} else {
	                			sendingDialog.set("content", "Error: " + response.value);
	                		}
	                	}, function(error) {
	                		sendingDialog.set("content", "Error sending data to the server: " + error);
	                	});
	                }
                });
                
                table.addChild(portField);
                table.addChild(infoField);
                
                table.addChild(buttonWrapper);

                form.domNode.appendChild(table.domNode);
				
                table.startup();
			},
		
			placeAt: function(container) {
				DomConstruct.place(this.mainContainer, container);
			}
		})
	}
);