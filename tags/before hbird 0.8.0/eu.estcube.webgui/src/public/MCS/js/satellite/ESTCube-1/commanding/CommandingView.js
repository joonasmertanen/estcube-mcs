define([
    "dojo/_base/declare",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/on",
    "dojo/_base/lang",
    "dojo/request",
    "dijit/form/Form",
    "dijit/form/Button",
    "dijit/form/ValidationTextBox",
    "dijit/form/SimpleTextarea",
    "dijit/layout/ContentPane",
    "dojox/layout/TableContainer",
    "dijit/form/Select",
    "dgrid/OnDemandGrid",
    "dojo/data/ItemFileReadStore", // REMOVE!
    "dojo/json",
    "config/config",
    "common/formatter/DateFormatterS",
    "dojo/store/Memory", 
    "dijit/form/FilteringSelect"
   ],

    function(declare, DomClass, DomConstruct, On, Lang, Request, Form, Button, ValidationTextBox, SimpleTextarea, ContentPane, TableContainer, Select, Grid, ItemFileReadStore, Json, Config, DateFormatter, Memory, FilteringSelect ) {

        return declare([], {

            constructor: function(args) {
				
				var self = this;
				
				// ------------------------------------------
				// create form
                this.main = DomConstruct.create("div", { "class": "commanding-container", "style": "width:1000px; margin:10px;" });
  
  
                this.errorMessageBox = DomConstruct.create("div", { "class": "commanding-container", "style": "font-size:13px; font-weight:bold; color:red; border: 1px solid lightgray; margin: 3px; padding: 4px;", innerHTML: "error" }, this.main);
                this.okMessageBox = DomConstruct.create("div", { "class": "commanding-container", "style": "font-size:13px; font-weight:bold; color:green; border: 1px solid lightgray; margin: 3px; padding: 4px;", innerHTML: "ok" }, this.main);
  
                this.formWrapperDiv = DomConstruct.create("div", { "style":"width:700px;"},  this.main);
                this.div = DomConstruct.create("div", {},  this.formWrapperDiv);
                this.form = new Form({ encType: "multipart/form-data", action: "", method: "" }, this.div);
                this.table = new TableContainer({cols: 2, labelWidth: 150 });    
                
                
                this.source = new Select({ 
                	name: "source", 
                	value:"", 
                	placeHolder: "",
                	label: "Source:", 
                	required:true,
                	style: "width:182px",
                	options: [
                		{label: "GS", value: Config.COMMANDING.DEFAULT_GS},
                	],
                	value: "",
                	onChange: function() {
                		self.removeMessages();
                	}
                });
                this.table.addChild(this.source);
                
                this.priority = new Select({ 
                	name: "priority", 
                	value:"", 
                	placeHolder: "",
                	label: "Priority:", 
                	required:true,
                	style: "width:182px",
                	options: [
                		{label: "0", value: 0},
                		{label: "1", value: 1},
                		{label: "2", value: 2},
                		{label: "3", value: 3},
                	],
                	onChange: function() {
                		self.removeMessages();
                	},
                }); 
                this.table.addChild(this.priority);
                
                this.destination = new Select({ 
                	name: "destination", 
                	value:"", 
                	placeHolder: "",
                	label: "Destination:", 
                	required:true,
                	style: "width:182px",
                	options: [
                		{label: "-", value: ""},
                		{label: "EPS", value: "0"},
                		{label: "COM", value: 1},
                		{label: "CDHS", value: 2},
                		{label: "ADCS", value: 3},
                		{label: "PL", value: 4},
                		{label: "CAM", value: 5},
                		{label: "GS", value: 6},
                		{label: "PC", value: 7},
                		{label: "PC2", value:8},
                	],
                	onChange: function( e ) {
                		self.id.query = {"subsystem": self.destination.get("value") };
                		self.id.set("value", "");
                		
                		self.loadCommandArguments();
                		
                		self.removeMessages();
                	}
                });  
                this.table.addChild(this.destination);
                
                this.CDHSSource = new ValidationTextBox({ 
                	name: "CDHSSource", 
                	value:"", 
                	placeHolder: "",
                	label: "CDHS source:", 
                	disabled: true,
                	required:true,
                	value: Config.COMMANDING.DEFAULT_CDHS_SOURCE,
                	
                	onChange: function() {
                		self.removeMessages();
                	}
                }); 
                this.table.addChild(this.CDHSSource);        
                
                var commandsStore = new ItemFileReadStore({
                	url: Config.COMMANDING.GET_COMMANDS_URL,
                	identifier: "id",
			    });
			    
                this.id = new FilteringSelect({ 
                	name: "id", 
                	placeHolder: "ID or name",
                	label: "Command:", 
                	required:true,
                	store: commandsStore,
                	searchAttr: "name",
                	query: {subsystem: "-" },
                	onChange: function() {
                		self.loadCommandArguments();
                		self.removeMessages();
                	}
                }); 
                this.table.addChild(this.id);
                 
                this.CDHSBlockIndex = new ValidationTextBox({ 
                	name: "CDHSBlockIndex", 
                	value:"", 
                	placeHolder: "",
                	label: "CDHS block index:", 
                	required:true,
                	disabled: true,
                	value: Config.COMMANDING.DEFAULT_CDHS_BLOCK_INDEX,
                	onChange: function() {
                		self.removeMessages();
                	}
                });  
                this.table.addChild(this.CDHSBlockIndex);

                this.arguments = new ValidationTextBox({ 
                	name: "arguments", 
                	value:"", 
                	placeHolder: "Separated by spaces",
                	label: "Arguments:", 
                	required:false,
                	style: "width:528px",
                	value: "",
                	colspan: 2,
                	onChange: function() {
                		self.removeMessages();
                	}
                });
                this.table.addChild(this.arguments);
                
                
                this.checkButton = new Button({ label: "Send command", title: "" });
                On(this.checkButton, "click", function() {
                    self.submitCommand();
                });
                
                this.argumentsInfo = new ContentPane({
                	colspan: 2,
                	innerHTML: "",
                	style: "font-size:11px; color:gray; padding:0; margin:2px;"
                });
                this.table.addChild(this.argumentsInfo );
                
                
                this.buttonWrapper = new ContentPane({ style: "margin: 0px; padding: 0px;" });
                this.buttonWrapper.addChild(this.checkButton);
				this.table.addChild(this.buttonWrapper);
				 
                this.form.domNode.appendChild(this.table.domNode);
                this.table.startup(); 
                
                this.removeMessages();
             },
            
            loadCommandArguments: function() {
            	var self = this;
            	var data = this.id.get('value').split("_");
            	
            	Request.get(Config.COMMANDING.GET_COMMAND_ARGUMENTS_URL, {
                    data: {
                        ommand: data[0],
                        subsys: data[1],
                    },
                    handleAs: "json",
                }).then(
                    function( response ) {
                        if( response.status == "ok" ) {
                        	self.argumentsInfo.domNode.innerHTML = response.message;              	
                        } else {
                       		self.argumentsInfo.domNode.innerHTML = "Command not found!";
                        }
                    },
                    function( error ) {
                    	alert( error );
                    }
                );
            },
            
            submitCommand: function() {
            	var self = this;
            	if (this.form.validate()) {
            		var data = this.id.get('value').split("_");
            		
            		Request.post(Config.COMMANDING.SEND_COMMAND_URL, {
	                    data: {
                            source: this.source.get('value'),
                            destination: data[1],
                            priority: this.priority.get('value'),
                            id: data[0],
                            CDHSSource: this.CDHSSource.get('value'),
                            CDHSBlockIndex: this.CDHSBlockIndex.get('value'),
                            arguments: this.arguments.get('value')
                        },
	                    handleAs: "json",
	                }).then(
	                    function( response ) {
                            self.setMessage( response.status != "ok", response.message );
                        },
	                    function( error ) {
	                    	alert( error );
	                    }
	                );
                    
                } else {
                    alert("Please insert correct values!");
                }
            	
            },
            
            placeAt: function(container) {
                DomConstruct.place( this.main, container);

            },
            
            removeMessages: function() {
            	this.errorMessageBox.style.display="none";
            	this.errorMessageBox.innerHTML ="";
            	this.okMessageBox.style.display="none";
            	this.okMessageBox.innerHTML ="";
            },
            
            setMessage: function( isError, message ) {
            	this.removeMessages();
            	if( isError ) {
            		this.errorMessageBox.innerHTML = message;
            		this.errorMessageBox.style.display = "";
            	} else {
            		this.okMessageBox.innerHTML = message;
            		this.okMessageBox.style.display = "";
            	}
            }
        });
    }
);
