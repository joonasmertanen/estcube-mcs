define([
    "dojo/_base/declare",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/on",
    "dojo/topic",
    "dijit/form/Form",
    "dijit/form/Button",
    "dijit/form/ValidationTextBox",
    "dijit/form/SimpleTextarea",
    "dijit/layout/ContentPane",
    "dojox/layout/TableContainer",
    "dgrid/OnDemandList",
    "config/config",
    "common/formatter/DateFormatter",
    "common/store/TleStore",
   ],

    function(declare, DomClass, DomConstruct, On, Topic, Form, Button, ValidationTextBox, SimpleTextarea, ContentPane, TableContainer, List, Config, DateFormatter, TleStore) {

        return declare([], {

            constructor: function(args) {
                    this.tleContainer = DomConstruct.create("div", { "class": "tle-container" });
                    var div = DomConstruct.create("div", { }, this.tleContainer);
                    var form = new Form({ encType: "multipart/form-data", action: "", method: "" }, div);

                    var table = new TableContainer({
                        cols: 1,
                        labelWidth: "100px",
                    });

                    var textSource = new ValidationTextBox({ name: "tleSource", label: "TLE Source", placeHolder: "Source of TLE", required: true });
                    var textTleValue = new SimpleTextarea({ name: "tleText", label: "TLE Value", cols: 70, rows: 2 });
                    var buttonWrapper = new ContentPane({ style: "margin: 0px; padding: 0px;" });
                    var buttonSubmit = new Button({ label: "Submit", title: "Upload TLE" });
                    buttonWrapper.addChild(buttonSubmit);

                    table.addChild(textSource);
                    table.addChild(textTleValue);
                    table.addChild(buttonWrapper);

                    form.domNode.appendChild(table.domNode);

                    tleList = new List({
                        store: TleStore,
                        query: { satellite: Config.TLE.SATELLITE },
                        renderRow: function(tle) {
                            var div = DomConstruct.create("div", { style: "padding: 3px; margin: 1px;" });
                            var spanDate = DomConstruct.create("span", { innerHTML: DateFormatter(tle.timestamp), style: "font-weight: bold; margin-right: 2em"}, div);
                            var spanDate = DomConstruct.create("span", { innerHTML: tle.satellite, style: "font-style: italic; margin-right: 2em"}, div);
                            var spanDate = DomConstruct.create("span", { innerHTML: "Uploader: TODO", style: "font-style: italic; margin-right: 2em; color: #dddddd"}, div);
                            var line1 = DomConstruct.create("p", { innerHTML: tle.tleLine1, style: "padding: 1px; margin: 1px; font-weight: normal;" }, div);
                            var line2 = DomConstruct.create("p", { innerHTML: tle.tleLine2, style: "padding: 1px; margin: 1px; font-weight: normal;" }, div);
                            return div;
                        },
                    });
                    tleList.set("sort", "timestamp", true);
                    DomClass.add(form.domNode, "pageElement");
                    DomClass.add(tleList.domNode, "pageElement");
                    DomConstruct.place(tleList.domNode, this.tleContainer);

                    table.startup();
                    tleList.startup();

                    On(buttonSubmit, "click", function() {
                        if (form.validate()) {
                            var value = form.get("value");
                            Topic.publish(Config.TLE.TOPIC_TLE_SUBMIT, value);
                            form.reset();
                        } else {
                            alert("Invalid data! Fix errors and resubmit!");
                        }
                    });
            },

            placeAt: function(container) {
                DomConstruct.place(this.tleContainer, container);
            },

        });
    }
);
