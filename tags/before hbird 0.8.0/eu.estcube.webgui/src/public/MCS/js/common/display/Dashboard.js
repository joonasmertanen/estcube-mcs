define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "dijit/registry",
    "dojox/layout/GridContainer",
    "dojox/widget/Portlet",
    "dojox/widget/PortletSettings",
    "./ContentProvider",
    ],

    function(declare, Arrays, Registry, GridContainer, Portlet, PortletSettings, Provider) {
        return declare([], {

                constructor: function(args) {
                    declare.safeMixin(this, args);
                    this.gridContainer = new dojox.layout.GridContainer({
                            nbZones: this.columns,
                            opacity: .5,
                            minColWidth: "30%",
                            hasResizableColumns: false,
                            allowAutoScroll: true,
                            withHandles: true,
                            dragHandleClass: "dijitTitlePaneTitle",
                            acceptTypes: ["Portlet"],
                            isOffset: true,
                        });

                    Arrays.forEach(this.config, function(item, index) {
                        var portlet = new dojox.widget.Portlet({
                            dndType: "Portlet",
                            title: item.title,
                            closable: item.closable,
                        });

                        if (item.settings != null) {
                            var settings = new dojox.widget.PortletSettings({ innerHTML: item.settings });
                            portlet.addChild(settings);
                        }

                        var provider = item.contentProvider == null ? new ContentProvider() : item.contentProvider;
                        var content = provider.getContent();
                        portlet.addChild(content);
                        if (item.col != null && item.row != null) {
                            this.gridContainer.addChild(portlet, item.col, item.row);
                        } else {
                            this.gridContainer.addChild(portlet);
                        }
                        provider.initialize();
                    }, this);
                    this.gridContainer.startup();
                },

                getContainer: function() {
                    return this.gridContainer;
                }
            });
    }
);

