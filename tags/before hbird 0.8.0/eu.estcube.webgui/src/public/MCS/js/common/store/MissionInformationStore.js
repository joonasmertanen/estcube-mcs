define([
    "dojo/store/Memory",
    "dojo/_base/array",
    "dojo/store/Observable",
    "config/config",
    "dojo/request",
    "./StoreMonitor",
    ],

    function(Memory, Arrays, Observable, Config, Request, StoreMonitor) {
        var store = new Observable(new Memory({ idProperty: "ID" }));
        new StoreMonitor({ store: store, storeName: "MissionInformationStore" });

        Request.get(Config.URL_CATALOGUE_SATELLITES, {handleAs: "json"})
               .then(function(data) {
                         Arrays.forEach(data, function(row) {store.put(row);});
                     });
        return store;
    }
);