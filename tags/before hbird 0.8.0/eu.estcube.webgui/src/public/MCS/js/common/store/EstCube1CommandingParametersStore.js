define([
    "dojo/store/Memory",
    "dojo/store/Observable",
    "config/config",
    "./DataHandler",
    "./StoreIdGenerator",
    "./StoreLimitHandler",
    "./StoreMonitor",
    ],

    function(Memory, Observable, Config, DataHandler, IdGenerator, StoreLimitHandler, StoreMonitor) {

        var channel = "/hbird.out.parameters";
        var storeId = "storeId";

        var store = new Observable(new Memory({ idProperty: storeId }));
        new StoreMonitor({ store: store, storeName: "SystemLogStore" });

        var handler = new DataHandler({ channels: [channel], callback: function(message, channel) {
        	if( message.name.substr(0,8) == "estcube." ) {
            	StoreLimitHandler.put(store, IdGenerator.generate(message, storeId), 500);
            }
        }});

        return store;
    }
);