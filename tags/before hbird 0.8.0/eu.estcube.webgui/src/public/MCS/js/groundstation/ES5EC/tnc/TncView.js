define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dgrid/OnDemandGrid",
    "dijit/layout/TabContainer",
    "dijit/layout/ContentPane",
    "config/config",
    "common/formatter/DateFormatter",
    "common/store/TransportFrameStore",
    ],

    function(declare, Lang, DomConstruct, DomClass, Grid, TabContainer, ContentPane, config, DateFormatter, store) {

        return declare([], {

            constructor: function(args) {
                var tncGrid = new Grid({
                    store: store,
                    query: function(item) {
                        return item.headers["class"] == "TncFrame";
                    },
                    columns: [
                        {
                            label: "Timestamp",
                            field: "timestamp",
                            formatter: DateFormatter,
                            className: "field-timestamp",
                        },
                        {
                            label: "Type",
                            field: "headers",
                            formatter: function(headers) {
                                return headers.communicationLinkType;
                            },
                            className: "field-communication-link-type",
                        },
                        {
                            label: "Ground Station",
                            field: "headers",
                            formatter: function(headers) {
                                return headers.location;
                            },
                            className: "field-groundStation",
                        },
                        {
                            label: "Satellite",
                            field: "headers",
                            formatter: function(headers) {
                                return headers.satellite;
                            },
                            className: "field-satellite",
                        },
                        {
                            label: "Contact",
                            field: "headers",
                            formatter: function(headers) {
                                return headers.contact;
                            },
                            className: "field-contact",
                        },
                        {
                            label: "Source",
                            field: "headers",
                            renderCell: function(object, value, node, options) {
                                node.innerHTML = object.headers.issuedBy + " : " + object.headers.serialPortName + " : " + object.frame.target;
                            },
                            className: "field-source",
                        },
                        {
                            label: "HEX Dump",
                            field: "frame",
                            formatter: function(frame) {
                                return frame.data;
                            },
                            className: "field-value",
                        },
                    ]
                });
                tncGrid.set("sort", "timestamp", true);

                var ax25Grid = new Grid({
                    store: store,
                    query: function(item) {
                        return item.headers["class"] == "Ax25UIFrame";
                    },
                    columns: [
                        {
                            label: "Timestamp",
                            field: "timestamp",
                            formatter: DateFormatter,
                            className: "field-timestamp",
                        },
                        {
                            label: "Type",
                            field: "headers",
                            formatter: function(headers) {
                                return headers.communicationLinkType;
                            },
                            className: "field-communication-link-type",
                        },
                        {
                            label: "Ground Station",
                            field: "headers",
                            formatter: function(headers) {
                                return headers.location || "-";
                            },
                            className: "field-groundStation",
                        },
                        {
                            label: "Satellite",
                            field: "headers",
                            formatter: function(headers) {
                                return headers.satellite || "-";
                            },
                            className: "field-satellite",
                        },
                        {
                            label: "Contact",
                            field: "headers",
                            formatter: function(headers) {
                                return headers.contact || "-";
                            },
                            className: "field-contact",
                        },
                        {
                            label: "Source",
                            field: "headers",
                            renderCell: function(object, value, node, options) {
                                node.innerHTML = object.headers.issuedBy + " : " + (object.headers.serialPortName || "-" ) + " : " + object.headers.tncPort;
                            },
                            className: "field-issuedBy",
                        },
                        {
                            label: "Destination Address",
                            field: "frame",
                            formatter: function(frame) {
                                return frame.destAddr;
                            },
                            className: "frame-address",
                        },
                        {
                            label: "Source Address",
                            field: "frame",
                            formatter: function(frame) {
                                return frame.srcAddr;
                            },
                            className: "frame-address",
                        },
                        {
                            label: "CTRL",
                            field: "frame",
                            formatter: function(frame) {
                                return frame.ctrl;
                            },
                            className: "field-short-value",
                        },
                        {
                            label: "PID",
                            field: "frame",
                            formatter: function(frame) {
                                return frame.pid;
                            },
                            className: "field-short-value",
                        },
                        {
                            label: "Info",
                            field: "frame",
                            formatter: function(frame) {
                                return frame.info;
                            },
                            className: "field-value",
                        },
                    ]
                });
                ax25Grid.set("sort", "timestamp", true);

                this.tabContainer = new TabContainer({ "class": "fill" });
                this.tabContainer.addChild(this.prepareGrid(tncGrid, "TNC Frames"));
                this.tabContainer.addChild(this.prepareGrid(ax25Grid, "AX.25 UI Frames"));

            },

            placeAt: function(container) {
                this.tabContainer.placeAt(container);
                this.tabContainer.startup();
            },

            prepareGrid: function(grid, title) {
                DomClass.add(grid.domNode, "fill");
                var gridPlaceholder = DomConstruct.create("div", { "class": "fill" });
                DomConstruct.place(grid.domNode, gridPlaceholder);

                var contentPane = new ContentPane({ title: title, content: gridPlaceholder });
                // XXX - 14.03.2013, kimmell - fix for dgrid header not visible bug
                contentPane.on("show", function() {
                    grid.set("showHeader", true);
                });

                return contentPane;
            },


        });
    }
);
