define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dijit/popup",
    "dijit/TooltipDialog",
    "config/config",
    "common/formatter/DateFormatter",
    "common/store/ParameterStore",
    "common/display/Dashboard",
    "common/display/GridContentProvider",
    "common/display/DGridTooltipSupport",
    ],

    function(declare, Arrays, DomClass, DomConstruct, Popup, TooltipDialog, Config, DateFormatter, ParameterStore, Dashboard, GridContentProvider, DGridTooltipSupport) {

        function capitaliseFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }

        function formatName(name) {
            var parts = name.split("/");
            var shortName = parts[parts.length - 1];
            var split = shortName.split(".");
            var finalName;
            split = Arrays.map(split, function(item) {
                return capitaliseFirstLetter(item);
            });
            return split.join(" ");
        }

        return declare([], {

            constructor: function(args) {
                var config = [
                    this.createProvider("meteo.physic.ut.ee"),
                    this.createProvider("emhi.ee"),
                    this.createProvider("ilm.ee"),
                ];
                this.dashboard = new Dashboard({ config: config, columns: Config.ES5EC_WEATHER.numberOfColumns });
                DomClass.add(this.dashboard.getContainer().domNode, "fill");
            },

            placeAt: function(container) {
                this.dashboard.getContainer().placeAt(container);
            },

            createProvider: function(source) {
                var provider = new GridContentProvider({
                    columns: {
                        name: { label: "Name", formatter: formatName, className: "field-short-name" },
                        value: { label: "Value", },
                        unit: { label: "Unit" },
                        timestamp: { label: "Time", formatter: DateFormatter },
                    },
                    store: ParameterStore,
                    query: function(message) {
                        return source == message.issuedBy;
                    },
                    orderBy: "name",
                    sortOrder: "ASC",
                    onStartup: function(provider) {
                        new DGridTooltipSupport(provider.grid, function(entry) {
                            return entry.description;
                        });
                    },
                });

                return {
                        title: source,
                        contentProvider: provider,
                };
            }

        });
    }
);
