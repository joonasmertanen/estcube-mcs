define([
    "dojo/_base/declare",
    "common/Controller",
    "./AntennaView",
    ],

    function(declare, Controller, AntennaView) {
        console.log("AntennaController");
        var s = declare([Controller], {

            constructor: function() {
                console.log("AntennaController.constructor");
                this.view = new AntennaView();
            },

            index: function(params) {
                console.log("AntennaController.index");
                this.placeWidget(this.view);
            },

        });
        return new s();
    }
);
