package eu.estcube.webserver.radiobeacon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;

import org.hbird.exchange.core.Issued;
import org.hbird.exchange.core.Label;
import org.hbird.exchange.core.Parameter;
import org.junit.Before;
import org.junit.Test;

import eu.estcube.codec.radiobeacon.RadioBeacon;
import eu.estcube.codec.radiobeacon.RadioBeaconTranslator;
import eu.estcube.codec.radiobeacon.parsers.RadioBeaconParametersNormalMode;
import eu.estcube.codec.radiobeacon.parsers.RadioBeaconParametersSafeMode;

public class RadioBeaconTranslatorTest {

    private RadioBeacon beacon1, beacon2, beacon3, beacon4, beacon5, beacon6, beacon7, beacon8, beacon9, beacon10,
            beacon11, beacon12, beacon13, beacon14;
    private RadioBeaconTranslator radioBeaconTranslator;
    private final String correctNormalBeacon = "ES5E/SEAAAAAAABBCCDDEEFFGGGHIIJJKKLLMMNNOOK";
    private final String correctSafeBeacon1 = "ES5E/STAAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSSKN";
    private final String correctSafeBeacon2 = "ES5E/STAA AAAAABBCCD DEEeeFFGGHHIIJJKKLLMMNNOPQQR RSSKN   ";
    private final String incorrectLenghtBeacon = "ES5AAAAAAAPRRRRKN";
    private final String incorrectBasisBeacon = "QS5R/STAAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSSKN";
    private final String nonASCIISafeBeacon = "ES5E/STAAAAAAABBCCDDEEEEFFGGHHáIJJKKLLMMNNOPQQRRSSKN"; //
    private final String correctNormalBeaconMode1 = "ES5E/SEAAAAAAABBCCDDEEFFGGGHIIJJKKLLMMNNOOK";
    private final String correctNormalBeaconMode2 = "ES5E/S#EAAAAAAABBCCDDEEFFGGGHIIJJKKLLMMNNOOK";
    private final String correctNormalBeaconMode3 = "ES5E/SEAAAAAAABBCCDDEEFFGGGHIIJJKKLLMMNNOO#";
    private final String correctNormalBeaconMode4 = "ES5E/S#AAAAAAABBCCDDEEFFGGGHIIJJKKLLMMNNOO#";
    private final String correctSafeBeaconMode1 = "ES5E/STAAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSSKN";
    private final String correctSafeBeaconMode2 = "ES5E/S#AAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSSKN";
    private final String correctSafeBeaconMode3 = "ES5E/STAAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSS##";
    private final String correctSafeBeaconMode4 = "ES5E/S#AAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSS##";

    private final String sampleMessageNormalMode = "ES5E/S ETASA ZZZZZ ZUABTCFFHTUS5BSNMFNFCAFE6HK";
    private final String sampleMessageSafeMode = "ES5E/S T TASAZZZ AT DA TT T6SN ZZ 6B TD 5B AB TC FF UE ZU B U WB NC SW KN";
    private final String samplePartialMessageNormalMode = "ES5E/SE###################################K";

    @Before
    public void setUp() throws Exception {
        radioBeaconTranslator = new RadioBeaconTranslator();
        beacon1 = new RadioBeacon(null, "", "", null);
        beacon2 = new RadioBeacon(null, "", "", null);
        beacon3 = new RadioBeacon(null, "", "", null);
        beacon4 = new RadioBeacon(null, "", "", null);
        beacon5 = new RadioBeacon(null, "", "", null);
        beacon6 = new RadioBeacon(null, "", "", null);
        beacon7 = new RadioBeacon(null, "", "", null);
        beacon8 = new RadioBeacon(null, "", "", null);
        beacon9 = new RadioBeacon(null, "", "", null);
        beacon10 = new RadioBeacon(null, "", "", null);
        beacon11 = new RadioBeacon(null, "", "", null);
        beacon12 = new RadioBeacon(null, "", "", null);
        beacon13 = new RadioBeacon(null, "", "", null);
        beacon14 = new RadioBeacon(null, "", "", null);

        beacon1.setValue(correctNormalBeacon);
        beacon2.setValue(correctSafeBeacon1);
        beacon3.setValue(incorrectLenghtBeacon);
        beacon4.setValue(correctSafeBeacon2);
        beacon5.setValue(incorrectBasisBeacon);
        beacon6.setValue(correctNormalBeaconMode1);
        beacon7.setValue(correctNormalBeaconMode2);
        beacon8.setValue(correctNormalBeaconMode3);
        beacon9.setValue(correctNormalBeaconMode4);
        beacon10.setValue(correctSafeBeaconMode1);
        beacon11.setValue(correctSafeBeaconMode2);
        beacon12.setValue(correctSafeBeaconMode3);
        beacon13.setValue(correctSafeBeaconMode4);
        beacon14.setValue(nonASCIISafeBeacon);

    }

    @Test
    public void checkBeaconLengthTest() throws Exception {
        assertTrue(radioBeaconTranslator.checkBeaconLength(beacon1.getValue()));
        assertTrue(radioBeaconTranslator.checkBeaconLength(beacon2.getValue()));
        assertFalse(radioBeaconTranslator.checkBeaconLength(beacon3.getValue()));
    }

    @Test
    public void determineRadioBeaconModeTest() throws Exception {
        assertEquals("Normal mode", radioBeaconTranslator.determineRadioBeaconMode(beacon6.getValue()));
        assertEquals("Normal mode", radioBeaconTranslator.determineRadioBeaconMode(beacon7.getValue()));
        assertEquals("Normal mode", radioBeaconTranslator.determineRadioBeaconMode(beacon8.getValue()));
        assertEquals("Undefined mode", radioBeaconTranslator.determineRadioBeaconMode(beacon9.getValue()));
        assertEquals("Safe mode", radioBeaconTranslator.determineRadioBeaconMode(beacon10.getValue()));
        assertEquals("Safe mode", radioBeaconTranslator.determineRadioBeaconMode(beacon11.getValue()));
        assertEquals("Safe mode", radioBeaconTranslator.determineRadioBeaconMode(beacon12.getValue()));
        assertEquals("Undefined mode", radioBeaconTranslator.determineRadioBeaconMode(beacon13.getValue()));
    }

    @Test
    public void reformBeaconMessageTest() throws Exception {
        assertEquals(correctSafeBeacon1, radioBeaconTranslator.reformBeaconMessage(beacon4.getValue()));
    }

    @Test
    public void checkRadioBeaconBasisTest() throws Exception {
        assertTrue(radioBeaconTranslator.checkRadioBeaconBasis(beacon1.getValue()));
        assertTrue(radioBeaconTranslator.checkRadioBeaconBasis(beacon2.getValue()));
        assertFalse(radioBeaconTranslator.checkRadioBeaconBasis(beacon5.getValue()));
    }

    @Test
    public void checkASCIIMessageTest() throws Exception {
        assertTrue(radioBeaconTranslator.checkASCIIMessage(beacon2.getValue()));
        assertFalse(radioBeaconTranslator.checkASCIIMessage(beacon14.getValue()));
    }

    @Test
    public void toParametersNormalModeTest() throws Exception {

        assertEquals(0, radioBeaconTranslator.toParameters(incorrectLenghtBeacon, null, "oliver").size());
        assertEquals(0, radioBeaconTranslator.toParameters(incorrectBasisBeacon, null, "oliver").size());
        assertEquals(0, radioBeaconTranslator.toParameters(nonASCIISafeBeacon, null, "oliver").size());

        HashMap<String, Issued> parameters = radioBeaconTranslator
                .toParameters(sampleMessageNormalMode, null, "peeter");

        assertEquals("ES5E/SETASAZZZZZZUABTCFFHTUS5BSNMFNFCAFE6HK",
                ((RadioBeacon) (parameters.get(RadioBeaconParametersSafeMode.RADIO_BEACON.getName()))).getValue());
        assertEquals("E",
                ((Label) (parameters.get(RadioBeaconParametersNormalMode.OPERATING_MODE.getName()))).getValue());
        assertEquals("peeter",
                ((Label) (parameters.get(RadioBeaconParametersNormalMode.OPERATING_MODE.getName()))).getIssuedBy());
        assertEquals(Long.parseLong("1352902792"),
                ((Label) (parameters.get(RadioBeaconParametersNormalMode.OPERATING_MODE.getName()))).getTimestamp());
        assertEquals(Long.parseLong("1352902792"),
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.TIMESTAMP.getName()))).getValue());
        assertEquals(2.414189978213456, /* 136 */
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.MAIN_BUS_VOLTAGE.getName()))).getValue());
        assertEquals(-126,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.AVERAGE_POWER_BALANCE.getName())))
                        .getValue());
        assertEquals(3.028209702144293/* 171 */,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.BATTERY_A_VOLTAGE.getName()))).getValue());
        assertEquals(0.22542297503644695/* 12 */,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.BATTERY_B_VOLTAGE.getName()))).getValue());
        assertEquals(120.9334 /* 255 */,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.BATTERY_A_TEMPERATURE.getName())))
                        .getValue());
        assertEquals(360.87933561309234,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.SPIN_RATE_Z.getName()))).getValue());
        assertEquals(3,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.RECEIVED_SIGNAL_STRENGTH.getName())))
                        .getValue());

        assertEquals(1,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.SATELLITE_MISSION_PHASE.getName())))
                        .getValue());
        assertEquals(1,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_RESET_CDHS.getName())))
                        .getValue());
        assertEquals(2,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_RESET_COM.getName())))
                        .getValue());
        assertEquals(3,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_RESET_EPS.getName())))
                        .getValue());

        assertEquals(1.1176470588235294,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.TETHER_CURRENT.getName()))).getValue());

        assertEquals(1,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_ADCS.getName())))
                        .getValue());
        assertEquals(3,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_CDHS.getName())))
                        .getValue());
        assertEquals(3,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_COM.getName())))
                        .getValue());
        assertEquals(3,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_EPS.getName())))
                        .getValue());

        assertEquals(39,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.CDHS_SYSTEM_STATUS_CODE.getName())))
                        .getValue());
        assertEquals(3,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.CDHS_SYSTEM_STATUS_VALUE.getName())))
                        .getValue());

        assertEquals(202,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.EPS_STATUS_CODE.getName()))).getValue());

        assertEquals(63,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.ADCS_SYSTEM_STATUS_CODE.getName())))
                        .getValue());
        assertEquals(2,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.ADCS_SYSTEM_STATUS_VALUE.getName())))
                        .getValue());

        assertEquals(25,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.COM_SYSTEM_STATUS_CODE.getName())))
                        .getValue());
        assertEquals(0,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.COM_SYSTEM_STATUS_VALUE.getName())))
                        .getValue());

    }

    @Test
    public void toParametersNormalModePartialMessageTest() throws Exception {

        // Part message test
        HashMap<String, Issued> parameters2 = radioBeaconTranslator.toParameters(samplePartialMessageNormalMode,
                (long) 10, "oliver");

        assertEquals(samplePartialMessageNormalMode,
                ((RadioBeacon) (parameters2.get(RadioBeaconParametersSafeMode.RADIO_BEACON.getName())))
                        .getValue());
        assertEquals("E",
                ((Label) (parameters2.get(RadioBeaconParametersNormalMode.OPERATING_MODE.getName()))).getValue());
        assertEquals("oliver",
                ((Label) (parameters2.get(RadioBeaconParametersNormalMode.OPERATING_MODE.getName()))).getIssuedBy());
        assertEquals(10,
                ((Label) (parameters2.get(RadioBeaconParametersNormalMode.OPERATING_MODE.getName()))).getTimestamp());
        assertEquals(null, ((parameters2.get(RadioBeaconParametersNormalMode.TIMESTAMP.getName()))));
        assertEquals(null, ((parameters2.get(RadioBeaconParametersNormalMode.MAIN_BUS_VOLTAGE.getName()))));
        assertEquals(null,
                ((parameters2.get(RadioBeaconParametersNormalMode.AVERAGE_POWER_BALANCE.getName()))));
        assertEquals(null, ((parameters2.get(RadioBeaconParametersNormalMode.BATTERY_A_VOLTAGE.getName()))));
        assertEquals(null, ((parameters2.get(RadioBeaconParametersNormalMode.BATTERY_B_VOLTAGE.getName()))));
        assertEquals(null,
                ((parameters2.get(RadioBeaconParametersNormalMode.BATTERY_A_TEMPERATURE.getName()))));
        assertEquals(null, ((parameters2.get(RadioBeaconParametersNormalMode.SPIN_RATE_Z.getName()))));
        assertEquals(null,
                ((parameters2.get(RadioBeaconParametersNormalMode.RECEIVED_SIGNAL_STRENGTH.getName()))));

        assertEquals(null,
                ((parameters2.get(RadioBeaconParametersNormalMode.SATELLITE_MISSION_PHASE.getName()))));
        assertEquals(null,
                ((parameters2.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_RESET_CDHS.getName()))));
        assertEquals(null,
                ((parameters2.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_RESET_COM.getName()))));
        assertEquals(null,
                ((parameters2.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_RESET_EPS.getName()))));

        assertEquals(null, ((parameters2.get(RadioBeaconParametersNormalMode.TETHER_CURRENT.getName()))));

        assertEquals(null,
                ((parameters2.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_ADCS.getName()))));
        assertEquals(null,
                ((parameters2.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_CDHS.getName()))));
        assertEquals(null,
                ((parameters2.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_COM.getName()))));
        assertEquals(null,
                ((parameters2.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_EPS.getName()))));

        assertEquals(null,
                ((parameters2.get(RadioBeaconParametersNormalMode.CDHS_SYSTEM_STATUS_CODE.getName()))));
        assertEquals(null,
                ((parameters2.get(RadioBeaconParametersNormalMode.CDHS_SYSTEM_STATUS_VALUE.getName()))));

        assertEquals(null, ((parameters2.get(RadioBeaconParametersNormalMode.EPS_STATUS_CODE.getName()))));

        assertEquals(null,
                ((parameters2.get(RadioBeaconParametersNormalMode.ADCS_SYSTEM_STATUS_CODE.getName()))));
        assertEquals(null,
                ((parameters2.get(RadioBeaconParametersNormalMode.ADCS_SYSTEM_STATUS_VALUE.getName()))));

        assertEquals(null,
                ((parameters2.get(RadioBeaconParametersNormalMode.COM_SYSTEM_STATUS_CODE.getName()))));
        assertEquals(null,
                ((parameters2.get(RadioBeaconParametersNormalMode.COM_SYSTEM_STATUS_VALUE.getName()))));

    }

    @Test
    public void toParametersSafeModeTest() throws Exception {

        // safe mode test
        HashMap<String, Issued> parameters3 = radioBeaconTranslator.toParameters(sampleMessageSafeMode, null, "oliver");

        assertEquals("ES5E/STTASAZZZATDATTT6SNZZ6BTD5BABTCFFUEZUBUWBNCSWKN",
                ((RadioBeacon) (parameters3.get(RadioBeaconParametersSafeMode.RADIO_BEACON.getName())))
                        .getValue());
        assertEquals("T",
                ((Label) (parameters3.get(RadioBeaconParametersSafeMode.OPERATING_MODE.getName()))).getValue());
        assertEquals("oliver",
                ((Label) (parameters3.get(RadioBeaconParametersNormalMode.OPERATING_MODE.getName()))).getIssuedBy());
        assertEquals(Long.parseLong("1352902792"),
                ((Label) (parameters3.get(RadioBeaconParametersSafeMode.OPERATING_MODE.getName()))).getTimestamp());
        assertEquals(Long.parseLong("1352902792"),
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.TIMESTAMP.getName()))).getValue());
        assertEquals(160,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.ERROR_CODE_1.getName()))).getValue());
        assertEquals(218,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.ERROR_CODE_2.getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.ERROR_CODE_3.getName()))).getValue());

        assertEquals(1593,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.TIME_IN_SAFE_MODE.getName()))).getValue());
        assertEquals(2.414189978213456 /* 136 */,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.MAIN_BUS_VOLTAGE.getName()))).getValue());

        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.CDHS_PROCESSOR_A.getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.CDHS_PROCESSOR_B.getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.CDHS_BUS_SWITCH.getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.COM_3V3_VOLTAGE_LINE.getName())))
                        .getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.COM_5V_VOLTAGE_LINE.getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.PL_3V3_VOLTAGE_LINE.getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.PL_5V_VOLTAGE_LINE.getName()))).getValue());
        assertEquals(1, ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.CAM_VOLTAGE.getName()))).getValue());

        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.ADCS_VOLTAGE.getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.BATTERY_A_CHARGING.getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.BATTERY_A_DISCHARGING.getName())))
                        .getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.BATTERY_B_CHARGING.getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.BATTERY_B_DISCHARGING.getName())))
                        .getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.SECONDARY_POWER_BUS_A.getName())))
                        .getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.SECONDARY_POWER_BUS_B.getName())))
                        .getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.V12_LINE_VOLTAGE.getName()))).getValue());

        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.REGULATOR_A_5V.getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.REGULATOR_B_5V.getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.LINE_VOLTAGE_5V.getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.REGULATOR_A_3V3.getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.REGULATOR_B_3V3.getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.LINE_VOLTAGE_3V3.getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.REGULATOR_A_12V.getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.REGULATOR_B_12V.getName()))).getValue());

        assertEquals(3.028209702144293 /* 171 */,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.BATTERY_A_VOLTAGE.getName()))).getValue());
        assertEquals(0.22542297503644695 /* 12 */,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.BATTERY_B_VOLTAGE.getName()))).getValue());
        assertEquals(120.9334 /* 255 */,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.BATTERY_A_TEMPERATURE.getName())))
                        .getValue());
        assertEquals(-28.271700000000003 /* 46 */,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.BATTERY_B_TEMPERATURE.getName())))
                        .getValue());
        assertEquals(-126,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.AVERAGE_POWER_BALANCE.getName())))
                        .getValue());
        assertEquals(11,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.FIRMWARE_VERSION_NUMBER.getName())))
                        .getValue());
        assertEquals(2,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.NUMBER_OF_CRASHES.getName()))).getValue());

        assertEquals(27,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.FORWARDED_RF_POWER.getName()))).getValue());
        assertEquals(-100,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.REFLECTED_RF_POWER.getName()))).getValue());
        assertEquals(
                49, // @TODO spec ütleb, et see peaks olema 81, kas asi ei tööta
                    // õigesti?
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.RECEIVED_SIGNAL_STRENGTH.getName())))
                        .getValue());
    }
}
