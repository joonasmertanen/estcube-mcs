package eu.estcube.codec.gcp.struct.parameters;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilderFactory;

import org.hbird.exchange.core.Parameter;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

public class GcpParameterInt32Test {
    private final GcpParameterInt32 p = new GcpParameterInt32("a", "b", "c", false);
    private final GcpParameterInt32 a = new GcpParameterInt32("a", "b", "c", true);

    @Test
    public void GcpParameterBit() throws Exception {

        String xml = "<param><name>test</name><description>a</description><unit>b</unit><type little_endian=\"false\">uint16</type></param>";
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                .parse(new InputSource(new StringReader(xml)));

        GcpParameterInt16 s = new GcpParameterInt16((Element) doc.getElementsByTagName("param").item(0));
        assertEquals("test", s.getName());
        assertEquals("a", s.getDescription());
        assertEquals("b", s.getUnit());
        assertEquals(false, s.getIsLittleEndian());
    }

    @Test
    public void getLength() {
        assertEquals(32, p.getLength());
    }

    @Test
    public void toValueStr() {
        assertEquals(2147483647, (int) p.toValue("2147483647")); // max
        assertEquals(0, (int) p.toValue("0")); // 0
        assertEquals(-1, (int) p.toValue("-1")); // rnd
        assertEquals(-2147483648, (int) p.toValue("-2147483648")); // min

        assertEquals(-2147483648, (int) p.toValue("0x80000000")); // min
        assertEquals(2147483647, (int) p.toValue("0x7FFFFFFF")); // min
        assertEquals(-1, (int) p.toValue("0xFFFFFFFF")); // max

        try {
            p.toValue("2147483648");
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        try {
            p.toValue("-2147483649"); // <min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }
    }

    @Test
    public void toBytes() {
        assertArrayEquals(new byte[] { -128, 0, 0, 0 }, p.toBytes(-2147483648)); // min
        assertArrayEquals(new byte[] { 127, -1, -1, -1 }, p.toBytes(2147483647)); // max
        assertArrayEquals(new byte[] { 0, 0, 0, 0 }, p.toBytes(0)); // 0
        try {
            p.toBytes(-2147483649L); // <min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        try {
            p.toBytes(2147483648L); // >max
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        assertArrayEquals(new byte[] { 0, 0, 0, -128 }, a.toBytes(-2147483648)); // min
        assertArrayEquals(new byte[] { -1, -1, -1, 127 }, a.toBytes(2147483647)); // max
        assertArrayEquals(new byte[] { 0, 0, 0, 0 }, a.toBytes(0)); // 0
        try {
            a.toBytes(-2147483649L); // <min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        try {
            a.toBytes(2147483648L); // >min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }
    }

    @Test
    public void toValueByte() {
        assertEquals(-2147483648, (int) p.toValue(new byte[] { -128, 0, 0, 0 })); // min
        assertEquals(2147483647, (int) p.toValue(new byte[] { 127, -1, -1, -1 })); // min
        assertEquals(0, (int) p.toValue(new byte[] { 0, 0, 0, 0 })); // 0

        assertEquals(-2147483648, (int) a.toValue(new byte[] { 0, 0, 0, -128 })); // min
        assertEquals(2147483647, (int) a.toValue(new byte[] { -1, -1, -1, 127 })); // max
        assertEquals(0, (int) a.toValue(new byte[] { 0, 0, 0, 0 })); // 0

        assertEquals(-1431655766, (int) p.toValue(new byte[] { -86, -86, -86, -86 }));

        try {
            p.toValue(new byte[] { 0, 0, 0, 0, 0 }); // more bytes
            fail("Must throw RuntimeException");
        } catch (RuntimeException e) {
        }

        try {
            p.toValue(new byte[] { 0, 0, 0 }); // less bytes
            fail("Must throw RuntimeException");
        } catch (RuntimeException e) {
        }
    }

    @Test
    public void getIssued() {
        Parameter parameter = p.getIssued("pref.", new byte[] { -128, 0, 0, 0 });
        assertEquals("pref.a", parameter.getName());
        assertEquals("b", parameter.getDescription());
        assertEquals(null, parameter.getIssuedBy());
        assertEquals(-2147483648, parameter.getValue());
        assertEquals("c", parameter.getUnit());
    }

    @Test
    public void getValueClass() {
        assertEquals(Integer.class, p.getValueClass());
    }
}
