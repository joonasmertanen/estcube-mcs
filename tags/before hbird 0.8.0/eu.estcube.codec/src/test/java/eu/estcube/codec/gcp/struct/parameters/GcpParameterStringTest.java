package eu.estcube.codec.gcp.struct.parameters;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilderFactory;

import org.hbird.exchange.core.Label;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

public class GcpParameterStringTest {

    private final GcpParameterString p = new GcpParameterString("a", "b");

    @Test
    public void GcpParameterBit() throws Exception {

        String xml = "<param><name>test</name><description>a</description><unit>b</unit></param>";
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                .parse(new InputSource(new StringReader(xml)));

        GcpParameterString s = new GcpParameterString((Element) doc.getElementsByTagName("param").item(0));
        assertEquals("test", s.getName());
        assertEquals("a", s.getDescription());
        assertEquals("b", s.getUnit());
    }

    @Test
    public void getLength() {
        assertEquals(-1, p.getLength());
    }

    @Test
    public void toValueStr() {

        assertEquals("Oliver testib striniks parsimist",
                p.toValue("Oliver testib striniks parsimist"));
        assertEquals("\u0000", p.toValue("\u0000"));
        assertEquals("0.1234567897654", p.toValue("0.1234567897654"));
    }

    @Test
    public void toBytes() {
        assertArrayEquals(new byte[] { 0, 0 }, p.toBytes("\u0000"));
        assertArrayEquals(new byte[] { 0, 97, 0, 98 }, p.toBytes("ab"));
    }

    @Test
    public void toValueByte() {
        assertEquals("\u0000", p.toValue(new byte[] { 0, 0 }));
        assertEquals("ab", p.toValue(new byte[] { 0, 97, 0, 98 }));
    }

    @Test
    public void getIssued() {
        Label label = p.getIssued("pref.", new byte[] { 0, 97, 0, 98 });
        assertEquals("pref.a", label.getName());
        assertEquals("b", label.getDescription());
        assertEquals(null, label.getIssuedBy());
        assertEquals("ab", label.getValue());
    }

    @Test
    public void getValueClass() {
        assertEquals(String.class, p.getValueClass());
    }
}
