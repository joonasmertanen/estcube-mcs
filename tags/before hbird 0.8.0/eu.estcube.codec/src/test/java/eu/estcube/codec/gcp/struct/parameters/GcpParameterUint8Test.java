package eu.estcube.codec.gcp.struct.parameters;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilderFactory;

import org.hbird.exchange.core.Parameter;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

public class GcpParameterUint8Test {

    private final GcpParameterUint8 p = new GcpParameterUint8("a", "b", "c");

    @Test
    public void GcpParameterBit() throws Exception {

        String xml = "<param><name>test</name><description>a</description><unit>b</unit></param>";
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                .parse(new InputSource(new StringReader(xml)));

        GcpParameterUint8 s = new GcpParameterUint8((Element) doc.getElementsByTagName("param").item(0));
        assertEquals("test", s.getName());
        assertEquals("a", s.getDescription());
        assertEquals("b", s.getUnit());
    }

    @Test
    public void getLength() {
        assertEquals(8, p.getLength());
    }

    @Test
    public void toValueStr() {
        assertEquals(255, (short) p.toValue("255")); // max
        assertEquals(0, (short) p.toValue("0")); // min
        assertEquals(129, (short) p.toValue("129")); // rnd

        assertEquals(128, (short) p.toValue("0x80")); // min
        assertEquals(127, (short) p.toValue("0x7F")); // min
        assertEquals(255, (short) p.toValue("0xFF")); // max

        try {
            p.toValue("256"); // >max
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        try {
            p.toValue("-1"); // <min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }
    }

    @Test
    public void toBytes() {

        assertArrayEquals(new byte[] { 0 }, p.toBytes(0)); // min
        assertArrayEquals(new byte[] { -1 }, p.toBytes(255)); // max
        assertArrayEquals(new byte[] { -128 }, p.toBytes(128)); // misc
        assertArrayEquals(new byte[] { 127 }, p.toBytes(127)); // misc

        try {
            p.toBytes(-1); // <min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        try {
            p.toBytes(256); // >min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

    }

    @Test
    public void toValueByte() {

        assertEquals(255, (int) p.toValue(new byte[] { -1 })); // max
        assertEquals(128, (int) p.toValue(new byte[] { -128 })); // misc
        assertEquals(127, (int) p.toValue(new byte[] { 127 })); // misc
        assertEquals(0, (int) p.toValue(new byte[] { 0 })); // 0

        try {
            p.toValue(new byte[] { 0, 0 }); // more bytes
            fail("Must throw RuntimeException");
        } catch (RuntimeException e) {
        }

    }

    @Test
    public void getIssued() {
        Parameter parameter = p.getIssued("pref.", new byte[] { -128 });
        assertEquals("pref.a", parameter.getName());
        assertEquals("b", parameter.getDescription());
        assertEquals(null, parameter.getIssuedBy());
        assertEquals((short) 128, parameter.getValue());
        assertEquals("c", parameter.getUnit());
    }

    @Test
    public void getValueClass() {
        assertEquals(Short.class, p.getValueClass());
    }
}
