package eu.estcube.webserver.radiobeacon.parser;

import static org.junit.Assert.assertEquals;

import org.hbird.exchange.core.Parameter;
import org.junit.Before;
import org.junit.Test;

import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserInteger;

public class RadionBeaconMessageParserIntegerTest {

    private static final String ISSUED_BY = "ISSUER";
    private static final String NAME = "NAME";
    private static final String DESCRIPTION = "DESCRIPTION";
    private static final String UNIT = "UNIT";

    private RadionBeaconMessageParserInteger parser1;
    private RadionBeaconMessageParserInteger parser2;

    @Before
    public void setUp() throws Exception {
        parser1 = new RadionBeaconMessageParserInteger(0, 2, new Parameter(ISSUED_BY, NAME, DESCRIPTION, UNIT));
        parser2 = new RadionBeaconMessageParserInteger(0, 4, new Parameter(ISSUED_BY, NAME, DESCRIPTION, UNIT));

    }

    @Test
    public void parseTest() throws Exception {
        assertEquals(parser1.parse("AT"), "160");
        assertEquals(parser2.parse("T6SN"), "1593");
    }
}
