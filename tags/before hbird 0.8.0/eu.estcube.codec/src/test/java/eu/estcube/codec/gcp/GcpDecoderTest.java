package eu.estcube.codec.gcp;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;

import org.hbird.exchange.core.Binary;
import org.hbird.exchange.core.Issued;
import org.hbird.exchange.core.Parameter;
import org.junit.Before;
import org.junit.Test;

import eu.estcube.codec.gcp.exceptions.SubsystemNotFoundException;
import eu.estcube.codec.gcp.struct.GcpReply;
import eu.estcube.codec.gcp.struct.GcpStruct;
import eu.estcube.codec.gcp.struct.GcpSubsystem;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterBit;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterByteArray;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterUint32;
import eu.estcube.common.ByteUtil;

public class GcpDecoderTest {

    private GcpStruct struct;

    private final byte[] message = { 0x02, 0x06, 0x00, 0x08, 0x00, 0x01, 0x20, 0x04, 0x20, 0x1B, 0x27, 0x01, -86, 1, 2,
            3, -127 };
    private final byte[] invalidCodeMessage = { 0x02, 0x06, 0x00, 0x08, 0x00, 0x15, 0x20, 0x04, 0x20, 0x1B, 0x27, 0x01,
            -86, 1, 2,
            3, -127 };

    @Before
    public void setup() {
        struct = new GcpStruct();

        GcpSubsystem CDHS = new GcpSubsystem("CDHS");

        GcpReply c1 = new GcpReply(1, "pong", "Pong");
        c1.addParameter(new GcpParameterUint32("timestamp", "Timestamp", "s", false));
        c1.addParameter(new GcpParameterBit("b1", "B1", ""));
        c1.addParameter(new GcpParameterBit("b2", "B2", ""));
        c1.addParameter(new GcpParameterBit("b3", "B3", ""));
        c1.addParameter(new GcpParameterBit("b4", "B4", ""));
        c1.addParameter(new GcpParameterBit("b5", "B5", ""));
        c1.addParameter(new GcpParameterBit("b6", "B6", ""));
        c1.addParameter(new GcpParameterBit("b7", "B7", ""));
        c1.addParameter(new GcpParameterBit("b8", "B8", ""));
        c1.addParameter(new GcpParameterByteArray("bytearray", "Bytearray"));
        try {
            c1.addParameter(new GcpParameterBit("b8", "B8", ""));
            fail("Must not be able to add new elements after MaxLength parameter type");
        } catch (Exception e) {

        }
        c1.addSubsystem(CDHS);
        struct.addReply(c1);
    }

    @Test
    public void testDecode() throws SubsystemNotFoundException {
        GcpDecoder decoder = new GcpDecoder();

        try {
            decoder.decode(invalidCodeMessage, struct, "");
            fail("Must throw exception for invalid messages");
        } catch (Exception e) {
        }

        List<Issued> result = decoder.decode(message, struct, "");
        assertEquals(20, result.size());

        Parameter p = (Parameter) result.get(0);
        result.get(1);

        assertEquals((long) 538650369, p.getValue());
        // assertEquals( m.); // @TODO Test metadata values

        assertEquals(1, ((Parameter) result.get(2)).getValue());
        assertEquals(0, ((Parameter) result.get(4)).getValue());
        assertEquals(1, ((Parameter) result.get(6)).getValue());
        assertEquals(0, ((Parameter) result.get(8)).getValue());
        assertEquals(1, ((Parameter) result.get(10)).getValue());
        assertEquals(0, ((Parameter) result.get(12)).getValue());
        assertEquals(1, ((Parameter) result.get(14)).getValue());
        assertEquals(0, ((Parameter) result.get(16)).getValue());

        assertArrayEquals(new byte[] { 1, 2, 3, -127 }, ((Binary) result.get(18)).getRawData());

    }

    @Test
    public void testBytesCopier() {

        GcpDecoder decoder = new GcpDecoder();

        byte[] bytes1 = { 0, -86, 0 };
        byte[] result1 = { -86 };
        assertArrayEquals(result1, decoder.byteCopier(bytes1, 8, 8));

        byte[] bytes3 = { 0, -86, 0 };
        byte[] result31 = { 1 };
        byte[] result32 = { 0 };
        byte[] result33 = { 1 };
        byte[] result34 = { 0 };
        byte[] result35 = { 1 };
        byte[] result36 = { 0 };
        byte[] result37 = { 1 };
        byte[] result38 = { 0 };
        assertArrayEquals(result31, decoder.byteCopier(bytes3, 8, 1));
        assertArrayEquals(result32, decoder.byteCopier(bytes3, 9, 1));
        assertArrayEquals(result33, decoder.byteCopier(bytes3, 10, 1));
        assertArrayEquals(result34, decoder.byteCopier(bytes3, 11, 1));
        assertArrayEquals(result35, decoder.byteCopier(bytes3, 12, 1));
        assertArrayEquals(result36, decoder.byteCopier(bytes3, 13, 1));
        assertArrayEquals(result37, decoder.byteCopier(bytes3, 14, 1));
        assertArrayEquals(result38, decoder.byteCopier(bytes3, 15, 1));

    }

    @Test
    public void testDecodingEpsDebugData() throws Exception {

        String message = "" +
                "00 06 00 76 02 03 00 72 " +
                "E6 00 81 00 D5 0C 6D 0A " +

                "6B 01 00 00 D6 0F 40 01 " +
                "1A 00 17 00 00 00 01 00 " +

                "9F 00 27 00 00 00 E6 00 " +
                "00 00 00 00 4F 00 E6 00 " +

                "00 00 00 00 B5 00 00 00 " +
                "00 00 00 00 D8 00 07 00 " +

                "0C 02 05 00 56 0A D9 03 " +
                "0D 00 04 00 5D 0A 2C 01 " +

                "60 0A B8 02 C2 0F FE 02 " +
                "EE 07 12 00 00 00 00 00 " +

                "01 00 00 00 00 00 00 00 " +
                "6D 00 D5 00 D5 00 00 00 " +

                "00 00 00 00 8E 0F 66 00 " +
                "00 00";
        byte[] bytes = ByteUtil.toBytesFromHexString(message);

        GcpStruct struct = new GcpStruct();
        struct.parseRepliesFromXml(struct.loadResourceXml("epsDebugReply.xml"));

        GcpDecoder decoder = new GcpDecoder();
        List<Issued> list = decoder.decode(bytes, struct, "e.{$subsystem}.");

        // For printing the results:
        // int i = 0;
        // for (Issued issued : list) {
        //
        // if (!(issued instanceof Metadata)) {
        //
        // String value = "";
        // if (issued instanceof Parameter) {
        // value += String.valueOf(((Parameter) issued).getValue());
        // }
        //
        // if (i % 8 == 0) {
        // System.out.println("---");
        // }
        // System.out.println("[Ax25UiFrameToIssuedProcessor] " + i + ") " +
        // issued.getClass().getName() + " "
        // + issued.getName()
        // + ": " + value);
        // }
        // i++;
        // }

        assertEquals("e.eps.mpb_avr", ((Parameter) list.get(0)).getName());
        assertEquals(230, ((Parameter) list.get(0)).getValue());
        assertEquals("e.eps.mpb_ext", ((Parameter) list.get(2)).getName());
        assertEquals(129, ((Parameter) list.get(2)).getValue());
        assertEquals("e.eps.mpb_ext1280", ((Parameter) list.get(4)).getName());
        assertEquals(3285, ((Parameter) list.get(4)).getValue());
        assertEquals("e.eps.reg_3v3_out", ((Parameter) list.get(6)).getName());
        assertEquals(2669, ((Parameter) list.get(6)).getValue());
        assertEquals("e.eps.reg3v3_a_cs", ((Parameter) list.get(8)).getName());
        assertEquals(363, ((Parameter) list.get(8)).getValue());
        assertEquals("e.eps.reg3v3_b_cs", ((Parameter) list.get(10)).getName());
        assertEquals(0, ((Parameter) list.get(10)).getValue());
        assertEquals("e.eps.reg5v_out", ((Parameter) list.get(12)).getName());
        assertEquals(4054, ((Parameter) list.get(12)).getValue());
        assertEquals("e.eps.reg5v_a_cs", ((Parameter) list.get(14)).getName());
        assertEquals(320, ((Parameter) list.get(14)).getValue());

        assertEquals("e.eps.bpbtb", ((Parameter) list.get(128)).getName());
        assertEquals(1, ((Parameter) list.get(128)).getValue());
        assertEquals("e.eps.payload3v3", ((Parameter) list.get(132)).getName());
        assertEquals(0, ((Parameter) list.get(132)).getValue());
        assertEquals("e.eps.cdhs3v3bsw", ((Parameter) list.get(134)).getName());
        assertEquals(1, ((Parameter) list.get(134)).getValue());
        assertEquals("e.eps.com5v", ((Parameter) list.get(144)).getName());
        assertEquals(1, ((Parameter) list.get(144)).getValue());
        assertEquals("e.eps.payload5v", ((Parameter) list.get(148)).getName());
        assertEquals(0, ((Parameter) list.get(148)).getValue());
        assertEquals("e.eps.spba", ((Parameter) list.get(108)).getName());
        assertEquals(1, ((Parameter) list.get(108)).getValue());
        assertEquals("e.eps.spbb", ((Parameter) list.get(110)).getName());
        assertEquals(0, ((Parameter) list.get(110)).getValue());
        assertEquals("e.eps.reg5va", ((Parameter) list.get(116)).getName());
        assertEquals(1, ((Parameter) list.get(116)).getValue());

    }
}
