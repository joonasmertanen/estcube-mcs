package eu.estcube.codec.gcp.struct.parameters;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilderFactory;

import org.hbird.exchange.core.Parameter;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

public class GcpParameterInt8Test {
    private final GcpParameterInt8 p = new GcpParameterInt8("a", "b", "c");

    @Test
    public void GcpParameterBit() throws Exception {

        String xml = "<param><name>test</name><description>a</description><unit>b</unit></param>";
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                .parse(new InputSource(new StringReader(xml)));

        GcpParameterInt8 s = new GcpParameterInt8((Element) doc.getElementsByTagName("param").item(0));
        assertEquals("test", s.getName());
        assertEquals("a", s.getDescription());
        assertEquals("b", s.getUnit());
    }

    @Test
    public void getLength() {
        assertEquals(8, p.getLength());
    }

    @Test
    public void toValueStr() {
        assertEquals(127, (byte) p.toValue("127")); // max
        assertEquals(0, (byte) p.toValue("0")); // 0
        assertEquals(-1, (byte) p.toValue("-1")); // rnd
        assertEquals(-128, (byte) p.toValue("-128")); // min

        assertEquals(-128, (int) p.toValue("0x80")); // min
        assertEquals(127, (int) p.toValue("0x7F")); // min
        assertEquals(-1, (int) p.toValue("0xFF")); // max

        try {
            p.toValue("128");
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        try {
            p.toValue("-129"); // <min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }
    }

    @Test
    public void toBytes() {

        assertArrayEquals(new byte[] { -128 }, p.toBytes(-128)); // min
        assertArrayEquals(new byte[] { 127 }, p.toBytes(127)); // max
        assertArrayEquals(new byte[] { 0 }, p.toBytes(0)); // 0
        try {
            p.toBytes(-129); // <min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        try {
            p.toBytes(128); // >max
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

    }

    @Test
    public void toValueByte() {

        assertEquals(-128, (byte) p.toValue(new byte[] { -128 })); // min
        assertEquals(127, (byte) p.toValue(new byte[] { 127 })); // min
        assertEquals(0, (byte) p.toValue(new byte[] { 0 })); // 0
        assertEquals(-1, (byte) p.toValue(new byte[] { -1 })); // 0
        try {
            p.toValue(new byte[] { 0, 0 }); // more bytes
            fail("Must throw RuntimeException");
        } catch (RuntimeException e) {
        }

    }

    @Test
    public void getIssued() {
        Parameter parameter = p.getIssued("pref.", new byte[] { -128 });
        assertEquals("pref.a", parameter.getName());
        assertEquals("b", parameter.getDescription());
        assertEquals(null, parameter.getIssuedBy());
        assertEquals((byte) -128, parameter.getValue());
        assertEquals("c", parameter.getUnit());
    }

    @Test
    public void getValueClass() {
        assertEquals(Byte.class, p.getValueClass());
    }
}
