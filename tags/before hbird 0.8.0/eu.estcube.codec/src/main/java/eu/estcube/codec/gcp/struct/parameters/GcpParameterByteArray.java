package eu.estcube.codec.gcp.struct.parameters;

import org.hbird.exchange.core.Binary;
import org.w3c.dom.Element;

import eu.estcube.codec.gcp.struct.GcpParameter;
import eu.estcube.common.ByteUtil;

public class GcpParameterByteArray extends GcpParameter implements GcpParameterMaxLength {

    public GcpParameterByteArray(String name, String description) {
        super(name, description, "");
    }

    public GcpParameterByteArray(Element e) {
        super(e);
    }

    @Override
    public int getLength() {
        return -1;
    }

    @Override
    public byte[] toValue(String input) {
        return ByteUtil.toBytesFromHexString(input);
    }

    @Override
    public byte[] toBytes(Object val) {
        return (byte[]) val;
    }

    @Override
    public Binary getIssued(String prefix, byte[] value) {
        return new Binary(null, prefix + getName(), getDescription(), toValue(value));
    }

    @Override
    public byte[] toValue(byte[] bytes) {
        return bytes;
    }

    @Override
    public Class<?> getValueClass() {
        return byte[].class;
    }

}
