package eu.estcube.codec.radiobeacon.parser;

import org.hbird.exchange.core.Parameter;

import eu.estcube.domain.JMSConstants;

public class RadionBeaconMessageParserBits extends RadioBeaconMessageParser {

    protected Integer startIndex;

    protected Integer endIndex;

    protected Parameter parameterBase;

    protected Integer bitMask;

    protected Integer bitShift;

    protected Integer result;

    public RadionBeaconMessageParserBits(Integer startIndex, Integer length, Integer bitMask, Integer bitShift,
            Parameter parameterBase) {
        this.startIndex = startIndex;
        this.endIndex = startIndex + length;
        this.bitMask = bitMask;
        this.bitShift = bitShift;
        this.parameterBase = parameterBase;
    }

    public String parse(String hex) {
        String part = getValidStringPart(hex, this.startIndex, this.endIndex);
        if (part == null) {
            result = null;
            return null;
        }
        result = (Integer.parseInt(cwToHex(part), JMSConstants.HEX_BASE) & bitMask) >> bitShift;
        return String.valueOf(result);
    }

    public Parameter parseToIssued(String message) {
        String part = parse(message);
        if (part == null) {
            return null;
        }
        Parameter parameter = new Parameter(parameterBase);
        parameter.setValue(Integer.parseInt(part));
        return parameter;
    }
}
