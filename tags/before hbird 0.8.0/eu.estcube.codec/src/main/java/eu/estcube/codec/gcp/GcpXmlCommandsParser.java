package eu.estcube.codec.gcp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hbird.business.core.CommandablePart;
import org.hbird.exchange.core.Command;
import org.hbird.exchange.core.CommandArgument;

import eu.estcube.codec.gcp.exceptions.SubsystemNotFoundException;
import eu.estcube.codec.gcp.struct.GcpCommand;
import eu.estcube.codec.gcp.struct.GcpParameter;
import eu.estcube.codec.gcp.struct.GcpStruct;
import eu.estcube.codec.gcp.struct.GcpSubsystem;
import eu.estcube.codec.gcp.struct.GcpSubsystemIdProvider;

public class GcpXmlCommandsParser {

    public List<CommandablePart> parse(GcpStruct struct) throws SubsystemNotFoundException {

        List<CommandablePart> list = new ArrayList<CommandablePart>();
        Map<String, CommandablePart> parts = new HashMap<String, CommandablePart>();

        for (GcpCommand gcpCommand : struct.getCommands()) {

            Command command = new Command(gcpCommand.getName(), gcpCommand.getDescription());
            List<CommandArgument> args = getDefaultArguments();

            // -------------------------------------
            // Parse arguments
            for (GcpParameter gcpParameter : gcpCommand.getParameters()) {
                CommandArgument argument = new CommandArgument(gcpParameter.getName(),
                        gcpParameter.getDescription(), gcpParameter.getValueClass(), true);
                args.add(argument);
            }
            command.setArgumentList(args);

            // -------------------------------------
            // Set for subsystems
            for (GcpSubsystem subsystem : gcpCommand.getSubsystems()) {

                CommandablePart part = parts.get(subsystem.getName());
                if (part == null) {
                    String subsysName = String.valueOf(GcpSubsystemIdProvider.getId(subsystem.getName()));
                    part = new CommandablePart(subsysName, subsystem.getName(), subsystem.getName());
                    list.add(part);
                    parts.put(part.getName(), part);
                }
                part.getCommands().add(command);
            }
        }

        return list;
    }

    private List<CommandArgument> getDefaultArguments() {
        List<CommandArgument> list = new ArrayList<CommandArgument>();
        list.add(new CommandArgument("source", "Source", Integer.class, false));
        list.add(new CommandArgument("destination", "Destination", Integer.class, true));
        list.add(new CommandArgument("priority", "Priority", Integer.class, true));
        list.add(new CommandArgument("commandId", "Command id", Integer.class, true));
        list.add(new CommandArgument("CDHSSource", "CDHS source", Integer.class, true));
        list.add(new CommandArgument("CDHSBlockIndex", "CDHS Block index", Integer.class, true));
        return list;
    }
}
