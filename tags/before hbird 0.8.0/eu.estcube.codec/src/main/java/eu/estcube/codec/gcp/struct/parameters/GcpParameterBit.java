package eu.estcube.codec.gcp.struct.parameters;

import org.hbird.exchange.core.Parameter;
import org.w3c.dom.Element;

import eu.estcube.codec.gcp.struct.GcpParameter;

public class GcpParameterBit extends GcpParameter {

    public GcpParameterBit(String name, String description, String unit) {
        super(name, description, unit);
    }

    public GcpParameterBit(Element e) {
        super(e);
    }

    @Override
    public int getLength() {
        return 1;
    }

    @Override
    public Boolean toValue(String input) {
        return input.equals("1") || input.equals("true") ? true : false;
    }

    @Override
    public byte[] toBytes(Object value) {
        byte[] bytes = { ((Boolean) value) == true ? (byte) 1 : (byte) 0 };
        return bytes;
    }

    @Override
    public Boolean toValue(byte[] bytes) {
        return (bytes[0] & 0x01) == 1;
    }

    @Override
    public Parameter getIssued(String prefix, byte[] value) {
        return new Parameter(null, prefix + getName(), getDescription(), toValue(value) == true ? 1 : 0, getUnit());
    }

    @Override
    public Class<?> getValueClass() {
        return Boolean.class;
    }

}
