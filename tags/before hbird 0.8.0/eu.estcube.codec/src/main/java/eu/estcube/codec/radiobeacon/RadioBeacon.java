package eu.estcube.codec.radiobeacon;

import org.hbird.exchange.core.Label;

public class RadioBeacon extends Label {

    public RadioBeacon(String issuedBy, String name, String description, String value) {
        super(issuedBy, name, description, value);
    }

    private static final long serialVersionUID = 7825434807994411460L;

}