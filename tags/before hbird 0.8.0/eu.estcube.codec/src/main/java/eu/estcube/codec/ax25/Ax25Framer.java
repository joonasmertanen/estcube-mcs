package eu.estcube.codec.ax25;

import eu.estcube.domain.transport.ax25.Ax25UIFrame;

/**
 * Creates AX.25 frames from the given data to be transferred. In most cases
 * those are TC packets that have been encoded.
 */
public interface Ax25Framer {

	/**
	 * Creates AX.25 frames from the given data.
	 * 
	 * @param data
	 *            That will be put into the info field.
	 * @return The AX.25 frames ready for encoding and tranfer.
	 */
	public Ax25UIFrame[] create(byte[] data);
}
