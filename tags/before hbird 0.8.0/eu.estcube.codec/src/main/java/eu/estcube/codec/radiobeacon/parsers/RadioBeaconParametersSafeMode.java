package eu.estcube.codec.radiobeacon.parsers;

import org.hbird.exchange.core.Label;
import org.hbird.exchange.core.Parameter;

import eu.estcube.codec.radiobeacon.RadioBeacon;

public class RadioBeaconParametersSafeMode {

    public static final String name = "ESTCube-1/beacon/";

    public static final RadioBeacon RADIO_BEACON = new RadioBeacon(null, name + "string", "Raw radiobeacon string",
            null);

    public static final Label OPERATING_MODE = new Label(null, name + "operating.mode",
            "Operating mode (E=normal or T=safe)", "");

    public static final Parameter TIMESTAMP = new Parameter(null, name + "timestamp", "Timestamp", 0, "sec");

    public static final Parameter ERROR_CODE_1 = new Parameter(null, name + "error.code.1", "Error code 1", 0, "");

    public static final Parameter ERROR_CODE_2 = new Parameter(null, name + "error.code.2", "Error code 2", 0, "");

    public static final Parameter ERROR_CODE_3 = new Parameter(null, name + "error.code.3", "Error code 3", 0, "");

    public static final Parameter TIME_IN_SAFE_MODE = new Parameter(null, name + "time.in.safe.mode",
            "Time in safe mode", 0,
            "min");

    public static final Parameter MAIN_BUS_VOLTAGE = new Parameter(null, name + "main.bus.voltage", "Main bus voltage",
            0, "V");

    public static final Parameter CDHS_PROCESSOR_A = new Parameter(null, name + "CDHS.processor.A", "0=OK, 1=FAULT", 0,
            "");

    public static final Parameter CDHS_PROCESSOR_B = new Parameter(null, name + "CDHS.processor.B", "0=OK, 1=FAULT", 0,
            "");

    public static final Parameter CDHS_BUS_SWITCH = new Parameter(null, name + "CDHS.bus.switch", "0=OK, 1=FAULT", 0,
            "");

    public static final Parameter COM_3V3_VOLTAGE_LINE = new Parameter(null, name + "COM.3v3.voltage.line",
            "0=OK, 1=FAULT",
            0, "");

    public static final Parameter COM_5V_VOLTAGE_LINE = new Parameter(null, name + "COM.5V.voltage.line",
            "0=OK, 1=FAULT", 0,
            "");

    public static final Parameter PL_3V3_VOLTAGE_LINE = new Parameter(null, name + "PL.3v3.voltage.line",
            "0=OK, 1=FAULT", 0,
            "");

    public static final Parameter PL_5V_VOLTAGE_LINE = new Parameter(null, name + "PL.5V.voltage.line",
            "0=OK, 1=FAULT", 0, "");

    public static final Parameter CAM_VOLTAGE = new Parameter(null, name + "CAM.voltage", "0=OK, 1=FAULT", 0, "");

    public static final Parameter ADCS_VOLTAGE = new Parameter(null, name + "ADCS.voltage", "0=OK, 1=FAULT", 0, "");

    public static final Parameter BATTERY_A_CHARGING = new Parameter(null, name + "battery.A.charging",
            "0=OK, 1=FAULT", 0, "");

    public static final Parameter BATTERY_A_DISCHARGING = new Parameter(null, name + "battery.A.discharging",
            "0=OK, 1=FAULT",
            0, "");

    public static final Parameter BATTERY_B_CHARGING = new Parameter(null, name + "battery.B.charging",
            "0=OK, 1=FAULT", 0, "");

    public static final Parameter BATTERY_B_DISCHARGING = new Parameter(null, name + "battery.B.discharging",
            "0=OK, 1=FAULT",
            0, "");

    public static final Parameter SECONDARY_POWER_BUS_A = new Parameter(null, name + "secondary.power.bus.A",
            "0=OK, 1=FAULT",
            0, "");

    public static final Parameter SECONDARY_POWER_BUS_B = new Parameter(null, name + "secondary.power.bus.B",
            "0=OK, 1=FAULT",
            0, "");

    public static final Parameter V12_LINE_VOLTAGE = new Parameter(null, name + "12V.line.voltage", "0=OK, 1=FAULT", 0,
            "");

    public static final Parameter REGULATOR_A_5V = new Parameter(null, name + "regulator.A.5V", "0=OK, 1=FAULT", 0, "");

    public static final Parameter REGULATOR_B_5V = new Parameter(null, name + "regulator.B.5V", "0=OK, 1=FAULT", 0, "");

    public static final Parameter LINE_VOLTAGE_5V = new Parameter(null, name + "line.voltage.5V", "0=OK, 1=FAULT", 0,
            "");

    public static final Parameter REGULATOR_A_3V3 = new Parameter(null, name + "regulator.A.3V3", "0=OK, 1=FAULT", 0,
            "");

    public static final Parameter REGULATOR_B_3V3 = new Parameter(null, name + "regulator.B.3V3", "0=OK, 1=FAULT", 0,
            "");

    public static final Parameter LINE_VOLTAGE_3V3 = new Parameter(null, name + "line.voltage.3V3", "0=OK, 1=FAULT", 0,
            "");

    public static final Parameter REGULATOR_A_12V = new Parameter(null, name + "regulator.A.12V", "0=OK, 1=FAULT", 0,
            "");

    public static final Parameter REGULATOR_B_12V = new Parameter(null, name + "regultator.B.12V", "0=OK, 1=FAULT", 0,
            "");

    public static final Parameter BATTERY_A_VOLTAGE = new Parameter(null, name + "battery.A.voltage",
            "battery.A.voltage", 0,
            "V");

    public static final Parameter BATTERY_B_VOLTAGE = new Parameter(null, name + "battery.B.voltage",
            "battery.B.voltage", 0,
            "V");

    public static final Parameter BATTERY_A_TEMPERATURE = new Parameter(null, name + "battery.A.temperature",
            "battery.A.temperature", 0, "C");

    public static final Parameter BATTERY_B_TEMPERATURE = new Parameter(null, name + "battery.B.temperature",
            "battery.B.temperature", 0, "C");

    public static final Parameter AVERAGE_POWER_BALANCE = new Parameter(null, name + "average.power.balance",
            "average.power.balance", 0, "W");

    public static final Parameter FIRMWARE_VERSION_NUMBER = new Parameter(null, name + "firmware.version.number",
            "Version number", 0, "");

    public static final Parameter NUMBER_OF_CRASHES = new Parameter(null, name + "number.of.crashes",
            "Number of crashes", 0,
            "");

    public static final Parameter FORWARDED_RF_POWER = new Parameter(null, name + "forwarded.RF.power",
            "forwarded.RF.power",
            0, "dBm");

    public static final Parameter REFLECTED_RF_POWER = new Parameter(null, name + "reflected.RF.power",
            "reflected.RF.power",
            0, "dBm");

    public static final Parameter RECEIVED_SIGNAL_STRENGTH = new Parameter(null, name + "received.signal.strength",
            "received.signal.strength", 0, "dBm");
}
