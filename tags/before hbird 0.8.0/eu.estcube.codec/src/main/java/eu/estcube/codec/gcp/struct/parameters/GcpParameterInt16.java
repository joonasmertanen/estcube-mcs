package eu.estcube.codec.gcp.struct.parameters;

import java.nio.ByteBuffer;

import org.apache.commons.lang3.ArrayUtils;
import org.w3c.dom.Element;

public class GcpParameterInt16 extends GcpParameterNumberic {

    public final static long BYTES = 2;

    public GcpParameterInt16(String name, String description, String unit, Boolean isLittleEndian) {
        super(name, description, unit, isLittleEndian);
    }

    public GcpParameterInt16(Element e) {
        super(e);
    }

    @Override
    public int getLength() {
        return 16;
    }

    @Override
    public Short toValue(String input) {
        if (isHexString(input)) {
            return (Short) toValueFromHex(input);
        }
        return Short.parseShort(input);
    }

    @Override
    public byte[] toBytes(Object value) {

        ByteBuffer buffer = ByteBuffer.allocate(2);
        buffer.putShort(toValue(String.valueOf(value)));
        byte[] bytes = { buffer.get(0), buffer.get(1) };

        if (getIsLittleEndian()) {
            ArrayUtils.reverse(bytes);
        }

        return bytes;
    }

    @Override
    public Short toValue(byte[] bytes) {

        if (bytes.length != BYTES) {
            throw new RuntimeException("byte[] length of the argument must be " + BYTES + "! Was " + bytes.length);
        }

        byte byte1 = bytes[0];
        byte byte2 = bytes[1];

        if (getIsLittleEndian()) {
            byte1 = bytes[1];
            byte2 = bytes[0];
        }

        return (short) ((byte1 & 0xFF) << 8 | (byte2 & 0xFF));
    }

    @Override
    public Class<?> getValueClass() {
        return Short.class;
    }

}
