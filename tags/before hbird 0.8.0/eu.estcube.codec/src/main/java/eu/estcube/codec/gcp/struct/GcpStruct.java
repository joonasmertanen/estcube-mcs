package eu.estcube.codec.gcp.struct;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

@Component
public class GcpStruct {

    private final List<GcpCommand> commands = new ArrayList<GcpCommand>();

    private final List<GcpReply> replies = new ArrayList<GcpReply>();

    public GcpStruct() {

    }

    @PostConstruct
    public void initFromXml() throws Exception {
        parseCommandsFromXml(loadResourceXml("commands.xml"));
        parseRepliesFromXml(loadResourceXml("replies.xml"));
    }

    public Document loadResourceXml(String file) throws Exception {
        InputStream fXmlFile = getClass().getResourceAsStream("/" + file);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fXmlFile);
        return doc;
    }

    public void parseCommandsFromXml(Document doc) {

        doc.getDocumentElement().normalize();

        NodeList nList = doc.getElementsByTagName("command");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            addCommand(new GcpCommand((Element) nList.item(temp)));
        }

    }

    public void parseRepliesFromXml(Document doc) {

        doc.getDocumentElement().normalize();

        NodeList nList = doc.getElementsByTagName("reply");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            addReply(new GcpReply((Element) nList.item(temp)));
        }

    }

    public GcpCommand getCommand(Integer id) {
        for (GcpCommand command : commands) {
            if (command.getId() == id) {
                return command;
            }
        }
        return null;
    }

    public GcpCommand getCommand(String name) {
        for (GcpCommand command : commands) {
            if (command.getName().equals(name)) {
                return command;
            }
        }
        return null;
    }

    public void addCommand(GcpCommand command) {
        commands.add(command);
    }

    public List<GcpCommand> getCommands() {
        return commands;
    }

    public GcpReply getReply(Integer id, String subsystem) {
        for (GcpReply reply : replies) {
            if (reply.getId() == id) {
                for (GcpSubsystem sub : reply.getSubsystems()) {
                    if (sub.getName().equals(subsystem)) {
                        return reply;
                    }
                }
            }
        }
        return null;
    }

    public void addReply(GcpReply reply) {
        replies.add(reply);
    }

    public List<GcpReply> getReplies() {
        return replies;
    }

}
