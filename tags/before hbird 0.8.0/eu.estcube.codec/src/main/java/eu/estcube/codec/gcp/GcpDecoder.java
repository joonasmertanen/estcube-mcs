package eu.estcube.codec.gcp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hbird.exchange.core.Issued;
import org.hbird.exchange.core.Metadata;

import eu.estcube.codec.gcp.exceptions.SubsystemNotFoundException;
import eu.estcube.codec.gcp.struct.GcpParameter;
import eu.estcube.codec.gcp.struct.GcpReply;
import eu.estcube.codec.gcp.struct.GcpStruct;
import eu.estcube.codec.gcp.struct.GcpSubsystemIdProvider;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterMaxLength;

public class GcpDecoder {

    /**
     * Telemetry parser
     * 
     * @throws SubsystemNotFoundException
     */
    public List<Issued> decode(byte[] bytes, GcpStruct struct, String namePrefix) throws SubsystemNotFoundException {

        List<Issued> list = new ArrayList<Issued>();

        int source = bytes[0] & 0xFF;
        int destination = bytes[1] & 0xFF;
        int length = ((bytes[2] & 0xFF) << 8) | (bytes[3] & 0xFF);
        int priority = (bytes[3] & 0xC0) >> 6;
        int id = ((bytes[4] & 0x3F) << 8) | (bytes[5] & 0xFF);
        int CDHSSource = (bytes[6] & 0xF0) >> 4;
        int CDHSBlockIndex = bytes[6] & 0xF;
        int argumentsLength = bytes[7] & 0xFF;

        GcpReply com = struct.getReply(id, GcpSubsystemIdProvider.getName(source));
        if (com == null) {
            throw new RuntimeException("[GcpDecoder] Reply with id:" + id + " subsystem:" + source + " not found!");
        }

        namePrefix = namePrefix.replace("{$subsystem}", GcpSubsystemIdProvider.getName(source).toLowerCase());

        int offset = 8 * 8;
        for (GcpParameter parameter : com.getParameters()) {

            int len = parameter.getLength();
            if (parameter instanceof GcpParameterMaxLength) {
                len = bytes.length * 8 - offset;
            }

            // ApiFactory.getPublishApi("{$service.name}").publish()
            // ApiFactory.getCatalogueApi("{$service.name}").getParameters().

            byte[] paramBytes = byteCopier(bytes, offset, len);
            offset += len;

            if (parameter.getIgnored()) {
                continue;
            }

            Issued issued = parameter.getIssued(namePrefix, paramBytes);
            list.add(issued);

            Map<String, Object> data = new HashMap<String, Object>();
            data.put("source", source);
            data.put("destination", destination);
            data.put("length", length);
            data.put("priority", priority);
            data.put("id", id);
            data.put("CDHSSource", CDHSSource);
            data.put("CDHSBlockIndex", CDHSBlockIndex);
            data.put("argumentsLength", argumentsLength);
            // @TODO Mis nimega Metadata peaks minema?
            Metadata meta = new Metadata(null, issued.getName() + ".meta", issued, data);
            list.add(meta);

        }
        return list;
    }

    public byte[] byteCopier(byte[] bytes, int offset, int length) {

        int resultLength = (int) Math.ceil((double) length / 8);
        int byteOffset = offset / 8;
        int byteLength = (int) Math.ceil((double) length / 8);

        byte[] paramBytes = new byte[resultLength];
        System.arraycopy(bytes, byteOffset, paramBytes, 0, byteLength);

        // If length is less than 1 byte, then we will remove all unused bits
        // and move used bit to the most right position.
        if (length < 8) {

            List<Byte> map = new ArrayList<Byte>();
            map.add((byte) 1);
            map.add((byte) 2);
            map.add((byte) 4);
            map.add((byte) 8);
            map.add((byte) 16);
            map.add((byte) 32);
            map.add((byte) 64);
            map.add((byte) 128);

            int b = 7 - offset % 8;
            paramBytes[0] = (byte) (((paramBytes[0] & map.get(b)) >> (b)) & 0x01);
        }

        return paramBytes;
    }
}
