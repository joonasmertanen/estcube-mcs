package eu.estcube.codec.gcp.struct.parameters;

import org.hbird.exchange.core.Parameter;
import org.w3c.dom.Element;

import eu.estcube.codec.gcp.struct.GcpParameter;
import eu.estcube.common.ByteUtil;

abstract public class GcpParameterNumberic extends GcpParameter {

    private boolean isLittleEndian = true;

    public GcpParameterNumberic(String name, String description, String unit, Boolean isLittleEndian) {
        super(name, description, unit);
        setIsLittleEndian(isLittleEndian);
    }

    public GcpParameterNumberic(Element e) {
        super(e);

        Element element = (Element) e.getElementsByTagName("type").item(0);
        if (element != null && element.getAttribute("little_endian") != null) {
            setIsLittleEndian(element.getAttribute("little_endian").equals(
                    "true"));
        }
    }

    public boolean isHexString(String input) {
        return input.length() > 2 && input.substring(0, 2).equals("0x");
    }

    public Number toValueFromHex(String input) {
        return toValue(ByteUtil.toBytesFromHexString(input.substring(2)));
    }

    @Override
    abstract public Number toValue(byte[] bytes);

    @Override
    public Parameter getIssued(String prefix, byte[] value) {
        return new Parameter(null, prefix + getName(), getDescription(), toValue(value), getUnit());
    }

    public void setIsLittleEndian(boolean isLittleEndian) {
        this.isLittleEndian = isLittleEndian;
    }

    public boolean getIsLittleEndian() {
        return isLittleEndian;
    }

}
