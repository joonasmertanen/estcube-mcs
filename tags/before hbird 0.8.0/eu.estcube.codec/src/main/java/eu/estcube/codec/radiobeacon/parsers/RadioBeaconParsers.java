package eu.estcube.codec.radiobeacon.parsers;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.hbird.exchange.core.Issued;

import eu.estcube.codec.radiobeacon.RadioBeacon;
import eu.estcube.codec.radiobeacon.parser.RadioBeaconMessageParser;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserTimestamp;

public abstract class RadioBeaconParsers {

    abstract public List<RadioBeaconMessageParser> getParsers();

    abstract public RadioBeacon getRadioBeacon();

    abstract public RadionBeaconMessageParserTimestamp getTimestampParser();

    public HashMap<String, Issued> parse(String message, Long timestamp, String issuedBy) {

        HashMap<String, Issued> list = new LinkedHashMap<String, Issued>();

        RadioBeacon beacon = getRadioBeacon();
        beacon.setValue(message);
        beacon.setIssuedBy(issuedBy);
        beacon.setTimestamp(timestamp);
        list.put(beacon.getName(), beacon);

        for (RadioBeaconMessageParser parser : getParsers()) {
            Issued issued = parser.parseToIssued(message);
            if (issued != null) {
                issued.setIssuedBy(issuedBy);
                issued.setTimestamp(timestamp);

                list.put(issued.getName(), issued);
            }
        }

        return list;
    }

}
