package eu.estcube.codec.gcp.struct;

import org.hbird.exchange.core.Issued;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import eu.estcube.codec.gcp.struct.parameters.GcpParameterBit;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterByteArray;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterDouble;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterFloat;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterInt16;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterInt32;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterInt8;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterString;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterUint16;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterUint32;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterUint8;

abstract public class GcpParameter {

    private String name;

    private String description = "";

    private String unit = "";

    private boolean ignored = false;

    public GcpParameter(String name, String description, String unit) {
        setName(name);
        setDescription(description);
        setUnit(unit);
    }

    public GcpParameter(Element node) {
        Node n = node.getElementsByTagName("name").item(0);
        if (n == null || n.getTextContent() == "") {
            throw new RuntimeException("Parameter must define name!");
        }
        setName(n.getTextContent());

        n = node.getElementsByTagName("description").item(0);
        if (n != null) {
            setDescription(n.getTextContent());
        }

        n = node.getElementsByTagName("unit").item(0);
        if (n != null) {
            setUnit(n.getTextContent());
        }

        n = node.getElementsByTagName("ignore").item(0);
        if (n != null) {
            String val = n.getTextContent();
            setIgnored(val.equals("1") || val.equals("true"));
        }
    }

    abstract public int getLength();

    abstract public Class<?> getValueClass();

    abstract public Object toValue(String input); // for encoding

    abstract public byte[] toBytes(Object value); // for encoding

    abstract public Object toValue(byte[] bytes); // for decoding

    abstract public Issued getIssued(String prefix, byte[] value);

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public static GcpParameter getParameterObjectFromType(Element element) {

        String type = element.getElementsByTagName("type").item(0).getTextContent();
        if (type.equals("int8")) {
            return new GcpParameterInt8(element);

        } else if (type.equals("uint8")) {
            return new GcpParameterUint8(element);

        } else if (type.equals("int16")) {
            return new GcpParameterInt16(element);

        } else if (type.equals("uint16")) {
            return new GcpParameterUint16(element);

        } else if (type.equals("int32")) {
            return new GcpParameterInt32(element);

        } else if (type.equals("uint32")) {
            return new GcpParameterUint32(element);

        } else if (type.equals("bytearray")) {
            return new GcpParameterByteArray(element);

        } else if (type.equals("double")) {
            return new GcpParameterDouble(element);

        } else if (type.equals("float")) {
            return new GcpParameterFloat(element);

        } else if (type.equals("string")) {
            return new GcpParameterString(element);

        } else if (type.equals("bit")) {
            return new GcpParameterBit(element);

        }

        throw new RuntimeException("Parameter " + type + " does not exist!");
    }

    public void setIgnored(boolean ignored) {
        this.ignored = ignored;
    }

    public Boolean getIgnored() {
        return ignored;
    }
}
