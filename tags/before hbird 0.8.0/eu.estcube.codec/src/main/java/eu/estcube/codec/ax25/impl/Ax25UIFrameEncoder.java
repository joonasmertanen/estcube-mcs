package eu.estcube.codec.ax25.impl;

import java.nio.ByteBuffer;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.common.ByteUtil;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;

/**
 * Encodes the AX.25 UI frame exactly according to the standarda and ready for
 * transmission.
 */
public final class Ax25UIFrameEncoder implements ProtocolEncoder {
    private static final Logger LOG = LoggerFactory.getLogger(Ax25UIFrameEncoder.class);

    // buffer size: account for stuffed bits, flags, filler byte at the end
    // every 5th bit stuffed: 55 = Ax25Frame.FRAME_CONTENTS_MAX_SIZE * 8 /
    // (5 * 8)
    private static final int BUFFER_SIZE = Ax25UIFrame.FRAME_CONTENTS_MAX_SIZE + 55 + 2 + 1;

    protected int bitBuffer;
    protected int numBits;
    protected int numSequentialOnes;

    public Ax25UIFrameEncoder() {
    }

    @Override
    public void encode(IoSession ioSession, Object message, ProtocolEncoderOutput out) throws Exception {
        Ax25UIFrame frame = (Ax25UIFrame) message;
        ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);

        encode(frame, buffer);

        buffer.flip();
        out.write(buffer);
        out.flush();
    }

    @Override
    public void dispose(IoSession ioSession) throws Exception {

    }

    protected void flush(ByteBuffer buffer) {
        // align the remaining bits with byte boundary
        if (numBits > 0) {
            bitBuffer <<= (8 - numBits);
            addByteToBuffer(bitBuffer, buffer);
        }
    }

    protected void reset() {
        bitBuffer = 0;
        numBits = 0;
        numSequentialOnes = 0;
    }

    /**
     * Encodes the given frame into the given buffer.
     * 
     * @param frame
     *        The frame to encode.
     * @param buffer
     *        The buffer where to encode (append).
     */
    public void encode(Ax25UIFrame frame, ByteBuffer buffer) {
        LOG.trace("Start encoding " + frame.toString());
        reset();

        encode(Ax25UIFrame.FLAG, true, false, buffer);
        encode(frame.getDestAddr(), true, true, buffer);
        encode(frame.getSrcAddr(), true, true, buffer);
        encode(frame.getCtrl(), true, true, buffer);
        encode(frame.getPid(), true, true, buffer);
        encode(frame.getInfo(), true, true, buffer);

        // According to AX.25 unlike other fields FCS goes MSB first!
        encode(frame.getFcs(), false, true, buffer);

        encode(Ax25UIFrame.FLAG, true, false, buffer);

        flush(buffer);
    }

    protected void encode(byte[] bb, boolean lsbFirst, boolean stuff, ByteBuffer buffer) {
        if (bb == null) {
            return;
        }

        for (byte b : bb) {
            encode(b, lsbFirst, stuff, buffer);
        }
    }

    protected void encode(byte b, boolean lsbFirst, boolean stuff, ByteBuffer buffer) {
        for (int bit : ByteUtil.toBits(b, lsbFirst)) {
            txBit(bit, stuff, buffer);
        }
    }

    protected void txBit(int bit, boolean stuff, ByteBuffer buffer) {
        numBits++;

        bitBuffer <<= 1;
        if (bit > 0) {
            bitBuffer |= 1;
        }

        if (numBits > 7) {
            addByteToBuffer(bitBuffer, buffer);
            numBits = 0;
        }

        numSequentialOnes = (bit > 0 && stuff) ? numSequentialOnes + 1 : 0;
        if (stuff && numSequentialOnes > 4) {
            txBit(0, stuff, buffer); // stuff
        }
    }

    protected void addByteToBuffer(int bitBuffer, ByteBuffer buffer) {
        buffer.put((byte) (bitBuffer & 0xFF));
    }
}
