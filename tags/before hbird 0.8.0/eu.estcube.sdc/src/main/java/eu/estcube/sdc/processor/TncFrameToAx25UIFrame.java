/** 
 *
 */
package eu.estcube.sdc.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.hbird.exchange.constants.StandardArguments;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.codec.ax25.impl.Ax25UIFrameKissDecoder;
import eu.estcube.common.Headers;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.domain.transport.tnc.TncFrame;

/**
 *
 */
@Component
public class TncFrameToAx25UIFrame implements Processor {

    public static final String TYPE = "AX.25";

    private static final Logger LOG = LoggerFactory.getLogger(TncFrameToAx25UIFrame.class);

    @Autowired
    private Ax25UIFrameKissDecoder decoder;

    /** @{inheritDoc . */
    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        out.copyFrom(in);
        try {
            TncFrame tncFrame = in.getBody(TncFrame.class);
            if (tncFrame == null) {
                throw new IllegalArgumentException("No TncFrame available in message body");
            }
            Ax25UIFrame ax25UIFrame = decoder.decode(tncFrame);
            out.setBody(ax25UIFrame);
            out.setHeader(StandardArguments.CLASS, ax25UIFrame.getClass().getSimpleName());
            out.setHeader(StandardArguments.TYPE, TYPE);
            out.setHeader(Headers.TNC_PORT, tncFrame.getTarget());
        } catch (Exception e) {
            LOG.error("Failed to process TncFrame to Ax25UIFrame; {}", e.getMessage());
            exchange.setException(e);
        }
    }
}
