/** 
 *
 */
package eu.estcube.sdc.gcp;

import org.apache.camel.Body;
import org.apache.camel.Handler;
import org.apache.commons.lang3.Validate;
import org.hbird.exchange.core.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.codec.gcp.GcpEncoder;
import eu.estcube.codec.gcp.struct.GcpStruct;
import eu.estcube.common.ByteUtil;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;

/**
 *
 */
@Component
public class CommandToAx25UiFrameProcessor {

    @Autowired
    private GcpStruct struct;

    @Handler
    public Ax25UIFrame decode(@Body Command command) throws Exception {

        Ax25UIFrame frame = new Ax25UIFrame();

        byte[] destAddr = ByteUtil.toBytesFromHexString("8A A6 6A 8A 40 40 76");
        Validate.isTrue(destAddr.length == Ax25UIFrame.DEST_ADDR_LEN, "destAddr length has to be %s bytes",
                Ax25UIFrame.DEST_ADDR_LEN);
        frame.setDestAddr(destAddr);

        byte[] srcAddr = ByteUtil.toBytesFromHexString("8A A6 6A 8A 86 40 62");
        Validate.isTrue(srcAddr.length == Ax25UIFrame.SRC_ADDR_LEN, "srcAddr length has to be %s bytes",
                Ax25UIFrame.SRC_ADDR_LEN);
        frame.setSrcAddr(srcAddr);

        byte[] ctrl = ByteUtil.toBytesFromHexString("03");
        Validate.isTrue(ctrl.length == 1, "ctrl has to be single byte");
        frame.setCtrl(ctrl[0]);

        byte[] pid = ByteUtil.toBytesFromHexString("F0");
        Validate.isTrue(pid.length == 1, "pid has to be single byte");
        frame.setPid(pid[0]);

        // Two random bytes required
        byte[] randomBytes = { (byte) (Math.random() * 100), (byte) (Math.random() * 100) };
        byte[] info = new GcpEncoder().encode(command, struct, randomBytes);

        Validate.isTrue(info.length <= Ax25UIFrame.INFO_MAX_SIZE, "info max length is %s", Ax25UIFrame.INFO_MAX_SIZE);
        frame.setInfo(info);

        return frame;
    }
}
