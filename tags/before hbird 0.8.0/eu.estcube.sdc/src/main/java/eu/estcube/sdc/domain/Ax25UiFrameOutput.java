/** 
 *
 */
package eu.estcube.sdc.domain;

/**
 *
 */
public class Ax25UiFrameOutput extends Ax25UiFrameInput {

    private String timestamp;
    private String serialPortName;
    private String serviceId;
    
    public Ax25UiFrameOutput() {
        super();
    }

    /**
     * Returns timestamp.
     *
     * @return the timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * Sets timestamp.
     *
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Returns serialPortName.
     *
     * @return the serialPortName
     */
    public String getSerialPortName() {
        return serialPortName;
    }

    /**
     * Sets serialPortName.
     *
     * @param serialPortName the serialPortName to set
     */
    public void setSerialPortName(String serialPortName) {
        this.serialPortName = serialPortName;
    }

    /**
     * Returns serviceId.
     *
     * @return the serviceId
     */
    public String getServiceId() {
        return serviceId;
    }

    /**
     * Sets serviceId.
     *
     * @param serviceId the serviceId to set
     */
    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
}
