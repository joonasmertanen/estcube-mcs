package eu.estcube.sdc.gcp;

import java.util.List;

import org.apache.camel.Body;
import org.apache.camel.Handler;
import org.hbird.exchange.core.Issued;
import org.hbird.exchange.core.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.codec.gcp.GcpDecoder;
import eu.estcube.codec.gcp.struct.GcpStruct;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;

@Component
public class Ax25UiFrameToIssuedProcessor {

    @Autowired
    private GcpStruct struct;

    private static final Logger LOG = LoggerFactory.getLogger(Ax25UiFrameToIssuedProcessor.class);

    @Handler
    public List<Issued> decode(@Body Ax25UIFrame frame) throws Exception {
        GcpDecoder decoder = new GcpDecoder();
        List<Issued> list = decoder.decode(frame.getInfo(), struct, "estcube.{$subsystem}.");

        LOG.info("Received Ax25UiFrame --------------------");

        int i = 0;
        for (Issued issued : list) {
            String value = "";
            if (issued instanceof Parameter) {
                value += String.valueOf(((Parameter) issued).getValue());
            }

            LOG.info("" + i + " " + issued.getClass().getName() + " "
                    + issued.getName()
                    + ": " + value);
            i++;
        }
        LOG.info("Created list with " + list.size() + " elements.------------");
        return list;
    }
}
