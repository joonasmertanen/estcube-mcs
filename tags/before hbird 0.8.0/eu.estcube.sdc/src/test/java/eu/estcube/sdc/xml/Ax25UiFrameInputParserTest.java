/** 
 *
 */
package eu.estcube.sdc.xml;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.thoughtworks.xstream.converters.ConversionException;

import eu.estcube.sdc.domain.Ax25UiFrameInput;

/**
 * 
 */
public class Ax25UiFrameInputParserTest {

    private File input;
    
    private Ax25UiFrameInputParser parser;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        parser = new Ax25UiFrameInputParser();
    }

    /**
     * Test method for {@link eu.estcube.sdc.xml.Ax25UiFrameInputParser#parse(java.io.File)}.
     */
    @Test
    public void testParseOne() throws Exception {
        input = new File(getClass().getResource("/uplink-01.xml").toURI());
        List<Ax25UiFrameInput> list = parser.parse(input);
        assertNotNull(list);
        assertEquals(1, list.size());
        assertEquals("00 00 00 00 00 00 01", list.get(0).getDestAddr());
        assertEquals("00 00 00 00 00 00 02", list.get(0).getSrcAddr());
        assertEquals("03", list.get(0).getCtrl());
        assertEquals("0F", list.get(0).getPid());
        assertEquals("09", list.get(0).getPrefix());
        assertEquals("00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F", list.get(0).getInfo());
        assertEquals(3, list.get(0).getTncTargetPort());
    }
    
    /**
     * Test method for {@link eu.estcube.sdc.xml.Ax25UiFrameInputParser#parse(java.io.File)}.
     */
    @Test
    public void testParseSeveral() throws Exception {
        input = new File(getClass().getResource("/uplink-02.xml").toURI());
        List<Ax25UiFrameInput> list = parser.parse(input);
        assertNotNull(list);
        assertEquals(2, list.size());
        assertEquals("00 00 00 00 00 00 01", list.get(0).getDestAddr());
        assertEquals("00 00 00 00 00 00 02", list.get(0).getSrcAddr());
        assertEquals("03", list.get(0).getCtrl());
        assertEquals("0F", list.get(0).getPid());
        assertEquals("09", list.get(0).getPrefix());
        assertEquals("00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F", list.get(0).getInfo());
        assertEquals(3, list.get(0).getTncTargetPort());
        
        assertEquals("00 00 00 00 00 00 03", list.get(1).getDestAddr());
        assertEquals("00 00 00 00 00 00 04", list.get(1).getSrcAddr());
        assertEquals("33", list.get(1).getCtrl());
        assertEquals("FF", list.get(1).getPid());
        assertNull(list.get(1).getPrefix());
        assertEquals("C0 FE BA BE", list.get(1).getInfo());
        assertEquals(5, list.get(1).getTncTargetPort());
    }
    
    /**
     * Test method for {@link eu.estcube.sdc.xml.Ax25UiFrameInputParser#parse(java.io.File)}.
     */
    @Test(expected = ConversionException.class)
    public void testParseWithException() throws Exception {
        input = new File(getClass().getResource("/uplink-03.xml").toURI());
        parser.parse(input);
    }
}
