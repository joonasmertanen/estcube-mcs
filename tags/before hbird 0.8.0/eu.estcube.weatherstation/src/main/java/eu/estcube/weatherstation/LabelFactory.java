/** 
 *
 */
package eu.estcube.weatherstation;

import org.hbird.exchange.core.Label;

/**
 * 
 */
public class LabelFactory implements NamedFactory<Label> {

    /** @{inheritDoc}. */
    public Label createNew(Label base, String value) {
        Label label = new Label(base);
        label.setValue(value);
        return label;
    }
}
