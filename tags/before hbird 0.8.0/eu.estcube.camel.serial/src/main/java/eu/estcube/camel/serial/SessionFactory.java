package eu.estcube.camel.serial;

import java.net.SocketAddress;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.mina.core.filterchain.DefaultIoFilterChainBuilder;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.transport.serial.SerialAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class SessionFactory {

    private static final Logger LOG = LoggerFactory.getLogger(SessionFactory.class);

    private final SerialConfig config;

    private final ExecutorService executorService;

    private final IoConnector connector;

    private final AtomicBoolean isClosing = new AtomicBoolean(false);

    public SessionFactory(SerialConfig config, ExecutorService executorService, IoConnector connector) {
        this.config = config;
        this.executorService = executorService;
        this.connector = connector;
    }

    IoSession openSession(SerialAddress address, SerialHandler ioHandler) throws InterruptedException {
        connector.setHandler(ioHandler);
        LOG.trace("Adding filters ...");
        addIoFilters(connector, config.getFilters());
        LOG.debug("Connecting to {}", address);
        ConnectFuture connect = connector.connect(address);
        connect.await();
        LOG.debug("Connected to {}", address);
        IoSession session = connect.getSession();
        session.setAttribute(SerialEndpoint.ATTRIBUTE_SERIAL_ADDRESS, address);
        return session;
    }

    void addIoFilters(IoConnector connector, List<IoFilter> filters) {
        if (filters != null) {
            DefaultIoFilterChainBuilder filterChain = connector.getFilterChain();
            for (int i = 0; i < filters.size(); i++) {
                IoFilter filter = filters.get(i);
                String name = String.format("#%s-%s", (i + 1), filter.toString());
                LOG.trace(" Filter {}", name);
                filterChain.addFirst(name, filter);
            }
        }
    }

    void close() {
        if (!isClosing.get()) {
            isClosing.set(true);
            executorService.execute(createConnectorDisposingRunnable(connector));
        }
    }

    Runnable createConnectorDisposingRunnable(final IoConnector connector) {
        return new Runnable() {
            @Override
            public void run() {
                SocketAddress address = connector.getDefaultRemoteAddress();
                LOG.debug("Disposing connector {}", address);
                connector.dispose();
                LOG.info("Disposed connector {}", address);
            }
        };
    }
}
