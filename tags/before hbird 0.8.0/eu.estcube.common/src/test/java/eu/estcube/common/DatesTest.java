package eu.estcube.common;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 */
public class DatesTest {

    private static final String DATE_AS_STRING = "20130319230948211";

    private static Date date;

    @BeforeClass
    public static void setupBeforeClass() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        date = sdf.parse(DATE_AS_STRING);
    }

    /**
     * Test method for {@link eu.estcube.common.Dates#toDefaultDateFormat(long)}
     * .
     */
    @Test
    public void testToDefaultDateFormat() {
        assertEquals("2013-078 23:09:48.211", Dates.toDefaultDateFormat(date.getTime()));
    }

    /**
     * Test method for {@link eu.estcube.common.Dates#toIso8601DateFormat(long)}
     * .
     */
    @Test
    public void testToIso8601DateFormat() {
        assertEquals("2013-03-19T23:09:48.211Z", Dates.toIso8601DateFormat(date.getTime()));
    }

    /**
     * Test method for {@link eu.estcube.common.Dates#toIso8601DateFormat(long)}
     * .
     */
    @Test
    public void testToIso8601BasicDateFormat() {
        assertEquals("20130319T230948.211Z", Dates.toIso8601BasicDateFormat(date.getTime()));
    }

    /**
     * Test method for
     * {@link eu.estcube.common.Dates#toDateInFileNameFormat(long)} .
     */
    @Test
    public void testToDateInFileNameFormat() {
        assertEquals("2013-03-19T230948-211", Dates.toDateInFileNameFormat(date.getTime()));
    }
}
