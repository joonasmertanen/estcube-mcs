package eu.estcube.common.json;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.inOrder;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.hbird.business.core.StartablePart;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.core.BusinessCard;
import org.hbird.exchange.core.Command;
import org.hbird.exchange.core.CommandArgument;
import org.hbird.exchange.core.Parameter;
import org.hbird.exchange.core.Part;
import org.hbird.exchange.interfaces.INamed;
import org.hbird.exchange.interfaces.IStartablePart;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.GsonBuilder;

import eu.estcube.common.Dates;
import eu.estcube.common.json.ToJsonProcessor;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ToJsonProcessorTest {

    private static final long NOW = System.currentTimeMillis();

    private ToJsonProcessor toJson;

    @Mock
    private Object object;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        toJson = new ToJsonProcessor();
        inOrder = inOrder(object);
    }

    /**
     * Test method for
     * {@link eu.estcube.common.json.ToJsonProcessor#process(java.lang.Object)}
     * .
     */
    @Test
    public void testProcess() {
        String result = toJson.process(object);
        assertNotNull(result);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.common.json.ToJsonProcessor#createBuilder()}.
     */
    @Test
    public void testCreateBuilder() {
        GsonBuilder gson = ToJsonProcessor.createBuilder();
        assertNotNull(gson);
        assertEquals("\"string\"", gson.create().toJson("string"));
        assertEquals(String.valueOf(NOW), gson.create().toJson(new Long(NOW)));
        assertEquals(String.valueOf(NOW), gson.create().toJson(NOW));
        assertEquals("0.0", gson.create().toJson(0.0D));
        assertEquals("-0.0", gson.create().toJson(-0.0D));
        assertEquals("0.01", gson.create().toJson(0.01D));
        assertEquals("NaN", gson.create().toJson(Double.NaN));
        assertEquals("-Infinity", gson.create().toJson(Double.NEGATIVE_INFINITY));
        assertEquals("Infinity", gson.create().toJson(Double.POSITIVE_INFINITY));
    }

    @Test
    public void testSerializeLoggingEvent() {
        GsonBuilder gson = ToJsonProcessor.createBuilder();
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(StandardArguments.ISSUED_BY, "test");
        LoggingEvent le = new LoggingEvent(null, Logger.getLogger("LOGGER"), NOW, Level.DEBUG, "message", "thread",
                null, null, null, properties);
        String expected = "{\"level\":\"DEBUG\",\"timestamp\":"
                + NOW
                + ",\"value\":\"message\",\"logger\":\"LOGGER\",\"issuedBy\":\"test\",\"thread\":\"thread\",\"throwable\":[]}";
        assertEquals(expected, gson.create().toJson(le));
    }

    @Test
    public void testSerializeINamed() {
        MyNamed named = new MyNamed();
        named.setName("INAMED-1");
        GsonBuilder gson = ToJsonProcessor.createBuilder();
        String expected = "{\"class\":\"MyNamed\",\"name\":\"INAMED-1\"}";
        assertEquals(expected, gson.create().toJson(named));
    }

    @Test
    public void testSerilazeINamedWithClass() {
        MyNamedWithClass named = new MyNamedWithClass();
        named.setName("INAMED-1");
        named.setClazz(Parameter.class);
        GsonBuilder gson = ToJsonProcessor.createBuilder();
        String expected = "{\"class\":\"MyNamedWithClass\",\"clazz\":\"org.hbird.exchange.core.Parameter\",\"name\":\"INAMED-1\"}";
        assertEquals(expected, gson.create().toJson(named));
    }

    @Test
    public void testSerializeNamed() {
        GsonBuilder gson = ToJsonProcessor.createBuilder();
        Parameter param = new Parameter("issuer", "PARAM1", "Parameter description", "unknown");
        param.setTimestamp(NOW);
        param.setValue(8);
        String expected = "{\"class\":\"Parameter\",\"value\":8,\"unit\":\"unknown\",\"issuedBy\":\"issuer\",\"timestamp\":"
                + NOW + ",\"name\":\"PARAM1\",\"description\":\"Parameter description\"}";
        assertEquals(expected, gson.create().toJson(param));
    }

    @Test
    public void testSerializeCommand() {
        Command cmd = new Noop();
        MyNamed named = new MyNamed();
        named.setName("MYNAMED");

        IStartablePart part = new StartablePart("ID", "dummy", "just4test", "none");

        cmd.setArgumentValue("First", named);
        cmd.setArgumentValue("Second", part);
        GsonBuilder gson = ToJsonProcessor.createBuilder();
        String result = gson.create().toJson(cmd);
        // the result contains variables like host name and timestamp
        // no easy way to compare - it's too long (~2700 chars) and I'm too lazy
        // to build regex for it
        assertNotNull(result);
    }

    @Test
    public void testSerializeBusinessCard() {
        BusinessCard card = new BusinessCard("Test", 5000L);
        GsonBuilder gson = ToJsonProcessor.createBuilder();
        String expected = "\\{\"class\":\"BusinessCard\",\"host\":\".*\",\"period\":5000,\"commandsIn\":\\{\\},\"commandsOut\":\\{\\},\"eventsIn\":\\{\\},\"eventsOut\":\\{\\},\"dataIn\":\\{\\},\"dataOut\":\\{\\},\"issuedBy\":\"Test\",\"timestamp\":.*,\"name\":\"Test\",\"description\":\"A businesscard from a startable component.\"\\}";
        String result = gson.create().toJson(card);
        assertTrue(result.matches(expected));
    }

    @Test
    public void testPartWithNoParent() {
        Part part = new Part("Part", "A");
        GsonBuilder gson = ToJsonProcessor.createBuilder();
        String expected = "{\"class\":\"Part\",\"ID\":\"Part\",\"name\":\"Part\",\"description\":\"A\"}";
        assertEquals(expected, gson.create().toJson(part));
    }

    @Test
    public void testPartWithParent() {
        Part root = new Part("A", "Root");
        Part parent = new Part("B", "Parent");
        Part child = new Part("C", "Node");
        child.setParent(parent);
        parent.setParent(root);
        GsonBuilder gson = ToJsonProcessor.createBuilder();
        String expected = "{\"class\":\"Part\",\"parent\":\"/A/B\",\"ID\":\"C\",\"name\":\"C\",\"description\":\"Node\"}";
        assertEquals(expected, gson.create().toJson(child));

        expected = "{\"class\":\"Part\",\"parent\":\"/A\",\"ID\":\"B\",\"name\":\"B\",\"description\":\"Parent\"}";
        assertEquals(expected, gson.create().toJson(parent));

        expected = "{\"class\":\"Part\",\"ID\":\"A\",\"name\":\"A\",\"description\":\"Root\"}";
        assertEquals(expected, gson.create().toJson(root));
    }

    @Test
    public void testSerializeBinary() {
        byte[] data = new byte[] { 0x0C, 0x00, 0x0F, 0x0F, 0x0E, 0x0B, 0x0A, 0x0B, 0x0E };
        GsonBuilder gson = ToJsonProcessor.createBuilder();
        assertEquals("\"DAAPDw4LCgsO\\r\\n\"", gson.create().toJson(data));
    }

    @Test
    public void testSerializeDate() {
        GsonBuilder gson = ToJsonProcessor.createBuilder();
        assertEquals("\"" + Dates.toDefaultDateFormat(NOW) + "\"", gson.create().toJson(new Date(NOW)));
    }

    static class MyNamed implements INamed {

        private String name;

        @Override
        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String getQualifiedName(String separator) {
            return getName();
        }

        @Override
        public String getQualifiedName() {
            return getName();
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getID() {
            return getName();
        }

        @Override
        public String getDescription() {
            return getName();
        }
    };

    static class MyNamedWithClass extends MyNamed {
        private Class<?> clazz;

        public void setClazz(Class<?> clazz) {
            this.clazz = clazz;
        }

        public Class<?> getClazz() {
            return clazz;
        }
    }

    static class Noop extends Command {
        private static final long serialVersionUID = -52135596395518233L;

        public Noop() {
            super("NOOP", "NO OP command");
        }

        /** @{inheritDoc . */
        @Override
        protected List<CommandArgument> getArgumentDefinitions(List<CommandArgument> args) {
            args.add(new CommandArgument("First", "First Argument", INamed.class, Boolean.TRUE));
            args.add(new CommandArgument("Second", "Second Argument", IStartablePart.class, Boolean.TRUE));
            return args;
        }
    };
}
