package eu.estcube.commanding.rotator2;

import java.util.List;

import org.hbird.exchange.navigation.PointingData;

import eu.estcube.commanding.api.OptimizeInterface;
import eu.estcube.commanding.optimizators.GlobalFunctions;
import eu.estcube.commanding.rotator1.Rotator1CrossElLow;

public class Rotator2CrossElLow implements OptimizeInterface {
	double receivingSector;
	double maxAz;
	double maxEl;


	public Rotator2CrossElLow(double receivingSector, double maxAz, double maxEl) {
		super();
		this.receivingSector = receivingSector;
		this.maxAz = maxAz;
		this.maxEl = maxEl;
	}

	@Override
	public List<PointingData> optimize(List<PointingData> coordinates) {
		boolean suitable = false;
		boolean clockwise = false;
		int crossPoint = 0;
		crossPoint = GlobalFunctions.getCrossPoint(crossPoint, coordinates);
		clockwise = GlobalFunctions.ifClockwise(clockwise, coordinates,
				crossPoint);
		suitable = withinMaxAzRange(suitable, clockwise, coordinates);
		coordinates = caseSuitable(suitable, clockwise, coordinates, crossPoint);

		return coordinates;
	}

	public List<PointingData> crossElLowOptimizeClockwise(
			List<PointingData> coordinates, int crossPoint) {
		double addValue;
		for (int i = crossPoint; i < coordinates.size(); i++) {
			addValue = (coordinates.get(i).getAzimuth() + 360);

			coordinates.get(i).setAzimuth(addValue);
		}

		return coordinates;
	}

	public List<PointingData> crossElLowOptimizeAntiClockwise(
			List<PointingData> coordinates, int crossPoint) {
		double addValue;
		for (int i = 0; i < crossPoint; i++) {
			addValue = (coordinates.get(i).getAzimuth() + 360);
			coordinates.get(i).setAzimuth( addValue);
		}

		return coordinates;
	}

	public List<PointingData> caseSuitable(boolean suitable, boolean clockwise,
			List<PointingData> coordinates, int crossPoint) {

		if (suitable == true) {
			if (clockwise == true) {
				coordinates = crossElLowOptimizeClockwise(coordinates,
						crossPoint);
			}
			if (clockwise == false) {
				coordinates= crossElLowOptimizeAntiClockwise(coordinates,
						crossPoint);

			}
		}
		if (suitable == false) {
			Rotator1CrossElLow caseToApply = new Rotator1CrossElLow(
					receivingSector, maxAz, maxEl);
			coordinates = caseToApply.optimize(coordinates);
			System.out.println("APPLYING CASE AS FOR ROTATOR 1");

		}

		return coordinates;
	}

	public boolean withinMaxAzRange(boolean suitable, boolean clockwise,
			List<PointingData> coordinates) {
		if (clockwise == true) {
			if (coordinates.get(coordinates.size() - 1).getAzimuth() < 90) {
				suitable = true;

			} else {
				suitable = false;
			}

		}
		if (clockwise == false) {
			if (coordinates.get(0).getAzimuth() < 90) {
				suitable = true;

			} else {
				suitable = false;
			}
		}

		return suitable;
	}

}