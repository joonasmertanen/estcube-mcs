package eu.estcube.commanding.rotator1;

import java.util.ArrayList;
import java.util.List;

import org.hbird.exchange.navigation.PointingData;

import eu.estcube.commanding.api.OptimizeInterface;
import eu.estcube.commanding.optimizators.GlobalFunctions;

public class Rotator1CrossElHigh implements OptimizeInterface {
	double receivingSector;
	double maxAz;
	double maxEl;
	List<Double> azimuthList;
	List<Double> elevationList;

	public Rotator1CrossElHigh(double receivingSector, double maxAz,
			double maxEl) {
		super();
		this.receivingSector = receivingSector;
		this.maxAz = maxAz;
		this.maxEl = maxEl;
	}

	@Override
	public List<PointingData> optimize(List<PointingData> coordinates) {
		coordinates = crossElHigh(coordinates);
		return coordinates;
	}

	public List<PointingData> crossElHigh(List<PointingData> coordinates) {
		double azLastValue = GlobalFunctions.changeAzimuthTo180(coordinates
				.get(0).getAzimuth().intValue());
		double elLastValue = coordinates.get(0).getElevation().intValue();
		int coordinatesListSize = coordinates.size();
		final int azLastPointSector = GlobalFunctions
				.getDegreeSector(coordinates.get(coordinatesListSize - 1)
						.getAzimuth().intValue());
		final int azFirstPointSector = GlobalFunctions
				.getDegreeSector(coordinates.get(0).getAzimuth().intValue());
		List<PointingData> coordinatesTemp = new ArrayList<PointingData>();
		for (int i = 0; i < coordinatesListSize; i++) {
			long timestamp = coordinates.get(i).getTimestamp();
			double doppler = coordinates.get(i).getDoppler();
			double dopplerShift = coordinates.get(i).getDopplerShift();
			String satelliteName = coordinates.get(i).getSatelliteName();
			String groundStationName = coordinates.get(i)
					.getGroundStationName();

			double currentValueSector = GlobalFunctions
					.getDegreeSector(coordinates.get(i).getAzimuth().intValue());
			double azCurrentValue = coordinates.get(i).getAzimuth().intValue();

			double elCurrentValue = coordinates.get(i).getElevation()
					.intValue();

			if (elLastValue <= elCurrentValue) {
				elCurrentValue = 180 - elCurrentValue;
			}
			if (currentValueSector == azFirstPointSector) {
				azCurrentValue = GlobalFunctions
						.changeAzimuthTo180(azCurrentValue);
				if (Math.abs(azCurrentValue - azLastValue) > 5) {
					azCurrentValue = azLastValue;
				}
			}

			if ((currentValueSector != azFirstPointSector)
					&& (currentValueSector != azLastPointSector)) {
				azCurrentValue = coordinates.get(i - 1).getAzimuth().intValue();
			}

			PointingData optimizedElement = new PointingData(timestamp,
					azCurrentValue, elCurrentValue, doppler, dopplerShift,
					satelliteName, groundStationName);
			coordinatesTemp.add(optimizedElement);
			azLastValue = coordinatesTemp.get(i).getAzimuth();
			elLastValue = coordinates.get(i).getElevation();

		}
		coordinates = coordinatesTemp;
		return coordinates;

	}
}