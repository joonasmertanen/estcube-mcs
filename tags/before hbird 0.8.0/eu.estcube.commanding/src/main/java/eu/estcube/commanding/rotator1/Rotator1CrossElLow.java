package eu.estcube.commanding.rotator1;
import java.util.ArrayList;
import java.util.List;

import org.hbird.exchange.navigation.PointingData;

import eu.estcube.commanding.api.OptimizeInterface;
import eu.estcube.commanding.optimizators.GlobalFunctions;


public class Rotator1CrossElLow implements OptimizeInterface {
	double receivingSector;
	double maxAz;
	double maxEl;

	public Rotator1CrossElLow(double receivingSector, double maxAz, double maxEl) {
		super();
		this.receivingSector = receivingSector;
		this.maxAz = maxAz;
		this.maxEl = maxEl;
	}

	@Override
	public List<PointingData> optimize(List<PointingData> coordinates) {
		coordinates = crossElLowStartEndMiddle(coordinates);
		return coordinates;
	}

	public List<PointingData> crossElLow(List<PointingData> coordinates) {
		int coordinatesListSize = coordinates.size();
		/*double azFirstValue = coordinates.get(0).getAzimuth().intValue();
		coordinates.get(0).setAzimuth(
				GlobalFunctions.changeAzimuthTo180(azFirstValue));
		coordinates.get(0).setElevation(180 - coordinates.get(0).getElevation());*/
		List<PointingData> coordinatesTemp = new ArrayList<PointingData>();

		for (int i = 0; i < coordinatesListSize; i++) {
			long timestamp = coordinates.get(i).getTimestamp();
			double doppler = coordinates.get(i).getDoppler();
			double dopplerShift = coordinates.get(i).getDopplerShift();
			String satelliteName = coordinates.get(i).getSatelliteName();
			String groundStationName = coordinates.get(i)
					.getGroundStationName();

			double azCurrentValue = coordinates.get(i).getAzimuth().intValue();
			double aznewValue = GlobalFunctions
					.changeAzimuthTo180(azCurrentValue);
			double elevation = 180 - coordinates.get(i).getElevation()
					.intValue();
			PointingData optimizedElement = new PointingData(timestamp,
					aznewValue, elevation, doppler, dopplerShift,
					satelliteName, groundStationName);
			coordinatesTemp.add(optimizedElement);
		}
		coordinates = coordinatesTemp;
		return coordinates;

	}

	public List<PointingData> crossElLowStartEndMiddle(
			List<PointingData> coordinates) {
		int coordinatesListSize = coordinates.size();
		double lastValue = coordinates.get(0).getAzimuth().intValue();
		double startAzimuth = lastValue;
		double endAzimuth = coordinates.get(coordinatesListSize - 1)
				.getAzimuth().intValue();
		int crossPoint = 0;
		crossPoint = getCrossPoint(lastValue, crossPoint, coordinates);
		int startEndMiddle = starEndMiddleCheck(crossPoint, startAzimuth,
				endAzimuth, receivingSector, coordinates);
		String status = "";
		List<PointingData> coordinatesTemp = new ArrayList<PointingData>();
		switch (startEndMiddle) {
		case 1: // START
			coordinatesTemp = deleteStartEndValues(1, crossPoint, coordinates);
			status = "contact start";

			break;
		case 2:// END
			coordinatesTemp = deleteStartEndValues(2, crossPoint, coordinates);
			status = "contact end";

			break;
		case 3:// MIDDLE
			coordinatesTemp = crossElLow(coordinates);
			status = "middle";

			break;
		}
		coordinates = coordinatesTemp;
		return coordinates;

	}

	public static int starEndMiddleCheck(int crossPoint, double azFirstValue,
			double azLastValue, double antennaReceivingSector,
			List<PointingData> coordinates) {
		int listCenter = coordinates.size() / 2;

		double left;
		double right;
		int caseToChoose = 0;
		double azCrossPointValue = coordinates.get(crossPoint).getAzimuth();

		left = Math.abs(azCrossPointValue - azFirstValue);

		right = Math.abs(azCrossPointValue - azLastValue);

		if (crossPoint < listCenter) {
			if (left > 300) {
				double change = 360 - left;
				if (change < antennaReceivingSector) {
					caseToChoose = 1;
				} else {
					caseToChoose = 3;
				}

			} else {
				if (left < antennaReceivingSector) {
					caseToChoose = 1;
				} else {
					caseToChoose = 3;
				}

			}
		}

		if (crossPoint > listCenter) {
			if (right > 300) {
				double change = 360 - right;
				if (change < antennaReceivingSector) {
					caseToChoose = 2;
				} else {
					caseToChoose = 3;
				}

			} else {
				if (right < antennaReceivingSector) {
					caseToChoose = 2;
				} else {
					caseToChoose = 3;
				}

			}
		}

		return caseToChoose;
	}

	public static int getCrossPoint(double lastValue, int crossPoint,
			List<PointingData> coordinates) {
		for (int i = 0; i < coordinates.size(); i++) {
			if (Math.abs(lastValue - coordinates.get(i).getAzimuth().intValue()) > 180) {
				crossPoint = i;
			}
			lastValue = coordinates.get(i).getAzimuth().intValue();
		}

		return crossPoint;
	}

	public List<PointingData> deleteStartEndValues(int cases, int crossPoint,
			List<PointingData> coordinates) {

		switch (cases) {
		case 1:
			for (int i = 0; i < crossPoint; i++) {
				coordinates.remove(0);

			}
			break;
		case 2:
			for (int i = crossPoint; i <= coordinates.size(); i++) {
				coordinates.remove(coordinates.size() - 1);

			}
			break;

		}
		return coordinates;

	}

}