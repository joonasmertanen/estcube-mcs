package eu.estcube.commanding.calculation;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.hbird.exchange.groundstation.NativeCommand;
import org.hbird.exchange.groundstation.Track;
import org.hbird.exchange.navigation.PointingData;

import eu.estcube.commanding.optimizators.GlobalFunctions;
import eu.estcube.gs.base.Rotator;

public class GenerateTestData {
	public static double antennaReceivingSector;
	

	public static void main(String[] args) throws IOException {
		antennaReceivingSector = 7.5;
	     List<PointingData> coordinates=new ArrayList<PointingData>();
		coordinates=chooseCase("case 4.1",coordinates);
		GlobalFunctions.printValues(coordinates);
		
		
		RotatorOptimizer optimizer=new RotatorOptimizer();
		Rotator rotator = new Rotator("rot", "Rotator", 0, 0, 450,
				0, 180, "dummy") {
			@Override
			public List<NativeCommand> track(Track arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<NativeCommand> stop() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<NativeCommand> pointTo() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<NativeCommand> park() {
				// TODO Auto-generated method stub
				return null;
			}
		};
		
		//coordinates=GlobalFunctions.listFinalCheck(coordinates);
		coordinates=optimizer.doOptimize(coordinates, rotator);
		GlobalFunctions.printValues(coordinates);
		
		
	}



	public static List<PointingData> chooseCase(String caseN,List<PointingData> coordinates)
			throws IOException {
		try {
			String fileName = new String("src/main/resources/cases/"
					+ caseN + ".txt");
			FileInputStream fstream = new FileInputStream(fileName);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			int counter = 1;
			String azimuth;
			String elevation;
			String space = new String(" ");
			long ts=0L;
			while ((strLine = br.readLine()) != null) {
				

				counter++;
				if ((counter > 9) && (counter < 29)) {
					azimuth = strLine.substring(21, 27);
					elevation = strLine.substring(28, 34);

					if (azimuth.charAt(0) == space.charAt(0)) {
						azimuth = azimuth.substring(1, azimuth.length() - 1);

					}
					if (azimuth.charAt(0) == space.charAt(0)) {
						azimuth = azimuth.substring(1, azimuth.length() - 1);

					}
					
					coordinates.add(new PointingData(ts, Double.parseDouble(azimuth), Double.parseDouble(elevation), 123.0, 123.0, "test", "test"));
					//System.out.println(coordinates.size()+" Added "+coordinates.get(coordinates.size()-1).getAzimuth()+", "+coordinates.get(coordinates.size()-1).getElevation());
				}
			}
			
			in.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
		return coordinates;

	}
}
