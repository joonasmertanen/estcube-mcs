package eu.estcube.commanding.calculation;

import java.util.ArrayList;
import java.util.List;

import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.log4j.Logger;
import org.orekit.tle.TLE;

import eu.estcube.domain.JMSConstants;

public class MetaDataConsumer {
	static Logger log = Logger.getLogger(Calculation.class.getName());
	public static String url = "tcp://localhost:61616";
	public static String TOPIC = JMSConstants.AMQ_COMMANDS_CONSUME_TIME;
	public static List<Long> contactTimeList = new ArrayList<Long>();

	public static void main(String[] args) throws Exception {
		System.setProperty("orekit.data.path",
				"src/main/resources/orekit-data.zip");
		double GS_LONGITUDE = Math.toRadians(26.7330);
		double GS_LATITUDE = Math.toRadians(58.3000);
		double GS_ALTITUDE = 59.0;
		double upFr = 100000000;
		double downFr = 437000000;
		TLE tle = new TLE(
				"1 27846U 03031G   13104.25319836  .00000216  00000-0  11893-3 0  5406",
"2 27846  98.6910 114.4374 0010068  55.8176  74.5716 14.21420837507771");

		double tolerance = 4;
		long start = 0;
		long end = 0;
		int gsMaxAz = 450;
		int gsMaxEl = 180;
		double gsReceivingSector = 7.5;

		while (true) {
			CamelContext context = new DefaultCamelContext();
			ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
					url);
			context.addComponent("activemq",
					JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
			ConsumerTemplate consumerTemplate = context
					.createConsumerTemplate();
			context.start();
			Long contactTime = consumerTemplate.receiveBody(TOPIC, Long.class);
			contactTimeList.add(contactTime);
			log.info("Received time for Calculations with value " + contactTime);
			if (contactTimeList.size() == 2) {
				sendToCalculator(contactTimeList.get(0),
						contactTimeList.get(1), GS_LONGITUDE, GS_LATITUDE,
						GS_ALTITUDE, upFr, downFr, tle, tolerance, start, end,
						gsMaxAz, gsMaxEl, gsReceivingSector);
				contactTimeList.clear();
			}
		}

	}

	public static void sendToCalculator(long start, long end,
			double gS_LONGITUDE, double gS_LATITUDE, double gS_ALTITUDE,
			double upFr, double downFr, TLE tle, double tolerance, long start2,
			long end2, int gsMaxAz, int gsMaxEl, double gsReceivingSector)
			throws Exception {

		Calculation commanding = new Calculation(gS_LONGITUDE, gS_LATITUDE,
				gS_ALTITUDE,/* upFr, downFr, */tle, tolerance, start, end,
				gsMaxAz, gsMaxEl, gsReceivingSector);
		commanding.calculate();

	}

}