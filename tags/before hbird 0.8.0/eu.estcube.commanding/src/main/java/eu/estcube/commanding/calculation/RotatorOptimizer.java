/** 
 *
 */
package eu.estcube.commanding.calculation;

import java.util.List;

import org.hbird.exchange.navigation.PointingData;
import org.springframework.beans.factory.annotation.Autowired;
import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.api.OptimizeInterface;
import eu.estcube.commanding.api.PassAnalyzeInterface;
import eu.estcube.commanding.api.RotatorTypeInterface;
import eu.estcube.commanding.analysis.PassAnalyze;
import eu.estcube.commanding.api.RotatorTypeSelectorInterface;
import eu.estcube.gs.base.Rotator;

/**
 * Class for optimizing satellite tracking data for given {@link Rotator}.
 */
public class RotatorOptimizer {

    /** Minimum number of {@link PointingData} elements needed for optimization. */
    public static final int INPUT_MIN_LENGTH = 7;

    @Autowired
    private PassAnalyzeInterface passAnalyzer=new PassAnalyze();

    @Autowired
    private RotatorTypeSelectorInterface rotatorTypeSelector=new RotatorTypeSelectorImpl();

    public List<PointingData> doOptimize(List<PointingData> input, Rotator rotator) {

        if (input == null) {
            throw new IllegalArgumentException("Pointing data input is null");
        }

        if (input.size() < INPUT_MIN_LENGTH) {
            throw new IllegalArgumentException("Optimizer needs at least " + INPUT_MIN_LENGTH
                    + " for the calculations; input.size=" + input.size());
        }

        if (rotator == null) {
            throw new IllegalArgumentException("Rotator is null");
        }
        
        OverPassType type = passAnalyzer.calculateType(input);
        RotatorTypeInterface typeInterface = rotatorTypeSelector.selectFor(rotator);
        OptimizeInterface optimizer = typeInterface.getOptimizer(type);
        List<PointingData> result = optimizer.optimize(input);
        return result;
    }
}
