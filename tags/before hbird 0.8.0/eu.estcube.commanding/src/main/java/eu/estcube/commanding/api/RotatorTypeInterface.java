package eu.estcube.commanding.api;

import eu.estcube.commanding.analysis.OverPassType;

public interface RotatorTypeInterface {

    OptimizeInterface getOptimizer(OverPassType type);
}
