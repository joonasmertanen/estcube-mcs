package eu.estcube.commanding.optimizators;

import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.hbird.exchange.navigation.PointingData;

public class GlobalFunctions {

	public static double changeAzimuthTo180(double azimuthValue) {
		if (azimuthValue > 180) {
			azimuthValue = azimuthValue - 180;
		} else {
			azimuthValue = azimuthValue + 180;
		}
		return azimuthValue;
	}

	public static double changeAzimuthTo360(double azimuthValue) {
		if (azimuthValue > 360) {
			azimuthValue = azimuthValue - 180;
		} else {
			azimuthValue = azimuthValue + 180;
		}
		return azimuthValue;

	}

	public static int getDegreeSector(double azimuthValue) {
		int sector = 0;
		if ((azimuthValue >= 0) && (azimuthValue <= 90)) {
			sector = 1;
		}
		if ((azimuthValue > 90) && (azimuthValue <= 180)) {
			sector = 2;
		}
		if ((azimuthValue > 180) && (azimuthValue <= 270)) {
			sector = 3;
		}
		if ((azimuthValue > 270) && (azimuthValue <= 359.99)) {
			sector = 4;
		}

		if ((azimuthValue > 360) && (azimuthValue <= 450)) {
			sector = 5;
		}

		return sector;

	}

	public static void printValues(List<PointingData> coordinates) {
		Date d;
		Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
		for (int i = 0; i < coordinates.size(); i++) {
			d = new Date(coordinates.get(i).getTimestamp());
			System.out.println(i + " az " + coordinates.get(i).getAzimuth()
					+ " el " + coordinates.get(i).getElevation() + " time "
					+ format.format(d).toString());

		}

	}

	public static List<PointingData> listFinalCheck(
			List<PointingData> coordinates) {
		List<PointingData> coordinatesTemp = new ArrayList<PointingData>();

		for (int i = 0; i < coordinates.size(); i++) {
			if (((coordinates.get(i).getElevation() > 0) && (coordinates.get(i)
					.getElevation() < 180))
					&& ((coordinates.get(i).getAzimuth() > 0) && (coordinates
							.get(i).getAzimuth() < 360))) {
				coordinatesTemp.add(coordinates.get(i));
			}

		}
		coordinates = coordinatesTemp;
		return coordinates;

	}

	public static int getCrossPoint(int crossPoint,
			List<PointingData> coordinates) {
		double lastValue = coordinates.get(0).getAzimuth();
		for (int i = 0; i < coordinates.size(); i++) {
			if (Math.abs(lastValue - coordinates.get(i).getAzimuth()) > 180) {
				crossPoint = i;
			}
			lastValue = coordinates.get(i).getAzimuth();
		}

		return crossPoint;
	}

	public static boolean ifClockwise(boolean clockwise,
			List<PointingData> coordinates, int crossPoint) {
		if (coordinates.get(crossPoint + 1).getAzimuth() > 180) {
			clockwise = false;

		}
		if (coordinates.get(crossPoint + 1).getAzimuth() < 180) {
			clockwise = true;
		}

		System.out.println("CLOCKWISE:" + clockwise);
		return clockwise;
	}

}
