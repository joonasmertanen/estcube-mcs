/** 
 *
 */
package eu.estcube.commanding.calculation;

import org.springframework.stereotype.Component;

import eu.estcube.commanding.api.RotatorTypeInterface;
import eu.estcube.commanding.api.RotatorTypeSelectorInterface;
import eu.estcube.commanding.rotator1.Rotator1;
import eu.estcube.commanding.rotator2.Rotator2;
import eu.estcube.gs.base.Rotator;

/**
 * 
 */
@Component
public class RotatorTypeSelectorImpl implements RotatorTypeSelectorInterface {

	
    /** @{inheritDoc . */
    @Override
    public RotatorTypeInterface selectFor(Rotator rotator) {
    	RotatorTypeInterface rotatorType1 = new Rotator1(7.5);
    	RotatorTypeInterface rotatorType2 = new Rotator2(7.5);
   
    	if(rotator.getMaxAzimuth()==360 && rotator.getMaxElevation()==180){
    		return rotatorType1;
    	}
    	else
    		return rotatorType2;

    }
}
