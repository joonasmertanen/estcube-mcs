package eu.estcube.commanding.rotator1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hbird.exchange.navigation.PointingData;

import eu.estcube.commanding.api.OptimizeInterface;
import eu.estcube.commanding.optimizators.GlobalFunctions;

public class Rotator1NoCrossElHigh implements OptimizeInterface {
    double receivingSector;
    double maxAz;
    double maxEl;
    Map<Double, Double> coordinates;

    public Rotator1NoCrossElHigh(double receivingSector, double maxAz,
            double maxEl) {
        super();
        this.receivingSector = receivingSector;
        this.maxAz = maxAz;
        this.maxEl = maxEl;

    }

    @Override
    public List<PointingData> optimize(List<PointingData> coordinates) {
        coordinates = noCrossElHigh( coordinates);
        return coordinates;
    }

    public List<PointingData> noCrossElHigh(List<PointingData> coordinates) { // CASE 3
        double elLastValue = coordinates.get(0).getElevation().intValue();
        int coordinatesListSize = coordinates.size();
        final int azLastPointSector = GlobalFunctions
				.getDegreeSector(coordinates.get(coordinatesListSize - 1)
						.getAzimuth().intValue());
		final int azFirstPointSector = GlobalFunctions
				.getDegreeSector(coordinates.get(0).getAzimuth().intValue());
        boolean externalCall = false;
        List<PointingData> coordinatesTemp = new ArrayList<PointingData>();
        
        
        if (this.maxAz < 360) {
            externalCall = false;
        }
        if (this.maxAz > 360) {
            externalCall = true;
        }

       

        for (int i = 0; i < coordinates.size(); i++) {
        	double currentValueSector = GlobalFunctions
					.getDegreeSector(coordinates.get(i).getAzimuth().intValue());
            double azCurrentValue =coordinates.get(i).getAzimuth().intValue();
            double elCurrentValue = coordinates.get(i).getElevation().intValue();
            long timestamp = coordinates.get(i).getTimestamp();
			double doppler = coordinates.get(i).getDoppler();
			double dopplerShift = coordinates.get(i).getDopplerShift();
			String satelliteName = coordinates.get(i).getSatelliteName();
			String groundStationName = coordinates.get(i)
					.getGroundStationName();

            if (currentValueSector == azLastPointSector) {
                if (i < coordinates.size() - 1) {
                    double azNextValue = coordinates.get(i + 1).getAzimuth();
                    if (Math.abs(azCurrentValue - azNextValue) >= 5) {
                        azCurrentValue = azNextValue;
                        if (externalCall == false) {
                            azCurrentValue = GlobalFunctions
                                    .changeAzimuthTo180(azCurrentValue);
                        }
                        if (externalCall == true) {
                            azCurrentValue = GlobalFunctions
                                    .changeAzimuthTo360(azCurrentValue);

                        }
                    } else {
                        if (externalCall == false) {
                            azCurrentValue = GlobalFunctions
                                    .changeAzimuthTo180(azCurrentValue);
                        }
                        if (externalCall == true) {
                            azCurrentValue = GlobalFunctions
                                    .changeAzimuthTo360(azCurrentValue);

                        }

                    }
                } else {
                    if (externalCall == false) {
                        azCurrentValue = GlobalFunctions
                                .changeAzimuthTo180(azCurrentValue);
                    }
                    if (externalCall == true) {
                        azCurrentValue = GlobalFunctions
                                .changeAzimuthTo360(azCurrentValue);

                    }
                }
            }

            if ((currentValueSector != azFirstPointSector)
                    && (currentValueSector != azLastPointSector)) {
                azCurrentValue = coordinatesTemp.get(coordinatesTemp.size() - 1).getAzimuth();

            }
            if (elCurrentValue < elLastValue) {
                elCurrentValue = 180 - (elCurrentValue);
            }
            PointingData optimizedElement = new PointingData(timestamp,
					azCurrentValue, elCurrentValue, doppler, dopplerShift,
					satelliteName, groundStationName);
            coordinatesTemp.add(optimizedElement);
            elLastValue =coordinates.get(i).getElevation();

        }
        coordinates = coordinatesTemp;
        return coordinates;

    }



}