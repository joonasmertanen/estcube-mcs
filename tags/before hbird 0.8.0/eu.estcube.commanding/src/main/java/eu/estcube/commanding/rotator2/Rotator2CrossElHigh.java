package eu.estcube.commanding.rotator2;

import java.util.List;

import org.hbird.exchange.navigation.PointingData;

import eu.estcube.commanding.api.OptimizeInterface;
import eu.estcube.commanding.optimizators.GlobalFunctions;
import eu.estcube.commanding.rotator1.Rotator1CrossElHigh;
import eu.estcube.commanding.rotator1.Rotator1NoCrossElHigh;

public class Rotator2CrossElHigh implements OptimizeInterface {
	double receivingSector;
	double maxAz;
	double maxEl;


	public Rotator2CrossElHigh(double receivingSector, double maxAz,
			double maxEl) {
		super();
		this.receivingSector = receivingSector;
		this.maxAz = maxAz;
		this.maxEl = maxEl;
	}

	@Override
	public List<PointingData> optimize(List<PointingData> coordinates) {
		boolean suitable = false;
		boolean clockwise = false;
		int crossPoint = 0;
		crossPoint = GlobalFunctions.getCrossPoint(crossPoint, coordinates);
		clockwise = GlobalFunctions.ifClockwise(clockwise, coordinates,
				crossPoint);
		suitable = caseSuitable(suitable, coordinates, crossPoint, clockwise);

		if (suitable == true) {
			if (clockwise == true) {
				Rotator1NoCrossElHigh caseToApply1 = new Rotator1NoCrossElHigh(
						receivingSector, 360, maxEl);
				coordinates = caseToApply1.optimize(coordinates);
				System.out.println("APPLYING CASE AS FOR ROTATOR 1 CASE 3");
			}

			if (clockwise == false) {
				Rotator1NoCrossElHigh caseToApply1 = new Rotator1NoCrossElHigh(
						receivingSector, maxAz, maxEl);
				coordinates = changeAzimuthList(coordinates, crossPoint);
				coordinates = caseToApply1.optimize(coordinates);
			}

		}
		if (suitable == false) {
			Rotator1CrossElHigh caseToApply = new Rotator1CrossElHigh(
					receivingSector, maxAz, maxEl);
			coordinates = caseToApply.optimize(coordinates);
			System.out.println("APPLYING CASE AS FOR ROTATOR 1 CASE 4");
		}

		return coordinates;
	}

	public boolean caseSuitable(boolean suitable,
			List<PointingData> coordinates, int crossPoint, boolean clockwise) {

		suitable = withinMaxAzRange(suitable, coordinates, clockwise);

		return suitable;
	}

	public List<PointingData> changeAzimuthList(List<PointingData> coordinates,
			int crossPoint) {
		double addValue = 0;
		for (int i = 0; i < crossPoint; i++) {
			addValue = (coordinates.get(i).getAzimuth() + 360);
			coordinates.get(i).setAzimuth(addValue);
		}

		return coordinates;
	}

	public boolean withinMaxAzRange(boolean suitable,
			List<PointingData> coordinates, boolean clockwise) {
		if (clockwise) {
			if (coordinates.get(coordinates.size() - 1).getAzimuth() < 90) {
				suitable = true;

			} else {
				suitable = false;
			}
		} else {
			if (coordinates.get(0).getAzimuth() < 90) {
				suitable = true;

			} else {
				suitable = false;
			}

		}

		return suitable;
	}

}