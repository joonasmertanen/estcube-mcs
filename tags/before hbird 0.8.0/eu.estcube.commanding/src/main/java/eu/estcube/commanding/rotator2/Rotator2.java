package eu.estcube.commanding.rotator2;

import eu.estcube.commanding.analysis.OptimizationType;
import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.api.RotatorTypeInterface;
import eu.estcube.commanding.optimizators.Optimization;

public class Rotator2 implements RotatorTypeInterface {
	double receivingSector;
	private final double maxAz = 445;
	private final double maxEl = 180;

	public Rotator2(double receivingSector) {
		super();
		this.receivingSector = receivingSector;
	}

	public Optimization getOptimizer(OverPassType type) {
		OptimizationType optType = OptimizationType.NOTHING;
		switch (type) {
		case CROSS_ELEVATION_HIGH:
			optType = OptimizationType.ROT_2_CROSS_EL_HIGH; // IF WITHIN RANGE
															// AFTER CROSS, THEN
															// LIKE FOR ROTATOR
															// 1, BUT CASE 3, IF
															// NOT, THEN FOR
															// ROTATOR CASE 4
			break;
		case NO_CROSS_ELEVATION_HIGH:
			optType = OptimizationType.ROT_1_NO_CROSS_EL_HIGH; // ROTATOR 1
			break;
		case CROSS_ELEVATION_LOW:
			optType = OptimizationType.ROT_2_CROSS_EL_LOW; // IF MOVEMENT
															// CLOCKWISE, IF
															// LAST POINT AFTER
															// CROSS IS <THAT
															// MAX AZIMUTH the
															// change to +360,
															// IF NOT, THEN CASE
															// FOR ROTATOR 1
			break;
		case NO_CROSS_ELEVATION_LOW:
			optType = OptimizationType.ROT_1_NO_CROSS_EL_LOW; // NO CHANGES
			break;

		case NOTHING:
			break;
		}
		System.out.println(optType.toString());
		Optimization optimization = new Optimization(optType, maxAz, maxEl,
				receivingSector);

		return optimization;
	}

	public double getReceivingSector() {
		return receivingSector;
	}

	public double getMaxAz() {
		return maxAz;
	}

	public double getMaxEl() {
		return maxEl;
	}

}