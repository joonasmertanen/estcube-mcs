package eu.estcube.commanding.rotator1;

import java.util.List;

import org.hbird.exchange.navigation.PointingData;

import eu.estcube.commanding.api.OptimizeInterface;

public class Rotator1NoCrossElLow implements OptimizeInterface {
    double receivingSector;
    double maxAz;
    double maxEl;

    public Rotator1NoCrossElLow(double receivingSector, double maxAz,
            double maxEl) {
        super();
        this.receivingSector = receivingSector;
        this.maxAz = maxAz;
        this.maxEl = maxEl;

    }

    @Override
    public List<PointingData>  optimize(List<PointingData> coordinates) {
        return coordinates;
    }



}