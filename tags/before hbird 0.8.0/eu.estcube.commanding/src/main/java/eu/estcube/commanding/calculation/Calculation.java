package eu.estcube.commanding.calculation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import eu.estcube.gs.base.Rotator;
import java.util.concurrent.TimeUnit;
import org.apache.commons.math.geometry.Vector3D;
import org.apache.log4j.Logger;
import org.orekit.bodies.BodyShape;
import org.orekit.bodies.GeodeticPoint;
import org.orekit.bodies.OneAxisEllipsoid;
import org.orekit.errors.OrekitException;
import org.orekit.frames.Frame;
import org.orekit.frames.FramesFactory;
import org.orekit.frames.LocalOrbitalFrame;
import org.orekit.frames.LocalOrbitalFrame.LOFType;
import org.orekit.frames.TopocentricFrame;
import org.orekit.orbits.CartesianOrbit;
import org.orekit.orbits.Orbit;
import org.orekit.propagation.Propagator;
import org.orekit.propagation.analytical.KeplerianPropagator;
import org.orekit.time.AbsoluteDate;
import org.orekit.time.TimeScalesFactory;
import org.orekit.tle.TLE;
import org.orekit.tle.TLEPropagator;
import org.orekit.utils.PVCoordinates;
import org.hbird.exchange.groundstation.NativeCommand;
import org.hbird.exchange.groundstation.Track;
import org.hbird.exchange.navigation.PointingData;
import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.analysis.PassAnalyze;
import eu.estcube.commanding.api.OptimizeInterface;
import eu.estcube.commanding.api.RotatorTypeInterface;
import eu.estcube.commanding.optimizators.GlobalFunctions;
import eu.estcube.commanding.optimizators.Optimization;
import eu.estcube.commanding.rotator2.Rotator2;

/**
 * @author Ivar Mahhonin This class makes necessary calculations for antenna
 *         rotator so it could follow satellite position and radio
 * 
 */
public class Calculation {
	// TODO implement antenna receiving tolerance (receiving width sector)
	public static final double ROT_RECEIVING_TOLERANCE = 7.5;
	private static double firstAzimuth;
	private static double firstElevation;
	/** Equatorial radius of Earth */
	public static final double EARTH_RADIUS = 6378137.0;
	/** Flattening */
	public static final double FLATENNING = 1.0 / 298.257223563;
	/** A central attraction coefficient */
	public static final double MU = 3.986004415e+14;
	/** Speed of light */
	public static final double SPD_OF_LIGHT = 299792458.0;
	/** Longitude of the ground station */
	double GS_LONGITUDE = Math.toRadians(26.7330);
	/** Latitude of the ground station */
	double GS_LATITUDE = Math.toRadians(58.3000);
	/** Altitude of the ground station */
	double GS_ALTITUDE = 59.0;
	/** Radio uplink frequency */
	double uplink_fr;
	/** Radio downling frequenct */
	double downlink_fr;
	/** TLE data for satelite */
	TLE tle;
	/** For counting 3 seconds and generating new command for radio */
	static int counter = 0;
	/**
	 * Tolerance for elevation and azimuth. According to it we calculate this
	 * values with stepp
	 */
	double tolerance;
	/** Contact start time */
	long startTime;
	/** Contact end time */
	long endTime;
	/** Ground station antenna rotator maximum azimuth value */
	int gsMaxAz;
	/** Ground station antenna rotator maximum elevation value */
	int gsMaxEl;
	/** Ground station antenna receiving width */
	double gsReceivingSector;
	/** logger */
	static Logger log = Logger.getLogger(Calculation.class.getName());

	/** Object constructor */

	public Calculation(double gS_LONGITUDE, double gS_LATITUDE,
			double gS_ALTITUDE,/* double uplink_fr, double downlink_fr,*/ TLE tle,
			double tolerance, long startTime, long endTime, int gsMaxAz,
			int gsMaxEl, double gsReceivingSector) {
		super();
		GS_LONGITUDE = gS_LONGITUDE;
		GS_LATITUDE = gS_LATITUDE;
		GS_ALTITUDE = gS_ALTITUDE;
		/*this.uplink_fr = uplink_fr;
		this.downlink_fr = downlink_fr;*/
		this.tle = tle;
		this.tolerance = tolerance;
		this.startTime = startTime;
		this.endTime = endTime;
		this.gsMaxAz = gsMaxAz;
		this.gsMaxEl = gsMaxEl;
		this.gsReceivingSector = gsReceivingSector;
	}

	/**
	 * Here we are setting contact time period and running endless loop in
	 * waiting for the contact time to start.
	 * 
	 * @param args
	 * @throws Exception
	 */

	public void calculate() throws Exception {
		long schPeriod;
		Date nowDate = new Date();
		System.out.println(nowDate.toString());
		System.setProperty("orekit.data.path",
				"src/main/resources/orekit-data.zip");
		Date start = new Date(startTime);
		Date end = new Date(endTime);
		log.info("Received contact time");
		log.info("upLink frequency: " + uplink_fr);
		log.info("downlink frequency: " + downlink_fr);
		schPeriod = calculateSchPeriod(start, end);

		GeodeticPoint station = new GeodeticPoint(GS_LATITUDE, GS_LONGITUDE,
				GS_ALTITUDE);

		OneAxisEllipsoid oae = new OneAxisEllipsoid(EARTH_RADIUS, FLATENNING,
				FramesFactory.getITRF2005());
		TopocentricFrame es5ec = new TopocentricFrame(oae, station, "ES5EC");

		AbsoluteDate now = new AbsoluteDate(start, TimeScalesFactory.getUTC());

		TLEPropagator propagator = TLEPropagator.selectExtrapolator(tle);

		printOut(now, es5ec, propagator, station, start, schPeriod);

	}

	/**
	 * Calculating @param azimuth, @param elevation, @param doppler in loop,
	 * while contact time is continues.
	 * 
	 * @param now
	 *            - current date and time in UTC format
	 * @param es5ec
	 *            -TopocentriFrame class object in order to its methods for
	 *            calculating @param azimuth, @param elevation, @param doppler
	 * @param propagator
	 *            - the models used are SGP4 and SDP4, initially proposed by
	 *            NORAD as the unique convenient
	 * @param station
	 *            -3D vector that defines our ground station position
	 * @param hoursStart
	 *            - setting hours of contact start time point
	 * @param hoursEnd
	 *            - setting minutes of contact start time point
	 * @param minStart
	 *            - setting hours of contact end time point
	 * @param minutesEnd
	 *            - setting minutes of contact end time point
	 * @throws Exception
	 */

	void printOut(AbsoluteDate now, TopocentricFrame es5ec,
			TLEPropagator propagator, GeodeticPoint station, Date start,
			long schPeriod) throws Exception {
		Date timeStamp = new Date(start.getTime());
		List<PointingData> coordinates = new ArrayList<PointingData>();
		/*
		 * List<Long> timeStampRotator = new ArrayList<Long>(); List<Long>
		 * timeStampRadio = new ArrayList<Long>(); List<Double> frUp = new
		 * ArrayList<Double>(); List<Double> frDown = new ArrayList<Double>();
		 * ThreeLists result; ThreeLists result2; TwoLists result3;
		 */

		Vector3D position = propagator.getPVCoordinates(now).getPosition();
		firstAzimuth = calculateAzimuth(es5ec, position, now);
		firstElevation = calculateElevation(es5ec, position, now);

		for (int second = 0; second < schPeriod * 2; second++) {

			position = propagator.getPVCoordinates(now).getPosition();
			// Vector3D velocity =
			// propagator.getPVCoordinates(now).getVelocity();
			double az = calculateAzimuth(es5ec, position, now);
			double el = calculateElevation(es5ec, position, now);
			/*
			 * double doppler1 = calculateDoppler(now, position, velocity,
			 * station, uplink_fr); double doppler2 = calculateDoppler(now,
			 * position, velocity, station, downlink_fr);
			 */

			/*
			 * result2 = sendForRadioAMQ(frUp, frDown, doppler1, doppler2,
			 * second, timeStamp.getTime(), timeStampRadio);
			 */
			coordinates = sendForRotatorAMQ(coordinates, az, el, second, /*
																		 * timeStampRotator
																		 * ,
																		 */
					timeStamp.getTime());
			/* timeStampRotator = result.getThirdList(); */
			/*
			 * frUp = result2.getFirstList(); frDown = result2.getSecondList();
			 * timeStampRadio = result2.getThirdList();
			 */

			counter++;
			timeStamp = increaseTimeStamp(timeStamp, 500);
			now = now.shiftedBy(0.5 * 1D);
		}

		try {

			coordinates = GlobalFunctions.listFinalCheck(coordinates);
			GlobalFunctions.printValues(coordinates);

			PassAnalyze analyze = new PassAnalyze();
			Rotator rotator = new Rotator("rot", "Rotator", 0, 0, this.gsMaxAz,
					0, this.gsMaxEl, "dummy") {
				@Override
				public List<NativeCommand> track(Track arg0) {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public List<NativeCommand> stop() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public List<NativeCommand> pointTo() {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public List<NativeCommand> park() {
					// TODO Auto-generated method stub
					return null;
				}
			};

			process(analyze, rotator, coordinates);

		} catch (IndexOutOfBoundsException e) {
			System.out.println("Elevation is all negative: " + e);
		}

		AMQsend.endConnection();

	}

	public void generateCommands(List<PointingData> coordinates/*
																 * List<Long>
																 * timeStampRadio
																 * ,
																 * List<Double>
																 * frUp,
																 * List<Double>
																 * frDown
																 */)
			throws Exception {
		for (int i = 0; i < coordinates.size(); i++) {
			long timeStamp = coordinates.get(i).getTimestamp();
			String azim = String.valueOf(coordinates.get(i).getAzimuth());
			int dotPosAzim = azim.indexOf(".");
			String elev = String.valueOf(coordinates.get(i).getElevation());
			int dotPosElev = elev.indexOf(".");
			AMQsend.sendToRotator(azim.substring(0, dotPosAzim),
					elev.substring(0, dotPosElev), timeStamp);
		}
		/*
		  for (int i = 0; i < frUp.size(); i++) { double freqUpDouble =
		  frUp.get(i); double freqDwnDouble = frDown.get(i); int freqUp = (int)
		  freqUpDouble; int freqDwn = (int) freqDwnDouble;
		  AMQsend.sendToRadio(Integer.toString(freqUp),
		 Integer.toString(freqDwn), "V Main", true, timeStampRadio.get(i));
		  AMQsend.sendToRadio(Integer.toString(freqUp),
		  Integer.toString(freqDwn), "V Sub", false, timeStampRadio.get(i)); }
		 */

	}

	public void process(PassAnalyze analyze, Rotator rotator,
			List<PointingData> coordinates) throws Exception {
		OverPassType type = analyze.calculateType(coordinates);
		RotatorTypeSelectorImpl selector = new RotatorTypeSelectorImpl();
		RotatorTypeInterface rotatorType = selector.selectFor(rotator);
		RotatorOptimizer doOptimize = new RotatorOptimizer();
		coordinates = doOptimize.doOptimize(coordinates, rotator);

		generateCommands(coordinates/*
									 * , timeStampRotator, timeStampRadio, frUp,
									 * frDown
									 */);

	}

	/**
	 * Calculating doppler for the satellite
	 * 
	 * @param now
	 *            - current date and time in UTC format
	 * @param position
	 *            -satellite position
	 * @param velocity
	 *            - satellite velocity
	 * @param station
	 *            - 3D vector that defines our ground station position
	 * @return
	 * @throws OrekitException
	 */
	public double calculateDoppler(AbsoluteDate now, Vector3D position,
			Vector3D velocity, GeodeticPoint station, double frequency)
			throws OrekitException {
		double doppler = 0;
		now = now.shiftedBy(0.5 * 1D);
		Frame inertialFrame = FramesFactory.getCIRF2000();
		PVCoordinates pvCoordinates = new PVCoordinates(position, velocity);
		Orbit initialOrbit = new CartesianOrbit(pvCoordinates, inertialFrame,
				now, MU);
		Propagator kepler = new KeplerianPropagator(initialOrbit);
		LocalOrbitalFrame lof = new LocalOrbitalFrame(inertialFrame,
				LOFType.QSW, kepler, "QSW");
		Frame ITRF2005 = FramesFactory.getITRF2005();
		BodyShape earth = new OneAxisEllipsoid(EARTH_RADIUS, FLATENNING,
				ITRF2005);
		TopocentricFrame staF = new TopocentricFrame(earth, station, "station");
		PVCoordinates origin = PVCoordinates.ZERO;
		PVCoordinates pv = staF.getTransformTo(lof, now)
				.transformPVCoordinates(origin);
		double dp = Vector3D.dotProduct(pv.getPosition(), pv.getVelocity())
				/ pv.getPosition().getNorm();
		doppler = ((1 - (dp / SPD_OF_LIGHT)) * frequency) - frequency;
		return doppler;
	}

	/**
	 * Generating commands for Rotator. When azimuth or elevation values are
	 * changing to 3 degrees, then we are generating new command for rotator,
	 * where refreshes only parameter with changed value.
	 * 
	 * @param azimuth
	 *            - current azimuth
	 * @param elevation
	 *            - current elevation
	 * @param i
	 *            - generate command at the very first time
	 * @paran tinmeStamp - time, when command must be released from ActivqMQ
	 */

	public List<PointingData> sendForRotatorAMQ(List<PointingData> coordinates,
			double azimuth, double elevation, int i,/* List<Long> timeStamps, */
			long timeStamp) throws Exception {

		if ((Math.abs(Math.abs(firstAzimuth) - Math.abs(azimuth)) >= 3)
				|| i == 1) {
			PointingData element = new PointingData(timeStamp, azimuth,
					elevation, 0.0, 0.0, "ESTCube-1", "Tartu");
			coordinates.add(element);
			firstAzimuth = azimuth;
		}

		if ((Math.abs(Math.abs(firstElevation) - Math.abs(elevation)) >= 3)) {
			PointingData element = new PointingData(timeStamp, azimuth,
					elevation, 0.0, 0.0, "ESTCube-1", "Tartu");
			coordinates.add(element);
			firstElevation = elevation;
		}
		return coordinates;

	}

	/**
	 * Generating command for Radio Calculating commands for Radio (for each
	 * band).
	 * 
	 * @param doppler1
	 *            - radio frequency uplink doppler shift
	 * @param doppler2
	 *            - radio frequency downlink doppler shift
	 * @param i
	 *            - generating command at the very first time
	 * @param timeStamp
	 *            - time, when command must be released from ActivqMQ
	 * @throws Exception
	 */

	/*public ThreeLists sendForRadioAMQ(List<Double> frUp, List<Double> frDown,
			double doppler1, double doppler2, int i, long timeStamp,
			List<Long> timeStamps) throws Exception {
		if (counter == 6 || i == 1) {
			counter = 0;
			double freqUpDouble = uplink_fr + doppler1;
			double freqDwnDouble = downlink_fr + doppler2;
			frUp.add(freqUpDouble);
			frDown.add(freqDwnDouble);
			timeStamps.add(timeStamp);
		}
		return new ThreeLists(frUp, frDown, timeStamps);

	}*/

	/**
	 * 
	 * @param es5ec
	 *            - TopocentriFrame class object in order to its methods for
	 *            calculating @param azimuth, @param elevation, @param doppler
	 * @param position
	 *            - satellite position
	 * @param now
	 *            - current date and time in UTC format
	 * @throws OrekitException
	 */
	public double calculateAzimuth(TopocentricFrame es5ec, Vector3D position,
			AbsoluteDate now) throws OrekitException {
		double azimuth = 0;
		now = now.shiftedBy(0.5 * 1D);
		double azimuthRad = es5ec.getAzimuth(position,
				FramesFactory.getCIRF2000(), now);
		azimuth = (int) (Math.toDegrees(azimuthRad) * 10) / 10;
		return azimuth;
	}

	/**
	 * 
	 * @param es5ec
	 *            - TopocentriFrame class object in order to its methods for
	 *            calculating @param azimuth, @param elevation, @param doppler
	 * @param position
	 *            - satellite position
	 * @param now
	 *            - current date and time in UTC format
	 * @throws OrekitException
	 */
	public double calculateElevation(TopocentricFrame es5ec, Vector3D position,
			AbsoluteDate now) throws OrekitException {
		double elevation = 0;
		now = now.shiftedBy(0.5 * 1D);
		double elevationRad = es5ec.getElevation(position,
				FramesFactory.getCIRF2000(), now);

		elevation = (int) (Math.toDegrees(elevationRad) * 10) / 10;
		return elevation;
	}

	/**
	 * 
	 * @param timeStamp
	 *            - getting timestamp for each command
	 * @param stepp
	 *            - timestamp stepp
	 * @return
	 */

	public Date increaseTimeStamp(Date timeStamp, int stepp) {
		Calendar c = Calendar.getInstance();
		c.setTime(timeStamp);

		c.add(Calendar.MILLISECOND, stepp);
		timeStamp = c.getTime();
		return timeStamp;
	}

	/**
	 * Calculating duration of a calculation period
	 * 
	 * @return
	 */

	public long calculateSchPeriod(Date start, Date end) {
		long schPeriod;
		long startMilliSec = start.getTime();
		long endMilliSec = end.getTime();
		schPeriod = (endMilliSec - startMilliSec);
		schPeriod = TimeUnit.MILLISECONDS.toSeconds(schPeriod);
		System.out.println("Period  is " + schPeriod + " seconds");
		return schPeriod;

	}

}