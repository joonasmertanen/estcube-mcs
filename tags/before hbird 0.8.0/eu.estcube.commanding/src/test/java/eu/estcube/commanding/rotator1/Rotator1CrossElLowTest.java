package eu.estcube.commanding.rotator1;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hbird.exchange.navigation.PointingData;
import org.junit.Test;

import com.google.common.primitives.Doubles;

import eu.estcube.commanding.analysis.OptimizationType;
import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.optimizators.GlobalFunctions;
import eu.estcube.commanding.optimizators.Optimization;
import eu.estcube.commanding.rotator1.Rotator1;

public class Rotator1CrossElLowTest {
	public static List<PointingData> coordinatesResult = new ArrayList<PointingData>();
	public static List<PointingData> coordinatesExpected = new ArrayList<PointingData>();
	public static Rotator1 rotator1 = new Rotator1(7.5);

	@Test
	public void crossElLowAntiClockwise() {
		double[] azArrayResult = { 56.1, 38.7, 24.0, 12.9, 4.0, 358.91, 354.43,
				350.99 };
		double[] elArrayResult = { 35.5, 33.95, 30.08, 25.28, 20.47, 16.06,
				12.13, 8.62 };
		double[] azArrayExpected = { 236.0, 218.0, 204.0, 192.0, 184.0, 178.0,
				174.0, 170.0 };
		double[] elArrayExpected = { 145.0, 147.0, 150.0, 155.0, 160.0, 164.0,
				168.0, 172.0 };

		fillLists( azArrayResult, elArrayResult,azArrayExpected,
				elArrayExpected);

		Optimization optimizationCase = new Optimization(
				OptimizationType.ROT_1_CROSS_EL_LOW, rotator1.getMaxAz(),
				rotator1.getMaxEl(), rotator1.getReceivingSector());

		coordinatesResult = optimizationCase.optimize(coordinatesResult);

		GlobalFunctions.printValues(coordinatesResult);
		System.out.println("-------------");
		GlobalFunctions.printValues(coordinatesExpected);

		for(int i=0;i<coordinatesResult.size();i++){
			assertEquals(coordinatesResult.get(i).getAzimuth(), coordinatesExpected.get(i).getAzimuth());
			assertEquals(coordinatesResult.get(i).getElevation(), coordinatesExpected.get(i).getElevation());
			
		}
		
		coordinatesResult.clear();
		coordinatesExpected.clear();

	}
	
	
	public void crossElLowAntiClockwiseEnd() {
		double[] azArrayResult = { 56.1, 38.7, 24.0, 12.9, 4.0, 358.91};
		double[] elArrayResult = { 35.5, 33.95, 30.08, 25.28, 20.47, 16.06,};
		double[] azArrayExpected = { 56.1, 38.7, 24.0, 12.9, 4.0};
		double[] elArrayExpected = { 35.5, 33.95, 30.08, 25.28, 20.47};

		fillLists( azArrayResult, elArrayResult,azArrayExpected,
				elArrayExpected);

		Optimization optimizationCase = new Optimization(
				OptimizationType.ROT_1_CROSS_EL_LOW, rotator1.getMaxAz(),
				rotator1.getMaxEl(), rotator1.getReceivingSector());

		coordinatesResult = optimizationCase.optimize(coordinatesResult);

		GlobalFunctions.printValues(coordinatesResult);
		System.out.println("-------------");
		GlobalFunctions.printValues(coordinatesExpected);

		for(int i=0;i<coordinatesResult.size();i++){
			assertEquals(coordinatesResult.get(i).getAzimuth(), coordinatesExpected.get(i).getAzimuth());
			assertEquals(coordinatesResult.get(i).getElevation(), coordinatesExpected.get(i).getElevation());
			
		}
		
		coordinatesResult.clear();
		coordinatesExpected.clear();

	}
	
	
	

	public void fillLists(double[] azArrayResult, double[] elArrayResult,
			double[] azArrayExpected, double[] elArrayExpected) {
		for (int i = 0; i < azArrayResult.length; i++) {
			coordinatesExpected.add(new PointingData(0L, azArrayExpected[i],
					elArrayExpected[i], 123.0, 123.0, "test", "test"));
			coordinatesResult.add(new PointingData(0L, azArrayResult[i],
					elArrayResult[i], 123.0, 123.0, "test", "test"));
		}

	}
}
