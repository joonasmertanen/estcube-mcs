package eu.estcube.commanding.rotator2;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.hbird.exchange.navigation.PointingData;
import org.junit.Test;

import eu.estcube.commanding.analysis.OptimizationType;
import eu.estcube.commanding.optimizators.GlobalFunctions;
import eu.estcube.commanding.optimizators.Optimization;

public class Rotator2CrossElLowTest {
	public static List<PointingData> coordinatesResult = new ArrayList<PointingData>();
	public static List<PointingData> coordinatesExpected = new ArrayList<PointingData>();
	public static Rotator2 rotator2 = new Rotator2(7.5);

	@Test
	public void crossElLowNotInRange() {
		double[] azArrayResult = { 56.1, 38.7, 24.0, 12.9, 4.0, 358.91, 354.43,
				350.99 };
		double[] elArrayResult = { 35.5, 33.95, 30.08, 25.28, 20.47, 16.06,
				12.13, 8.62 };
		double[] azArrayExpected = { 416.1, 398.7, 384.0, 372.9, 364.0, 358.91,
				354.43, 350.99 };
		double[] elArrayExpected = { 35.5, 33.95, 30.08, 25.28, 20.47, 16.06,
				12.13, 8.62 };

		fillLists(azArrayResult, elArrayResult, azArrayExpected,
				elArrayExpected);

		Optimization optimizationCase = new Optimization(
				OptimizationType.ROT_2_CROSS_EL_LOW, rotator2.getMaxAz(),
				rotator2.getMaxEl(), rotator2.getReceivingSector());

		coordinatesResult = optimizationCase.optimize(coordinatesResult);

		System.out.println("-------------");
		GlobalFunctions.printValues(coordinatesResult);
		System.out.println("-------------");
		GlobalFunctions.printValues(coordinatesExpected);
		System.out.println("-------------");

		for (int i = 0; i < coordinatesResult.size(); i++) {
			assertEquals(coordinatesResult.get(i).getAzimuth(),
					coordinatesExpected.get(i).getAzimuth());
			assertEquals(coordinatesResult.get(i).getElevation(),
					coordinatesExpected.get(i).getElevation());

		}

		coordinatesResult.clear();
		coordinatesExpected.clear();

	}

	@Test
	public void crossElLowInRange() {
		double[] azArrayResult = { 345, 350.0, 355.6, 359.8, 0.0, 1.0, 2.0,
				3.0, 4.0 };
		double[] elArrayResult = { 35.5, 33.95, 30.08, 25.28, 20.47, 16.06,
				15.0, 12.13, 8.62 };
		double[] azArrayExpected = { 345.0, 350.0, 355.6, 359.8, 360.0, 361.0,
				362.0, 363.0, 364.0 };
		double[] elArrayExpected = { 35.5, 33.95, 30.08, 25.28, 20.47, 16.06,
				15.0, 12.13, 8.62 };

		fillLists(azArrayResult, elArrayResult, azArrayExpected,
				elArrayExpected);

		Optimization optimizationCase = new Optimization(
				OptimizationType.ROT_2_CROSS_EL_LOW, rotator2.getMaxAz(),
				rotator2.getMaxEl(), rotator2.getReceivingSector());

		coordinatesResult = optimizationCase.optimize(coordinatesResult);

		System.out.println("-------------");
		GlobalFunctions.printValues(coordinatesResult);
		System.out.println("-------------");
		GlobalFunctions.printValues(coordinatesExpected);
		System.out.println("-------------");

		for (int i = 0; i < coordinatesResult.size(); i++) {
			assertEquals(coordinatesResult.get(i).getAzimuth(),
					coordinatesExpected.get(i).getAzimuth());
			assertEquals(coordinatesResult.get(i).getElevation(),
					coordinatesExpected.get(i).getElevation());

		}
		coordinatesResult.clear();
		coordinatesExpected.clear();

	}

	public void fillLists(double[] azArrayResult, double[] elArrayResult,
			double[] azArrayExpected, double[] elArrayExpected) {
		for (int i = 0; i < azArrayResult.length; i++) {
			coordinatesExpected.add(new PointingData(0L, azArrayExpected[i],
					elArrayExpected[i], 123.0, 123.0, "test", "test"));
			coordinatesResult.add(new PointingData(0L, azArrayResult[i],
					elArrayResult[i], 123.0, 123.0, "test", "test"));
		}

	}
}
