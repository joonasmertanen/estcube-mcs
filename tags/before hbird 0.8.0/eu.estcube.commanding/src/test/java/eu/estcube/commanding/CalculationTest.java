package eu.estcube.commanding;

import static org.junit.Assert.assertEquals;
import org.apache.commons.math.geometry.Vector3D;
import org.junit.Before;
import org.junit.Test;
import org.orekit.bodies.GeodeticPoint;
import org.orekit.bodies.OneAxisEllipsoid;
import org.orekit.errors.OrekitException;
import org.orekit.frames.FramesFactory;
import org.orekit.frames.TopocentricFrame;
import org.orekit.time.AbsoluteDate;
import org.orekit.time.TimeScalesFactory;
import org.orekit.tle.TLE;
import org.orekit.tle.TLEPropagator;
import eu.estcube.commanding.calculation.Calculation;

public class CalculationTest {
	double GS_LONGITUDE = Math.toRadians(26.7330);
	double GS_LATITUDE = Math.toRadians(58.3000);
	double GS_ALTITUDE = 59.0;
	/*
	 * double upFr = 100000000; double downFr = 437000000;
	 */
	double tolerance = 4;
	/** MASAT-1 frequency according to to 2013-02-24-14-01-46 */
	public double FREQUENCY = 100000000;
	/** 2013-02-24-14-01-46 */
	public AbsoluteDate now;
	/**
	 * the models used are SGP4 and SDP4, initially proposed by NORAD as the
	 * unique convenient
	 */
	TLEPropagator propagator;
	/** position -satellite position */
	Vector3D position;
	/** MASAT-1 TLE data according to 2013-02-24-14-01-46 */
	String tle1 = "1 38081U 12006E   13054.10096693  .00039570  33082-5  86766-3 0  8907";
	String tle2 = "2 38081  69.4788  93.7652 0662838 286.7841  66.1545 14.34813775 53317";
	/** TLE object from tle string */
	TLE tle;
	/** satellite velocity */
	Vector3D velocity;
	/** 3D vector that defines our ground station position */
	GeodeticPoint station;
	/** OneAxisEllipsoid */
	OneAxisEllipsoid oae;
	/**
	 * TopocentriFrame class object in order to its methods for calculating @param
	 * azimuth, @param elevation, @param doppler
	 */
	TopocentricFrame es5ec;

	/** initializing variables */
	@Before
	public void calculate() throws Exception {
		System.setProperty("orekit.data.path",
				"src/main/resources/orekit-data.zip");
		now = new AbsoluteDate(2013, 02, 24, 14, 01, 46,
				TimeScalesFactory.getUTC());
		tle = new TLE(tle1, tle2);
		propagator = TLEPropagator.selectExtrapolator(tle);
		position = propagator.getPVCoordinates(now).getPosition();
		// velocity = propagator.getPVCoordinates(now).getVelocity();
		station = new GeodeticPoint(GS_LATITUDE, GS_LONGITUDE, GS_ALTITUDE);
		oae = new OneAxisEllipsoid(Calculation.EARTH_RADIUS,
				Calculation.FLATENNING, FramesFactory.getITRF2005());
		es5ec = new TopocentricFrame(oae, station, "ES5EC");

	}

	/*
	 * @Test public void testSetDoppler() throws OrekitException { Calculation
	 * commanding = new Calculation(GS_LONGITUDE, GS_LATITUDE, GS_ALTITUDE,upFr,
	 * downFr, tle, tolerance, 0, 0, 450, 180, 7.5); double DOPPLER_EXPECTED =
	 * -806; assertEquals(DOPPLER_EXPECTED, commanding.calculateDoppler(now,
	 * position, velocity, station, FREQUENCY), 5); }
	 */
	/** Testing azimuth calculation */
	@Test
	public void testSetAzimuth() throws OrekitException {
		Calculation commanding = new Calculation(GS_LONGITUDE, GS_LATITUDE,
				GS_ALTITUDE, /* upFr, downFr, */tle, tolerance, 0, 0, 450, 180,
				7.5);
		double AZIMUTH_EXPECTED = 6.88;
		assertEquals(AZIMUTH_EXPECTED,
				commanding.calculateAzimuth(es5ec, position, now), 5);
	}

	/** Testing elevation calculation */
	@Test
	public void testSetElevation() throws OrekitException {
		Calculation commanding = new Calculation(GS_LONGITUDE, GS_LATITUDE,
				GS_ALTITUDE,/* upFr, downFr, */tle, tolerance, 0, 0, 450, 180,
				7.5);
		double ELEVATION_EXPECTED = -15.94;
		assertEquals(ELEVATION_EXPECTED,
				commanding.calculateElevation(es5ec, position, now), 5);
	}

}