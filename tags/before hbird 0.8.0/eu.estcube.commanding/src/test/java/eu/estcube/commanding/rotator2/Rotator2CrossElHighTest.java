package eu.estcube.commanding.rotator2;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.hbird.exchange.navigation.PointingData;
import org.junit.Test;

import eu.estcube.commanding.analysis.OptimizationType;
import eu.estcube.commanding.optimizators.GlobalFunctions;
import eu.estcube.commanding.optimizators.Optimization;

public class Rotator2CrossElHighTest {
	public static List<PointingData> coordinatesResult = new ArrayList<PointingData>();
	public static List<PointingData> coordinatesExpected = new ArrayList<PointingData>();
	public static Rotator2 rotator2 = new Rotator2(7.5);

	@Test
	public void crossElHighNotInRange() {
		double[] azArrayResult = { 160.81, 160.73, 160.52, 159.96, 158.05,
				98.6, 347.04, 344.9, 344.31, 344.1, 344.04 };
		double[] elArrayResult = { 25.04, 33.89, 46.54, 64.87, 87.84, 66.47,
				47.6, 34.55, 25.45, 18.77, 13.59 };
		double[] azArrayExpected = { 340.0, 340.0, 340.0, 339.0, 338.0, 338.0,
				347.0, 344.0, 344.0, 344.0, 344.0 };
		double[] elArrayExpected = { 155.0, 147.0, 134.0, 116.0, 93.0, 66.0,
				47.0, 34.0, 25.0, 18.0, 13.0 };

		fillLists(azArrayResult, elArrayResult, azArrayExpected,
				elArrayExpected);

		Optimization optimizationCase = new Optimization(
				OptimizationType.ROT_2_CROSS_EL_HIGH, rotator2.getMaxAz(),
				rotator2.getMaxEl(), rotator2.getReceivingSector());

		coordinatesResult = optimizationCase.optimize(coordinatesResult);

		System.out.println("-------------");
		GlobalFunctions.printValues(coordinatesResult);
		System.out.println("-------------");
		GlobalFunctions.printValues(coordinatesExpected);
		System.out.println("-------------");

		for (int i = 0; i < coordinatesResult.size(); i++) {
			assertEquals(coordinatesResult.get(i).getAzimuth(),
					coordinatesExpected.get(i).getAzimuth());
			assertEquals(coordinatesResult.get(i).getElevation(),
					coordinatesExpected.get(i).getElevation());

		}
		
		coordinatesResult.clear();
		coordinatesExpected.clear();

	}

	@Test
	public void crossElHighInRange() {
		double[] azArrayResult = { 85.81, 85.73, 85.52, 84.96, 83.05, 263.6,
				263.04, 262.9, 261.31, 261.1, 260.04 };
		double[] elArrayResult = { 25.04, 33.89, 46.54, 64.87, 87.84, 66.47,
				47.6, 34.55, 25.45, 18.77, 13.59 };
		double[] azArrayExpected = { 445.0, 445.0, 445.0, 444.0, 443.0, 443.0,
				443.0, 442.0, 441.0, 441.0, 440.0 };
		double[] elArrayExpected = { 25.00, 33.0, 46.0, 64.0, 87.0, 114.0,
				133.0, 146.0, 155.0, 162.0, 167.0 };

		fillLists(azArrayResult, elArrayResult, azArrayExpected,
				elArrayExpected);

		Optimization optimizationCase = new Optimization(
				OptimizationType.ROT_2_CROSS_EL_HIGH, rotator2.getMaxAz(),
				rotator2.getMaxEl(), rotator2.getReceivingSector());

		coordinatesResult = optimizationCase.optimize(coordinatesResult);

		System.out.println("-------------");
		GlobalFunctions.printValues(coordinatesResult);
		System.out.println("-------------");
		GlobalFunctions.printValues(coordinatesExpected);
		System.out.println("-------------");

		for (int i = 0; i < coordinatesResult.size(); i++) {
			assertEquals(coordinatesResult.get(i).getAzimuth(),
					coordinatesExpected.get(i).getAzimuth());
			assertEquals(coordinatesResult.get(i).getElevation(),
					coordinatesExpected.get(i).getElevation());

		}

		coordinatesResult.clear();
		coordinatesExpected.clear();

	}

	public void fillLists(double[] azArrayResult, double[] elArrayResult,
			double[] azArrayExpected, double[] elArrayExpected) {
		for (int i = 0; i < azArrayResult.length; i++) {
			coordinatesExpected.add(new PointingData(0L, azArrayExpected[i],
					elArrayExpected[i], 123.0, 123.0, "test", "test"));
			coordinatesResult.add(new PointingData(0L, azArrayResult[i],
					elArrayResult[i], 123.0, 123.0, "test", "test"));
		}
	}

}
