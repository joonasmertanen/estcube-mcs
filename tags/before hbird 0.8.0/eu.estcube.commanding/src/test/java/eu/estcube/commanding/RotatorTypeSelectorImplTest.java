package eu.estcube.commanding;

import static org.junit.Assert.*;

import java.util.List;

import org.hbird.exchange.groundstation.NativeCommand;
import org.hbird.exchange.groundstation.Track;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import eu.estcube.commanding.api.RotatorTypeInterface;
import eu.estcube.commanding.calculation.RotatorTypeSelectorImpl;
import eu.estcube.commanding.rotator1.Rotator1;
import eu.estcube.commanding.rotator2.Rotator2;
import eu.estcube.gs.base.Rotator;

public class RotatorTypeSelectorImplTest {

	@Test
	public void test_type1() {
		RotatorTypeSelectorImpl selector=new RotatorTypeSelectorImpl();
		Rotator rotator = new Rotator("rot", "Rotator", 0, 0, 360,
				0, 180, "dummy") {
			@Override
			public List<NativeCommand> track(Track arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<NativeCommand> stop() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<NativeCommand> pointTo() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<NativeCommand> park() {
				// TODO Auto-generated method stub
				return null;
			}
		};
		RotatorTypeInterface type=selector.selectFor(rotator);
		assertEquals(type.getClass(),Rotator1.class);
		
	}
	
	@Test
	public void test_type2() {
		RotatorTypeSelectorImpl selector=new RotatorTypeSelectorImpl();
		Rotator rotator = new Rotator("rot", "Rotator", 0, 0, 360,
				0, 450, "dummy") {
			@Override
			public List<NativeCommand> track(Track arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<NativeCommand> stop() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<NativeCommand> pointTo() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<NativeCommand> park() {
				// TODO Auto-generated method stub
				return null;
			}
		};
		RotatorTypeInterface type=selector.selectFor(rotator);
		assertEquals(type.getClass(),Rotator2.class);
		
		
	}
	
	
	@Test
	public void test_type2AzFalse() {
		RotatorTypeSelectorImpl selector=new RotatorTypeSelectorImpl();
		Rotator rotator = new Rotator("rot", "Rotator", 0, 0, 180,
				0, 200, "dummy") {
			@Override
			public List<NativeCommand> track(Track arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<NativeCommand> stop() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<NativeCommand> pointTo() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<NativeCommand> park() {
				// TODO Auto-generated method stub
				return null;
			}
		};
		RotatorTypeInterface type=selector.selectFor(rotator);
		assertNotSame(type.getClass(),Rotator1.class);
		
		
	}
	
	@Test
	public void test_type2ElFalse() {
		RotatorTypeSelectorImpl selector=new RotatorTypeSelectorImpl();
		Rotator rotator = new Rotator("rot", "Rotator", 0, 0,50 ,
				0, 360, "dummy") {
			@Override
			public List<NativeCommand> track(Track arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<NativeCommand> stop() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<NativeCommand> pointTo() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<NativeCommand> park() {
				// TODO Auto-generated method stub
				return null;
			}
		};
		RotatorTypeInterface type=selector.selectFor(rotator);
		assertNotSame(type.getClass(),Rotator1.class);
		
		
	}


}
