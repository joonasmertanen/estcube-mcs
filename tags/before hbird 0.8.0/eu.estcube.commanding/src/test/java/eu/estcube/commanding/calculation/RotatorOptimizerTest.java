/** 
 *
 */
package eu.estcube.commanding.calculation;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.util.List;

import org.hbird.exchange.navigation.PointingData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.api.OptimizeInterface;
import eu.estcube.commanding.api.PassAnalyzeInterface;
import eu.estcube.commanding.api.RotatorTypeInterface;
import eu.estcube.commanding.api.RotatorTypeSelectorInterface;
import eu.estcube.gs.base.Rotator;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class RotatorOptimizerTest {

    private static final OverPassType OVER_PASS_TYPE = OverPassType.NO_CROSS_ELEVATION_HIGH;

    @Mock
    private PassAnalyzeInterface passAnalyser;

    @Mock
    private RotatorTypeSelectorInterface selctor;

    @Mock
    private Rotator rotator;

    @Mock
    private RotatorTypeInterface rotatorType;

    @Mock
    private OptimizeInterface realOptimizer;

    @Mock
    private List<PointingData> input;

    @Mock
    private List<PointingData> result;

    @InjectMocks
    private RotatorOptimizer optimizer;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        inOrder = inOrder(passAnalyser, selctor, rotator, rotatorType, realOptimizer, input, result);
        when(input.size()).thenReturn(RotatorOptimizer.INPUT_MIN_LENGTH);
        when(passAnalyser.calculateType(input)).thenReturn(OVER_PASS_TYPE);
        when(selctor.selectFor(rotator)).thenReturn(rotatorType);
        when(rotatorType.getOptimizer(OVER_PASS_TYPE)).thenReturn(realOptimizer);
        when(realOptimizer.optimize(input)).thenReturn(result);
    }

    /**
     * Test method for
     * {@link eu.estcube.commanding.calculation.RotatorOptimizer#doOptimize(java.util.List, eu.estcube.gs.base.Rotator)}
     * .
     */
    @Test
    public void testDoOptimize() {
        assertEquals(result, optimizer.doOptimize(input, rotator));
        inOrder.verify(input, times(1)).size();
        inOrder.verify(passAnalyser, times(1)).calculateType(input);
        inOrder.verify(selctor, times(1)).selectFor(rotator);
        inOrder.verify(rotatorType, times(1)).getOptimizer(OVER_PASS_TYPE);
        inOrder.verify(realOptimizer, times(1)).optimize(input);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.commanding.calculation.RotatorOptimizer#doOptimize(java.util.List, eu.estcube.gs.base.Rotator)}
     * .
     */
    @Test(expected = IllegalArgumentException.class)
    public void testDoOptimizeNullPointinData() {
        optimizer.doOptimize(null, rotator);
    }

    /**
     * Test method for
     * {@link eu.estcube.commanding.calculation.RotatorOptimizer#doOptimize(java.util.List, eu.estcube.gs.base.Rotator)}
     * .
     */
    @Test(expected = IllegalArgumentException.class)
    public void testDoOptimizePointinDataToShort() {
        when(input.size()).thenReturn(RotatorOptimizer.INPUT_MIN_LENGTH - 1);
        optimizer.doOptimize(input, rotator);
    }

    /**
     * Test method for
     * {@link eu.estcube.commanding.calculation.RotatorOptimizer#doOptimize(java.util.List, eu.estcube.gs.base.Rotator)}
     * .
     */
    @Test(expected = IllegalArgumentException.class)
    public void testDoOptimizeNullRotator() {
        optimizer.doOptimize(input, null);
    }
}
