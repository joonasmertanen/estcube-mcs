package eu.estcube.commanding.rotator1;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.hbird.exchange.navigation.PointingData;
import org.junit.Test;

import eu.estcube.commanding.analysis.OptimizationType;
import eu.estcube.commanding.optimizators.GlobalFunctions;
import eu.estcube.commanding.optimizators.Optimization;

public class Rotator1CrossElHighTest {
	public static List<PointingData> coordinatesResult = new ArrayList<PointingData>();
	public static List<PointingData> coordinatesExpected = new ArrayList<PointingData>();

	@Test
	public void crossElhigh() {
		double[] azArrayResult = { 160.81, 160.73, 160.52, 159.96, 158.05,
				98.6, 347.04, 344.9, 344.31, 344.1, 344.04 };
		double[] elArrayResult = { 25.04, 33.89, 46.54, 64.87, 87.84, 66.47,
				47.6, 34.55, 25.45, 18.77, 13.59 };
		double[] azArrayExpected = { 340.0, 340.0, 340.0, 339.0, 338.0, 338.0,
				347.0, 344.0, 344.0, 344.0, 344.0 };
		double[] elArrayExpected = { 155.0, 147.0, 134.0, 116.0, 93.0, 66.0,
				47.0, 34.0, 25.0, 18.0, 13.0 };

		fillLists(azArrayResult, elArrayResult, azArrayExpected,
				elArrayExpected);

		Optimization optimizationCase = new Optimization(
				OptimizationType.ROT_1_CROSS_EL_HIGH, 360, 180, 7.5);

		coordinatesResult = optimizationCase.optimize(coordinatesResult);

		GlobalFunctions.printValues(coordinatesResult);
		System.out.println("-------------");
		GlobalFunctions.printValues(coordinatesExpected);
		for(int i=0;i<coordinatesResult.size();i++){
			assertEquals(coordinatesResult.get(i).getAzimuth(), coordinatesExpected.get(i).getAzimuth());
			assertEquals(coordinatesResult.get(i).getElevation(), coordinatesExpected.get(i).getElevation());
			
		}
		
		coordinatesResult.clear();
		coordinatesExpected.clear();

		

	}

	public void fillLists(double[] azArrayResult, double[] elArrayResult,
			double[] azArrayExpected, double[] elArrayExpected) {
		for (int i = 0; i < azArrayResult.length; i++) {
			coordinatesExpected.add(new PointingData(0L, azArrayExpected[i],
					elArrayExpected[i], 123.0, 123.0, "test", "test"));
			coordinatesResult.add(new PointingData(0L, azArrayResult[i],
					elArrayResult[i], 123.0, 123.0, "test", "test"));
		}

	}
}
