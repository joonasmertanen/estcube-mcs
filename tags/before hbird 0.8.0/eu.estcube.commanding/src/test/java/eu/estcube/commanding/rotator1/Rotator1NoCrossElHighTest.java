package eu.estcube.commanding.rotator1;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hbird.exchange.navigation.PointingData;
import org.junit.Test;

import com.google.common.primitives.Doubles;

import eu.estcube.commanding.analysis.OptimizationType;
import eu.estcube.commanding.optimizators.GlobalFunctions;
import eu.estcube.commanding.optimizators.Optimization;
import eu.estcube.commanding.rotator1.Rotator1;

public class Rotator1NoCrossElHighTest {
	public static List<PointingData> coordinatesResult = new ArrayList<PointingData>();
	public static List<PointingData> coordinatesExpected = new ArrayList<PointingData>();
	public static Rotator1 rotator1 = new Rotator1(7.5);

	@Test
	public void NoCrossElHigh() {
		double[] azArrayResult = { 317.82, 316.51, 311.02, 164.43, 146.0,
				144.55, 143.8 };
		double[] elArrayResult = { 58.71, 75.77, 83.72, 64.86, 48.82, 36.39,
				19.41, 13.35, 8.28 };
		double[] azArrayExpected = { 317.0, 316.0, 311.0, 326.0, 326.0,
				324.0, 323.0 };
		double[] elArrayExpected = { 58.0, 75.0, 83.0, 116.0, 132.0,
				144.0, 161.0 };

		fillLists( azArrayResult, elArrayResult,azArrayExpected,
				elArrayExpected);

		Optimization optimizationCase = new Optimization(
				OptimizationType.ROT_1_NO_CROSS_EL_HIGH, rotator1.getMaxAz(),
				rotator1.getMaxEl(), rotator1.getReceivingSector());

		coordinatesResult = optimizationCase.optimize(coordinatesResult);

		GlobalFunctions.printValues(coordinatesResult);
		System.out.println("-------------");
		GlobalFunctions.printValues(coordinatesExpected);

		for(int i=0;i<coordinatesResult.size();i++){
			assertEquals(coordinatesResult.get(i).getAzimuth(), coordinatesExpected.get(i).getAzimuth());
			assertEquals(coordinatesResult.get(i).getElevation(), coordinatesExpected.get(i).getElevation());
			
		}
		coordinatesResult.clear();
		coordinatesExpected.clear();

	}

	public void fillLists(double[] azArrayResult, double[] elArrayResult,
			double[] azArrayExpected, double[] elArrayExpected) {
		for (int i = 0; i < azArrayResult.length; i++) {
			coordinatesExpected.add(new PointingData(0L, azArrayExpected[i],
					elArrayExpected[i], 123.0, 123.0, "test", "test"));
			coordinatesResult.add(new PointingData(0L, azArrayResult[i],
					elArrayResult[i], 123.0, 123.0, "test", "test"));
		}

	}
}
