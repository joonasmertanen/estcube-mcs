package eu.estcube.webserverpublic;

import java.net.InetSocketAddress;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.hbird.exchange.constants.StandardArguments;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import eu.estcube.webserverpublic.radiobeacon.RadioBeaconSendEmailServlet;
import eu.estcube.webserverpublic.radiobeacon.RadioBeaconTranslationServlet;

public class WebServerPublic extends RouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(WebServerPublic.class);

    @Value("${service.id}")
    private String serviceId;

    @Value("${service.version}")
    private String webServerVersion;

    @Value("${web.server.host}")
    private String webServerHost;

    @Value("${web.server.port}")
    private int webServerPort;

    @Autowired
    private RadioBeaconSendEmailServlet radioBeaconSendEmailServlet;

    @Autowired
    private RadioBeaconTranslationServlet radioBeaconTranslationServlet;

    @Override
    public void configure() throws Exception {
        LOG.info("Configuring WebServer");
        MDC.put(StandardArguments.ISSUED_BY, serviceId);

        InetSocketAddress address = new InetSocketAddress(webServerHost, webServerPort);
        Server server = new Server(address);

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
        context.setContextPath("/");

        context.addServlet(new ServletHolder(radioBeaconSendEmailServlet), "/radiobeacon/sendEmail");
        context.addServlet(new ServletHolder(radioBeaconTranslationServlet), "/radiobeacon/translate");

        server.setHandler(context);
        server.start();
    }

    public static void main(String[] args) throws Exception {
        LOG.info("Starting WebServer");
        try {
            new Main().run(args);
        } catch (Exception e) {
            LOG.error("Failed to start " + WebServerPublic.class.getName(), e);
        }
    }
}
