package eu.estcube.webserverpublic.radiobeacon;

import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.camel.EndpointInject;
import org.apache.camel.Handler;
import org.apache.camel.ProducerTemplate;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Component
public class RadioBeaconSendEmailServlet extends HttpServlet {

    @EndpointInject(uri = "direct:radioBeaconInput")
    ProducerTemplate producer;

    @Value("${radiobeacon.to}")
    String toEmail;

    @Value("${radiobeacon.from}")
    String fromEmail;

    @Value("${radiobeacon.host}")
    String host;

    @Value("${radiobeacon.port}")
    String port;

    @Value("${radiobeacon.auth_user}")
    String authUser;

    @Value("${radiobeacon.auth_pw}")
    String authPw;

    @Value("${radiobeacon.check_value}")
    String checkValue;

    class RadioBeacon {
        protected Set<String> radioBeaconMessage = new HashSet<String>();
    }

    @Override
    @Handler
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String datetime = request.getParameter("datetime").trim();
        String source = request.getParameter("source").trim();
        String data = request.getParameter("data").trim();
        String check = request.getParameter("check").trim();

        JSONObject json = new JSONObject();

        try {
            if (check.equals(checkValue) == false) {
                json.put("status", "error");
                json.put("message", "Invalid check value!");
            } else {
                try {
                    sendEmail("Beacon data: " + datetime + " " + source + "",
                            buildMessage(data, datetime, source));

                    json.put("status", "ok");
                    json.put("message", "Message sent!");

                } catch (Exception e) {
                    json.put("status", "error");
                    json.put("message", "Message could not be sent: " + e.getMessage());
                }
            }

        } catch (JSONException e1) {
            throw new ServletException(e1);
        }

        response.getWriter().write(json.toString());

    }

    public String buildMessage(String data, String datetime, String source) throws Exception {
        return data + ";" + source + ";" + datetime;
    }

    public Boolean sendEmail(String title, String content) throws AddressException, MessagingException {

        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", host);
        properties.setProperty("mail.smtp.port", port);
        properties.setProperty("mail.user", authUser);
        properties.setProperty("mail.password", authPw);

        Session session = Session.getDefaultInstance(properties);

        MimeMessage message = new MimeMessage(session);

        message.setFrom(new InternetAddress(fromEmail));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
        message.setSubject(title);
        message.setText(content);

        Transport.send(message);

        return true;

    }
}
