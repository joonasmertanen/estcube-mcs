package eu.estcube.gs.hamlib;

import org.springframework.beans.factory.annotation.Value;

import eu.estcube.domain.config.GroundStationConfigurationBase;

/**
 *
 */
public class HamlibDriverConfiguration extends GroundStationConfigurationBase {

    @Value("${device.name}")
    private String deviceName;

    @Value("${device.type}")
    private String deviceType;

    @Value("${device.port}")
    private int devicePort;

    @Value("${device.host}")
    private String deviceHost;
    
    @Value("${timer.fire.interval}")
    private Long timerFireInterval;

    @Value("${poll.delay.interval}")
    private int pollDelayInterval;

    @Value("${poll.round.scale}")
    private int pollRoundingScale;
    
    public String getDeviceName() {
        return deviceName;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public int getDevicePort() {
        return devicePort;
    }

    public String getDeviceHost() {
        return deviceHost;
    }

    public Long getTimerFireInterval() {
        return timerFireInterval;
    }

    public int getPollDelayInterval() {
        return pollDelayInterval;
    }

    public int getPollRoundingScale() {
        return pollRoundingScale;
    }

    public String getAddress() {
        return deviceHost + ":" + devicePort;
    }

    public String getComponentId() {
        return deviceType + "_" + getGroundstationId() + "_" + deviceName;
    }
}
