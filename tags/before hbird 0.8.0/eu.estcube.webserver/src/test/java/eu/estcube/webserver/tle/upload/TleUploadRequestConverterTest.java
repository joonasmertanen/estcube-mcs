package eu.estcube.webserver.tle.upload;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import org.hbird.exchange.navigation.TleOrbitalParameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.webserver.domain.TleUploadRequest;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TleUploadRequestConverterTest {

    public static final String SAT = "ESTCube-X";
    public static final String SOURCE = "inbox";
    public static final String LINE_1 = "1: 1 2 3";
    public static final String LINE_2 = "2: 4 5 6";
    public static final String TLE = " " + LINE_1 + " \n " + LINE_2 + " \n ";
    public static final long NOW = System.currentTimeMillis();

    private TleUploadRequestConverter converter;

    @Mock
    private TleUploadRequest req;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        converter = new TleUploadRequestConverter();
        inOrder = inOrder(req);
        when(req.getSatellite()).thenReturn(SAT);
        when(req.getTimestamp()).thenReturn(NOW);
        when(req.getTleSource()).thenReturn(SOURCE);
        when(req.getTleText()).thenReturn(TLE);
    }

    /**
     * Test method for
     * {@link eu.estcube.webserver.tle.upload.TleUploadRequestConverter#convert(eu.estcube.webserver.domain.TleUploadRequest)}
     * .
     */
    @Test
    public void testConvert() {
        TleOrbitalParameters request = converter.convert(req);
        assertNotNull(request);
        assertEquals(NOW, request.getTimestamp());
        assertNotNull(request.getName());
        assertEquals(SOURCE, request.getIssuedBy());
        assertEquals(SAT, request.getSatelliteName());

        assertEquals(LINE_1, request.getTleLine1());
        assertEquals(LINE_2, request.getTleLine2());
        assertEquals(SOURCE, request.getIssuedBy());
        assertEquals(SAT, request.getSatelliteName());

        assertNotNull(request.getID());
        assertNotNull(request.getDescription());

        inOrder.verify(req, times(1)).getSatellite();
        inOrder.verify(req, times(1)).getTimestamp();
        inOrder.verify(req, times(1)).getTleSource();
        inOrder.verify(req, times(1)).getTleText();
        inOrder.verifyNoMoreInteractions();
    }
}
