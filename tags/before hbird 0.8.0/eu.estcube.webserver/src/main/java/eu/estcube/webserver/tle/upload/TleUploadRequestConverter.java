package eu.estcube.webserver.tle.upload;

import org.apache.camel.Body;
import org.hbird.exchange.dataaccess.TlePropagationRequest;
import org.hbird.exchange.navigation.TleOrbitalParameters;
import org.springframework.stereotype.Component;

import eu.estcube.common.Naming;
import eu.estcube.common.Naming.Base;
import eu.estcube.webserver.domain.TleUploadRequest;

/**
 * Converts {@link TleUploadRequest} to {@link TleOrbitalParameters}.
 */
@Component
public class TleUploadRequestConverter {

    /** TLE parameter name. */
    public static final String PARAMTER_TLE = "TLE";

    /** TLE description. */
    public static final String DESCRIPTION_TLE = "Two Line Element to describe the orbit.";

    /**
     * Converts {@link TleUploadRequest} to {@link TlePropagationRequest}.
     * 
     * @param request
     *        {@link TleUploadRequest} to convert
     * @return new instance of {@link TlePropagationRequest}
     */
    public TleOrbitalParameters convert(@Body TleUploadRequest request) {
        String satellite = request.getSatellite();
        Long timestamp = request.getTimestamp();
        String issuedBy = request.getTleSource();
        String[] lines = request.getTleText().trim().split("\n");
        String name = Naming.createParameterAbsoluteName(Base.SATELLITE, satellite, PARAMTER_TLE);
        TleOrbitalParameters top = new TleOrbitalParameters(issuedBy, name,
                DESCRIPTION_TLE, timestamp, satellite, lines[0].trim(), lines[1].trim());
        return top;
    }
}
