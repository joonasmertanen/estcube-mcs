package eu.estcube.webserver;

import org.apache.camel.Body;
import org.hbird.business.api.IPublish;
import org.hbird.exchange.core.Named;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class NamedObjectPublisher {

    @Autowired
    private IPublish publisher;

    public Named publishAndCommit(@Body Named named) {
        publisher.publish(named);
        publisher.commit();
        return named;
    }
}
