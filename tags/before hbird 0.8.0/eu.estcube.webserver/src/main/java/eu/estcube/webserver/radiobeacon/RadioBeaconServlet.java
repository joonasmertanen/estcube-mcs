package eu.estcube.webserver.radiobeacon;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hbird.exchange.core.Issued;
import org.springframework.stereotype.Component;

import eu.estcube.codec.radiobeacon.RadioBeaconDateInputParser;
import eu.estcube.codec.radiobeacon.RadioBeaconTranslator;

@SuppressWarnings("serial")
@Component
public class RadioBeaconServlet extends HttpServlet {

    @EndpointInject(uri = "direct:radioBeaconInput")
    ProducerTemplate producer;

    class RadioBeacon {
        protected Set<String> radioBeaconMessage = new HashSet<String>();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String datetime = request.getParameter("datetime").trim();
        String source = request.getParameter("source").trim();
        String data = request.getParameter("data").trim();

        JSONObject json = new JSONObject();

        try {
            try {
                RadioBeaconTranslator translator = new RadioBeaconTranslator();
                HashMap<String, Issued> parameters = translator.toParameters(data,
                        new RadioBeaconDateInputParser().parse(datetime), source);

                for (Issued key : parameters.values()) {
                    producer.sendBody(key);
                }

                json.put("status", "ok");
                json.put("message", "Data sent!");

            } catch (Exception e) {
                json.put("status", "error");
                json.put("message", "There were problems with parsing the data! Nothing sent! " + e.getMessage());
            }
        } catch (JSONException e1) {
            throw new ServletException(e1);
        }

        response.getWriter().write(json.toString());

    }
}
