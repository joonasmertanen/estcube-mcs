package eu.estcube.webserver.cache;

import org.hbird.exchange.interfaces.IIssued;
import org.hbird.exchange.interfaces.INamed;

public class NamedCache<T extends INamed> extends AbstractCache<String, T> {

    /**
     * Creates new NamedCache.
     * 
     * @param store
     */
    public NamedCache(Cache<String, T> store) {
        super(store);
    }

    /** @{inheritDoc . */
    @Override
    protected boolean shouldReplace(INamed oldValue, INamed newValue) {

        // TODO - 11.04.2013; kimmell - check if comparing ID-s would be good
        // enough

        if (oldValue instanceof IIssued && newValue instanceof IIssued) {
            IIssued oldIssued = (IIssued) oldValue;
            IIssued newIssued = (IIssued) newValue;
            return oldIssued.getTimestamp() <= newIssued.getTimestamp();
        } else {
            // we have INamed
            return true;
        }

    }

    /** @{inheritDoc . */
    @Override
    protected String getKey(T named) {
        return named.getQualifiedName();
    }
}