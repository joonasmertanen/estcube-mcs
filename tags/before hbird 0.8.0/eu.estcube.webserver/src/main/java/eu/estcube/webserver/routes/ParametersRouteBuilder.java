/** 
 *
 */
package eu.estcube.webserver.routes;

import org.hbird.exchange.interfaces.INamed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.common.json.ToJsonProcessor;
import eu.estcube.webserver.cache.Cache;
import eu.estcube.webserver.cache.NamedCache;
import eu.estcube.webserver.cache.guava.GuavaTimeoutStore;

/**
 *
 */
@Component
public class ParametersRouteBuilder extends WebsocketWithCacheRouteBuilder {

    public static final String WEBSOCKET = "websocket://hbird.out.parameters";

    // TODO - 07.04.2013; kimmell - 10 minutes time out is probably too short
    // for the parameters
    public static final long CACHE_TIMEOUT_FOR_NAMED_OBJECTS = 1000L * 60 * 10;

    private final Cache<String, INamed> cache = new NamedCache<INamed>(new GuavaTimeoutStore<String, INamed>(
            CACHE_TIMEOUT_FOR_NAMED_OBJECTS));

    @Autowired
    private ToJsonProcessor toJson;

    /** @{inheritDoc . */
    @Override
    protected Cache<?, ?> getCache() {
        return cache;
    }

    /** @{inheritDoc . */
    @Override
    protected String getSource() {
        return WebserverMonitoringDispatcher.DESTINATION_PARAMETERS;
    }

    /** @{inheritDoc . */
    @Override
    protected String getDestination() {
        return WEBSOCKET;
    }

    /** @{inheritDoc . */
    @Override
    protected Object getSerializer() {
        return toJson;
    }
}
