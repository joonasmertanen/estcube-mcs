/** 
 *
 */
package eu.estcube.webserver.catalogue;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hbird.business.api.ApiFactory;
import org.hbird.business.api.IDataAccess;
import org.hbird.exchange.navigation.LocationContactEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.common.json.ToJsonProcessor;
import eu.estcube.webserver.utils.HttpResponseSupport;

/**
 *
 */
@Component
public class SatellitesServlet extends HttpServlet {

    private static final long serialVersionUID = -2655050951676720703L;

    private static final Logger LOG = LoggerFactory.getLogger(SatellitesServlet.class);

    // @Autowired
    // private ICatalogue catalogue;

    @Autowired
    private ToJsonProcessor toJson;

    @Autowired
    private HttpResponseSupport responseSupport;

    /** @{inheritDoc . */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            // LOG.debug("Request pathInfo: {}", req.getPathInfo());
            // LOG.debug("Request pathTranslated: {}", req.getPathTranslated());
            // LOG.debug("Request requestURI {}", req.getRequestURI());
            // LOG.debug("Request requestURL {}", req.getRequestURL());

            IDataAccess dao = ApiFactory.getDataAccessApi("webserver");

            long now = System.currentTimeMillis();
            long delta = 1000L * 60 * 60 * 24 * 365;

            List<LocationContactEvent> events = dao.retrieveNextLocationContactEventsFor("ES5EC", now - delta);
            responseSupport.sendAsJson(resp, toJson, events);

            // List<OrbitalState> list = dao.retrieveOrbitalStatesFor("RAX-2",
            // now - delta, now + delta);
            // responseSupport.sendAsJson(resp, toJson, list);

            // ICatalogue catalogue = ApiFactory.getCatalogueApi("webserver");
            // // List<Command> commands = catalogue.getCommands();
            // // responseSupport.sendAsJson(resp, toJson, commands);
            //
            // List<Satellite> satellites = catalogue.getSatellites();
            // responseSupport.sendAsJson(resp, toJson, satellites);
        } catch (Exception e) {
            String message = "Failed to process Satellites request";
            LOG.error(message, e);
            throw new ServletException(message, e);
        }
    }
}
