/** 
 *
 */
package eu.estcube.webserver.cache;

import org.hbird.exchange.interfaces.INamed;

/**
 * 
 */
public class NamedIdCache<T extends INamed> extends AbstractCache<String, T> {

    /**
     * Creates new NamedIdCache.
     * 
     * @param store
     */
    public NamedIdCache(Cache<String, T> store) {
        super(store);
    }

    /** @{inheritDoc . */
    @Override
    protected String getKey(T named) {
        return named.getID();
    }
}
