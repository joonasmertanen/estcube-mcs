/** 
 *
 */
package eu.estcube.webserver.routes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.common.json.ToJsonProcessor;

/**
 *
 */
@Component
public class WebcamRouteBuilder extends WebsocketRouteBuilder {

    public static final String SOURCE = "activemq:topic:webcamSend";
    public static final String WEBSOCKET = "websocket://hbird.out.binary";

    @Autowired
    private ToJsonProcessor toJson;

    /** @{inheritDoc . */
    @Override
    protected String getSource() {
        return SOURCE;
    }

    /** @{inheritDoc . */
    @Override
    protected String getDestination() {
        return WEBSOCKET;
    }

    /** @{inheritDoc . */
    @Override
    protected Object getSerializer() {
        return toJson;
    }
}
