package eu.estcube.domain;

public class JMSConstants {
        public static final String gsRecive = "activemq:topic:gsReceive";
        public static final String gsSend = "activemq:topic:gsSend";
        public static final String psQueue = "activemq:queue:psQueue";
        
    public JMSConstants() {

    }
}
