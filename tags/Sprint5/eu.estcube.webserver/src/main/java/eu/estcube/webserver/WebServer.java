/**
 * 
 */
package eu.estcube.webserver;

import java.net.InetSocketAddress;
import java.util.Map;

import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.apache.camel.component.websocket.*;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.SessionManager;
import org.eclipse.jetty.server.session.HashSessionManager;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import eu.estcube.domain.JMSConstants;
import eu.estcube.common.*;

public class WebServer extends RouteBuilder{
    
    private static final Logger LOG = LoggerFactory.getLogger(WebServer.class);
    @Value("${webSocketAdress}")
    private String webSocketAdress;
    
    @Value("${webSocketCacheReqAdress}")
    private String webSocketCacheReqAdress;
    
    @Value("${webSocketPort}")
    private int webSocketPort;
    
    @Value("${staticResources}")
    private String staticResources;

    @Autowired
    private JsonToTelemetryCommand jsonToTelemetryCommand;
    @Autowired
    private TelemetryObjectToJson telemetryObjectToJson;  
    private ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
    
    @Override
    public void configure() throws Exception {
/*-   This server prevents .js files from being cached.
        InetSocketAddress address = new InetSocketAddress("localhost", 9393);
        Server server = new Server(address);

        context.setContextPath("/");

        SessionManager sm = new HashSessionManager();
        SessionHandler sh = new SessionHandler(sm);
        context.setSessionHandler(sh);

        context.setResourceBase("../eu.estcube.webgui/src/public/");
        DefaultServlet defaultServlet = new DefaultServlet();
        ServletHolder holder = new ServletHolder(defaultServlet);

        holder.setInitParameter("useFileMappedBuffer", "false");
        holder.setInitParameter("cacheControl", "no-store,no-cache,must-revalidate");
        holder.setInitParameter("maxCachedFiles", "0");
        context.addServlet(holder, "/");

        server.setHandler(context);
        server.start();
-*/
        WebsocketComponent websocketComponent = (WebsocketComponent) getContext().getComponent("websocket");

        websocketComponent.setPort(webSocketPort);
        websocketComponent.setStaticResources(staticResources);  

        Endpoint websocket = websocketComponent.createEndpoint(webSocketAdress); 
        Endpoint websocketCacheRequest = websocketComponent.createEndpoint(webSocketCacheReqAdress);

            from("timer://status?repeatCount=1").to(JMSConstants.psQueue);
        
            from(websocketCacheRequest)
                .bean(CacheMessage.class, "getCache")
                    .split().method("telObjSplitter", "splitMessage")
                        .process(telemetryObjectToJson)
                            .setHeader(WebsocketConstants.SEND_TO_ALL, constant(true))
                                .to(websocketCacheRequest);
        
            from(websocket).process(jsonToTelemetryCommand)
        	//.to(websocket);  // for GUI testing
            .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    exchange.getIn().setHeader("groundStationID", "ES5EC");
                    exchange.getIn().setHeader("device", "rotctld");
                }
             })        	
        	.to(JMSConstants.gsRecive); // real route
        	
       
//        from("websocket://cache").bean(CacheMessage.class, "getCache")
//        	.to("websocket://cache");
     
        		
        from("stream:in")
            .choice()
                .when(body().isEqualTo("ps"))
                    .process(new Processor() {
                        public void process(Exchange exchange) throws Exception {
                            exchange.getOut().setBody("Request parameters.");
                        }
                    }).to(JMSConstants.psQueue)
                .when(body().isEqualTo("cache"))
                    .bean(CacheMessage.class, "getCache").split().method("telObjSplitter", "splitMessage").process(telemetryObjectToJson).to("log:echo");
                                
        from(JMSConstants.gsSend).bean(CacheMessage.class, "addToCache").process(telemetryObjectToJson)
        .setHeader(WebsocketConstants.SEND_TO_ALL, constant(true))
            .to(websocket)
            .to("log:echo");

        
    }
    
    public static void main(String[] args) throws Exception { 
        LOG.info("Starting WebServer");
        new Main().run(args);
    }
}
