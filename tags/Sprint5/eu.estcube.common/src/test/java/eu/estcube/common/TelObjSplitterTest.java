package eu.estcube.common;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Message;
import org.junit.Before;
import org.junit.Test;
import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryParameter;

public class TelObjSplitterTest {
    private TelObjSplitter telObjSplitter;
    private ArrayList<TelemetryObject> telemetryObjects;
    private TelemetryObject telemetryObject;
    private TelemetryObject telemetryObject2;
    private String telObjName = "telObjName";
    private String telObjName2 = "telObjName2";
    private TelemetryParameter telemetryParameter;
    private TelemetryParameter telemetryParameter2;
    private String telParName = "telParName";
    private String telParName2 = "telParName2";
    private String value = "123";
    private String value2 = "someValue 5";

    @Before
    public void setUp() {
        telObjSplitter = new TelObjSplitter();
        telemetryObject = new TelemetryObject(telObjName);
        telemetryObject2 = new TelemetryObject(telObjName2);

        telemetryParameter = new TelemetryParameter(telParName, value);
        telemetryParameter2 = new TelemetryParameter(telParName2, value2);

        telemetryObject.addParameter(telemetryParameter);
        telemetryObject.addParameter(telemetryParameter2);
        telemetryObject2.addParameter(telemetryParameter2);

        telemetryObjects = new ArrayList<TelemetryObject>();
        telemetryObjects.add(telemetryObject);
        telemetryObjects.add(telemetryObject2);
    }

    @Test
    public void testSplitter() {
        List<Message> messages = telObjSplitter.splitMessage(telemetryObjects);
        assertEquals(messages.size(), telemetryObjects.size());

        for (int i = 0; i < telemetryObjects.size(); i++) {
            assertEquals(messages.get(i).getBody(TelemetryObject.class), telemetryObjects.get(i));
        }
    }
}
