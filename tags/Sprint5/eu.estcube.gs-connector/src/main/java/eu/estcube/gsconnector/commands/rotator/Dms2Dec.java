package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class Dms2Dec extends CommandStringBuilder {
    private StringBuilder messageString;

    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString = new StringBuilder();

        messageString.append("+");
        messageString.append("D");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Degrees"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Minutes"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Seconds"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("S/W"));
        messageString.append("\n");

        return messageString;
    }
}
