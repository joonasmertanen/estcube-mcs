package eu.estcube.gsconnector;

import java.util.ArrayList;
import java.util.List;
import org.apache.camel.Body;
import org.apache.camel.Message;
import org.apache.camel.impl.DefaultMessage;
import org.springframework.stereotype.Component;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryRadioConstants;

@Component
public class RadioStatusSplitter {

    private List<String> commands;

    public RadioStatusSplitter() {
        this.commands = new ArrayList<String>();
        commands.add(TelemetryRadioConstants.GET_ANTENNA_NR);
        commands.add(TelemetryRadioConstants.GET_CTCSS_TONE);
        commands.add(TelemetryRadioConstants.GET_DCS_CODE);
        commands.add(TelemetryRadioConstants.GET_FREQUENCY);
        commands.add(TelemetryRadioConstants.GET_MEMORY_CHANNELNR);
        commands.add(TelemetryRadioConstants.GET_MODE);
        commands.add(TelemetryRadioConstants.GET_PTT);
        commands.add(TelemetryRadioConstants.GET_RIT);
        commands.add(TelemetryRadioConstants.GET_RPTR_OFFSET);
        commands.add(TelemetryRadioConstants.GET_RPTR_SHIFT);
        commands.add(TelemetryRadioConstants.GET_SPLIT_VFO);
        commands.add(TelemetryRadioConstants.GET_TRANCEIVE_MODE);
        commands.add(TelemetryRadioConstants.GET_TRANSMIT_FREQUENCY);
        commands.add(TelemetryRadioConstants.GET_TRANSMIT_MODE);
        commands.add(TelemetryRadioConstants.GET_TUNING_STEP);
        commands.add(TelemetryRadioConstants.GET_VFO);
        commands.add(TelemetryRadioConstants.GET_XIT);
    }

    public List<Message> splitMessage() {
        List<Message> answer = new ArrayList<Message>();

        for (int i = 0; i < commands.size(); i++) {
            DefaultMessage message = new DefaultMessage();
            message.setBody(new TelemetryCommand(commands.get(i)));
            answer.add(message);
        }
        return answer;
    }
}