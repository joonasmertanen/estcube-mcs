package eu.estcube.gsconnector.commands;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoder;

public class CapabilitiesTest {
    
    
    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand = new TelemetryCommand("CAPABILITIES");
    private Capabilities createMessage = new Capabilities();
    private static final Logger LOG = LoggerFactory.getLogger(CapabilitiesTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @Test
    public void testCreateMessageString() {
        string.append("+1\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
