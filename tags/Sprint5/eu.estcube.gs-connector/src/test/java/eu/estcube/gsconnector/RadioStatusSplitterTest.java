package eu.estcube.gsconnector;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.camel.CamelContext;
import org.apache.camel.Message;
import org.apache.camel.impl.DefaultExchange;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class RadioStatusSplitterTest {
    
    private static final Logger LOG = LoggerFactory.getLogger(StoreSetValuesTest.class);
    private RadioStatusSplitter radioStatusSplitter;
    private DefaultExchange exchange;
    private CamelContext ctx;
    
    @Before
    public void setUp() throws Exception {        
        radioStatusSplitter = new RadioStatusSplitter();
    }

    @Test
    public void testSplitMessage() {
        List<Message> answer = radioStatusSplitter.splitMessage();
        assertTrue(answer.size()== 17);
    }


}
