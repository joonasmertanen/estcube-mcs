package eu.estcube.gsconnector;


import static org.junit.Assert.assertTrue;

import java.util.List;

import org.apache.camel.CamelContext;
import org.apache.camel.Message;
import org.apache.camel.impl.DefaultExchange;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RotatorStatusSplitterTest {

    private static final Logger LOG = LoggerFactory.getLogger(StoreSetValuesTest.class);
    private RotatorStatusSplitter rotatorStatusSplitter;
    private DefaultExchange exchange;
    private CamelContext ctx;
    
    @Before
    public void setUp() throws Exception {        
        rotatorStatusSplitter = new RotatorStatusSplitter();
    }

    @Test
    public void testSplitMessage() {
        List<Message> answer = rotatorStatusSplitter.splitMessage();
        assertTrue(answer.size()== 1);
    }

}
