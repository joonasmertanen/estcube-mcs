package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class ConvertMW2PowerTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private ConvertMW2Power createMessage = new ConvertMW2Power();
    private static final Logger LOG = LoggerFactory.getLogger(ConvertMW2PowerTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("CONVERT_MW2POWER");
        telemetryCommand.addParameter("Power mW", 0);
        telemetryCommand.addParameter("Frequency", 0);
        telemetryCommand.addParameter("Mode", "..");
        string.append("+4 0 0 ..\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
