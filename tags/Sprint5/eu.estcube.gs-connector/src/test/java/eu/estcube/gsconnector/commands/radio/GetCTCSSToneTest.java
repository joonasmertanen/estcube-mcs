package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class GetCTCSSToneTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private GetCTCSSTone createMessage = new GetCTCSSTone();
    private static final Logger LOG = LoggerFactory.getLogger(GetCTCSSToneTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        
    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("GET_CTCSS_TONE");
        string.append("+c\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
