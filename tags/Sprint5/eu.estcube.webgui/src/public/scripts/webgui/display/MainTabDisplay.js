dojo.provide("webgui.display.MainTabDisplay");

dojo.require("dijit.layout.TabContainer");
dojo.require("dijit.layout.StackContainer");
dojo.require("dijit.layout.ContentPane");
dojo.require("dojox.charting.Chart2D");
dojo.require("dojox.charting.themes.Claro");			
dojo.require("dojo.store.Observable");
dojo.require("dojo.store.Memory");
dojo.require("dojox.charting.StoreSeries");
dojo.require("dojox.charting.action2d.Tooltip");
dojo.require("dojox.charting.action2d.MoveSlice");

dojo.require("dojox.charting.widget.Legend");
dojo.require("dojox.charting.action2d.Tooltip");
dojo.require("dojox.charting.action2d.Magnify");

dojo.declare("webgui.display.MainTabDisplay", null, {

	constructor : function(view) {

		console.log("TabDisplay");

		var data = [
			{ id: 1, value: 0, parameter: "Azimuth" },
			{ id: 2, value: 0, parameter: "Elevation" }
			];
		
		var telemetryTab = new dijit.layout.ContentPane({
			id: "chartTab",
			title : "Telemetry",
			preventCache: true
				
		});

		var gsTab = new dijit.layout.ContentPane({
			title : "GS",
			content : "Groundstation online!",
			closable : "true",
			preventCache: true
		});

		var commandTab = new dijit.layout.ContentPane({
			id : "commandTab", // fix error
			title : "Command",
			content : "Insert Commands",
			preventCache: true
		});

		var tabs = new dijit.layout.TabContainer({
			region : "center",
			tabPosition : "bottom",
			id : "mainTabContainer",
			preventCache: true
		});

		var tempCont;
		tempCont = dijit.byId("CenterContainer");

		tabs.addChild(telemetryTab);
		tabs.addChild(gsTab);
		tabs.addChild(commandTab);
		tempCont.addChild(tabs);
		tempCont.selectChild(tabs);
		tabs.startup();
		
		var chartArea = dojo.create("div", {
			id : "chartArea",
			region : "center",
			width: 600,
			height: 600
		}, "chartTab");	
		

		
		var store = dojo.store.Observable(new dojo.store.Memory({
			data: {
				identifier: "id",
				items: data
			}
		}));
		
	    var chart = new dojox.charting.Chart2D("chartArea");
	    
	    chart.setTheme(dojox.charting.themes.Claro);
	    chart.addPlot("default", {
	        type: "Lines",
	        markers: true
	    });
	    
		chart.addAxis("x", { microTickStep: 1, minorTickStep: 1});
		chart.addAxis("y", { vertical: true, fixLower: "major", fixUpper: "major", minorTickStep: 1 });
		chart.addSeries("Azimuth", new dojox.charting.StoreSeries(store, { query: { parameter: "Azimuth" } }, "value"));
		chart.addSeries("Elevation", new dojox.charting.StoreSeries(store, { query: { parameter: "Elevation" } }, "value"));
//		chart.setAxisWindow("x",2,10).render();
		
	    var tip = new dojox.charting.action2d.Tooltip(chart,"default");
	    var mag = new dojox.charting.action2d.Magnify(chart,"default");
	    
	    chart.render();
//	    var legend = new dojox.charting.widget.Legend({ chart: chart }, "legend");
	    
	    var startNumber = 1;
		dojo.subscribe("GetPositionAzimuth", function(myObject){
		    console.log("New position recieved!", myObject.name);
//		    if(startNumber == 1){
//		    	store.newItem({
//		    		value: myObject.params[1].value, 
//		    		id: ++startNumber, 
//		    		parameter: myObject.params[1].name
//				});
//		    	store.newItem({
//		    		value: myObject.params[2].value, 
//		    		id: ++startNumber, 
//		    		parameter: myObject.params[2].name
//				});
//		    }
			store.notify({ value: myObject.params[1].value, id: ++startNumber, parameter: myObject.params[1].name });
			store.notify({ value: myObject.params[2].value, id: ++startNumber, parameter: myObject.params[2].name });
		});
		
		var cmdArea = dojo.create("div", {
			id : "commandArea",
			region : "center"
		}, "commandTab");

		var cmdLogArea = dojo.create("div", {
			id : "commandLogArea",
			region : "center",
			style : "width:100%; overflow:visible;"
		}, "commandTab");
		
		var commandBox = new dijit.form.TextBox({
			id : "commandTextBox",
			value : "GET_POSITION"
		}).placeAt("commandArea");

		var buttonSend = new dijit.form.Button({
			label : "Send",
			onClick : function() {
				var muutuja = dijit.byId("commandTextBox");
				var split = muutuja.get('value').split(" ");

				var params = new Array();
				console.log(split.length % 2);

				if ((split.length - 1) % 2 == 0) {
					for (i = 1; i < split.length - 1; i = i + 2) {
						params.push(new TelemetryParameter(split[i],
								split[i + 1]));
					}
				}

				var telemetryCommand = new TelemetryCommand(split[0], params);
				dojo.byId("commandLogArea").innerHTML += JSON.stringify(telemetryCommand) + "<br>";
				dojo.publish("toWS", [JSON.stringify(telemetryCommand)]);
				
			}
		}).placeAt("commandArea");
		
		var buttonClear = new dijit.form.Button({
			label : "Clear",
			onClick : function() {
				dojo.byId("commandLogArea").innerHTML = "";
			}
		}).placeAt("commandArea");
		
		function TelemetryParameter(name, value) {
			this.name = name;
			this.value = value;
		}

		function TelemetryCommand(name, params) {
			this.name = name;
			this.params = params;
		}
		
		

	}
});