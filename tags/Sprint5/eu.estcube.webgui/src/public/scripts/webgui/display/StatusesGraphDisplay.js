dojo.provide("webgui.display.StatusesGraphDisplay");
dojo.require("dijit.layout.StackContainer");
dojo.require("dojox.gfx");
dojo.require("dojox.gfx.Moveable");

dojo.declare("webgui.display.StatusesGraphDisplay", null, {

	constructor : function() {

		console.log("StatusesGraphDisplay activated!!!");

		var nodeWidth = 100;
		var nodeHeight = 100;
		var xPos = 200;
		var yPos = 50;
		var nameSize = 12;

		var statusesGraphContent = new dijit.layout.ContentPane({
			id : "statusesGraphTab",
			title : "Graph",
			preventCache : true
		});

		var tempCont;
		tempCont = dijit.byId("CenterContainer");

		tempCont.addChild(statusesGraphContent);
		tempCont.selectChild(statusesGraphContent);

		var surfaceVar = dojo.create("div", {
			id : "surfaceElement",
			region : "center"
		}, "statusesGraphTab");

		var surface = dojox.gfx.createSurface("surfaceElement", 800, 600);

		// CAMERA
		var camNode = surface.createRect({
			x : xPos,
			y : yPos,
			width : nodeWidth,
			height : nodeHeight
		}).setFill("green");

		var camName = surface.createText({
			x : xPos,
			y : yPos + 2 * nameSize,
			text : "CAM",
			align : "start"
		}).setFont({
			family : "Arial",
			size : nameSize + "pt",
			weight : "bold"
		}).setFill("black");

		var grpCAM = surface.createGroup();
		grpCAM.add(camNode);
		grpCAM.add(camName);
		grpCAM.connect("onclick", function(e) {
			var tc = dijit.byId("componentTabContainer");
			tc.selectChild(dijit.byId("camTab"));
		});
		// COM
		var comNode = surface.createRect({
			x : xPos + 2 * nodeWidth,
			y : yPos,
			width : nodeWidth,
			height : nodeHeight
		}).setFill("green");

		var comName = surface.createText({
			x : xPos + 2 * nodeWidth,
			y : yPos + 2 * nameSize,
			text : "COM",
			align : "start"
		}).setFont({
			family : "Arial",
			size : nameSize + "pt",
			weight : "bold"
		}).setFill("black");

		var grpCOM = surface.createGroup();
		grpCOM.add(comNode);
		grpCOM.add(comName);
		grpCOM.connect("onclick", function(e) {
			var tc = dijit.byId("componentTabContainer");
			tc.selectChild(dijit.byId("comTab"));
		});
		// CDHS
		var cdhsNode = surface.createRect({
			x : xPos,
			y : yPos + 2 * nodeHeight,
			width : nodeWidth,
			height : nodeHeight
		}).setFill("green");

		var cdhsName = surface.createText({
			x : xPos,
			y : yPos + 2 * (nameSize + nodeHeight),
			text : "CDHS",
			align : "start"
		}).setFont({
			family : "Arial",
			size : nameSize + "pt",
			weight : "bold"
		}).setFill("black");

		var grpCDHS = surface.createGroup();
		grpCDHS.add(cdhsNode);
		grpCDHS.add(cdhsName);
		grpCDHS.connect("onclick", function(e) {
			var tc = dijit.byId("componentTabContainer");
			tc.selectChild(dijit.byId("cdhsTab"));
		});
		// EPS
		var epsNode = surface.createRect({
			x : xPos + 2 * nodeWidth,
			y : yPos + 2 * nodeHeight,
			width : nodeWidth,
			height : nodeHeight
		}).setFill("green");

		var epsName = surface.createText({
			x : xPos + 2 * nodeWidth,
			y : yPos + 2 * (nameSize + nodeHeight),
			text : "EPS",
			align : "start"
		}).setFont({
			family : "Arial",
			size : nameSize + "pt",
			weight : "bold"
		}).setFill("black");

		var grpEPS = surface.createGroup();
		grpEPS.add(epsNode);
		grpEPS.add(epsName);
		grpEPS.connect("onclick", function(e) {
			var tc = dijit.byId("componentTabContainer");
			tc.selectChild(dijit.byId("epsTab"));
		});
		// BEACON
		var beaconNode = surface.createRect({
			x : xPos + 4 * nodeWidth,
			y : yPos + 2 * nodeHeight,
			width : nodeWidth,
			height : nodeHeight
		}).setFill("green");

		var beaconName = surface.createText({
			x : xPos + 4 * nodeWidth,
			y : yPos + 2 * (nameSize + nodeHeight),
			text : "BEACON",
			align : "start"
		}).setFont({
			family : "Arial",
			size : nameSize + "pt",
			weight : "bold"
		}).setFill("black");

		var grpBEACON = surface.createGroup();
		grpBEACON.add(beaconNode);
		grpBEACON.add(beaconName);
		grpBEACON.connect("onclick", function(e) {
			var tc = dijit.byId("componentTabContainer");
			tc.selectChild(dijit.byId("beaconTab"));
		});
		// ADCS
		var adcsNode = surface.createRect({
			x : xPos - 2 * nodeWidth,
			y : yPos + 2 * nodeHeight,
			width : nodeWidth,
			height : nodeHeight
		}).setFill("green");

		var adcsName = surface.createText({
			x : xPos - 2 * nodeWidth,
			y : yPos + 2 * (nameSize + nodeHeight),
			text : "ADCS",
			align : "start"
		}).setFont({
			family : "Arial",
			size : nameSize + "pt",
			weight : "bold"
		}).setFill("black");

		var grpADCS = surface.createGroup();
		grpADCS.add(adcsNode);
		grpADCS.add(adcsName);
		grpADCS.connect("onclick", function(e) {
			var tc = dijit.byId("componentTabContainer");
			tc.selectChild(dijit.byId("adcsTab"));
		});
		// CONNECTIONS
		var firstLevelHorizontal = yPos + nodeHeight / 2;
		var secondLevelHorizontal = yPos + 2.5 * nodeHeight;

		var firstLevelVertical = xPos + nodeWidth / 2;
		var secondLevelVertical = xPos + 2.5 * nodeWidth;
		var thirdLevelvertical = xPos + 4.5 * nodeWidth;

		// VERTICAL
		var camToCdhs = surface.createLine({
			x1 : firstLevelVertical,
			y1 : yPos + nodeHeight,
			x2 : firstLevelVertical,
			y2 : yPos + 2 * nodeHeight
		}).setStroke({
			width : 3,
			color : "black"
		});

		var comToEps = surface.createLine({
			x1 : secondLevelVertical,
			y1 : yPos + nodeHeight,
			x2 : secondLevelVertical,
			y2 : yPos + 2 * nodeHeight
		}).setStroke({
			width : 3,
			color : "black"
		});

		// HORIZONTAL
		var camToCom = surface.createLine({
			x1 : xPos + nodeWidth,
			y1 : firstLevelHorizontal,
			x2 : xPos + 2 * nodeWidth,
			y2 : firstLevelHorizontal
		}).setStroke({
			width : 3,
			color : "black"
		});

		var cdhasToEps = surface.createLine({
			x1 : xPos + nodeWidth,
			y1 : secondLevelHorizontal,
			x2 : xPos + 2 * nodeWidth,
			y2 : secondLevelHorizontal
		}).setStroke({
			width : 3,
			color : "black"
		});

		var cdhasToAds = surface.createLine({
			x1 : xPos,
			y1 : secondLevelHorizontal,
			x2 : xPos - nodeWidth,
			y2 : secondLevelHorizontal
		}).setStroke({
			width : 3,
			color : "black"
		});

		var epsToBeacon = surface.createLine({
			x1 : xPos + 3 * nodeWidth,
			y1 : secondLevelHorizontal,
			x2 : xPos + 4 * nodeWidth,
			y2 : secondLevelHorizontal
		}).setStroke({
			width : 3,
			color : "black"
		});

		// POLYCONNECTION
		
	}
});
