/**
 * 
 */
package eu.estcube.domain;

import java.util.ArrayList;
import java.util.List;

public class TelemetryObject {

    private final String name;
    private List<TelemetryParameter> params;

    /**
     * Creates new TelemetryObject, with only a name.
     * 
     * @param name
     */
    public TelemetryObject(String name) {
        this.name = name;
        params = new ArrayList<TelemetryParameter>();
    }

    /**
     * Creates new TelemetryObject.
     * 
     * @param name
     * @param params
     */
    public TelemetryObject(String name, List<TelemetryParameter> params) {
        this.name = name;
        this.params = params;
    }

    /**
     * Returns name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns parameters.
     * 
     * @return the params
     */
    public List<TelemetryParameter> getParams() {
        return params;
    }

    /**
     * Returns parameters.
     * 
     * @return a params
     */
    public TelemetryParameter getParameter(String name) {
        for (TelemetryParameter param : params) {
            if (param.getName().equals(name)) {
                return param;
            }
        }
        return null;
    }

    /**
     * Adds a list of parameters
     * 
     * @param params
     *            the parameters to add
     */
    public void addParams(List<TelemetryParameter> params) {
        this.params.addAll(params);
    }

    /**
     * Adds a parameter
     * 
     * @param param
     *            the parameter to add
     */
    public void addParameter(TelemetryParameter param) {
        this.params.add(param);
    }

}
