/**
 * 
 */
package eu.estcube.domain;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class TelemetryObjectTest {

    private static final String NAME = "A";
    private static List<TelemetryParameter> PARAMS;
    
    private TelemetryObject to, to2;
    private TelemetryParameter tp;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        PARAMS = new ArrayList<TelemetryParameter>();
        tp = new TelemetryParameter("Name1", "Value1");
        to = new TelemetryObject("B");
        to2 = new TelemetryObject(NAME, PARAMS);
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryObject#TelemetryObject(java.lang.String)}.
     */
    @Test
    public void testTelemetryObjectString() {
        assertNotNull(to);
        assertEquals(0, to.getParams().size());
        assertEquals("B", to.getName());
        assertNotNull(to.toString());
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryObject#TelemetryObject(java.lang.String, java.util.List)}.
     */
    @Test
    public void testTelemetryObjectStringListOfTelemetryParameter() {
        assertNotNull(to2);
        assertEquals(0, to2.getParams().size());
        assertEquals(NAME, to2.getName());
        assertNotNull(to2.toString());
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryObject#getName()}.
     */
    @Test
    public void testGetName() {
        assertEquals("B", to.getName());
        assertEquals(NAME, to2.getName());
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryObject#getParams()}.
     */
    @Test
    public void testGetParams() {
        to.addParams(PARAMS);
        assertEquals(PARAMS, to.getParams());
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryObject#getParameter()}.
     */
    @Test
    public void testGetParameter() {
        to.addParameter(tp);
        assertEquals(tp, to.getParameter("Name1"));
    }
    
    /**
     * Test method for {@link eu.estcube.domain.TelemetryObject#addParams(java.util.List)}.
     */
    @Test
    public void testAddParams() {
        to.addParams(PARAMS);
        assertEquals(PARAMS, to.getParams());
    }
    

    /**
     * Test method for {@link eu.estcube.domain.TelemetryObject#addParameter(java.util.List)}.
     */
    @Test
    public void testAddParameter() {
        to.addParameter(tp);
        assertEquals(tp, to.getParameter("Name1"));
    }

}
