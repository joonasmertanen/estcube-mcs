package eu.estcube.webserver;

import java.util.HashMap;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.estcube.domain.TelemetryObject;

/**
 * Bean for caching telemetryobjects from messages that are passed through it
 * and returning cached elements by request in the message body.
 * @author Kaupo Kuresson
 */
@Component
public class CacheMessage {
    /** HashMap containing cached TelemetryObjects as values and their names(String) as keys. */
    private static HashMap<String, TelemetryObject> cache;
    private static final Logger LOG = LoggerFactory.getLogger(CacheMessage.class);
        
    /**
     * Class constructor
     */
    public CacheMessage() {
        cache = new HashMap<String, TelemetryObject>();
    }
    
    /**
     * Takes a TelemetryObject from the body of a passing message and adds it to the cache.
     * @param ex Camel automatically binds the Exchange to this parameter.
     * @see <a href="http://camel.apache.org/maven/current/camel-core/apidocs/org/apache/camel/Exchange.html">Exchange</a>
     */
    public void addToCache(Exchange ex) {
        cache.put(ex.getIn().getBody(TelemetryObject.class).getName(), ex.getIn().getBody(TelemetryObject.class));
    }
    
    /**
     * Gets a specific element from the cache and returns it in the message body.
     * @param ex Camel automatically binds the Exchange to this parameter.
     * @param param Name of the TelemetryObject to get out of the cache.
     * @see <a href="http://camel.apache.org/maven/current/camel-core/apidocs/org/apache/camel/Exchange.html">Exchange</a>
     */
    public void getCachedElement(Exchange ex, String param) {
        ex.getOut().setBody(cache.get(param));
    }
    
    /**
     * Returns the whole cache in the message body.
     * @param ex Camel automatically binds the Exchange to this parameter.
     * @see <a href="http://camel.apache.org/maven/current/camel-core/apidocs/org/apache/camel/Exchange.html">Exchange</a>
     */
    public void getCache(Exchange ex) {
        ex.getOut().setBody(cache);
    }
}
