﻿dojo.provide("webgui.assembly.Assembler");

dojo.require("webgui.comm.ProxyBase");
dojo.require("webgui.display.ConsoleDisplay");
dojo.require("webgui.display.InfoDisplay");
dojo.require("webgui.display.MenuDisplay");
dojo.require("webgui.display.PictureDisplay");
dojo.require("webgui.display.TabDisplay");
dojo.require("webgui.msgbus");
dojo.require("webgui.common.Constants");
	
dojo.declare("webgui.assembly.Assembler", null, {

	    loadAssembly: function() {	    	
	    	
	    	console.log("loadAssembly Started!!");
	    	
	    	new webgui.display.MenuDisplay();
	    	new webgui.display.ConsoleDisplay();
	    	new webgui.display.InfoDisplay();
	    	new webgui.display.PictureDisplay();
	    	new webgui.display.TabDisplay();
	    	
	    	console.log("loadAssembly done!!");
	    }
});

