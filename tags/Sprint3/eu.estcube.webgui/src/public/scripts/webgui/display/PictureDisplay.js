dojo.provide("webgui.display.PictureDisplay");

dojo.require("dijit.form.TextBox");
dojo.require("dijit.form.Button");
dojo.require("dijit.layout.ContentPane")
dojo.require("dojox.socket");

dojo.declare("webgui.display.PictureDisplay", null, {
	
	constructor: function() {
		var socket = dojox.socket("ws://localhost:9292/foo/");
		console.log("PictureDisplay2");

		dojo.create("div", {id: "commandArea"}, "PictureView");
		dojo.create("div", {id: "commandLogArea", style: "width: 300px; length: 300px;"}, "PictureView");
		
		var commandBox = new dijit.form.TextBox({
			id: "commandTextBox",
			value: ""
		}).placeAt("commandArea");
		
		var buttonSend = new dijit.form.Button({
			label: "Send",
			onClick: function() {
				var muutuja = dijit.byId("commandTextBox");
				var split = muutuja.get('value').split(" ");
				console.log(muutuja.get('value'));
    			var params = [];   			
    			var telemetryCommand = new TelemetryCommand(split[0],params);
    		  	socket.send(JSON.stringify(telemetryCommand))
				
				
    			socket.on("message", function(message){ //telecommand is sent back to us
    				console.log("Listening: I got ", message.data);
    				var myObject = eval('(' + message.data +')'); //make object out of json
    				dojo.byId("commandLogArea").innerHTML = myObject.name + "<br>"; //name of command to logArea
    				return   JSON.stringify(message);
    			});
			}
		}).placeAt("commandArea");
		
    	function TelemetryParameter(name,value) {
    		this.name=name;
    		this.value=value;
    	}
    	
    	function TelemetryCommand(name, params) {
    		this.name = name;
    		this.params = params;
    	}	
	}
});