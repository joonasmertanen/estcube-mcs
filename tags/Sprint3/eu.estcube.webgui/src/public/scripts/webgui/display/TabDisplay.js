dojo.provide("webgui.display.TabDisplay");

dojo.require("dijit.layout.TabContainer");
dojo.require("dijit.layout.ContentPane");

dojo.declare("webgui.display.TabDisplay", null, {
	
	constructor: function() {
				
		console.log("TabDisplay!");
		
		var tabs = dijit.byId("DetailsView");
		
	        var cp1 = new dijit.layout.ContentPane({
	            title: "Telemetry",
	            content: "New data!"
	        });
	        tabs.addChild(cp1);

	        var cp2 = new dijit.layout.ContentPane({
	            title: "GS",
	            content: "Groundstation online!",
	            closable: "true"
	        });
	        tabs.addChild(cp2);

	        tabs.startup();        
	        
	}
});