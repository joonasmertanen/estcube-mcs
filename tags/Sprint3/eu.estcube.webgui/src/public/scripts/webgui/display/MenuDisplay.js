dojo.provide("webgui.display.MenuDisplay");

dojo.require("dijit.MenuBar");
dojo.require("dijit.MenuBarItem");
dojo.require("dijit.PopupMenuBarItem");
dojo.require("dijit.Menu");
dojo.require("dijit.MenuItem");
dojo.require("dijit.layout.BorderContainer");
dojo.require("dijit.layout.ContentPane");


dojo.declare("MenuDisplay", null, {
	
	constructor: function() {
		console.log("MenuDisplay!");
		
		dojo.create("div", {id: "menuContainer", layoutPriority: "2"}, "MenuView");
		
		var mainMenu = new dijit.Menu({style: "width: 100%;"}, "menuContainer");
		var menuItem1 = new dijit.MenuItem({ label: "Menu1"});
		var menuItem2 = new dijit.MenuItem({ label: "Console"});
		var menuItem3 = new dijit.MenuItem({ label: "Menu3"});
		var menuItem4 = new dijit.MenuItem({ label: "Menu4"});
		
		mainMenu.addChild(menuItem1);
		mainMenu.addChild(menuItem2);
		mainMenu.addChild(menuItem3);
		mainMenu.addChild(menuItem4);	
		
	}
});

dojo.declare("webgui.display.MenuDisplay", null, {

	constructor: function(args) {
		var menu = new MenuDisplay();
	}
});