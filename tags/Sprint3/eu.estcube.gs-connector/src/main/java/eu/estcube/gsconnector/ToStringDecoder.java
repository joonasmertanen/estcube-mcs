package eu.estcube.gsconnector;

import java.nio.charset.Charset;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.string.StringDecoder;
import org.springframework.stereotype.Component;

@Component("stringDecoder")
public class ToStringDecoder extends StringDecoder{
        
    public ToStringDecoder(){
        super(Charset.forName("ASCII"));
    }
    
    public Object decode(ChannelHandlerContext ctx, Channel evt, Object msg) throws Exception {  
        return super.decode(ctx, evt, msg);
    }
}
