package eu.estcube.gsconnector.commands;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class CAPABILITIES extends CommandStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        messageString.append("+");
        messageString.append("1");
        messageString.append("\n");
        return messageString;
    }
}
