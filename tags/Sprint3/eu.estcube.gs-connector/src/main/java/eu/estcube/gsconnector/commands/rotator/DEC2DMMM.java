package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.domain.TelemetryRotatorConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class DEC2DMMM extends CommandStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        
        
        messageString.append("+");
        messageString.append("e");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Dec Deg"));
        messageString.append("\n");
        
        
        return messageString;
    }
}
