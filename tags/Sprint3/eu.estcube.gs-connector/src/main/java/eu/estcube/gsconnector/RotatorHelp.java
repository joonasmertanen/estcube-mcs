package eu.estcube.gsconnector;

import java.util.HashMap;
import java.util.Map;

import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryParameter;
import eu.estcube.domain.TelemetryRotatorConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;
import eu.estcube.gsconnector.commands.rotator.*;

public class RotatorHelp {

    private Map<String, String> helpHashMap;
    
    public RotatorHelp(){
        helpHashMap = new HashMap<String, String>();
        helpHashMap.put("P", TelemetryRotatorConstants.SET_POSITION);
        helpHashMap.put("p", TelemetryRotatorConstants.GET_POSITION);
        helpHashMap.put("K", TelemetryRotatorConstants.PARK);
        helpHashMap.put("S", TelemetryRotatorConstants.STOP);
        helpHashMap.put("M", TelemetryRotatorConstants.MOVE);
        helpHashMap.put("1", TelemetryRotatorConstants.CAPABILITIES);
        helpHashMap.put("R", TelemetryRotatorConstants.RESET);
        helpHashMap.put("C", TelemetryRotatorConstants.SET_CONFIG);
        helpHashMap.put("_", TelemetryRotatorConstants.GET_INFO);
        helpHashMap.put("w", TelemetryRotatorConstants.SEND_RAW_CMD);
        helpHashMap.put("L", TelemetryRotatorConstants.LONLAT2LOC);
        helpHashMap.put("l", TelemetryRotatorConstants.LOC2LONLAT);
        helpHashMap.put("D", TelemetryRotatorConstants.DMS2DEC);
        helpHashMap.put("d", TelemetryRotatorConstants.DEC2DMS);
        helpHashMap.put("E", TelemetryRotatorConstants.DMMM2DEC);
        
        helpHashMap.put("e", TelemetryRotatorConstants.DEC2DMMM);
        helpHashMap.put("B", TelemetryRotatorConstants.QRB);
        helpHashMap.put("A", TelemetryRotatorConstants.AZIMUTH_SHORTPATH2LONGPATH);
        helpHashMap.put("a", TelemetryRotatorConstants.DISTANCE_SHORTPATH2LONGPATH);
        helpHashMap.put("?", TelemetryRotatorConstants.HELP);
    }
    
    public TelemetryObject createHelpList(TelemetryObject telemetryObject, String[] messageSplit){
        
        String[] messagePiece={null, null, null};
        for(int i=1;i<messageSplit.length;i++){
            if(messageSplit[i].contains("RPRT")){
                break;
            }
            messagePiece = messageSplit[i].split(":");
            if(messagePiece.length==2){
                telemetryObject.addParameter(
                        new TelemetryParameter(
                                helpHashMap.get(messagePiece[0]), 
                                messagePiece[1].substring(
                                        (messagePiece[1].indexOf("(")+1), 
                                        messagePiece[1].indexOf(")"))
                                        )
                        );
            }            
            else if(messagePiece.length==3){
                telemetryObject.addParameter(
                        new TelemetryParameter(
                                helpHashMap.get(messagePiece[0]), 
                                messagePiece[1].substring(
                                        (messagePiece[1].indexOf("(")+1), 
                                        messagePiece[1].indexOf(")"))
                                        )
                        );

                telemetryObject.addParameter(
                        new TelemetryParameter(
                                helpHashMap.get(messagePiece[1].substring(
                                        (messagePiece[1].length()-1))
                                        ), 
                                messagePiece[2].substring(
                                        (messagePiece[2].indexOf("(")+1), 
                                        messagePiece[2].indexOf(")"))
                                )
                        );
            }
        }
        
        return telemetryObject;
    }
    
}
