package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.domain.TelemetryRotatorConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SET_POSITION extends CommandStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        messageString.append("+");
        messageString.append("P");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Azimuth"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Elevation"));
        messageString.append("\n");
        return messageString;
    }
}
