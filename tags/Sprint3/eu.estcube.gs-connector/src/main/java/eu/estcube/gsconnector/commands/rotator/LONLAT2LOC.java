package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class LONLAT2LOC extends CommandStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        
        messageString.append("+");
        messageString.append("L");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Longitude"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Latitude"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Loc Len"));
        messageString.append("\n");
        
        
        
        return messageString;
    }
}
