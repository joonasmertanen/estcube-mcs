package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class CONVERT_POWER2MW extends CommandStringBuilder{
    private StringBuilder messageString;

    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        messageString.append("+");
        messageString.append("2");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Power"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Frequency"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Mode"));
        messageString.append("\n");
        return messageString;
    }
}
