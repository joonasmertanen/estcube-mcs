package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SET_CONFIG extends CommandStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        
        
        
        messageString.append("+");
        messageString.append("C");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Token"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Value"));
        messageString.append("\n");
        
        return messageString;
    }
}
