package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class DMMM2DEC extends CommandStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        
        messageString.append("+");
        messageString.append("E");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Degrees"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Dec Minutes"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("S/W"));
        messageString.append("\n");
        
        
        
        return messageString;
    }
}
