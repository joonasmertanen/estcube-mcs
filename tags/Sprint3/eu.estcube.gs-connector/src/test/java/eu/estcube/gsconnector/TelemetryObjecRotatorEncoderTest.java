package eu.estcube.gsconnector;


import static org.junit.Assert.assertEquals;

import org.jboss.netty.buffer.ChannelBuffers;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;

public class TelemetryObjecRotatorEncoderTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private TelemetryObjecRotatorEncoder encode = new TelemetryObjecRotatorEncoder();
    

    private static final Logger LOG = LoggerFactory.getLogger(TelemetryObjecRotatorEncoderTest.class);

    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        
    }

    @Test
    public void testEncode() throws Exception
    {
        telemetryCommand = null;
        Object test = encode.encode(null, null, telemetryCommand);
        assertEquals(ChannelBuffers.EMPTY_BUFFER, test);

        telemetryCommand = new TelemetryCommand("STOP");
        string.append("+S\n");
        assertEquals(string.toString(), encode.encode(null, null, telemetryCommand).toString());
    }

}
