package eu.estcube.gsconnector.commands.rotator;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class DMS2DECTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private DMS2DEC createMessage = new DMS2DEC();
    private static final Logger LOG = LoggerFactory.getLogger(DMS2DECTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("DMS2DEC");
        telemetryCommand.addParameter("Degrees", 0);
        telemetryCommand.addParameter("Minutes", 0);
        telemetryCommand.addParameter("Seconds", 0.0);
        telemetryCommand.addParameter("S/W", 0);
        string.append("+D 0 0 0.0 0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
