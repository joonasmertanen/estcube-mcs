package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class SET_TRANCEIVE_MODETest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SET_TRANCEIVE_MODE createMessage = new SET_TRANCEIVE_MODE();
    private static final Logger LOG = LoggerFactory.getLogger(SET_TRANCEIVE_MODETest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("CONVERT_MW2POWER");
        telemetryCommand.addParameter("Transceive", "OFF");
        string.append("+A OFF\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
