package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class SEND_MORSETest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SEND_MORSE createMessage = new SEND_MORSE();
    private static final Logger LOG = LoggerFactory.getLogger(SEND_MORSETest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("SEND_MORSE");
        telemetryCommand.addParameter("Morse", "..--..");
        string.append("+b ..--..\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
