package eu.estcube.gsconnector.commands.rotator;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class DEC2DMSTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private DEC2DMS createMessage = new DEC2DMS();
    private static final Logger LOG = LoggerFactory.getLogger(DEC2DMSTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("DEC2DMS");
        telemetryCommand.addParameter("Dec Degrees", 0.0);
        string.append("+d 0.0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
