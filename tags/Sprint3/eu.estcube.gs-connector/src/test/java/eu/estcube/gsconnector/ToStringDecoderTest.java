/** 
 *
 */
package eu.estcube.gsconnector;

import static org.junit.Assert.*;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author kimmell
 *
 */
public class ToStringDecoderTest {

    private static final Logger LOG = LoggerFactory.getLogger(ToStringDecoderTest.class);
    private ToStringDecoder decoder;
    private ChannelHandlerContext ctx;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        decoder = new ToStringDecoder();
        ctx = Mockito.mock(ChannelHandlerContext.class);
    }

    /**
     * Test method for {@link eu.estcube.gsconnector.ToStringDecoder#decode(org.jboss.netty.channel.ChannelHandlerContext, org.jboss.netty.channel.Channel, java.lang.Object)}.
     * @throws Exception 
     */
    @Test
    public void testDecodeChannelHandlerContextChannelObject() throws Exception {
        Object o = new Object();
        assertEquals(o, decoder.decode(ctx, null, o));
        o = new Long(-123);
        assertEquals(o, decoder.decode(ctx, null, o));
        
        Mockito.verifyNoMoreInteractions(ctx);
    }
    
//    /**
//     * Test method for {@link eu.estcube.gsconnector.ToStringDecoder#decode(org.jboss.netty.channel.ChannelHandlerContext, org.jboss.netty.channel.Channel, java.lang.Object)}.
//     * @throws Exception 
//     */
//    @Test
//    public void testDecodeChannelHandlerContextChannelObjectTrue() throws Exception {
//        Object o = new Object();
//        Mockito.when(ctx.canHandleDownstream()).thenReturn(true, false);
//        assertEquals(o, decoder.decode(ctx, null, o));
//        assertEquals(null, decoder.decode(ctx, null, o));
//        assertNull(decoder.decode(ctx, null, o));
//        Mockito.verify(ctx, Mockito.atLeast(4)).canHandleDownstream();
//    }
//    
//    /**
//     * Test method for {@link eu.estcube.gsconnector.ToStringDecoder#decode(org.jboss.netty.channel.ChannelHandlerContext, org.jboss.netty.channel.Channel, java.lang.Object)}.
//     * @throws Exception 
//     */
//    @Test
//    public void testDecodeChannelHandlerContextChannelObjectFalse() throws Exception {
//        Object o = new Object();
//        Mockito.when(ctx.canHandleDownstream()).thenReturn(false);
//        assertEquals(null, decoder.decode(ctx, null, o));
//        Mockito.verify(ctx, Mockito.times(1)).canHandleDownstream();
//    }


    /**
     * Test method for {@link eu.estcube.gsconnector.ToStringDecoder#decode(org.jboss.netty.channel.ChannelHandlerContext, org.jboss.netty.channel.Channel, java.lang.Object)}.
     * @throws Exception 
     */
    @Test
    public void testDecodeChannelHandlerContextChannelObjectChannelBuffer() throws Exception {
        ChannelBuffer buffer = ChannelBuffers.wrappedBuffer(new byte[] {});
        assertEquals("", decoder.decode(ctx, null, buffer));
        buffer = ChannelBuffers.wrappedBuffer(new byte[] { 'a' });
        assertEquals("a", decoder.decode(ctx, null, buffer));
        String str = "Ö";
        buffer = ChannelBuffers.wrappedBuffer(str.getBytes("UTF-8"));
        String result = (String)decoder.decode(ctx, null, buffer);
        System.out.println(result);
        assertNotSame("Ö", decoder.decode(ctx, null, buffer));
    }
}
