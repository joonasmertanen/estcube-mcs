package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class SET_TRANSMIT_MODETest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SET_TRANSMIT_MODE createMessage = new SET_TRANSMIT_MODE();
    private static final Logger LOG = LoggerFactory.getLogger(SET_TRANSMIT_MODETest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("SET_TRANSMIT_MODE");
        telemetryCommand.addParameter("TX Mode", "AM");
        telemetryCommand.addParameter("TX Passband", 0);
        string.append("+X AM 0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
