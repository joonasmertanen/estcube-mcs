package eu.estcube.gsconnector.commands.rotator;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class LONLAT2LOCTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private LONLAT2LOC createMessage = new LONLAT2LOC();
    private static final Logger LOG = LoggerFactory.getLogger(LONLAT2LOCTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("LONLAT2LOC");
        telemetryCommand.addParameter("Longitude", 1);
        telemetryCommand.addParameter("Latitude", 1);
        telemetryCommand.addParameter("Loc Len", 1);
        string.append("+L 1 1 1\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
