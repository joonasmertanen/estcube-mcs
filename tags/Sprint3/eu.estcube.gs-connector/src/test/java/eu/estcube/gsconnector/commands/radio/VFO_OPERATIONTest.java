package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class VFO_OPERATIONTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private VFO_OPERATION createMessage = new VFO_OPERATION();
    private static final Logger LOG = LoggerFactory.getLogger(VFO_OPERATIONTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("VFO_OPERATION");
        telemetryCommand.addParameter("Mem/VFO Op", "CPY");
        string.append("+G CPY\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
