package eu.estcube.webserver;

import static org.junit.Assert.*;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryParameter;

public class TelemetryObjectToJsonTest {

    private TelemetryObjectToJson telemetryObjectToJson;
    private TelemetryObject telemetryObject;
    private String telObjName = "telObjName";
    private TelemetryParameter telemetryParameter;
    private TelemetryParameter telemetryParameter2;
    private String telParName = "telParName";
    private String telParName2 = "telParName2";
    private String value = "123";
    private Double value2 = new Double(2.3334);
    private Exchange ex;
    private Message message;
    private Object answer;

    @Before
    public void setUp() throws Exception {
        telemetryObject = new TelemetryObject(telObjName);
        telemetryParameter = new TelemetryParameter(telParName, value);
        telemetryParameter2 = new TelemetryParameter(telParName2, value2);

        telemetryObject.addParameter(telemetryParameter);
        telemetryObject.addParameter(telemetryParameter2);

        message = Mockito.mock(Message.class);
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObject);

        telemetryObjectToJson = new TelemetryObjectToJson();
        ex = Mockito.mock(Exchange.class);

        Mockito.when(ex.getIn()).thenReturn(message);
        Mockito.when(ex.getOut()).thenReturn(message);

        Mockito.doAnswer(new Answer<Object>() {
            public Object answer(InvocationOnMock invocation) {
                answer = invocation.getArguments()[0];
                return answer;
            }
        }).when(message).setBody(Mockito.any());
    }

    @Test
    public void testProcess() throws Exception {
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObject);
        telemetryObjectToJson.process(ex);
        assertEquals("{\"name\":\"" + telObjName + "\",\"params\":[{\"name\":\"" + telParName + "\",\"value\":\""
                + value + "\"},{\"name\":\"" + telParName2 + "\",\"value\":" + value2 + "}]}", (String) answer);
    }

}
