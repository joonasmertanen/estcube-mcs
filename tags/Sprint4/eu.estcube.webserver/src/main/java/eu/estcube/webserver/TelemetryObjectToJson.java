package eu.estcube.webserver;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.google.gson.Gson;
import eu.estcube.domain.TelemetryObject;

@Component
public class TelemetryObjectToJson implements Processor {
    private static final Logger LOG = LoggerFactory.getLogger(TelemetryObjectToJson.class);

    public void process(Exchange ex) throws Exception {
        TelemetryObject telemetryObject = ex.getIn().getBody(TelemetryObject.class);
        Gson gson = new Gson();

        String json = gson.toJson(telemetryObject);

        ex.getOut().setBody(json);
    }

}
