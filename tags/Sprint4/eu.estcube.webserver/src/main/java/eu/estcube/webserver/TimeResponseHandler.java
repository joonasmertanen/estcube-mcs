/** 
 *
 */
package eu.estcube.webserver;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.estcube.domain.TelemetryCommand;

@Component
public class TimeResponseHandler implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(TimeResponseHandler.class);

    /** @{inheritDoc . */
    public void process(Exchange exchange) throws Exception {
        TelemetryCommand response = exchange.getIn().getBody(TelemetryCommand.class);
        LOG.debug("Time response: {}", response.getParameter("command"));
        exchange.getOut().setBody("Time response: " + response.getParameter("command"));
    }
}
