package eu.estcube.parameterstorage;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import eu.estcube.domain.JMSConstants;
import eu.estcube.domain.TelemetryCommand;
import static org.apache.camel.builder.PredicateBuilder.not;

public class ParameterStorage extends RouteBuilder{
    private static final Logger LOG = LoggerFactory.getLogger(ParameterStorage.class);
    @Autowired
    private TelemetryObjectToDb telObjToDb;
    @Autowired
    private DbToTelemetryObject dbToTelObj;
    @Autowired
    private TelObjSplitter telObjSplitter;
    @Override
    public void configure() throws Exception {

        from("timer://startup?repeatCount=1")
        .process(new Processor() {
            public void process(Exchange exchange) throws Exception {
                exchange.getIn().setHeader("groundStationID", "ES5EC");
                exchange.getIn().setHeader("device", "status");
                TelemetryCommand getStatus = new TelemetryCommand("GET_STATUS");
                exchange.getIn().setBody(getStatus, TelemetryCommand.class);
            }
         })
         .to(JMSConstants.gsRecive); 
        
        from("stream:in")
        .choice()
            .when(body().contains("pstorage"))
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        exchange.getOut().setBody("ES5EC");
                    }
                }).process(dbToTelObj).split().method("telObjSplitter", "splitMessage").to("log:echo");
        
        from(JMSConstants.gsSend)
        .choice()
            .when(not(header("fromPS").regex("true")))
                .process(telObjToDb)
        .otherwise()
        .end();
                
        from(JMSConstants.psQueue)
            .process(dbToTelObj)
                .split().method("telObjSplitter", "splitMessage")
                    .process(new Processor() {
                        public void process(Exchange exchange) throws Exception {
                            exchange.getIn().setHeader("fromPS", "true");
                        }
                    })
                    .to(JMSConstants.gsSend);
        

           
    }
    
    public static void main(String[] args) throws Exception { 
        LOG.info("Starting Parameter storage");
        new Main().run(args);
    }
}
