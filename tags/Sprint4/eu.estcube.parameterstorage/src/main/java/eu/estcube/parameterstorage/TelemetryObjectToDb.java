package eu.estcube.parameterstorage;

import java.sql.Types;
import java.util.List;
import javax.sql.DataSource;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryParameter;

@Component
public class TelemetryObjectToDb implements Processor {
    private static final Logger LOG = LoggerFactory.getLogger(TelemetryObjectToDb.class);
    
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;
       
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    public void process(Exchange ex) throws Exception {
               
        TelemetryObject telObj = ex.getIn().getBody(TelemetryObject.class);
        List<TelemetryParameter> params = telObj.getParams();

        String commandName = telObj.getName();
        String source = ex.getIn().getHeader("groundStationID").toString();
        String device = ex.getIn().getHeader("device").toString();
        
        String sql = "INSERT INTO TELEMETRYOBJECTS (NAME, SOURCE , DEVICE) VALUES (?,?,?);";  
        SqlUpdate su = new SqlUpdate();
        su.setDataSource(dataSource);
        su.setSql(sql);
        su.declareParameter(new SqlParameter(Types.VARCHAR));
        su.declareParameter(new SqlParameter(Types.VARCHAR));
        su.declareParameter(new SqlParameter(Types.VARCHAR));
        su.setReturnGeneratedKeys(true);
        su.compile();

        Object[] sqlParams = new Object[]{commandName, source, device};
        KeyHolder keyHolder = new GeneratedKeyHolder();
        su.update(sqlParams, keyHolder);
        int id = keyHolder.getKey().intValue();
            
        LOG.debug("id: " + id);
    
        su = new SqlUpdate();
        su.setDataSource(dataSource);
        sql = "INSERT INTO PARAMETERS (ID, KEY , VALUE) VALUES (?,?,?);";
        su.setSql(sql);
        su.declareParameter(new SqlParameter(Types.INTEGER));
        su.declareParameter(new SqlParameter(Types.VARCHAR));
        su.declareParameter(new SqlParameter(Types.VARCHAR));
        su.compile();
        
        for (TelemetryParameter param : params) {
            sqlParams = new Object[]{id, param.getName(), param.getValue()};
            su.update(sqlParams);
        }
    }
}
