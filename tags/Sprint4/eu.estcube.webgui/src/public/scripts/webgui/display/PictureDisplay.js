dojo.provide("webgui.display.PictureDisplay");

dojo.require("dijit.form.TextBox");
dojo.require("dijit.form.Button");
dojo.require("dijit.layout.ContentPane")
dojo.require("dojox.socket");

dojo.declare("webgui.display.PictureDisplay", null, {
	
	constructor: function() {
		console.log("PictureDisplay!");
		
		var exists = dijit.byId("PictureViewInner");
		
		if(exists) {
			console.log("It's alive");
			exists.destroy();
		}

		var picDisplay = dojo.create("div", {dojoType: "dijit.layout.ContentPane",
										id: "PictureViewInner", 
										region: "leading",
										content: "Hi! This is Main Picture view!",
										layoutPriority: "3"
										});
		
		dojo.place(picDisplay, "PictureView", "first");
	}
});