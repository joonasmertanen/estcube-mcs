dojo.provide("webgui.display.InfoDisplay");

dojo.require("dijit.layout.ContentPane");

dojo.declare("webgui.display.InfoDisplay", null, {
	
	constructor: function(view) {
		
		console.log("InfoDisplay!");

		var infoDisplay;
		
			infoDisplay = dojo.create("div", {dojoType: "dijit.layout.BorderContainer",
			id: "InfoViewInner", 
			region: "leading",
			innerHTML: "<p>Hi! This is Main info view!</p>",
			});

		dojo.place(infoDisplay, "InfoViewInner", "replace");
		
	}
});