package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class SET_MEMORY_CHANNELNRTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SET_MEMORY_CHANNELNR createMessage = new SET_MEMORY_CHANNELNR();
    private static final Logger LOG = LoggerFactory.getLogger(SET_MEMORY_CHANNELNRTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("SET_MEMORY_CHANNELNR");
        telemetryCommand.addParameter("Memory#", 0);
        string.append("+E 0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
