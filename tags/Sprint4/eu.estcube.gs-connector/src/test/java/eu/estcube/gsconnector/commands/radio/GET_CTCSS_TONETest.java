package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class GET_CTCSS_TONETest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private GET_CTCSS_TONE createMessage = new GET_CTCSS_TONE();
    private static final Logger LOG = LoggerFactory.getLogger(GET_CTCSS_TONETest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        
    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("GET_CTCSS_TONE");
        string.append("+c\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
