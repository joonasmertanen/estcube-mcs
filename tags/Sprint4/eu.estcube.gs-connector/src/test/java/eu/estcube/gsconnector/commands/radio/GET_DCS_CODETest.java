package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class GET_DCS_CODETest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private GET_DCS_CODE createMessage = new GET_DCS_CODE();
    private static final Logger LOG = LoggerFactory.getLogger(GET_DCS_CODETest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("GET_DCS_CODE");
        string.append("+d\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
