package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class SET_MEMORY_CHANNELDATATest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SET_MEMORY_CHANNELDATA createMessage = new SET_MEMORY_CHANNELDATA();
    private static final Logger LOG = LoggerFactory.getLogger(SET_MEMORY_CHANNELDATATest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("SET_MEMORY_CHANNELDATA");
        telemetryCommand.addParameter("Channel", "..");
        string.append("+H ..\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
