package eu.estcube.gsconnector.commands.rotator;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class AZIMUTH_SHORTPATH2LONGPATHTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private AZIMUTH_SHORTPATH2LONGPATH createMessage = new AZIMUTH_SHORTPATH2LONGPATH();
    private static final Logger LOG = LoggerFactory.getLogger(AZIMUTH_SHORTPATH2LONGPATHTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("AZIMUTH_SHORTPATH2LONGPATH");
        telemetryCommand.addParameter("Short Path Deg", 0.0);
        string.append("+A 0.0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
