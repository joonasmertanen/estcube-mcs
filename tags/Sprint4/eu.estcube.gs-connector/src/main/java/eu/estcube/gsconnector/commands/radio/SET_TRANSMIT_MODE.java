package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SET_TRANSMIT_MODE extends CommandStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        messageString.append("+");
        messageString.append("X");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("TX Mode"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("TX Passband"));
        messageString.append("\n");
        return messageString;
    }
}
