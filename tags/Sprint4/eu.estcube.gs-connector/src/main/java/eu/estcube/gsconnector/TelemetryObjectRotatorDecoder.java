package eu.estcube.gsconnector;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryParameter;



@Component("rotatorDecoder")
public class TelemetryObjectRotatorDecoder extends OneToOneDecoder{

    @Value("${gsName}")
    private String gsName;
    
    private static final Logger LOG = LoggerFactory.getLogger(TelemetryObjectRotatorDecoder.class);
    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, Object message) throws Exception {
        
        String[] messageSplit = message.toString().split("\n");
        
        String[] source = messageSplit[0].split(":");
        TelemetryObject telemetryObject = new TelemetryObject(source[0]);
        
        telemetryObject.setSource(gsName);
        
        if(messageSplit[0].equals("Commands (some may not be available for this rig):")){
            RotatorHelp rotatorHelp = new RotatorHelp();
            telemetryObject = rotatorHelp.createHelpList(telemetryObject, messageSplit);
        }
        else{
            String[] messagePiece={null, null};
            
            for(int i=0;i<messageSplit.length;i++){
    
                if(messageSplit[i].contains(":\t\t")){
                    messagePiece = messageSplit[i].split(":\t\t");
                }
                else if(messageSplit[i].contains(":\t")){
                    messagePiece = messageSplit[i].split(":\t");
                }
                else if(messageSplit[i].contains(": ")){                     ///////////ohukohad
                    messagePiece = messageSplit[i].split(": ");
                    if(messagePiece.length==1){
                        telemetryObject.addParameter(new TelemetryParameter(messagePiece[0], ""));
                        continue;
                    }
                }
                else if(messageSplit[i].contains(":")){                     ///////////ohukohad
                    messagePiece = messageSplit[i].split(":");
                    if(messagePiece.length==1){
                        telemetryObject.addParameter(new TelemetryParameter(messagePiece[0], ""));
                        continue;
                    }
                }
                else if(messageSplit[i].contains(" ")){
                    messagePiece = messageSplit[i].split(" ");
                }
                else{
                    continue;
                }
                telemetryObject.addParameter(new TelemetryParameter(messagePiece[0], messagePiece[1]));
            }
//            LOG.debug("Message piece 1: {}, Message piece 2: {}", messagePiece[0], messagePiece[1]);   
        }
        return telemetryObject;
//        return null;
    }

}
