package eu.estcube.gsconnector;

import java.util.HashMap;
import java.util.Map;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneDecoder;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CAPABILITIES;
import eu.estcube.gsconnector.commands.CommandStringBuilder;
import eu.estcube.gsconnector.commands.GET_INFO;
import eu.estcube.gsconnector.commands.HELP;
import eu.estcube.gsconnector.commands.SEND_RAW_CMD;
import eu.estcube.gsconnector.commands.radio.*;


@Component("radioEncoder")
public class TelemetryObjecRadioEncoder extends OneToOneEncoder{
    private Map<String, CommandStringBuilder> commandsHashMap;
    private static final Logger LOG = LoggerFactory.getLogger(TelemetryObjecRadioEncoder.class);
    
    public TelemetryObjecRadioEncoder(){
        commandsHashMap = new HashMap<String, CommandStringBuilder>();
        commandsHashMap.put(TelemetryRadioConstants.SET_FREQUENCY, new SET_FREQUENCY());
        commandsHashMap.put(TelemetryRadioConstants.GET_FREQUENCY, new GET_FREQUENCY());
        commandsHashMap.put(TelemetryRadioConstants.SET_MODE, new SET_MODE());
        commandsHashMap.put(TelemetryRadioConstants.SET_LEVEL, new SET_LEVEL());
        commandsHashMap.put(TelemetryRadioConstants.SET_VFO, new SET_VFO());
        commandsHashMap.put(TelemetryRadioConstants.SET_PTT, new SET_PTT());
        commandsHashMap.put(TelemetryRadioConstants.GET_PTT, new GET_PTT());
        commandsHashMap.put(TelemetryRadioConstants.SET_ANTENNA_NR, new SET_ANTENNA_NR());
        commandsHashMap.put(TelemetryRadioConstants.CAPABILITIES, new CAPABILITIES());
        commandsHashMap.put(TelemetryRadioConstants.CONFIGURATION, new CONFIGURATION());
        commandsHashMap.put(TelemetryRadioConstants.GET_MODE, new GET_MODE());
        commandsHashMap.put(TelemetryRadioConstants.SET_TRANSMIT_FREQUENCY, new SET_TRANSMIT_FREQUENCY());
        commandsHashMap.put(TelemetryRadioConstants.GET_TRANSMIT_FREQUENCY, new GET_TRANSMIT_FREQUENCY());
        commandsHashMap.put(TelemetryRadioConstants.SET_TRANSMIT_MODE, new SET_TRANSMIT_MODE());
        commandsHashMap.put(TelemetryRadioConstants.GET_TRANSMIT_MODE, new GET_TRANSMIT_MODE());
        commandsHashMap.put(TelemetryRadioConstants.SET_SPLIT_VFO, new SET_SPLIT_VFO());
        commandsHashMap.put(TelemetryRadioConstants.GET_SPLIT_VFO, new GET_SPLIT_VFO());
        commandsHashMap.put(TelemetryRadioConstants.SET_TUNING_STEP, new SET_TUNING_STEP());
        commandsHashMap.put(TelemetryRadioConstants.GET_TUNING_STEP, new GET_TUNING_STEP());
        commandsHashMap.put(TelemetryRadioConstants.GET_LEVEL, new GET_LEVEL());
        commandsHashMap.put(TelemetryRadioConstants.SET_FUNC, new SET_FUNC());
        commandsHashMap.put(TelemetryRadioConstants.GET_FUNC, new GET_FUNC());
        commandsHashMap.put(TelemetryRadioConstants.SET_PARM, new SET_PARM());
        commandsHashMap.put(TelemetryRadioConstants.GET_PARM, new GET_PARM());
        commandsHashMap.put(TelemetryRadioConstants.VFO_OPERATION, new VFO_OPERATION());
        commandsHashMap.put(TelemetryRadioConstants.SCAN, new SCAN());
        commandsHashMap.put(TelemetryRadioConstants.SET_TRANCEIVE_MODE, new SET_TRANCEIVE_MODE());
        commandsHashMap.put(TelemetryRadioConstants.GET_TRANCEIVE_MODE, new GET_TRANCEIVE_MODE());
        commandsHashMap.put(TelemetryRadioConstants.SET_RPTR_SHIFT, new SET_RPTR_SHIFT());
        commandsHashMap.put(TelemetryRadioConstants.GET_RPTR_SHIFT, new GET_RPTR_SHIFT());
        commandsHashMap.put(TelemetryRadioConstants.SET_RPTR_OFFSET, new SET_RPTR_OFFSET());
        commandsHashMap.put(TelemetryRadioConstants.GET_RPTR_OFFSET, new GET_RPTR_OFFSET());
        commandsHashMap.put(TelemetryRadioConstants.SET_CTCSS_TONE, new SET_CTCSS_TONE());
        commandsHashMap.put(TelemetryRadioConstants.GET_CTCSS_TONE, new GET_CTCSS_TONE());
        commandsHashMap.put(TelemetryRadioConstants.SET_DCS_CODE, new SET_DCS_CODE());
        commandsHashMap.put(TelemetryRadioConstants.GET_DCS_CODE, new GET_DCS_CODE());
        commandsHashMap.put(TelemetryRadioConstants.SET_MEMORY_CHANNELNR, new SET_MEMORY_CHANNELNR());
        commandsHashMap.put(TelemetryRadioConstants.GET_MEMORY_CHANNELNR, new GET_MEMORY_CHANNELNR());
        commandsHashMap.put(TelemetryRadioConstants.SET_MEMORY_CHANNELDATA, new SET_MEMORY_CHANNELDATA());
        commandsHashMap.put(TelemetryRadioConstants.GET_MEMORY_CHANNELDATA, new GET_MEMORY_CHANNELDATA());
        commandsHashMap.put(TelemetryRadioConstants.SET_MEMORYBANK_NR, new SET_MEMORYBANK_NR());
        commandsHashMap.put(TelemetryRadioConstants.GET_INFO, new GET_INFO());
        commandsHashMap.put(TelemetryRadioConstants.SET_RIT, new SET_RIT());
        commandsHashMap.put(TelemetryRadioConstants.GET_RIT, new GET_RIT());
        commandsHashMap.put(TelemetryRadioConstants.GET_VFO, new GET_VFO());
        commandsHashMap.put(TelemetryRadioConstants.SET_XIT, new SET_XIT());
        commandsHashMap.put(TelemetryRadioConstants.GET_XIT, new GET_XIT());
        commandsHashMap.put(TelemetryRadioConstants.GET_ANTENNA_NR, new GET_ANTENNA_NR());
        commandsHashMap.put(TelemetryRadioConstants.RESET, new RESET());
        commandsHashMap.put(TelemetryRadioConstants.SEND_RAW_CMD, new SEND_RAW_CMD());
        commandsHashMap.put(TelemetryRadioConstants.SEND_MORSE, new SEND_MORSE());
        commandsHashMap.put(TelemetryRadioConstants.CONVERT_POWER2MW, new CONVERT_POWER2MW());
        commandsHashMap.put(TelemetryRadioConstants.CONVERT_MW2POWER, new CONVERT_MW2POWER());
        commandsHashMap.put(TelemetryRadioConstants.HELP, new HELP());
        commandsHashMap.put(TelemetryRadioConstants.RadioStatus, new RadioStatus());
        
    }

    @Override
    protected Object encode(ChannelHandlerContext ctx, Channel channel, Object message) throws Exception {
        if(message==null){
            return ChannelBuffers.EMPTY_BUFFER;
        }          
        
        TelemetryCommand telemetryCommand = (TelemetryCommand) message;
        StringBuilder messageString=new StringBuilder();


        messageString.append(commandsHashMap.get(telemetryCommand.getName()).createMessageString(telemetryCommand));
        LOG.debug("SENT: {}, that is: {}",telemetryCommand.getName(), messageString.toString());
        
        return messageString;
    }
}
