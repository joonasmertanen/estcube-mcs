package eu.estcube.gsconnector.commands;

import eu.estcube.domain.TelemetryCommand;

public class CommandStringBuilder {
    protected StringBuilder messageString;
    


    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        return messageString;
    }

}
