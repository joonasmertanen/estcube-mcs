package eu.estcube.gsconnector;

import java.nio.charset.Charset;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("my-encoder")
public class Encoder extends SimpleChannelHandler {

    private static final Logger LOG = LoggerFactory.getLogger(Encoder.class);
    @Override
    public void writeRequested(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        byte[] bytes = checkEndLine(e);
        if (!(bytes.length==0)){
            ChannelBuffer buffer = ChannelBuffers.buffer(bytes.length);
            buffer.writeBytes(bytes);
            Channels.write(ctx, e.getFuture(), buffer);            
        }

    }
    
    public byte[] checkEndLine(MessageEvent e){
        String str = (String) e.getMessage().toString();
        if(str.length()==0){
            return null;
        }
//        if(!str.startsWith("+")||!str.endsWith("1")){
//            str="+"+str;
//        }
//            
        if(!str.endsWith("\n")){
            str+="\n";
        }
        
        byte[] bytes = str.getBytes(Charset.forName("ASCII"));
        return bytes;
    }

}
