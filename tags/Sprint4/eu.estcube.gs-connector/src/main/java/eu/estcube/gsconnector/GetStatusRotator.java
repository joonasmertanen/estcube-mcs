package eu.estcube.gsconnector;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("getStatusRotator")
public class GetStatusRotator implements Processor {
    
    ProducerTemplate producer;

    private static final Logger LOG = LoggerFactory.getLogger(GetStatusRotator.class);
    
    public void setProducer(ProducerTemplate producer) {
      this.producer = producer;
    }

    
    public void process(Exchange inExchange) {

      // some loop for each message 
      for (int i = 0; i<10;i++) {
          
//          inExchange.getContext().createProducerTemplate().sendBody("direct:lol", i+"number");
//          LOG.debug("Message piece 1: {}", inExchange.getIn().getBody().toString());  
//          this.producer.sendBodyAndHeader("direct:lol", "This is the body", "Gold", 
//                  "<hello>world!</hello>");
//          
          
              inExchange.getContext().createProducerTemplate().send("direct:lol", new Processor() {
                  public void process(Exchange outExchange) {
                      outExchange.getOut().setBody("This is the body");
                      // set some headers too?
                  }
               });
      }
  }
}
