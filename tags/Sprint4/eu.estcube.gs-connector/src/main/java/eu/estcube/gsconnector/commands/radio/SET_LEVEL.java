package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SET_LEVEL extends CommandStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        messageString.append("+");
        messageString.append("L");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Level"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Level Value"));
        messageString.append("\n");
        return messageString;
    }
}
