/** 
 *
 */
package eu.estcube.gsconnector;

import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import eu.estcube.domain.TelemetryCommand;

@Component("aeg")
public class TimeRequestProcessor implements Processor {
    
    public void process(Exchange exchange) throws Exception {
        TelemetryCommand request = exchange.getIn().getBody(TelemetryCommand.class);
        String message = String.format("%s - %s", System.getProperty("user.name"), new Date());
        TelemetryCommand response = new TelemetryCommand(request.getName());
        response.addParameter("command", message);
        exchange.getOut().setBody(response, TelemetryCommand.class);
    }
}