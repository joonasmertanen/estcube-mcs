package eu.estcube.gsconnector;



import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jms.JmsMessageType;
import org.apache.camel.spring.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import eu.estcube.domain.JMSConstants;
import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.domain.TelemetryRotatorConstants;

public class Connector extends RouteBuilder{

    private static final Logger LOG = LoggerFactory.getLogger(Connector.class);

    @Autowired
    Processor getStatusRotator;
    
    @Autowired
    private TimeRequestProcessor timeRequestProcessor;
    
    @Value("${rotctldAddress}")
    private String rotAddress;
    
    @Value("${rigctldAddress}")
    private String rigAddress;
   
    @Value("${gsName}")
    private String gsName;

    
    @Value("${timerFireInterval}")
    private String timerFireInterval;
    
    @Override
    public void configure() throws Exception {
        LOG.info("Starting Connector");
        String netty_rigctld= "netty:tcp://"
                +rigAddress
                    +"?sync=true&decoders=#newLineDecoder,#stringDecoder,#hamlibDecoder,#radioDecoder&encoders=#my-encoder,#radioEncoder";
        String netty_rotctld= "netty:tcp://"
                +rotAddress
                    +"?sync=true&decoders=#newLineDecoder,#stringDecoder,#hamlibDecoder,#rotatorDecoder&encoders=#my-encoder,#rotatorEncoder";
        
        
        /*
         * **==========================>>REAL ROUTES<<============================**
         */

        /*
         * Universal tunnels
         */
        //Incoming messages
        from(JMSConstants.gsRecive)
            .log("log:DebugRecievedMessage")
            .choice()
                .when(header("groundStationID").isEqualTo(gsName))
                    .choice()
                        .when(header("device").isEqualTo("rigctld"))
                            .to("seda:rigctld")
                        .when(header("device").isEqualTo("rotctld"))
                            .to("seda:rotctld")      
                        .when(header("device").isEqualTo("status"))
                            .to("direct:status")         
                 .otherwise()
                    .log("log:DebugMessageDropped");
                    
        //Outgoing messages
        from("direct:device")
            .log("log:DEBUGgsSEND")
            .to(JMSConstants.gsSend);
        
        //status
        from("direct:status")
            .multicast()
            .parallelProcessing()
            .to("direct:rigStatus", "direct:rotStatus"); 
        

        
       
        /*
         * rigctld
         */
        //Direction to the device
        from("seda:rigctld")
            .log("log:DebugRigDeviceBeforeResponse")
            .to(netty_rigctld)
            .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    exchange.getIn().setHeader("groundStationID", gsName);
                    exchange.getIn().setHeader("device", "rigctld");
                }
             })
            .to("log:DebugRigDevice")
            .to("direct:device");

        
        //Periodical time fire rigctld
        from("timer://rigTimer?period="+timerFireInterval)
            .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRotatorConstants.CAPABILITIES));
                }
             })
            .to("log:DebugRigPeriodicalTimeFire")
            .to("seda:rigctld");
        
        //STATUS ROUTES
        from("direct:rigStatus")
//            .process(new Processor() {
//                public void process(Exchange exchange) throws Exception {
//                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRadioConstants.GET_ANTENNA_NR));
//                }
//             })
//            .to("seda:rigctld")
//            .process(new Processor() {
//                public void process(Exchange exchange) throws Exception {
//                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRadioConstants.GET_CTCSS_TONE));
//                }
//             })
//            .to("seda:rigctld")
//            .process(new Processor() {
//                public void process(Exchange exchange) throws Exception {
//                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRadioConstants.GET_DCS_CODE));
//                }
//             })
//             .to("seda:rigctld")
//             .process(new Processor() {
//                public void process(Exchange exchange) throws Exception {
//                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRadioConstants.GET_FREQUENCY));
//                }
//             })
//             .to("seda:rigctld")
//            .process(new Processor() {
//                public void process(Exchange exchange) throws Exception {
//                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRadioConstants.GET_MEMORY_CHANNELNR));
//                }
//             })
//             .to("seda:rigctld")
//             .process(new Processor() {
//                public void process(Exchange exchange) throws Exception {
//                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRadioConstants.GET_MODE));
//                }
//             })
//             .to("seda:rigctld")
//             .process(new Processor() {
//                public void process(Exchange exchange) throws Exception {
//                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRadioConstants.GET_PTT));
//                }
//             })
//             .to("seda:rigctld")
//             .process(new Processor() {
//                public void process(Exchange exchange) throws Exception {
//                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRadioConstants.GET_RIT));
//                }
//             })
//             .to("seda:rigctld")
//             .process(new Processor() {
//                public void process(Exchange exchange) throws Exception {
//                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRadioConstants.GET_RPTR_OFFSET));
//                }
//             })
//             .to("seda:rigctld")
//             .process(new Processor() {
//                public void process(Exchange exchange) throws Exception {
//                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRadioConstants.GET_RPTR_SHIFT));
//                }
//             })
//             .to("seda:rigctld")
//             .process(new Processor() {
//                public void process(Exchange exchange) throws Exception {
//                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRadioConstants.GET_SPLIT_VFO));
//                }
//             })
//             .to("seda:rigctld")
//             .process(new Processor() {
//                public void process(Exchange exchange) throws Exception {
//                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRadioConstants.GET_TRANCEIVE_MODE));
//                }
//             })
//             .to("seda:rigctld")
//             .process(new Processor() {
//                public void process(Exchange exchange) throws Exception {
//                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRadioConstants.GET_TRANSMIT_FREQUENCY));
//                }
//             })
//             .to("seda:rigctld")
//             .process(new Processor() {
//                public void process(Exchange exchange) throws Exception {
//                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRadioConstants.GET_TRANSMIT_MODE));
//                }
//             })
//             .to("seda:rigctld")            
//             .process(new Processor() {
//                public void process(Exchange exchange) throws Exception {
//                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRadioConstants.GET_TUNING_STEP));
//                }
//             })
//             .to("seda:rigctld")
//             .process(new Processor() {
//                public void process(Exchange exchange) throws Exception {
//                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRadioConstants.GET_VFO));
//                }
//             })
//             .to("seda:rigctld")
//             .process(new Processor() {
//                public void process(Exchange exchange) throws Exception {
//                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRadioConstants.GET_XIT));
//                }
//             })
//             .to("seda:rigctld");  
              .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    exchange.getIn().setBody(new TelemetryCommand("RadioStatus"));
                }
             })
             .to("seda:rigctld");
        
        
        /*
         * rotctld
         */
        //Direction to the device
        from("seda:rotctld")
            .log("log:DebugRotDeviceBeforeResponse")
            .to(netty_rotctld)
            .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    exchange.getIn().setHeader("groundStationID", gsName);
                    exchange.getIn().setHeader("device", "rotctld");
                }
             })
            .to("log:DebugRotDevice")
            .to("direct:device");

        
        //Periodical time fire rotctld
        from("timer://rotTimer?period="+timerFireInterval)
            .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRotatorConstants.CAPABILITIES));
                }
             })
            .log("log:DebugRotPeriodicalTimeFire")
            .to("seda:rotctld");
        
        //STATUS ROUTES
        from("direct:rotStatus")
            .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRotatorConstants.GET_POSITION));
                }
             })
             .to("seda:rotctld");
                
        //sudo
        //sudo rpc.rotd -m 1 -vvvv
        //sudo rotctld -m 101 -vvvvv  //4533
        
        //rigctld -m 1901 -vvvvv  //4532
        //rotctl --help
        //nr 2 opn net TCP
        //rotctl -m 2 -r localhost:4533
        // ENDLINE CODES:  \n ; RPTR x\n
        
        
        
        /*
         * **==========================>>TESTROUTES<<============================**
         * FROM: http://docs.jboss.org/netty/3.2/guide/html_single/index.html
         * Additionally, Netty provides out-of-the-box decoders which enables you to implement most protocols very easily and helps you 
         * avoid from ending up with a monolithic unmaintainable handler implementation. Please refer to the following packages for 
         * more detailed examples: 
         *   org.jboss.netty.example.factorial for a binary protocol, and 
         *   org.jboss.netty.example.telnet for a text line-based protocol.
         *   Camel + Netty demo routes
         */
        
        from("stream:in")
            .to("direct:status");

//            .beanRef("getStatusRotator")
//            .to("netty:tcp://localhost:4533?sync=true&decoders=#newLineDecoder,#stringDecoder,#hamlibDecoder&encoders=#my-encoder")
//            .to("log:echo");
//            .to("netty:tcp://localhost:10111?sync=true")
//            .setBody().simple("p")

//            .to(netty_rotctld)
//            .to("log:result");
//
//        from("netty:tcp://localhost:10111")
//            .process(new Processor() {
//                public void process(Exchange exchange) throws Exception {
//                    LOG.debug("IN:" + exchange.getIn().getBody());
//                    exchange.getOut().setBody("response: " + exchange.getIn().getBody());
//                    LOG.debug("IN:" + exchange.getIn().getBody());
//                }
//            });
//        
//        //Time request demo route
//        from("activemq:topic:time-request")
//            .beanRef("aeg")
//            .to("log:time-response")
//            .to("activemq:topic:time-response")
//            .end();
        

    }
    public static void main(String[] args) throws Exception {
        LOG.info("Starting Connector");
        LOG.info("Starting Camel");
        new Main().run(args);

    }




}
