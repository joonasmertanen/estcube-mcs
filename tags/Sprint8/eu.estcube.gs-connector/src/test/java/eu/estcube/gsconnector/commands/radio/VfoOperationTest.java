package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class VfoOperationTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private VfoOperation createMessage = new VfoOperation();
    
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand(TelemetryRadioConstants.VFO_OPERATION);
        telemetryCommand.addParameter("Mem/VFO Op", "CPY");
        string.append("+G CPY\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
