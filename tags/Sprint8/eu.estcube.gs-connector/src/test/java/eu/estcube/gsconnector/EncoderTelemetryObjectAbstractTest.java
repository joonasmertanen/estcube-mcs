package eu.estcube.gsconnector;


import static org.junit.Assert.assertEquals;

import org.jboss.netty.buffer.ChannelBuffers;
import org.junit.Test;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.domain.TelemetryRotatorConstants;

public class EncoderTelemetryObjectAbstractTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private EncoderTelemetryObjectAbstract encode;

    @Test
    public void testRadioEncode() throws Exception
    {
        encode = new EncoderTelemetryObjecRadio();
        telemetryCommand = null;
        Object test = encode.encode(null, null, telemetryCommand);
        assertEquals(ChannelBuffers.EMPTY_BUFFER, test);

        telemetryCommand = new TelemetryCommand(TelemetryRadioConstants.GET_MODE);
        string.append("+m\n");
        assertEquals(string.toString(), encode.encode(null, null, telemetryCommand).toString());
    }
    
    @Test
    public void testRotatorEncode() throws Exception
    {
        encode = new EncoderTelemetryObjecRotator();
        telemetryCommand = null;
        Object test = encode.encode(null, null, telemetryCommand);
        assertEquals(ChannelBuffers.EMPTY_BUFFER, test);

        telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.STOP);
        string.append("+S\n");
        assertEquals(string.toString(), encode.encode(null, null, telemetryCommand).toString());
    }

}
