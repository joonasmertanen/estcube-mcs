package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SetRptrOffset implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+O ");
        messageString.append(telemetryCommand.getParams().get("Rptr Offset"));
        messageString.append("\n");
        return messageString;
    }
}
