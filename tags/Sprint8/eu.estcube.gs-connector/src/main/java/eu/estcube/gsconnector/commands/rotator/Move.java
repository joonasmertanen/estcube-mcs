package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class Move implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+M ");
        messageString.append(telemetryCommand.getParams().get("Direction"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Speed"));
        messageString.append("\n");
        return messageString;
    }
}
