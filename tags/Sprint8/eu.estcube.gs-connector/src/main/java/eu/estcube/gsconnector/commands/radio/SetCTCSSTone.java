package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SetCTCSSTone implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+C ");
        messageString.append(telemetryCommand.getParams().get("CTCSS Tone"));
        messageString.append("\n");
        return messageString;
    }
}
