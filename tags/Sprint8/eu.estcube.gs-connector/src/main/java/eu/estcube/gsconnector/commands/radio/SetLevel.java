package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SetLevel implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+L ");
        messageString.append(telemetryCommand.getParams().get("Level"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Level Value"));
        messageString.append("\n");
        return messageString;
    }
}
