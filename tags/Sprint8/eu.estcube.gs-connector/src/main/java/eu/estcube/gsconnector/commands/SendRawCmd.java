package eu.estcube.gsconnector.commands;

import eu.estcube.domain.TelemetryCommand;

public class SendRawCmd implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+w ")
            .append(telemetryCommand.getParams().get("Cmd"))
            .append("\n");
        return messageString;
    }
}
