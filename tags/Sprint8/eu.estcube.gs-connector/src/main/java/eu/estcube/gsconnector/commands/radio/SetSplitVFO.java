package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SetSplitVFO implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+S ");
        messageString.append(telemetryCommand.getParams().get("Split"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("TX VFO"));
        messageString.append("\n");
        return messageString;
    }
}
