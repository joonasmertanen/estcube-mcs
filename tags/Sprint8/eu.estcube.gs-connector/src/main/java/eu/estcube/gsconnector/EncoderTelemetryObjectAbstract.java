package eu.estcube.gsconnector;

import java.util.Map;

import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public abstract class EncoderTelemetryObjectAbstract extends OneToOneEncoder{

    protected abstract Map<String, CommandStringBuilder> getCommandsHashMap();
    
   
    protected Object encode(ChannelHandlerContext ctx, Channel channel, Object message) throws Exception {
        if (message == null) {
            return ChannelBuffers.EMPTY_BUFFER;
        }
        TelemetryCommand telemetryCommand = (TelemetryCommand) message;
        StringBuilder messageString = new StringBuilder();
        messageString.append(getCommandsHashMap().get(telemetryCommand.getName()).createMessageString(telemetryCommand));
        return messageString;
    }
}
