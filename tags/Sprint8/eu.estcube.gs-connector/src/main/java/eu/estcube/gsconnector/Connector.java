package eu.estcube.gsconnector;



import java.io.IOException;
import java.nio.channels.ClosedChannelException;

import org.apache.camel.CamelExchangeException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import eu.estcube.domain.JMSConstants;
import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.domain.TelemetryRotatorConstants;

public class Connector extends RouteBuilder{

    private static final Logger LOG = LoggerFactory.getLogger(Connector.class);
   
    @Autowired
    private SplitterRadioStatus radioStatusSplitter;

    @Autowired
    private SplitterRotatorStatus rotatorStatusSplitter;
    
    @Autowired
    private PollStoreSetValues storeSetValues;
    
    @Value("${rotctldAddress}")
    private String rotAddress;
    
    @Value("${rigctldAddress}")
    private String rigAddress;
   
    @Value("${gsName}")
    private String gsName;
    
    @Value("${timerFireInterval}")
    private String timerFireInterval;
    
    @Value("${pollDelayInterval}")
    private int pollDelayInterval;
    
    @Value("${version}")
    private String connectorVersion;
    
    private boolean getRotStatus = true;
    private boolean getRigStatus = true;
    
    @Override
    public void configure() throws Exception {
        String nettyRigCtld = "netty:tcp://" + rigAddress + "?sync=true&decoders=#" + "radioDecoderNewLine"
                + ",#" + "radioDecoderToString" + ",#" + "radioDecoderHamlib" + ",#"
                + JMSConstants.GS_RADIO_PROTOCOL_DECODER + "&encoders=#" + JMSConstants.GS_ENCODE_STR_TO_BYTE + ",#"
                + JMSConstants.GS_RADIO_PROTOCOL_ENCODER;

        String nettyRotCtld = "netty:tcp://" + rotAddress + "?sync=true&decoders=#" + "rotatorDecoderNewLine"
                + ",#" + "rotatorDecoderToString" + ",#" + "rotatorDecoderHamlib" + ",#"
                + JMSConstants.GS_ROTATOR_PROTOCOL_DECODER + "&encoders=#" + JMSConstants.GS_ENCODE_STR_TO_BYTE + ",#"
                + JMSConstants.GS_ROTATOR_PROTOCOL_ENCODER;
        
        /*
         * **==========================>>REAL ROUTES<<============================**
         */

        /*
         * Universal tunnels
         */
        //Incoming messages - Route 1
        from(JMSConstants.AMQ_GS_RECEIVE)
            .log("log:RecievedMessage")
            .choice()
                .when(header(JMSConstants.HEADER_GROUNDSTATIONID).isEqualTo(gsName)) 
                    .to(JMSConstants.DIRECT_CHOOSE_DEVICE)
                .when(header(JMSConstants.HEADER_GROUNDSTATIONID).isEqualTo(JMSConstants.GS_ALL_NAMES)) 
                    .to(JMSConstants.DIRECT_CHOOSE_DEVICE)
                .otherwise()
                    .log("log:WrongStation");
        
        from(JMSConstants.DIRECT_CHOOSE_DEVICE)
            .bean(storeSetValues)
            .choice()
                .when(header(JMSConstants.HEADER_DEVICE).isEqualTo(JMSConstants.GS_RIG_CTLD))
                    .to(JMSConstants.DIRECT_RIG_CTLD, JMSConstants.DIRECT_POLL_COMMAND)
                .when(header(JMSConstants.HEADER_DEVICE).isEqualTo(JMSConstants.GS_ROT_CTLD))
                    .to(JMSConstants.DIRECT_ROT_CTLD, JMSConstants.DIRECT_POLL_COMMAND)      
                .when(header(JMSConstants.HEADER_DEVICE).isEqualTo("status"))
                    .to(JMSConstants.DIRECT_STATUS)
                .otherwise()
                    .log("log:InvalidDevice");
                    
        //monitors status updates - Route 3
        from(JMSConstants.DIRECT_POLL_COMMAND)
            .split().method(JMSConstants.CLASS_POLL_COMMANDS, "checkPolling")
            .delay(pollDelayInterval)
            .recipientList(header(JMSConstants.HEADER_FORWARD));
        
        //Outgoing messages - Route 4
        from(JMSConstants.DIRECT_SEND)
            .log("log:SendMessage")
            .to(JMSConstants.AMQ_GS_SEND);
        
        //status - Route 5
        from(JMSConstants.DIRECT_STATUS)
            .multicast()
            .parallelProcessing()
            .to(JMSConstants.DIRECT_RIG_STATUS)
            .to(JMSConstants.DIRECT_ROT_STATUS); 
       
//        /*
//         * rigctld
//         */
        //Direction to the device - Route 6
        from(JMSConstants.DIRECT_RIG_CTLD)
            .log("log:DebugRigDeviceBeforeResponse")
            .doTry()
                .to(nettyRigCtld)
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        String forwardRoute = JMSConstants.DIRECT_SEND;
                        exchange.getIn().setHeader(JMSConstants.HEADER_GROUNDSTATIONID, gsName);
                        exchange.getIn().setHeader(JMSConstants.HEADER_DEVICE, JMSConstants.GS_RIG_CTLD);
                        if (getRigStatus) {
                            forwardRoute += "," + JMSConstants.DIRECT_RIG_STATUS;
                            getRigStatus = false;
                        }
                        exchange.getIn().setHeader(JMSConstants.HEADER_FORWARD, forwardRoute);
                    }
                })
            .doCatch(IOException.class, Exception.class, CamelExchangeException.class, ClosedChannelException.class)
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        LOG.warn("Radio channel is closed!");
                        getRigStatus = true;
                    }
                 })
            .end();

        
        //Periodical time fire rigctld - Route 7
        from("timer://rigTimer?period="+timerFireInterval)
            .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRadioConstants.CAPABILITIES));
                }
             })
            .log("log:DebugRigPeriodicalTimeFire")
            .doTry()
                .to(JMSConstants.DIRECT_RIG_CTLD)
                .recipientList(header(JMSConstants.HEADER_FORWARD))
            .endDoTry()
            .doCatch(IOException.class, Exception.class, CamelExchangeException.class, ClosedChannelException.class)
                .log("Timerfire failed, probably channel closed")
            .end();
        
        
        //STATUS ROUTES - Route 8
        from(JMSConstants.DIRECT_RIG_STATUS)
            .split().method(radioStatusSplitter, "splitMessage")
            .to(JMSConstants.DIRECT_RIG_CTLD, JMSConstants.DIRECT_SEND);
        

        
        /*
         * rotctld
         */
        //Direction to the device - Route 9
        from(JMSConstants.DIRECT_ROT_CTLD)
            .log("log:DebugRotDeviceBeforeResponse")
            .doTry()
                .to(nettyRotCtld)
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        String forwardRoute = JMSConstants.DIRECT_SEND;
                        exchange.getIn().setHeader(JMSConstants.HEADER_GROUNDSTATIONID, gsName);
                        exchange.getIn().setHeader(JMSConstants.HEADER_DEVICE, JMSConstants.GS_ROT_CTLD);
                        if (getRotStatus) {
                            forwardRoute += "," + JMSConstants.DIRECT_ROT_STATUS;
                            getRotStatus = false;
                        }
                        exchange.getIn().setHeader(JMSConstants.HEADER_FORWARD, forwardRoute);
                    }
                })
            .doCatch(IOException.class, Exception.class, CamelExchangeException.class, ClosedChannelException.class)
                .process(new Processor() {
                    public void process(Exchange exchange) throws Exception {
                        LOG.warn("Rotator channel is closed!");
                        getRotStatus = true;
                    }
                })
            .end();

        
        //Periodical time fire rotctld - Route 10
        from("timer://rotTimer?period="+timerFireInterval)
            .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    exchange.getIn().setBody(new TelemetryCommand(TelemetryRotatorConstants.CAPABILITIES));
                }
             })
            .log("log:DebugRotPeriodicalTimeFire")
            .doTry()
                .to(JMSConstants.DIRECT_ROT_CTLD)
                .recipientList(header(JMSConstants.HEADER_FORWARD))
            .endDoTry()
            .doCatch(IOException.class, Exception.class, CamelExchangeException.class, ClosedChannelException.class)
                .log("Timerfire failed, probably channel closed")
            .end();
        
        //STATUS ROUTES - Route 11
        from(JMSConstants.DIRECT_ROT_STATUS)
            .split().method(rotatorStatusSplitter, "splitMessage")
            .to(JMSConstants.DIRECT_ROT_CTLD, JMSConstants.DIRECT_SEND);
        
//        //Hamlib info
//        //sudo rpc.rotd -m 1 -vvvv
//        //sudo rotctld -m 1 -vvvvv  //4533
//        //sudo rpc.rigd -m 1 -vvvv
//        //sudo rigctld -m 1901 -vvvvv  //4532

        /*
         * **==========================>>TESTROUTE<<============================**
         */
        
        from("stream:in")
                    .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    exchange.getIn().setHeader(JMSConstants.HEADER_DEVICE, JMSConstants.GS_RIG_CTLD);
                    TelemetryCommand telemetryCommand = new TelemetryCommand(TelemetryRadioConstants.SET_FREQUENCY);
                    telemetryCommand.addParameter("Frequency", 143.456);
                    exchange.getIn().setBody(telemetryCommand);
                }
             })
            .to(JMSConstants.DIRECT_CHOOSE_DEVICE);
        
        from(JMSConstants.AMQ_VERSIONS_REQUEST).process(new Processor() {
            public void process(Exchange ex) throws Exception {
                ex.getOut().setHeader(JMSConstants.HEADER_GROUNDSTATIONID, gsName);
                ex.getOut().setHeader(JMSConstants.HEADER_COMPONENT, JMSConstants.COMPONENT_CONNECTOR);
                ex.getOut().setBody(connectorVersion, String.class);          
            }            
        }).to(JMSConstants.AMQ_VERSIONS_RECEIVE);
        
        from("timer://sendVersion?repeatCount=1").process(new Processor() {
            public void process(Exchange ex) throws Exception {
                ex.getOut().setHeader(JMSConstants.HEADER_GROUNDSTATIONID, gsName);
                ex.getOut().setHeader(JMSConstants.HEADER_COMPONENT, JMSConstants.COMPONENT_CONNECTOR);
                ex.getOut().setBody(connectorVersion, String.class);          
            }            
        }).to(JMSConstants.AMQ_VERSIONS_RECEIVE);
    }
    public static void main(String[] args) throws Exception {
        LOG.info("Starting Connector");
        new Main().run(args);
    }
}
