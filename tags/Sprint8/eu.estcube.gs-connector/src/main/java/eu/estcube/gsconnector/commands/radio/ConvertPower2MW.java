package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class ConvertPower2MW implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+2 ");
        messageString.append(telemetryCommand.getParams().get("Power"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Frequency"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Mode"));
        messageString.append("\n");
        return messageString;
    }
}
