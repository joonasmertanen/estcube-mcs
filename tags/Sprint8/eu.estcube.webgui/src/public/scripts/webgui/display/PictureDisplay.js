dojo.provide("webgui.display.PictureDisplay");

dojo.require("dijit.form.TextBox");
dojo.require("dijit.form.Button");
dojo.require("dijit.layout.ContentPane");
dojo.require("dijit.layout.StackContainer");
dojo.require("dojox.socket");
dojo.require("dojox.gfx");

var img;
dojo.declare("webgui.display.PictureDisplay", null, {

	constructor : function() {
		console.log("PictureDisplay!");
		var pictureTab = new dijit.layout.ContentPane({
			id : "pictureTab",
			title : "Picture",
			preventCache : true
		});

		var tempCont;
		tempCont = dijit.byId("PictureView");
		tempCont.addChild(pictureTab);

		var surfaceVar = dojo.create("div", {
			id : "surfaceElement",
			region : "center",
            style:"padding: 55px"
		}, "pictureTab");


		img = dojo.doc.createElement('img');
		dojo.attr(img, {
			src : "image.jpg",
			alt : "Webcam image"
		});
		dojo.place(img, "surfaceElement", "first");
		dojo.subscribe("fromWebCam", updateImage);
	}
});

var updateImage = function(message) {
	dojo.attr(img, {
		src : "data:image/jpeg;base64," + message + "",
		alt : "Webcam image"
	});
};