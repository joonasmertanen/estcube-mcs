dojo.provide("webgui.display.Graph");
dojo.require("dojox.charting.Chart");
dojo.require("dojox.charting.themes.Claro");
dojo.require("dojox.charting.StoreSeries");
dojo.require("dojox.charting.action2d.Tooltip");
dojo.require("dojox.charting.action2d.MoveSlice");
dojo.require("dojox.charting.widget.Legend");
dojo.require("dojox.data.PersevereStore");
dojo.require("dojo.date.locale");

dojo.declare("webgui.display.Graph", null,{

    OFFSET_FROM_CONTAINER_BORDER: 10,
    MINIMUM_CONTAINER_SIZE: 46,
    MINIMUM_HEIGTH: 60,
    MINIMUM_WIDTH: 300,
    LABEL_TIMESTAMP_FORMAT: "HH:mm:ss",
    LEGEND_NAME: "legend",

    chart: null,
    countStoreItemsTowardMaxSize: 0,
    countItemsInStore: 0,
    key: 1,
    store: null,
    storedata: null,
    storeMaxSize: null,
    storeMaxSize: 10,
    chartArea: null,
    legend: null,
    yMinValue: -180,
    yMaxValue: 180,
        
    constructor: function(chartArea, subscriptionString, graphYRange)
    {
        this.yMinValue = graphYRange.minY;
        this.yMaxValue = graphYRange.maxY;
        this.chartArea = chartArea;
        this.storedata = { identifier: "key", items: [] };
        this.store = new dojo.data.ItemFileWriteStore({ data: this.storedata });
        this.chart = new dojox.charting.Chart(this.chartArea.id);
	    this.chart.setTheme(dojox.charting.themes.Claro);
        this.chart.addPlot("default", {
            type: "Lines",
            markers: true,
            lines: true,
            labelOffset: -30,
            shadows: { dx:2, dy:2, dw:2 }
        });

        this.chart.addAxis("x", {
            labelFunc: function(str, value){
            return dojo.date.locale.format(new Date(value), {datePattern: this.LABEL_TIMESTAMP_FORMAT, selector: "date"});
        },
            majorTickStep: 60000,
            majorLabels: true,
            minorTicks: false,
            minorLabels: true,
            microTicks: false
        });

        this.chart.addAxis("y", { vertical: true, min: this.yMinValue, max: this.yMaxValue });
        
        if(chartArea.offsetWidth > this.MINIMUM_CONTAINER_SIZE && chartArea.offsetHeight > this.MINIMUM_CONTAINER_SIZE){
            this.chart.resize(dojo.hitch(this, chartArea.offsetWidth-this.OFFSET_FROM_CONTAINER_BORDER), dojo.hitch(this, chartArea.offsetHeight-this.OFFSET_FROM_CONTAINER_BORDER));
        }else{
            this.chart.resize(this.MINIMUM_WIDTH, this.MINIMUM_HEIGTH);
        }

        new dojox.charting.action2d.Tooltip(this.chart, "default");
        new dojox.charting.action2d.Magnify(this.chart, "default");

        dojo.connect(window, "onresize", this, function(evt) {
            if(chartArea.offsetWidth > this.MINIMUM_CONTAINER_SIZE && chartArea.offsetHeight > this.MINIMUM_CONTAINER_SIZE){
                this.chart.resize(dojo.hitch(this, chartArea.offsetWidth-this.OFFSET_FROM_CONTAINER_BORDER), dojo.hitch(this, chartArea.offsetHeight-this.OFFSET_FROM_CONTAINER_BORDER));
            }
        });

        dojo.connect(this.store, "newItem", dojo.hitch(this, this.updateViewCallback));
        dojo.subscribe(subscriptionString, dojo.hitch(this, this.handleParameter));
    },

        //Adds new data to series
        getSeriesData: function(itemName) {
            var seriesExists = false;
            var seriesArrayData;

            dojo.forEach(this.chart.series, function(entry, key) {
                if (entry.name == itemName) {
                    seriesExists = true;
                    seriesArrayData = entry.data;
                }
            });

            if (!seriesExists) {
                return false;
            }

            if (seriesArrayData.length >= this.storeMaxSize) {
                seriesArrayData.splice(0, seriesArrayData.length - this.storeMaxSize);
            }
            return seriesArrayData;
        },

        //Renders the chart and updates the legend
        updateView: function() {
            this.chart.render();
            if (this.legend) {
                this.legend.destroy();
            }
            this.chartArea.appendChild(dojo.create("div", { id: this.LEGEND_NAME }));
            this.legend = new dojox.charting.widget.Legend({ chart:this.chart }, this.LEGEND_NAME);
        },

        //Pushes new data to series, defines major tick step size, calls chart update
        updateViewCallback: function(item) {
            var seriesArrayData = this.getSeriesData(item[webgui.common.Constants.telemetryCommandName]);
            var jsdate = webgui.common.Constants.parseDate(item[webgui.common.Constants.telemetryCommandTime]);
            var tooltipString = item[webgui.common.Constants.telemetryCommandName]+"<br/>" + item[webgui.common.Constants.telemetryCommandValue]+"<br/>" + webgui.common.Constants.formatDate(jsdate);
            if (seriesArrayData === false) {
                this.chart.addSeries(item[webgui.common.Constants.telemetryCommandName], [{ x: jsdate, y: item[webgui.common.Constants.telemetryCommandValue], tooltip: tooltipString}]);
            } else {
                seriesArrayData.push({ x: jsdate, y: item[webgui.common.Constants.telemetryCommandValue], tooltip: tooltipString});
                this.chart.updateSeries(item[webgui.common.Constants.telemetryCommandName], seriesArrayData);
                this.chart.getAxis("x").opt.majorTickStep = (seriesArrayData[seriesArrayData.length-1].x.getTime() - seriesArrayData[0].x.getTime())/5;
                this.updateView();
            }
        },

        //Adds new data to store, removes old data from store.
        handleParameter: function (myObject) {
            for(var i = 1; i<myObject.params.length-1; i++){
                var storeElem = {};
                storeElem[webgui.common.Constants.telemetryCommandValue] = myObject.params[i][webgui.common.Constants.telemetryCommandValue];
                storeElem[webgui.common.Constants.telemetryCommandName] = myObject.params[i][webgui.common.Constants.telemetryCommandName];
                storeElem[webgui.common.Constants.telemetryCommandTime] = myObject[webgui.common.Constants.telemetryCommandTime];
                storeElem.key = this.key;
                this.store.newItem(storeElem);
                this.key++;
                this.countStoreItemsTowardMaxSize++;
                
                if (this.countStoreItemsTowardMaxSize > this.storeMaxSize*(myObject.params.length-2)) {
                    // getting the size of the store
                    var getStoreSize = function(size, request) {
                        console.log(myObject);
                        // remove excess elements
                        this.store.fetch({ countItemsInStore: (size - this.storeMaxSize*(myObject.params.length-2)),
                            onItem: dojo.hitch(this, function(item) {
                                this.store.deleteItem(item);
                            })
                        });
                    };
                    this.store.fetch({ query: {}, onBegin: dojo.hitch(this, getStoreSize), start: 0, countItemsInStore: 0 });
                }
            }
        }
    }
);