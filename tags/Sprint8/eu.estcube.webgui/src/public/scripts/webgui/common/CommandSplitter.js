dojo.provide("webgui.common.CommandSplitter");

splitCommand = function(selectedTarget, commandString, split) {
	console.log("splitCommand activated! - " + commandString);

	var params = new Array();

	console.log(split.length % 2);

	if((split.length - 1) % 2 == 0) {
		for( i = 1; i < split.length - 1; i = i + 2) {
			params.push(new TelemetryParameter(split[i], split[i + 1]));
		}
	}
	
	var telemetryCommand = new TelemetryCommand(split[0], params, selectedTarget);
	console.log("CMDsplitter: " + JSON.stringify(telemetryCommand));
	console.log("Target: " + selectedTarget);

	
	dojo.publish("toWS", [JSON.stringify(telemetryCommand)]);
	
	return telemetryCommand;
}

function TelemetryParameter(name, value) {
			this.name = name;
			this.value = value;
		}
		
		// add device
		function TelemetryCommand(name, params, device) {
			this.device = device;
			this.name = name;
			this.params = params;
		}

