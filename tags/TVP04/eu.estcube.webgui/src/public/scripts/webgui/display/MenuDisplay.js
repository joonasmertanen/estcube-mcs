dojo.provide("webgui.display.MenuDisplay");
dojo.require("dijit.MenuBar");
dojo.require("dijit.MenuBarItem");
dojo.require("dijit.PopupMenuBarItem");
dojo.require("dijit.Menu");
dojo.require("dijit.MenuItem");
dojo.require("dijit.layout.BorderContainer");
dojo.require("dijit.layout.ContentPane");
dojo.require("webgui.display.StatusesTableDisplay");
dojo.require("webgui.display.PictureDisplay");
dojo.require("webgui.display.InfoDisplay");
dojo.require("webgui.display.MainTabDisplay");
dojo.require("webgui.display.StatusesInfoDisplay");
dojo.require("webgui.display.StatusesGraphDisplay");
dojo.require("dijit.Dialog");
	
  	

dojo.declare("MenuDisplay", null, {

	constructor : function() {
		console.log("MenuDisplay!");
		
	// Under construction prompt	
	function underConstruction() {
   	alert("This page is under construction!");
    }
	
	var version = 0.1;
		
	// Create a "hidden" version Dialog:
  	var dialogVersion = new dijit.Dialog({ title:"Test" , content: ("Mission Control System version " + version + "<br /><br />Some more important information"), style: "width: 350px", id: "versionNumber"});
  	dialogVersion.startup();

    // Show the version dialog
    function showVersion() {
        dijit.byId("versionNumber").show();
    }
    // Hide the version dialog
    function hideVersion() {
        dijit.byId("versionNumber").hide();
    }
		

	// Active GS dialog
  	var dialogGS = new dijit.Dialog({ title:"Active GS" , content: ("Active GS: <br><br>ES5EC"), style: "width: 350px", id: "activeGS"});
  	dialogGS.startup();

    // Show the GS dialog
    function showGS() {
        dijit.byId("activeGS").show();
    }
    // Hide the GS dialog
    function hideGS() {
        dijit.byId("activeGS").hide();
    }
		
		
		var menuMain = new dijit.MenuItem({
			label : "Main View",
			onClick : function() {
				dijit.byId('CenterContainer').selectChild(dijit.byId("mainTabContainer"));
				dijit.byId('InfoView').selectChild(dijit.byId("mainInfoTab"));
				dijit.byId('PictureView').selectChild(dijit.byId("pictureTab"));
			}
		});

		var menuStatuses = new dijit.MenuItem({
			label : "Statuses View",
			onClick : function() {
				dijit.byId('CenterContainer').selectChild(dijit.byId("statusesGraphTab"));
				dijit.byId('InfoView').selectChild(dijit.byId("statusesInfoTab"));
				dijit.byId('PictureView').selectChild(dijit.byId("componentTabContainer"));
			}
		});

		var menuCommand = new dijit.MenuItem({
			label : "Commands View",
			onClick : underConstruction,
		});

		var menuLog = new dijit.MenuItem({
			onClick : underConstruction,
			label : "Log View"
		});

		var menuTimeline = new dijit.MenuItem({
			onClick : underConstruction,
			label : "Timeline View"
		});

		//Active GS
		var activeDevice = "ES5EC";
		var menuGroundstation = new dijit.MenuItem({
			onClick : showGS,
			label : "Active device: " + activeDevice,
		});

		//Version no.
		var menuVersion = new dijit.MenuItem({
			onClick : showVersion,
			label : "GUI version: " + version,
		});
		
		var mainMenu = dijit.byId("MenuView");
		mainMenu.addChild(menuMain);
		mainMenu.addChild(menuStatuses);
		mainMenu.addChild(menuCommand);
		mainMenu.addChild(menuLog);
		mainMenu.addChild(menuTimeline);
		mainMenu.addChild(menuGroundstation);
		mainMenu.addChild(menuVersion);

	}
});

dojo.declare("webgui.display.MenuDisplay", null, {

	constructor : function(args) {
		var menu = new MenuDisplay();
	}
});