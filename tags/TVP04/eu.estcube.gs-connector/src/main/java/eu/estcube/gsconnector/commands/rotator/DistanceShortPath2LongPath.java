package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class DistanceShortPath2LongPath implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+a ");
        messageString.append(telemetryCommand.getParams().get("Short Path km"));
        messageString.append("\n");
        return messageString;
    }
}
