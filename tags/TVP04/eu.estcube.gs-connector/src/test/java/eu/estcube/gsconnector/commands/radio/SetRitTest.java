package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class SetRitTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SetRIT createMessage = new SetRIT();
    
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand(TelemetryRadioConstants.SET_RIT);
        telemetryCommand.addParameter("RIT", 0);
        string.append("+J 0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
