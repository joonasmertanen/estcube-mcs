package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class SetSplitVfoTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SetSplitVFO createMessage = new SetSplitVFO();

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand(TelemetryRadioConstants.SET_SPLIT_VFO);
        telemetryCommand.addParameter("Split", 0);
        telemetryCommand.addParameter("TX VFO", "VFO");
        string.append("+S 0 VFO\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
