/** 
 *
 */
package eu.estcube.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class TelemetryCommand implements Serializable {

    private static final long serialVersionUID = 2092380317315772625L;

    private final String name;
    private Map<String, Object> params;

    /**
     * Creates new TelemetryCommand, with only a name.
     * 
     * @param name
     */
    public TelemetryCommand(String name) {
        this.name = name;
        params = new HashMap<String, Object>();
    }

    /**
     * Creates new TelemetryCommand.
     * 
     * @param name
     * @param params
     */
    public TelemetryCommand(String name, Map<String, Object> params) {
        this.name = name;
        this.params = params;
    }

    /**
     * Returns name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns parameters.
     * 
     * @return the params
     */
    public Map<String, Object> getParams() {
        return params;
    }

    /**
     * Adds new parameter to map.
     */
    public void addParameter(String name, Object value) {
        params.put(name, value);
    }

    /**
     * Returns a parameter with given name.
     * 
     * @return a param with given name
     */
    public Object getParameter(String name) {
        return params.get(name);
    }
}
