package eu.estcube.webcamera;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.imageio.ImageIO;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

@Component
public class TimeStampJpg implements Processor{
    private String gsName = "";
    
    public void process(Exchange ex) throws Exception {
        InputStream is = ex.getIn().getBody(InputStream.class);
        
        BufferedImage image = ImageIO.read(is);
        Graphics2D graphics = image.createGraphics();
        Font font = new Font("ARIAL", Font.PLAIN, 18);
        graphics.setFont(font);
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy HH:mm:ss zzz");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateString = formatter.format(new Date());
        graphics.setColor(Color.black);
        graphics.fillRect(20, 10, 320, 30);
        graphics.setColor(Color.white);
        graphics.drawString(gsName + " - " + dateString + "", 30, 30);
        image.flush();
        
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(image,"jpg", os); 
        InputStream fis = new ByteArrayInputStream(os.toByteArray());
        
        ex.getOut().setBody(fis, InputStream.class);
    }

    public void setGsName(String gsName) {
        this.gsName = gsName;
    }
}
