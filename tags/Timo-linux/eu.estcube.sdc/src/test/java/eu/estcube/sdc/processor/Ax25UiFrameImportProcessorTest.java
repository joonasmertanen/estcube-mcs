/** 
 *
 */
package eu.estcube.sdc.processor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.util.Dates;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.common.Headers;
import eu.estcube.domain.transport.Downlink;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.sdc.domain.Ax25UiFrameImport;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class Ax25UiFrameImportProcessorTest {

    private static final String SAT_ID = "/ESTCUBE/Satellites/ESTCube-1";
    private static final String GS_ID = "/ESTCUBE/GroundStations/ES5EC";
    private static final long NOW = System.currentTimeMillis();
    private static final long ORBIT_NUMBER = 1234L;
    private static final int TNC_PORT = 13;

    @Mock
    private Exchange exchange;

    @Mock
    private Message in;

    @Mock
    private Message out;

    @Mock
    private InputToAx25UiFrameConverter converter;

    @Mock
    private Ax25UiFrameImport input;

    @Mock
    private Ax25UIFrame frame;

    @InjectMocks
    private Ax25UiFrameImportProcessor processor;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        inOrder = inOrder(exchange, in, out, converter, input, frame);
        when(exchange.getIn()).thenReturn(in);
        when(exchange.getOut()).thenReturn(out);
        when(in.getBody(Ax25UiFrameImport.class)).thenReturn(input);
        when(converter.convert(input)).thenReturn(frame);
        when(input.getSatelliteId()).thenReturn(SAT_ID);
        when(input.getGroundStationId()).thenReturn(GS_ID);
        when(input.getTncTargetPort()).thenReturn(TNC_PORT);
    }

    @Test
    public void testProcess() throws Exception {
        when(input.getTimestamp()).thenReturn(Dates.toIso8601DateFormat(NOW));
        when(input.getOrbitNumber()).thenReturn(ORBIT_NUMBER);
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getBody(Ax25UiFrameImport.class);
        inOrder.verify(converter, times(1)).convert(input);
        inOrder.verify(out, times(1)).setBody(frame);
        inOrder.verify(input, times(1)).getTimestamp();
        inOrder.verify(input, times(1)).getOrbitNumber();
        inOrder.verify(input, times(1)).getSatelliteId();
        inOrder.verify(input, times(1)).getGroundStationId();
        inOrder.verify(input, times(1)).getTncTargetPort();
        inOrder.verify(out, times(1)).setHeader(StandardArguments.TIMESTAMP, NOW);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.ISSUED_BY, GS_ID);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.SATELLITE_ID, SAT_ID);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.GROUND_STATION_ID, GS_ID);
        inOrder.verify(out, times(1))
                .setHeader(StandardArguments.CONTACT_ID, GS_ID + ":" + SAT_ID + ":" + ORBIT_NUMBER);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.CLASS, frame.getClass().getSimpleName());
        inOrder.verify(out, times(1)).setHeader(StandardArguments.TYPE, TncFrameToAx25UIFrame.TYPE);
        inOrder.verify(out, times(1)).setHeader(Headers.COMMUNICATION_LINK_TYPE, Downlink.class.getSimpleName());
        inOrder.verify(out, times(1)).setHeader(Headers.TNC_PORT, TNC_PORT);
    }

    @Test
    public void testProcessNoTimestamp() throws Exception {
        when(input.getTimestamp()).thenReturn(null);
        when(input.getOrbitNumber()).thenReturn(ORBIT_NUMBER);
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getBody(Ax25UiFrameImport.class);
        inOrder.verify(converter, times(1)).convert(input);
        inOrder.verify(out, times(1)).setBody(frame);
        inOrder.verify(input, times(1)).getTimestamp();
        inOrder.verify(input, times(1)).getOrbitNumber();
        inOrder.verify(input, times(1)).getSatelliteId();
        inOrder.verify(input, times(1)).getGroundStationId();
        inOrder.verify(input, times(1)).getTncTargetPort();
        ArgumentCaptor<Long> captor = ArgumentCaptor.forClass(Long.class);
        inOrder.verify(out, times(1)).setHeader(eq(StandardArguments.TIMESTAMP), captor.capture());
        assertTrue(NOW <= captor.getValue());
        assertTrue(captor.getValue() <= System.currentTimeMillis());
        inOrder.verify(out, times(1)).setHeader(StandardArguments.ISSUED_BY, GS_ID);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.SATELLITE_ID, SAT_ID);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.GROUND_STATION_ID, GS_ID);
        inOrder.verify(out, times(1))
                .setHeader(StandardArguments.CONTACT_ID, GS_ID + ":" + SAT_ID + ":" + ORBIT_NUMBER);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.CLASS, frame.getClass().getSimpleName());
        inOrder.verify(out, times(1)).setHeader(StandardArguments.TYPE, TncFrameToAx25UIFrame.TYPE);
        inOrder.verify(out, times(1)).setHeader(Headers.COMMUNICATION_LINK_TYPE, Downlink.class.getSimpleName());
        inOrder.verify(out, times(1)).setHeader(Headers.TNC_PORT, TNC_PORT);
    }

    @Test
    public void testProcessNoOrbitNumber() throws Exception {
        when(input.getTimestamp()).thenReturn("XXII");
        when(input.getOrbitNumber()).thenReturn(Ax25UiFrameImport.DEFAULT_ORBIT_NUMBER);
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getBody(Ax25UiFrameImport.class);
        inOrder.verify(converter, times(1)).convert(input);
        inOrder.verify(out, times(1)).setBody(frame);
        inOrder.verify(input, times(1)).getTimestamp();
        inOrder.verify(input, times(1)).getOrbitNumber();
        inOrder.verify(input, times(1)).getSatelliteId();
        inOrder.verify(input, times(1)).getGroundStationId();
        inOrder.verify(input, times(1)).getTncTargetPort();
        ArgumentCaptor<Long> captor = ArgumentCaptor.forClass(Long.class);
        inOrder.verify(out, times(1)).setHeader(eq(StandardArguments.TIMESTAMP), captor.capture());
        assertTrue(NOW <= captor.getValue());
        assertTrue(captor.getValue() <= System.currentTimeMillis());
        inOrder.verify(out, times(1)).setHeader(StandardArguments.ISSUED_BY, GS_ID);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.SATELLITE_ID, SAT_ID);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.GROUND_STATION_ID, GS_ID);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.CONTACT_ID,
                GS_ID + ":" + SAT_ID + ":" + Ax25UiFrameImportProcessor.DEFAULT_ORBIT_NUMBER);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.CLASS, frame.getClass().getSimpleName());
        inOrder.verify(out, times(1)).setHeader(StandardArguments.TYPE, TncFrameToAx25UIFrame.TYPE);
        inOrder.verify(out, times(1)).setHeader(Headers.COMMUNICATION_LINK_TYPE, Downlink.class.getSimpleName());
        inOrder.verify(out, times(1)).setHeader(Headers.TNC_PORT, TNC_PORT);
    }

    @Test
    public void testToOrbitNumber() {
        assertEquals(Ax25UiFrameImportProcessor.DEFAULT_ORBIT_NUMBER,
                processor.toOrbitNumber(Ax25UiFrameImport.DEFAULT_ORBIT_NUMBER));
        assertEquals(Ax25UiFrameImport.DEFAULT_ORBIT_NUMBER + 1,
                processor.toOrbitNumber(Ax25UiFrameImport.DEFAULT_ORBIT_NUMBER + 1));
        assertEquals(Ax25UiFrameImport.DEFAULT_ORBIT_NUMBER - 1,
                processor.toOrbitNumber(Ax25UiFrameImport.DEFAULT_ORBIT_NUMBER - 1));
        assertEquals(100120, processor.toOrbitNumber(100120));
    }

    @Test
    public void testToTimestamp() {
        assertEquals(NOW, processor.toTimestamp(String.valueOf(NOW)));
        assertEquals(NOW, processor.toTimestamp(Dates.toIso8601DateFormat(NOW)));
        long result = processor.toTimestamp(null);
        assertTrue(NOW <= result && result <= System.currentTimeMillis());
        result = processor.toTimestamp("");
        assertTrue(NOW <= result && result <= System.currentTimeMillis());
        result = processor.toTimestamp(" ");
        assertTrue(NOW <= result && result <= System.currentTimeMillis());
        result = processor.toTimestamp("abc");
        assertTrue(NOW <= result && result <= System.currentTimeMillis());
        result = processor.toTimestamp(Dates.toIso8601BasicDateFormat(NOW));
        assertTrue(NOW <= result && result <= System.currentTimeMillis());
        result = processor.toTimestamp(Dates.toDefaultDateFormat(NOW));
        assertTrue(NOW <= result && result <= System.currentTimeMillis());
        result = processor.toTimestamp(Dates.toDateInFileNameFormat(NOW));
        assertTrue(NOW <= result && result <= System.currentTimeMillis());
    }
}
