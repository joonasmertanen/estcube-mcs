/** 
 *
 */
package eu.estcube.sdc.processor;

import org.springframework.stereotype.Component;

import eu.estcube.common.ByteUtil;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.sdc.domain.Ax25UiFrameOutput;

/**
 *
 */
@Component
public class Ax25UiFrameToOutputConverter {

    public Ax25UiFrameOutput convert(Ax25UIFrame frame) {
        Ax25UiFrameOutput output = new Ax25UiFrameOutput();
        output.setDestAddr(toHexString(frame.getDestAddr()));
        output.setSrcAddr(toHexString(frame.getSrcAddr()));
        output.setCtrl(toHexString(frame.getCtrl()));
        output.setPid(toHexString(frame.getPid()));
        output.setInfo(toHexString(frame.getInfo()));
        return output;
    }

    String toHexString(Byte value) {
        return value == null ? "" : ByteUtil.toHexString(value);
    }

    String toHexString(byte[] bytes) {
        return bytes == null || bytes.length < 1 ? "" : ByteUtil.toHexString(bytes);
    }
}
