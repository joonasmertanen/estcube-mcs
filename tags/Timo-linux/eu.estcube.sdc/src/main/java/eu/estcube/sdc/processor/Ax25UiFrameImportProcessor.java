/** 
 *
 */
package eu.estcube.sdc.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.util.Dates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.common.Headers;
import eu.estcube.domain.transport.Downlink;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.sdc.domain.Ax25UiFrameImport;

/**
 * 
 */
@Component
public class Ax25UiFrameImportProcessor implements Processor {

    public static final long DEFAULT_ORBIT_NUMBER = -1L;

    @Autowired
    private InputToAx25UiFrameConverter converter;

    /** @{inheritDoc . */
    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();

        out.copyFrom(in);

        Ax25UiFrameImport input = in.getBody(Ax25UiFrameImport.class);
        Ax25UIFrame frame = converter.convert(input);
        out.setBody(frame);

        long timestamp = toTimestamp(input.getTimestamp());
        long orbitNumber = toOrbitNumber(input.getOrbitNumber());
        String satelliteId = input.getSatelliteId();
        String groundStationId = input.getGroundStationId();
        // TODO - 20.05.2013; kimmell - fix with next Hbird version!
        String contactId = groundStationId + ":" + satelliteId + ":" + orbitNumber;
        int port = input.getTncTargetPort();

        out.setHeader(StandardArguments.TIMESTAMP, timestamp);
        out.setHeader(StandardArguments.ISSUED_BY, groundStationId);
        out.setHeader(StandardArguments.SATELLITE_ID, satelliteId);
        out.setHeader(StandardArguments.GROUND_STATION_ID, groundStationId);
        out.setHeader(StandardArguments.CONTACT_ID, contactId);
        out.setHeader(StandardArguments.CLASS, frame.getClass().getSimpleName());
        out.setHeader(StandardArguments.TYPE, TncFrameToAx25UIFrame.TYPE);
        out.setHeader(Headers.COMMUNICATION_LINK_TYPE, Downlink.class.getSimpleName());
        out.setHeader(Headers.TNC_PORT, port);
    }

    long toOrbitNumber(long value) {
        return value == Ax25UiFrameImport.DEFAULT_ORBIT_NUMBER ? DEFAULT_ORBIT_NUMBER : value;
    }

    long toTimestamp(String value) {
        if (StringUtils.isBlank(value)) {
            return System.currentTimeMillis();
        }
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException nfe) {
            // ignore
        }
        try {
            return Dates.ISO_8601_DATE_FORMATTER.parseDateTime(value).getMillis();
        } catch (IllegalArgumentException iae) {
            // ignore
        }
        return System.currentTimeMillis();
    }
}
