/** 
 *
 */
package eu.estcube.sdc.domain;

/**
 *
 */
public class Ax25UiFrameImport extends Ax25UiFrameInput {

    public static final long DEFAULT_ORBIT_NUMBER = 0;

    private String timestamp;
    private String satelliteId;
    private String groundStationId;
    private long orbitNumber = DEFAULT_ORBIT_NUMBER;

    public Ax25UiFrameImport() {
        super();
    }

    /**
     * Returns timestamp.
     * 
     * @return the timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * Sets timestamp.
     * 
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Returns satelliteId.
     * 
     * @return the satelliteId
     */
    public String getSatelliteId() {
        return satelliteId;
    }

    /**
     * Sets satelliteId.
     * 
     * @param satelliteId the satelliteId to set
     */
    public void setSatelliteId(String satelliteId) {
        this.satelliteId = satelliteId;
    }

    /**
     * Returns groundStationId.
     * 
     * @return the groundStationId
     */
    public String getGroundStationId() {
        return groundStationId;
    }

    /**
     * Sets groundStationId.
     * 
     * @param groundStationId the groundStationId to set
     */
    public void setGroundStationId(String groundStationId) {
        this.groundStationId = groundStationId;
    }

    /**
     * Returns orbitNumber.
     * 
     * @return the orbitNumber
     */
    public long getOrbitNumber() {
        return orbitNumber;
    }

    /**
     * Sets orbitNumber.
     * 
     * @param orbitNumber the orbitNumber to set
     */
    public void setOrbitNumber(long orbitNumber) {
        this.orbitNumber = orbitNumber;
    }
}
