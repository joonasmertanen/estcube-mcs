/** 
 *
 */
package eu.estcube.sdc.xml;

import java.io.File;
import java.util.List;

import org.apache.camel.Body;
import org.apache.camel.Handler;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

import eu.estcube.sdc.domain.Ax25UiFrameImport;

/**
 *
 */
@Component
public class Ax25UiFrameImportParser {

    private final XStream xstream = new XStream(new StaxDriver());

    public Ax25UiFrameImportParser() {
        xstream.alias("frame", Ax25UiFrameImport.class);
        xstream.alias("frames", List.class);
    }

    @SuppressWarnings("unchecked")
    @Handler
    public List<Ax25UiFrameImport> parse(@Body File file) throws Exception {
        String xml = FileUtils.readFileToString(file, Charsets.UTF_8);
        return (List<Ax25UiFrameImport>) xstream.fromXML(xml);
    }
}
