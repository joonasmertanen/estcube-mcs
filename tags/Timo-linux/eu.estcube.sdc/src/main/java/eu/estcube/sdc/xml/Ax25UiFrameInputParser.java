/** 
 *
 */
package eu.estcube.sdc.xml;

import java.io.File;
import java.util.List;

import org.apache.camel.Body;
import org.apache.camel.Handler;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

import eu.estcube.sdc.domain.Ax25UiFrameInput;

/**
 *
 */
@Component
public class Ax25UiFrameInputParser {

    
    private XStream xstream = new XStream(new StaxDriver());
    
    public Ax25UiFrameInputParser() {
        xstream.alias("frame", Ax25UiFrameInput.class);
        xstream.alias("frames", List.class);
    }
    
    @SuppressWarnings("unchecked")
    @Handler
    public List<Ax25UiFrameInput> parse(@Body File file) throws Exception {
        String xml = FileUtils.readFileToString(file, Charsets.UTF_8);
        return (List<Ax25UiFrameInput>) xstream.fromXML(xml);
    }
}
