package eu.estcube.codec.radiobeacon.parser;

import org.hbird.exchange.core.Parameter;

public class RadionBeaconMessageParserSpinRate extends RadioBeaconMessageParser {

    protected Integer startIndex;

    protected Integer endIndex;

    protected Parameter parameterBase;

    protected Double result;

    public RadionBeaconMessageParserSpinRate(Integer startIndex, Integer length, Parameter parameterBase) {
        this.startIndex = startIndex;
        this.endIndex = startIndex + length;
        this.parameterBase = parameterBase;
    }

    @Override
    public String parse(String hex) {
        String part = getValidStringPart(hex, this.startIndex, this.endIndex);
        if (part == null) {
            result = null;
            return null;
        }
        result = (Double.parseDouble(parseHex(cwToHex(part))) * 720) / 2047;
        return String.valueOf(result);
    }

    @Override
    public Parameter parseToIssued(String message) {
        parse(message);
        if (result == null) {
            return null;
        }
        Parameter parameter = parameterBase.cloneEntity();
        parameter.setValue(result);
        return parameter;
    }
}
