package eu.estcube.codec.radiobeacon.parsers;

import java.util.ArrayList;
import java.util.List;

import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.core.Label;

import eu.estcube.codec.radiobeacon.parser.RadioBeaconMessageParser;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParser2ComplementInteger;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserBits;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserCalibratedDouble;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserHex;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserInteger;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserSpinRate;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserTetherCurrent;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserTimestamp;

public class RadioBeaconParsersNormalMode extends RadioBeaconParsers {

    /**
     * Creates new RadioBeaconParsersNormalMode.
     * 
     * @param idBuilder
     */
    public RadioBeaconParsersNormalMode(IdBuilder idBuilder) {
        super(idBuilder);
    }

    @Override
    public List<RadioBeaconMessageParser> getParsers() {
        List<RadioBeaconMessageParser> parsers = new ArrayList<RadioBeaconMessageParser>();
        parsers.add(new RadionBeaconMessageParserHex(6, 1, RadioBeaconParametersNormalMode.OPERATING_MODE));
        parsers.add(getTimestampParser());
        parsers.add(new RadionBeaconMessageParserCalibratedDouble(14, 2,
                RadioBeaconParametersNormalMode.MAIN_BUS_VOLTAGE, 0.001240978485204 * 16, -0.001670625667674));

        parsers.add(new RadionBeaconMessageParser2ComplementInteger(16, 2,
                RadioBeaconParametersNormalMode.AVERAGE_POWER_BALANCE));
        parsers.add(new RadionBeaconMessageParserCalibratedDouble(18, 2,
                RadioBeaconParametersNormalMode.BATTERY_A_VOLTAGE, 0.017686154075981, 0.003877355151542));
        parsers.add(new RadionBeaconMessageParserCalibratedDouble(20, 2,
                RadioBeaconParametersNormalMode.BATTERY_B_VOLTAGE, 0.017645083640731, 0.013681971347675));
        parsers.add(new RadionBeaconMessageParserCalibratedDouble(22, 2,
                RadioBeaconParametersNormalMode.BATTERY_A_TEMPERATURE, 0.7139, -61.1111));
        parsers.add(new RadionBeaconMessageParserSpinRate(24, 3, RadioBeaconParametersNormalMode.SPIN_RATE_Z));
        parsers.add(new RadionBeaconMessageParser2ComplementInteger(27, 1,
                RadioBeaconParametersNormalMode.RECEIVED_SIGNAL_STRENGTH));

        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0xC0, 6,
                RadioBeaconParametersNormalMode.SATELLITE_MISSION_PHASE));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x30, 4,
                RadioBeaconParametersNormalMode.TIME_SINCE_LAST_RESET_CDHS));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0xC, 2,
                RadioBeaconParametersNormalMode.TIME_SINCE_LAST_RESET_COM));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x3, 0,
                RadioBeaconParametersNormalMode.TIME_SINCE_LAST_RESET_EPS));

        parsers.add(new RadionBeaconMessageParserTetherCurrent(30, 2, RadioBeaconParametersNormalMode.TETHER_CURRENT));

        parsers.add(new RadionBeaconMessageParserBits(32, 2, 0xC0, 6,
                RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_ADCS));
        parsers.add(new RadionBeaconMessageParserBits(32, 2, 0x30, 4,
                RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_CDHS));
        parsers.add(new RadionBeaconMessageParserBits(32, 2, 0xC, 2,
                RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_COM));
        parsers.add(new RadionBeaconMessageParserBits(32, 2, 0x3, 0,
                RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_EPS));

        parsers.add(new RadionBeaconMessageParserBits(34, 2, 0xFC, 2,
                RadioBeaconParametersNormalMode.CDHS_SYSTEM_STATUS_CODE));
        parsers.add(new RadionBeaconMessageParserBits(34, 2, 0x3, 0,
                RadioBeaconParametersNormalMode.CDHS_SYSTEM_STATUS_VALUE));

        parsers.add(new RadionBeaconMessageParserInteger(36, 2, RadioBeaconParametersNormalMode.EPS_STATUS_CODE));

        parsers.add(new RadionBeaconMessageParserBits(38, 2, 0xFC, 2,
                RadioBeaconParametersNormalMode.ADCS_SYSTEM_STATUS_CODE));
        parsers.add(new RadionBeaconMessageParserBits(38, 2, 0x3, 0,
                RadioBeaconParametersNormalMode.ADCS_SYSTEM_STATUS_VALUE));

        parsers.add(new RadionBeaconMessageParserBits(40, 2, 0xFC, 2,
                RadioBeaconParametersNormalMode.COM_SYSTEM_STATUS_CODE));
        parsers.add(new RadionBeaconMessageParserBits(40, 2, 0x3, 0,
                RadioBeaconParametersNormalMode.COM_SYSTEM_STATUS_VALUE));

        return parsers;
    }

    @Override
    public Label getRadioBeacon() {
        return RadioBeaconParametersNormalMode.RADIO_BEACON;
    }

    @Override
    public String getEntityID() {
        return RadioBeaconParametersNormalMode.ENTITY_ID;
    }

    @Override
    public RadionBeaconMessageParserTimestamp getTimestampParser() {
        return new RadionBeaconMessageParserTimestamp(7, 7, RadioBeaconParametersNormalMode.TIMESTAMP);
    }

}
