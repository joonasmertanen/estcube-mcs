package eu.estcube.codec.gcp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.Validate;
import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.core.EntityInstance;
import org.hbird.exchange.core.Metadata;

import eu.estcube.codec.gcp.exceptions.SubsystemNotFoundException;
import eu.estcube.codec.gcp.struct.GcpParameter;
import eu.estcube.codec.gcp.struct.GcpReply;
import eu.estcube.codec.gcp.struct.GcpStruct;
import eu.estcube.codec.gcp.struct.GcpSubsystemIdProvider;
import eu.estcube.codec.gcp.struct.parameters.GcpParameterMaxLength;
import eu.estcube.common.Headers;
import eu.estcube.common.hbird.MetadataFactory;

public class GcpDecoder {

    private final MetadataFactory metadataFactory;

    private final String issuedBy;

    private final IdBuilder idBuilder;

    public GcpDecoder(MetadataFactory metadataFactory, String issuedBy, IdBuilder idBuilder) {
        Validate.isTrue(metadataFactory != null, "MetadataFactory can't be null");
        Validate.isTrue(idBuilder != null, "IdBuilder can't be null");
        this.metadataFactory = metadataFactory;
        this.issuedBy = issuedBy;
        this.idBuilder = idBuilder;
    }

    /**
     * Telemetry parser
     * 
     * @throws SubsystemNotFoundException
     */
    @SuppressWarnings("deprecation")
    public List<EntityInstance> decode(byte[] bytes, GcpStruct struct, String entityId)
            throws SubsystemNotFoundException {

        List<EntityInstance> list = new ArrayList<EntityInstance>();

        int source = bytes[0] & 0xFF;
        int destination = bytes[1] & 0xFF;
        int length = ((bytes[2] & 0xFF) << 8) | (bytes[3] & 0xFF);
        int priority = (bytes[3] & 0xC0) >> 6;
        int id = ((bytes[4] & 0x3F) << 8) | (bytes[5] & 0xFF);
        int CDHSSource = (bytes[6] & 0xF0) >> 4;
        int CDHSBlockIndex = bytes[6] & 0xF;
        int argumentsLength = bytes[7] & 0xFF;

        GcpReply com = struct.getReply(id, GcpSubsystemIdProvider.getName(source));
        if (com == null) {
            throw new RuntimeException("[GcpDecoder] Reply with id:" + id + " subsystem:" + source + " not found!");
        }

        entityId = entityId.replace("{$subsystem}", GcpSubsystemIdProvider.getName(source));

        int offset = 8 * 8;
        for (GcpParameter parameter : com.getParameters()) {

            int len = parameter.getLength();
            if (parameter instanceof GcpParameterMaxLength) {
                len = bytes.length * 8 - offset;
            }

            // ApiFactory.getPublishApi("{$service.name}").publish()
            // ApiFactory.getCatalogueApi("{$service.name}").getParameters().

            byte[] paramBytes = byteCopier(bytes, offset, len);
            offset += len;

            if (parameter.getIgnored()) {
                continue;
            }

            EntityInstance issued = parameter.getIssued(paramBytes);
            String finalEntityId = idBuilder.buildID(entityId, issued.getName());
            issued.setID(finalEntityId);
            issued.setIssuedBy(issuedBy);
            list.add(issued);

            Map<String, Object> data = new HashMap<String, Object>();
            data.put(StandardArguments.SOURCE, source);
            data.put(StandardArguments.DESTINATION, destination);
            data.put(StandardArguments.LENGTH, length);
            data.put(StandardArguments.PRIORITY, priority);
            data.put(Headers.ID, id);
            data.put(StandardArguments.ARGUMENTS_LENGTH, argumentsLength);
            data.put(Headers.CDHS_SOURCE, CDHSSource);
            data.put(Headers.CDHS_BLOCK_INDEX, CDHSBlockIndex);
            Metadata meta = metadataFactory.createMetadata(issued, data, issuedBy);
            list.add(meta);

        }
        return list;
    }

    public byte[] byteCopier(byte[] bytes, int offset, int length) {

        int resultLength = (int) Math.ceil((double) length / 8);
        int byteOffset = offset / 8;
        int byteLength = (int) Math.ceil((double) length / 8);

        byte[] paramBytes = new byte[resultLength];
        System.arraycopy(bytes, byteOffset, paramBytes, 0, byteLength);

        // If length is less than 1 byte, then we will remove all unused bits
        // and move used bit to the most right position.
        if (length < 8) {

            List<Byte> map = new ArrayList<Byte>();
            map.add((byte) 1);
            map.add((byte) 2);
            map.add((byte) 4);
            map.add((byte) 8);
            map.add((byte) 16);
            map.add((byte) 32);
            map.add((byte) 64);
            map.add((byte) 128);

            int b = 7 - offset % 8;
            paramBytes[0] = (byte) (((paramBytes[0] & map.get(b)) >> (b)) & 0x01);
        }

        return paramBytes;
    }
}
