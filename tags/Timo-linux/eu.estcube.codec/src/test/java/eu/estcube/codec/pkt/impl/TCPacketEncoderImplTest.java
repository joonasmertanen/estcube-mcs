package eu.estcube.codec.pkt.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Before;
import org.junit.Test;

import eu.estcube.domain.transport.pkt.TCPacket;
import eu.estcube.domain.transport.pkt.TCPacketFramePrimaryHeader;
import eu.estcube.domain.transport.pkt.TCPacketFrameSecondaryHeader;
import eu.estcube.domain.transport.pkt.TCPacketPrimaryFrame;
import eu.estcube.domain.transport.pkt.TCPacketSecondaryFrame;

public class TCPacketEncoderImplTest {
	public static final int FRAMESIZE = 22; // Primary header is 20 bytes, 2
											// left for body
	public static final int PHEADER = 20;
	public static final int SHEADER = 5;

	private TCPacketEncoderImpl pktEncoder;

	@Before
	public void setUp() throws Exception {
		pktEncoder = new TCPacketEncoderImpl(FRAMESIZE);

	}

	@Test
	public void testAx25FramedTCPktEncoder() {
		assertEquals(FRAMESIZE, pktEncoder.getFrameSize());
	}

	@Test
	public void testEncodeEqualToPrimary() {
		int spid = 511; // 0000000111111111
		long et = 1095233372415L; // 1111111100000000111111110000000011111111
		long ts = 4294967295L;
		int ssc = 65535;
		int len = 1;
		byte[] mac = new byte[] { 1, 2, 3, 4 };
		byte[] payload = new byte[] { (byte) 233, (byte) 255 };

		TCPacketFramePrimaryHeader primaryHeader = new TCPacketFramePrimaryHeader(ssc, len, spid, ts, mac, et);
		TCPacketPrimaryFrame primaryFrame = new TCPacketPrimaryFrame(primaryHeader, payload);
		TCPacket packet = new TCPacket(primaryFrame);

		byte[] packetEncoded = pktEncoder.encode(packet);

		// Check length
		assertEquals(PHEADER + payload.length, packetEncoded.length);

		// Check SSC
		assertEquals(0, packetEncoded[0]);
		assertEquals((byte) 255, packetEncoded[1]);
		assertEquals((byte) 255, packetEncoded[2]);

		// Check LEN
		assertEquals(0, packetEncoded[3]);
		assertEquals(1, packetEncoded[4]);

		// Check SPID
		assertEquals(1, packetEncoded[5]);
		assertEquals((byte) 255, packetEncoded[6]);

		// Check TS
		assertEquals((byte) 255, packetEncoded[7]);
		assertEquals((byte) 255, packetEncoded[8]);
		assertEquals((byte) 255, packetEncoded[9]);
		assertEquals((byte) 255, packetEncoded[10]);

		// Check MAC
		assertEquals((byte) 1, packetEncoded[11]);
		assertEquals((byte) 2, packetEncoded[12]);
		assertEquals((byte) 3, packetEncoded[13]);
		assertEquals((byte) 4, packetEncoded[14]);

		// Check ET
		assertEquals((byte) 255, packetEncoded[15]);
		assertEquals((byte) 0, packetEncoded[16]);
		assertEquals((byte) 255, packetEncoded[17]);
		assertEquals((byte) 0, packetEncoded[18]);
		assertEquals((byte) 255, packetEncoded[19]);

		// Check payload
		assertTrue(Arrays.equals(payload, Arrays.copyOfRange(packetEncoded, 20, 22)));
	}

	@Test
	public void testEncodeTwoPlusFrames() {
		int spid = 511; // 0000000111111111
		long et = 1095233372415L; // 1111111100000000111111110000000011111111
		long ts = 4294967295L;
		int ssc = 65535;
		int len = 3;
		byte[] mac = new byte[] { 1, 2, 3, 4 };

		// Payload is 20 bytes - fits into primary frame, one secondary and 1
		// byte to another secondary
		// frame
		byte[] payload = new byte[] { (byte) 233, (byte) 255, (byte) 233, (byte) 255, (byte) 233, (byte) 255,
				(byte) 233, (byte) 255, (byte) 233, (byte) 255, (byte) 233, (byte) 255, (byte) 233, (byte) 255,
				(byte) 233, (byte) 255, (byte) 233, (byte) 255, (byte) 233, (byte) 255 };

		TCPacketFramePrimaryHeader primaryHeader = new TCPacketFramePrimaryHeader(ssc, len, spid, ts, mac, et);
		TCPacketPrimaryFrame primaryFrame = new TCPacketPrimaryFrame(primaryHeader, ArrayUtils.subarray(payload, 0, 2));

		TCPacketFrameSecondaryHeader secondaryHeader1 = new TCPacketFrameSecondaryHeader(ssc + 1, len);
		TCPacketSecondaryFrame secondaryFrame1 = new TCPacketSecondaryFrame(secondaryHeader1, ArrayUtils.subarray(
				payload, 2, 19));

		TCPacketFrameSecondaryHeader secondaryHeader2 = new TCPacketFrameSecondaryHeader(ssc + 2, len);
		TCPacketSecondaryFrame secondaryFrame2 = new TCPacketSecondaryFrame(secondaryHeader2, ArrayUtils.subarray(
				payload, 19, 20));

		TCPacket packet = new TCPacket(primaryFrame);
		packet.addSecondaryFrame(secondaryFrame1);
		packet.addSecondaryFrame(secondaryFrame2);

		byte[] packetEncoded = pktEncoder.encode(packet);

		// Check length
		assertEquals(PHEADER + SHEADER + SHEADER + payload.length, packetEncoded.length);

		// Check SSC (primary)
		assertEquals(0, packetEncoded[0]);
		assertEquals((byte) 255, packetEncoded[1]);
		assertEquals((byte) 255, packetEncoded[2]);

		// Check LEN (primary)
		assertEquals(0, packetEncoded[3]);
		assertEquals(3, packetEncoded[4]);

		// Check SPID
		assertEquals(1, packetEncoded[5]);
		assertEquals((byte) 255, packetEncoded[6]);

		// Check TS
		assertEquals((byte) 255, packetEncoded[7]);
		assertEquals((byte) 255, packetEncoded[8]);
		assertEquals((byte) 255, packetEncoded[9]);
		assertEquals((byte) 255, packetEncoded[10]);

		// Check MAC
		assertEquals((byte) 1, packetEncoded[11]);
		assertEquals((byte) 2, packetEncoded[12]);
		assertEquals((byte) 3, packetEncoded[13]);
		assertEquals((byte) 4, packetEncoded[14]);

		// Check ET
		assertEquals((byte) 255, packetEncoded[15]);
		assertEquals((byte) 0, packetEncoded[16]);
		assertEquals((byte) 255, packetEncoded[17]);
		assertEquals((byte) 0, packetEncoded[18]);
		assertEquals((byte) 255, packetEncoded[19]);

		// Check payload (primary)
		assertTrue(Arrays.equals(Arrays.copyOfRange(payload, 0, 2), Arrays.copyOfRange(packetEncoded, 20, 22)));

		// 2nd frame

		// Check SSC (secondary) - don't forget that 2nd highest bit is 1 as it
		// is a secondary frame
		assertEquals(65, packetEncoded[22]); // 0100 0001
		assertEquals(0, packetEncoded[23]);
		assertEquals(0, packetEncoded[24]);

		// Check LEN (secondary)
		assertEquals(0, packetEncoded[25]);
		assertEquals(3, packetEncoded[26]);

		// Check payload (secondary)
		byte[] p1 = Arrays.copyOfRange(payload, 2, 19);
		byte[] p2 = Arrays.copyOfRange(packetEncoded, 27, 44);
		assertTrue(Arrays.equals(p1, p2));

		// 3rd frame

		// Check SSC (secondary) - don't forget that 2nd highest bit is 1 as it
		// is a secondary frame
		assertEquals(65, packetEncoded[44]); // 0100 0001 0000 0000 0000 0001
		assertEquals(0, packetEncoded[45]);
		assertEquals(1, packetEncoded[46]);

		// Check LEN (secondary)
		assertEquals(0, packetEncoded[47]);
		assertEquals(3, packetEncoded[48]);

		// Check payload (secondary)
		byte[] r1 = Arrays.copyOfRange(payload, 19, 20);
		byte[] r2 = Arrays.copyOfRange(packetEncoded, 49, 50);
		assertTrue(Arrays.equals(r1, r2));
	}

	@Test
	public void testEncodeTwoFrames() {
		int spid = 511; // 0000000111111111
		long et = 1095233372415L; // 1111111100000000111111110000000011111111
		long ts = 4294967295L;
		int ssc = 65535;
		int len = 2;
		byte[] mac = new byte[] { 1, 2, 3, 4 };

		// Payload is 19 bytes - fits into primary frame and one secondary
		// frame
		byte[] payload = new byte[] { (byte) 233, (byte) 255, (byte) 233, (byte) 255, (byte) 233, (byte) 255,
				(byte) 233, (byte) 255, (byte) 233, (byte) 255, (byte) 233, (byte) 255, (byte) 233, (byte) 255,
				(byte) 233, (byte) 255, (byte) 233, (byte) 255, (byte) 233 };

		TCPacketFramePrimaryHeader primaryHeader = new TCPacketFramePrimaryHeader(ssc, len, spid, ts, mac, et);
		TCPacketPrimaryFrame primaryFrame = new TCPacketPrimaryFrame(primaryHeader, ArrayUtils.subarray(payload, 0, 2));

		TCPacketFrameSecondaryHeader secondaryHeader1 = new TCPacketFrameSecondaryHeader(ssc + 1, len);
		TCPacketSecondaryFrame secondaryFrame1 = new TCPacketSecondaryFrame(secondaryHeader1, ArrayUtils.subarray(
				payload, 2, 19));

		TCPacket packet = new TCPacket(primaryFrame);
		packet.addSecondaryFrame(secondaryFrame1);

		byte[] packetEncoded = pktEncoder.encode(packet);

		// Check length
		assertEquals(PHEADER + SHEADER + payload.length, packetEncoded.length);

		// Check SSC (primary)
		assertEquals(0, packetEncoded[0]);
		assertEquals((byte) 255, packetEncoded[1]);
		assertEquals((byte) 255, packetEncoded[2]);

		// Check LEN (primary)
		assertEquals(0, packetEncoded[3]);
		assertEquals(2, packetEncoded[4]);

		// Check SPID
		assertEquals(1, packetEncoded[5]);
		assertEquals((byte) 255, packetEncoded[6]);

		// Check TS
		assertEquals((byte) 255, packetEncoded[7]);
		assertEquals((byte) 255, packetEncoded[8]);
		assertEquals((byte) 255, packetEncoded[9]);
		assertEquals((byte) 255, packetEncoded[10]);

		// Check MAC
		assertEquals((byte) 1, packetEncoded[11]);
		assertEquals((byte) 2, packetEncoded[12]);
		assertEquals((byte) 3, packetEncoded[13]);
		assertEquals((byte) 4, packetEncoded[14]);

		// Check ET
		assertEquals((byte) 255, packetEncoded[15]);
		assertEquals((byte) 0, packetEncoded[16]);
		assertEquals((byte) 255, packetEncoded[17]);
		assertEquals((byte) 0, packetEncoded[18]);
		assertEquals((byte) 255, packetEncoded[19]);

		// Check payload (primary)
		assertTrue(Arrays.equals(Arrays.copyOfRange(payload, 0, 2), Arrays.copyOfRange(packetEncoded, 20, 22)));

		// Check SSC (secondary) - don't forget that 2nd highest bit is 1 as it
		// is a secondary frame
		assertEquals(65, packetEncoded[22]); // 0100 0001
		assertEquals(0, packetEncoded[23]);
		assertEquals(0, packetEncoded[24]);

		// Check LEN (secondary)
		assertEquals(0, packetEncoded[25]);
		assertEquals(2, packetEncoded[26]);

		// Check payload (secondary)
		byte[] p1 = Arrays.copyOfRange(payload, 2, 19);
		byte[] p2 = Arrays.copyOfRange(packetEncoded, 27, 44);
		assertTrue(Arrays.equals(p1, p2));

	}

	@Test
	public void testEncodeLessThanPrimary() {
		int spid = 511; // 0000000111111111
		long et = 1095233372415L; // 1111111100000000111111110000000011111111
		long ts = 4294967295L;
		int ssc = 65535;
		int len = 1;
		byte[] mac = new byte[] { 1, 2, 3, 4 };

		// 1. Payload is 1 byte - less than fits into primary frame
		byte[] payload = new byte[] { (byte) 233 };

		TCPacketFramePrimaryHeader primaryHeader = new TCPacketFramePrimaryHeader(ssc, len, spid, ts, mac, et);
		TCPacketPrimaryFrame primaryFrame = new TCPacketPrimaryFrame(primaryHeader, ArrayUtils.subarray(payload, 0, 2));
		TCPacket pkt = new TCPacket(primaryFrame);

		byte[] packet = pktEncoder.encode(pkt);

		// Check length
		assertEquals(PHEADER + payload.length, packet.length);

		// Check SSC
		assertEquals(0, packet[0]);
		assertEquals((byte) 255, packet[1]);
		assertEquals((byte) 255, packet[2]);

		// Check LEN
		assertEquals(0, packet[3]);
		assertEquals(1, packet[4]);

		// Check SPID
		assertEquals(1, packet[5]);
		assertEquals((byte) 255, packet[6]);

		// Check TS
		assertEquals((byte) 255, packet[7]);
		assertEquals((byte) 255, packet[8]);
		assertEquals((byte) 255, packet[9]);
		assertEquals((byte) 255, packet[10]);

		// Check MAC
		assertEquals((byte) 1, packet[11]);
		assertEquals((byte) 2, packet[12]);
		assertEquals((byte) 3, packet[13]);
		assertEquals((byte) 4, packet[14]);

		// Check ET
		assertEquals((byte) 255, packet[15]);
		assertEquals((byte) 0, packet[16]);
		assertEquals((byte) 255, packet[17]);
		assertEquals((byte) 0, packet[18]);
		assertEquals((byte) 255, packet[19]);

		// Check payload
		assertTrue(Arrays.equals(payload, Arrays.copyOfRange(packet, 20, 21)));
	}

	@Test
	public void testEncodeSsc() {
		byte[] ssc;

		ssc = pktEncoder.encodeSsc(255L, 255L, 1, true); // 00111111
		assertTrue(Arrays.equals(new byte[] { 63 }, ssc));

		ssc = pktEncoder.encodeSsc(255L, 255L, 1, false); // 01111111
		assertTrue(Arrays.equals(new byte[] { 127 }, ssc));

		ssc = pktEncoder.encodeSsc(0L, 255L, 1, true); // 00000000
		assertTrue(Arrays.equals(new byte[] { 0 }, ssc));

		ssc = pktEncoder.encodeSsc(0L, 255L, 1, false); // 01000000
		assertTrue(Arrays.equals(new byte[] { 64 }, ssc));
	}

	@Test
	public void testEncodeUnsignedInteger() {
		try {
			pktEncoder.encodeUnsignedInteger(-1L, 1L, 1);
		} catch (Throwable t) {
			assertEquals(IllegalArgumentException.class, t.getClass());
		}

		try {
			pktEncoder.encodeUnsignedInteger(2L, 1L, 1);
		} catch (Throwable t) {
			assertEquals(IllegalArgumentException.class, t.getClass());
		}

		assertTrue(Arrays.equals(new byte[] { 1 }, pktEncoder.encodeUnsignedInteger(1L, 1L, 1)));
		assertTrue(Arrays.equals(new byte[] { (byte) 255 }, pktEncoder.encodeUnsignedInteger(255L, 255L, 1)));
		assertTrue(Arrays.equals(new byte[] { (byte) 237 }, pktEncoder.encodeUnsignedInteger(237L, 255L, 1)));

		assertTrue(Arrays.equals(new byte[] { (byte) 255, (byte) 255 },
				pktEncoder.encodeUnsignedInteger(65535L, 65535L, 2)));
		assertTrue(Arrays
				.equals(new byte[] { (byte) 0, (byte) 255 }, pktEncoder.encodeUnsignedInteger(255L, 65535L, 2)));

		assertTrue(Arrays.equals(new byte[] { (byte) 255, (byte) 255 },
				pktEncoder.encodeUnsignedInteger(65535L, 65535L, 2)));

		assertTrue(Arrays.equals(new byte[] { (byte) 255, (byte) 255, (byte) 255 },
				pktEncoder.encodeUnsignedInteger(16777215L, 16777215L, 3)));

		assertTrue(Arrays.equals(new byte[] { (byte) 0, (byte) 255, (byte) 255 },
				pktEncoder.encodeUnsignedInteger(65535L, 16777215L, 3)));

		assertTrue(Arrays.equals(new byte[] { (byte) 255, (byte) 255, (byte) 255, (byte) 255, (byte) 255 },
				pktEncoder.encodeUnsignedInteger(1099511627775L, 1099511627775L, 5)));

		assertTrue(Arrays.equals(new byte[] { (byte) 255, (byte) 0, (byte) 255, (byte) 255, (byte) 255 },
				pktEncoder.encodeUnsignedInteger(1095233437695L, 1099511627775L, 5)));

	}
}
