package eu.estcube.webserver.catalogue;

import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class CatalogueQueryNode {

    public abstract class Op {
        abstract public Object getResult(StringTokenizer st);
    }

    Map<String, Op> m_options = new TreeMap<String, CatalogueQueryNode.Op>();

    Object runQuery(StringTokenizer st) {
        if (!st.hasMoreTokens())
            return null;

        String name = st.nextToken();

        Op op = m_options.get(name);
        if (null != op) {
            return op.getResult(st);
        }

        return null;
    }

    protected void addOption(String name, Op operation) {
        m_options.put(name, operation);
    }

}
