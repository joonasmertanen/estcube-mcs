package eu.estcube.limitchecking.xmlparser;

import java.io.File;
import java.util.HashMap;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import eu.estcube.limitchecking.domain.InfoContainer;

public class Parser {

    /**
     * @param args
     */
    @SuppressWarnings("unchecked")
    public static HashMap<String, InfoContainer> getCalibrations(File f) {

        HashMap<String, InfoContainer> table;

        XStream xs = new XStream(new DomDriver());
        xs.alias("limitChecker", HashMap.class);
        xs.registerConverter(new HashMapConverter());

        table = (HashMap<String, InfoContainer>) xs.fromXML(f);

        return table;

    }

}
