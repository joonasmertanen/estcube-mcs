package eu.estcube.limichecking.domain;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

import eu.estcube.limitchecking.domain.InfoContainer;

public class TestInfoContainerWithSanityLimits {
    
    
    private String id;
    private BigDecimal sanityLower;
    private BigDecimal sanityUpper;
    private BigDecimal hardLower;
    private BigDecimal hardUpper;
    private BigDecimal softLower;
    private BigDecimal softUpper;
    private InfoContainer info;

    @Before
    public void createInfoContainerWithSanityLimits(){
        
        id = "test1";

        sanityLower = new BigDecimal("0");
        sanityUpper = new BigDecimal("100");

        hardLower = new BigDecimal("37.2");
        hardUpper = new BigDecimal("65.37");

        softLower = new BigDecimal("40");
        softUpper = new BigDecimal("50");

        info = new InfoContainer(id, sanityLower, sanityUpper,
                hardLower, hardUpper, softLower, softUpper);
        
        
    }

    @Test
    public void testInfoContainerId() {
               
        assertEquals(id, info.getId());

    }
    
    
    @Test
    public void testInfoContainerSanityLower(){
        
        assertEquals(sanityLower, info.getSanityLower());
        
    }
    
    @Test
    public void testInfoContainerSanityUpper(){
        
        assertEquals(sanityUpper, info.getSanityUpper());
    }
    
    @Test
    public void testInfoContainerHardLower(){
        
        assertEquals(hardLower, info.getHardLower());
    }
    
    @Test
    public void testInfoContainerHardUpper(){
        
        assertEquals(hardUpper, info.getHardUpper());
    }
    
    @Test
    public void testInfoContainerSoftLower(){
        
        assertEquals(softLower, info.getSoftLower());
    }
    
    
    @Test
    public void testInfoContainerSoftUpper(){
        
        assertEquals(softUpper, info.getSoftUpper());
    }
    
    
    @Test
    public void testSanityLimitsAvailable(){
        
        assertTrue(info.isSanityCheckAvailable());
    }

}
