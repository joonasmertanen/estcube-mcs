/** 
 *
 */
package eu.estcube.gs.tnc.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.hbird.exchange.constants.StandardArguments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.camel.serial.SerialConsumer;
import eu.estcube.common.Headers;
import eu.estcube.common.TimestampExtractor;
import eu.estcube.domain.transport.Downlink;
import eu.estcube.gs.tnc.Tnc;
import eu.estcube.gs.tnc.domain.TncDriverConfiguration;

/**
 *
 */
@Component
public class AddHeaders implements Processor {

    public static final String DEFAULT_CONTACT_ID = "unknown";

    @Autowired
    private TncDriverConfiguration config;

    @Autowired
    private LocationContactEventHolder eventHolder;

    @Autowired
    private TimestampExtractor timestampExtractor;

    /** @{inheritDoc . */
    @SuppressWarnings("deprecation")
    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        out.copyFrom(in);

        long timestamp = timestampExtractor.getTimestamp(in);
        String eventId = eventHolder.getContactId(timestamp);
        if (eventId == null) {
            eventId = DEFAULT_CONTACT_ID;
        }

        out.setHeader(StandardArguments.TIMESTAMP, timestamp);
        out.setHeader(StandardArguments.ISSUED_BY, config.getServiceId());
        out.setHeader(StandardArguments.SATELLITE_ID, config.getSatelliteId());
        out.setHeader(StandardArguments.GROUND_STATION_ID, config.getGroundstationId());
        out.setHeader(StandardArguments.CONTACT_ID, eventId);
        out.setHeader(StandardArguments.CLASS, in.getBody().getClass().getSimpleName());
        out.setHeader(StandardArguments.TYPE, Tnc.TNC);
        out.setHeader(Headers.SERIAL_PORT_NAME, in.getHeader(SerialConsumer.SERIAL_PORT));
        out.setHeader(Headers.COMMUNICATION_LINK_TYPE, Downlink.class.getSimpleName());
    }
}
