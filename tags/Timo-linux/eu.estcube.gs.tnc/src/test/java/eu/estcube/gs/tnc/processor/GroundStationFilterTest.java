/** 
 *
 */
package eu.estcube.gs.tnc.processor;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import org.hbird.exchange.navigation.LocationContactEvent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.gs.tnc.domain.TncDriverConfiguration;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class GroundStationFilterTest {

    private static final String GS_ID_1 = "GS-1";
    private static final String GS_ID_2 = "GS-2";

    @Mock
    private TncDriverConfiguration config;

    @Mock
    private LocationContactEvent event;

    @InjectMocks
    private GroundStationFilter filter;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        inOrder = inOrder(config, event);
    }

    @Test
    public void testMatches() throws Exception {
        when(config.getGroundstationId()).thenReturn(GS_ID_1);
        when(event.getGroundStationID()).thenReturn(GS_ID_1);
        assertTrue(filter.matches(event));
        inOrder.verify(config, times(1)).getGroundstationId();
        inOrder.verify(event, times(1)).getGroundStationID();
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void testNoMatch() throws Exception {
        when(config.getGroundstationId()).thenReturn(GS_ID_1);
        when(event.getGroundStationID()).thenReturn(GS_ID_2);
        assertFalse(filter.matches(event));
        inOrder.verify(config, times(1)).getGroundstationId();
        inOrder.verify(event, times(1)).getGroundStationID();
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void testIdNullInEvent() throws Exception {
        when(config.getGroundstationId()).thenReturn(GS_ID_1);
        when(event.getGroundStationID()).thenReturn(null);
        assertFalse(filter.matches(event));
        inOrder.verify(config, times(1)).getGroundstationId();
        inOrder.verify(event, times(1)).getGroundStationID();
        inOrder.verifyNoMoreInteractions();
    }
}
