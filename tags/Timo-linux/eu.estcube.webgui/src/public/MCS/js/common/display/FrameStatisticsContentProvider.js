define([
    "dojo/_base/declare", 
    "dojo/_base/lang", 
    "dojo/_base/array", 
    "dgrid/Grid",
    "./ContentProvider", 
    "common/store/TransportFrameStore",
    ], 
    function(declare, Lang, Arrays, Grid, ContentProvider, TransportFrameStore) {
    	
    	var statsGrid;
    	    	
        return declare(ContentProvider, {        
           	grid: statsGrid,
                     
            getContent: function (args) {
                declare.safeMixin(this, args);
                statsGrid = new Grid({
                    id: null,
                    class: "frame-stats-grid",
                    columns: {
                        GS: {
                            label: "Ground Station",
                            className: "frame-stats-gs",
                        },
                        count: {
                            label: "Frame Count",
                            className: "frame-stats-count",
                        },
                        ratio: {
                        	label: "Percentage",
                        	className: "frame-stats-ratio",
                        },
                    },
                });
                
                
                // update data
                var queryResults = TransportFrameStore.query();
                queryResults.observe( function(obj, oldIndex, newIndex) {
                	// Create compound array for grid display
                	var tempArray = [];
                	var totalCount = TransportFrameStore.data.length;
                	var groundStationIDs = TransportFrameStore.groundStationIDs;
                	var frameCounts = TransportFrameStore.frameCounts;
                	
                	for(var i = 0; i < frameCounts.length; i++) {
                		if (totalCount > 0)
	                		tempArray[i] = {GS:groundStationIDs[i], count:frameCounts[i], ratio: frameCounts[i] / totalCount * 100.0};
	                	else
	                		tempArray[i] = {GS:groundStationIDs[i], count:frameCounts[i], ratio: 0.0};
                	}

					statsGrid.refresh(); // removes all old data from grid
					statsGrid.set( "showHeader", true );
                	statsGrid.renderArray(tempArray);
                }, true );
                
                
				return statsGrid;
            },

            startup: function() {
  				statsGrid.startup();
            },
            
        });
    });
    



