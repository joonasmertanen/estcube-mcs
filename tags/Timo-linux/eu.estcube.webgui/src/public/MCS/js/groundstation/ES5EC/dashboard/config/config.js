define([
    "dojo/domReady!"
    ],

    function(ready) {

        return {
            routes: {
            	ES5EC_DASHBOARD: {
                    path: "ES5EC/dasboard",
                    defaults: {
                        controller: "ES5ECDashboard/DashboardController",
                        method: "index",
                    }
                },
            },


        };
    }
);