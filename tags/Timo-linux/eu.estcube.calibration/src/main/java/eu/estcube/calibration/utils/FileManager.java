package eu.estcube.calibration.utils;

import java.io.File;
import java.io.FilenameFilter;

import eu.estcube.calibration.constants.CalibrationConstants;

public class FileManager {

    public static File[] getFiles() {

        String currentDirectory = System.getProperty(CalibrationConstants.PATHPROPERTY);

        File folder = new File(currentDirectory + File.separator + "calibrate");

        FilenameFilter filter = new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {

                return name.endsWith(".xml");
            }
        };

        File[] files = folder.listFiles(filter);

        return files;
    }

}
