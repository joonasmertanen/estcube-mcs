package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SetPosition implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+P ");
        messageString.append(telemetryCommand.getParams().get("Azimuth"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Elevation"));
        messageString.append("\n");
        return messageString;
    }
}
