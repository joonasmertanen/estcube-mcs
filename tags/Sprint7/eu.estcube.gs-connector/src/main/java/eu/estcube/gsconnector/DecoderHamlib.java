package eu.estcube.gsconnector;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneDecoder;
import org.springframework.stereotype.Component;

import eu.estcube.domain.JMSConstants;

@Component(JMSConstants.GS_HAMLIB_DECODER)
public class DecoderHamlib extends OneToOneDecoder {

    private StringBuffer messageBuffer = new StringBuffer();

    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel evt, Object msg) throws Exception {
        String checkRPRT = msg.toString();
        messageBuffer.append(checkRPRT);

        if (checkRPRT.contains(JMSConstants.GS_DEVICE_END_MESSAGE)) {
            String message = messageBuffer.toString();
            messageBuffer.setLength(0);
            return message;
        }
        return null;

    }

}
