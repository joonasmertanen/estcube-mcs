package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SetFunc implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+U ");
        messageString.append(telemetryCommand.getParams().get("Func"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Func Status"));
        messageString.append("\n");
        return messageString;
    }
}
