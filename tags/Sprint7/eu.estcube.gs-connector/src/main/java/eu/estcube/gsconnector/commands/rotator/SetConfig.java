package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SetConfig implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+C ");
        messageString.append(telemetryCommand.getParams().get("Token"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Value"));
        messageString.append("\n");
        return messageString;
    }
}
