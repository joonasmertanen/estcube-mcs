package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SetTransmitFrequency implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+l ");
        messageString.append(telemetryCommand.getParams().get("TX Frequency"));
        messageString.append("\n");
        return messageString;
    }
}
