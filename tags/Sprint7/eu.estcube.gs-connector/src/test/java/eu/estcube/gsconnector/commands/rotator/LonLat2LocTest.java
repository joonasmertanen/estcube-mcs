package eu.estcube.gsconnector.commands.rotator;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRotatorConstants;

public class LonLat2LocTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private LonLat2Loc createMessage = new LonLat2Loc();

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.LONLAT2LOC);
        telemetryCommand.addParameter("Longitude", 1);
        telemetryCommand.addParameter("Latitude", 1);
        telemetryCommand.addParameter("Loc Len", 1);
        string.append("+L 1 1 1\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
