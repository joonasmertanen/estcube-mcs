package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class ConvertMW2PowerTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private ConvertMW2Power createMessage = new ConvertMW2Power();
    
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand(TelemetryRadioConstants.CONVERT_MW2POWER);
        telemetryCommand.addParameter("Power mW", 0);
        telemetryCommand.addParameter("Frequency", 0);
        telemetryCommand.addParameter("Mode", "..");
        string.append("+4 0 0 ..\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
