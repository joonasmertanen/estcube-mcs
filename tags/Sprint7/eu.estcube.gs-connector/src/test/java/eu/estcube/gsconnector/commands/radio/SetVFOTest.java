package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class SetVFOTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SetVFO createMessage = new SetVFO();
    
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand(TelemetryRadioConstants.SET_VFO);
        telemetryCommand.addParameter("VFO", "VFO");
        string.append("+V VFO\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
