package eu.estcube.gsconnector.commands.rotator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRotatorConstants;

public class AzimuthShortPath2LongPathTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private AzimuthShortPath2LongPath createMessage = new AzimuthShortPath2LongPath();

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.AZIMUTH_SHORTPATH2LONGPATH);
        telemetryCommand.addParameter("Short Path Deg", 0.0);
        string.append("+A 0.0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
