package eu.estcube.webserver;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.TimeZone;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import eu.estcube.domain.TelemetryObject;

@Component
public class TelemetryObjectToJson implements Processor {
    private static final Logger LOG = LoggerFactory.getLogger(TelemetryObjectToJson.class);

    public void process(Exchange ex) throws Exception {
        TelemetryObject telemetryObject = ex.getIn().getBody(TelemetryObject.class);

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new JsonDateSerializer());
       
        Gson gson = gsonBuilder.create();      
        String json = gson.toJson(telemetryObject);     

        ex.getOut().setBody(json);
    }
}
