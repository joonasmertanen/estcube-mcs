define({

    DEFAULT_DATE_FORMAT:        		"yyyy-DDD HH:mm:ss",
    EXPIRATION_DATE:					"365",
    	
    WEB_SOCKET_PORT:      				"1337",
    
    TOPIC_CHANNEL_REQUEST:      		"/channel/request",
    TOPIC_CHANNEL_EVENT:        		"/channel/event",
    TOPIC_CHANNEL_SEND_MESSAGE: 		"/channel/message",

    TOPIC_SELECTION_PARAMETER:  		"/selection/parameter",
    TOPIC_SELECTION_COMMAND:    		"/selection/command",

    TOPIC_PARAMETER_SHOW:       		"/parameter/show",
    TOPIC_PARAMETER_HIDE:       		"/parameter/hide",

    DND_TYPE_PARAMETER:         		"/dnd/type/parameter",
    DND_TYPE_COMMAND:           		"/dnd/type/command",
    
    SATELLITE_NAVIGATION_BAR_URL: 		"/MCS/satellites/ESTCube-1/data/satelliteNavbar.json",
    FS_NAVIGATON_BAR_URL:				"/MCS/satellites/ESTCube-1-FS/data/satelliteNavbar.json",
    GROUND_STATION_NAVIGATION_BAR_URL: 	"/MCS/gs/ES5EC/data/groundStationNavbar.json",
    COMPONENTS_NAVIGATION_BAR_URL:		"/MCS/data/generalWidgets.json",
    COMMON_WIDGETS_NAVIGATION_BAR_URL:	"/MCS/data/generalWidgets.json"

});
