define(["dojo/_base/declare",
        "dojo/_base/window",
        "dojo/date/locale",
        "dojo/io-query",
        "dojo/_base/xhr",
        "dojo/dom", 
        "dojo/_base/array",
		"dojo/dom-construct", 
		"dojo/on",
        "./Constants"
       ], function(declare, win, locale, ioQuery, xhr, dom, arrays, domConstruct, on, Constants) {
        
        function checkLinkType(item) {
        	if(item.type == "Satellite" || item.type == "Satellite FS" || item.type == "Ground Station") {
        		return true;
        	}
        	return false;
        }
        
        function createListElements(data, list, proxy) {
			arrays.forEach(data, function(component, index) {
					var item = domConstruct.create("li", {
						innerHTML : component.abbreviation,
						id : component.id
					}, list);
                var url = window.location.host + "/MCS/" + component.link;
				on(item, "click", dojo.hitch(component, function(event) {
					if(checkLinkType(component)) {
						window.location = "http://" + url;
					}
					else if(!(proxy || checkLinkType(component))) {
						window.location = "http://" + url + "?proxy=ws";
					}
					else {
						window.location = "http://" + url + "?proxy=" + proxy;
					}
				}));
			});
        }
        
        function setUserRoles(data, list) {
			arrays.forEach(data, function(role, index) {
				var item = domConstruct.create("li", {
					innerHTML : role 
				}, list);
			});
        }
        
        function setUserName(data, list) {
        	var node = dom.byId(list).innerHTML = data;
        }
        
        return {

            hashCode: function(str) {
                var hash = 0;
                if (str.length == 0) {
                    return code;
                }
                for (i = 0; i < str.length; i++) {
                    char = str.charCodeAt(i);
                    hash = 31 * hash + char;
                    hash = hash & hash; // Convert to 32bit integer
                }
                return hash.toString(16);
            },
            
            loadJsonData : function(url, list, proxy, successCallback) {
				xhr.get({
                    url: url,
                    handleAs: "json",
                    load: function(data) {
                    	if(!successCallback) {
                    		createListElements(data, list, proxy);
                    	}
                    	else {
                    		successCallback();
                    	}
                    }
				});
            },
            
            loadUser : function(name, roles) {
				xhr.get({
                    url: "/user",
                    handleAs: "json",
                    load: function(data) {
                    	setUserName(data.username, name);
                    	setUserRoles(data.roles, roles);
                    }
				});
            },
            
            hideNavigationBar : function() {
            	dojo.style(dojo.byId('collapseRight'), "display", "block");
				dojo.style(dojo.byId('collapseLeft'), "display", "none");
				if(arguments.length == 0) {
					dojo.style(dojo.byId('gerenalWidgets'), "display", "none");
				}
				else {
					dojo.style(dojo.byId(arguments[0]), "display", "none");
				}
				dojo.style(dojo.byId('collapse'), "left", "0px");
            },
            
            showNavigationBar : function() {
            	dojo.style(dojo.byId('collapseRight'), "display", "none");
				dojo.style(dojo.byId('collapseLeft'), "display", "block");
				if(arguments.length == 0) {
					dojo.style(dojo.byId('gerenalWidgets'), "display", "block");
				}
				else {
					dojo.style(dojo.byId(arguments[0]), "display", "block");
				}
				dojo.style(dojo.byId('collapse'), "left", "234px");
            },
            
        	showDialog : function(title, text){
        		var d = $("<div>" + text + "</div>" ).dialog({
        			resizable: false,
        			modal: false,
        			title: title,
        			height: 150,
        			width: 250,
        			autoOpen: false
        		});
        		d.dialog('open');
        		return d;
        	}
            
        };
});
