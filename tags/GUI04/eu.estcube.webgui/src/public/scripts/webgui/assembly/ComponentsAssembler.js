define([
    "dojo/_base/declare",
    "dojo/data/ItemFileWriteStore",
    "../common/Constants",
    "./AssemblerBase",
    "../net/WebSocketProxy",
    "../display/ComponentsView"
    ],

    function(declare, ItemFileWriteStore, Constants, base, proxy, view) {
        return [

            declare("ComponentsAssembler", AssemblerBase, {

                build: function() {
                    this.setupCacheRequest({ delay: 1500, channels: ["/system"] });
                    this.setupCommunication();

                    var store =  new ItemFileWriteStore({
                        data: {
                            identifier: "id",
                            items: [],
                        }
                    });

                    var view = new ComponentsView({ store: store, parentDivId: "CenterContainer" });
                    var controller = new ComponentsController({ id: "System Components", channels: ["/system"], store: store });
                }

            })

        ];
    }
);