define([
    "dojo/_base/declare",
    "dojo/data/ItemFileWriteStore",
    "../common/Constants",
    "./AssemblerBase",
    "../net/WebSocketProxy",
    "../display/DashboardView"
    ],

    function(declare, ItemFileWriteStore, Constants, base, proxy, view) {
        return [

            declare("DashboardAssembler", AssemblerBase, {

            	build: function(components) {
                    this.setupCacheRequest({ delay: 1500, channels: ["/parameters"], });
                    this.setupCommunication();
                    
                    var components = this.object;
                    
                    var store =  new ItemFileWriteStore({
                        data : {
                            identifier : "storeId",
                            items: [],
                        }
                    });
                    
                    var controller = new DashboardController({ id: "Parameters Dashboard", channels: ["/parameters"], store: store });
                    var view = new DashboardView({ parentDivId: "dashboardContainer", store: store, object: components});    
                }

            })

        ];
    }
);