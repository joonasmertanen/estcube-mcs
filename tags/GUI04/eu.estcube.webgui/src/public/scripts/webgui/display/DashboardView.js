define(
		["dojo/_base/declare",
		 "dojox/grid/DataGrid",
		 "dojox/layout/GridContainer",
		 "dojox/widget/Portlet",
		 "../common/Constants",
		 "../common/Utils",
		 "dijit/form/TextBox",
		 "./ViewBase"
		 ],
		 
		 function(declare, DataGrid, GridContainer, Portlet, Constants, Utils, TextBox, ViewBase){
			
			return [

		            declare("DashboardController", Controller, {

		           			        		channelHandler: function(message, channel) {
		           			        			if (this.store != null) {		           			                        
		           			                        
		           			                   this.store.fetch({
		           			                        scope: this,
		           			                        onItem: function(item){
		           			                        	if (item == null) {
		           			                                this.store.newItem({
		           			                                    datasetidentifier: message.datasetidentifier,
		           			                                    timestamp: message.timestamp,
		           			                                    paramName: message.name,
		           			                                    paramValue: message.value,
		           			                                });
		           			                            } else {
		           			                                this.store.setValue(item, "timestamp", message.timestamp);
		           			                                this.store.setValue(item, "datasetidentifier", message.datasetidentifier);
		           			                                this.store.setValue(item, "paramName", message.name);
		           			                                this.store.setValue(item, "paramValue", message.value);
		           			                            }
		           			                        	
		           			                        }
		           			                        })
		           			        			}      
		           			        		}
		           			        }),

			        
					declare("DashboardView", View, {
						
					divId: "DashboardView",
						
						build: function(){	
							var gridContainer = new dojox.layout.GridContainer({
								nbZones: 3,
								opacity: .5,
								hasResizableColumns: false,
								allowAutoScroll: false,
								withHandles: true,
								dragHandleClass: 'dijitTitlePaneTitle',
								style: {
									width:'100%',
									color: 'black'
								},
								acceptTypes: ['Portlet'],
								isOffset: true
								}, this.parentDivId);
							
							var store = this.store;
							
							for(var i = 0; i < this.object.length; i++){		
								var textBox = new dijit.form.TextBox();
								textBox.attr({value: store.paramName +' : '+ store.paramValue, disabled: 'disabled'})
								
								var portlet = new Array();
								portlet[i] = new dojox.widget.Portlet({
									id: 'Portlet'[i],
									closable: false,
									dndType: 'Portlet',
									title: this.object[i],
//									content: portletContent[i]
								});
								portlet[i].addChild(textBox);
								gridContainer.addChild(portlet[i]);
							}

							gridContainer.startup();
					}
				})
			];
});

