dojo.provide("webgui.common.Constants");

dojo.require("dojo.parser");

webgui.common.Constants.SOCKET = "ws://localhost:9292/foo/";
webgui.common.Constants.parseDate = function(timestamp){
    return dojo.date.locale.parse(timestamp, {datePattern: "yyyy-DD HH:mm:ss.SSSZ", selector: "date"});
};
webgui.common.Constants.formatDate = function(timestamp){
    return dojo.date.locale.format(timestamp, {datePattern: "yyyy-DD HH:mm:ss", selector: "date"});
};
webgui.common.Constants.telemetryCommandTime = "time";
webgui.common.Constants.telemetryCommandValue = "value";
webgui.common.Constants.telemetryCommandName = "name";
webgui.common.Constants.GUIVERSION="0.0.1-SNAPSHOT";
webgui.common.Constants.COMPONENT_WEBSERVER = "Webserver";
webgui.common.Constants.COMPONENT_CONNECTOR = "Connector";
webgui.common.Constants.COMPONENT_WEBCAMERA_TRANSMITTER = "Webcamera transmitter";
webgui.common.Constants.COMPONENT_PARAMETER_STORAGE = "Parameter storage";