dojo.provide("webgui.display.MainTabDisplay")
dojo.require("webgui.display.Graph");

dojo.require("dijit.layout.TabContainer");
dojo.require("dijit.layout.StackContainer");
dojo.require("dijit.layout.ContentPane");
dojo.require("dojox.charting.Chart");
dojo.require("dojox.charting.themes.Claro");
dojo.require("dojo.store.Observable");
dojo.require("dojo.store.Memory");
dojo.require("dojox.charting.StoreSeries");
;
dojo.require("dojox.charting.action2d.MoveSlice");
dojo.require("dijit.Tooltip");

dojo.require("dojox.charting.widget.Legend");
dojo.require("dojox.charting.action2d.Tooltip");
dojo.require("dojox.charting.action2d.Magnify");
dojo.require("dojox.grid.DataGrid");
dojo.require("dojox.charting.DataChart");

dojo.require("dojo.parser");
dojo.require("dojox.data.PersevereStore");
dojo.require("dojox.cometd.RestChannels");
dojo.require("dojo.cookie");
dojo.require("dojo.date.locale");

dojo.require("dojox.charting.themes.PlotKit.red");

dojo.require("webgui.common.CommandSplitter");

dojo.declare("webgui.display.MainTabDisplay", null, {

	constructor : function(graphYRange) {
		console.log("TabDisplay");

		var selectedTarget = "rotctld";

		var telemetryTab = new dijit.layout.ContentPane({
			id : "chartTab",
			title : "Telemetry",
			preventCache : true

		});

		var gsTab = new dijit.layout.ContentPane({
			title : "GS",
			content : "Groundstation online!",
			closable : "true",
			preventCache : true
		});

		var commandTab = new dijit.layout.BorderContainer({
			id : "commandTab",
			title : "Command",
			preventCache : true
		});

		var commandLogA = new dijit.layout.ContentPane({
			id : "commandLogA",
			title : "LogArea",
			region : "center",
			preventCache : true
		});

		var commandA = new dijit.layout.ContentPane({
			id : "commandA",
			region : "bottom",
			title : "CmdArea",
			content : "Insert Commands:",
			preventCache : true
		});

		commandTab.addChild(commandLogA);
		commandTab.addChild(commandA);

		var tabs = new dijit.layout.TabContainer({
			region : "center",
			tabPosition : "bottom",
			id : "mainTabContainer",
			preventCache : true
		});

		var tempCont;
		tempCont = dijit.byId("CenterContainer");

		tabs.addChild(telemetryTab);
		tabs.addChild(gsTab);
		tabs.addChild(commandTab);
		tempCont.addChild(tabs);
		tempCont.selectChild(tabs);
		tabs.startup();

		var chartArea = dojo.create("div", {
			id : "chartArea",
			region : "center",
			style : "width:100%; height:90%;"
		}, "chartTab");

		var chart = new webgui.display.Graph(chartArea, "GetPositionAzimuth", graphYRange);

		var cmdLogArea = dojo.create("div", {
			id : "commandLogArea2",
			region : "center",
			style : "width:100%; height:90; overflow:auto;"
		}, "commandLogA");

		var someData = [];
		var storeGS = new dojo.data.ItemFileWriteStore({
			data : {
				items : someData
			}
		});

		var tableHeader = [{
			field : "name",
			name : "Telemetry Command",
			width : 11
		}, {
			field : "params",
			name : "Parameters",
			width : '100%',
			formatter : function(params, rowIndex) {
				var item = this.grid.getItem(rowIndex);
				return JSON.stringify(item.params);
			}
		}];

		var cmdLogArea = new dojox.grid.DataGrid({
			id : "commandLogArea",
			title : "Telemetry Commands",
			store : storeGS,
			width : '100%',
			structure : tableHeader
		}, "commandLogArea2");

		cmdLogArea.startup();
		cmdLogArea.canSort = false;

		var cmdArea = dojo.create("div", {
			id : "commandArea",
			region : "center",
			style : "border: none"
		}, "commandA");

		var commandBox = new dijit.form.TextBox({
			id : "commandTextBox",
			value : "GET_POSITION"
		}).placeAt("commandArea");

		var buttonSend = new dijit.form.Button({
			label : "Send",
			onClick : function() {
				var cmdTextBox = dijit.byId("commandTextBox");
				var split = cmdTextBox.get('value').split(" ");

				var telemetryCommand = splitCommand(selectedTarget, cmdTextBox.get('value'), split);

				if(cmdLogArea.rowCount > 0) {
					cmdLogArea.store.deleteItem(cmdLogArea.getItem(cmdLogArea.rowCount - 1));
				}
				storeGS.newItem(telemetryCommand);
				storeGS.newItem({
					name : ''
				});

				cmdLogArea.scrollToRow(cmdLogArea.rowCount);
			}
		}).placeAt("commandArea");

		var buttonClear = new dijit.form.Button({
			label : "Clear",
			onClick : function() {
				cmdLogArea.store.fetch({
					onItem : function(item) {
						cmdLogArea.store.deleteItem(item);
					}
				});
			}
		}).placeAt("commandArea");

		function insertCommand() {
			console.log(dijit.byId("commandTextBox").value);
			var cmdVal = dijit.byId("commandVal").value;
			var azimVal = dijit.byId("azimuthVal").value;
			var elevVal = dijit.byId("elevationVal").value;
			dijit.byId("commandTextBox").set('value', cmdVal + " Azimuth " + azimVal + " Elevation " + elevVal);
		}

		var dialogCmd = new dijit.TooltipDialog({
			id : "dlgCmd",
			content : '<label for="command">Command:</label> <br> <input dojoType="dijit.form.TextBox" id="commandVal" name="command" value="SET_POSITION"><br>' + '<label for="azimuth">Azimuth:</label> <br> <input dojoType="dijit.form.TextBox" id="azimuthVal" name="azimuth"><br>' + '<label for="elevation">Elevation:</label> <br> <input dojoType="dijit.form.TextBox" id="elevationVal" name="elevation" region="right"><br>'
		});

		var buttonCmd = new dijit.form.DropDownButton({
			id : "btnCommand",
			label : "Commands",
			dropDown : dialogCmd
		}).placeAt("commandArea");

		var buttonInsert = new dijit.form.Button({
			label : "Insert",
			onClick : insertCommand
		}).placeAt("commandArea");

		// Button for changing component!
		var targetComponentMenu = new dijit.Menu({
			style : "display: none;"
		});

		var targetRotator = new dijit.MenuItem({
			label : "Rotator",
			id : "targetRotatorId",
			onClick : function() {
				selectedTarget = "rotctld";
				alert("Rotator set as target! (" + selectedTarget + ")");
			}
		});
		targetComponentMenu.addChild(targetRotator);

		var targetRadio = new dijit.MenuItem({
			label : "Radio",
			id : "targetRadioId",
			onClick : function() {
				selectedTarget = "rigctld";
				alert("Radio set as target! (" + selectedTarget + ")");
			}
		});
		targetComponentMenu.addChild(targetRadio);

		var buttonCmd = new dijit.form.DropDownButton({
			id : "btnTarget",
			label : "Target component",
			dropDown : targetComponentMenu,
		}).placeAt("commandArea");

	}
});
