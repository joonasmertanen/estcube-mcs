dojo.provide("webgui.display.InfoDisplay");

dojo.require("dijit.layout.ContentPane");

dojo.declare("webgui.display.InfoDisplay", null, {

	constructor : function(view) {

		console.log("InfoDisplay!");

		var statusesInfoTab = new dijit.layout.ContentPane({
			id : "mainInfoTab",
			title : "Main Info",
			content : "Some kind of raw Info!",
			preventCache : true
		});

		var tempCont;
		tempCont = dijit.byId("InfoView");
		tempCont.addChild(statusesInfoTab);

	}
});
