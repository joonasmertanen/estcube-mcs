package eu.estcube.weatherstation;

import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_PRESSURE;
import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_TEMPERATURE;
import static eu.estcube.weatherstation.DefaultWeatherParameters.GAMMA;
import static eu.estcube.weatherstation.DefaultWeatherParameters.LUX;
import static eu.estcube.weatherstation.DefaultWeatherParameters.PRECIPITATIONS;
import static eu.estcube.weatherstation.DefaultWeatherParameters.RELATIVE_HUMIDITY;
import static eu.estcube.weatherstation.DefaultWeatherParameters.SOLE;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WIND_DIRECTION;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WIND_SPEED;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.hbird.exchange.core.Named;
import org.jdom2.Document;
import org.jdom2.Element;

public class MeteoHandlerImpl extends AbstractDataHandler {

    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private static final DateFormat FORMATTER = new SimpleDateFormat(DATE_FORMAT);

    @SuppressWarnings({ "unchecked", "rawtypes" })
    protected Map<String, NamedConfig<? extends Named>> getParameterMapping() {
        Map<String, NamedConfig<? extends Named>> map = new HashMap<String, NamedConfig<? extends Named>>();

        map.put("temp", new NamedConfig(AIR_TEMPERATURE, new DoubleParameterFactory()));
        map.put("humid", new NamedConfig(RELATIVE_HUMIDITY, new DoubleParameterFactory()));
        map.put("wind_dir", new NamedConfig(WIND_DIRECTION, new LabelFactory()));
        map.put("wind_len", new NamedConfig(WIND_SPEED, new DoubleParameterFactory()));
        map.put("lux", new NamedConfig(LUX, new DoubleParameterFactory()));
        map.put("sole", new NamedConfig(SOLE, new DoubleParameterFactory()));
        map.put("gamma", new NamedConfig(GAMMA, new DoubleParameterFactory()));
        map.put("precip", new NamedConfig(PRECIPITATIONS, new DoubleParameterFactory()));
        map.put("baro", new NamedConfig(AIR_PRESSURE, new DoubleParameterFactory()));

        return map;
    }

    protected String getDataSourceName() {
        return "meteo.physic.ut.ee";
    }

    /** @{inheritDoc}. */
    @Override
    protected Element getParentNodeForWeatherData(Document document) {
        return document.getRootElement();
    }

    /** @{inheritDoc}. */
    @Override
    protected String getAttribute(Element element) {
        return element.getAttributeValue("id");
    }

    /** @{inheritDoc}. */
    @Override
    protected Date parseDate(Document document) throws ParseException {
        String time = document.getRootElement().getAttributeValue("time");
        return FORMATTER.parse(time);
    }
}
