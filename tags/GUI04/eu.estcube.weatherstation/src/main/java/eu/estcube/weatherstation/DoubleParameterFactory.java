/** 
 *
 */
package eu.estcube.weatherstation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DoubleParameterFactory extends ParameterFactory<Double> {

    private static final Logger LOG = LoggerFactory.getLogger(DoubleParameterFactory.class);

    /** @{inheritDoc}. */
    @Override
    protected Double parse(String value) {
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException nfe) {
            LOG.debug("Failed to parse string {} to double. Returning null", value);
            return null;
        }
    }
}
