/** 
 *
 */
package eu.estcube.weatherstation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class IntegerParameterFactory extends ParameterFactory<Integer> {

    public static final int DEFAULT_VALUE = -0;

    private static final Logger LOG = LoggerFactory.getLogger(IntegerParameterFactory.class);

    /** @{inheritDoc}. */
    @Override
    protected Integer parse(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException nfe) {
            LOG.warn("Failed to parse int from string {}. Falling back to default value {}", value, DEFAULT_VALUE);
            return DEFAULT_VALUE;
        }
    }
}
