package eu.estcube.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hbird.exchange.core.Label;

public class TelemetryObject implements Serializable {

    private static final long serialVersionUID = 2092380317315772625L;
    private final String name;
    private String source;
    private String device;
    private Date time;
    private List<Label> labels;

    public TelemetryObject(String name, Date time) {
        this.name = name;
        this.time = time;
        labels = new ArrayList<Label>();
    }

    public TelemetryObject(String name, List<Label> params) {
        this.name = name;
        this.labels = params;
        this.time = new Date();
    }

    public String getName() {
        return name;
    }

    public List<Label> getLabels() {
        return labels;
    }

    public Label getLabel(String name) {
        for (Label label : labels) {
            if (label.getName().equals(name)) {
                return label;
            }
        }
        return null;
    }

    public void addLabels(List<Label> labels) {
        this.labels.addAll(labels);
    }

    public void addLabel(Label label) {
        this.labels.add(label);
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String toString() {
        String result = name + "\n" + source + "\n" + device + "\n" + time.toString() + "\n";
        for (Label label : labels) {
            result += label.toString();
        }
        result += "\n\n";
        return result;
    }
}
