package eu.estcube.domain;

import java.io.Serializable;

import org.hbird.exchange.core.Label;

public class GroundstationLabel extends Label implements Serializable {

    private static final long serialVersionUID = 1340205089337640173L;

    public GroundstationLabel(String name) {
        super(null, name, null, null, null);
    }

    public GroundstationLabel(String name, String value) {
        super(null, name, null, null, value);
    }
}
