package eu.estcube.domain;

import java.io.Serializable;
import java.util.Map;

import org.hbird.exchange.core.Command;
import org.hbird.exchange.core.Label;

public class TelemetryCommand extends Command implements Serializable {

    private static final long serialVersionUID = 2092380317315772625L;
    private String commandName;
    private String device;

    public TelemetryCommand(String name) {
        super(null, null, name, null);
        this.commandName = name;
    }

    public TelemetryCommand(String name, Map<String, Object> params) {
        super(null, null, name, null);
        this.commandName = name;
        setArguments(params);
    }

    public String getCommandName() {
        return commandName;
    }

    public Map<String, Object> getParams() {
        return getArguments();
    }

    public void addLabel(Label label) {
        getArguments().put(label.getName(), label);
    }

    public String getLabelValue(String name) {
        Label label = (Label) getArguments().get(name);
        return label != null ? label.getValue() : null;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }
}
