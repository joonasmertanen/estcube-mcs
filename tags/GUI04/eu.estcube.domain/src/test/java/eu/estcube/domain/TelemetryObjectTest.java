/**
 * 
 */
package eu.estcube.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hbird.exchange.core.Label;
import org.junit.Before;
import org.junit.Test;

public class TelemetryObjectTest {

    private static final String NAME = "A";

    private TelemetryObject to, to2, to3;
    private Label l1, l2;
    private List<Label> labels;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        labels = new ArrayList<Label>();
        l1 = new GroundstationLabel("Name1", "100");
        l2 = new GroundstationLabel("Name2", "101");
        to = new TelemetryObject("B", new Date());
        to2 = new TelemetryObject(NAME, labels);
        to3 = new TelemetryObject("C", new Date());
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryObject#TelemetryObject(java.lang.String)}.
     */
    @Test
    public void testTelemetryObjectString() {
        assertNotNull(to);
        assertEquals(0, to.getLabels().size());
        assertEquals("B", to.getName());
        assertNotNull(to.toString());
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryObject#TelemetryObject(java.lang.String, java.util.List)}.
     */
    @Test
    public void testTelemetryObjectStringListOfTelemetryParameter() {
        assertNotNull(to2);
        assertEquals(0, to2.getLabels().size());
        assertEquals(NAME, to2.getName());
        assertEquals(labels, to2.getLabels());
        assertNotNull(to2.toString());
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryObject#getName()}.
     */
    @Test
    public void testGetName() {
        assertEquals("B", to.getName());
        assertEquals(NAME, to2.getName());
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryObject#getLabels()}.
     */
    @Test
    public void testGetParams() {
        to.addLabels(labels);
        labels.add(l1);
        labels.add(l2);
        to.addLabel(l1);
        to.addLabel(l2);
        assertEquals(labels, to.getLabels());
        assertEquals(labels, to2.getLabels());
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryObject#getParameter()}.
     */
    @Test
    public void testGetParameter() {
        to.addLabel(l1);
        to2.addLabel(l1);
        to2.addLabel(l2);
        assertEquals(l1, to.getLabel("Name1"));
        assertEquals(null, to.getLabel("Name2"));
        assertEquals(l2, to2.getLabel("Name2"));
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryObject#addLabels(java.util.List)}.
     */
    @Test
    public void testAddParams() {
        to.addLabels(labels);
        assertEquals(labels, to.getLabels());
        labels.add(l1);
        labels.add(l2);
        to3.addLabels(labels);
        assertEquals(labels, to3.getLabels());
    }

    /**
     * Test method for {@link eu.estcube.domain.TelemetryObject#addLabel(java.util.List)}.
     */
    @Test
    public void testAddParameter() {
        to.addLabel(l1);
        assertEquals(l1, to.getLabel("Name1"));
        to2.addLabel(l2);
        assertEquals(l2, to2.getLabel("Name2"));
    }

}
