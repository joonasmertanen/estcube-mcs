package eu.estcube.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.hbird.exchange.core.Label;
import org.junit.Before;
import org.junit.Test;

public class CommandTest {

    private static final String TEST_NAME = "A";
    private static final String TEST_VALUE = "20";
    private static final Map<String, Object> PARAMS = new HashMap<String, Object>();

    private TelemetryCommand tc, tc2;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        tc = new TelemetryCommand(TEST_NAME);
        tc2 = new TelemetryCommand(TEST_NAME, PARAMS);
    }

    @Test
    public void testTelemetryCommandString() {
        assertNotNull(tc);
        assertNotNull(tc.getArguments());
        assertEquals(0, tc.getArguments().size());
        assertEquals(TEST_NAME, tc.getCommandName());
        assertNotNull(tc.toString());
    }

    @Test
    public void testTelemetryCommandStringMapOfStringObject() {
        assertNotNull(tc2);
        assertNotNull(tc2.getArguments());
        assertEquals(0, tc2.getArguments().size());
        assertEquals(TEST_NAME, tc2.getCommandName());
        assertNotNull(tc2.toString());
    }

    @Test
    public void testGetName() {
        assertEquals(TEST_NAME, tc.getCommandName());
    }

    @Test
    public void testgetArguments() {
        assertEquals(PARAMS, tc.getArguments());
    }

    @Test
    public void testAddParameter() {
        tc.addLabel(new GroundstationLabel(TEST_NAME, TEST_VALUE));
        assertEquals(TEST_VALUE, tc.getLabelValue(TEST_NAME));
    }

    @Test
    public void testGetParameter() {
        tc.addLabel(new GroundstationLabel(TEST_NAME, TEST_VALUE));
        assertEquals(TEST_VALUE, tc.getLabelValue(TEST_NAME));
    }

    @Test
    public void getParameters() {
        tc.addLabel(new GroundstationLabel(TEST_NAME, TEST_VALUE));
        tc.addLabel(new GroundstationLabel("B", "30"));
        Map<String, Object> params = tc.getArguments();
        assertEquals(2, params.size());

        Label parToGet = null;
        parToGet = (Label) params.get(TEST_NAME);
        assertEquals(TEST_VALUE, parToGet.getValue());

        parToGet = (Label) params.get("B");
        assertEquals("30", parToGet.getValue());
    }
}
