package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class ConvertPower2MW implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand command) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+2 ");
        messageString.append(command.getParameter("Power"));
        messageString.append(" ");
        messageString.append(command.getParameter("Frequency"));
        messageString.append(" ");
        messageString.append(command.getParameter("Mode"));
        messageString.append("\n");
        return messageString;
    }
}
