package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SetConfig implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand command) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+C ");
        messageString.append(command.getParameter("Token"));
        messageString.append(" ");
        messageString.append(command.getParameter("Value"));
        messageString.append("\n");
        return messageString;
    }
}
