package eu.estcube.gs.hamlib;

import java.util.Date;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import eu.estcube.domain.GroundstationLabel;
import eu.estcube.domain.TelemetryObject;
import eu.estcube.gs.config.DriverConfiguration;

public class DecoderStringToTelemetryObject extends OneToOneDecoder {

    private static final Logger LOG = LoggerFactory.getLogger(DecoderStringToTelemetryObject.class);

    @Value("#{config}")
    private DriverConfiguration config;

    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, Object message) {
        // creates a TelemetryObject out of a string

        String[] messageSplit = message.toString().split("\n");
        TelemetryObject telemetryObject;
        LOG.debug("Message: {}", message.toString());

        String[] name = messageSplit[0].split(":");
        telemetryObject = new TelemetryObject(name[0], new Date());
        addParametersToTelemetryObject(messageSplit, telemetryObject);

        telemetryObject.setSource(config.getGroundstationId());
        telemetryObject.setDevice(config.getDeviceType());
        return telemetryObject;
    }

    private void addParametersToTelemetryObject(String[] messageSplit, TelemetryObject telemetryObject) {
        String[] messagePiece = { null, null };

        for (int i = 0; i < messageSplit.length; i++) {
            messageSplit[i] = messageSplit[i].replace(":\t\t", ":");
            messageSplit[i] = messageSplit[i].replace(":\t", ":");
            messageSplit[i] = messageSplit[i].replace(": ", ":");

            if (messageSplit[i].contains(":")) {
                messagePiece = messageSplit[i].split(":");
            } else if (messageSplit[i].contains(" ")) {
                messagePiece = messageSplit[i].split(" ");
            } else {
                continue;
            }

            if (messagePiece.length == 1) {
                telemetryObject.addLabel(new GroundstationLabel(messagePiece[0]));
                continue;
            }

            telemetryObject.addLabel(new GroundstationLabel(messagePiece[0], messagePiece[1]));
        }
    }
}
