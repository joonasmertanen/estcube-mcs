package eu.estcube.webserver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import eu.estcube.domain.RadioBeacon;

public class RadioBeaconTranslatorTest {

    private RadioBeacon beacon1, beacon2, beacon3, beacon4, beacon5, beacon6, beacon7, beacon8, beacon9, beacon10,
        beacon11, beacon12, beacon13, beacon14;
    private RadioBeaconTranslator radioBeaconTranslator;
    private String correctNormalBeacon = "ES5E/SEAAAAAAABBCCDDEEFFGGGHIIJJKKLLMMNNOOK";
    private String correctSafeBeacon1 = "ES5E/STAAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSSKN";
    private String correctSafeBeacon2 = "ES5E/STAA AAAAABBCCD DEEeeFFGGHHIIJJKKLLMMNNOPQQR RSSKN   ";
    private String incorrectLenghtBeacon = "ES5AAAAAAAPRRRRKN";
    private String incorrectBasisBeacon = "QS5R/STAAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSSKN";
    private String nonASCIISafeBeacon = "ES5E/STAAAAAAABBCCDDEEEEFFGGHHáIJJKKLLMMNNOPQQRRSSKN"; //
    private String correctNormalBeaconMode1 = "ES5E/SEAAAAAAABBCCDDEEFFGGGHIIJJKKLLMMNNOOK";
    private String correctNormalBeaconMode2 = "ES5E/S#EAAAAAAABBCCDDEEFFGGGHIIJJKKLLMMNNOOK";
    private String correctNormalBeaconMode3 = "ES5E/SEAAAAAAABBCCDDEEFFGGGHIIJJKKLLMMNNOO#";
    private String correctNormalBeaconMode4 = "ES5E/S#AAAAAAABBCCDDEEFFGGGHIIJJKKLLMMNNOO#";
    private String correctSafeBeaconMode1 = "ES5E/STAAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSSKN";
    private String correctSafeBeaconMode2 = "ES5E/S#AAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSSKN";
    private String correctSafeBeaconMode3 = "ES5E/STAAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSS##";
    private String correctSafeBeaconMode4 = "ES5E/S#AAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSS##";

    private String hex1 = "2A";
    private String hex2 = "2A4B";
    private String hex3 = "QW";
    private String hex4 = "";

    private String cw1 = "5TASAZZZ";
    private String cw2 = "HTUD";
    private String cw3 = "";

    private String unixTimestamp1 = "1352902792";

    @Before
    public void setUp() throws Exception {
        radioBeaconTranslator = new RadioBeaconTranslator();
        beacon1 = new RadioBeacon();
        beacon2 = new RadioBeacon();
        beacon3 = new RadioBeacon();
        beacon4 = new RadioBeacon();
        beacon5 = new RadioBeacon();
        beacon6 = new RadioBeacon();
        beacon7 = new RadioBeacon();
        beacon8 = new RadioBeacon();
        beacon9 = new RadioBeacon();
        beacon10 = new RadioBeacon();
        beacon11 = new RadioBeacon();
        beacon12 = new RadioBeacon();
        beacon13 = new RadioBeacon();
        beacon14 = new RadioBeacon();

        beacon1.setRadioBeacon(correctNormalBeacon);
        beacon2.setRadioBeacon(correctSafeBeacon1);
        beacon3.setRadioBeacon(incorrectLenghtBeacon);
        beacon4.setRadioBeacon(correctSafeBeacon2);
        beacon5.setRadioBeacon(incorrectBasisBeacon);
        beacon6.setRadioBeacon(correctNormalBeaconMode1);
        beacon7.setRadioBeacon(correctNormalBeaconMode2);
        beacon8.setRadioBeacon(correctNormalBeaconMode3);
        beacon9.setRadioBeacon(correctNormalBeaconMode4);
        beacon10.setRadioBeacon(correctSafeBeaconMode1);
        beacon11.setRadioBeacon(correctSafeBeaconMode2);
        beacon12.setRadioBeacon(correctSafeBeaconMode3);
        beacon13.setRadioBeacon(correctSafeBeaconMode4);
        beacon14.setRadioBeacon(nonASCIISafeBeacon);

    }

    @Test
    public void checkBeaconLengthTest() throws Exception {
        assertTrue(radioBeaconTranslator.checkBeaconLength(beacon1.getRadioBeacon()));
        assertTrue(radioBeaconTranslator.checkBeaconLength(beacon2.getRadioBeacon()));
        assertFalse(radioBeaconTranslator.checkBeaconLength(beacon3.getRadioBeacon()));
    }

    @Test
    public void determineRadioBeaconModeTest() throws Exception {
        assertEquals("Normal mode", radioBeaconTranslator.determineRadioBeaconMode(beacon6.getRadioBeacon()));
        assertEquals("Normal mode", radioBeaconTranslator.determineRadioBeaconMode(beacon7.getRadioBeacon()));
        assertEquals("Normal mode", radioBeaconTranslator.determineRadioBeaconMode(beacon8.getRadioBeacon()));
        assertEquals("Undefined mode", radioBeaconTranslator.determineRadioBeaconMode(beacon9.getRadioBeacon()));
        assertEquals("Safe mode", radioBeaconTranslator.determineRadioBeaconMode(beacon10.getRadioBeacon()));
        assertEquals("Safe mode", radioBeaconTranslator.determineRadioBeaconMode(beacon11.getRadioBeacon()));
        assertEquals("Safe mode", radioBeaconTranslator.determineRadioBeaconMode(beacon12.getRadioBeacon()));
        assertEquals("Undefined mode", radioBeaconTranslator.determineRadioBeaconMode(beacon13.getRadioBeacon()));
    }

    @Test
    public void reformBeaconMessageTest() throws Exception {
        assertEquals(correctSafeBeacon1, radioBeaconTranslator.reformBeaconMessage(beacon4.getRadioBeacon()));
    }

    @Test
    public void checkRadioBeaconBasisTest() throws Exception {
        assertTrue(radioBeaconTranslator.checkRadioBeaconBasis(beacon1.getRadioBeacon()));
        assertTrue(radioBeaconTranslator.checkRadioBeaconBasis(beacon2.getRadioBeacon()));
        assertFalse(radioBeaconTranslator.checkRadioBeaconBasis(beacon5.getRadioBeacon()));
    }

    @Test
    public void checkASCIIMessageTest() throws Exception {
        assertTrue(radioBeaconTranslator.checkASCIIMessage(beacon2.getRadioBeacon()));
        assertFalse(radioBeaconTranslator.checkASCIIMessage(beacon14.getRadioBeacon()));
    }

    @Test
    public void parseHexTest() throws Exception {
        assertEquals(radioBeaconTranslator.parseHex(hex1), "42");
        assertEquals(radioBeaconTranslator.parseHex(hex2), "10827");
        assertEquals(radioBeaconTranslator.parseHex(hex3), "0");
        assertEquals(radioBeaconTranslator.parseHex(hex4), "0");
    }

    @Test
    public void cwToHexTest() throws Exception {
        assertEquals(radioBeaconTranslator.cwToHex(cw1), "50A3A888");
        assertEquals(radioBeaconTranslator.cwToHex(cw2), "402D");
        assertEquals(radioBeaconTranslator.cwToHex(cw3), "");
    }

    @Test
    public void UnixTimestampToDateTest() throws Exception {
        assertEquals(radioBeaconTranslator.unixTimestampToDate(unixTimestamp1), "14.11.2012 16:19:52");
    }

}
