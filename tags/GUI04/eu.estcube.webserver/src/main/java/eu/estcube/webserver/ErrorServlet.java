package eu.estcube.webserver;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eu.estcube.crowd.CrowdLoginService;

public class ErrorServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getOutputStream().println("<!DOCTYPE html>");
        resp.getOutputStream().println("<html>");
        resp.getOutputStream().println("<head>");
        resp.getOutputStream().println("<meta charset=\"utf-8\"/>");
        resp.getOutputStream().println("<title>ESTCube</title>");
        resp.getOutputStream().println("<meta http-equiv=\"cache-control\" content=\"max-age=0\" />");
        resp.getOutputStream().println("<meta http-equiv=\"cache-control\" content=\"no-cache\" />");
        resp.getOutputStream().println("<meta http-equiv=\"expires\" content=\"0\" />");
        resp.getOutputStream().println("<meta http-equiv=\"expires\" content=\"Tue, 01 Jan 1980 1:00:00 GMT\" />");
        resp.getOutputStream().println("<meta http-equiv=\"pragma\" content=\"no-cache\" />");
        resp.getOutputStream().println("<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/css.css\" />");
        resp.getOutputStream()
            .println(
                "<link href=\"http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css\" rel=\"stylesheet\" type=\"text/css\"/>");
        resp.getOutputStream().println(
            "<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js\"></script>");
        resp.getOutputStream().println(
            "<script src=\"http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js\"></script>");
        resp.getOutputStream().println("<script type=\"text/javascript\">");
        resp.getOutputStream().println("$(document).ready( function(){");
        resp.getOutputStream().println("$(\"#helpButton\").click(showDialog);");
        resp.getOutputStream().println("helpDialog = $('#helpDialog');");
        resp.getOutputStream().println("helpDialog.dialog({ ");
        resp.getOutputStream().println("height: 200,");
        resp.getOutputStream().println("width: 300,");
        resp.getOutputStream().println("modal: true,");
        resp.getOutputStream().println("position: 'center',");
        resp.getOutputStream().println("autoOpen: false,");
        resp.getOutputStream().println("resizable: false,");
        resp.getOutputStream().println("title:'Information',");
        resp.getOutputStream().println("overlay: {");
        resp.getOutputStream().println("opacity: 0.5,");
        resp.getOutputStream().println("background: 'black'");
        resp.getOutputStream().println("}");
        resp.getOutputStream().println("});");
        resp.getOutputStream().println("}");
        resp.getOutputStream().println(");");
        resp.getOutputStream().println("var showDialog = function() {");
        resp.getOutputStream().println("helpDialog.show(); ");
        resp.getOutputStream().println("helpDialog.dialog(\"open\");");
        resp.getOutputStream().println("}");
        resp.getOutputStream().println("var closeDialog = function() {");
        resp.getOutputStream().println("helpDialog.dialog(\"close\");");
        resp.getOutputStream().println("}");
        resp.getOutputStream().println("</script>");
        resp.getOutputStream().println("</head>");

        resp.getOutputStream().println("<body class=\"firstPage\">");
        resp.getOutputStream().println("<form action=\"/j_security_check\" method=\"POST\">");
        resp.getOutputStream().println("<br />");
        resp.getOutputStream().println("<br />");
        resp.getOutputStream().println("<br />");
        resp.getOutputStream()
            .println(
                "<a href=\"http://www.estcube.eu\" target=\"_blank\"><img src=\"/images/customLogo.png\" alt=\"ESTCube logo\"></a>");
        resp.getOutputStream().println("<br />");
        if (CrowdLoginService.getException().contains("XFireRuntimeException")) {
            resp.getOutputStream().println(
                "<font color=\"red\">Crowd server error. Unable to connect crowd server</font>");
        } else {
            resp.getOutputStream().println("<font color=\"red\">Invalid username or password</font>");
        }
        resp.getOutputStream().println("<div id=\"spacer\"></div>");
        resp.getOutputStream().println("Username: <br /> ");
        resp.getOutputStream().println("<input type=\"text\" class=\"text-box\" size=\"25\" name=\"j_username\" />");
        resp.getOutputStream().println("<div id=\"spacer\"></div>");
        resp.getOutputStream().println("Password: <br /> ");
        resp.getOutputStream()
            .println("<input type=\"password\" class=\"text-box\" size=\"25\" name=\"j_password\" />");
        resp.getOutputStream().println("<div>");
        resp.getOutputStream().println("<br />");
        resp.getOutputStream().println(
            "<input id=\"loginButton\" type=\"submit\" class=\"button blue\" value=\"Login\" />");
        resp.getOutputStream().println(
            "<input id=\"helpButton\" type=\"button\" class=\"button blue\" value=\"Help?\" />");
        resp.getOutputStream().println("</div>");
        resp.getOutputStream().println("<div id=\"helpDialog\" style=\"display: none\">");
        resp.getOutputStream().println("<p>");
        resp.getOutputStream().println("Insert username and password.<br />");
        resp.getOutputStream().println("For additional information please contact system administrator.");
        resp.getOutputStream().println("</p>");
        resp.getOutputStream().println("</div>");
        resp.getOutputStream().println("</form>");
        resp.getOutputStream().println("</body>");
        resp.getOutputStream().println("</html>");
    }
}
