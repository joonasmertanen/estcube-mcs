package eu.estcube.webserver;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.ListUtils;

import eu.estcube.domain.JMSConstants;
import eu.estcube.domain.RadioBeacon;

public class RadioBeaconTranslator {

    public String determineRadioBeaconMode(String message) {
        String messageModeID = message.substring(6, 7);
        if (messageModeID.equals("E")) {
            return "Normal mode";
        } else if (messageModeID.equals("T")) {
            return "Safe mode";
        } else {
            String messageEndCharacter = message.substring(message.length() - 2, message.length());
            if (messageEndCharacter.equals("KN")) {
                return "Safe mode";
            } else {
                messageEndCharacter = messageEndCharacter.substring(messageEndCharacter.length() - 1);
                if (messageEndCharacter.equals("K")) {
                    return "Normal mode";
                }
                return "Undefined mode";
            }
        }
    }

    public String reformBeaconMessage(String message) {
        String newMessage = message.replaceAll("\\s", "");
        newMessage = newMessage.toUpperCase();
        return newMessage;
    }

    public boolean checkRadioBeaconBasis(String message) {
        String messageBasis = message.substring(0, 6);
        if (messageBasis.equals("ES5E/S")) {
            return true;
        }
        return false;
    }

    public boolean checkBeaconLength(String message) {
        int messageLength = message.length();
        if (messageLength == JMSConstants.NORMAL_BEACON_MESSAGE_LENGTH
            || messageLength == JMSConstants.SAFE_BEACON_MESSAGE_LENGTH) {
            return true;
        }
        return false;
    }

    public boolean checkBeaconValidity(String message) {
        if ((determineRadioBeaconMode(message).equals("Normal mode") || determineRadioBeaconMode(message).equals(
            "Safe mode"))
            && checkRadioBeaconBasis(message)) {
            return true;
        }
        return false;
    }

    public boolean checkASCIIMessage(String message) {
        if (message.matches("\\A\\p{ASCII}*\\z")) {
            return true;
        }
        return false;
    }

    public boolean checkBeaconMessage(String message) {
        if (checkBeaconLength(message) && checkASCIIMessage(message)) {
            if (checkBeaconValidity(message)) {
                return true;
            }
            return false;
        }
        return false;
    }

    public ArrayList<String> splitMessageBeginning(String message) {
        ArrayList<String> firstThreeSymbolsList = new ArrayList<String>();
        firstThreeSymbolsList.add(message.substring(0, 6));
        firstThreeSymbolsList.add(message.substring(6, 7));
        firstThreeSymbolsList.add(message.substring(7, 14));
        return firstThreeSymbolsList;
    }

    @SuppressWarnings("unchecked")
    public List<String> concatMessageLists(List<String> firstThreeParamatersList, List<String> messageBodyList) {
        return (List<String>) ListUtils.union(firstThreeParamatersList, messageBodyList);
    }

    protected List<String> splitMessageIntoParamaters(String message) {
        List<String> wholeMessageList = new ArrayList<String>();
        List<String> firstThreeParametersList = splitMessageBeginning(message);
        String beaconMode = determineRadioBeaconMode(message);
        // substring in order to operate with remaining parameters
        message = message.substring(14);
        if (!(beaconMode.equals("Undefined mode"))) {
            if (beaconMode.equals("Normal mode")) {
                List<String> messageBodyList = new ArrayList<String>();
                int metric = 0;
                int sub = 0;
                int messageLenght = message.length();
                while (metric < messageLenght) {
                    if (metric == 10) {
                        sub = 3;
                    } else if (metric == 13 || metric == 28) {
                        sub = 1;
                    } else {
                        sub = 2;
                    }
                    messageBodyList.add(message.substring(0, sub));
                    message = message.substring(sub);
                    metric += sub;
                }
                wholeMessageList = concatMessageLists(firstThreeParametersList, messageBodyList);
            } else {
                List<String> messageBodyList = new ArrayList<String>();
                int metric = 0;
                int sub = 0;
                int messageLenght = message.length();
                while (metric < messageLenght) {
                    if (metric == 6) {
                        sub = 4;
                    } else {
                        sub = 2;
                    }
                    messageBodyList.add(message.substring(0, sub));
                    message = message.substring(sub);
                    metric += sub;
                }
                wholeMessageList = concatMessageLists(firstThreeParametersList, messageBodyList);
            }
        }
        return wholeMessageList;
    }

    protected HashMap<String, String> toParameters(String message) throws Exception {
        List<String> splittedMessage = splitMessageIntoParamaters(message);
        HashMap<String, String> parameters = new HashMap<String, String>();
        String beaconMode = determineRadioBeaconMode(message);
        // TODO: beacon translation magic
        // idea for translating - translating depends on beacon mode
        // for each parameter from splittedMessage except mode (second and last)
        // parse to hex string using cwToHex method
        // parse to int value using parseHex method
        // translate each parameter value using business logic (not determined)
        // put value as string into parameter map
        if (!(beaconMode.equals("Undefined mode"))) {
            if (beaconMode.equals("Normal mode")) {
                parameters.put("EPStimestamp", unixTimestampToDate(parseHex(cwToHex(splittedMessage.get(1)))));
                parameters.put("beaconMode", beaconMode);
            } else {
                parameters.put("EPStimestamp", unixTimestampToDate(parseHex(cwToHex(splittedMessage.get(1)))));
                parameters.put("beaconMode", beaconMode);
            }
        }
        return parameters;
    }

    public String cwToHex(String message) {
        Map<String, String> map = new HashMap<String, String>();
        map = getRules();
        StringBuilder result = new StringBuilder();
        for (char c : message.toCharArray()) {
            result.append(map.get(c + ""));
        }
        return result.toString();
    }

    public Map<String, String> getRules() {
        Map<String, String> rulesMap = new HashMap<String, String>();
        rulesMap.put("T", "0");
        rulesMap.put("W", "1");
        rulesMap.put("U", "2");
        rulesMap.put("S", "3");
        rulesMap.put("H", "4");
        rulesMap.put("5", "5");
        rulesMap.put("6", "6");
        rulesMap.put("M", "7");
        rulesMap.put("Z", "8");
        rulesMap.put("N", "9");
        rulesMap.put("A", "A");
        rulesMap.put("B", "B");
        rulesMap.put("C", "C");
        rulesMap.put("D", "D");
        rulesMap.put("E", "E");
        rulesMap.put("F", "E");
        return rulesMap;
    }

    public String parseHex(String hex) {
        try {
            return Integer.toString(Integer.parseInt(hex, JMSConstants.HEX_BASE));
        } catch (NumberFormatException e) {
            return "0";
        }
    }

    public RadioBeacon translateMessage(String message) throws Exception {
        RadioBeacon translatedRadioBeacon = new RadioBeacon();
        HashMap<String, String> parameters = toParameters(message);
        translatedRadioBeacon.setParameters(parameters);
        return translatedRadioBeacon;
    }

    public String unixTimestampToDate(String date) {
        Date d = new Date();
        d.setTime((long) 1352902792 * 1000);
        Format formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        String result = formatter.format(d);
        System.out.println(result);
        return result;
    }

}
