package eu.estcube.webserver;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.websocket.WebsocketComponent;
import org.apache.camel.component.websocket.WebsocketConstants;
import org.apache.camel.spring.Main;
import org.eclipse.jetty.http.security.Constraint;
import org.eclipse.jetty.security.Authenticator;
import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.LoginService;
import org.eclipse.jetty.security.authentication.FormAuthenticator;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.SessionManager;
import org.eclipse.jetty.server.session.HashSessionManager;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.hbird.business.heartbeat.Heart;
import org.hbird.exchange.core.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.atlassian.crowd.service.soap.client.SecurityServerClientFactory;

import eu.estcube.crowd.CrowdLoginService;
import eu.estcube.crowd.LogOutServlet;
import eu.estcube.domain.JMSConstants;
import eu.estcube.domain.TelemetryCommand;

public class WebServer extends RouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(WebServer.class);
    private final int heartBeatInterval = 3000;

    @Value("${componentName}")
    private String componentName;

    @Value("${webSocketAdress}")
    private String webSocketAdress;

    @Value("${webSocketCacheReqAdress}")
    private String webSocketCacheReqAdress;

    @Value("${webSocketVersionsAdress}")
    private String webSocketVersionsAdress;

    @Value("${webSocketForLog}")
    private String webSocketForLog;

    @Value("${webSocketForSystem}")
    private String webSocketForSystem;

    @Value("${webSocketForMap}")
    private String webSocketForMap;

    @Value("${webSocketPort}")
    private int webSocketPort;

    @Value("${staticResources}")
    private String staticResources;

    @Value("${maxObjectsCached}")
    private int cacheLimit;

    @Value("${version}")
    private String webServerVersion;

    @Value("${heartbeatIntervalLink}")
    private String heartbeatIntervalLink;

    @Autowired
    private JsonToTelemetryCommand jsonToTelemetryCommand;

    @Autowired
    private InputStreamToBase64 inputStreamToBase64;

    @Autowired
    private VersionInfoToJson versionInfoToJson;

    @Autowired
    private TelemetryObjectToJson telemetryObjectToJson;

    @Autowired
    private ToJsonProcessor toJsonProcessor;

    @Autowired
    private NamedCache systemCache;

    private ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
    private HashMap<String, String> versions = new HashMap<String, String>();
    private Heart heart;

    @Override
    public void configure() throws Exception {
        versions.put(JMSConstants.COMPONENT_WEBSERVER, webServerVersion);

        CacheMessage.setCacheLimit(cacheLimit);
        LOG.debug("Max number of cached objects: " + cacheLimit);
        /* //- This server prevents .js files from being cached. */
        InetSocketAddress address = new InetSocketAddress("0.0.0.0", 9292);
        Server server = new Server(address);

        context.setContextPath("/");

        SessionManager sm = new HashSessionManager();
        SessionHandler sh = new SessionHandler(sm);
        context.setSessionHandler(sh);

        context.setResourceBase("../eu.estcube.webgui/src/public/");
        DefaultServlet defaultServlet = new DefaultServlet();
        ServletHolder holder = new ServletHolder(defaultServlet);

        holder.setInitParameter("useFileMappedBuffer", "false");
        holder.setInitParameter("cacheControl", "no-store,no-cache,must-revalidate");
        holder.setInitParameter("maxCachedFiles", "0");
        context.addServlet(holder, "/");
        context.addServlet(LogOutServlet.class, "/logout");
        context.addServlet(UserInfoServlet.class, "/user");
        context.addServlet(ErrorServlet.class, "/error");
        context.addServlet(RadioBeaconServlet.class, "/radioBeacon");
        context.addServlet(RadioBeaconTranslationServlet.class, "/translateRadioBeacon");

        LoginService loginService = new CrowdLoginService(SecurityServerClientFactory.getSecurityServerClient());
        server.addBean(loginService);

        ConstraintSecurityHandler security = new ConstraintSecurityHandler();
        server.setHandler(security);

        Constraint constraint = new Constraint();
        constraint.setName("auth");
        constraint.setAuthenticate(true);
        constraint.setRoles(new String[] { "mcs-premium-admin", "mcs-premium-op", "mcs-op" });

        ConstraintMapping mapping = new ConstraintMapping();
        mapping.setPathSpec("/MCS/*");
        mapping.setConstraint(constraint);

        ConstraintMapping userInfoMapping = new ConstraintMapping();
        userInfoMapping.setPathSpec("/user");
        userInfoMapping.setConstraint(constraint);

        ConstraintMapping radioBeaconMapping = new ConstraintMapping();
        radioBeaconMapping.setPathSpec("/radioBeacon");
        radioBeaconMapping.setConstraint(constraint);

        ConstraintMapping radioBeaconTranslationMapping = new ConstraintMapping();
        radioBeaconTranslationMapping.setPathSpec("/translateRadioBeacon");
        radioBeaconMapping.setConstraint(constraint);

        List<ConstraintMapping> constraintMappings = new ArrayList<ConstraintMapping>();
        constraintMappings.add(mapping);
        constraintMappings.add(userInfoMapping);

        Set<String> knownRoles = new HashSet<String>();
        knownRoles.add("mcs-premium-admin");
        knownRoles.add("mcs-premium-op");
        knownRoles.add("mcs-op");

        security.setConstraintMappings(constraintMappings, knownRoles);
        /*
         * FormAuthentication gets the parameters from the page entered as first
         * argument (login.html)
         * In case of login failure, returns to the page entered in the second
         * argument (login_error.html)
         */
        Authenticator authenticator = new FormAuthenticator("/MCS/login.html", "/error", true);
        security.setAuthenticator(authenticator);
        security.setLoginService(loginService);
        security.setStrict(false);

        context.setHandler(security);

        server.setHandler(context);
        server.start();

        WebsocketComponent websocketComponent = (WebsocketComponent) getContext().getComponent("websocket");

        websocketComponent.setPort(webSocketPort);
        websocketComponent.setStaticResources(staticResources);

        Endpoint websocket = websocketComponent.createEndpoint(webSocketAdress);
        Endpoint websocketCacheRequest = websocketComponent.createEndpoint(webSocketCacheReqAdress);
        Endpoint websocketForLog = websocketComponent.createEndpoint(webSocketForLog);
        Endpoint websocketForSystem = websocketComponent.createEndpoint(webSocketForSystem);
        Endpoint websocketForMap = websocketComponent.createEndpoint(webSocketForMap);

        from("timer://status?repeatCount=1").to(JMSConstants.AMQ_PS_QUEUE);
        from("timer://versionsRequest?repeatCount=1").to(JMSConstants.AMQ_VERSIONS_REQUEST);

        from(JMSConstants.AMQ_VERSIONS_RECEIVE)
            .process(new Processor() {
                public void process(Exchange ex) throws Exception {
                    String componentId = "";
                    try {
                        componentId = ex.getIn().getHeader(JMSConstants.HEADER_COMPONENT_ID, String.class);
                    } catch (Exception e) {
                    }
                    versions.put(ex.getIn().getHeader(JMSConstants.HEADER_COMPONENT, String.class) + "-" + componentId,
                        ex.getIn().getBody(String.class));
                }
            }).process(new Processor() {
                public void process(Exchange ex) throws Exception {
                    ex.getOut().setBody(versions);
                }
            }).process(versionInfoToJson).setHeader(WebsocketConstants.SEND_TO_ALL, constant(true))
            .to(webSocketVersionsAdress);

        from(JMSConstants.AMQ_WEBSERVER_FOR_LOG).bean(toJsonProcessor)
            .setHeader(WebsocketConstants.SEND_TO_ALL, constant(true)).to(websocketForLog);
        from(JMSConstants.AMQ_WEBSERVER_FOR_LOG).bean(toJsonProcessor)
            .setHeader(WebsocketConstants.SEND_TO_ALL, constant(true)).to(websocketForLog);

        from(JMSConstants.AMQ_WEBSERVER_FOR_MAP).bean(toJsonProcessor)
            .setHeader(WebsocketConstants.SEND_TO_ALL, constant(true)).to(websocketForMap);

        from(JMSConstants.AMQ_WEBSERVER_FOR_SYSTEM).choice()
            // cache only Named objects
            .when(body().isInstanceOf(Named.class)).bean(systemCache, "updateCache").end().bean(toJsonProcessor)
            .setHeader(WebsocketConstants.SEND_TO_ALL, constant(true)).to(websocketForSystem);

        from(webSocketVersionsAdress).process(new Processor() {
            public void process(Exchange ex) throws Exception {
                ex.getOut().setBody(versions);
            }
        }).process(versionInfoToJson).setHeader(WebsocketConstants.SEND_TO_ALL, constant(true))
            .to(webSocketVersionsAdress);

        from(websocketCacheRequest).bean(CacheMessage.class, "getCache").split()
            .method("telemetryObjectSplitter", "splitMessage").process(telemetryObjectToJson)
            .setHeader(WebsocketConstants.SEND_TO_ALL, constant(true)).to(websocketCacheRequest);

        from(websocket).process(jsonToTelemetryCommand).process(new Processor() {
            public void process(Exchange exchange) throws Exception {
                exchange.getIn().setHeader("groundStationID", "ES5EC");
                exchange.getIn().setHeader("device", exchange.getIn().getBody(TelemetryCommand.class).getDevice());
            }
        }).to(JMSConstants.AMQ_GS_RECEIVE);

        from(websocketForLog).bean(toJsonProcessor).setHeader(WebsocketConstants.SEND_TO_ALL, constant(true))
            .to(websocketForLog);

        from(websocketForSystem).transform(body().append("")).bean(systemCache, "getCache").split()
            .method(Splitter.class, "doSplit").bean(toJsonProcessor).to(websocketForSystem);

        from(JMSConstants.AMQ_GS_SEND).bean(CacheMessage.class, "addToCache").process(telemetryObjectToJson)
            .setHeader(WebsocketConstants.SEND_TO_ALL, constant(true)).to(websocket);

        from(JMSConstants.AMQ_WEBCAM_SEND).process(inputStreamToBase64)
            .setHeader(WebsocketConstants.SEND_TO_ALL, constant(true)).to("websocket://webcam");

        heart = new Heart(componentName, heartBeatInterval);
        from(heartbeatIntervalLink + heartBeatInterval).bean(heart).to(JMSConstants.AMQ_WEBSERVER_FOR_SYSTEM);
    }

    public static void main(String[] args) throws Exception {
        LOG.info("Starting WebServer");
        try {
            new Main().run(args);
        } catch (Exception e) {
            LOG.error("Failed to start " + WebServer.class.getName(), e);
        }
    }
}
