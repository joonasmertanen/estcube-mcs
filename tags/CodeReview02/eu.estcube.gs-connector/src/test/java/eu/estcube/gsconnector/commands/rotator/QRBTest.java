package eu.estcube.gsconnector.commands.rotator;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRotatorConstants;

public class QRBTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private QRB createMessage = new QRB();
    
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.QRB);
        telemetryCommand.addParameter("Lon 1", 0.0);
        telemetryCommand.addParameter("Lat 1", 0.0);
        telemetryCommand.addParameter("Lon 2", 0.0);
        telemetryCommand.addParameter("Lat 2", 0.0);
        string.append("+B 0.0 0.0 0.0 0.0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
