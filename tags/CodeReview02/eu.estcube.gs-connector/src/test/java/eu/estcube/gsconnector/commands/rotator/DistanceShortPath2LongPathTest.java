package eu.estcube.gsconnector.commands.rotator;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRotatorConstants;

public class DistanceShortPath2LongPathTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private DistanceShortPath2LongPath createMessage = new DistanceShortPath2LongPath();
    
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.DISTANCE_SHORTPATH2LONGPATH);
        telemetryCommand.addParameter("Short Path km", 0.0);
        string.append("+a 0.0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
        
        string = new StringBuilder();
        telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.DISTANCE_SHORTPATH2LONGPATH);
        string.append("+a null\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
        
    }

}
