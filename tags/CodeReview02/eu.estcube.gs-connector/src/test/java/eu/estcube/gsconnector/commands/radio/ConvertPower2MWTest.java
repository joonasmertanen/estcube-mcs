package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class ConvertPower2MWTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private ConvertPower2MW createMessage = new ConvertPower2MW();
    
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand(TelemetryRadioConstants.CONVERT_POWER2MW);
        telemetryCommand.addParameter("Power", 0.0);
        telemetryCommand.addParameter("Frequency", 0.0);
        telemetryCommand.addParameter("Mode", "..");
        string.append("+2 0.0 0.0 ..\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
