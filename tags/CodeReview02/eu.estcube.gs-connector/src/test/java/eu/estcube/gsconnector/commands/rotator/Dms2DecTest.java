package eu.estcube.gsconnector.commands.rotator;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRotatorConstants;

public class Dms2DecTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private Dms2Dec createMessage = new Dms2Dec();
    
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.DMS2DEC);
        telemetryCommand.addParameter("Degrees", 0);
        telemetryCommand.addParameter("Minutes", 0);
        telemetryCommand.addParameter("Seconds", 0.0);
        telemetryCommand.addParameter("S/W", 0);
        string.append("+D 0 0 0.0 0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
