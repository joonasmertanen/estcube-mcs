package eu.estcube.gsconnector.commands;

import eu.estcube.domain.TelemetryCommand;

public class SendRawCmd implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+w ");
        messageString.append(telemetryCommand.getParams().get("Cmd"));
        messageString.append("\n");
        return messageString;
    }
}
