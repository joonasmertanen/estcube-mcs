package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class Dec2Dms implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+d ");
        messageString.append(telemetryCommand.getParams().get("Dec Degrees"));
        messageString.append("\n");
        return messageString;
    }
}
