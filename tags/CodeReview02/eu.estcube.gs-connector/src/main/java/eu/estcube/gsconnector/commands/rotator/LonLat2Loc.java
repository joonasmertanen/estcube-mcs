package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class LonLat2Loc implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+L ");
        messageString.append(telemetryCommand.getParams().get("Longitude"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Latitude"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Loc Len"));
        messageString.append("\n");
        return messageString;
    }
}
