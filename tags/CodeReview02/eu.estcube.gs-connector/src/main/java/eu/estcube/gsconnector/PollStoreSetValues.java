package eu.estcube.gsconnector;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Exchange;
import org.springframework.stereotype.Component;

import eu.estcube.domain.JMSConstants;
import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.domain.TelemetryRotatorConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;
import eu.estcube.gsconnector.commands.radio.SetAntennaNr;
import eu.estcube.gsconnector.commands.radio.SetCTCSSTone;
import eu.estcube.gsconnector.commands.radio.SetDCSCode;
import eu.estcube.gsconnector.commands.radio.SetFrequency;
import eu.estcube.gsconnector.commands.radio.SetMemoryChannelNr;
import eu.estcube.gsconnector.commands.radio.SetMode;
import eu.estcube.gsconnector.commands.radio.SetPTT;
import eu.estcube.gsconnector.commands.radio.SetRIT;
import eu.estcube.gsconnector.commands.radio.SetRprtShift;
import eu.estcube.gsconnector.commands.radio.SetRptrOffset;
import eu.estcube.gsconnector.commands.radio.SetSplitVFO;
import eu.estcube.gsconnector.commands.radio.SetTranceiveMode;
import eu.estcube.gsconnector.commands.radio.SetTransmitFrequency;
import eu.estcube.gsconnector.commands.radio.SetTransmitMode;
import eu.estcube.gsconnector.commands.radio.SetTuningStep;
import eu.estcube.gsconnector.commands.radio.SetVFO;
import eu.estcube.gsconnector.commands.rotator.SetPosition;

@Component("storeSetValues")
public final class PollStoreSetValues {
    private static Map<String, TelemetryCommand> requiredParameterValueDataStore;

    public static Map<String, TelemetryCommand> getRequiredParameterValueDataStore() {
        return requiredParameterValueDataStore;
    }

    public static void setRequiredParameterValueDataStore(Map<String, TelemetryCommand> requiredParameterValueDataStore) {
        PollStoreSetValues.requiredParameterValueDataStore = requiredParameterValueDataStore;
    }

    private Map<String, CommandStringBuilder> commandsHashMap;

    public PollStoreSetValues() {
        requiredParameterValueDataStore = new HashMap<String, TelemetryCommand>();
        commandsHashMap = new HashMap<String, CommandStringBuilder>();
        commandsHashMap.put(TelemetryRotatorConstants.SET_POSITION, new SetPosition());
        commandsHashMap.put(TelemetryRadioConstants.SET_ANTENNA_NR, new SetAntennaNr());
        commandsHashMap.put(TelemetryRadioConstants.SET_CTCSS_TONE, new SetCTCSSTone());
        commandsHashMap.put(TelemetryRadioConstants.SET_DCS_CODE, new SetDCSCode());
        commandsHashMap.put(TelemetryRadioConstants.SET_FREQUENCY, new SetFrequency());
        commandsHashMap.put(TelemetryRadioConstants.SET_MEMORY_CHANNELNR, new SetMemoryChannelNr());
        commandsHashMap.put(TelemetryRadioConstants.SET_MODE, new SetMode());
        commandsHashMap.put(TelemetryRadioConstants.SET_PTT, new SetPTT());
        commandsHashMap.put(TelemetryRadioConstants.SET_RIT, new SetRIT());
        commandsHashMap.put(TelemetryRadioConstants.SET_RPTR_OFFSET, new SetRptrOffset());
        commandsHashMap.put(TelemetryRadioConstants.SET_RPTR_SHIFT, new SetRprtShift());
        commandsHashMap.put(TelemetryRadioConstants.SET_SPLIT_VFO, new SetSplitVFO());
        commandsHashMap.put(TelemetryRadioConstants.SET_TRANCEIVE_MODE, new SetTranceiveMode());
        commandsHashMap.put(TelemetryRadioConstants.SET_TRANSMIT_FREQUENCY, new SetTransmitFrequency());
        commandsHashMap.put(TelemetryRadioConstants.SET_TRANSMIT_MODE, new SetTransmitMode());
        commandsHashMap.put(TelemetryRadioConstants.SET_TUNING_STEP, new SetTuningStep());
        commandsHashMap.put(TelemetryRadioConstants.SET_VFO, new SetVFO());
    }

    public void storeData(Exchange exchange) {
        TelemetryCommand command = null;
        try {
            command = (TelemetryCommand) exchange.getIn().getBody();
        } catch (ClassCastException e) {
            exchange.getIn().setHeaders(null);
            exchange.getIn().setBody(null);
            return;
        }
        if (commandsHashMap.get(command.getName()) != null) {
            requiredParameterValueDataStore.put(command.getName(), command);
            exchange.getIn().setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_SET_COMMAND);
        }

        if (exchange.getIn().getHeader(JMSConstants.HEADER_POLLING) == null) {
            exchange.getIn().setHeader(JMSConstants.HEADER_POLLING, JMSConstants.POLL_NOT_REQUIRED);
        }
    }
}