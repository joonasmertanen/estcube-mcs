package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SetParm implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+P ");
        messageString.append(telemetryCommand.getParams().get("Parm"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Parm Value"));
        messageString.append("\n");
        return messageString;
    }
}
