dojo.provide("webgui.display.Graph");

dojo.require("dijit.layout.TabContainer");
dojo.require("dijit.layout.StackContainer");
dojo.require("dijit.layout.ContentPane");
dojo.require("dojox.charting.Chart");
dojo.require("dojox.charting.themes.Claro");
dojo.require("dojo.store.Observable");
dojo.require("dojo.store.Memory");
dojo.require("dojox.charting.StoreSeries");
dojo.require("dojox.charting.action2d.Tooltip");
dojo.require("dojox.charting.action2d.MoveSlice");


dojo.require("dojox.charting.widget.Legend");
dojo.require("dojox.charting.action2d.Tooltip");
dojo.require("dojox.charting.action2d.Magnify");
dojo.require("dojox.grid.DataGrid");
dojo.require("dojox.charting.DataChart");

dojo.require("dojo.parser");
dojo.require("dojox.data.PersevereStore");
dojo.require("dojox.cometd.RestChannels");
dojo.require("dojo.cookie");
dojo.require("dojo.date.locale");
dojo.require("dijit.form.Button");

dojo.require("dojox.charting.themes.PlotKit.red");

dojo.declare("webgui.display.Graph", null,{

    chart: null,
    count: 0,
    counter: 0,
    limit: 10,
    key: 1,
    storedata: null,
    store: null,
    historyLimit: 5,
    chartArea: null,
    legend: null,
        
    constructor: function(chartArea, subscriptionString)
    {
        this.chartArea = chartArea;
        this.storedata = { identifier: "key", items: [] };
        this.store = new dojo.data.ItemFileWriteStore({ data: this.storedata });
        this.chart = new dojox.charting.Chart(this.chartArea.id);
	    this.chart.setTheme(dojox.charting.themes.Claro);
        var getChart = function() {
            return chart;
        };
        this.chart.addPlot("default", {
            type: "Lines", // type of chart
            markers: true,
            lines: true,
            labelOffset: -30,
            shadows: { dx:2, dy:2, dw:2 }
        });
        this.chart.addAxis("x", {
            labelFunc: function(str, value){
            return dojo.date.locale.format(new Date(value), {datePattern: "HH:mm:ss", selector: "date"});
        },
            majorTickStep: 60000,
            majorLabels: true,
            minorTicks: false,
            minorLabels: true,
            microTicks: false
        });
        this.chart.addAxis("y", { vertical: true, min: 0, max: 90 });
        this.chart.resize(chartArea.getAttribute("width")-(chartArea.getAttribute("width")/20), chartArea.getAttribute("height")-(chartArea.getAttribute("height")/3));
        new dojox.charting.action2d.Tooltip(this.chart, "default");
        new dojox.charting.action2d.Magnify(this.chart, "default");

        dojo.connect(dijit.byId(chartArea.id), "resize", this, function(evt) {
            var dim = dijit.byId(chartArea.id)._contentBox;
            this.chart.resize(dim.w, dim.h);
        });


        // TODO not a good idea to update the view constantly
        dojo.connect(this.getStore(), "newItem", dojo.hitch(this, this.updateViewCallback));
        dojo.subscribe(subscriptionString, dojo.hitch(this, this.handleParameter));
        },

        getSeriesData: function(itemName) {
            var seriesExists = false;
            var seriesArrayData;
            dojo.forEach(this.chart.series, function(entry, key) {
                if (entry.name == itemName) {
                    seriesExists = true;
                    seriesArrayData = entry.data;
                }
            });
            if (!seriesExists) {
                return false;
            }
            if (seriesArrayData.length >= this.historyLimit) {
                seriesArrayData.splice(0, seriesArrayData.length - this.historyLimit);
            }
            //TODO opt should be changed
            //TODO labels always appear to be 1 step behind
            return seriesArrayData;
        },

        updateView: function() {
            this.chart.render();
            if (this.legend) {
                this.legend.destroy();
            }
            this.chartArea.appendChild(dojo.create("div", { id: "legend" }));
            this.legend = new dojox.charting.widget.Legend({ chart:this.chart }, "legend");
        },
        updateViewCallback: function(item) {
            var seriesArrayData = this.getSeriesData(item.Name);
            var jsdate = dojo.date.locale.parse(item["Timestamp"], {datePattern: "yyyy-DD HH:mm:ss.SSSZ", selector: "date"});
            var tooltipString = item.Name+"<br/>"+item.Value+"<br/>"+dojo.date.locale.format(jsdate, {datePattern: "yyyy-DD HH:mm:ss", selector: "date"});
            if (seriesArrayData === false) {
                this.chart.addSeries(item.Name, [{ x: jsdate, y: item["Value"], tooltip: tooltipString}]);
            } else {
                seriesArrayData.push({ x: jsdate, y: item["Value"], tooltip: tooltipString}); //, tooltip: "hello"
                this.chart.updateSeries(item.Name, seriesArrayData);
                this.chart.getAxis("x").opt.majorTickStep = (seriesArrayData[seriesArrayData.length-1].x.getTime() - seriesArrayData[0].x.getTime())/5;
                this.updateView();
            }
        },
        
        handleParameter: function (myObject) {

            //firstParameter
            for(var i=1; i<myObject.params.length-1; i++){
                var storeElem = {};
                storeElem.Value = myObject.params[i].value;
                storeElem.Name = myObject.params[i].name;
                storeElem.Timestamp = myObject.time;
                storeElem.key = this.key;
                this.store.newItem(storeElem);
                this.key++;
                this.counter++;
            }

            if (this.counter > this.limit) {
                // getting the size of the store
                var size = function(size, request) {
                    // remove excess elements
                    this.store.fetch({ count: (size - this.limit),
                        onItem: function(item) {
                        this.store.deleteItem(item);
                        }
                    });
                };
                console.log(this.store);
                this.store.fetch({ query: {}, onBegin: size, start: 0, count: 0 });
                }
            },

        getStore: function() {
         return this.store;
    }
        
    }
);