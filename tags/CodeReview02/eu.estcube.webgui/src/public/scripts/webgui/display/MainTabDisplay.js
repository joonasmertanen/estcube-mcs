dojo.provide("webgui.display.MainTabDisplay")
dojo.require("webgui.display.Graph");

dojo.require("dijit.layout.TabContainer");
dojo.require("dijit.layout.StackContainer");
dojo.require("dijit.layout.ContentPane");
dojo.require("dojox.charting.Chart");
dojo.require("dojox.charting.themes.Claro");
dojo.require("dojo.store.Observable");
dojo.require("dojo.store.Memory");
dojo.require("dojox.charting.StoreSeries");;
dojo.require("dojox.charting.action2d.MoveSlice");


dojo.require("dojox.charting.widget.Legend");
dojo.require("dojox.charting.action2d.Tooltip");
dojo.require("dojox.charting.action2d.Magnify");
dojo.require("dojox.grid.DataGrid");
dojo.require("dojox.charting.DataChart");

dojo.require("dojo.parser");
dojo.require("dojox.data.PersevereStore");
dojo.require("dojox.cometd.RestChannels");
dojo.require("dojo.cookie");
dojo.require("dojo.date.locale");

dojo.require("dojox.charting.themes.PlotKit.red");

dojo.declare("webgui.display.MainTabDisplay", null, {

	constructor : function(view) {

		console.log("TabDisplay");
		
		var telemetryTab = new dijit.layout.ContentPane({
			id: "chartTab",
			title : "Telemetry",
			preventCache: true
				
		});

		var gsTab = new dijit.layout.ContentPane({
			title : "GS",
			content : "Groundstation online!",
			closable : "true",
			preventCache: true
		});
		
		var commandTab = new dijit.layout.BorderContainer({
			id : "commandTab",
			title : "Command",
			preventCache: true
		});		
		
		var commandLogA = new dijit.layout.ContentPane({
			id : "commandLogA",
			title : "LogArea",
			region : "center",
			preventCache: true
		});	

		var commandA = new dijit.layout.ContentPane({
			id : "commandA",
			region : "bottom",
			title : "CmdArea",
			content: "Insert Commands:",
			preventCache: true
		});	
		
		commandTab.addChild(commandLogA);
		commandTab.addChild(commandA);
		

		var tabs = new dijit.layout.TabContainer({
			region : "center",
			tabPosition : "bottom",
			id : "mainTabContainer",
			preventCache: true
		});

		var tempCont;
		tempCont = dijit.byId("CenterContainer");

		tabs.addChild(telemetryTab);
		tabs.addChild(gsTab);
		tabs.addChild(commandTab);
		tempCont.addChild(tabs);
		tempCont.selectChild(tabs);
		tabs.startup();
		
		var chartArea = dojo.create("div", {
			id : "chartArea",
			region : "center",
			width: 600,
			height: 600
		}, "chartTab");	
		
        var chart = new webgui.display.Graph(chartArea, "GetPositionAzimuth");
        
		var cmdLogArea = dojo.create("div", {
			id : "commandLogArea2",
			region : "center",
			style : "width:100%; height:90; overflow:auto;"
		}, "commandLogA");
		
		var someData = [];
		var store3 = new dojo.data.ItemFileWriteStore({
			data : {
				items : someData
			}
		});

		var tableHeader = [{
			field: "name",
			name: "Telemetry Command",
			width: 11
		},{
			field:  "params",
			name: "Parameters",		
			width: '100%',	
			formatter: function(params, rowIndex) {
			    var item = this.grid.getItem(rowIndex);
			    return JSON.stringify(item.params);
			}
		}];
		
		var cmdLogArea = new dojox.grid.DataGrid({
			id: "commandLogArea",
			title: "Telemetry Commands",
			store: store3,
			width: '100%',
			structure: tableHeader
            }, "commandLogArea2");

		cmdLogArea.startup();
		cmdLogArea.canSort = false; 

		var cmdArea = dojo.create("div", {
			id : "commandArea",
			region : "center",
			style : "border: none"
		}, "commandA");
		
		var commandBox = new dijit.form.TextBox({
			id : "commandTextBox",
			value : "GET_POSITION"
		}).placeAt("commandArea");

		var buttonSend = new dijit.form.Button({
			label : "Send",
			onClick : function() {
				var muutuja = dijit.byId("commandTextBox");
				var split = muutuja.get('value').split(" ");

				var params = new Array();
				var dummyObject = {
        			name:  ''
			    };
				
				console.log(split.length % 2);
				
				if ((split.length - 1) % 2 == 0) {
					for (i = 1; i < split.length - 1; i = i + 2) {
						params.push(new TelemetryParameter(split[i],
								split[i + 1]));
					}
				}
// 
				var telemetryCommand = new TelemetryCommand(split[0], params);
				if (cmdLogArea.rowCount > 0){
					cmdLogArea.store.deleteItem(cmdLogArea.getItem(cmdLogArea.rowCount - 1));
				}
				store3.newItem(telemetryCommand);
				store3.newItem(dummyObject);
				
				dojo.publish("toWS", [JSON.stringify(telemetryCommand)]);
				cmdLogArea.scrollToRow(cmdLogArea.rowCount);
			}
		}).placeAt("commandArea");
		
		var buttonClear = new dijit.form.Button({
			label : "Clear",
			onClick : function() {
				cmdLogArea.store.fetch({
					onItem: function(item){
						cmdLogArea.store.deleteItem(item);
					}
				});
			}
		}).placeAt("commandArea");
		
		function TelemetryParameter(name, value) {
			this.name = name;
			this.value = value;
		}

		function TelemetryCommand(name, params) {
			this.name = name;
			this.params = params;
		}	

	}
});