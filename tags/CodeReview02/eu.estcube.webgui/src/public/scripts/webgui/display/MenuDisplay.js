dojo.provide("webgui.display.MenuDisplay");
dojo.require("dijit.MenuBar");
dojo.require("dijit.MenuBarItem");
dojo.require("dijit.PopupMenuBarItem");
dojo.require("dijit.Menu");
dojo.require("dijit.MenuItem");
dojo.require("dijit.layout.BorderContainer");
dojo.require("dijit.layout.ContentPane");
dojo.require("webgui.display.StatusesTableDisplay");
dojo.require("webgui.display.PictureDisplay");
dojo.require("webgui.display.InfoDisplay");
dojo.require("webgui.display.MainTabDisplay");
dojo.require("webgui.display.StatusesInfoDisplay");
dojo.require("webgui.display.StatusesGraphDisplay");
dojo.require("dijit.Dialog");

 
	
  	

dojo.declare("MenuDisplay", null, {

	constructor : function() {
		console.log("MenuDisplay!");
		
	
	var version = 0.1;
		
	// create a "hidden" version Dialog:
  	var dialogVersion = new dijit.Dialog({ title:"Test" , content: ("Mission Control System version " + version + "<br /><br />Some more important information"), style: "width: 350px", id: "versionNumber"});
  	dialogVersion.startup();

    // Show the version dialog
    function showVersion() {
        dijit.byId("versionNumber").show();
    }
    // Hide the version dialog
    function hideVersion() {
        dijit.byId("versionNumber").hide();
    }
		

	// Active GS dialog
  	var dialogGS = new dijit.Dialog({ title:"Active GS" , content: ("Active GS: <br><br>ES5EC"), style: "width: 350px", id: "activeGS"});
  	dialogGS.startup();

    // Show the GS dialog
    function showGS() {
        dijit.byId("activeGS").show();
    }
    // Hide the GS dialog
    function hideGS() {
        dijit.byId("activeGS").hide();
    }
		
	// Active satellite dialog
  	var dialogSatellite = new dijit.Dialog({ title:"Active satellite" , content: ("Active satellite: <br /><br />EstCube-1"), style: "width: 350px", id: "activeSatellite"});
  	dialogSatellite.startup();

    // Show the satellite dialog
    function showSatellite() {
        dijit.byId("activeSatellite").show();
    }
    // Hide the satellite dialog
    function hideSatellite() {
        dijit.byId("activeSatellite").hide();
    }
	
	
		
		var menuMain = new dijit.MenuItem({
			label : "Main View1",
			onClick : function() {
				dijit.byId('CenterContainer').selectChild(dijit.byId("mainTabContainer"));
				dijit.byId('InfoView').selectChild(dijit.byId("mainInfoTab"));
				dijit.byId('PictureView').selectChild(dijit.byId("pictureTab"));
			}
		});

		var menuStatuses = new dijit.MenuItem({
			label : "Statuses View",
			onClick : function() {
				dijit.byId('CenterContainer').selectChild(dijit.byId("statusesGraphTab"));
				dijit.byId('InfoView').selectChild(dijit.byId("statusesInfoTab"));
				dijit.byId('PictureView').selectChild(dijit.byId("componentTabContainer"));
			}
		});

		var menuCommand = new dijit.MenuItem({
			label : "Commands View",
			onClick : function() {
				dijit.byId('CenterContainer').forward();
				dijit.byId('InfoView').forward();
				dijit.byId('PictureView').forward();
			}
		});

		var menuLog = new dijit.MenuItem({
			label : "Log View"
		});

		var menuTimeline = new dijit.MenuItem({
			label : "Timeline View"
		});

		//Active satellite
		var satellite = "EstCube-1";
		var menuSatellite = new dijit.MenuItem({
			onClick : showSatellite,
			label : "Active satellite: " + satellite,
		});

		//Active GS
		var gs = "ES5EC";
		var menuGroundstation = new dijit.MenuItem({
			onClick : showGS,
			label : "Active GS: " + gs,
		});

		//Version no.
		var menuVersion = new dijit.MenuItem({
			onClick : showVersion,
			label : "GUI version: " + version,
		});
		
		var mainMenu = dijit.byId("MenuView");
		mainMenu.addChild(menuMain);
		mainMenu.addChild(menuStatuses);
		mainMenu.addChild(menuCommand);
		mainMenu.addChild(menuLog);
		mainMenu.addChild(menuTimeline);
		mainMenu.addChild(menuSatellite);
		mainMenu.addChild(menuGroundstation);
		mainMenu.addChild(menuVersion);

	}
});

dojo.declare("webgui.display.MenuDisplay", null, {

	constructor : function(args) {
		var menu = new MenuDisplay();
	}
});