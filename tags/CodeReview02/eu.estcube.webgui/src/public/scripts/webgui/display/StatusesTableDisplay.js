dojo.provide("webgui.display.StatusesTableDisplay");
dojo.require("dojox.grid.DataGrid");
dojo.require("dojox.grid.EnhancedGrid");
dojo.require("dojo.store.Memory");
dojo.require("dojo.data.ObjectStore");
dojo.require("dijit.layout.ContentPane");
dojo.require("dijit.layout.StackContainer");
dojo.require("dijit.layout.TabContainer");
dojo.require("dojo.data.ItemFileWriteStore");
dojo.require("dojox.layout.ContentPane");
dojo.require("webgui.display.StatusesGraphDisplayGS");

dojo.declare("webgui.display.StatusesTableDisplay", null, {

	constructor : function() {
		var someData = [];
		var radioData = [];
		var rotatorData = [];

		var radioStore = new dojo.data.ItemFileWriteStore({
			data : {
				identifier : 'radioStoreID',
				items : radioData
			}
		});

		var rotatorStore = new dojo.data.ItemFileWriteStore({
			data : {
				identifier : 'rotatorStoreID',
				items : rotatorData
			}
		});

		var store = new dojo.data.ItemFileWriteStore({
			data : {
				identifier : 'id',
				items : someData
			}
		});

		console.log("StatusesTableDisplay activated");

		var componentInfoContainer = new dijit.layout.TabContainer({
			id : "componentTabContainer",
			tabPosition : "bottom"
		});

		var tableHeaders = [{
			field : "source",
			name : "Source",
			width : 5
		}, {
			field : "device",
			name : "Device",
			width : 4
		}, {
			field : "name",
			name : "Name",
			width : 8
		}, {
			field : "param",
			name : "Param",
			width : 8
		}, {
			field : "value",
			name : "Value",
			width : 8
		}];

		// create a new grid:
		var dataField = new dojox.grid.EnhancedGrid({
			id : "statusesTable",
			title : "Component Info",
			store : store,
			structure : tableHeaders
		});

		var radioContent = new dojox.grid.EnhancedGrid({
			id : "radioTab",
			title : "RADIO",
			store : radioStore,
			structure : tableHeaders
		});

		var rotatorContent = new dojox.grid.EnhancedGrid({
			id : "rotatorTab",
			title : "ROTATOR",
			store : rotatorStore,
			structure : tableHeaders
		});

		componentInfoContainer.addChild(dataField);
		componentInfoContainer.addChild(radioContent);
		componentInfoContainer.addChild(rotatorContent);

		var tempCont;
		tempCont = dijit.byId("PictureView");

		tempCont.addChild(componentInfoContainer);
		dataField.startup();

		var makeKey = function(obj, i) {
			return obj.source + obj.device + obj.name + obj.params[i].name;
		};

		dojo.subscribe("fromWS", function(message) {
			console.log("Tabledisplay got: ", message);

			var myObject = JSON.parse(message);
			if(myObject.name == "GET_POSITION") {
				dojo.publish("GetPositionAzimuth", [myObject]);
			}

			// make object out of json
			for( i = 0; i < myObject.params.length; i++) {

				if(myObject.params[i].name == 'Azimuth') {
					putRotatorData(myObject.params[i].value, 0);
				} else if(myObject.params[i].name == 'Elevation') {
					putRotatorData(myObject.params[i].value, 1);
				} else if(myObject.params[i].name == 'Model name') {
					putRotatorData(myObject.params[i].value, 2);
				}

				var identity = makeKey(myObject, i);
				store.fetchItemByIdentity({
					'identity' : identity,
					onItem : function(item) {
						if(item == null) {
							store.newItem({
								id : makeKey(myObject, i),
								source : myObject.source,
								device : myObject.device,
								name : myObject.name,
								param : myObject.params[i].name,
								value : myObject.params[i].value,
								time : myObject.time
							});
						} else {
							store.setValue(item, 'source', myObject.source);
							store.setValue(item, 'device', myObject.device);
							store.setValue(item, 'name', myObject.name);
							store.setValue(item, 'param', myObject.params[i].name);
							store.setValue(item, 'value', myObject.params[i].value);
							store.setValue(item, 'time', myObject.time);
						}
					}
				});
			}
		});
	}
});
