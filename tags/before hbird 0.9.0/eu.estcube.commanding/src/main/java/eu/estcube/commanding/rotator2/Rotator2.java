package eu.estcube.commanding.rotator2;

import eu.estcube.commanding.analysis.OptimizationType;
import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.api.RotatorTypeInterface;
import eu.estcube.commanding.calculation.Calculation;
import eu.estcube.commanding.optimizators.Optimization;

/**
 * 2 Rotator type, that supports azimuth values in >450 range. Elevation is 180.
 * 
 * 
 * @author Ivar Mahhonin
 * 
 */
public class Rotator2 implements RotatorTypeInterface {
	double receivingSector;
	private final double maxAz = 445;
	private final double maxEl = 180;

	public Rotator2(final double receivingSector) {
		super();
		this.receivingSector = receivingSector;
	}

	public double getMaxAz() {
		return maxAz;
	}

	public double getMaxEl() {
		return maxEl;
	}

	@Override
	public Optimization getOptimizer(final OverPassType type) {
		OptimizationType optType = OptimizationType.NOTHING;
		switch (type) {
		case CROSS_ELEVATION_HIGH:
			// TODO delete class Rotator2CrossElHigh, because there are the same
			// optimizations basicallt as for Rotator1.
			optType = OptimizationType.ROT_2_CROSS_EL_HIGH;
			break;
		case NO_CROSS_ELEVATION_HIGH:
			optType = OptimizationType.ROT_1_NO_CROSS_EL_HIGH;
			break;
		case CROSS_ELEVATION_LOW:
			optType = OptimizationType.ROT_2_CROSS_EL_LOW;
			break;
		case NO_CROSS_ELEVATION_LOW:
			optType = OptimizationType.ROT_1_NO_CROSS_EL_LOW; // NO CHANGES
			break;

		case NOTHING:
			break;
		}
		Calculation.log.info(optType.toString());
		final Optimization optimization = new Optimization(optType, maxAz,
				maxEl, receivingSector);

		return optimization;
	}

	public double getReceivingSector() {
		return receivingSector;
	}

}