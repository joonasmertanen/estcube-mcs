package eu.estcube.commanding.rotator1;

import java.util.ArrayList;
import java.util.List;

import org.hbird.exchange.navigation.PointingData;

import eu.estcube.commanding.api.OptimizeInterface;
import eu.estcube.commanding.optimizators.GlobalFunctions;

/**
 * When satellite crosses the north line and elevation is low, then
 * class checks whether coordinates after the cross point are within
 * the receiving range. If yes, then we are deleting this points and antenna
 * does not go further, when it reaches this point.
 * If not, then we are changing current azimuth value to the opposite
 * and elevation is chaning from 180 to 0.
 * 
 * @author Ivar Mahhonin
 * 
 */
public class Rotator1CrossElLow implements OptimizeInterface {
	/**
	 * Finding the index on point, where trajectory crosses the north line.
	 * 
	 * @param lastValue - azimuth previous value.
	 * @param coordinates - satellite trajectory.
	 * @return - point, where trajectory crosses the north line.
	 */
	public static int getCrossPoint(double lastValue,
			final List<PointingData> coordinates) {
		int crossPoint = 0;
		for (int i = 0; i < coordinates.size(); i++) {
			if (Math.abs(lastValue - coordinates.get(i).getAzimuth().intValue()) > 180) {
				crossPoint = i;
			}
			lastValue = coordinates.get(i).getAzimuth().intValue();
		}

		return crossPoint;
	}

	/**
	 * Checking, if coordinates after the crosspoint are within antenna
	 * receiving range or not.
	 * 
	 * @param crossPoint
	 * @param azFirstValue
	 * @param azLastValue
	 * @param antennaReceivingSector
	 * @param coordinates
	 * @return
	 */
	public static int starEndMiddleCheck(final int crossPoint,
			final double azFirstValue, final double azLastValue,
			final double antennaReceivingSector,
			final List<PointingData> coordinates) {
		final int listCenter = coordinates.size() / 2;

		double left;
		double right;
		int caseToChoose = 0;
		final double azCrossPointValue = coordinates.get(crossPoint)
				.getAzimuth();

		left = Math.abs(azCrossPointValue - azFirstValue);

		right = Math.abs(azCrossPointValue - azLastValue);

		if (crossPoint < listCenter) {
			if (left > 300) {
				final double change = 360 - left;
				if (change < antennaReceivingSector) {
					caseToChoose = 1;
				} else {
					caseToChoose = 3;
				}

			} else {
				if (left < antennaReceivingSector) {
					caseToChoose = 1;
				} else {
					caseToChoose = 3;
				}

			}
		}

		if (crossPoint > listCenter) {
			if (right > 300) {
				final double change = 360 - right;
				if (change < antennaReceivingSector) {
					caseToChoose = 2;
				} else {
					caseToChoose = 3;
				}

			} else {
				if (right < antennaReceivingSector) {
					caseToChoose = 2;
				} else {
					caseToChoose = 3;
				}

			}
		}

		return caseToChoose;
	}

	double receivingSector;

	double maxAz;

	double maxEl;

	public Rotator1CrossElLow(final double receivingSector, final double maxAz,
			final double maxEl) {
		super();
		this.receivingSector = receivingSector;
		this.maxAz = maxAz;
		this.maxEl = maxEl;
	}

	/**
	 * Changing current azimuth values to the opposite.
	 * Chanigng elevation values from 180 to 0.
	 * 
	 * @param coordinates
	 * @return
	 */
	public List<PointingData> crossElLow(List<PointingData> coordinates) {
		final int coordinatesListSize = coordinates.size();

		final List<PointingData> coordinatesTemp = new ArrayList<PointingData>();

		for (int i = 0; i < coordinatesListSize; i++) {
			final long timestamp = coordinates.get(i).getTimestamp();
			final double doppler = coordinates.get(i).getDoppler();
			final String satelliteId = coordinates.get(i).getSatelliteId();
			final String groundStationId = coordinates.get(i)
					.getGroundStationId();

			final double azCurrentValue = coordinates.get(i).getAzimuth()
					.intValue();
			final double aznewValue = GlobalFunctions
					.changeAzimuthTo180(azCurrentValue);
			final double elevation = 180 - coordinates.get(i).getElevation()
					.intValue();
			final PointingData optimizedElement = new PointingData(timestamp,
					aznewValue, elevation, doppler, satelliteId,
					groundStationId);
			coordinatesTemp.add(optimizedElement);
		}
		coordinates = coordinatesTemp;
		return coordinates;

	}

	/**
	 * Case selector.
	 * 
	 * @param coordinates
	 * @return
	 */
	public List<PointingData> crossElLowStartEndMiddle(
			List<PointingData> coordinates) {
		final int coordinatesListSize = coordinates.size();
		final double lastValue = coordinates.get(0).getAzimuth().intValue();
		final double startAzimuth = lastValue;
		final double endAzimuth = coordinates.get(coordinatesListSize - 1)
				.getAzimuth().intValue();
		int crossPoint = 0;
		crossPoint = getCrossPoint(lastValue, coordinates);
		final int startEndMiddle = starEndMiddleCheck(crossPoint, startAzimuth,
				endAzimuth, receivingSector, coordinates);
		String status = "";
		List<PointingData> coordinatesTemp = new ArrayList<PointingData>();
		switch (startEndMiddle) {
		case 1:
			coordinatesTemp = deleteStartEndValues(1, crossPoint, coordinates);
			status = "contact start";

			break;
		case 2:
			coordinatesTemp = deleteStartEndValues(2, crossPoint, coordinates);
			status = "contact end";

			break;
		case 3:
			coordinatesTemp = crossElLow(coordinates);
			status = "middle";

			break;
		}
		coordinates = coordinatesTemp;
		return coordinates;

	}

	/**
	 * If coordinates after cross point are within antenna receiving range, then
	 * we are deleting this values.
	 * 
	 * @param cases - case type
	 * @param crossPoint - point index, where north line is crossed
	 * @param coordinates - satellite trajectory.
	 * @return
	 */

	public List<PointingData> deleteStartEndValues(final int cases,
			final int crossPoint, final List<PointingData> coordinates) {

		switch (cases) {
		case 1:
			for (int i = 0; i < crossPoint; i++) {
				coordinates.remove(0);

			}
			break;
		case 2:
			for (int i = crossPoint; i <= coordinates.size(); i++) {
				coordinates.remove(coordinates.size() - 1);

			}
			break;

		}
		return coordinates;

	}

	@Override
	public List<PointingData> optimize(List<PointingData> coordinates) {
		coordinates = crossElLowStartEndMiddle(coordinates);
		return coordinates;
	}

}