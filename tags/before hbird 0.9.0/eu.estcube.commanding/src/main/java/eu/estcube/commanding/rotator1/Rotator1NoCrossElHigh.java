package eu.estcube.commanding.rotator1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hbird.exchange.navigation.PointingData;

import eu.estcube.commanding.api.OptimizeInterface;
import eu.estcube.commanding.optimizators.GlobalFunctions;

/**
 * When north line is not crossed and elevation is high.
 * In this case,azimuth values are at the same sector.
 * Satellite is tracked using elevation rotators.
 * Azimuth values change is significant.
 * 
 * @author Ivar Mahhonin
 * 
 */
public class Rotator1NoCrossElHigh implements OptimizeInterface {
	double receivingSector;
	double maxAz;
	double maxEl;
	Map<Double, Double> coordinates;

	public Rotator1NoCrossElHigh(final double receivingSector,
			final double maxAz, final double maxEl) {
		super();
		this.receivingSector = receivingSector;
		this.maxAz = maxAz;
		this.maxEl = maxEl;

	}

	/**
	 * Looking through coordinates values.
	 * If current azimuth value belongs to the sector from where satellite
	 * starts its pass, then value stays the same.
	 * If value belongs to the sector that is not satellite start or end sector,
	 * then we are changing azimuth current value to the previous.
	 * When azimuth value belongs to the satellite end sector, then we are
	 * changing this value to the opposite.
	 * 
	 * @param coordinates -satellite trajectory.
	 * @return
	 */
	public List<PointingData> noCrossElHigh(List<PointingData> coordinates) {
		double elLastValue = coordinates.get(0).getElevation().intValue();
		final int coordinatesListSize = coordinates.size();
		final int azLastPointSector = GlobalFunctions
				.getDegreeSector(coordinates.get(coordinatesListSize - 1)
						.getAzimuth().intValue());
		final int azFirstPointSector = GlobalFunctions
				.getDegreeSector(coordinates.get(0).getAzimuth().intValue());
		boolean externalCall = false;
		final List<PointingData> coordinatesTemp = new ArrayList<PointingData>();

		if (this.maxAz < 360) {
			externalCall = false;
		}
		if (this.maxAz > 360) {
			externalCall = true;
		}

		for (int i = 0; i < coordinates.size(); i++) {
			final double currentValueSector = GlobalFunctions
					.getDegreeSector(coordinates.get(i).getAzimuth().intValue());
			double azCurrentValue = coordinates.get(i).getAzimuth().intValue();
			double elCurrentValue = coordinates.get(i).getElevation()
					.intValue();
			final long timestamp = coordinates.get(i).getTimestamp();
			final double doppler = coordinates.get(i).getDoppler();
			final String satelliteId = coordinates.get(i).getSatelliteId();
			final String groundStationId = coordinates.get(i)
					.getGroundStationId();

			if (currentValueSector == azLastPointSector) {
				if (i < coordinates.size() - 1) {
					final double azNextValue = coordinates.get(i + 1)
							.getAzimuth();
					if (Math.abs(azCurrentValue - azNextValue) >= 5) {
						azCurrentValue = azNextValue;
						if (externalCall == false) {
							azCurrentValue = GlobalFunctions
									.changeAzimuthTo180(azCurrentValue);
						}
						if (externalCall == true) {
							azCurrentValue = GlobalFunctions
									.changeAzimuthTo360(azCurrentValue);

						}
					} else {
						if (externalCall == false) {
							azCurrentValue = GlobalFunctions
									.changeAzimuthTo180(azCurrentValue);
						}
						if (externalCall == true) {
							azCurrentValue = GlobalFunctions
									.changeAzimuthTo360(azCurrentValue);

						}

					}
				} else {
					if (externalCall == false) {
						azCurrentValue = GlobalFunctions
								.changeAzimuthTo180(azCurrentValue);
					}
					if (externalCall == true) {
						azCurrentValue = GlobalFunctions
								.changeAzimuthTo360(azCurrentValue);

					}
				}
			}

			if ((currentValueSector != azFirstPointSector)
					&& (currentValueSector != azLastPointSector)) {
				azCurrentValue = coordinatesTemp
						.get(coordinatesTemp.size() - 1).getAzimuth();

			}
			if (elCurrentValue < elLastValue) {
				elCurrentValue = 180 - (elCurrentValue);
			}
			final PointingData optimizedElement = new PointingData(timestamp,
					azCurrentValue, elCurrentValue, doppler, satelliteId,
					groundStationId);
			coordinatesTemp.add(optimizedElement);
			elLastValue = coordinates.get(i).getElevation();

		}
		coordinates = coordinatesTemp;
		return coordinates;

	}

	@Override
	public List<PointingData> optimize(List<PointingData> coordinates) {
		coordinates = noCrossElHigh(coordinates);
		return coordinates;
	}

}