package eu.estcube.commanding.calculation;

import java.util.ArrayList;
import java.util.List;

import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.log4j.Logger;
import org.orekit.tle.TLE;

import eu.estcube.common.Constants;

public class MetaDataConsumer {
	static Logger log = Logger.getLogger(Calculation.class.getName());
	public static String url = "tcp://localhost:61616";
	public static String TOPIC = Constants.AMQ_COMMANDS_CONSUME_TIME;
	public static List<Long> contactTimeList = new ArrayList<Long>();

	public static void main(final String[] args) throws Exception {
		System.setProperty("orekit.data.path",
				"src/main/resources/orekit-data.zip");
		final double GS_LONGITUDE = Math.toRadians(26.7330);
		final double GS_LATITUDE = Math.toRadians(58.3000);
		final double GS_ALTITUDE = 59.0;
		final double upFr = 100000000;
		final double downFr = 437000000;
		final TLE tle = new TLE(
				"1 27846U 03031G   13104.25319836  .00000216  00000-0  11893-3 0  5406",
				"2 27846  98.6910 114.4374 0010068  55.8176  74.5716 14.21420837507771");

		final double tolerance = 4;
		final long start = 0;
		final long end = 0;
		final int gsMaxAz = 450;
		final int gsMaxEl = 180;
		final double gsReceivingSector = 7.5;

		while (true) {
			final CamelContext context = new DefaultCamelContext();
			final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
					url);
			context.addComponent("activemq",
					JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
			final ConsumerTemplate consumerTemplate = context
					.createConsumerTemplate();
			context.start();
			final Long contactTime = consumerTemplate.receiveBody(TOPIC,
					Long.class);
			contactTimeList.add(contactTime);
			log.info("Received time for Calculations with value " + contactTime);
			if (contactTimeList.size() == 2) {
				sendToCalculator(contactTimeList.get(0),
						contactTimeList.get(1), GS_LONGITUDE, GS_LATITUDE,
						GS_ALTITUDE, upFr, downFr, tle, tolerance, start, end,
						gsMaxAz, gsMaxEl, gsReceivingSector);
				contactTimeList.clear();
			}
		}

	}

	public static void sendToCalculator(final long start, final long end,
			final double gS_LONGITUDE, final double gS_LATITUDE,
			final double gS_ALTITUDE, final double upFr, final double downFr,
			final TLE tle, final double tolerance, final long start2,
			final long end2, final int gsMaxAz, final int gsMaxEl,
			final double gsReceivingSector) throws Exception {

		final Calculation commanding = new Calculation(gS_LONGITUDE,
				gS_LATITUDE, gS_ALTITUDE, tle, tolerance, start, end, gsMaxAz,
				gsMaxEl, gsReceivingSector);
		commanding.initCalculate();

	}

}