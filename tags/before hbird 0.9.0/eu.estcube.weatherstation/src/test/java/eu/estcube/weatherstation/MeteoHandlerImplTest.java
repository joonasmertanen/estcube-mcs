package eu.estcube.weatherstation;

import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_PRESSURE;
import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_TEMPERATURE;
import static eu.estcube.weatherstation.DefaultWeatherParameters.GAMMA;
import static eu.estcube.weatherstation.DefaultWeatherParameters.LUX;
import static eu.estcube.weatherstation.DefaultWeatherParameters.PRECIPITATIONS;
import static eu.estcube.weatherstation.DefaultWeatherParameters.RELATIVE_HUMIDITY;
import static eu.estcube.weatherstation.DefaultWeatherParameters.SOLE;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WIND_DIRECTION;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WIND_SPEED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.hbird.exchange.core.Label;
import org.hbird.exchange.core.Parameter;
import org.hbird.exchange.interfaces.IEntityInstance;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import eu.estcube.common.Naming;
import eu.estcube.common.Naming.Base;

public class MeteoHandlerImplTest {

    private MeteoHandlerImpl handler;
    private List<IEntityInstance> weatherParams;

    @Before
    public void setup() throws Exception {
        SAXBuilder builder = new SAXBuilder();
        Document document = builder.build(getClass().getResourceAsStream("/meteo.physics.xml"));
        handler = new MeteoHandlerImpl();
        weatherParams = handler.parseDocument(document);
    }

    /**
     * Test method for {@link IlmPars#getResult()}.
     * 
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws JDOMException
     */
    @Test
    public final void testParameters() {

        assertEquals(9, weatherParams.size());

        assertNotNull(weatherParams.get(0).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(0).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), AIR_TEMPERATURE.getName()), weatherParams.get(0).getName());
        assertEquals(AIR_TEMPERATURE.getDescription(), weatherParams.get(0).getDescription());
        assertEquals(AIR_TEMPERATURE.getUnit(), ((Parameter) weatherParams.get(0)).getUnit());
        assertEquals(21.6466262090116D, ((Parameter) weatherParams.get(0)).getValue());

        assertNotNull(weatherParams.get(1).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(1).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), RELATIVE_HUMIDITY.getName()), weatherParams.get(1).getName());
        assertEquals(RELATIVE_HUMIDITY.getDescription(), weatherParams.get(1).getDescription());
        assertEquals(RELATIVE_HUMIDITY.getUnit(), ((Parameter) weatherParams.get(1)).getUnit());
        assertEquals(70.0695341688312D, ((Parameter) weatherParams.get(1)).getValue());

        assertNotNull(weatherParams.get(2).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(2).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), AIR_PRESSURE.getName()), weatherParams.get(2).getName());
        assertEquals(AIR_PRESSURE.getDescription(), weatherParams.get(2).getDescription());
        assertEquals(AIR_PRESSURE.getUnit(), ((Parameter) weatherParams.get(2)).getUnit());
        assertEquals(1001.96090655264D, ((Parameter) weatherParams.get(2)).getValue());

        assertNotNull(weatherParams.get(3).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(3).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), WIND_DIRECTION.getName()), weatherParams.get(3).getName());
        assertEquals(WIND_DIRECTION.getDescription(), weatherParams.get(3).getDescription());
        assertEquals("-", ((Label) weatherParams.get(3)).getValue());

        assertNotNull(weatherParams.get(4).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(4).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), WIND_SPEED.getName()), weatherParams.get(4).getName());
        assertEquals(WIND_SPEED.getDescription(), weatherParams.get(4).getDescription());
        assertEquals(WIND_SPEED.getUnit(), ((Parameter) weatherParams.get(4)).getUnit());
        assertNull(((Parameter) weatherParams.get(4)).getValue());

        assertNotNull(weatherParams.get(5).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(5).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), LUX.getName()), weatherParams.get(5).getName());
        assertEquals(LUX.getDescription(), weatherParams.get(5).getDescription());
        assertEquals(LUX.getUnit(), ((Parameter) weatherParams.get(5)).getUnit());
        assertEquals(10056.0792521323D, ((Parameter) weatherParams.get(5)).getValue());

        assertNotNull(weatherParams.get(6).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(6).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), SOLE.getName()), weatherParams.get(6).getName());
        assertEquals(SOLE.getDescription(), weatherParams.get(6).getDescription());
        assertEquals(SOLE.getUnit(), ((Parameter) weatherParams.get(6)).getUnit());
        assertNull(((Parameter) weatherParams.get(6)).getValue());

        assertNotNull(weatherParams.get(7).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(7).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), GAMMA.getName()), weatherParams.get(7).getName());
        assertEquals(GAMMA.getDescription(), weatherParams.get(7).getDescription());
        assertEquals(GAMMA.getUnit(), ((Parameter) weatherParams.get(7)).getUnit());
        assertEquals(0.0675110184290355D, ((Parameter) weatherParams.get(7)).getValue());

        assertNotNull(weatherParams.get(8).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(8).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), PRECIPITATIONS.getName()), weatherParams.get(8).getName());
        assertEquals(PRECIPITATIONS.getDescription(), weatherParams.get(8).getDescription());
        assertEquals(PRECIPITATIONS.getUnit(), ((Parameter) weatherParams.get(8)).getUnit());
        assertEquals(0D, ((Parameter) weatherParams.get(8)).getValue());
    }
}
