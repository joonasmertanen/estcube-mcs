package eu.estcube.weatherstation;

import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_PRESSURE;
import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_TEMPERATURE;
import static eu.estcube.weatherstation.DefaultWeatherParameters.NAME;
import static eu.estcube.weatherstation.DefaultWeatherParameters.PHENOMENON;
import static eu.estcube.weatherstation.DefaultWeatherParameters.PRECIPITATIONS;
import static eu.estcube.weatherstation.DefaultWeatherParameters.RELATIVE_HUMIDITY;
import static eu.estcube.weatherstation.DefaultWeatherParameters.VISIBILITY;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WIND_DIRECTION;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WIND_SPEED;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WIND_SPEED_MAX;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WMO_CODE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.hbird.exchange.core.Label;
import org.hbird.exchange.core.Parameter;
import org.hbird.exchange.interfaces.IEntityInstance;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import eu.estcube.common.Naming;
import eu.estcube.common.Naming.Base;

public class EmhiHandlerImplTest {

    private EmhiHandlerImpl handler;
    private List<IEntityInstance> weatherParams;

    @Before
    public void setup() throws Exception {
        SAXBuilder builder = new SAXBuilder();
        Document document = builder.build(getClass().getResourceAsStream("/EMHI.xml"));
        handler = new EmhiHandlerImpl();
        weatherParams = handler.parseDocument(document);
    }

    /**
     * Test method for {@link IlmPars#getResult()}.
     * 
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws JDOMException
     */
    @Test
    public final void testParameters() {

        assertEquals(11, weatherParams.size());

        assertNotNull(weatherParams.get(0).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(0).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), NAME.getName()), weatherParams.get(0).getName());
        assertEquals(NAME.getDescription(), weatherParams.get(0).getDescription());
        assertEquals("Tartu (Tõravere)", ((Label) weatherParams.get(0)).getValue());

        assertNotNull(weatherParams.get(1).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(1).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), WMO_CODE.getName()), weatherParams.get(1).getName());
        assertEquals(WMO_CODE.getDescription(), weatherParams.get(1).getDescription());
        assertEquals(WMO_CODE.getUnit(), ((Parameter) weatherParams.get(1)).getUnit());
        assertEquals(26242, ((Parameter) weatherParams.get(1)).getValue());

        assertNotNull(weatherParams.get(2).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(2).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), PHENOMENON.getName()), weatherParams.get(2).getName());
        assertEquals(PHENOMENON.getDescription(), weatherParams.get(2).getDescription());
        assertEquals("Light shower", ((Label) weatherParams.get(2)).getValue());

        assertNotNull(weatherParams.get(3).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(3).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), VISIBILITY.getName()), weatherParams.get(3).getName());
        assertEquals(VISIBILITY.getDescription(), weatherParams.get(3).getDescription());
        assertEquals(VISIBILITY.getUnit(), ((Parameter) weatherParams.get(3)).getUnit());
        assertEquals(26.0D, ((Parameter) weatherParams.get(3)).getValue());

        assertNotNull(weatherParams.get(4).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(4).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), PRECIPITATIONS.getName()), weatherParams.get(4).getName());
        assertEquals(PRECIPITATIONS.getDescription(), weatherParams.get(4).getDescription());
        assertEquals(PRECIPITATIONS.getUnit(), ((Parameter) weatherParams.get(4)).getUnit());
        assertEquals(0.3D, ((Parameter) weatherParams.get(4)).getValue());

        assertNotNull(weatherParams.get(5).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(5).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), AIR_PRESSURE.getName()), weatherParams.get(5).getName());
        assertEquals(AIR_PRESSURE.getDescription(), weatherParams.get(5).getDescription());
        assertEquals(AIR_PRESSURE.getUnit(), ((Parameter) weatherParams.get(5)).getUnit());
        assertEquals(1006.4D, ((Parameter) weatherParams.get(5)).getValue());

        assertNotNull(weatherParams.get(6).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(6).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), RELATIVE_HUMIDITY.getName()), weatherParams.get(6).getName());
        assertEquals(RELATIVE_HUMIDITY.getDescription(), weatherParams.get(6).getDescription());
        assertEquals(RELATIVE_HUMIDITY.getUnit(), ((Parameter) weatherParams.get(6)).getUnit());
        assertEquals(77D, ((Parameter) weatherParams.get(6)).getValue());

        assertNotNull(weatherParams.get(7).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(7).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), AIR_TEMPERATURE.getName()), weatherParams.get(7).getName());
        assertEquals(AIR_TEMPERATURE.getDescription(), weatherParams.get(7).getDescription());
        assertEquals(AIR_TEMPERATURE.getUnit(), ((Parameter) weatherParams.get(7)).getUnit());
        assertEquals(16.5D, ((Parameter) weatherParams.get(7)).getValue());

        assertNotNull(weatherParams.get(8).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(8).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), WIND_DIRECTION.getName()), weatherParams.get(8).getName());
        assertEquals(WIND_DIRECTION.getDescription(), weatherParams.get(8).getDescription());
        assertEquals("223", ((Label) weatherParams.get(8)).getValue());

        assertNotNull(weatherParams.get(9).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(9).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), WIND_SPEED.getName()), weatherParams.get(9).getName());
        assertEquals(WIND_SPEED.getDescription(), weatherParams.get(9).getDescription());
        assertEquals(WIND_SPEED.getUnit(), ((Parameter) weatherParams.get(9)).getUnit());
        assertEquals(2.6D, ((Parameter) weatherParams.get(9)).getValue());

        assertNotNull(weatherParams.get(10).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(10).getIssuedBy());
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), WIND_SPEED_MAX.getName()), weatherParams.get(10).getName());
        assertEquals(WIND_SPEED_MAX.getDescription(), weatherParams.get(10).getDescription());
        assertEquals(WIND_SPEED_MAX.getUnit(), ((Parameter) weatherParams.get(10)).getUnit());
        assertEquals(5.7D, ((Parameter) weatherParams.get(10)).getValue());
    }
}
