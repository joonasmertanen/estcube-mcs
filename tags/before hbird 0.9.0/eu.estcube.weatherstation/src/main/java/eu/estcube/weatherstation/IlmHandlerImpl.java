package eu.estcube.weatherstation;

import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_PRESSURE;
import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_PRESSURE_TREND;
import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_TEMPERATURE;
import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_TEMPERATURE_TREND;
import static eu.estcube.weatherstation.DefaultWeatherParameters.PHENOMENON;
import static eu.estcube.weatherstation.DefaultWeatherParameters.RELATIVE_HUMIDITY;
import static eu.estcube.weatherstation.DefaultWeatherParameters.RELATIVE_HUMIDITY_TREND;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WIND_DIRECTION;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WIND_SPEED;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.hbird.exchange.interfaces.IEntityInstance;
import org.jdom2.Document;
import org.jdom2.Element;

public class IlmHandlerImpl extends AbstractDataHandler {

    public static final String DATE_FORMAT = "yyyyMMddHHmmss";

    private static final DateFormat FORMATTER = new SimpleDateFormat(DATE_FORMAT);

    protected String getDataSourceName() {
        return "ilm.ee";
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    protected Map<String, NamedConfig<? extends IEntityInstance>> getParameterMapping() {
        Map<String, NamedConfig<? extends IEntityInstance>> map = new HashMap<String, NamedConfig<? extends IEntityInstance>>();
        map.put("temp", new NamedConfig(AIR_TEMPERATURE, new DoubleParameterFactory()));
        map.put("temptrend", new NamedConfig(AIR_TEMPERATURE_TREND, new LabelFactory()));
        map.put("rohk", new NamedConfig(AIR_PRESSURE, new DoubleParameterFactory()));
        map.put("rohktrend", new NamedConfig(AIR_PRESSURE_TREND, new LabelFactory()));
        map.put("niiskus", new NamedConfig(RELATIVE_HUMIDITY, new DoubleParameterFactory()));
        map.put("niiskustrend", new NamedConfig(RELATIVE_HUMIDITY_TREND, new LabelFactory()));
        map.put("tuul", new NamedConfig(WIND_SPEED, new DoubleParameterFactory()));
        map.put("suund", new NamedConfig(WIND_DIRECTION, new LabelFactory()));
        map.put("nahtus", new NamedConfig(PHENOMENON, new LabelFactory()));
        return map;
    }

    /** @{inheritDoc . */
    @Override
    protected Element getParentNodeForWeatherData(Document document) throws IllegalArgumentException {
        return getBaseNode(document).getChild("blokk");
    }

    /** @{inheritDoc . */
    @Override
    protected Date parseDate(Document document) throws ParseException {
        return FORMATTER.parse(getBaseNode(document).getAttributeValue("id"));
    }

    Element getBaseNode(Document document) {
        return document.getRootElement().getChild("rubriik").getChild("sisestus");
    }
}
