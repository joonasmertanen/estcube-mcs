/** 
 *
 */
package eu.estcube.sdc.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

/**
 *
 */
public class SpaceDataChainConfigurationTest {

    private SpaceDataChainConfiguration config;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        config = new SpaceDataChainConfiguration();
    }

    @Test
    public void testIsUseFiles() throws Exception {
        testSetUseFiles();
    }

    @Test
    public void testSetUseFiles() throws Exception {
        assertFalse(config.isUseFiles());
        config.setUseFiles(true);
        assertTrue(config.isUseFiles());
    }

    @Test
    public void testGetUplinkAx25() throws Exception {
        testSetUplinkAx25();
    }

    @Test
    public void testSetUplinkAx25() throws Exception {
        assertNull(config.getUplinkAx25());
        config.setUplinkAx25("UPLINK-AX.25");
        assertEquals("UPLINK-AX.25", config.getUplinkAx25());
    }

    @Test
    public void testGetDownlinkAx25() throws Exception {
        testSetDownlinkAx25();
    }

    @Test
    public void testSetDownlinkAx25() throws Exception {
        assertNull(config.getDownlinkAx25());
        config.setDownlinkAx25("DOWNLINK-AX.25");
        assertEquals("DOWNLINK-AX.25", config.getDownlinkAx25());
    }

    @Test
    public void testGetImportAx25() throws Exception {
        testSetImportAx25();
    }

    @Test
    public void testSetImportAx25() throws Exception {
        assertNull(config.getImportAx25());
        config.setImportAx25("IMPORT-AX.25");
        assertEquals("IMPORT-AX.25", config.getImportAx25());
    }

    @Test
    public void testGetFilePollInterval() throws Exception {
        assertEquals(0, config.getFilePollInterval());
    }

    @Test
    public void testGetMaxMessagesPerPoll() throws Exception {
        assertEquals(0, config.getMaxMessagesPerPoll());
    }
}
