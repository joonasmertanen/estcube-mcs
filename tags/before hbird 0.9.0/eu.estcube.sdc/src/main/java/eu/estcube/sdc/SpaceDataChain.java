/** 
 *
 */
package eu.estcube.sdc;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.core.BusinessCard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import eu.estcube.common.Headers;
import eu.estcube.common.PrepareForInjection;
import eu.estcube.common.UpdateTimestamp;
import eu.estcube.domain.transport.Downlink;
import eu.estcube.domain.transport.Uplink;
import eu.estcube.sdc.domain.SpaceDataChainConfiguration;
import eu.estcube.sdc.gcp.Ax25UiFrameToIssuedProcessor;
import eu.estcube.sdc.gcp.CommandToAx25UiFrameProcessor;
import eu.estcube.sdc.processor.Ax25UIFrameToTncFrame;
import eu.estcube.sdc.processor.Ax25UiFrameImportProcessor;
import eu.estcube.sdc.processor.Ax25UiFrameInputProcessor;
import eu.estcube.sdc.processor.Ax25UiFrameOutputProcessor;
import eu.estcube.sdc.processor.Ax25UiFrameOutputToFileProcessor;
import eu.estcube.sdc.processor.TncFrameToAx25UIFrame;
import eu.estcube.sdc.xml.Ax25UiFrameImportParser;
import eu.estcube.sdc.xml.Ax25UiFrameInputParser;
import eu.estcube.sdc.xml.Ax25UiFrameOutputSerializer;

/**
 *
 */
public class SpaceDataChain extends RouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(SpaceDataChain.class);

    @Autowired
    private SpaceDataChainConfiguration config;

    @Autowired
    private PrepareForInjection preparator;

    @Autowired
    private TncFrameToAx25UIFrame tncToAx25Decoder;

    @Autowired
    private Ax25UIFrameToTncFrame ax25ToTncEncoder;

    @Autowired
    private Ax25UiFrameInputParser xmlParser;

    @Autowired
    private Ax25UiFrameInputProcessor inputProcesor;

    @Autowired
    private Ax25UiFrameOutputProcessor outputProcessor;

    @Autowired
    private Ax25UiFrameOutputSerializer xmlSerializer;

    @Autowired
    private Ax25UiFrameOutputToFileProcessor toFile;

    @Autowired
    private Ax25UiFrameToIssuedProcessor ax25UiFrameToIssuedProcessor;

    @Autowired
    private CommandToAx25UiFrameProcessor commandToAx25UiFrameProcessor;

    @Autowired
    private UpdateTimestamp updateTimestamp;

    @Autowired
    private Ax25UiFrameImportParser importXmlParser;

    @Autowired
    private Ax25UiFrameImportProcessor importProcessor;

    /** @{inheritDoc . */
    @Override
    public void configure() throws Exception {
        MDC.put(StandardArguments.ISSUED_BY, config.getServiceId());

        BusinessCard card = new BusinessCard(config.getServiceId(), config.getHeartBeatInterval());
        card.setDescription(String.format("Space Data Chain; version: %s", config.getServiceVersion()));

        // @formatter:off
        
        // send business card
        from("timer://heartbeat?fixedRate=true&period=" + config.getHeartBeatInterval())
            .bean(card, "touch")
            .process(preparator)
            .to("activemq:topic:hbird.monitoring");
        
        // Down-link

        from(Downlink.AX25_FRAMES)
                .bean( ax25UiFrameToIssuedProcessor )
                .split( body() )
                .process(preparator)
                .to("log:eu.estcube.sdc.ax25ToGcp?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
                .to("activemq:topic:hbird.monitoring");
        
        from("direct:toDownlink")
            .to(Downlink.AX25_FRAMES)
            .end();
        
        from(Downlink.FROM_TNC)
            .bean(tncToAx25Decoder)
            .to("log:eu.estcube.sdc.fromTnc?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
            .to("direct:toDownlink");
        
        // Up-link

        from("direct:toTnc")
            // add Ground Station ID as header to enable filter based consumption for example in TNC driver
            .setHeader(StandardArguments.GROUND_STATION_NAME, constant(config.getGroundstationId()))
            .to("log:eu.estcube.sdc.toTnc?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
            .to(Uplink.TNC_FRAMES)
            .end();
        
        from("direct:encodeForTnc")
            .bean(ax25ToTncEncoder)
            .to("direct:toTnc");
        
        from("seda:logUplinkAx25Frames")
            .bean(updateTimestamp) // update the timestamp header value to the current one
            .setHeader(StandardArguments.CLASS, simple("${body.class.getSimpleName}"))
            .setHeader(StandardArguments.TYPE, constant(TncFrameToAx25UIFrame.TYPE)) // TODO - 02.04.2013; kimmell - refactor
            .setHeader(StandardArguments.ISSUED_BY, constant(config.getServiceId()))
            .setHeader(Headers.COMMUNICATION_LINK_TYPE, constant(Uplink.class.getSimpleName()))
            .to(Uplink.AX25_FRAMES_LOG)
            .end();
        
        from(Uplink.AX25_FRAMES)
            .multicast()
            .to("direct:encodeForTnc", "seda:logUplinkAx25Frames");
        
        from(Uplink.COMMANDS)
            .bean(commandToAx25UiFrameProcessor)
            .to("log:eu.estcube.sdc.commandToAx25UiFrames?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
            .to(Uplink.AX25_FRAMES);
        
        if (config.isUseFiles()) {
            from("file://" + config.getUplinkAx25() + "?recursive=true&move=.sent&maxMessagesPerPoll=" + config.getMaxMessagesPerPoll() + "&delay=" + config.getFilePollInterval())
                .bean(xmlParser)
                .split(body())
                .bean(inputProcesor)
                .to("log:eu.estcube.sdc.input.ax25.xml?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
                .to(Uplink.AX25_FRAMES);
            
            from(Downlink.AX25_FRAMES)
                .bean(outputProcessor)
                .bean(toFile)
                .to("log:eu.estcube.sdc.output.ax25.xml?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
                .to("file://" + config.getDownlinkAx25());
            
            from("file://" + config.getImportAx25() + "?move=.imported&moveFailed=.invalid&maxMessagesPerPoll=" + config.getMaxMessagesPerPoll() + "&delay=" + config.getFilePollInterval())
                .bean(importXmlParser)
                .split(body())
                .bean(importProcessor)
                .to("log:eu.estcube.sdc.import.ax25.xml?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
                .to("direct:toDownlink");
        }
        
        // @formatter:on
    }

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        LOG.info("Starting Space Data Chain");
        new Main().run(args);
    }
}
