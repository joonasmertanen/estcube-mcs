/** 
 *
 */
package eu.estcube.webserver.routes;

import org.hbird.exchange.configurator.StandardEndpoints;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class WebserverMonitoringDispatcher extends WebserverRouteBuilder {

    public static final String FILTER_BUSINESS_CARDS = "${in.header.class} == 'BusinessCard'";
    public static final String FILTER_PARAMETERS = "${in.header.class} in 'Parameter,State,Label,Metadata'";

    public static final String DESTINATION_BUSINESS_CARDS = "direct:businessCards";
    public static final String DESTINATION_PARAMETERS = "direct:parameters";
    public static final String DESTINATION_UNFILTERED = "direct:unfiltered";

    /** @{inheritDoc . */
    @Override
    protected String getSource() {
        return StandardEndpoints.MONITORING;
    }

    /** @{inheritDoc . */
    @Override
    protected String getDestination() {
        return null;
    }

    /** @{inheritDoc . */
    @Override
    public void configure() throws Exception {
        // @formatter:off
        from(getSource())
            .choice()
                .when(simple(FILTER_BUSINESS_CARDS)).to(DESTINATION_BUSINESS_CARDS)
                .when(simple(FILTER_PARAMETERS)).to(DESTINATION_PARAMETERS)
                .otherwise().to(DESTINATION_UNFILTERED)
            .end();
        // @formatter:on
    }
}
