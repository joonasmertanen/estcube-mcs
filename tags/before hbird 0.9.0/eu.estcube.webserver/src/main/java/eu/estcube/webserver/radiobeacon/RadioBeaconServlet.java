package eu.estcube.webserver.radiobeacon;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hbird.exchange.core.EntityInstance;
import org.springframework.stereotype.Component;

import eu.estcube.codec.radiobeacon.RadioBeaconDateInputParser;
import eu.estcube.codec.radiobeacon.RadioBeaconTranslator;
import eu.estcube.codec.radiobeacon.exceptions.InvalidRadioBeaconException;
import eu.estcube.common.Constants;

@SuppressWarnings("serial")
@Component
public class RadioBeaconServlet extends HttpServlet {

    @EndpointInject(uri = "direct:radioBeaconInput")
    ProducerTemplate producer;

    class RadioBeacon {
        protected Set<String> radioBeaconMessage = new HashSet<String>();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String datetime = request.getParameter("datetime").trim();
        String source = request.getParameter("source").trim();
        String data = request.getParameter("data").trim();

        // used for multibeacon upload error display
        String lineNumber = request.getParameter("lineNumber");
        if (lineNumber != null)
            lineNumber = lineNumber.trim();

        JSONObject json = new JSONObject();

        try {
            try {
                if (source.length() > Constants.DATABASE_BEACON_ISSUEDBY_LENGTH) {
                    throw new InvalidRadioBeaconException("Issuer name too long. Max " +
                            Constants.DATABASE_BEACON_ISSUEDBY_LENGTH + " chars.");
                }

                RadioBeaconTranslator translator = new RadioBeaconTranslator();
                HashMap<String, EntityInstance> parameters = translator.toParameters(data,
                        new RadioBeaconDateInputParser().parse(datetime), source);

                if (parameters.values().size() == 0)
                    throw new InvalidRadioBeaconException(
                            "Possible errors: incorrect beacon length, invalid symbols, incorrect callsign");

                for (EntityInstance key : parameters.values()) {
                    producer.sendBody(key);
                }

                json.put("status", "ok");
                json.put("message", "Data sent!");
                json.put("lineNumber", lineNumber);

            } catch (Exception e) {
                json.put("status", "error");
                json.put("message", "There were problems with parsing the data! Nothing sent! " + e.getMessage());
                json.put("exceptionMessage", e.getMessage());
                json.put("lineNumber", lineNumber);
            }
        } catch (JSONException e1) {
            throw new ServletException(e1);
        }

        response.getWriter().write(json.toString());

    }
}
