package eu.estcube.webserver.tle.upload;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Body;
import org.hbird.exchange.core.Metadata;
import org.hbird.exchange.dataaccess.TlePropagationRequest;
import org.hbird.exchange.interfaces.IEntityInstance;
import org.hbird.exchange.navigation.TleOrbitalParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.estcube.common.Headers;
import eu.estcube.common.hbird.MetadataFactory;
import eu.estcube.webserver.domain.TleUploadRequest;

/**
 * Converts {@link TleUploadRequest} to {@link TleOrbitalParameters}.
 */
@Component
public class TleUploadRequestConverter {

    /** TLE parameter name. */
    public static final String PARAMTER_TLE = "TLE";

    /** TLE description. */
    public static final String DESCRIPTION = "Two Line Element to describe the orbit.";

    @Value("${service.id}")
    public String serviceId;

    @Autowired
    public MetadataFactory metadataFactory;

    /**
     * Converts {@link TleUploadRequest} to {@link TlePropagationRequest}.
     * 
     * @param request
     *        {@link TleUploadRequest} to convert
     * @return new instance of {@link TlePropagationRequest}
     */
    @SuppressWarnings("deprecation")
    public List<IEntityInstance> convert(@Body TleUploadRequest request) {
        String satellite = request.getSatellite();
        Long timestamp = request.getTimestamp();
        String[] lines = request.getTleText().trim().split("\n");
        String name = String.format("%s/%s", satellite, PARAMTER_TLE);
        TleOrbitalParameters top = new TleOrbitalParameters(serviceId, name,
                DESCRIPTION, timestamp, satellite, lines[0].trim(), lines[1].trim());
        top.setEntityID(name);

        Map<String, Object> data = new HashMap<String, Object>(2);
        data.put(Headers.USERNAME, request.getUploader());
        data.put(Headers.SOURCE, request.getTleSource());
        Metadata meta = metadataFactory.createMetadata(top, data, serviceId);

        List<IEntityInstance> result = new ArrayList<IEntityInstance>(2);
        result.add(top);
        result.add(meta);

        return result;
    }
}
