package eu.estcube.webserver.catalogue;

import java.util.List;
import java.util.StringTokenizer;

import org.hbird.business.api.ICatalogue;
import org.hbird.business.api.IDataAccess;
import org.hbird.exchange.core.EntityInstance;
import org.hbird.exchange.dataaccess.LocationContactEventRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// FIXME - 30.04.2013; kimmell - code conventions!
public class RootNode extends CatalogueQueryNode {

    private static final Logger LOG = LoggerFactory.getLogger(RootNode.class);

    private ICatalogue m_catalogue;
    private IDataAccess m_dao;

    private String decodeID(String id) {
        return id.replaceAll("([^_]|^)_([^_]|$)", "$1/$2").replaceAll("_(_+)", "$1");
    }

    private Op m_op_satellite_list = new Op() {
        @Override
        public Object getResult(java.util.StringTokenizer st) {
            return m_catalogue.getSatellites();
        };
    };

    private Op m_op_satellite_query = new Op() {
        @Override
        public Object getResult(java.util.StringTokenizer st) {
            if (!st.hasMoreTokens())
                return null;

            return m_catalogue.getSatelliteByName(st.nextToken());
        };
    };

    private Op m_op_groundstation_list = new Op() {
        @Override
        public Object getResult(java.util.StringTokenizer st) {
            return m_catalogue.getGroundStations();
        };
    };

    private Op m_op_groundstation_query = new Op() {
        @Override
        public Object getResult(java.util.StringTokenizer st) {
            if (!st.hasMoreTokens())
                return null;

            return m_catalogue.getGroundStationByName(st.nextToken());
        };
    };

    private Op m_op_orbitalState_query = new Op() {
        @Override
        public Object getResult(StringTokenizer st) {
            if (!st.hasMoreTokens()) {
                return null;
            }
            long now = System.currentTimeMillis();
            long start = now - 1000L * 60 * 60;
            long end = now + 1000L * 60 * 60 * 2;
            return m_dao.getOrbitalStatesFor(decodeID(st.nextToken()), start, end);
        }
    };

    private Op m_op_contacEvents_query = new Op() {
        @Override
        public Object getResult(StringTokenizer st) {
            if (!st.hasMoreTokens()) {
                return null;
            }

            LocationContactEventRequest request = new LocationContactEventRequest("webserver",
                    decodeID(st.nextToken()));
            request.setSatelliteName(null);
            List<EntityInstance> events = m_dao.getData(request);
            return events;
        }
    };

    public RootNode(ICatalogue catalogue, IDataAccess dao) {
        m_catalogue = catalogue;
        m_dao = dao;

        addOption("satellites", m_op_satellite_list);
        addOption("satellite", m_op_satellite_query);
        addOption("groundstations", m_op_groundstation_list);
        addOption("groundstation", m_op_groundstation_query);
        addOption("orbitalstates", m_op_orbitalState_query);
        addOption("contactevents", m_op_contacEvents_query);
    };

}
