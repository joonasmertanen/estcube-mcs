package eu.estcube.webserver.tle.request;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hbird.business.archive.api.DataAccess;
import org.hbird.exchange.core.EntityInstance;
import org.hbird.exchange.dataaccess.TleRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.estcube.common.hbird.HbirdApiHelper;
import eu.estcube.common.json.ToJsonProcessor;
import eu.estcube.webserver.utils.HttpResponseSupport;

/**
 *
 */
@Component
public class TleServlet extends HttpServlet {

    /** */
    private static final long serialVersionUID = -4343823849095171602L;

    private static final Logger LOG = LoggerFactory.getLogger(TleServlet.class);

    /** To JSON processor for result serialization. */
    @Autowired
    private ToJsonProcessor toJson;

    @Autowired
    private HttpResponseSupport responseSupport;

    @Value("${service.id}")
    private String serviceId;

    /** @{inheritDoc . */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DataAccess dao = new DataAccess(serviceId);
        try {
            // long now = System.currentTimeMillis();
            // long year = 1000L * 60 * 60 * 24 * 365;
            // List<TleOrbitalParameters> data =
            // dao.getTleFor("/ESTCUBE/Satellites/ESTCube-1", now - year, now +
            // year);
            TleRequest request = new TleRequest(serviceId, "/ESTCUBE/Satellites/ESTCube-1");
            request.setIsInitialization(true);
            List<EntityInstance> data = dao.getData(request);
            responseSupport.sendAsJson(resp, toJson, data);
        } catch (Exception e) {
            LOG.error("Failed to handle TLE request", e);
            throw new ServletException("Failed to handle TLE request", e);
        } finally {
            HbirdApiHelper.dispose(dao);
        }
    }
}
