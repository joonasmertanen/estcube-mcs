package eu.estcube.webserver;

import org.apache.camel.Body;
import org.hbird.business.archive.api.Publish;
import org.hbird.exchange.core.EntityInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.estcube.common.hbird.HbirdApiHelper;

/**
 *
 */
@Component
public class NamedObjectPublisher {

    private static final Logger LOG = LoggerFactory.getLogger(NamedObjectPublisher.class);

    @Value("${service.id}")
    private String serviceId;

    public EntityInstance publishAndCommit(@Body EntityInstance entityInstance) {
        Publish publisher = new Publish(serviceId);
        try {
            publisher.publish(entityInstance);
            publisher.commit();
        } catch (Exception e) {
            LOG.error("Failed to publish {}", entityInstance.prettyPrint(), e);
        } finally {
            HbirdApiHelper.dispose(publisher);
        }
        return entityInstance;
    }
}
