package eu.estcube.webserver.tle.upload;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.common.json.ToJsonProcessor;
import eu.estcube.webserver.domain.TleUploadRequest;
import eu.estcube.webserver.domain.UIResponse;
import eu.estcube.webserver.domain.UserInfo;
import eu.estcube.webserver.utils.CamelSender;
import eu.estcube.webserver.utils.HttpResponseSupport;
import eu.estcube.webserver.utils.UserInfoSupport;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TleSubmitServletTest {

    private static final String TLE_SOURCE = "interwebs";

    private static final String TLE_TEXT = "Two Line Element";

    private static final String USER = "Baul";

    private static final String SATELLITE = "ESTCube-1";

    @InjectMocks
    private TleSubmitServlet servlet;

    @Mock
    private CamelSender camel;

    @Mock
    private UIResponse uiResponse;

    @Mock
    private UserInfoSupport uis;

    @Mock
    private UserInfo userInfo;

    @Mock
    private HttpServletRequest req;

    @Mock
    private HttpSession session;

    @Mock
    private HttpServletResponse resp;

    @Mock
    private HttpResponseSupport responseSupport;

    @Mock
    private ToJsonProcessor toJson;

    private Exception exception;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        exception = new Exception();
        inOrder = inOrder(camel, uiResponse, uis, userInfo, req, session, resp, responseSupport, toJson);
    }

    /**
     * Test method for
     * {@link eu.estcube.webserver.tle.upload.TleSubmitServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}
     * .
     */
    @Test
    public void testDoPost() throws Exception {
        when(req.getParameter(TleSubmitServlet.POST_PARAMETER_TLE_SOURCE)).thenReturn(TLE_SOURCE);
        when(req.getParameter(TleSubmitServlet.POST_PARAMETER_TLE_TEXT)).thenReturn(TLE_TEXT);
        when(req.getParameter(TleSubmitServlet.POST_PARAMETER_TLE_SATELLITE_ID)).thenReturn(SATELLITE);
        when(req.getSession()).thenReturn(session);
        when(uis.getUserInfo(session)).thenReturn(userInfo);
        when(userInfo.getUsername()).thenReturn(USER);
        when(camel.send(any(TleUploadRequest.class))).thenReturn(uiResponse);
        servlet.doPost(req, resp);

        inOrder.verify(req, times(1)).getParameter(TleSubmitServlet.POST_PARAMETER_TLE_SOURCE);
        inOrder.verify(req, times(1)).getParameter(TleSubmitServlet.POST_PARAMETER_TLE_TEXT);
        inOrder.verify(req, times(1)).getParameter(TleSubmitServlet.POST_PARAMETER_TLE_SATELLITE_ID);
        inOrder.verify(req, times(1)).getSession();
        inOrder.verify(uis, times(1)).getUserInfo(session);
        inOrder.verify(userInfo, times(1)).getUsername();
        ArgumentCaptor<TleUploadRequest> captor = ArgumentCaptor.forClass(TleUploadRequest.class);
        inOrder.verify(camel, times(1)).send(captor.capture());
        inOrder.verify(responseSupport, times(1)).sendAsJson(resp, toJson, uiResponse);
        inOrder.verifyNoMoreInteractions();

        assertEquals(TLE_SOURCE, captor.getValue().getTleSource());
        assertEquals(TLE_TEXT, captor.getValue().getTleText());
        assertEquals(USER, captor.getValue().getUploader());
        long diff = System.currentTimeMillis() - captor.getValue().getTimestamp();
        assertTrue("diff=" + diff, diff >= 0 && diff <= 1000L * 30);
        assertEquals(SATELLITE, captor.getValue().getSatellite());
    }

    /**
     * Test method for
     * {@link eu.estcube.webserver.tle.upload.TleSubmitServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}
     * .
     */
    @Test
    public void testDoPostWithCamelException() throws Exception {
        when(req.getParameter(TleSubmitServlet.POST_PARAMETER_TLE_SOURCE)).thenReturn(TLE_SOURCE);
        when(req.getParameter(TleSubmitServlet.POST_PARAMETER_TLE_TEXT)).thenReturn(TLE_TEXT);
        when(req.getParameter(TleSubmitServlet.POST_PARAMETER_TLE_SATELLITE_ID)).thenReturn(SATELLITE);
        when(req.getSession()).thenReturn(session);
        when(uis.getUserInfo(session)).thenReturn(userInfo);
        when(userInfo.getUsername()).thenReturn(USER);
        when(camel.send(any(TleUploadRequest.class))).thenThrow(exception);
        try {
            servlet.doPost(req, resp);
            fail("Exception expected");
        } catch (Exception e) {
            assertEquals(ServletException.class, e.getClass());
            assertEquals(exception, e.getCause());
        }

        inOrder.verify(req, times(1)).getParameter(TleSubmitServlet.POST_PARAMETER_TLE_SOURCE);
        inOrder.verify(req, times(1)).getParameter(TleSubmitServlet.POST_PARAMETER_TLE_TEXT);
        inOrder.verify(req, times(1)).getSession();
        inOrder.verify(uis, times(1)).getUserInfo(session);
        inOrder.verify(userInfo, times(1)).getUsername();
        ArgumentCaptor<TleUploadRequest> captor = ArgumentCaptor.forClass(TleUploadRequest.class);
        inOrder.verify(camel, times(1)).send(captor.capture());
        inOrder.verifyNoMoreInteractions();

        assertEquals(TLE_SOURCE, captor.getValue().getTleSource());
        assertEquals(TLE_TEXT, captor.getValue().getTleText());
        assertEquals(USER, captor.getValue().getUploader());
        long diff = System.currentTimeMillis() - captor.getValue().getTimestamp();
        assertTrue("diff=" + diff, diff >= 0 && diff <= 1000L * 30);
        assertEquals(SATELLITE, captor.getValue().getSatellite());
    }

    /**
     * Test method for
     * {@link eu.estcube.webserver.tle.upload.TleSubmitServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}
     * .
     */
    @Test
    public void testDoPostWithResponseException() throws Exception {
        when(req.getParameter(TleSubmitServlet.POST_PARAMETER_TLE_SOURCE)).thenReturn(TLE_SOURCE);
        when(req.getParameter(TleSubmitServlet.POST_PARAMETER_TLE_TEXT)).thenReturn(TLE_TEXT);
        when(req.getParameter(TleSubmitServlet.POST_PARAMETER_TLE_SATELLITE_ID)).thenReturn(SATELLITE);
        when(req.getSession()).thenReturn(session);
        when(uis.getUserInfo(session)).thenReturn(userInfo);
        when(userInfo.getUsername()).thenReturn(USER);
        when(camel.send(any(TleUploadRequest.class))).thenReturn(uiResponse);
        doThrow(exception).when(responseSupport).sendAsJson(resp, toJson, uiResponse);
        try {
            servlet.doPost(req, resp);
            fail("Exception expected");
        } catch (Exception e) {
            assertEquals(ServletException.class, e.getClass());
            assertEquals(exception, e.getCause());
        }

        inOrder.verify(req, times(1)).getParameter(TleSubmitServlet.POST_PARAMETER_TLE_SOURCE);
        inOrder.verify(req, times(1)).getParameter(TleSubmitServlet.POST_PARAMETER_TLE_TEXT);
        inOrder.verify(req, times(1)).getSession();
        inOrder.verify(uis, times(1)).getUserInfo(session);
        inOrder.verify(userInfo, times(1)).getUsername();
        ArgumentCaptor<TleUploadRequest> captor = ArgumentCaptor.forClass(TleUploadRequest.class);
        inOrder.verify(camel, times(1)).send(captor.capture());
        inOrder.verify(responseSupport, times(1)).sendAsJson(resp, toJson, uiResponse);
        inOrder.verifyNoMoreInteractions();

        assertEquals(TLE_SOURCE, captor.getValue().getTleSource());
        assertEquals(TLE_TEXT, captor.getValue().getTleText());
        assertEquals(USER, captor.getValue().getUploader());
        long diff = System.currentTimeMillis() - captor.getValue().getTimestamp();
        assertTrue("diff=" + diff, diff >= 0 && diff <= 1000L * 30);
        assertEquals(SATELLITE, captor.getValue().getSatellite());
    }
}
