package eu.estcube.webcamera;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.core.Binary;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.estcube.common.Naming;
import eu.estcube.domain.hbird.extension.ExctendedBinary;

/**
 *
 */
@Component
public class ImageMessageCreator {

    public static final String PARAMETER_NAME = "webcam.image";
    public static final String DESCRIPTION = "Image from ground station webcamera";

    @Value("${service.id}")
    protected String service;

    @Value("${gs.id}")
    protected String groundStationId;

    public Binary create(@Header(StandardArguments.TIMESTAMP) long timestamp,
            @Header(StandardArguments.TYPE) String type, @Body byte[] data) {
        // TODO: migrate to the hbird id.builder in the future
        String name = new StringBuilder().append(groundStationId).append(Naming.PARAMETER_NAME_SEPARATOR)
                .append(PARAMETER_NAME).toString();
        Binary result = new ExctendedBinary(service, name, DESCRIPTION, data, type);
        result.setTimestamp(timestamp);
        return result;
    }
}
