package eu.estcube.webcamera;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.hbird.exchange.core.Binary;
import org.junit.Before;
import org.junit.Test;

import eu.estcube.common.Naming;
import eu.estcube.domain.hbird.extension.ExctendedBinary;

/**
 *
 */
public class ImageMessageCreatorTest {

    private static final String GS_ID = "/ESTCUBE/GroundStations/ES5EC";

    private static final String SERVICE_ID = "webcam";

    private static final Long NOW = System.currentTimeMillis();

    private static final byte[] DATA = new byte[] { 0x04, 0x04, 0x04, 0x0C, 0x00, 0x01, 0x08, 0x05 };

    private static final String TYPE = "raw test data";

    private ImageMessageCreator imageMessageCreator;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        imageMessageCreator = new ImageMessageCreator();
        imageMessageCreator.groundStationId = GS_ID;
        imageMessageCreator.service = SERVICE_ID;
    }

    /**
     * Test method for
     * {@link eu.estcube.webcamera.ImageMessageCreator#create(long, java.lang.String, byte[])}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testCreate() {
        Binary binary = imageMessageCreator.create(NOW, TYPE, DATA);
        assertNotNull(binary);
        assertNotNull(binary.getID());
        assertEquals(
                new StringBuilder().append(GS_ID).append(Naming.PARAMETER_NAME_SEPARATOR)
                        .append(ImageMessageCreator.PARAMETER_NAME).toString(),
                binary.getName()); // TODO: migrate to the hbird id.builder in
                                   // the future
        assertEquals(NOW, new Long(binary.getTimestamp()));
        assertEquals(ImageMessageCreator.DESCRIPTION, binary.getDescription());
        assertEquals(SERVICE_ID, binary.getIssuedBy());
        assertTrue(Arrays.equals(DATA, binary.getRawData()));
        assertEquals(TYPE, ((ExctendedBinary) binary).getType());
    }
}
