package eu.estcube.camel.serial;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.apache.camel.Component;
import org.apache.camel.Consumer;
import org.apache.camel.Processor;
import org.apache.camel.Producer;
import org.apache.mina.core.future.CloseFuture;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.transport.serial.SerialAddress;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SerialEndpointTest {
    
    private static final String ENDPOINT_URI = "serial://dev/ttyS177";
    
    @Mock
    private Component component;


    @Mock
    private SerialAddress serialAddress;

    @Mock
    private SerialHandler serialHandler;

    @Mock
    private SessionFactory sessionFactory;
    
    @Mock
    private IoSession session1;
    
    @Mock
    private IoSession session2;
    
    @Mock
    private CloseFuture closeFuture;
    
    @Mock
    private Processor processor;
    
    private SerialEndpoint serialEndpoint;
    
    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        serialEndpoint = new SerialEndpoint(ENDPOINT_URI, component, serialAddress, serialHandler, sessionFactory);
        inOrder = inOrder(component, serialAddress, serialHandler, sessionFactory, session1, session2, closeFuture, processor);
        when(sessionFactory.openSession(serialAddress, serialHandler)).thenReturn(session1);
    }

    /**
     * Test method for {@link eu.estcube.camel.serial.SerialEndpoint#doStop()}.
     * @throws Exception 
     */
    @Test
    public void testDoStopNoSession() throws Exception {
        serialEndpoint.doStop();
        inOrder.verify(sessionFactory, times(1)).close();
        inOrder.verify(serialAddress, times(1)).getName();
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.camel.serial.SerialEndpoint#doStop()}.
     * @throws Exception 
     */
    @Test
    public void testDoStopSessionIsClosed() throws Exception {
        when(session1.isConnected()).thenReturn(false);
        serialEndpoint.getIoSession(); // to initialize session
        serialEndpoint.doStop();
        inOrder.verify(sessionFactory, times(1)).openSession(serialAddress, serialHandler);
        inOrder.verify(sessionFactory, times(1)).close();
        inOrder.verify(serialAddress, times(1)).getName();
        inOrder.verify(session1, times(1)).isConnected();
        inOrder.verifyNoMoreInteractions();
    }
    
    /**
     * Test method for {@link eu.estcube.camel.serial.SerialEndpoint#doStop()}.
     * @throws Exception 
     */
    @Test
    public void testDoStopSessionIsConnected() throws Exception {
        when(session1.isConnected()).thenReturn(true);
        when(session1.close(true)).thenReturn(closeFuture);
        serialEndpoint.getIoSession(); // to initialize session
        serialEndpoint.doStop();
        inOrder.verify(sessionFactory, times(1)).openSession(serialAddress, serialHandler);
        inOrder.verify(sessionFactory, times(1)).close();
        inOrder.verify(serialAddress, times(1)).getName();
        inOrder.verify(session1, times(1)).isConnected();
        inOrder.verify(session1, times(1)).close(true);
        inOrder.verify(closeFuture, times(1)).await();
        inOrder.verifyNoMoreInteractions();
    }
    
    /**
     * Test method for {@link eu.estcube.camel.serial.SerialEndpoint#createConsumer(org.apache.camel.Processor)}.
     * @throws Exception 
     */
    @Test
    public void testCreateConsumer() throws Exception {
        when(session1.isConnected()).thenReturn(true);
        
        Consumer sc1 = serialEndpoint.createConsumer(processor);
        Consumer sc2 = serialEndpoint.createConsumer(processor);
        assertNotNull(sc1);
        assertNotNull(sc2);
        assertNotSame(sc1, sc2);
        assertEquals(serialEndpoint, sc1.getEndpoint());
        assertEquals(serialEndpoint, sc2.getEndpoint());
        
        inOrder.verify(sessionFactory, times(1)).openSession(serialAddress, serialHandler);
        inOrder.verify(session1, times(1)).getId();
        ArgumentCaptor<SerialConsumer> captor = ArgumentCaptor.forClass(SerialConsumer.class);
        inOrder.verify(serialHandler, times(1)).addConsumer(captor.capture());
        inOrder.verify(session1, times(1)).isConnected();
        inOrder.verify(session1, times(1)).getId();
        inOrder.verify(serialHandler, times(1)).addConsumer(captor.capture());
        inOrder.verifyNoMoreInteractions();
        assertEquals(sc1, captor.getAllValues().get(0));
        assertEquals(sc2, captor.getAllValues().get(1));
    }

    /**
     * Test method for {@link eu.estcube.camel.serial.SerialEndpoint#createProducer()}.
     * @throws Exception 
     */
    @Test
    public void testCreateProducer() throws Exception {
        when(session1.isConnected()).thenReturn(true);
        
        Producer sp1 = serialEndpoint.createProducer(); 
        Producer sp2 = serialEndpoint.createProducer(); 
        assertNotNull(sp1);
        assertNotNull(sp2);
        assertNotSame(sp1, sp2);
        assertEquals(serialEndpoint, sp1.getEndpoint());
        assertEquals(serialEndpoint, sp2.getEndpoint());
        
        inOrder.verify(sessionFactory, times(1)).openSession(serialAddress, serialHandler);
        inOrder.verify(session1, times(1)).isConnected();
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.camel.serial.SerialEndpoint#isSingleton()}.
     */
    @Test
    public void testIsSingleton() {
        assertFalse(serialEndpoint.isSingleton());
    }

    /**
     * Test method for {@link eu.estcube.camel.serial.SerialEndpoint#getIoSession()}.
     * @throws Exception 
     */
    @Test
    public void testGetIoSession() throws Exception {
        assertEquals(session1, serialEndpoint.getIoSession());
        inOrder.verify(sessionFactory, times(1)).openSession(serialAddress, serialHandler);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.camel.serial.SerialEndpoint#getIoSession()}.
     * @throws Exception 
     */
    @Test
    public void testGetIoSessionSecondTime() throws Exception {
        when(session1.isConnected()).thenReturn(true);
        assertEquals(session1, serialEndpoint.getIoSession());
        assertEquals(session1, serialEndpoint.getIoSession());
        inOrder.verify(sessionFactory, times(1)).openSession(serialAddress, serialHandler);
        inOrder.verify(session1, times(1)).isConnected();
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.camel.serial.SerialEndpoint#getIoSession()}.
     * @throws Exception 
     */
    @Test
    public void testGetIoSessionWhenClosed() throws Exception {
        when(sessionFactory.openSession(serialAddress, serialHandler)).thenReturn(session1, session2);
        when(session1.isConnected()).thenReturn(false);
        assertEquals(session1, serialEndpoint.getIoSession());
        assertEquals(session2, serialEndpoint.getIoSession());
        inOrder.verify(sessionFactory, times(1)).openSession(serialAddress, serialHandler);
        inOrder.verify(session1, times(1)).isConnected();
        inOrder.verify(sessionFactory, times(1)).openSession(serialAddress, serialHandler);
        inOrder.verifyNoMoreInteractions();
    }
}
