package eu.estcube.domain.config;

import org.springframework.beans.factory.annotation.Value;

/**
 *
 */
public class GroundStationConfigurationBase extends ConfigurationBase {

    @Value("${gs.id}")
    protected String groundstationId;

    public String getGroundstationId() {
        return groundstationId;
    }
}
