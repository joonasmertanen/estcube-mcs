/** 
 *
 */
package eu.estcube.common.json;

import static org.junit.Assert.assertFalse;

import java.lang.reflect.Field;

import org.hbird.exchange.core.Entity;
import org.hbird.exchange.core.EntityInstance;
import org.hbird.exchange.core.Part;
import org.hbird.exchange.interfaces.IEntity;
import org.hbird.exchange.interfaces.IEntityInstance;
import org.hbird.exchange.interfaces.IPart;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.FieldAttributes;

/**
 *
 */
public class IPartParentExclusionStrategyTest {

    private IPartParentExclusionStrategy strategy;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        strategy = new IPartParentExclusionStrategy();
    }

    /**
     * Test method for
     * {@link eu.estcube.common.json.IPartParentExclusionStrategy#shouldSkipField(com.google.gson.FieldAttributes)}
     * .
     * 
     * @throws NoSuchFieldException
     * @throws SecurityException
     */
    @Test
    public void testShouldSkipField() throws Exception {
        FieldAttributes skip = new FieldAttributes(getField(Part.class, "isPartOf"));
        FieldAttributes accept1 = new FieldAttributes(getField(Part.class, "name"));
        FieldAttributes accept2 = new FieldAttributes(getField(Part.class, "ID"));
        FieldAttributes accept3 = new FieldAttributes(getField(Part.class, "description"));
        FieldAttributes accept4 = new FieldAttributes(getField(PartWithFriend.class, "friend"));
        FieldAttributes accept5 = new FieldAttributes(getField(WithParent.class,
                IPartParentExclusionStrategy.FIELD_NAME_PARENT));

        // assertTrue(strategy.shouldSkipField(skip));
        assertFalse(strategy.shouldSkipField(accept1));
        assertFalse(strategy.shouldSkipField(accept2));
        assertFalse(strategy.shouldSkipField(accept3));
        assertFalse(strategy.shouldSkipField(accept4));
        assertFalse(strategy.shouldSkipField(accept5));
    }

    /**
     * Test method for
     * {@link eu.estcube.common.json.IPartParentExclusionStrategy#shouldSkipClass(java.lang.Class)}
     * .
     */
    @Test
    public void testShouldSkipClass() {
        assertFalse(strategy.shouldSkipClass(Object.class));
        assertFalse(strategy.shouldSkipClass(String.class));
        assertFalse(strategy.shouldSkipClass(IPart.class));
        assertFalse(strategy.shouldSkipClass(Part.class));
        assertFalse(strategy.shouldSkipClass(IEntityInstance.class));
        assertFalse(strategy.shouldSkipClass(EntityInstance.class));
        assertFalse(strategy.shouldSkipClass(IEntity.class));
        assertFalse(strategy.shouldSkipClass(Entity.class));
    }

    static Field getField(Class<?> c, String name) {
        Field[] fields = c.getDeclaredFields();
        for (Field f : fields) {
            if (f.getName().equals(name)) {
                return f;
            }
        }
        Class<?> superClass = c.getSuperclass();
        return c == null ? null : getField(superClass, name);
    }

    @SuppressWarnings("serial")
    static class PartWithFriend extends Part {

        @SuppressWarnings("unused")
        private IPart friend;

        public PartWithFriend(String name, String description) {
            super(name, description);
        }
    }

    @SuppressWarnings("serial")
    static class WithParent extends Entity {

        @SuppressWarnings("unused")
        private IPart parent;

        public WithParent(String id, String issuedBy, String name, String description) {
            super(id, issuedBy, name, description);
        }
    }
}
