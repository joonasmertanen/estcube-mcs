/** 
 *
 */
package eu.estcube.common.json;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import org.hbird.exchange.interfaces.IPart;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.JsonNull;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ParentAbsoluteNameAppenderAdapterFactoryTest {

    private static final String PARENT_NAME = "PARENT/NAME";

    @Mock
    private IPart part;

    @Mock
    private IPart parent;

    private ParentAbsoluteNameAppenderAdapterFactory factory;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        factory = new ParentAbsoluteNameAppenderAdapterFactory();
        inOrder = inOrder(part, parent);
    }

    /**
     * Test method for
     * {@link eu.estcube.common.json.ParentAbsoluteNameAppenderAdapterFactory#toCustomValue(java.lang.Object)}
     * .
     */
    @Test
    public void testToCustomValue() {
        when(part.getIsPartOf()).thenReturn(PARENT_NAME);
        assertEquals(PARENT_NAME, factory.toCustomValue(part).getAsString());
        inOrder.verify(part, times(1)).getIsPartOf();
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.common.json.ParentAbsoluteNameAppenderAdapterFactory#toCustomValue(java.lang.Object)}
     * .
     */
    @Test
    public void testToCustomValueNoParent() {
        when(part.getIsPartOf()).thenReturn(null);
        assertEquals(JsonNull.INSTANCE, factory.toCustomValue(part));
        inOrder.verify(part, times(1)).getIsPartOf();
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.common.json.ParentAbsoluteNameAppenderAdapterFactory#toCustomValue(java.lang.Object)}
     * .
     */
    @Test(expected = NullPointerException.class)
    public void testToCustomValueNull() {
        factory.toCustomValue(null);
    }
}
