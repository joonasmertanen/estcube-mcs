/** 
 *
 */
package eu.estcube.common.hbird;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.inOrder;

import java.util.Map;

import org.hbird.exchange.core.EntityInstance;
import org.hbird.exchange.core.Metadata;
import org.hbird.exchange.interfaces.IEntityInstance;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class MetadataFactoryTest {

    private static final String ID = "/ESTCUBE/Satellites/ESTCube-1/beacon/raw";
    private static final String NAME = "Normal Mode Beacon Message";
    private static final String ISSUER = "sdc";
    private static final String META_ISSUER = "meta issuer";
    private static final String DESCRIPTION = "Entity description";

    private IEntityInstance entityInstance;

    @Mock
    private Map<String, Object> data;

    private MetadataFactory factory;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        entityInstance = new EntityInstance(ID, ISSUER, NAME, DESCRIPTION) {
            private static final long serialVersionUID = 5609191967812851132L;
        };
        factory = new MetadataFactory();
        inOrder = inOrder(data);
    }

    @Test
    public void testCreateMetadata() throws Exception {
        Metadata meta = factory.createMetadata(entityInstance, data, META_ISSUER);
        assertNotNull(meta);
        assertEquals(ID + "/" + Metadata.class.getSimpleName(), meta.getID());
        assertEquals(NAME + " " + Metadata.class.getSimpleName(), meta.getName());
        assertEquals(META_ISSUER, meta.getIssuedBy());
        assertTrue(entityInstance.getInstanceID().startsWith(ID + ":"));
        assertEquals(entityInstance.getInstanceID(), meta.applicableTo());
        assertEquals(entityInstance.getInstanceID(), meta.getSubject());
        assertTrue(meta.getInstanceID().startsWith(ID + "/" + Metadata.class.getSimpleName() + ":"));
        inOrder.verifyNoMoreInteractions();
    }
}
