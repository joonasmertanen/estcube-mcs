package eu.estcube.common;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.interfaces.IEntity;
import org.hbird.exchange.interfaces.IEntityInstance;
import org.springframework.stereotype.Component;

/**
 * Prepares {@link Exchange} containing {@link Named} object for injection to
 * the Camel routes. Copies values from {@link Named} object to {@link Message}
 * headers.
 * Values are mapped as:
 * <ul>
 * <li>{@link Named#getUuid()} to header {@link Headers#UUID}.</li>
 * <li>{@link Named#getName()} to header {@link Headers#NAME}.</li>
 * <li>{@link Named#getIssuedBy()} to header {@link Headers#ISSUED_BY}.</li>
 * <li>{@link Class#getSimpleName()} to header {@link Headers#CLASS}.</li>
 * <li>{@link Named#getType()} to header {@link Headers#TYPE}.</li>
 * <li>{@link Named#getDatasetidentifier()} to header
 * {@link Headers#DATA_SET_IDENTIFIER}.</li>
 * <li>{@link Named#getTimestamp()} to header {@link Headers#TIMESTAMP}.</li>
 * </ul>
 */
@Component
public class PrepareForInjection implements Processor {

    /** @{inheritDoc . */
    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        out.copyFrom(in);
        IEntity named = in.getBody(IEntity.class);
        out.setHeader(StandardArguments.ENTITY_ID, named.getID());
        out.setHeader(StandardArguments.NAME, named.getName());
        out.setHeader(StandardArguments.CLASS, named.getClass().getSimpleName());
        if (named instanceof IEntityInstance) {
            IEntityInstance issued = (IEntityInstance) named;
            out.setHeader(StandardArguments.ISSUED_BY, issued.getIssuedBy());
            out.setHeader(StandardArguments.TIMESTAMP, issued.getTimestamp());
        }
    }
}
