/** 
 *
 */
package eu.estcube.common.hbird;

import java.util.Map;

import org.hbird.exchange.core.Metadata;
import org.hbird.exchange.interfaces.IEntityInstance;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class MetadataFactory {

    public Metadata createMetadata(IEntityInstance entity, Map<String, Object> data, String issuedBy) {
        String name = String.format("%s %s", entity.getName(), Metadata.class.getSimpleName());
        String id = String.format("%s/%s", entity.getID(), Metadata.class.getSimpleName());
        Metadata meta = new Metadata(issuedBy, name, entity, data);
        meta.setEntityID(id);
        meta.setSubject(entity.getInstanceID());
        return meta;
    }
}
