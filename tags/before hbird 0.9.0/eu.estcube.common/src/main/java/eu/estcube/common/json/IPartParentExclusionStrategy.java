/** 
 *
 */
package eu.estcube.common.json;

import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.interfaces.IPart;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

/**
 * 
 */
public class IPartParentExclusionStrategy implements ExclusionStrategy {

    // TODO - 14.04.2013; kimmell - remove when StandardArguments#PARENT
    /**
     * @deprecated Use {@link StandardArguments#PARENT}
     */
    @Deprecated
    public static final String FIELD_NAME_PARENT = "parent";

    /** @{inheritDoc . */
    @Override
    public boolean shouldSkipField(FieldAttributes f) {
        // exclude
        return IPart.class.isAssignableFrom(f.getDeclaringClass()) && f.getDeclaredClass().equals(IPart.class)
                && FIELD_NAME_PARENT.equals(f.getName());
    }

    /** @{inheritDoc . */
    @Override
    public boolean shouldSkipClass(Class<?> clazz) {
        return false;
    }
}
