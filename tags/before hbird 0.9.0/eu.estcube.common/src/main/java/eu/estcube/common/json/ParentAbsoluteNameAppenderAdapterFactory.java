/** 
 *
 */
package eu.estcube.common.json;

import org.hbird.exchange.interfaces.IPart;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonPrimitive;

/**
 *
 */
public class ParentAbsoluteNameAppenderAdapterFactory extends CustomFieldAppenderAdapterFactory {

    /**
     * Creates new AbsoluteNameAppenderAdapterFactory.
     * 
     * @param baseClass
     * @param fieldName
     * @param override
     */
    public ParentAbsoluteNameAppenderAdapterFactory() {
        super(IPart.class, IPartParentExclusionStrategy.FIELD_NAME_PARENT, true);
    }

    /** @{inheritDoc . */
    @Override
    protected <T> JsonElement toCustomValue(T value) {
        IPart part = (IPart) value;
        String parent = part.getIsPartOf();
        return parent == null ? JsonNull.INSTANCE : new JsonPrimitive(parent);
    }
}
