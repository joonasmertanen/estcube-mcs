define([
    "config/config",
    "dojo/domReady!"
    ],

    function(Config) {

        return [
        	// Left, centre and right positioning is done in init.js
        	// Priority is applicable within a single container only
            // level 1
            { id: "system", parentId: "", priority: 1, label: "System", url: "/MCS", tooltip: "", icon: "/images/transmit.png", roles: Config.ROLES_ALL},
            { id: "satellites", parentId: "", priority: 2, label: "Satellites", url: "/MCS", tooltip: "Satellites" , roles: Config.ROLES_ALL},
            { id: "groundStations", parentId: "", priority: 3, label: "Ground Stations", url: "/MCS", tooltip: "Ground Stations", roles: Config.ROLES_ALL},
            { id: "clock", parentId: "", priority: 4, label: "CLOCK", url: "", tooltip: "" , roles: Config.ROLES_ALL},
            { id: "aosLosTimer", parentId: "", priority: 5, label: "AOSLOS", url: "", tooltip: "" , roles: Config.ROLES_ALL},
            { id: "settings", parentId: "", priority: 8, label: "Settings", url:"/MCS", tooltip:"Change settings", roles: Config.ROLES_ALL},
            { id: "bstream", parentId: "", priority: 6, label: "", url:"", tooltip:"Beacon Stream", iconClass: "speaker", roles: Config.ROLES_ALL},
            { id: "notifications", parentId: "", priority: 7, label:"", url:"", tooltip:"Notifications", iconClass: "speaker", roles: Config.ROLES_ALL},
	        { id: "user", parentId: "", priority: 9, label: "username", url:"", tooltip:"", roles: Config.ROLES_ALL},
            { id: "logout", parentId: "", priority: 10, label: "Logout", url: "LOGOUT", tooltip: "Logout user" , roles: Config.ROLES_ALL},

            // level 2: system
            { id: "dashboard", parentId: "system", priority: 1, label: "Dashboard", url: "DASHBOARD", tooltip: "MCS starting page" , roles: Config.ROLES_ALL},
            { parentId: "system", priority: 2, label: "separator", roles: Config.ROLES_ALL},
            { id: "map", parentId: "system", priority: 3, label: "Map", url: "MAP", tooltip: "Map view" , roles: Config.ROLES_ALL},
            { parentId: "system", priority: 4, label: "separator", roles: Config.ROLES_ALL},
            { id: "contacts", parentId: "system", priority: 5, label: "Contacts", url: "CONTACTS", tooltip: "Contacts" , roles: Config.ROLES_ALL},
            { id: "skyatglance", parentId: "system", priority: 6, label: "Sky At Glance", url: "SKYATGLANCE", tooltip: "Sky at glance", roles: Config.ROLES_ALL},
            { parentId: "system", priority: 7, label: "separator", roles: Config.ROLES_ALL},
            { id: "components", parentId: "system", priority: 8, label: "System Components", url: "SYSTEM_COMPONENTS", tooltip: "Status of system components" , roles: Config.ROLES_ALL},
            { id: "log", parentId: "system", priority: 9, label: "System Log", url: "SYSTEM_LOG", tooltip: "System log" , roles: Config.ROLES_ALL},
            { id: "messages", parentId: "system", priority: 10, label: "System Messages", url: "MESSAGES", tooltip: "System messages" , roles: Config.ROLES_ALL},
            { id: "diagnostics", parentId: "system", priority: 11, label: "UI Diagnostics", url: "DIAGNOSTICS", tooltip: "UI diagnostics" , roles: Config.ROLES_ALL},
            { parentId: "system", priority: 12, label: "separator", roles: Config.ROLES_ALL},
            { id: "debug", parentId: "system", priority: 13, label: "Debug", url: "DEBUG", tooltip: "Debug view" , roles: Config.ROLES_ALL},

            // level 2: satellites
            { id: "ESTCube-1", parentId: "satellites", priority: 2, label: "ESTCube-1", url: "/ESTCUBE/Satellites/ESTCube-1", tooltip: "Satellite ESTCube-1" , roles: Config.ROLES_ALL},
            { id: "ESTCube-1-FS", parentId: "satellites", priority: 2, label: "ESTCube-1-FS", url: "/ESTCUBE/Satellites/ESTCube-1-FS", tooltip: "Satellite ESTCube-1-FS" , roles: Config.ROLES_ALL},

            // level 2: ground stations
            { id: "ES5EC", parentId: "groundStations", priority: 3, label: "ES5EC", url: "/ESTCUBE/GroundStations/ES5EC", tooltip: "Ground Station ES5EC" , roles: Config.ROLES_ALL},

            // level 2: settings
            { id: "audio", parentId: "settings", priority: 1, label: "Audio", url: "AUDIO", tooltip: "Change audio settings" , roles: Config.ROLES_ALL},
            
            // level 3: satellites -> ESTCube-1
            { id: "ESTCube-1/dashboard", parentId: "ESTCube-1", priority: 1, label: "Dashboard", url: "ESTCube1_DASHBOARD", tooltip: "ESTCube-1 Dashboard" , roles: Config.ROLES_ALL},
            { parentId: "ESTCube-1", priority: 2, label: "separator", roles: Config.ROLES_ALL},
            { id: "ESTCube-1/TLE", parentId: "ESTCube-1", priority: 3, label: "TLE", url: "ESTCube1_TLE", tooltip: "Two Line Element Input" , roles: Config.ROLES_ALL},
            { parentId: "ESTCube-1", priority: 4, label: "separator", roles: Config.ROLES_ALL},
            { id: "ESTCube-1/beacons", parentId: "ESTCube-1", priority: 5, label: "View Beacons", url: "ESTCube1_BEACONS", tooltip: "Received Radio Beacons", roles: Config.ROLES_ALL},
            { id: "ESTCube-1/AddBeacons", parentId: "ESTCube-1", priority: 6, label: "Multi-Beacon Input", url: "ESTCube1_addbeacons", tooltip: "Upload csv radio beacons" , roles: Config.ROLES_ALL},
            { id: "ESTCube-1/beacon", parentId: "ESTCube-1", priority: 7, label: "Beacon Input", url: "ESTCube1_Beacon", tooltip: "Radio Beacon Input" , roles: Config.ROLES_ALL},
            { parentId: "ESTCube-1", priority: 8, label: "separator", roles: Config.ROLES_ALL},
            { id: "ESTCube-1/telemetry", parentId: "ESTCube-1", priority: 9, label: "View Telemetry", url: "ESTCube1_TELEMETRY", tooltip: "Satellite Telemetry" , roles: Config.ROLES_ALL},
            { id: "ESTCube-1/commanding", parentId: "ESTCube-1", priority: 10, label: "Send Commands", url: "ESTCube1_Commanding", tooltip: "Satellite Commanding" , roles: Config.ROLES_CAN_COMMAND },
            { id: "ESTCube-1/AX.25", parentId: "ESTCube-1", priority: 11, label: "Send Hex", url: "ESTCube1_AX25", tooltip: "AX.25 frame input", roles: Config.ROLES_CAN_COMMAND },
            
            // level 3: ground stations -> ES5EC
	  		{ id: "ES5EC/TNC", parentId: "ES5EC", priority: 1, label: "TNC", url: "ES5EC_TNC", tooltip: "Terminal Node Controller" , roles: Config.ROLES_ALL},
	  		{ parentId: "ES5EC", priority: 2, label: "separator", roles: Config.ROLES_ALL},
	  		{ id: "ES5EC/webcam", parentId: "ES5EC", priority: 3, label: "Webcam", url: "ES5EC_WEBCAM", tooltip: "Web Camera" , roles: Config.ROLES_ALL},
	  		{ id: "ES5EC/antenna", parentId: "ES5EC", priority: 4, label: "Antenna", url: "ANTENNA", tooltip: "Antenna visualisation" , roles: Config.ROLES_ALL},
	  		{ parentId: "ES5EC", priority: 5, label: "separator", roles: Config.ROLES_ALL},
	  		{ id: "ES5EC/weather", parentId: "ES5EC", priority: 6, label: "Weather", url: "ES5EC_WEATHER", tooltip: "Weather Information" , roles: Config.ROLES_ALL},
            
        ];
    }
);
