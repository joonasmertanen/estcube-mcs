require([
    "dojo/dom",
    "dojo/Deferred",
    "common/ConfigLoader",
    "common/Router",
    "dojo/cookie",
    "dojo/domReady!"
    ],

    function(dom, Deferred, ConfigLoader, router,Cookie) {

        function setupCommunication(num) {
            var deferred = new Deferred();
            require([
                "common/net/WebSocketProxy",
                ],

                function(Proxy) {
                    new Proxy();
                    deferred.resolve({ number: num, message: "Proxy loaded" });
                }
            );
            return deferred.promise;
        }

        function setupDiagnostics(num) {
            var deferred = new Deferred();
            require([
                "common/store/DiagnosticsStore",
                ],

                function(DiagnosticsStore) {
                    deferred.resolve({ number: num, message: "DiagnosticsStore loaded" });
                }
            );
            return deferred.promise;
        }

        function setupMessaging(num) {
            var deferred = new Deferred();
            require([
                "common/messages/MessageController",
                "common/store/SystemMessageStore",
                ],

                function(MessageController, SystemMessageStore) {
                    deferred.resolve({ number: num, message: "Messaging system loaded" });
                }
            );
            return deferred.promise;
        }

        function initRouter(num) {
            router.init(ConfigLoader.getConfig2(), dom.byId("content"));
            router.goToCurrent();
            return { number: num, message: "Router initialized" };
        }

        function setupMenu(num) {
            var deferred = new Deferred();
            require([
                "common/display/MenuBar",
                "common/store/PageStore"
                ],

                function(MenuBar, pageStore) {
                    new MenuBar({
                        store: pageStore,
                        parentDivId: "menu",
                        leftItems: ["system", "ESTCube-1", "ES5EC" ],
                        centerItems: ["aosLosTimer", "clock"],
                        rightItems: ["bstream","notifications","settings","user","logout"]
			    
                    });
                    deferred.resolve({ number: num, message: "Menu loaded" });
                }
            );
            return deferred.promise;
        }

        function setupMib(num) {
            var deferred = new Deferred();
            require([
                "common/store/MissionInformationStore",
                ],

                function(MissionInformationStore) {
                    deferred.resolve({ number: num, message: "MissionInformationStore loaded" });
		}
            );
            return deferred.promise;
        }

        function log(data) {
            console.log("[init] " + data.number + ": " + data.message);
            return data.number + 1;
        }
        function initSound(){
    		var mySound = null;
    		soundManager.setup({
    		  url: config.SOUND_URL_FLASH,
    		  flashVersion: 9, // optional: shiny features (default = 8)
    		  // optional: ignore Flash where possible, use 100% HTML5 mode
    		  preferFlash: false,
    		  onready: function() {
    			mySound = soundManager.createSound({
    				  id: config.SOUND_ID_STREAM,
    				  stream: true,
    				  url: config.SOUND_URL_STREAM,
    				  volume: (Cookie(config.COOKIE_STREAM_VOLUME)=== undefined)? 50 : Cookie(config.COOKIE_STREAM_VOLUME)
    				});
    			var notificationsVolume = (Cookie(config.COOKIE_NOTIFICATIONS_VOLUME)=== undefined)? 50 : Cookie(config.COOKIE_NOTIFICATIONS_VOLUME);
    			var AOSSound = soundManager.createSound({
    				  id: config.SOUND_ID_AOS,
    				  url: config.SOUND_URL_AOS,
    				  volume: notificationsVolume
    				});
    			var LOSSound = soundManager.createSound({
    				  id: config.SOUND_ID_LOS,
    				  url: config.SOUND_URL_LOS,
    				  volume: notificationsVolume
    				});
    			var AOSinOne = soundManager.createSound({
    				  id: config.SOUND_ID_AOSINONE,
    				  url: config.SOUND_URL_AOSINONE,
    				  volume: notificationsVolume
    				});
    			var LOSinOne = soundManager.createSound({
    				  id: config.SOUND_ID_LOSINONE,
    				  url: config.SOUND_URL_LOSINONE,
    				  volume: notificationsVolume
    				});
    			var AOSinFive = soundManager.createSound({
    				  id: config.SOUND_ID_AOSINFIVE,
    				  url: config.SOUND_URL_AOSINFIVE,
    				  volume: notificationsVolume
    				});
    			var LOSinFive = soundManager.createSound({
    				  id: config.SOUND_ID_LOSINFIVE,
    				  url: config.SOUND_URL_LOSINFIVE,
    				  volume: notificationsVolume
    				});

    			  
    			require(["dojo/cookie", "dojo/domReady!"],
    					function(Cookie){
    						
    						if(Cookie(config.COOKIE_NOTIFICATIONS_MUTE) === undefined){
    							Cookie(config.COOKIE_NOTIFICATIONS_MUTE, "false", { expires: 5 });
    						}
    						if(Cookie(config.COOKIE_STREAM_MUTE) === undefined){
    							Cookie(config.COOKIE_STREAM_MUTE, "false", { expires: 5 });
    						}
    					  	
    					  	mySound.play();
    					  	
    					  	if(Cookie(config.COOKIE_STREAM_MUTE)=="true"){
    					  		soundManager.toggleMute(config.SOUND_ID_STREAM);
    					  	}
    					  	
    					  	if(Cookie(config.COOKIE_NOTIFICATIONS_MUTE) == "true"){
    					  		
    					  		soundManager.toggleMute(config.SOUND_ID_AOS);
    	                    	soundManager.toggleMute(config.SOUND_ID_LOS);
    	                    	soundManager.toggleMute(config.SOUND_ID_AOSINONE);
    	                    	soundManager.toggleMute(config.SOUND_ID_LOSINONE);
    	                    	soundManager.toggleMute(config.SOUND_ID_AOSINFIVE);
    	                    	soundManager.toggleMute(config.SOUND_ID_LOSINFIVE);
    	                    	
    					  	}
    					  	
    					});
    			}
    		});
    		
        }
        
        ConfigLoader.load( packages, function() {

            // calling async loaders here synchronously
            // to make sure all components are initialized
            // in rigth order
            setupCommunication(1)       // [init] 1
                .then(log)
                .then(setupDiagnostics) // [init] 2
                .then(log)
                .then(setupMessaging)   // [init] 3
                .then(log)
                .then(initRouter)       // [init] 4
                .then(log)
                .then(setupMenu)        // [init] 5
                .then(log)
                .then(setupMib)         // [init] 6
                .then(log);
        });
        
        initSound();
        
        // Set window title
        //
        // Parameters contained in userInfo:
        // username, roles, serviceId, serviceVersion, host, port, class
        //
        // Other available parameters:
        // window.location, window.location.host, window.location.hostname, window.location.port
        
        function setTitle( uInfo ) {
            window.document.title = "" + uInfo.serviceId;   // Set the browser window/tab title
        }
        
        require(["common/store/GetUserInformation"], function(GetUserInformation) {
           GetUserInformation(setTitle); 
        });
});
