define([
    "dojo/_base/declare", 
    "dojo/_base/lang", 
    "dojo/_base/array", 
    "dojo/aspect", 
    "dojo/dom-class", 
    "dgrid/OnDemandGrid", 
    "dgrid/util/misc", 
    "./ContentProvider", 
    "common/store/ParameterStore", 
    "common/formatter/DateFormatter", 
    "common/store/SubSystemView",
    ], 
    function(declare, Lang, Arrays, Aspect, DomClass, Grid, Misc, ContentProvider, ParameterStore, DateFormatter,SubSystemView) {
        return declare(ContentProvider, {        
            grid: null,
            store: ParameterStore,
            lookupStore: ParameterStore,          
            getContent: function(args) {              
                declare.safeMixin(this, args);      
                this.grid = new Grid({                
                    id: this.id,
                    columns: this.columns || {
                        timestamp: {
                            label: "Timestamp",
                            field: "timestamp",
                            formatter: DateFormatter,
                            className: "field-telemetry-provider-timestamp",
                        },
                        name: {
                            label: "Name",
                            className: "field-telemetry-provider-description"
                        },
                        description: {
                            label: "Description",
                            className: "field-telemetry-provider-description"
                        },
                        value: {
                            label: "Value",
                            className: "field-telemetry-provider-value",
                        },
                        unit: {
                            label: "Unit",
                            className: "field-telemetry-provider-unit",
                        },
                    },
                    store: this.store,
                    
                    /*TODO 07.07.13, ivar.mahhonin - couldn't figure out for the better way,                     
                     *how to pass two objects for the function, 
                     *where first argument is incoming automatically and second must be set.
                     */
                    
                    query: Lang.hitch(this,function(message) {
                    return SubSystemView(message,this);}),
                    
                });
                              
               setTimeout(Lang.hitch(this, function() {
                    this.grid.set("showHeader", true), this.grid.set("sort", "timestamp", true)
                }), 1);         
                return this.grid;
            },
            
        });
    });