define([
    "dojo/store/Memory",
    "dojo/store/Observable",
    "config/config",
    "./DataHandler",
    "./StoreMonitor",
    ],

    function(Memory, Observable, Config, DataHandler, StoreMonitor) {

		// Messages: {id, timestamp, satellite_id, lat, lon}
        var channel = Config.WEBSOCKET_ORBITAL_PREDICTIONS;
        var storeId = "id";

        var store = new Observable(new Memory({ idProperty: storeId }));
        new StoreMonitor({ store: store, storeName: "OrbitalPredictionsStore" });
        
        // XXX: Everything below is testing code - remove when sure that map works 
        function CreateInterpolator(points) {
            var degree = points.length - 1;
            
            return function(x) {
                var y = 0;
                
                for(var i = 0; i <= degree; i++) {
                    var term = points[i].y;
                    
                    for(var j = 0; j <= degree; j++) {
                        if(j != i) {
                            term *= (x - points[j].x) / (points[i].x - points[j].x); 
                        }
                    }
                    
                    y += term;
                }
                
                return y;
            };
        }
        
        // Can also do: parametric curves, x = x(t), y = y(t), but for this purpose doesn't really matter
        var corePoints = [{ x: 26.7, y: 59 }, { x: 29, y: 65 }, { x: 31, y: 63 }, { x: 35, y: 67 }];
        var interpolator = CreateInterpolator(corePoints);
        
        var data = [];
        var pointCount = 50;
        var duration = 60000;
        
        var delay = duration / pointCount;
        var messageDelay = 500;
        var startupDelay = 10;
        
        for(var id = 0; id < pointCount; id++) {     
            var x = 26.7 + id * (35 - 26.7) / pointCount;
            
            msg = {
                    id: id,
                    timestamp: Date.now() + delay * id,
                    satellite_id: "ESTCube-1",
                    lat: interpolator(x),
                    lon: x
            }
            
            data.push(msg);
        }
        
        // (c) Taken from http://jsfromhell.com/array/shuffle
        function shuffle(o) { //v1.0
            for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
            return o;
        };
      
        shuffle(data);
        
        setTimeout(function() {
            var i = 0;
            var timer = setInterval(function() {
                if(i >= pointCount) {
                    clearTimeout(timer);
                } else {
                    store.put(data[i]);
                    i++;
                }
            }, messageDelay)
        }, startupDelay)

        // TODO: Enable
        /*var handler = new DataHandler({ channels: [channel], callback: function(message, channel) {
        	store.put(message)
        }});*/

        return store;
    }
);