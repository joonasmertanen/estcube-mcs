define([
        "dojo/_base/declare", 
        "dojo/dom-construct", 
        "dojo/dom-attr",
		"dijit/layout/ContentPane", 
		"./ContentProvider", 
		],

/**
 * When creating WebCam content provider, you can set image size with setting
 * "width" argument. If you dont set the argument, default size will be chosen.
 */

	function(declare, DomConstruct, DomAttr, ContentPane, ContentProvider) {
		return declare(ContentProvider, {
			getContent : function() {
				var img = DomConstruct.create("img", {
					alt : "Webcam image",
					src : this.initialImage
				});
				img.style.width = this.width;
				var pane = new ContentPane({
					content : img
				});
				this.store.query({
					name : this.imageId
				}).observe(function(value, removedFrom, insertedInto) {
					if (!/image\//.test(value.type)) {
						// no type set; return
						return;
					}
					dojo.attr(img, {
						src : "data:" + value.type + ";base64," + value.rawData,
						alt : value.name
					});
				}, true);
				return pane;
			},
	
		});
	});