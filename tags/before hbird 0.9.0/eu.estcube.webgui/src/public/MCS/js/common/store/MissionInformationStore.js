define([
    "dojo/store/Memory",
    "dojo/_base/array",
    "dojo/store/Observable",
    "config/config",
    "dojo/request",
    "./StoreMonitor",
    "common/net/IDEncoder"
    ],

    function(Memory, Arrays, Observable, Config, Request, StoreMonitor, IDEncoder) {
        var store = new Observable(new Memory({ idProperty: "ID" }));
        new StoreMonitor({ store: store, storeName: "MissionInformationStore" });

        // UserInfo
        Request.get( "/user", {handleAs: "json"} ).then(
        	function(data) {
        		data.class = "UserInfo";   // add class key to userInfo
        		store.put(data);
        	}
        );
        
        Request.get(Config.URL_CATALOGUE_SATELLITES, {handleAs: "json"})
               .then(function(data) {
                         Arrays.forEach(data, function(row) {store.put(row);});
                     });

        Request.get(Config.URL_CATALOGUE_GROUND_STATIONS, {handleAs: "json"})
                .then(function(data) {
                    Arrays.forEach(data, function(row) { store.put(row); });
                    
                    loadContactEvents(store);
                });

        function loadContactEvents(store) {
            Arrays.forEach(store.query({ class: "GroundStation" }), function(gs) {
                Request.get(Config.URL_CATALOGUE_CONTACTS + IDEncoder(gs.ID), {handleAs: "json"}).then(function(data) {
                    Arrays.forEach(data, function(row) {
                        // XXX - 03.05.2013, kimmell - fix this
                        var id = row.groundStationId + row.satelliteId + row.orbitNumber;
                        row.ID = id;
                        store.put(row);
                    });
                });
            });
        }


        setInterval(function() {
            loadContactEvents(store);
        }, Config.CONTACT_UPDATE_INTERVAL);

        return store;
    }
);