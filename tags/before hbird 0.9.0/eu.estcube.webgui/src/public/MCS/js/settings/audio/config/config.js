define([
        "dojo/domReady!"
        ],

        function(ready) {
            return {
                routes: {
                    AUDIO: {
                        path: "settings/audio",
                        defaults: {
                            controller: "Audio/AudioController",
                            method: "index",
                        }
                    },
                },

                AUDIO: {
                }

            };
        }
    );