define([
    "dojo/_base/declare",
    "common/Controller",
    "./AudioView",
    ],

    function(declare, Controller, AudioView) {
        var s = declare([Controller], {

            constructor: function() {
                this.view = new AudioView();
            },

            index: function(params) {
                this.placeWidget(this.view);
            },

        });
        return new s();
    }
);