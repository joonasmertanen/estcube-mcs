define([
    "dojo/_base/declare",
    "common/Controller",
    "./AX25View",
    ],

    function(declare, Controller, AX25View) {
        var s = declare([Controller], {

            constructor: function() {
                this.view = new AX25View();
            },

            index: function(params) {
                this.placeWidget(this.view);
            },

        });
        return new s();
    }
);