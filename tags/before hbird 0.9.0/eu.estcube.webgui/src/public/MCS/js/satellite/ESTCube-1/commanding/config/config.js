define([
    "dojo/domReady!"
    ],

    function(ready) {
        return {
            routes: {
                ESTCube1_Commanding: {
                    path: "ESTCube-1/commanding",
                    defaults: {
                        controller: "ESTCube-1.commanding/CommandingController",
                        method: "index",
                    },
                    roles: config.ROLES_CAN_COMMAND
                },
            },
            COMMANDING: {
                SEND_COMMAND_URL: "/sendCommand",
                GET_COMMANDS_URL: "/getCommands",
                GET_COMMAND_ARGUMENTS_URL: "/getCommandArguments",
                DEFAULT_GS: "06",
                DEFAULT_CDHS_SOURCE: "06",
                DEFAULT_CDHS_BLOCK_INDEX: "0",
            }

        };
    }
);
