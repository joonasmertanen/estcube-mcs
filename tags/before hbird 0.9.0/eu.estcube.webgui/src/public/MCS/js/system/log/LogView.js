define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dijit/Dialog",
    "dgrid/OnDemandGrid",
    "config/config",
    "common/formatter/DateFormatter",
    "common/store/SystemLogStore",
    "common/display/ObjectToDiv",
    ],

    function(declare, Lang, DomConstruct, DomClass, Dialog, Grid, config, DateFormatter, store, ObjectToDiv) {

        return declare([], {

            constructor: function(args) {
                this.grid = new Grid({
                    store: store,
                    columns: {
                        timestamp: { label: "Timestamp", formatter: DateFormatter, className: "field-timestamp", },
                        issuedBy: { label: "Component Name", className: "field-issuedBy", },
                        level: { label: "Level", className: "field-level", renderCell: function(object, value, node, options) {
                                node.innerHTML = value;
                                if (value == "TRACE" || value == "DEBUG" || value == "INFO") {
                                    DomClass.add(node, "value-ok");
                                }
                                else if (value == "WARN") {
                                    DomClass.add(node, "value-warning");
                                }
                                else {
                                    DomClass.add(node, "value-error");
                                }
                        }},
                        value: { label: "Message", className: "field-value", },
                    }
                });
                this.grid.set("sort", "timestamp", true);
                DomClass.add(this.grid.domNode, "fill");

                var dialog = new Dialog({
                    title: "Log Entry Details",
                    style: "width: 800px; background-color: #ffffff",
                });


                this.grid.on("dblclick", Lang.hitch(this, function(event) {
                    var source = event.srcElement;
                    var row = this.grid.row(source);
                    if (row) {
                        dialog.set("content", ObjectToDiv.toDiv(row.data, {
//                            title: "Details",
                            titleClass: "details-title",
                            labelClass: "details-label",
                            valueClass: "details-value",
                            rows: {
                                timestamp: { label: "Timestamp", formatter: DateFormatter },
                                issuedBy: { label: "Component Name", formatter: function(item) {
                                    return item ? "Unknown" : item;
                                }},
                                level: { label: "Level", },
                                value: { label: "Message", },
                                logger: { label: "Logger", },
                                thread: { label: "Thread", },
                                throwable: { label: "Exception", },
                            },

                        }));
                        dialog.show();
                    }
                }));

                this.grid.startup();

                // XXX - workaround for grid titles not visible bug
                setTimeout(Lang.hitch(this, function() { this.grid.set("showHeader", true) }), 500);
            },

            placeAt: function(container) {
                DomConstruct.place(this.grid.domNode, container);
            },

        });
    }
);
