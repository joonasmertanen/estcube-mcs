define([
    "dojo/_base/declare",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dgrid/OnDemandGrid",
    "dijit/layout/TabContainer",
    "dijit/layout/ContentPane",
    "config/config",
    "common/store/DiagnosticsStore",
    "common/formatter/DateFormatter",
    "common/formatter/StatusFormatter",
   ],

    function(declare, DomClass, DomConstruct, Grid, TabContainer, ContentPane, Config, DiagnosticsStore, DateFormatter, StatusFormatter) {

        return declare([], {

            constructor: function(args) {
                var storeGrid = new Grid({
                    store: DiagnosticsStore,
                    query: { category: "store" },
                    columns: {
                        storeName: { label: "Store name", className: "field-name", },
                        timestamp: { label: "Last change", className: "field-timestamp", formatter: DateFormatter, },
                        storeSize: { label: "Size", className: "field-value", },
                        add: { label: "Add", className: "field-value", },
                        update: { label: "Update", className: "field-value", },
                        remove: { label: "Remove", className: "field-value", },
                    },
                });
                storeGrid.set("sort", "storeName", false);

                var socketGrid = new Grid({
                    store: DiagnosticsStore,
                    query: { category: "socket" },
                    columns: {
                        channel: { label: "Channel name", className: "field-name", },
                        timestamp: { label: "Last status change", className: "field-timestamp", formatter: DateFormatter, },
                        status: { label: "Status", className: "field-value", },
                        lastMessage: { label: "Last message", className: "field-timestamp", formatter: DateFormatter, },
                        in: { label: "Messages in", className: "field-value", },
                        out: { label: "Messages out", className: "field-value", },
                        source: { label: "Source", className: "field-value", },
                        error: { label: "Description", className: "field-value", },
                    },
                });
                socketGrid.set("sort", "channel", false);

                var xhrGrid = new Grid({
                    store: DiagnosticsStore,
                    query: { category: "xhr" },
                    columns: {
                        url: { label: "Request URL", className: "field-name", },
                        method: { label: "Method", className: "field-value" },
                        status: { label: "Status", className: "field-value", renderCell: StatusFormatter, },
                        out: { label: "Requests out", className: "field-value", },
                        lastRequest: { label: "Last Request", className: "field-timestamp", formatter: DateFormatter, },
                        success: { label: "Successful Requests", className: "field-value" },
                        lastSuccess: { label: "Last Success", className: "field-timestamp", formatter: DateFormatter, },
                        failure: { label: "Failed Requests", className: "field-value" },
                        lastFailure: { label: "Last Failure", className: "field-timestamp", formatter: DateFormatter, },
                    },
                });
                xhrGrid.set("sort", "method", false);
                xhrGrid.set("sort", "url", false);

                this.tabContainer = new TabContainer({ "class": "fill" });
                this.tabContainer.addChild(this.prepareGrid(storeGrid, "Stores"));
                this.tabContainer.addChild(this.prepareGrid(socketGrid, "Sockets"));
                this.tabContainer.addChild(this.prepareGrid(xhrGrid, "Ajax Requests"));
            },

            placeAt: function(container) {
                this.tabContainer.placeAt(container);
                this.tabContainer.startup();
            },

            prepareGrid: function(grid, title) {
                DomClass.add(grid.domNode, "fill");
                var gridPlaceholder = DomConstruct.create("div", { "class": "fill" });
                DomConstruct.place(grid.domNode, gridPlaceholder);

                var contentPane = new ContentPane({ title: title, content: gridPlaceholder });
                // XXX - 14.03.2013, kimmell - fix for dgrid header not visible bug
                contentPane.on("show", function() {
                    grid.set("showHeader", true);
                });

                return contentPane;
            },

        });
    }
);
