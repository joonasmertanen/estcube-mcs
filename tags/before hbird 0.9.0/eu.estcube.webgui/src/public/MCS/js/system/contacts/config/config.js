define([
    "dojo/domReady!"
    ],

    function(ready) {
        return {
            routes: {
                CONTACTS: {
                    path: "system/contacts",
                    defaults: {
                        controller: "Contacts/ContactsController",
                        method: "index",
                    }
                },
            },

            CONTACTS: {
            }

        };
    }
);
