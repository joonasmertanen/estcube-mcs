define([
    "dojo/dom",
    "dojo/domReady!"
    ],

    function(dom) {
        return {
            routes: {
                ES5EC_TNC: {
                    path: "ES5EC/TNC",
                    defaults: {
                        controller: "ES5ECTnc/TncController",
                        method: "index",
                    }
                },
            },

            ES5EC_TNC: {
            }

        };
    }
);
