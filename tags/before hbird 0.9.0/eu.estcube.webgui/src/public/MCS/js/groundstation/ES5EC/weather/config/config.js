define([
    "dojo/domReady!"
    ],

    function(ready) {
        return {
            routes: {
                ES5EC_WEATHER: {
                    path: "ES5EC/weather",
                    defaults: {
                        controller: "ES5ECWeather/WeatherController",
                        method: "index",
                    }
                },
            },

            ES5EC_WEATHER: {
                numberOfColumns: 3,
            }

        };
    }
);
