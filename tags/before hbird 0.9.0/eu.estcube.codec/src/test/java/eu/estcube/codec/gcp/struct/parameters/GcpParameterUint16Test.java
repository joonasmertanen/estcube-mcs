package eu.estcube.codec.gcp.struct.parameters;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilderFactory;

import org.hbird.exchange.core.Parameter;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

public class GcpParameterUint16Test {

    private final GcpParameterUint16 p = new GcpParameterUint16("a", "b", "c", false);
    private final GcpParameterUint16 a = new GcpParameterUint16("a", "b", "c", true);

    @Test
    public void GcpParameterBit() throws Exception {

        String xml = "<param><name>test</name><description>a</description><unit>b</unit></param>";
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                .parse(new InputSource(new StringReader(xml)));

        GcpParameterUint16 s = new GcpParameterUint16((Element) doc.getElementsByTagName("param").item(0));
        assertEquals("test", s.getName());
        assertEquals("a", s.getDescription());
        assertEquals("b", s.getUnit());
    }

    @Test
    public void getLength() {
        assertEquals(16, p.getLength());
    }

    @Test
    public void toValueStr() {
        assertEquals(65535, (int) p.toValue("65535")); // max
        assertEquals(0, (int) p.toValue("0")); // min
        assertEquals(15, (int) p.toValue("15")); // rnd

        assertEquals(32768, (int) p.toValue("0x8000")); // min
        assertEquals(32767, (int) p.toValue("0x7FFF")); // min
        assertEquals(65535, (int) p.toValue("0xFFFF")); // max

        try {
            p.toValue("65536"); // >max
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        try {
            p.toValue("-1"); // <min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }
    }

    @Test
    public void toBytes() {

        assertArrayEquals(new byte[] { 0, 0 }, p.toBytes(0)); // min
        assertArrayEquals(new byte[] { -1, -1 }, p.toBytes(65535)); // max
        assertArrayEquals(new byte[] { -128, 0 }, p.toBytes(32768)); // misc
        assertArrayEquals(new byte[] { 127, -1 }, p.toBytes(32767)); // misc

        try {
            p.toBytes(-1); // <min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        try {
            p.toBytes(65536); // >min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        assertArrayEquals(new byte[] { 0, 0 }, a.toBytes(0)); // min
        assertArrayEquals(new byte[] { -1, -1 }, a.toBytes(65535)); // max
        assertArrayEquals(new byte[] { 0, -128 }, a.toBytes(32768)); // misc
        assertArrayEquals(new byte[] { -1, 127 }, a.toBytes(32767)); // misc

        try {
            a.toBytes(-1); // <min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        try {
            a.toBytes(65536); // >min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

    }

    @Test
    public void toValueByte() {

        assertEquals(65535, (int) p.toValue(new byte[] { -1, -1 })); // max
        assertEquals(32768, (int) p.toValue(new byte[] { -128, 0 })); // misc
        assertEquals(32767, (int) p.toValue(new byte[] { 127, -1 })); // misc
        assertEquals(0, (int) p.toValue(new byte[] { 0, 0 })); // 0

        assertEquals(65535, (int) a.toValue(new byte[] { -1, -1 })); // max
        assertEquals(32768, (int) a.toValue(new byte[] { 0, -128 })); // misc
        assertEquals(32767, (int) a.toValue(new byte[] { -1, 127 })); // misc
        assertEquals(0, (int) a.toValue(new byte[] { 0, 0 })); // 0

        assertEquals(43690, (int) p.toValue(new byte[] { -86, -86 }));
        assertEquals(43529, (int) p.toValue(new byte[] { -86, 9 }));
        assertEquals(2474, (int) a.toValue(new byte[] { -86, 9 }));

        try {
            p.toValue(new byte[] { 0, 0, 0 }); // more bytes
            fail("Must throw RuntimeException");
        } catch (RuntimeException e) {
        }

        try {
            p.toValue(new byte[] { 0 }); // less bytes
            fail("Must throw RuntimeException");
        } catch (RuntimeException e) {
        }
    }

    @Test
    public void getIssued() {
        Parameter parameter = p.getIssued("pref.", new byte[] { -128, 0 });
        assertEquals("a", parameter.getName());
        assertEquals("pref.a", parameter.getID());
        assertEquals("b", parameter.getDescription());
        assertEquals(null, parameter.getIssuedBy());
        assertEquals(32768, parameter.getValue());
        assertEquals("c", parameter.getUnit());
    }

    @Test
    public void getValueClass() {
        assertEquals(Integer.class, p.getValueClass());
    }
}
