package eu.estcube.codec.gcp.struct.parameters;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilderFactory;

import org.hbird.exchange.core.Parameter;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

public class GcpParameterFloatTest {
    private final GcpParameterFloat p = new GcpParameterFloat("a", "b", "c");

    @Test
    public void GcpParameterBit() throws Exception {

        String xml = "<param><name>test</name><description>a</description><unit>b</unit></param>";
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                .parse(new InputSource(new StringReader(xml)));

        GcpParameterFloat s = new GcpParameterFloat((Element) doc.getElementsByTagName("param").item(0));
        assertEquals("test", s.getName());
        assertEquals("a", s.getDescription());
        assertEquals("b", s.getUnit());
    }

    @Test
    public void getLength() {
        assertEquals(4 * 8, p.getLength());
    }

    @Test
    public void toValueStr() {
        assertEquals(11.123f, p.toValue("11.123"), 0.00001); // max
        assertEquals(0, p.toValue("0"), 0.00001); // 0
        assertEquals(-1.123f, p.toValue("-1.123"), 0.00001); // rnd
        assertEquals(-128.123f, p.toValue("-128.123"), 0.00001); // min

        assertEquals(11.123f, p.toValue("0x4131F7CF"), 0.00001);
    }

    @Test
    public void toBytes() {
        assertArrayEquals(new byte[] { 65, 49, -9, -49 }, p.toBytes(11.123)); // min
    }

    @Test
    public void toValueByte() {
        assertEquals(11.123, p.toValue(new byte[] { 65, 49, -9, -49 }), 0.00001); // min

        try {
            p.toValue(new byte[] { 0, 0, 0 }); // more bytes
            fail("Must throw RuntimeException");
        } catch (RuntimeException e) {
        }
    }

    @Test
    public void getIssued() {
        Parameter parameter = p.getIssued("pref.", new byte[] { 65, 49, -9, -49 });
        assertEquals("a", parameter.getName());
        assertEquals("pref.a", parameter.getID());
        assertEquals("b", parameter.getDescription());
        assertEquals(null, parameter.getIssuedBy());
        assertEquals(11.123, (Float) parameter.getValue(), 0.00001);
        assertEquals("c", parameter.getUnit());
    }

    @Test
    public void getValueClass() {
        assertEquals(Float.class, p.getValueClass());
    }
}
