package eu.estcube.codec.gcp.struct.parameters;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilderFactory;

import org.hbird.exchange.core.Parameter;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

public class GcpParameterInt16Test {
    private final GcpParameterInt16 p = new GcpParameterInt16("a", "b", "c", false);
    private final GcpParameterInt16 a = new GcpParameterInt16("a", "b", "c", true);

    @Test
    public void GcpParameterBit() throws Exception {

        String xml = "<param><name>test</name><description>a</description><unit>b</unit><type little_endian=\"true\">uint16</type></param>";
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                .parse(new InputSource(new StringReader(xml)));

        GcpParameterInt16 s = new GcpParameterInt16((Element) doc.getElementsByTagName("param").item(0));
        assertEquals("test", s.getName());
        assertEquals("a", s.getDescription());
        assertEquals("b", s.getUnit());
        assertEquals(true, s.getIsLittleEndian());
    }

    @Test
    public void getLength() {
        assertEquals(16, p.getLength());
    }

    @Test
    public void toValueStr() {
        assertEquals(32767, (short) p.toValue("32767")); // max
        assertEquals(0, (short) p.toValue("0")); // 0
        assertEquals(-1, (short) p.toValue("-1")); // rnd
        assertEquals(-32768, (short) p.toValue("-32768")); // min

        assertEquals(-32768, (short) p.toValue("0x8000")); // min
        assertEquals(32767, (short) p.toValue("0x7FFF")); // min
        assertEquals(-1, (short) p.toValue("0xFFFF")); // max

        try {
            p.toValue("32768"); // >max
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        try {
            p.toValue("-32769"); // <min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }
    }

    @Test
    public void toBytes() {

        assertArrayEquals(new byte[] { -128, 0 }, p.toBytes(-32768)); // min
        assertArrayEquals(new byte[] { 127, -1 }, p.toBytes(32767)); // max
        assertArrayEquals(new byte[] { 0, 0 }, p.toBytes(0)); // 0
        try {
            p.toBytes(-32769); // <min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        try {
            p.toBytes(32768); // >max
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        assertArrayEquals(new byte[] { 0, -128 }, a.toBytes(-32768)); // min
        assertArrayEquals(new byte[] { -1, 127 }, a.toBytes(32767)); // max
        assertArrayEquals(new byte[] { 0, 0 }, a.toBytes(0)); // 0
        try {
            a.toBytes(-32769); // <min
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

        try {
            a.toBytes(32768); // >max
            fail("Must throw NumberFormatException");
        } catch (NumberFormatException e) {
        }

    }

    @Test
    public void toValueByte() {

        assertEquals(-32768, (short) p.toValue(new byte[] { -128, 0 })); // min
        assertEquals(32767, (short) p.toValue(new byte[] { 127, -1 })); // min
        assertEquals(0, (short) p.toValue(new byte[] { 0, 0 })); // 0

        assertEquals(-32768, (short) a.toValue(new byte[] { 0, -128 })); // min
        assertEquals(32767, (short) a.toValue(new byte[] { -1, 127 })); // max
        assertEquals(0, (short) a.toValue(new byte[] { 0, 0 })); // 0

        assertEquals(-21846, (short) p.toValue(new byte[] { -86, -86 }));
        assertEquals(-22007, (short) p.toValue(new byte[] { -86, 9 }));
        assertEquals(2474, (short) a.toValue(new byte[] { -86, 9 }));

        try {
            p.toValue(new byte[] { 0, 0, 0 }); // more bytes
            fail("Must throw RuntimeException");
        } catch (RuntimeException e) {
        }

        try {
            p.toValue(new byte[] { 0 }); // less bytes
            fail("Must throw RuntimeException");
        } catch (RuntimeException e) {
        }
    }

    @Test
    public void getIssued() {
        Parameter parameter = p.getIssued("pref.", new byte[] { -128, 0 });
        assertEquals("a", parameter.getName());
        assertEquals("pref.a", parameter.getID());
        assertEquals("b", parameter.getDescription());
        assertEquals(null, parameter.getIssuedBy());
        assertEquals((short) -32768, parameter.getValue());
        assertEquals("c", parameter.getUnit());
    }

    @Test
    public void getValueClass() {
        assertEquals(Short.class, p.getValueClass());
    }
}
