package eu.estcube.codec.ax25.impl;

import static org.junit.Assert.assertEquals;

import java.nio.ByteBuffer;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import eu.estcube.common.ByteUtil;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;

public class Ax25UIFrameEncoderTest {
	private Ax25UIFrameEncoder encoder;
	private IoSession ioSession;
	private ProtocolEncoderOutput out;

	@Before
	public void setUp() throws Exception {
		encoder = new Ax25UIFrameEncoder();
		ioSession = Mockito.mock(IoSession.class);
		out = Mockito.mock(ProtocolEncoderOutput.class);

	}

	@Test
	public void testEncodeIoSessionObjectProtocolEncoderOutput() throws Exception {
		Ax25UIFrame message = new Ax25UIFrame();
		message.setDestAddr(new byte[] { 1, 2, 3, 4, 5, 6, 7 });
		message.setSrcAddr(new byte[] { 8, 9, 10, 11, 12, 13, 14 });
		message.setCtrl((byte) 15);
		message.setPid((byte) 16);
		message.setInfo(new byte[] { (byte) 0x00, (byte) 0x00 });
		message.setFcs(new byte[] { 19, 20 });

		InOrder io = Mockito.inOrder(out);
		Mockito.doAnswer(new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				ByteBuffer b = (ByteBuffer) invocation.getArguments()[0];
				assertEquals(0, b.position());
				assertEquals(22, b.limit());
				return null;
			}

		}).when(out).write(Mockito.any(Object.class));

		encoder.encode(ioSession, message, out);

		io.verify(out).write(Mockito.any(Object.class));
		io.verify(out).flush();
		io.verifyNoMoreInteractions();
	}

	@Test
	public void testFlush() {
		ByteBuffer buf = ByteBuffer.allocate(1);

		encoder.bitBuffer = 1; // 001[00000]

		// No unaligned bits
		encoder.numBits = 0;
		encoder.flush(buf);
		assertEquals(0, buf.position());

		// 5 missing bits
		encoder.numBits = 3;
		encoder.flush(buf);
		assertEquals(1, buf.position());
		assertEquals(32, buf.get(0));

	}

	@Test
	public void testReset() {
		encoder.bitBuffer = 9;
		encoder.numBits = 15;
		encoder.numSequentialOnes = 11;
		encoder.reset();
		assertEquals(0, encoder.bitBuffer);
		assertEquals(0, encoder.numBits);
		assertEquals(0, encoder.numSequentialOnes);
	}

	@Test
	public void testEncodeAx25UIFrameByteBuffer() {
		ByteBuffer buf = ByteBuffer.allocate(44);

		Ax25UIFrame frame1 = new Ax25UIFrame();
		frame1.setDestAddr(new byte[] { 1, 2, 3, 4, 5, 6, 7 });
		frame1.setSrcAddr(new byte[] { 8, 9, 10, 11, 12, 13, 14 });
		frame1.setCtrl((byte) 15);
		frame1.setPid((byte) 16);
		frame1.setInfo(new byte[] { (byte) 0x00, (byte) 0x00 });
		frame1.setFcs(new byte[] { 19, 20 });

		// Ax25UIFrame frame2 = new Ax25UIFrame();
		// frame2.setDestAddr(new byte[] { 1, 2, 3, 4, 5, 6, 7 });
		// frame2.setSrcAddr(new byte[] { 8, 9, 10, 11, 12, 13, 14 });
		// frame2.setCtrl((byte) 15);
		// frame2.setPid((byte) 16);
		// frame2.setInfo(new byte[] { 17, 18 });
		// frame2.setFcs(new byte[] { 19, 20 });

		encoder.encode(frame1, buf);
		encoder.encode(frame1, buf);

		assertEquals(44, buf.position());

		// Flag
		assertEquals((byte) 0x7E, buf.get(0));

		// Dest address
		assertEquals(ByteUtil.mirror((byte) 1), buf.get(1));
		assertEquals(ByteUtil.mirror((byte) 2), buf.get(2));
		assertEquals(ByteUtil.mirror((byte) 3), buf.get(3));
		assertEquals(ByteUtil.mirror((byte) 4), buf.get(4));
		assertEquals(ByteUtil.mirror((byte) 5), buf.get(5));
		assertEquals(ByteUtil.mirror((byte) 6), buf.get(6));
		assertEquals(ByteUtil.mirror((byte) 7), buf.get(7));

		// Src address
		assertEquals(ByteUtil.mirror((byte) 8), buf.get(8));
		assertEquals(ByteUtil.mirror((byte) 9), buf.get(9));
		assertEquals(ByteUtil.mirror((byte) 10), buf.get(10));
		assertEquals(ByteUtil.mirror((byte) 11), buf.get(11));
		assertEquals(ByteUtil.mirror((byte) 12), buf.get(12));
		assertEquals(ByteUtil.mirror((byte) 13), buf.get(13));
		assertEquals(ByteUtil.mirror((byte) 14), buf.get(14));

		// Ctrl
		assertEquals(ByteUtil.mirror((byte) 15), buf.get(15));

		// PID
		assertEquals(ByteUtil.mirror((byte) 16), buf.get(16));

		// Info
		assertEquals(ByteUtil.mirror((byte) 0), buf.get(17));
		assertEquals(ByteUtil.mirror((byte) 0), buf.get(18));

		// FCS
		assertEquals((byte) 19, buf.get(19));
		assertEquals((byte) 20, buf.get(20));

		// Flag
		assertEquals((byte) 0x7E, buf.get(21));

		// Flag
		assertEquals((byte) 0x7E, buf.get(22));

		// Dest address
		assertEquals(ByteUtil.mirror((byte) 1), buf.get(23));
		assertEquals(ByteUtil.mirror((byte) 2), buf.get(24));
		assertEquals(ByteUtil.mirror((byte) 3), buf.get(25));
		assertEquals(ByteUtil.mirror((byte) 4), buf.get(26));
		assertEquals(ByteUtil.mirror((byte) 5), buf.get(27));
		assertEquals(ByteUtil.mirror((byte) 6), buf.get(28));
		assertEquals(ByteUtil.mirror((byte) 7), buf.get(29));

		// Src address
		assertEquals(ByteUtil.mirror((byte) 8), buf.get(30));
		assertEquals(ByteUtil.mirror((byte) 9), buf.get(31));
		assertEquals(ByteUtil.mirror((byte) 10), buf.get(32));
		assertEquals(ByteUtil.mirror((byte) 11), buf.get(33));
		assertEquals(ByteUtil.mirror((byte) 12), buf.get(34));
		assertEquals(ByteUtil.mirror((byte) 13), buf.get(35));
		assertEquals(ByteUtil.mirror((byte) 14), buf.get(36));

		// Ctrl
		assertEquals(ByteUtil.mirror((byte) 15), buf.get(37));

		// PID
		assertEquals(ByteUtil.mirror((byte) 16), buf.get(38));

		// Info
		assertEquals(ByteUtil.mirror((byte) 0), buf.get(39));
		assertEquals(ByteUtil.mirror((byte) 0), buf.get(40));

		// FCS
		assertEquals((byte) 19, buf.get(41));
		assertEquals((byte) 20, buf.get(42));

		// Flag
		assertEquals((byte) 0x7E, buf.get(43));
	}

	@Test
	public void testEncodeByteArrayBooleanBooleanByteBuffer() {
		ByteBuffer buf = ByteBuffer.allocate(4);

		// Just verify that all bytes are added, values are checked
		// already in the byte level method
		byte[] array = new byte[] { 5, 6 };

		encoder.encode(array, false, false, buf);

		assertEquals(2, buf.position());
		assertEquals(5, buf.get(0));
		assertEquals(6, buf.get(1));

	}

	@Test
	public void testEncodeByteBooleanBooleanByteBuffer() {
		ByteBuffer b = ByteBuffer.allocate(4);

		encoder.encode((byte) 2, true, true, b);
		assertEquals(1, b.position());
		assertEquals((byte) 64, b.get(0)); // bit order is LSB, therefore value
											// changes

		encoder.encode((byte) 2, false, true, b);
		assertEquals(2, b.position());
		assertEquals((byte) 2, b.get(1));

		// No bit stuffing
		encoder.encode((byte) 0xFF, true, false, b);
		assertEquals(3, b.position());
		assertEquals(0, encoder.numBits);
		assertEquals(-1, b.get(2));

		// Bit stuffing
		encoder.encode((byte) 0xFF, true, true, b);
		assertEquals(4, b.position());
		assertEquals(1, encoder.numBits); // 1 left over as 1 bit stuffed
		assertEquals((byte) Integer.parseInt("11111011", 2), b.get(3)); // 11111011
	}

	@Test
	public void testTxBit() {
		ByteBuffer b = ByteBuffer.allocate(4);

		// Insert AX.25 separator, no bit stuffing
		encoder.txBit(0, false, b);
		encoder.txBit(1, false, b);
		encoder.txBit(1, false, b);
		encoder.txBit(1, false, b);
		encoder.txBit(1, false, b);
		encoder.txBit(1, false, b);
		encoder.txBit(1, false, b);
		encoder.txBit(0, false, b);

		assertEquals(0, encoder.numSequentialOnes);
		assertEquals(0, encoder.numBits);
		assertEquals(1, b.position());
		assertEquals("01111110", ByteUtil.toBinaryString(b.get(0), 8));

		// Insert data matching the separator, now bit stuffing should be done
		encoder.txBit(0, true, b);
		encoder.txBit(1, true, b);
		encoder.txBit(1, true, b);
		encoder.txBit(1, true, b);
		encoder.txBit(1, true, b);
		encoder.txBit(1, true, b);
		encoder.txBit(1, true, b);
		encoder.txBit(0, true, b);

		assertEquals(0, encoder.numSequentialOnes);
		assertEquals(1, encoder.numBits);
		assertEquals(2, b.position());

		assertEquals("01111101", ByteUtil.toBinaryString(b.get(1), 8));

		// Insert just 5 ones in a row, stuffing should happen and two bits
		// "left over"
		encoder.txBit(1, true, b);
		encoder.txBit(1, true, b);
		encoder.txBit(1, true, b);
		encoder.txBit(1, true, b);
		encoder.txBit(1, true, b);
		encoder.txBit(0, true, b);
		encoder.txBit(0, true, b);
		encoder.txBit(0, true, b);

		assertEquals(0, encoder.numSequentialOnes);
		assertEquals(2, encoder.numBits);
		assertEquals(3, b.position());

		assertEquals("01111100", ByteUtil.toBinaryString(b.get(2), 8));

		// Insert add another 6 bits, byte should complete
		encoder.txBit(1, true, b);
		encoder.txBit(1, true, b);
		encoder.txBit(0, true, b);
		encoder.txBit(1, true, b);
		encoder.txBit(1, true, b);
		encoder.txBit(0, true, b);

		assertEquals(0, encoder.numSequentialOnes);
		assertEquals(0, encoder.numBits);
		assertEquals(4, b.position());

		assertEquals("00110110", ByteUtil.toBinaryString(b.get(3), 8));
	}

	@Test
	public void testAddByteToBuffer() {
		ByteBuffer b = ByteBuffer.allocate(5);

		encoder.addByteToBuffer(1, b);
		encoder.addByteToBuffer(140, b);
		encoder.addByteToBuffer(255, b);
		encoder.addByteToBuffer(256, b);
		encoder.addByteToBuffer(4195071, b);

		assertEquals(5, b.position());
		assertEquals(1, b.get(0));
		assertEquals((byte) 140, b.get(1));
		assertEquals((byte) 255, b.get(2));
		assertEquals(0, b.get(3));
		assertEquals((byte) 0xFF, b.get(4));

	}

}
