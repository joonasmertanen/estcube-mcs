package eu.estcube.codec.gcp.struct.parameters;

import java.nio.ByteBuffer;

import org.w3c.dom.Element;

public class GcpParameterUint8 extends GcpParameterNumberic {

    public final static long MIN_VALUE = 0;
    public final static long MAX_VALUE = 255;
    public final static long BYTES = 1;

    public GcpParameterUint8(String name, String description, String unit) {
        super(name, description, unit, false);
    }

    public GcpParameterUint8(Element e) {
        super(e);
    }

    @Override
    public int getLength() {
        return 8;
    }

    @Override
    public Short toValue(String input) {

        if (isHexString(input)) {
            return (Short) toValueFromHex(input);
        }

        short num = Short.parseShort(input);
        if (num < MIN_VALUE) {
            throw new NumberFormatException(getClass().getName() + " value must be bigger than " + MIN_VALUE);
        }
        if (num > MAX_VALUE) {
            throw new NumberFormatException(getClass().getName() + " value must be less than " + MAX_VALUE);
        }
        return (short) (num & 0x00ff);
    }

    @Override
    public byte[] toBytes(Object value) {

        ByteBuffer buffer = ByteBuffer.allocate(2);
        buffer.putShort(toValue(String.valueOf(value)));
        byte[] bytes = { buffer.get(1) };

        return bytes;
    }

    @Override
    public Short toValue(byte[] bytes) {

        if (bytes.length != BYTES) {
            throw new RuntimeException("byte[] length of the argument must be " + BYTES + "! Was " + bytes.length);
        }

        return (short) (bytes[0] & 0x00FF);
    }

    @Override
    public Class<?> getValueClass() {
        return Short.class;
    }

}
