package eu.estcube.codec.ax25.impl;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.common.ByteUtil;
import eu.estcube.domain.transport.ax25.Ax25FrameStatus;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;

/**
 * Decoder for AX.25 UI frames from a stream of bits.
 */
public final class Ax25UIFrameDecoder implements ProtocolDecoder {
    private final static Logger LOG = LoggerFactory.getLogger(Ax25UIFrameDecoder.class);

    private final byte[] buf;
    private int bufptr;
    private boolean rxstate;
    private int bitstream;
    private int bitbuf;
    private int bitsleft = 7;

    private Ax25FrameStatus frameStatus;

    public Ax25UIFrameDecoder() {
        buf = new byte[Ax25UIFrame.FRAME_CONTENTS_MAX_SIZE];
        bufptr = 0;
        rxstate = false;
        bitstream = 0;
        bitbuf = 0;
        frameStatus = new Ax25FrameStatus();
    }

    @Override
    public void decode(IoSession session, IoBuffer in, ProtocolDecoderOutput out) throws Exception {
        while (in.hasRemaining()) {
            byte b = in.get();
            Ax25UIFrame frame = decode(b);
            if (frame != null) {
                out.write(frame);
            }
        }
    }

    @Override
    public void dispose(IoSession session) throws Exception {
    }

    @Override
    public void finishDecode(IoSession session, ProtocolDecoderOutput out) throws Exception {
    }

    /**
     * Decodes one byte from a stream of bits. Returns a decoded frame or null,
     * if all the bits for a frame have not been received yet.
     */
    public Ax25UIFrame decode(byte b) {
        if (frameStatus == null) {
            frameStatus = new Ax25FrameStatus();
        }

        Ax25UIFrame frame = null;

        // LOG.debug("Byte received: " + Ax25Util.hex(b));
        for (int bit : ByteUtil.bitsLsbFirst(b)) {
            frame = rxbit(bit, frameStatus);
        }

        // Reset status if a frame was created
        if (frame != null) {
            frameStatus = new Ax25FrameStatus();
        }

        return frame;
    }

    // TODO: crc
    // TODO: address check: a component

    /**
     * This code adapted from net.tomhaynes.android.ModemLib.
     * <p>
     * http://code.google.com/p/cellbots/source/browse/trunk/android/java/
     * audioModemLib/src/net/tomhaynes/android/ModemLib
     * <p>
     * svn checkout http://cellbots.googlecode.com/svn/trunk/ cellbots-read-only
     */
    protected Ax25UIFrame rxbit(int bit, Ax25FrameStatus fs) {
        Ax25UIFrame f = null;

        // Bytes are passed lsb first
        // bitstream - bits as they come, stuffed
        // bitbuf - unstuffed

        bitstream <<= 1;
        bitstream |= bit;

        if ((bitstream & 0xFF) == Ax25UIFrame.FLAG) {
            if (rxstate && bufptr > 2) {
                f = createFrame(buf, bufptr, bitsleft > 0, fs);
            }
            rxstate = true;
            bufptr = 0;
            bitbuf = 0;
            bitsleft = 7;
            return f;
        }

        // 7 bits in a row - bad
        if ((bitstream & 0x7f) == 0x7f) {
            fs.setErrorUnstuffedBits(true);

            f = new Ax25UIFrame();
            f.setStatus(fs);

            rxstate = false;
            return f;
        }

        if (!rxstate) {
            return f;
        }

        if ((bitstream & 0x3f) == 0x3e) {
            return f; // stuffed bit - don't add
        }

        if (bit > 0) {
            bitbuf |= 0x100; // push
        }

        if (bitsleft == 0) {
            if (bufptr > Ax25UIFrame.FRAME_CONTENTS_MAX_SIZE - 1) {
                frameStatus.setErrorTooLong(true);
                return f; // rxstate not changed, continue receiving
            }
            buf[bufptr++] = (byte) (bitbuf >>> 1);
            bitbuf = 0;
            bitsleft = 7;
            return f;
        }

        bitbuf >>>= 1; // shift right unsigned
        bitsleft--;

        return f;
    }

    /**
     * Creates AX.25 frame from given source.
     * 
     * @param source
     *        The source to create from.
     * @param len
     *        The length of data or exclusive end index of data in source
     * @param unAligned
     *        If number of bits did not match full bytes.
     * @return
     */
    protected Ax25UIFrame createFrame(byte[] source, int len, boolean unAligned, Ax25FrameStatus fs) {
        Ax25UIFrame f = new Ax25UIFrame();
        f.setStatus(fs);

        fs.setErrorUnAligned(unAligned);
        fs.setErrorTooShort(len < Ax25UIFrame.NON_INFO_LENGTH);

        int ptr = 0;
        int fieldlens = 0;

        if (len >= (fieldlens += Ax25UIFrame.DEST_ADDR_LEN)) {
            f.setDestAddr(ArrayUtils.subarray(source, ptr, ptr += Ax25UIFrame.DEST_ADDR_LEN));
        }

        if (len >= (fieldlens += Ax25UIFrame.SRC_ADDR_LEN)) {
            f.setSrcAddr(ArrayUtils.subarray(source, ptr, ptr += Ax25UIFrame.SRC_ADDR_LEN));
        }

        if (len >= fieldlens++) {
            f.setCtrl(source[ptr++]);
        }

        if (len >= fieldlens++) {
            f.setPid(source[ptr++]);
        }

        int infolen = len - Ax25UIFrame.NON_INFO_LENGTH;
        if (infolen > 0) {
            f.setInfo(ArrayUtils.subarray(source, ptr, ptr + infolen));
        }

        int fcsLength = len - (Ax25UIFrame.NON_INFO_LENGTH - Ax25UIFrame.FCS_LEN);
        if (fcsLength > 0) {
            // According to AX.25, the FCS field (unlike the others) is
            // transferred using MS bit and not LS bit first,
            // therefore we have to change the bits in the octets / bytes
            byte[] fcs = ArrayUtils.subarray(source, len - Ax25UIFrame.FCS_LEN, len);
            for (int i = 0; i < fcs.length; i++) {
                fcs[i] = ByteUtil.mirror(fcs[i]);
            }
            f.setFcs(fcs);
        }

        // TODO: Check FCS and set error accordingly

        LOG.trace("Finished decoding " + f.toString());
        return f;
    }

    protected int crc(Ax25UIFrame f) {
        byte[] bytes = ArrayUtils.addAll(f.getDestAddr(), f.getSrcAddr());
        bytes = ArrayUtils.addAll(bytes, new byte[] { f.getCtrl() });
        bytes = ArrayUtils.addAll(bytes, new byte[] { f.getPid() });
        bytes = ArrayUtils.addAll(bytes, f.getFcs());
        int crc = ByteUtil.CRC16_CCITT_INIT;
        return ByteUtil.crc16CCITT(crc, bytes);
    }

}
