package eu.estcube.codec.radiobeacon.parser;

import org.hbird.exchange.core.EntityInstance;
import org.hbird.exchange.core.Parameter;

import eu.estcube.common.Constants;

public class RadionBeaconMessageParserCalibratedDouble extends RadioBeaconMessageParser {

    protected Integer startIndex;

    protected Integer endIndex;

    protected Double multiplier;

    protected Double add;

    protected Parameter parameterBase;

    private Double result;

    public RadionBeaconMessageParserCalibratedDouble(Integer startIndex, Integer length, Parameter parameterBase,
            Double multiplier, Double add) {
        this.startIndex = startIndex;
        this.endIndex = startIndex + length;
        this.parameterBase = parameterBase;
        this.multiplier = multiplier;
        this.add = add;
    }

    @Override
    public String parse(String message) {
        String part = getValidStringPart(message, this.startIndex, this.endIndex);
        if (part == null) {
            result = null;
            return null;
        }
        Integer rawValue = Integer.parseInt(cwToHex(part), Constants.HEX_BASE);

        result = ((double) rawValue) * multiplier + add;
        return String.valueOf(result);
    }

    @Override
    public EntityInstance parseToIssued(String message) {
        parse(message);
        if (result == null) {
            return null;
        }
        Parameter parameter = new Parameter(parameterBase);
        parameter.setValue(result);
        return parameter;
    }
}
