package eu.estcube.codec.radiobeacon.parsers;

import java.util.ArrayList;
import java.util.List;

import org.hbird.exchange.core.Label;

import eu.estcube.codec.radiobeacon.parser.RadioBeaconMessageParser;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParser2ComplementInteger;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserBits;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserCalibratedDouble;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserHex;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserInteger;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserTimestamp;

public class RadioBeaconParsersSafeMode extends RadioBeaconParsers {

    @Override
    public List<RadioBeaconMessageParser> getParsers() {
        List<RadioBeaconMessageParser> parsers = new ArrayList<RadioBeaconMessageParser>();

        parsers.add(new RadionBeaconMessageParserHex(6, 1, RadioBeaconParametersSafeMode.OPERATING_MODE));
        parsers.add(getTimestampParser());

        parsers.add(new RadionBeaconMessageParserInteger(14, 2, RadioBeaconParametersSafeMode.ERROR_CODE_1));
        parsers.add(new RadionBeaconMessageParserInteger(16, 2, RadioBeaconParametersSafeMode.ERROR_CODE_2));
        parsers.add(new RadionBeaconMessageParserInteger(18, 2, RadioBeaconParametersSafeMode.ERROR_CODE_3));
        parsers.add(new RadionBeaconMessageParserInteger(20, 4, RadioBeaconParametersSafeMode.TIME_IN_SAFE_MODE));
        parsers.add(new RadionBeaconMessageParserCalibratedDouble(24, 2,
                RadioBeaconParametersSafeMode.MAIN_BUS_VOLTAGE, 0.001240978485204 * 16, -0.001670625667674));

        parsers.add(new RadionBeaconMessageParserBits(26, 2, 0x80, 7, RadioBeaconParametersSafeMode.CDHS_PROCESSOR_A));
        parsers.add(new RadionBeaconMessageParserBits(26, 2, 0x40, 6, RadioBeaconParametersSafeMode.CDHS_PROCESSOR_B));
        parsers.add(new RadionBeaconMessageParserBits(26, 2, 0x20, 5, RadioBeaconParametersSafeMode.CDHS_BUS_SWITCH));
        parsers.add(new RadionBeaconMessageParserBits(26, 2, 0x10, 4,
                RadioBeaconParametersSafeMode.COM_3V3_VOLTAGE_LINE));
        parsers.add(new RadionBeaconMessageParserBits(26, 2, 0x8, 3, RadioBeaconParametersSafeMode.COM_5V_VOLTAGE_LINE));
        parsers.add(new RadionBeaconMessageParserBits(26, 2, 0x4, 2, RadioBeaconParametersSafeMode.PL_3V3_VOLTAGE_LINE));
        parsers.add(new RadionBeaconMessageParserBits(26, 2, 0x2, 1, RadioBeaconParametersSafeMode.PL_5V_VOLTAGE_LINE));
        parsers.add(new RadionBeaconMessageParserBits(26, 2, 0x1, 0, RadioBeaconParametersSafeMode.CAM_VOLTAGE));

        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x80, 7, RadioBeaconParametersSafeMode.ADCS_VOLTAGE));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x40, 6, RadioBeaconParametersSafeMode.BATTERY_A_CHARGING));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x20, 5,
                RadioBeaconParametersSafeMode.BATTERY_A_DISCHARGING));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x10, 4, RadioBeaconParametersSafeMode.BATTERY_B_CHARGING));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x8, 3,
                RadioBeaconParametersSafeMode.BATTERY_B_DISCHARGING));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x4, 2,
                RadioBeaconParametersSafeMode.SECONDARY_POWER_BUS_A));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x2, 1,
                RadioBeaconParametersSafeMode.SECONDARY_POWER_BUS_B));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x1, 0, RadioBeaconParametersSafeMode.V12_LINE_VOLTAGE));

        parsers.add(new RadionBeaconMessageParserBits(30, 2, 0x80, 7, RadioBeaconParametersSafeMode.REGULATOR_A_5V));
        parsers.add(new RadionBeaconMessageParserBits(30, 2, 0x40, 6, RadioBeaconParametersSafeMode.REGULATOR_B_5V));
        parsers.add(new RadionBeaconMessageParserBits(30, 2, 0x20, 5, RadioBeaconParametersSafeMode.LINE_VOLTAGE_5V));
        parsers.add(new RadionBeaconMessageParserBits(30, 2, 0x10, 4, RadioBeaconParametersSafeMode.REGULATOR_A_3V3));
        parsers.add(new RadionBeaconMessageParserBits(30, 2, 0x8, 3, RadioBeaconParametersSafeMode.REGULATOR_B_3V3));
        parsers.add(new RadionBeaconMessageParserBits(30, 2, 0x4, 2, RadioBeaconParametersSafeMode.LINE_VOLTAGE_3V3));
        parsers.add(new RadionBeaconMessageParserBits(30, 2, 0x2, 1, RadioBeaconParametersSafeMode.REGULATOR_A_12V));
        parsers.add(new RadionBeaconMessageParserBits(30, 2, 0x1, 0, RadioBeaconParametersSafeMode.REGULATOR_B_12V));

        parsers.add(new RadionBeaconMessageParserCalibratedDouble(32, 2,
                RadioBeaconParametersSafeMode.BATTERY_A_VOLTAGE,
                0.017686154075981, 0.003877355151542));
        parsers.add(new RadionBeaconMessageParserCalibratedDouble(34, 2,
                RadioBeaconParametersSafeMode.BATTERY_B_VOLTAGE,
                0.017645083640731, 0.013681971347675));
        parsers.add(new RadionBeaconMessageParserCalibratedDouble(36, 2,
                RadioBeaconParametersSafeMode.BATTERY_A_TEMPERATURE, 0.7139, -61.1111));
        parsers.add(new RadionBeaconMessageParserCalibratedDouble(38, 2,
                RadioBeaconParametersSafeMode.BATTERY_B_TEMPERATURE, 0.7139, -61.1111));
        parsers.add(new RadionBeaconMessageParser2ComplementInteger(40, 2,
                RadioBeaconParametersSafeMode.AVERAGE_POWER_BALANCE));
        parsers.add(new RadionBeaconMessageParserInteger(42, 1, RadioBeaconParametersSafeMode.FIRMWARE_VERSION_NUMBER));
        parsers.add(new RadionBeaconMessageParserInteger(43, 1, RadioBeaconParametersSafeMode.NUMBER_OF_CRASHES));
        parsers.add(new RadionBeaconMessageParser2ComplementInteger(44, 2,
                RadioBeaconParametersSafeMode.FORWARDED_RF_POWER));
        parsers.add(new RadionBeaconMessageParser2ComplementInteger(46, 2,
                RadioBeaconParametersSafeMode.REFLECTED_RF_POWER));
        parsers.add(new RadionBeaconMessageParser2ComplementInteger(48, 2,
                RadioBeaconParametersSafeMode.RECEIVED_SIGNAL_STRENGTH));
        return parsers;
    }

    @Override
    public Label getRadioBeacon() {
        return RadioBeaconParametersSafeMode.RADIO_BEACON;
    }

    @Override
    public String getEntityID() {
        return RadioBeaconParametersSafeMode.ENTITY_ID;
    }

    @Override
    public RadionBeaconMessageParserTimestamp getTimestampParser() {
        return new RadionBeaconMessageParserTimestamp(7, 7, RadioBeaconParametersSafeMode.TIMESTAMP);
    }
}
