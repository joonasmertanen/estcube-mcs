package eu.estcube.codec.radiobeacon.parser;

import org.hbird.exchange.core.Parameter;

public class RadionBeaconMessageParserTetherCurrent extends RadioBeaconMessageParser {

	protected Integer startIndex;

	protected Integer endIndex;

	protected Parameter parameterBase;

	protected Double result;

	public RadionBeaconMessageParserTetherCurrent(Integer startIndex, Integer length, Parameter parameterBase) {
		this.startIndex = startIndex;
		this.endIndex = startIndex + length;
		this.parameterBase = parameterBase;
	}

	public String parse(String hex) {
		String part = getValidStringPart(hex, this.startIndex, this.endIndex);
		if (part == null) {
			result = null;
			return null;
		}
		result = Double.parseDouble(parseHex(cwToHex(part))) * 5 / 255;
		return String.valueOf(result);
	}

	public Parameter parseToIssued(String message) {
		parse(message);
		if (result == null) {
			return null;
		}
		Parameter parameter = new Parameter(parameterBase);
		parameter.setValue(result);
		return parameter;
	}
}
