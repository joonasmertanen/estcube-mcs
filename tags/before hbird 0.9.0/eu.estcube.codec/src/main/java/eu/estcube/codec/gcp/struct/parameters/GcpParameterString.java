package eu.estcube.codec.gcp.struct.parameters;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;

import org.hbird.exchange.core.Label;
import org.w3c.dom.Element;

import eu.estcube.codec.gcp.struct.GcpParameter;

public class GcpParameterString extends GcpParameter implements GcpParameterMaxLength {

    public GcpParameterString(String name, String description) {
        super(name, description, "");
    }

    public GcpParameterString(Element e) {
        super(e);
    }

    @Override
    public int getLength() {
        return -1;
    }

    @Override
    public String toValue(String input) {
        return input;
    }

    @Override
    public byte[] toBytes(Object val) {
        String value = toValue((String) val);
        ByteBuffer buffer = ByteBuffer.allocate(value.length() * 2);
        for (int i = 0; i < value.length(); i++) {
            buffer.putChar(value.charAt(i));
        }
        return buffer.array();
    }

    @Override
    public Label getIssued(String entityID, byte[] value) {
        Label label = new Label(null, getName(), getDescription(), toValue(value));
        label.setEntityID(entityID + getName());
        return label;
    }

    @Override
    public String toValue(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(bytes.length);
        for (int i = 0; i < bytes.length; i++) {
            buffer.put(bytes[i]);
        }
        buffer.position(0);
        CharBuffer charBuffer = buffer.asCharBuffer();
        return charBuffer.toString();
    }

    @Override
    public Class<?> getValueClass() {
        return String.class;
    }

}
