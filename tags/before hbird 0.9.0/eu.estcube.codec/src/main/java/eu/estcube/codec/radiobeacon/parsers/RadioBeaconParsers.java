package eu.estcube.codec.radiobeacon.parsers;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.hbird.exchange.core.EntityInstance;
import org.hbird.exchange.core.Label;

import eu.estcube.codec.radiobeacon.parser.RadioBeaconMessageParser;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserTimestamp;

public abstract class RadioBeaconParsers {

    abstract public List<RadioBeaconMessageParser> getParsers();

    abstract public String getEntityID();

    abstract public Label getRadioBeacon();

    abstract public RadionBeaconMessageParserTimestamp getTimestampParser();

    public HashMap<String, EntityInstance> parse(String message, Long timestamp, String issuedBy) {

        HashMap<String, EntityInstance> list = new LinkedHashMap<String, EntityInstance>();

        Label beacon = getRadioBeacon();
        beacon.setValue(message);
        beacon.setIssuedBy(issuedBy);
        beacon.setTimestamp(timestamp);
        beacon.setEntityID(getEntityID() + beacon.getName());
        list.put(beacon.getName(), beacon);

        for (RadioBeaconMessageParser parser : getParsers()) {
            EntityInstance issued = parser.parseToIssued(message);
            if (issued != null) {
                issued.setIssuedBy(issuedBy);
                issued.setTimestamp(timestamp);
                issued.setEntityID(getEntityID() + issued.getName());

                list.put(issued.getName(), issued);
            }
        }

        return list;
    }

}
