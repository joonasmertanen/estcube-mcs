package eu.estcube.templating.placeholders;

/**
 * An enum defining all the usable placeholders for the templates.
 * 
 * @author deiwin
 */
public enum StringPlaceholder implements IPlaceholder<String> {

    COMMAND_VALUE("value"),
    RADIO_MODE("mode"),
    ISSUED_BY("issued_by");

    private final String name;

    StringPlaceholder(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public Class<String> getType() {
        return String.class;
    }

    @Override
    public Placeholder toPlaceholder() throws PlaceholderException {
        Placeholder result = null;

        switch (this) {
            case COMMAND_VALUE:
                result = Placeholder.COMMAND_VALUE;
                break;
            case RADIO_MODE:
                result = Placeholder.RADIO_MODE;
                break;
            case ISSUED_BY:
                result = Placeholder.ISSUED_BY;
                break;
        }

        // Don't add as a default case, because this way a compiler warning will
        // still be given if a switch statement is missing
        if (result == null) {
            throw new PlaceholderException("String placeholder \"" + this + "\" not defined in Placeholders.");
        }

        return result;
    }

}
