package eu.estcube.templating.placeholders;

import java.util.HashMap;
import java.util.Map;

/**
 * A map that links values to appropriate placeholders.
 * 
 * @author deiwin
 */
public class PlaceholderMap {

    private final Map<IPlaceholder<?>, Object> map;

    public PlaceholderMap() {
        map = new HashMap<IPlaceholder<?>, Object>();
    }

    public <T> T put(IPlaceholder<T> key, T value) throws PlaceholderException {
        if (key.getType().isAssignableFrom(value.getClass())) {
            return key.getType().cast(map.put(key, value));
        } else {
            throw new PlaceholderException("Expected a value of type: \"" + key.getType().getName()
                    + "\"for placeholder \"" + key + "\"");
        }
    }

    public <T> T get(IPlaceholder<T> key) throws PlaceholderException {
        T result = key.getType().cast(map.get(key));
        if (result == null) {
            throw new PlaceholderException("Value for placeholder \"" + key + "\" not defined.");
        } else {
            return result;
        }
    }

    @Override
    public PlaceholderMap clone() {
        PlaceholderMap result = new PlaceholderMap();

        result.map.putAll(map);

        return result;
    }

}
