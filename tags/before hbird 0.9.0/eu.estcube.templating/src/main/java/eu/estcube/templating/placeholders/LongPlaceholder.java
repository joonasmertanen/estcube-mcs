package eu.estcube.templating.placeholders;

public enum LongPlaceholder implements IPlaceholder<Long> {

    COMMAND_TIMESTAMP("timestamp"),
    COMMAND_HEADER_TIMESTAMP("start_timestamp"),
    COMMAND_TAIL_TIMESTAMP("end_timestamp");

    private final String name;

    private LongPlaceholder(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public Class<Long> getType() {
        return Long.class;
    }

    @Override
    public Placeholder toPlaceholder() throws PlaceholderException {
        Placeholder result = null;

        switch (this) {
            case COMMAND_TIMESTAMP:
                result = Placeholder.COMMAND_TIMESTAMP;
                break;
            case COMMAND_TAIL_TIMESTAMP:
                result = Placeholder.COMMAND_TAIL_TIMESTAMP;
                break;
            case COMMAND_HEADER_TIMESTAMP:
                result = Placeholder.COMMAND_HEADER_TIMESTAMP;
                break;
        }

        // Don't add as a default case, because this way a compiler warning will
        // still be given if a switch statement is missing
        if (result == null) {
            throw new PlaceholderException("Long placeholder \"" + this + "\" not defined in Placeholders.");
        }

        return result;
    }

}
