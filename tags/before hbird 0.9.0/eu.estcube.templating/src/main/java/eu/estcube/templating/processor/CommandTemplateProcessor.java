package eu.estcube.templating.processor;

import org.hbird.exchange.core.Command;

import eu.estcube.domain.hbird.extension.ExtendedNativeCommand;
import eu.estcube.templating.placeholders.LongPlaceholder;
import eu.estcube.templating.placeholders.PlaceholderException;
import eu.estcube.templating.placeholders.PlaceholderMap;
import eu.estcube.templating.placeholders.StringPlaceholder;
import eu.estcube.templating.template.CommandTemplate;
import eu.estcube.templating.template.provider.CommandTemplateProvider;

public class CommandTemplateProcessor extends TemplateProcessor<CommandTemplate, Command, CommandTemplateProvider> {

    @Override
    public Command process(CommandTemplate template, PlaceholderMap values) throws PlaceholderException {
        String dest = processString(template.getTargetId(), values);
        String command = processString(template.getValue(), values);
        String issuedBy = values.get(StringPlaceholder.ISSUED_BY);
        ExtendedNativeCommand ncmd = new ExtendedNativeCommand(issuedBy, dest, command);
        ncmd.setExecutionTime(values.get(LongPlaceholder.COMMAND_TIMESTAMP));
        return ncmd;
    }
}
