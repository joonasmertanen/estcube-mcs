package eu.estcube.templating.template;

import java.util.Collection;


/**
 * A bean holding all the contact commands: header, body and tail.
 * 
 * @author deiwin
 */
public class ContactTemplate implements Template {

    private Collection<CommandTemplate> header;
    private Collection<CommandTemplate> body;
    private Collection<CommandTemplate> tail;

    public ContactTemplate(Collection<CommandTemplate> header, Collection<CommandTemplate> body,
            Collection<CommandTemplate> tail) {
        this.header = header;
        this.body = body;
        this.tail = tail;
    }

    public Collection<CommandTemplate> getHeaderCommands() {
        return header;
    }

    public Collection<CommandTemplate> getBodyCommands() {
        return body;
    }

    public Collection<CommandTemplate> getTailCommands() {
        return tail;
    }

    @Override
    public String toString() {
        return "ContactTemplate [header=" + header + ", body=" + body
                + ", tail=" + tail + "]";
    }

}
