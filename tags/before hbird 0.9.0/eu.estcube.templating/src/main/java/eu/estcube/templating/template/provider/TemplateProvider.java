package eu.estcube.templating.template.provider;

import eu.estcube.templating.template.Template;

public interface TemplateProvider<T extends Template> {

    public T getTemplate();
}
