package eu.estcube.gs.tnc.codec;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class TncFrameCodecFactory implements ProtocolCodecFactory {

    private static final Logger LOG = LoggerFactory.getLogger(TncFrameCodecFactory.class);
    
    private ProtocolEncoder endoder = new TncFrameEncoder();
    
    private ProtocolDecoder decoder = new TncFrameDecoder();
    
    /** @{inheritDoc}. */
    @Override
    public ProtocolEncoder getEncoder(IoSession session) throws Exception {
        LOG.trace(" Calling FRAME encoder");
        return endoder;
    }

    /** @{inheritDoc}. */
    @Override
    public ProtocolDecoder getDecoder(IoSession session) throws Exception {
        LOG.trace(" Callnig FRAME decoder");
        return decoder;
    }
}
