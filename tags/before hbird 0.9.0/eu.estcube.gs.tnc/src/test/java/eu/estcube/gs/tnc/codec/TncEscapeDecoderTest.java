package eu.estcube.gs.tnc.codec;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.common.ByteUtil;

/**
 *
 */
 @RunWith(MockitoJUnitRunner.class)
public class TncEscapeDecoderTest {
     
    @Mock
    private IoSession session;

    @Mock
    private ProtocolDecoderOutput out;
    
    private IoBuffer in;
    
    private TncEscapeDecoder decoder;
    
    private InOrder inOrder;
     
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        decoder = new TncEscapeDecoder();
        inOrder = inOrder(session, out);
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncEscapeDecoder#decode(org.apache.mina.core.session.IoSession, org.apache.mina.core.buffer.IoBuffer, org.apache.mina.filter.codec.ProtocolDecoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testDecodeOneEscapedFend() throws Exception {
        in = IoBuffer.wrap(new byte[] { TncConstants.FESC, TncConstants.TFEND, });
        decoder.decode(session, in, out);
        ArgumentCaptor<IoBuffer> captor = ArgumentCaptor.forClass(IoBuffer.class);
        inOrder.verify(out, times(1)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        IoBuffer buffer = captor.getValue();
        byte[] result = new byte[buffer.limit()];
        buffer.get(result, 0, result.length);
        assertTrue(ByteUtil.toHexString(result), Arrays.equals(new byte[] { TncConstants.FEND }, result));
    }
    
    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncEscapeDecoder#decode(org.apache.mina.core.session.IoSession, org.apache.mina.core.buffer.IoBuffer, org.apache.mina.filter.codec.ProtocolDecoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testDecodeOneEscapedFesc() throws Exception {
        in = IoBuffer.wrap(new byte[] { TncConstants.FESC, TncConstants.TFESC, });
        decoder.decode(session, in, out);
        ArgumentCaptor<IoBuffer> captor = ArgumentCaptor.forClass(IoBuffer.class);
        inOrder.verify(out, times(1)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        IoBuffer buffer = captor.getValue();
        byte[] result = new byte[buffer.limit()];
        buffer.get(result, 0, result.length);
        assertTrue(ByteUtil.toHexString(result), Arrays.equals(new byte[] { TncConstants.FESC }, result));
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncEscapeDecoder#decode(org.apache.mina.core.session.IoSession, org.apache.mina.core.buffer.IoBuffer, org.apache.mina.filter.codec.ProtocolDecoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testDecodeOneEscapedByte() throws Exception {
        in = IoBuffer.wrap(new byte[] { TncConstants.FESC, 0x0C, });
        decoder.decode(session, in, out);
        ArgumentCaptor<IoBuffer> captor = ArgumentCaptor.forClass(IoBuffer.class);
        inOrder.verify(out, times(1)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        IoBuffer buffer = captor.getValue();
        byte[] result = new byte[buffer.limit()];
        buffer.get(result, 0, result.length);
        assertTrue(ByteUtil.toHexString(result), Arrays.equals(new byte[] { 0x0C }, result));
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncEscapeDecoder#decode(org.apache.mina.core.session.IoSession, org.apache.mina.core.buffer.IoBuffer, org.apache.mina.filter.codec.ProtocolDecoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testDecodeFescAtTheEnd() throws Exception {
        in = IoBuffer.wrap(new byte[] { 0x0C, TncConstants.FESC });
        decoder.decode(session, in, out);
        ArgumentCaptor<IoBuffer> captor = ArgumentCaptor.forClass(IoBuffer.class);
        inOrder.verify(out, times(1)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        IoBuffer buffer = captor.getValue();
        byte[] result = new byte[buffer.limit()];
        buffer.get(result, 0, result.length);
        assertTrue(ByteUtil.toHexString(result), Arrays.equals(new byte[] { 0x0C }, result));
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncEscapeDecoder#decode(org.apache.mina.core.session.IoSession, org.apache.mina.core.buffer.IoBuffer, org.apache.mina.filter.codec.ProtocolDecoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testDecode() throws Exception {
        in = IoBuffer.wrap(new byte[] { 0x0C, TncConstants.FESC, TncConstants.TFEND, 
                                        0x0D, TncConstants.FESC, TncConstants.TFESC,
                                        0x0E, TncConstants.FESC, 0x0F });
        decoder.decode(session, in, out);
        ArgumentCaptor<IoBuffer> captor = ArgumentCaptor.forClass(IoBuffer.class);
        inOrder.verify(out, times(1)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        IoBuffer buffer = captor.getValue();
        byte[] result = new byte[buffer.limit()];
        buffer.get(result, 0, result.length);
        assertTrue(ByteUtil.toHexString(result), Arrays.equals(new byte[] { 0x0C, TncConstants.FEND, 0x0D, TncConstants.FESC, 0x0E, 0x0F }, result));
    }
}
