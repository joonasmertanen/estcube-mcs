package eu.estcube.gs.radio;

import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.core.BusinessCard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import eu.estcube.common.PrepareForInjection;
import eu.estcube.common.Constants;
import eu.estcube.domain.hbird.extension.ExtendedNativeCommand;
import eu.estcube.gs.hamlib.DeviceType;
import eu.estcube.gs.hamlib.HamlibDriverConfiguration;
import eu.estcube.gs.hamlib.HamlibIO;

/**
 * 
 */
public class Radio extends RouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(Radio.class);

    @Value("#{config}")
    private HamlibDriverConfiguration config;
    
    @Autowired
    private PrepareForInjection preparator;

    @Override
    public void configure() {
        MDC.put(StandardArguments.ISSUED_BY, config.getServiceId());
        sendMetainfo();
        configureRoutes();
    }

    private void sendMetainfo() {
        // Outgoing to AMQ metadata topic
        from("timer://metaTimer?period=" + config.getTimerFireInterval())
            .pollEnrich("file:src/main/resources?fileName=metainfo.json&noop=true&idempotent=false")
            .to("log:metainfo?level=DEBUG")
            .to(Constants.AMQ_GS_META_SEND);
    }
   

    private void configureRoutes() {
        
        BusinessCard card = new BusinessCard(config.getServiceId(), config.getHeartBeatInterval());
        card.setDescription(String.format("Radio driver for %s device %s; version: %s", 
                config.getGroundstationId(), config.getDeviceName(), config.getServiceVersion()));
        
        // @formatter: off
            
        from("timer://heartbeat?fixedRate=true&period=" + config.getHeartBeatInterval())
            .bean(card, "touch")
            .process(preparator)
            .to("log:out?level=TRACE")
            .to("activemq:topic:hbird.monitoring");
        
        from(Constants.AMQ_GS_COMMANDS_RADIO)
            .log("log: Received message in AMQ queue")
            .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    String dataMessage = exchange.getIn().getBody(ExtendedNativeCommand.class).getCommandToExecute();
                    exchange.getIn().setBody(dataMessage);
                    LOG.info("Message value is "+dataMessage +" .Class is: " + dataMessage.getClass().getName());
                }
            })
            .choice()
                .when(header(Constants.HEADER_COMPONENT_ID)
                    .isEqualTo(config.getComponentId()))
                        .to(Constants.DIRECT_RIG_CTLD_INTERMEDIATE)
                .otherwise()
                    .to("log:WrongStation");
 

     /*   // Incoming from AMQ
        from(Constants.AMQ_GS_COMMANDS_RADIO).log("log: Received message in AMQ queue").choice()
            .when(header(Constants.HEADER_COMPONENT_ID).isEqualTo(config.getComponentId()))
            .to(Constants.DIRECT_RIG_CTLD_INTERMEDIATE).otherwise().log("log:WrongStation");*/

        // From AMQ to rigctld
        from(Constants.DIRECT_RIG_CTLD_INTERMEDIATE)
            .log("log: Sending from intermediate to radio")
            .doTry()
                .to(Constants.DIRECT_RIG_CTLD)
                .recipientList(header(Constants.HEADER_FORWARD))
            .endDoTry()
            .doCatch(Exception.class)
                .log("Log: Sending message from the AMQ receiving queue to the radio driver failed")
            .end();

        // Outgoing to AMQ
        from(Constants.DIRECT_SEND)
            .log("Log: Sending message from " + Constants.DIRECT_SEND + " to " + Constants.AMQ_GS_SEND)
            .to(Constants.AMQ_GS_SEND);

        // Outgoing to the radio
        String nettyRotctld = HamlibIO.getDeviceDriverUrl(DeviceType.RADIO, config);
        from(Constants.DIRECT_RIG_CTLD)
            .log("log: Sending message to rigctld")
            .doTry()
                .inOut(nettyRotctld)
                .process(new Processor() {
                    /**
                     * Designate an endpoint to which to forward the response from
                     * Hamlib. To enable forwarding, Constants.HEADER_FORWARD needs
                     * to be specified as the recipientList() when sending a message to
                     * Constants.DIRECT_RIG_CTLD. See other routes in this file for
                     * examples on how to do that.
                     */
                    public void process(Exchange exchange) {
                        String forwardRoute = Constants.DIRECT_SEND;
                        exchange.getIn().setHeader(Constants.HEADER_FORWARD, forwardRoute);
                        exchange.getIn().setHeader(Constants.HEADER_COMPONENT_ID, config.getComponentId());
                        exchange.getIn().setHeader(Constants.HEADER_DEVICE, Constants.GS_RIG_CTLD);
                    }
                })
            .doCatch(Exception.class)
                .process(new Processor() {
                    public void process(Exchange exchange) {
                        LOG.error("Command failed to pass through - exchange properties:");
    
                        Map<String, Object> properties = exchange.getProperties();
                        for (String propName : properties.keySet()) {
                            LOG.error(" {}: {}", propName, properties.get(propName));
                        }
                    }
                })
            .end();
        
        // @formatter: on
    }

    public static void main(String... args) {
        LOG.info("Starting radio driver");
        try {
            new Main().run();
        } catch (Exception e) {
            LOG.error("Failed to start radio driver", e);
        }
    }
}