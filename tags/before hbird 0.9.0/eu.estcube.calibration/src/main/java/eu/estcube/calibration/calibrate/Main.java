package eu.estcube.calibration.calibrate;

import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Queue;

import org.hbird.exchange.core.Parameter;

import eu.estcube.calibration.domain.InfoContainer;
import eu.estcube.calibration.utils.FileManager;
import eu.estcube.calibration.utils.QueueMap;
import eu.estcube.calibration.xmlparser.Parser;

public class Main {
	
	
	
	
	private static Hashtable<String, Hashtable<String,InfoContainer>> subsystems;

	
	public static Hashtable<String,InfoContainer> getSubsystem(String key){
		
		
		return subsystems.get(key);
		
	}
	

	
	
	private static void populateSubsystems(){
		
		subsystems = new Hashtable<String, Hashtable<String,InfoContainer>>();
		
		File[] configFiles = FileManager.getFiles();
		
		for (File f: configFiles){
			
			Hashtable<String,InfoContainer> subsystem = Parser.getCalibrations(f);
			
			String key = f.getName().substring(0, f.getName().lastIndexOf('.'));
			
			subsystems.put(key, subsystem);
			
		}
		
	}
	
	
	
	public static void main (String [] args){
		
	populateSubsystems();

	
	Parameter p0 = new Parameter("cdhs", "rtc_temp", null, 23.75, null);
	Parameter p1 = new Parameter("cdhs", "internal_raw", null, 1753, null);
	Parameter p2 = new Parameter("cdhs", "vref_raw", null, 1450, null);	
	
	QueueMap<String, Parameter> nonCalibrated = new QueueMap<String, Parameter>();
	
	nonCalibrated.offer(p0.getName(), p0);
	nonCalibrated.offer(p1.getName(), p1);
	nonCalibrated.offer(p2.getName(), p2);
	
	
	Calibrate c = new Calibrate(nonCalibrated);
	c.calibrate("cdhs");
	c.printCalibrated();
	
	
	
	
	
		
	}
	
}
