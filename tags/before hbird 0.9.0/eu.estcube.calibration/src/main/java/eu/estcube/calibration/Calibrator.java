/** 
 *
 */
package eu.estcube.calibration;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.hbird.exchange.configurator.StandardEndpoints;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.core.BusinessCard;
import org.hbird.exchange.core.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import eu.estcube.calibration.processors.ParameterProcessor;
import eu.estcube.common.PrepareForInjection;

/**
 *
 */
public class Calibrator extends RouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(Calibrator.class);

    @Autowired
    private CalibratorConfig config;

    @Autowired
    private ParameterProcessor parameterProcessor;

    @Autowired
    private PrepareForInjection preparator;

    /** @{inheritDoc . */
    @Override
    public void configure() throws Exception {

        // @formatter:off
        from(StandardEndpoints.MONITORING)
            .filter(header(StandardArguments.CLASS).isEqualTo(Parameter.class.getSimpleName()))
            .process(parameterProcessor)
            .process(preparator)
            .to(StandardEndpoints.MONITORING);
        
        BusinessCard card = new BusinessCard(config.getServiceId(), config.getHeartBeatInterval());
        card.setDescription(String.format("Calibrator; version: %s", config.getServiceVersion()));
        from("timer://heartbeat?fixedRate=true&period=" + config.getHeartBeatInterval())
            .bean(card, "touch")
            .process(preparator)
            .to(StandardEndpoints.MONITORING);
        // @formatter:on

    }

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        LOG.info("Starting Calibrator transmitter");
        new Main().run(args);
    }
}
