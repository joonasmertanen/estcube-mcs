package eu.estcube.calibration.utils;

import java.util.ArrayList;

/**
 * Data structure that resembles a Queue but adding Map functionalities. Also, you can read (but not pull) an element from any chosen position.
 * 
 * @author David Fernandez <david@davidfm.es>
 *
 * @param <T>
 * @param <V>
 */


public class QueueMap<T, V> {

	private ArrayList<T> keys;
	private ArrayList<V> elements;

	public QueueMap() {

		keys = new ArrayList<T>();
		elements = new ArrayList<V>();
	}

	public QueueMap(int size) {

		keys = new ArrayList<T>(size);
		elements = new ArrayList<V>(size);
	}
	


	public boolean offer(T key, V element) {

		

			keys.add(key);
			elements.add(element);

			return true;

	}

	public V poll() {

		keys.remove(0);
		V element = elements.get(0);
		elements.remove(0);

		return element;
	}

	public boolean contains(T key) {

		return keys.contains(key);
	}
	
	
	public V get(T key){
		
				
		return elements.get(keys.indexOf(key));
	}
	
	public V peek(){
		
		return elements.get(0);
	}
	
	public boolean isEmpty(){
		
		return keys.size() == 0 && elements.size() == 0;
	}


}
