/** 
 *
 */
package eu.estcube.calibration.processors;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.hbird.exchange.core.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class ParameterProcessor implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(ParameterProcessor.class);

    private final Map<String, ParameterCalibrator> calibrators = initCalibrators();

    /** @{inheritDoc . */
    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        out.copyFrom(in);

        Parameter input = in.getBody(Parameter.class);
        ParameterCalibrator pc = calibrators.get(input.getID());
        if (pc != null) {
            // found calibrator for the parameter
            LOG.debug("Calibrating Parameter; ID: {}; value: {}", input.getID(), input.getValue());
            Parameter output = pc.calibrate(input);
            LOG.debug(" Result ; ID: {}; value: {}", output.getID(), output.getValue());
            out.setBody(output);
        } else {
            // no calibrator found - stop the processing route
            LOG.debug("Skipping Parameter; ID: {}", input.getID(), input.getName());
            exchange.setProperty(Exchange.ROUTE_STOP, Boolean.TRUE);
        }
    }

    private static Map<String, ParameterCalibrator> initCalibrators() {
        Map<String, ParameterCalibrator> map = new HashMap<String, ParameterCalibrator>();
        map.put("WeatherStation/meteo.physic.ut.ee/air.temperature", new TemperatureToFahrenheit());
        map.put("WeatherStation/emhi.ee/air.temperature", new TemperatureToFahrenheit());
        return map;
    }

    private static class TemperatureToFahrenheit implements ParameterCalibrator {

        /** @{inheritDoc . */
        @Override
        public Parameter calibrate(Parameter input) {
            Parameter output = new Parameter(input);
            output.setEntityID(input.getID() + "/fahrenheit");
            output.setName(input.getName() + " (F)");
            output.setUnit("°F");
            double c = input.getValue().doubleValue();
            double f = c * (9 / 5) + 32;
            output.setValue(f);
            return output;
        }
    };

}
