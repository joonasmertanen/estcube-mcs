/** 
 *
 */
package eu.estcube.calibration;

import org.hbird.exchange.core.ConfigurationBase;
import org.springframework.stereotype.Component;

/**
 * Calibrator specific configuration.
 */
@Component
public class CalibratorConfig extends ConfigurationBase {

    private static final long serialVersionUID = 7796683996586902758L;

}
