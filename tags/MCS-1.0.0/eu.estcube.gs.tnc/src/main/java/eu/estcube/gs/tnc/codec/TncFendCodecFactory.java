package eu.estcube.gs.tnc.codec;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class TncFendCodecFactory implements ProtocolCodecFactory {

    private static final Logger LOG = LoggerFactory.getLogger(TncFendCodecFactory.class);
    
    private ProtocolEncoder encoder = new TncFendEncoder();
    
    private ProtocolDecoder decoder = new TncFendDecoder();
    
    /** @{inheritDoc}. */
    @Override
    public ProtocolEncoder getEncoder(IoSession session) throws Exception {
        LOG.trace(" Calling FEND encoder");
        return encoder;
    }

    /** @{inheritDoc}. */
    @Override
    public ProtocolDecoder getDecoder(IoSession session) throws Exception {
        LOG.trace(" Calling FEND decoder");
        return decoder;
    }
}
