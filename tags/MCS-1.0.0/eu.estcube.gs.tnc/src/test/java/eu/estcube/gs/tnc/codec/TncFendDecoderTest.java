package eu.estcube.gs.tnc.codec;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TncFendDecoderTest {

    
    private IoBuffer in;
    
    @Mock
    private IoSession session;
    
    @Mock
    private ProtocolDecoderOutput out;
    
    private TncFendDecoder decoder;
    
    private InOrder inOrder;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        decoder = new TncFendDecoder();
        inOrder = inOrder(session, out);
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFendDecoder#doDecode(org.apache.mina.core.session.IoSession, org.apache.mina.core.buffer.IoBuffer, org.apache.mina.filter.codec.ProtocolDecoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testDoDecodeFendOnly() throws Exception {
        in = IoBuffer.wrap(new byte[] { TncConstants.FEND } );
        assertTrue(decoder.doDecode(session, in, out));
        assertFalse(decoder.doDecode(session, in, out));
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFendDecoder#doDecode(org.apache.mina.core.session.IoSession, org.apache.mina.core.buffer.IoBuffer, org.apache.mina.filter.codec.ProtocolDecoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testDoDecodeBytes() throws Exception {
        in = IoBuffer.wrap(new byte[] { 0x1, 0x2, 0x3 } );
        assertFalse(decoder.doDecode(session, in, out));
        inOrder.verifyNoMoreInteractions();
    }
    
    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFendDecoder#doDecode(org.apache.mina.core.session.IoSession, org.apache.mina.core.buffer.IoBuffer, org.apache.mina.filter.codec.ProtocolDecoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testDoDecodeBytesFend() throws Exception {
        in = IoBuffer.wrap(new byte[] { 0x1, 0x2, 0x3, TncConstants.FEND } );
        assertTrue(decoder.doDecode(session, in, out));
        assertFalse(decoder.doDecode(session, in, out));
        ArgumentCaptor<IoBuffer> captor = ArgumentCaptor.forClass(IoBuffer.class);
        inOrder.verify(out, times(1)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        assertEquals(captor.getValue().getHexDump(), "01 02 03", captor.getValue().getHexDump());
    }
    
    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFendDecoder#doDecode(org.apache.mina.core.session.IoSession, org.apache.mina.core.buffer.IoBuffer, org.apache.mina.filter.codec.ProtocolDecoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testDoDecodeFendBytesFend() throws Exception {
        in = IoBuffer.wrap(new byte[] { TncConstants.FEND, 0x1, 0x2, 0x3, TncConstants.FEND } );
        assertTrue(decoder.doDecode(session, in, out));
        assertTrue(decoder.doDecode(session, in, out));
        assertFalse(decoder.doDecode(session, in, out));
        ArgumentCaptor<IoBuffer> captor = ArgumentCaptor.forClass(IoBuffer.class);
        inOrder.verify(out, times(1)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        assertEquals(captor.getValue().getHexDump(), "01 02 03", captor.getValue().getHexDump());
    }
    
    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFendDecoder#doDecode(org.apache.mina.core.session.IoSession, org.apache.mina.core.buffer.IoBuffer, org.apache.mina.filter.codec.ProtocolDecoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testDoDecodeFendBytesFendBytesFend() throws Exception {
        in = IoBuffer.wrap(new byte[] { TncConstants.FEND, 0x1, 0x2, 0x3, TncConstants.FEND, 0x4, 0x5, 0x6, TncConstants.FEND } );
        assertTrue(decoder.doDecode(session, in, out));
        assertTrue(decoder.doDecode(session, in, out));
        assertTrue(decoder.doDecode(session, in, out));
        assertFalse(decoder.doDecode(session, in, out));
        ArgumentCaptor<IoBuffer> captor = ArgumentCaptor.forClass(IoBuffer.class);
        inOrder.verify(out, times(2)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        assertEquals(captor.getAllValues().get(0).getHexDump(), "01 02 03", captor.getAllValues().get(0).getHexDump());
        assertEquals(captor.getAllValues().get(1).getHexDump(), "04 05 06", captor.getAllValues().get(1).getHexDump());
    }
    
    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFendDecoder#doDecode(org.apache.mina.core.session.IoSession, org.apache.mina.core.buffer.IoBuffer, org.apache.mina.filter.codec.ProtocolDecoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testDoDecodeFendBytesFendBytesFendBytesFend() throws Exception {
        in = IoBuffer.wrap(new byte[] { TncConstants.FEND, 0x1, 0x2, 0x3, TncConstants.FEND, 0x4, 0x5, 0x6, TncConstants.FEND, 0x7, 0x8, 0x9, TncConstants.FEND } );
        assertTrue(decoder.doDecode(session, in, out));
        assertTrue(decoder.doDecode(session, in, out));
        assertTrue(decoder.doDecode(session, in, out));
        assertTrue(decoder.doDecode(session, in, out));
        assertFalse(decoder.doDecode(session, in, out));
        ArgumentCaptor<IoBuffer> captor = ArgumentCaptor.forClass(IoBuffer.class);
        inOrder.verify(out, times(3)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        assertEquals(captor.getAllValues().get(0).getHexDump(), "01 02 03", captor.getAllValues().get(0).getHexDump());
        assertEquals(captor.getAllValues().get(1).getHexDump(), "04 05 06", captor.getAllValues().get(1).getHexDump());
        assertEquals(captor.getAllValues().get(2).getHexDump(), "07 08 09", captor.getAllValues().get(2).getHexDump());
    }
    
    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFendDecoder#doDecode(org.apache.mina.core.session.IoSession, org.apache.mina.core.buffer.IoBuffer, org.apache.mina.filter.codec.ProtocolDecoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testDoDecodeFendBytesFendFendBytesFend() throws Exception {
        in = IoBuffer.wrap(new byte[] { TncConstants.FEND, 0x1, 0x2, 0x3, TncConstants.FEND, TncConstants.FEND, 0x4, 0x5, 0x6, TncConstants.FEND } );
        assertTrue(decoder.doDecode(session, in, out));
        assertTrue(decoder.doDecode(session, in, out));
        assertTrue(decoder.doDecode(session, in, out));
        assertTrue(decoder.doDecode(session, in, out));
        assertFalse(decoder.doDecode(session, in, out));
        ArgumentCaptor<IoBuffer> captor = ArgumentCaptor.forClass(IoBuffer.class);
        inOrder.verify(out, times(2)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        assertEquals(captor.getAllValues().get(0).getHexDump(), "01 02 03", captor.getAllValues().get(0).getHexDump());
        assertEquals(captor.getAllValues().get(1).getHexDump(), "04 05 06", captor.getAllValues().get(1).getHexDump());
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFendDecoder#doDecode(org.apache.mina.core.session.IoSession, org.apache.mina.core.buffer.IoBuffer, org.apache.mina.filter.codec.ProtocolDecoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testDoDecodeFendBytesFendFendBytesFendBytesFend() throws Exception {
        in = IoBuffer.wrap(new byte[] { TncConstants.FEND, 0x1, 0x2, 0x3, TncConstants.FEND, TncConstants.FEND, 0x4, 0x5, 0x6, TncConstants.FEND, 0x07, 0x08, 0x09, TncConstants.FEND } );
        assertTrue(decoder.doDecode(session, in, out));
        assertTrue(decoder.doDecode(session, in, out));
        assertTrue(decoder.doDecode(session, in, out));
        assertTrue(decoder.doDecode(session, in, out));
        assertTrue(decoder.doDecode(session, in, out));
        assertFalse(decoder.doDecode(session, in, out));
        ArgumentCaptor<IoBuffer> captor = ArgumentCaptor.forClass(IoBuffer.class);
        inOrder.verify(out, times(3)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        assertEquals(captor.getAllValues().get(0).getHexDump(), "01 02 03", captor.getAllValues().get(0).getHexDump());
        assertEquals(captor.getAllValues().get(1).getHexDump(), "04 05 06", captor.getAllValues().get(1).getHexDump());
        assertEquals(captor.getAllValues().get(2).getHexDump(), "07 08 09", captor.getAllValues().get(2).getHexDump());
    }
    
    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncFendDecoder#doDecode(org.apache.mina.core.session.IoSession, org.apache.mina.core.buffer.IoBuffer, org.apache.mina.filter.codec.ProtocolDecoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testDoDecodeFendFendBytesFendFendBytesFendFend() throws Exception {
        in = IoBuffer.wrap(new byte[] { TncConstants.FEND, TncConstants.FEND, 0x1, 0x2, 0x3, TncConstants.FEND, TncConstants.FEND, 0x4, 0x5, 0x6, TncConstants.FEND, TncConstants.FEND } );
        assertTrue(decoder.doDecode(session, in, out));
        assertTrue(decoder.doDecode(session, in, out));
        assertTrue(decoder.doDecode(session, in, out));
        assertTrue(decoder.doDecode(session, in, out));
        assertTrue(decoder.doDecode(session, in, out));
        assertTrue(decoder.doDecode(session, in, out));
        assertFalse(decoder.doDecode(session, in, out));
        ArgumentCaptor<IoBuffer> captor = ArgumentCaptor.forClass(IoBuffer.class);
        inOrder.verify(out, times(2)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        assertEquals(captor.getAllValues().get(0).getHexDump(), "01 02 03", captor.getAllValues().get(0).getHexDump());
        assertEquals(captor.getAllValues().get(1).getHexDump(), "04 05 06", captor.getAllValues().get(1).getHexDump());
    }


}
