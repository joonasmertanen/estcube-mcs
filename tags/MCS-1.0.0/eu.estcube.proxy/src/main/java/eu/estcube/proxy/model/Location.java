package eu.estcube.proxy.model;

/**
 * Class for storing location data of the packet sent from 3rd party
 * groundstations. Location can be determined by either WWL (World Wide Locator,
 * i.e. KO38ii) or by geographical coordinates (latitude and logitude, i.e.
 * 26.74E, 58.36N).
 * 
 * @author Kaarel Hanson
 * 
 */
public class Location {

	private String longitude;
	private String latitude;
	private String wwl;

	public Location(String latitude, String longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public Location(String wwl) {
		this.wwl = wwl;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getWwl() {
		return wwl;
	}

	public void setWwl(String wwl) {
		this.wwl = wwl;
	}
}
