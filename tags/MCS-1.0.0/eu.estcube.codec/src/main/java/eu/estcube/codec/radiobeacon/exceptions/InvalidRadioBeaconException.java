package eu.estcube.codec.radiobeacon.exceptions;

public class InvalidRadioBeaconException extends Exception {

    private static final long serialVersionUID = -3176682605501895240L;

    public InvalidRadioBeaconException(String beacon) {
        super("Invalid radiobeacon message: " + beacon);
    }

}
