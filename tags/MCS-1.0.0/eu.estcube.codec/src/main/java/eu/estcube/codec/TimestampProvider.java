package eu.estcube.codec;

/**
 * Provides time stamp service, used for instance when setting the time stamp in
 * TC packets.
 */
public interface TimestampProvider {

	/**
	 * Returns the time stamp. as number of <b>seconds!</b> from 01.01.1970.
	 * 
	 * @return Time stamp as number of <b>seconds!</b> from 01.01.1970.
	 */
	public long getTimestamp();
}
