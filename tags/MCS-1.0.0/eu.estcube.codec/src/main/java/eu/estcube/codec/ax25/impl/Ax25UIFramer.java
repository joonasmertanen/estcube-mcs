package eu.estcube.codec.ax25.impl;

import org.apache.commons.lang3.ArrayUtils;

import eu.estcube.codec.ax25.Ax25Framer;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;

/**
 * Creates AX.25 UI frames from given data which normally is encoded TC
 * packet(s). The frame size given should match the frame size that was used
 * when creating the TC packets.
 */

public class Ax25UIFramer implements Ax25Framer {
	private int frameSize;
	private byte ctrl;
	private byte pid;
	private byte[] srcAddr;
	private byte[] destAddr;

	/**
	 * Creates new framer.
	 * 
	 * @param frameSize
	 *            The frame size - the maximum size of the field INFO in bytes.
	 * @param ctrl
	 *            The CTRL field value.
	 * @param pid
	 *            The PID field value.
	 * @param srcAddr
	 *            The source address field value.
	 * @param destAddr
	 *            The destination address field value.
	 */
	public Ax25UIFramer(int frameSize, byte ctrl, byte pid, byte[] srcAddr, byte[] destAddr) {
		super();
		this.frameSize = frameSize;
		this.ctrl = ctrl;
		this.pid = pid;
		this.srcAddr = srcAddr;
		this.destAddr = destAddr;
	}

	@Override
	public Ax25UIFrame[] create(byte[] data) {
		int frameCount = (int) Math.ceil((double) data.length / (double) frameSize);
		Ax25UIFrame[] frames = new Ax25UIFrame[frameCount];

		int dataWritten = 0;
		int dataToWrite = Math.min(data.length, frameSize);
		int i = 0;

		while (dataWritten < data.length) {
			frames[i] = new Ax25UIFrame();
			frames[i].setDestAddr(destAddr);
			frames[i].setSrcAddr(srcAddr);
			frames[i].setCtrl(ctrl);
			frames[i].setPid(pid);
			frames[i].setInfo(ArrayUtils.subarray(data, dataWritten, dataWritten + dataToWrite));
			frames[i].setFcs(new byte[] { 0, 0 });// TODO: Calucate FCS

			dataWritten += dataToWrite;
			dataToWrite = Math.min(data.length - dataWritten, frameSize);
			i++;
		}

		return frames;
	}
}
