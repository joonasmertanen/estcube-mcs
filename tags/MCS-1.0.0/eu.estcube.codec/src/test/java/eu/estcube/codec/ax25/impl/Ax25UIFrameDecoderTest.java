package eu.estcube.codec.ax25.impl;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import eu.estcube.domain.transport.ax25.Ax25FrameStatus;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;

public class Ax25UIFrameDecoderTest {
	private Ax25UIFrameDecoder ax25Decoder;

	@Before
	public void setUp() throws Exception {
		ax25Decoder = new Ax25UIFrameDecoder();
	}

	@Test
	public void testAx25Decoder() {
		// fail("Not yet implemented");
	}

	@Test
	public void testDecodeIoSessionIoBufferProtocolDecoderOutput() {
		// fail("Not yet implemented");
	}

	@Test
	public void testDispose() {
		// fail("Not yet implemented");
	}

	@Test
	public void testFinishDecode() {
		// fail("Not yet implemented");
	}

	@Test
	public void testDecodeByte() {
		// fail("Not yet implemented");
	}

	@Test
	public void testCreateFrame() throws IOException {
		byte[] destAddress = new byte[] { 8, 9, 10, 11, 12, 13, 14 };
		byte[] srcAddress = new byte[] { 1, 2, 3, 4, 5, 6, 7 };
		byte[] control = new byte[] { 15 };
		byte[] pid = new byte[] { 16 };
		byte[] info = new byte[] { 17, 18 };
		byte[] fcs = new byte[] { 19, 20 };

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		outputStream.write(destAddress);
		outputStream.write(srcAddress);
		outputStream.write(control);
		outputStream.write(pid);
		outputStream.write(info);
		outputStream.write(fcs);

		byte source[] = outputStream.toByteArray();

		Ax25FrameStatus fs = new Ax25FrameStatus();
		fs.setErrorUnAligned(true);

		Ax25UIFrame f = ax25Decoder.createFrame(source, 20, fs);
		assertTrue(f.getStatus() == fs);
		assertTrue(!fs.isValid());
		assertTrue(Arrays.equals(srcAddress, f.getSrcAddr()));
		assertTrue(Arrays.equals(destAddress, f.getDestAddr()));
		assertEquals(control[0], f.getCtrl());
		assertEquals(pid[0], f.getPid());
		assertTrue(Arrays.equals(info, f.getInfo()));
		assertTrue(Arrays.equals(fcs, f.getFcs()));
	}

	@Test
	public void testDecode() {
		String bitStream = "";

		// Flag
		bitStream += "01111110";

		// Dest. address 1, 1, 1, 1, 1, 1, 1;
		bitStream += "10000000|10000000|10000000|10000000|10000000|10000000|10000000";

		// Src. address 2,2,2,2,2,2,2
		bitStream += "01000000|01000000|01000000|01000000|01000000|01000000|01000000";

		// Control 3
		bitStream += "11000000";

		// PID 4
		bitStream += "00100000";

		// Info 128, 128
		bitStream += "00000001|00000001";

		// FCS 1, 128
		bitStream += "10000000|00000001";

		// Another identical frame
		bitStream += bitStream;

		// Flag
		bitStream += "01111110";

		int[] bits = stringToBits(bitStream);

		List<Ax25UIFrame> frames = new ArrayList<Ax25UIFrame>();

		for (int i = 0; i < bits.length / 8; i++) {
		    byte b = 0;
		    for (int j = 0; j < 8; j++) b = (byte) ((b << 1) | bits[i * 8 + j]);
			Ax25UIFrame f = ax25Decoder.decode(b);
			if (f != null) {
				frames.add(f);
			}
		}

		assertTrue(2 == frames.size());
		for (Ax25UIFrame f : frames) {
			verifyFrame(f);
		}
	}

	protected void verifyFrame(Ax25UIFrame frame) {
		assertTrue(Arrays.equals(new byte[] { 1, 1, 1, 1, 1, 1, 1 }, frame.getDestAddr()));
		assertTrue(Arrays.equals(new byte[] { 2, 2, 2, 2, 2, 2, 2 }, frame.getSrcAddr()));
		assertEquals(3, frame.getCtrl());
		assertEquals(4, frame.getPid());
		assertTrue(Arrays.equals(new byte[] { -128, -128 }, frame.getInfo()));
		assertTrue(Arrays.equals(new byte[] { 1, -128 }, frame.getFcs()));

	}

	private void assertEquals(int i, byte ctrl) {
		// TODO Auto-generated method stub

	}

	public int[] stringToBits(String s) {
		List<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == '0') {
				list.add(0);
			}
			if (s.charAt(i) == '1') {
				list.add(1);
			}
		}
		int[] bits = new int[list.size()];
		int pos = 0;
		for (Integer i : list) {
			bits[pos++] = i;
		}

		return bits;
	}
}
