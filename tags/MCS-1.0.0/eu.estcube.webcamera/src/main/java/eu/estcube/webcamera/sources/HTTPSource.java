package eu.estcube.webcamera.sources;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.http.HttpMethods;
import org.apache.camel.model.RouteDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.webcamera.FeedSource;

public class HTTPSource extends FeedSource {
    public static final String WEB_CAM_REQUEST_BODY = "Image request";
    private static final Logger LOG = LoggerFactory.getLogger(HTTPSource.class);

    private String url;
    private int pollInterval;

    public HTTPSource(String id, int pollInterval, String url) {
        super(id);

        this.url = url;
        this.pollInterval = pollInterval;

        LOG.info("Created HTTPSource: " + id + "," + pollInterval + "," + url);
    }

    @Override
    public RouteDefinition getRouteAction() {
        return new RouteDefinition("timer://httpcam-" + this.getID() + "?fixedRate=true&period=" + pollInterval)
                .process(new Processor() {
                    public void process(Exchange ex) throws Exception {
                        // no reply when body is null
                        ex.getOut().setBody(WEB_CAM_REQUEST_BODY, String.class);
                    }
                })
                .setHeader(Exchange.HTTP_METHOD, HttpMethods.POST)
                .to(url);
    }
}
