/**
 *
 */
package eu.estcube.sdc;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.hbird.business.api.IPublisher;
import org.hbird.business.core.AddHeaders;
import org.hbird.exchange.configurator.StandardEndpoints;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.core.BusinessCard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import eu.estcube.common.Headers;
import eu.estcube.common.UpdateTimestamp;
import eu.estcube.domain.transport.Downlink;
import eu.estcube.domain.transport.Uplink;
import eu.estcube.sdc.domain.SpaceDataChainConfiguration;
import eu.estcube.sdc.gcp.Ax25UiFrameToIssuedProcessor;
import eu.estcube.sdc.gcp.CommandToAx25UiFrameProcessor;
import eu.estcube.sdc.processor.Ax25UiFrameImportProcessor;
import eu.estcube.sdc.processor.Ax25UiFrameInputProcessor;
import eu.estcube.sdc.processor.Ax25UiFrameOutputProcessor;
import eu.estcube.sdc.processor.Ax25UiFrameOutputToFileProcessor;
import eu.estcube.sdc.xml.Ax25UiFrameImportParser;
import eu.estcube.sdc.xml.Ax25UiFrameInputParser;
import eu.estcube.sdc.xml.Ax25UiFrameOutputSerializer;

/**
 *
 */
public class SpaceDataChain extends RouteBuilder {

    public static final String TYPE = "AX.25";

    private static final Logger LOG = LoggerFactory.getLogger(SpaceDataChain.class);

    @Autowired
    private AddHeaders addHeaders;

    @Autowired
    private SpaceDataChainConfiguration config;

    @Autowired
    private Ax25UiFrameInputParser xmlParser;

    @Autowired
    private Ax25UiFrameInputProcessor inputProcesor;

    @Autowired
    private Ax25UiFrameOutputProcessor outputProcessor;

    @Autowired
    private Ax25UiFrameOutputSerializer xmlSerializer;

    @Autowired
    private Ax25UiFrameOutputToFileProcessor toFile;

    @Autowired
    private Ax25UiFrameToIssuedProcessor ax25UiFrameToIssuedProcessor;

    @Autowired
    private CommandToAx25UiFrameProcessor commandToAx25UiFrameProcessor;

    @Autowired
    private UpdateTimestamp updateTimestamp;

    @Autowired
    private Ax25UiFrameImportParser importXmlParser;

    @Autowired
    private Ax25UiFrameImportProcessor importProcessor;

    @Autowired
    private IPublisher publisher;

    /** @{inheritDoc . */
    @Override
    public void configure() throws Exception {
        MDC.put(StandardArguments.ISSUED_BY, config.getServiceId());

        BusinessCard card = new BusinessCard(config.getServiceId(), config.getServiceName());
        card.setPeriod(config.getHeartBeatInterval());
        card.setDescription(String.format("Space Data Chain; version: %s", config.getServiceVersion()));

        // @formatter:off

        // send business card
        from("timer://heartbeat?fixedRate=true&period=" + config.getHeartBeatInterval())
            .bean(card, "touch")
            .process(addHeaders)
            .to(StandardEndpoints.MONITORING);

        // Down-link

        from(Downlink.AX25_FRAMES)
            .bean(ax25UiFrameToIssuedProcessor)
            .split(body())
            .process(addHeaders)
            .to("log:eu.estcube.sdc.ax25ToGcp?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
            .multicast()
            .bean(publisher) // Here publisher should not be an Injector, otherwise the frame is duplicated in monitoring queue
            .to(StandardEndpoints.MONITORING);

        // Up-link

        from("seda:logUplinkAx25Frames")
            .bean(updateTimestamp) // update the timestamp header value to the current one
            .setHeader(StandardArguments.ISSUED_BY, constant(config.getServiceId()))
            // Will have to listen to Track commands from hbird to get orbit number
            // or it could be set at some earlier point (e.g. some nearest groundstation selection class that uses Orekit)
            // or sending to AX25_FRAMES_LOG should take place later (in sdr or tnc driver).
            //out.setHeader(StandardArguments.CONTACT_ID, eventId);
            //out.setHeader(StandardArguments.ORBIT_NUMBER, orbitNumber);
            .setHeader(StandardArguments.CLASS, simple("${body.class.getSimpleName}"))
            .setHeader(StandardArguments.TYPE, constant(TYPE))
            .setHeader(Headers.COMMUNICATION_LINK_TYPE, constant(Uplink.class.getSimpleName()))
            .to(Uplink.AX25_FRAMES_LOG)
            .end();

        from("direct:toUplink")
            // In the config file there is some default GS ID? The correct one,
            // which can be changing, could be set with config.setGroundstationId(),
            // automatically to the one with closest contact, or some other magic.
            .setHeader(StandardArguments.GROUND_STATION_ID, constant(config.getGroundstationId()))
            .multicast()
            .to(Uplink.AX25_FRAMES, "seda:logUplinkAx25Frames");

        from("direct:processCommands")
            .bean(commandToAx25UiFrameProcessor)
            // FIXME: sat ID should be set in the command, to fix in WebServer or other place?
            .setHeader(StandardArguments.SATELLITE_ID, constant("/ESTCUBE/Satellites/ESTCube-1"))
            .to("log:eu.estcube.sdc.commandToAx25UiFrames?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
            .to("direct:toUplink");

        from(Uplink.COMMANDS)
            .multicast()
            .to("direct:processCommands")
            .bean(publisher);

        if (config.isUseFiles()) {
            from("file://" + config.getUplinkAx25() + "?recursive=true&move=.sent&maxMessagesPerPoll=" + config.getMaxMessagesPerPoll() + "&delay=" + config.getFilePollInterval())
                .bean(xmlParser)
                // FIXME: sat ID should be set in the xml file, to fix in ICP terminal?
                .setHeader(StandardArguments.SATELLITE_ID, constant("/ESTCUBE/Satellites/ESTCube-1"))
                .split(body())
                .bean(inputProcesor)
                .to("log:eu.estcube.sdc.input.ax25.xml?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
                .to("direct:toUplink");

            from(Downlink.AX25_FRAMES)
                .bean(outputProcessor)
                .bean(toFile)
                .to("log:eu.estcube.sdc.output.ax25.xml?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
                .to("file://" + config.getDownlinkAx25());

            from("file://" + config.getImportAx25() + "?move=.imported&moveFailed=.invalid&maxMessagesPerPoll=" + config.getMaxMessagesPerPoll() + "&delay=" + config.getFilePollInterval())
                .bean(importXmlParser)
                .split(body())
                .bean(importProcessor)
                .to("log:eu.estcube.sdc.import.ax25.xml?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
                .to(Downlink.AX25_FRAMES);
        }

        // @formatter:on
    }

    public static void main(String[] args) throws Exception {
        LOG.info("Starting Space Data Chain");
        // new Main().run(args);
        // TODO - 14.08.2013; kimmell - switch back when upgrading to Camel 2.12
        AbstractApplicationContext context = new ClassPathXmlApplicationContext("META-INF/spring/camel-context.xml");
        SpaceDataChain sdc = context.getAutowireCapableBeanFactory().createBean(SpaceDataChain.class);

        Main m = new Main();
        m.setApplicationContext(context);
        m.addRouteBuilder(sdc);
        m.run(args);
    }
}
