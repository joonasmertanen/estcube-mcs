package eu.estcube.limitchecking.notifiers;

import java.security.GeneralSecurityException;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.hbird.exchange.core.CalibratedParameter;
import org.hbird.exchange.core.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.estcube.common.utils.Emailer;
import eu.estcube.limitchecking.data.ParameterLimits;

@Component
public class EmailNotifier {

    private static final Logger LOG = LoggerFactory.getLogger(EmailNotifier.class);

    @Value("${email.to}")
    private String to;

    @Autowired
    private Emailer emailer;

    public void notify(ParameterLimits pl, Parameter parameter, int levelBefore, int levelNow) {
        if (to == null || to.isEmpty())
            return;
        boolean increasing = levelNow > levelBefore;

        // Parameter value has gone to/gone out of all/returned to X limits
        String title = (parameter instanceof CalibratedParameter ? "Calibrated p" : "P") + "arameter \""
                + getComponentName(pl.getID()) + "\" value has "
                + (increasing ? "gone" : "returned") + " "
                + getLimitName(levelNow, (increasing ? "into " : "to ")) + " limits (" + parameter.getValue() + ")";
        StringBuilder sb = new StringBuilder();
        sb.append("---LimitChecking notification---").append("\n");
        sb.append("Parameter: ").append(pl.getID()).append("\n");
        sb.append("Value: ").append(parameter.getValue()).append("\n\n");
        sb.append("Soft limits: ").append(parseLimits(pl.getSoft())).append("\n");
        sb.append("Hard limits: ").append(parseLimits(pl.getHard())).append("\n");
        sb.append("Sanity limits: ").append(parseLimits(pl.getSanity())).append("\n");
        sb.append("Current limit zone: ").append(getLimitName(levelNow, "")).append("\n");
        sb.append("Previous limit zone: ").append(getLimitName(levelBefore, "")).append("\n\n");
        sb.append("Parameter data: ").append(parameter.toString()).append("\n");

        try {
            emailer.sendEmail(to, title, sb.toString());
        } catch (AddressException e) {
            LOG.error("Failed to send email", e);
        } catch (MessagingException e) {
            LOG.error("Failed to send email", e);
        } catch (GeneralSecurityException e) {
            LOG.error("Failed to send email", e);
        }
    }

    private String getComponentName(String id) {
        return id.substring(id.lastIndexOf("/") + 1);
    }

    private String parseLimits(double[] limits) {
        if (limits == null)
            return "none";
        return limits[0] + " - " + limits[1];
    }

    private String getLimitName(int level, String s) {
        switch (level) {
            case 0:
                return s + "soft";
            case 1:
                return s + "hard";
            case 2:
                return s + "sanity";
            default:
                return "out of all";
        }
    }
}
