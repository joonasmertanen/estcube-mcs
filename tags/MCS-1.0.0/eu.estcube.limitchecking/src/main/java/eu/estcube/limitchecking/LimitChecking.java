/** 
 *
 */
package eu.estcube.limitchecking;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.hbird.business.core.AddHeaders;
import org.hbird.exchange.configurator.StandardEndpoints;
import org.hbird.exchange.core.BusinessCard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import eu.estcube.limitchecking.processors.ParameterProcessor;

/**
 *
 */
public class LimitChecking extends RouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(LimitChecking.class);

    private static final String FILTER_ALL_PARAMETERS = "${in.header.class} in 'CalibratedParameter,Parameter'";

    @Autowired
    private LimitCheckingConfig config;

    @Autowired
    private ParameterProcessor parameterProcessor;

    @Autowired
    private AddHeaders addHeaders;

    /** @{inheritDoc . */
    @Override
    public void configure() throws Exception {

        // @formatter:off
        from(StandardEndpoints.MONITORING)
            .filter(simple(FILTER_ALL_PARAMETERS))
            .process(parameterProcessor);
        
        BusinessCard card = new BusinessCard(config.getServiceId(), config.getServiceName());
        card.setPeriod(config.getHeartBeatInterval());
        card.setDescription(String.format("LimitChecking; version: %s", config.getServiceVersion()));
        from("timer://heartbeat?fixedRate=true&period=" + config.getHeartBeatInterval())
            .bean(card, "touch")
            .process(addHeaders)
            .to(StandardEndpoints.MONITORING);
        // @formatter:on

    }

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        LOG.info("Starting Limit Checking");
        // new Main().run(args);
        // TODO - 14.08.2013; kimmell - switch back when upgrading to Camel 2.12
        try {
            AbstractApplicationContext context = new ClassPathXmlApplicationContext("META-INF/spring/camel-context.xml");
            LimitChecking calibrator = context.getAutowireCapableBeanFactory().createBean(LimitChecking.class);

            Main m = new Main();
            m.setApplicationContext(context);
            m.addRouteBuilder(calibrator);
            m.run(args);
        } catch (Exception e) {
            LOG.error("Failed to start " + LimitChecking.class.getName(), e);
        }
    }
}
