package eu.estcube.calibration.data;

import org.hbird.exchange.core.CalibratedParameter;

import de.congrace.exp4j.Calculable;
import de.congrace.exp4j.ExpressionBuilder;
import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;

/**
 * Holds calibration expression and unit data for specified parameter.
 * 
 * @author Gregor Eesmaa
 * 
 */
public class CalibrationExpression {

    private String id;
    private String expression;

    private Calculable calculable;

    /**
     * 
     * @return a <code>Calculable</code> containing the expression for finding
     *         the calibrated value
     * @throws UnknownFunctionException
     * @throws UnparsableExpressionException
     */
    public Calculable getCalculable() throws UnknownFunctionException, UnparsableExpressionException {
        if (calculable == null) {
            String expression = getExpression();
            if (expression == null)
                return null;
            calculable = new ExpressionBuilder(expression).withVariableNames("x").build();
        }
        return calculable;
    }

    /**
     * 
     * @return a <code>String</code> containing <code>Parameter</code>'s ID,
     *         corresponding to this calibration data.
     */
    public String getID() {
        return id.replace("//", "/");
    }

    /**
     * 
     * @return String containing the equation to find the calibrated value
     */
    public String getExpression() {
        return expression;
    }

    /**
     * Calibrates parameter using given expression, also adds specified unit to
     * the parameter
     * 
     * @param p - The parameter to be calibrated
     * @throws UnparsableExpressionException
     * @throws UnknownFunctionException
     */
    public void calibrate(CalibratedParameter cp) throws UnknownFunctionException,
            UnparsableExpressionException {
        Number n = cp.getValue();
        if (n != null) {
            Calculable calculable = getCalculable();
            if (calculable != null) {
                cp.setValue(calculable.calculate(n.doubleValue()));
            }
        }
    }
}
