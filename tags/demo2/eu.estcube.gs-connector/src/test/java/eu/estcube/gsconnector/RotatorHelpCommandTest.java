package eu.estcube.gsconnector;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryParameter;


public class RotatorHelpCommandTest {

    private RotatorHelpCommand rotatorHelp = new RotatorHelpCommand();
    

    private static final Logger LOG = LoggerFactory.getLogger(RotatorHelpCommandTest.class);
    @Test
    public void testCreateHelpList()
    {
        TelemetryObject telemetryObject = new TelemetryObject("Rotator reply");
        String[] messageSplit = {null, "P: set_pos     (Azimuth, Elevation)", 
                "p: get_pos     ()", 
                "get_info:?Info: None?RPRT 0"
                };
        telemetryObject = rotatorHelp.createHelpList(new TelemetryObject("Rotator reply"), messageSplit);
        assertEquals("SET_POSITION", telemetryObject.getParams().get(0).getName());
        assertEquals("Azimuth, Elevation", telemetryObject.getParams().get(0).getValue());
        
        assertEquals("GET_POSITION", telemetryObject.getParams().get(1).getName());
        assertEquals("", telemetryObject.getParams().get(1).getValue());
        
        assertTrue(telemetryObject.getParams().size()==2);
        
    }

}
