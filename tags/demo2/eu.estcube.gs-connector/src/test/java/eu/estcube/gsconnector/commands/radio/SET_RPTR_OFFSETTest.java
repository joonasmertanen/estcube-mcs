package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class SET_RPTR_OFFSETTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SET_RPTR_OFFSET createMessage = new SET_RPTR_OFFSET();
    private static final Logger LOG = LoggerFactory.getLogger(SET_RPTR_OFFSETTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("SET_RPTR_OFFSET");
        telemetryCommand.addParameter("Rptr Offset", 0);
        string.append("+O 0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
