package eu.estcube.gsconnector;

import java.nio.charset.Charset;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import junit.framework.TestCase;

public class HamlibDecoderTest extends TestCase {

    private static final Logger LOG = LoggerFactory.getLogger(HamlibDecoderTest.class);
    private HamlibDecoder decoder;

    @Before
    public void setUp() throws Exception {
        decoder = new HamlibDecoder();
    }
    
    /**
     * Test method for {@link eu.estcube.gsconnector.HamlibDecoder#decode(org.jboss.netty.channel.ChannelHandlerContext, org.jboss.netty.channel.Channel, java.lang.Object)}.
     * @throws Exception 
     */
    @Test
    public void testHamlibDecoder()throws Exception{
        ChannelBuffer buffer = ChannelBuffers.wrappedBuffer(new byte[] {});
        Object msg = new Object();
        buffer = ChannelBuffers.wrappedBuffer(new byte[] { 'a' });
        msg = ((ChannelBuffer) buffer).toString(Charset.forName("ASCII"));
        assertEquals(null, decoder.decode(null, null, msg));
        
        buffer = ChannelBuffers.wrappedBuffer(new byte[] { 'a', 'b', 'c' });
        msg = ((ChannelBuffer) buffer).toString(Charset.forName("ASCII"));
        assertEquals(null, decoder.decode(null, null, msg));     

        buffer = ChannelBuffers.wrappedBuffer(new byte[] { 'R', 'P', 'R', 'T', ' ', '\n' });
        msg = ((ChannelBuffer) buffer).toString(Charset.forName("ASCII"));
        //System.out.println(msg);
        assertEquals("aabcRPRT \n", decoder.decode(null, null, msg));  
        
        buffer = ChannelBuffers.wrappedBuffer(new byte[] {});
        buffer = ChannelBuffers.wrappedBuffer(new byte[] { ' ', '\n', '\n', 'R', '\n', 'P', 'R', 'T' });
        msg = ((ChannelBuffer) buffer).toString(Charset.forName("ASCII"));
        assertEquals(null, decoder.decode(null, null, msg));  
        
        buffer = ChannelBuffers.wrappedBuffer(new byte[] { 'r', 'p', 'r', 't', ' ', '\n' });
        msg = ((ChannelBuffer) buffer).toString(Charset.forName("ASCII"));
        assertEquals(null, decoder.decode(null, null, msg));  
        
        buffer = ChannelBuffers.wrappedBuffer(new byte[] { 'R', 'P', 'R', ' ', ' ', 'T', '\n' });
        msg = ((ChannelBuffer) buffer).toString(Charset.forName("ASCII"));
        assertEquals(null, decoder.decode(null, null, msg));  
    }

}
