package eu.estcube.gsconnector.commands.rotator;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class DMMM2DECTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private DMMM2DEC createMessage = new DMMM2DEC();
    private static final Logger LOG = LoggerFactory.getLogger(DMMM2DECTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("DMMM2DEC");
        telemetryCommand.addParameter("Degrees", 0);
        telemetryCommand.addParameter("Dec Minutes", 0.0);
        telemetryCommand.addParameter("S/W", 0);
        string.append("+E 0 0.0 0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
