package eu.estcube.gsconnector.commands;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class HELPTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private HELP createMessage = new HELP();
    private static final Logger LOG = LoggerFactory.getLogger(HELPTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("HELP");
        string.append("??+_\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
