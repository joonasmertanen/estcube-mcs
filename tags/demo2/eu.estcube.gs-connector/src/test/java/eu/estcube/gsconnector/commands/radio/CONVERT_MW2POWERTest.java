package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.HamlibDecoderTest;

public class CONVERT_MW2POWERTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private CONVERT_MW2POWER createMessage = new CONVERT_MW2POWER();
    private static final Logger LOG = LoggerFactory.getLogger(CONVERT_MW2POWERTest.class);
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand("CONVERT_MW2POWER");
        telemetryCommand.addParameter("Power mW", 0);
        telemetryCommand.addParameter("Frequency", 0);
        telemetryCommand.addParameter("Mode", "..");
        string.append("+4 0 0 ..\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
