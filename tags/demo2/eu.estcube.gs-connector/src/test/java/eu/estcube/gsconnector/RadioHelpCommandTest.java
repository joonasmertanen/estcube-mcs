package eu.estcube.gsconnector;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryParameter;


public class RadioHelpCommandTest {

    private RadioHelpCommand radioHelp = new RadioHelpCommand();
    

    private static final Logger LOG = LoggerFactory.getLogger(RadioHelpCommandTest.class);
    @Test
    public void testCreateHelpList()
    {
        TelemetryObject telemetryObject = new TelemetryObject("Rotator reply");
        String[] messageSplit = {null, "F: set_freq        (Frequency)         f: get_freq        ()", 
                "get_info:?Info: None?RPRT 0"
                };
        telemetryObject = radioHelp.createHelpList(new TelemetryObject("Rotator reply"), messageSplit);
        assertEquals("SET_FREQUENCY", telemetryObject.getParams().get(0).getName());
        assertEquals("Frequency", telemetryObject.getParams().get(0).getValue());
        
        assertEquals("GET_FREQUENCY", telemetryObject.getParams().get(1).getName());
        assertEquals("", telemetryObject.getParams().get(1).getValue());
        
        assertTrue(telemetryObject.getParams().size()==2);
        
    }

}
