package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class MOVE extends CommandStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        
        messageString.append("+");
        messageString.append("M");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Direction"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Speed"));
        
        messageString.append("\n");
        
        
        return messageString;
    }
}
