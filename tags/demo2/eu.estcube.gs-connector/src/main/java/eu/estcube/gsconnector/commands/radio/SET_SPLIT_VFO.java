package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SET_SPLIT_VFO extends CommandStringBuilder{
    private StringBuilder messageString;
    
    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString=new StringBuilder();
        messageString.append("+");
        messageString.append("S");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Split"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("TX VFO"));
        messageString.append("\n");
        return messageString;
    }
}
