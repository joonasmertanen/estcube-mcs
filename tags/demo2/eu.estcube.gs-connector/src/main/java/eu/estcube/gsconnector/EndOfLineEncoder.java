package eu.estcube.gsconnector;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;
import org.springframework.stereotype.Component;

@Component("EndOfLineEncoder")
public class EndOfLineEncoder extends OneToOneEncoder{
    
    @Override
    protected Object encode(ChannelHandlerContext channelHandlerContext,
            Channel channel, Object msg) throws Exception {
        return msg;
    }

}
