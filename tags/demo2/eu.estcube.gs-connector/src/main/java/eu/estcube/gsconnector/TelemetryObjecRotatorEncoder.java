package eu.estcube.gsconnector;

import java.util.HashMap;
import java.util.Map;

import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneDecoder;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.domain.TelemetryRotatorConstants;
import eu.estcube.gsconnector.commands.CAPABILITIES;
import eu.estcube.gsconnector.commands.CommandStringBuilder;
import eu.estcube.gsconnector.commands.GET_INFO;
import eu.estcube.gsconnector.commands.HELP;
import eu.estcube.gsconnector.commands.SEND_RAW_CMD;
import eu.estcube.gsconnector.commands.radio.RadioStatus;
import eu.estcube.gsconnector.commands.rotator.*;



@Component("rotatorEncoder")
public class TelemetryObjecRotatorEncoder extends OneToOneEncoder{

    private Map<String, CommandStringBuilder> commandsHashMap;

    private static final Logger LOG = LoggerFactory.getLogger(TelemetryObjecRotatorEncoder.class);

    public TelemetryObjecRotatorEncoder(){
        commandsHashMap = new HashMap<String, CommandStringBuilder>();
        commandsHashMap.put(TelemetryRotatorConstants.SET_POSITION, new SET_POSITION());
        commandsHashMap.put(TelemetryRotatorConstants.GET_POSITION, new GET_POSITION());
        commandsHashMap.put(TelemetryRotatorConstants.STOP, new STOP());
        commandsHashMap.put(TelemetryRotatorConstants.PARK, new PARK());
        commandsHashMap.put(TelemetryRotatorConstants.MOVE, new MOVE());
        commandsHashMap.put(TelemetryRotatorConstants.CAPABILITIES, new CAPABILITIES());
        commandsHashMap.put(TelemetryRotatorConstants.RESET, new RESET());
        commandsHashMap.put(TelemetryRotatorConstants.SET_CONFIG, new SET_CONFIG());
        commandsHashMap.put(TelemetryRotatorConstants.GET_INFO, new GET_INFO());
        commandsHashMap.put(TelemetryRotatorConstants.SEND_RAW_CMD, new SEND_RAW_CMD());
        commandsHashMap.put(TelemetryRotatorConstants.LONLAT2LOC, new LONLAT2LOC());
        commandsHashMap.put(TelemetryRotatorConstants.LOC2LONLAT, new LOC2LONLAT());
        commandsHashMap.put(TelemetryRotatorConstants.DMS2DEC, new DMS2DEC());
        commandsHashMap.put(TelemetryRotatorConstants.DEC2DMS, new DEC2DMS());
        commandsHashMap.put(TelemetryRotatorConstants.DMMM2DEC, new DMMM2DEC());
        commandsHashMap.put(TelemetryRotatorConstants.DEC2DMMM, new DEC2DMMM());
        commandsHashMap.put(TelemetryRotatorConstants.QRB, new QRB());
        commandsHashMap.put(TelemetryRotatorConstants.AZIMUTH_SHORTPATH2LONGPATH, new AZIMUTH_SHORTPATH2LONGPATH());
        commandsHashMap.put(TelemetryRotatorConstants.DISTANCE_SHORTPATH2LONGPATH, new DISTANCE_SHORTPATH2LONGPATH());
        commandsHashMap.put(TelemetryRadioConstants.HELP, new HELP());
        commandsHashMap.put(TelemetryRotatorConstants.RotatorStatus, new RotatorStatus());

    }
    
    @Override
    protected Object encode(ChannelHandlerContext ctx, Channel channel, Object message) throws Exception {
        if(message==null){
            return ChannelBuffers.EMPTY_BUFFER;
        }       
        TelemetryCommand telemetryCommand = (TelemetryCommand) message;
        StringBuilder messageString=new StringBuilder();
        messageString.append(commandsHashMap.get(telemetryCommand.getName()).createMessageString(telemetryCommand));
 

        return messageString;
    }
}
