package eu.estcube.gsconnector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.domain.TelemetryRotatorConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;
import eu.estcube.gsconnector.commands.radio.*;
import eu.estcube.gsconnector.commands.rotator.SET_POSITION;


@Component("storeSetValues")
public final class StoreSetValues {        
    public static Map<String, TelemetryCommand> dataStore ;

    private static final Logger LOG = LoggerFactory.getLogger(StoreSetValues.class);
    private Map<String, CommandStringBuilder> commandsHashMap;
    
    public StoreSetValues(){
        dataStore = new HashMap<String, TelemetryCommand>();
        commandsHashMap = new HashMap<String, CommandStringBuilder>();
        commandsHashMap.put(TelemetryRotatorConstants.SET_POSITION, new SET_POSITION());        
        commandsHashMap.put(TelemetryRadioConstants.SET_ANTENNA_NR, new SET_ANTENNA_NR());
        commandsHashMap.put(TelemetryRadioConstants.SET_CTCSS_TONE, new SET_CTCSS_TONE());
        commandsHashMap.put(TelemetryRadioConstants.SET_DCS_CODE, new SET_DCS_CODE());
        commandsHashMap.put(TelemetryRadioConstants.SET_FREQUENCY, new SET_FREQUENCY());
        commandsHashMap.put(TelemetryRadioConstants.SET_MEMORY_CHANNELNR, new SET_MEMORY_CHANNELNR());
        commandsHashMap.put(TelemetryRadioConstants.SET_MODE, new SET_MODE());
        commandsHashMap.put(TelemetryRadioConstants.SET_PTT, new SET_PTT());
        commandsHashMap.put(TelemetryRadioConstants.SET_RIT, new SET_RIT());
        commandsHashMap.put(TelemetryRadioConstants.SET_RPTR_OFFSET, new SET_RPTR_OFFSET());
        commandsHashMap.put(TelemetryRadioConstants.SET_RPTR_SHIFT, new SET_RPTR_SHIFT());
        commandsHashMap.put(TelemetryRadioConstants.SET_SPLIT_VFO, new SET_SPLIT_VFO());
        commandsHashMap.put(TelemetryRadioConstants.SET_TRANCEIVE_MODE, new SET_TRANCEIVE_MODE());
        commandsHashMap.put(TelemetryRadioConstants.SET_TRANSMIT_FREQUENCY, new SET_TRANSMIT_FREQUENCY());
        commandsHashMap.put(TelemetryRadioConstants.SET_TRANSMIT_MODE, new SET_TRANSMIT_MODE());
        commandsHashMap.put(TelemetryRadioConstants.SET_TUNING_STEP, new SET_TUNING_STEP());
        commandsHashMap.put(TelemetryRadioConstants.SET_VFO, new SET_VFO());
    }

    public void storeData(Exchange exchange, @Body String body) { 
        TelemetryCommand command = (TelemetryCommand) exchange.getIn().getBody();
        exchange.getIn().setHeader("polling", "notRequired");
        if(commandsHashMap.get(command.getName())!=null){
            dataStore.put(command.getName(), command);
            exchange.getIn().setHeader("polling", "setCommand");
        }
    }
}