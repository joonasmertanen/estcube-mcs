package eu.estcube.gsconnector;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("hamlibDecoder")
public class HamlibDecoder extends OneToOneDecoder {

    private static final Logger LOG = LoggerFactory.getLogger(HamlibDecoder.class);
    private StringBuffer messageBuffer=new StringBuffer();
    
        @Override
        protected Object decode(ChannelHandlerContext ctx, Channel evt, Object msg) throws Exception {
            String checkRPRT = msg.toString();
            messageBuffer.append(checkRPRT);
            
            if(checkRPRT.contains("RPRT")){
                String message = messageBuffer.toString();
                messageBuffer.setLength(0);
                return message;
            }
            return null;
                        
        }    

}
