package eu.estcube.gsconnector;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.Delimiters;
import org.jboss.netty.handler.codec.frame.DelimiterBasedFrameDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("newLineDecoder")
class NewLineDecoder extends DelimiterBasedFrameDecoder {
    private static final Logger LOG = LoggerFactory.getLogger(NewLineDecoder.class);
            
    protected NewLineDecoder(){
        super(10240, false, Delimiters.lineDelimiter());
    }
    
    @Override
    public Object decode(ChannelHandlerContext ctx, Channel evt, ChannelBuffer arg2) throws Exception {  
        return super.decode(ctx, evt, arg2);
    }}