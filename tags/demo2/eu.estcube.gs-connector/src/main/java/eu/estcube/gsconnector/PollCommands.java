package eu.estcube.gsconnector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.impl.DefaultMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.domain.TelemetryRotatorConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;
import eu.estcube.gsconnector.commands.rotator.*;


@Component("pollCommands")
public final class PollCommands {        

    private Map<String, String> commandsSetToGet;
    private Map<String, String> commandsGetToSet;
    private List<String> commandsWithNumberResponse;
    

    private static final Logger LOG = LoggerFactory.getLogger(PollCommands.class);
    
    public PollCommands(){
        commandsSetToGet = new HashMap<String, String>();
        commandsGetToSet = new HashMap<String, String>();
        commandsWithNumberResponse = new ArrayList<String>();
        commandsSetToGet.put(TelemetryRotatorConstants.SET_POSITION, TelemetryRotatorConstants.GET_POSITION);
        commandsSetToGet.put(TelemetryRadioConstants.SET_ANTENNA_NR, TelemetryRadioConstants.GET_ANTENNA_NR);
        commandsSetToGet.put(TelemetryRadioConstants.SET_CTCSS_TONE, TelemetryRadioConstants.GET_CTCSS_TONE);
        commandsSetToGet.put(TelemetryRadioConstants.SET_DCS_CODE, TelemetryRadioConstants.GET_DCS_CODE);
        commandsSetToGet.put(TelemetryRadioConstants.SET_FREQUENCY, TelemetryRadioConstants.GET_FREQUENCY);
        commandsSetToGet.put(TelemetryRadioConstants.SET_MEMORY_CHANNELNR, TelemetryRadioConstants.GET_MEMORY_CHANNELNR);
        commandsSetToGet.put(TelemetryRadioConstants.SET_MODE, TelemetryRadioConstants.GET_MODE);
        commandsSetToGet.put(TelemetryRadioConstants.SET_PTT, TelemetryRadioConstants.GET_PTT);
        commandsSetToGet.put(TelemetryRadioConstants.SET_RIT, TelemetryRadioConstants.GET_RIT);
        commandsSetToGet.put(TelemetryRadioConstants.SET_RPTR_OFFSET, TelemetryRadioConstants.GET_RPTR_OFFSET);
        commandsSetToGet.put(TelemetryRadioConstants.SET_RPTR_SHIFT, TelemetryRadioConstants.GET_RPTR_SHIFT);
        commandsSetToGet.put(TelemetryRadioConstants.SET_SPLIT_VFO, TelemetryRadioConstants.GET_SPLIT_VFO);
        commandsSetToGet.put(TelemetryRadioConstants.SET_TRANCEIVE_MODE, TelemetryRadioConstants.GET_TRANCEIVE_MODE);
        commandsSetToGet.put(TelemetryRadioConstants.SET_TRANSMIT_FREQUENCY, TelemetryRadioConstants.GET_TRANSMIT_FREQUENCY);
        commandsSetToGet.put(TelemetryRadioConstants.SET_TRANSMIT_MODE, TelemetryRadioConstants.GET_TRANSMIT_MODE);
        commandsSetToGet.put(TelemetryRadioConstants.SET_TUNING_STEP, TelemetryRadioConstants.GET_TUNING_STEP);
        commandsSetToGet.put(TelemetryRadioConstants.SET_VFO, TelemetryRadioConstants.GET_VFO);
        
        commandsGetToSet.put(TelemetryRotatorConstants.GET_POSITION, TelemetryRotatorConstants.SET_POSITION);
        commandsSetToGet.put(TelemetryRadioConstants.GET_ANTENNA_NR, TelemetryRadioConstants.SET_ANTENNA_NR);
        commandsSetToGet.put(TelemetryRadioConstants.GET_CTCSS_TONE, TelemetryRadioConstants.SET_CTCSS_TONE);
        commandsSetToGet.put(TelemetryRadioConstants.GET_DCS_CODE, TelemetryRadioConstants.SET_DCS_CODE);
        commandsSetToGet.put(TelemetryRadioConstants.GET_FREQUENCY, TelemetryRadioConstants.SET_FREQUENCY);
        commandsSetToGet.put(TelemetryRadioConstants.GET_MEMORY_CHANNELNR, TelemetryRadioConstants.SET_MEMORY_CHANNELNR);
        commandsSetToGet.put(TelemetryRadioConstants.GET_MODE, TelemetryRadioConstants.SET_MODE);
        commandsSetToGet.put(TelemetryRadioConstants.GET_PTT, TelemetryRadioConstants.SET_PTT);
        commandsSetToGet.put(TelemetryRadioConstants.GET_RIT, TelemetryRadioConstants.SET_RIT);
        commandsSetToGet.put(TelemetryRadioConstants.GET_RPTR_OFFSET, TelemetryRadioConstants.SET_RPTR_OFFSET);
        commandsSetToGet.put(TelemetryRadioConstants.GET_RPTR_SHIFT, TelemetryRadioConstants.SET_RPTR_SHIFT);
        commandsSetToGet.put(TelemetryRadioConstants.GET_SPLIT_VFO, TelemetryRadioConstants.SET_SPLIT_VFO);
        commandsSetToGet.put(TelemetryRadioConstants.GET_TRANCEIVE_MODE, TelemetryRadioConstants.SET_TRANCEIVE_MODE);
        commandsSetToGet.put(TelemetryRadioConstants.GET_TRANSMIT_FREQUENCY, TelemetryRadioConstants.SET_TRANSMIT_FREQUENCY);
        commandsSetToGet.put(TelemetryRadioConstants.GET_TRANSMIT_MODE, TelemetryRadioConstants.SET_TRANSMIT_MODE);
        commandsSetToGet.put(TelemetryRadioConstants.GET_TUNING_STEP, TelemetryRadioConstants.SET_TUNING_STEP);
        commandsSetToGet.put(TelemetryRadioConstants.GET_VFO, TelemetryRadioConstants.SET_VFO);
        
        commandsWithNumberResponse.add("Azimuth");
        commandsWithNumberResponse.add("Elevation");
    }

    public List<Message> checkPolling(Exchange exchange) { 
        List<Message> messageList = new ArrayList<Message>();
        TelemetryObject response = (TelemetryObject) exchange.getIn().getBody();
        
        createOriginalMessage(exchange, messageList);

        LOG.debug("HEADER: {}",exchange.getIn().getHeaders());
        if(exchange.getIn().getHeader("polling").equals("setCommand")){

            if(response.getParameter("RPRT").getValue().equals("0")){

                createPollingMessage(exchange, messageList, response);
            }
        }
        if(exchange.getIn().getHeader("polling").equals("getCommand")){

            checkStatusUpdate(exchange, messageList, response);
        }
//        for(int i = 0;i<messageList.size();i++){
//            LOG.debug("HEADER: {}",messageList.get(i).getHeaders());
//            LOG.debug("BODY: {}",messageList.get(i).getBody());
//            LOG.debug("COUNT: {}",i);
//        }

        LOG.debug("HEADER: {}",exchange.getIn().getHeaders());
        return messageList;
        }

    private void checkStatusUpdate(Exchange exchange, List<Message> messageList, TelemetryObject response) {
        for(int i = 1; i < response.getParams().size()-1;i++){
            String dataStoreKey = commandsGetToSet.get(response.getName());

            if(commandsWithNumberResponse.contains(response.getParams().get(i).getName())){
                    double currentParameterValue = Double.parseDouble(response.getParams().get(i).getValue().toString());
                    double requiredParameterValue = Double.parseDouble(StoreSetValues.dataStore.get(dataStoreKey).getParameter(
                            response.getParams().get(i).getName()).toString());
//                        LOG.debug("GETSTATUS DOUBLE CONVERTED: {}, BUT IS : {}",currentParameterValue, requiredParameterValue);
                    if(!(currentParameterValue==requiredParameterValue)){
                        LOG.debug("WIN");
                        createPollingMessage(exchange, messageList, response);
                        break;
                    }
                    break;
            }
            else{
            String currentParameterValue = response.getParams().get(i).getValue().toString();
            String requiredParameterValue = StoreSetValues.dataStore.get(dataStoreKey).getParameter(
                    response.getParams().get(i).getName()).toString();
//                LOG.debug("GETSTATUS STILL STRING: {}, BUT IS : {}",currentParameterValue, requiredParameterValue);
            if(!currentParameterValue.equals(requiredParameterValue)){
                createPollingMessage(exchange, messageList, response);
                break;
            }
            }
        }
    }

    
    private void createOriginalMessage(Exchange exchange, List<Message> messageList) {
        DefaultMessage originalMessage = new DefaultMessage();
        originalMessage.setBody(exchange.getIn().getBody());
        originalMessage.setHeader("device", exchange.getIn().getHeader("device"));
        originalMessage.setHeader("groundStationID", exchange.getIn().getHeader("groundStationID"));
        originalMessage.setHeader("forward", "direct:gsSend");
        messageList.add(originalMessage);
    }

    private void createPollingMessage(Exchange exchange, List<Message> messageList, TelemetryObject response) {
        DefaultMessage pollingMessage = new DefaultMessage();
        
        if(exchange.getIn().getHeader("polling").equals("getCommand")){
            pollingMessage.setBody(new TelemetryCommand(response.getName()));
        }
        else{
            pollingMessage.setBody(new TelemetryCommand(commandsSetToGet.get(response.getName())));
        }
        pollingMessage.setHeader("device", exchange.getIn().getHeader("device"));
        pollingMessage.setHeader("groundStationID", exchange.getIn().getHeader("groundStationID"));
        pollingMessage.setHeader("polling", "getCommand");
        pollingMessage.setHeader("forward", "direct:chooseDevice");
        messageList.add(pollingMessage);
        }
    }
