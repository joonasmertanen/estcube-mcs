package eu.estcube.parameterstorage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryParameter;

public class DbToTelemetryObject implements Processor {
    private static final Logger LOG = LoggerFactory.getLogger(DbToTelemetryObject.class);
    private JdbcTemplate jdbcTemplate;
    private Long id;
    private List<TelemetryObject> telemetryObjects = new ArrayList<TelemetryObject>();

    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void process(Exchange ex) throws Exception {
        int rowCount = jdbcTemplate.queryForInt("select count(0) from TELEMETRYOBJECTS");

        if (rowCount > 0) {
            List<Map<String, Object>> distinctObjectsByGs = (List<Map<String, Object>>) jdbcTemplate
                    .queryForList("SELECT DISTINCT NAME, SOURCE, DEVICE FROM TELEMETRYOBJECTS;");

            for (Map<String, Object> row : distinctObjectsByGs) {
                TelemetryObject telObjtoAdd = (TelemetryObject) jdbcTemplate
                        .queryForObject(
                                "SELECT * FROM TELEMETRYOBJECTS WHERE "
                                        + "TIME =(SELECT MAX(TIME) FROM TELEMETRYOBJECTS WHERE NAME =? AND SOURCE = ? AND DEVICE=?);",
                                new RowMapper<TelemetryObject>() {
                                    public TelemetryObject mapRow(ResultSet rs, int rowNum) throws SQLException {
                                        TelemetryObject telObj = new TelemetryObject(rs.getString("NAME"));
                                        telObj.setSource(rs.getString("SOURCE"));
                                        telObj.setDevice(rs.getString("DEVICE"));
                                        id = rs.getLong("ID");
                                        return telObj;
                                    }
                                }, row.get("NAME"), row.get("SOURCE"), row.get("DEVICE"));

                String sql = "SELECT * FROM PARAMETERS WHERE ID=?";
                List<TelemetryParameter> params = new ArrayList<TelemetryParameter>();
                List<Map<String, Object>> paramRows = jdbcTemplate.queryForList(sql, id);
                
                for (Map<String, Object> paramRow : paramRows) {
                    TelemetryParameter parameter = new TelemetryParameter((String) paramRow.get("KEY"),
                            (String) paramRow.get("VALUE"));
                    params.add(parameter);
                }
                telObjtoAdd.addParams(params);
                telemetryObjects.add(telObjtoAdd);
            }
            ex.getOut().setBody(new ArrayList<TelemetryObject>(telemetryObjects));
            ex.getOut().setHeader("fromPS", "true");
            telemetryObjects.clear();
        } else {
            // nothing to return from DB
            ex.getOut().setBody(new ArrayList<TelemetryObject>());
            ex.getOut().setHeader("fromPS", "true");
        }
    }
}
