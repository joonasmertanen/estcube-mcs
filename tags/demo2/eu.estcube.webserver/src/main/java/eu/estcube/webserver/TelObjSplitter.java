package eu.estcube.webserver;

import java.util.ArrayList;
import java.util.List;
import org.apache.camel.Body;
import org.apache.camel.Message;
import org.apache.camel.impl.DefaultMessage;
import org.springframework.stereotype.Component;

import eu.estcube.domain.TelemetryObject;

@Component
public class TelObjSplitter {
   
    /**
     * The split message method returns something that is iteratable such as a java.util.List.
     *
     * @param body the payload of the incoming message
     * @return a list containing each part splitted
     */
    public List<Message> splitMessage(@Body List<TelemetryObject> telemetryObjects) {
        List<Message> answer = new ArrayList<Message>();
        
        for (TelemetryObject telemetryObject : telemetryObjects) {
            DefaultMessage message = new DefaultMessage();
            message.setHeader("groundStationID", telemetryObject.getSource());
            message.setHeader("device", telemetryObject.getDevice()); 
            message.setBody(telemetryObject);

            answer.add(message);
        }
        return answer;
    }
}