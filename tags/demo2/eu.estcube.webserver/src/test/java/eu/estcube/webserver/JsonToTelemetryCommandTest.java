package eu.estcube.webserver;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import eu.estcube.domain.TelemetryCommand;

public class JsonToTelemetryCommandTest {

	private JsonToTelemetryCommand jsonToTelemetryCommand;
	private Exchange ex;
	private Message message;
	private Object answer;

	@Before
	public void setUp() throws Exception {
		String json = " {\"name\":\"nimi\",\"params\":[{\"name\":\"parameeter1\",\"value\":\"2.5\"},"
				+ "{\"name\":\"parameeter2\",\"value\":\"X5 Y2 Z7\"}]}";
		message = Mockito.mock(Message.class);
		Mockito.when(message.getBody(String.class)).thenReturn(json);

		jsonToTelemetryCommand = new JsonToTelemetryCommand();
		ex = Mockito.mock(Exchange.class);

		Mockito.when(ex.getOut()).thenReturn(message);

		Mockito.doAnswer(new Answer<Object>() {
			public Object answer(InvocationOnMock invocation) {
				answer = invocation.getArguments()[0];
				return answer;
			}
		}).when(message).setBody(Mockito.any());
	}

	@Test
	public void testProcess() throws Exception {
		jsonToTelemetryCommand.process(ex);

		assertEquals("nimi", ((TelemetryCommand) answer).getName());
		Map<String, Object> parameters = ((TelemetryCommand) answer)
				.getParams();
		assertEquals("2.5", parameters.get("parameeter1"));
		assertEquals("X5 Y2 Z7", parameters.get("parameeter2"));
	}

}
