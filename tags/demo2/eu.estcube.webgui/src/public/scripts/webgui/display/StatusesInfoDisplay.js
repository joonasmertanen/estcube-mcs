dojo.provide("webgui.display.StatusesInfoDisplay");


dojo.declare("webgui.display.StatusesInfoDisplay", null, {
	
	constructor: function() {

		console.log("StatusesInfoDisplay activated");
			
		var statusesInfoDisplay;
		
		statusesInfoDisplay = dojo.create("div", {dojoType: "dijit.layout.BorderContainer",
		id: "InfoViewInner", 
		region: "leading",
		innerHTML: "<p>Hi! This is Statuses info view!</p>",
		});

		dojo.place(statusesInfoDisplay, "InfoViewInner", "replace");
	}
});