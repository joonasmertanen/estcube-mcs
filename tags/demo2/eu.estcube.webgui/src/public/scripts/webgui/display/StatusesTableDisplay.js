dojo.provide("webgui.display.StatusesTableDisplay");

dojo.require("dijit.form.TextBox");
dojo.require("dijit.form.Button");
dojo.require("dijit.layout.ContentPane");
dojo.require("dijit.layout.TabContainer");
dojo.require("dojox.grid.DataGrid");

dojo.declare("webgui.display.StatusesTableDisplay", null, {
	
	constructor: function() {

		console.log("StatusesTableDisplay activated");

		var exists = dijit.byId("PictureViewInner");
		
		var componentInfoContainer = new dijit.layout.TabContainer();
		
		if(exists) {
			console.log("It's alive");
			exists.destroy();
			var exists2 = dijit.byId("PictureViewInner");
			if(exists2){
				console.log("Did not deleted");
			}
		}
		
		
		
		var tableHeaders = [{
            field: 'Component',
            name: 'Component',
            width: '30%'
        },
        {
            field: 'Info',
            name: 'Info',
            width: '70%'
        }];

        // create a new grid:
        var dataField = new dojox.grid.DataGrid({
            query: {
                Title: 'Object Info'
            },
            clientSort: true,
            rowSelector: '20px',
            structure: tableHeaders
        }, dojo.create("div",{id: "PictureViewInner"}));
        

        dojo.byId("PictureView").appendChild(dataField.domNode);
        
        dataField.startup();
		
		console.log("StatusesDisplay replaced");
	}
});