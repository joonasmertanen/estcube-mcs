dojo.provide("webgui.display.MainTabDisplay");

dojo.require("dijit.layout.TabContainer");
dojo.require("dijit.layout.ContentPane");

dojo.declare("webgui.display.MainTabDisplay", null, {
	
	constructor: function(view) {
				
		console.log("TabDisplay");
			
//		var exists = dijit.byId("CenterContainerInner");
//		if(exists) {
//			console.log("It's alive");
//			//exists.destroy();
//			
//			var exists2 = dijit.byId("CenterContainerInner");
//			if(exists2){
//				console.log("Did not deleted");
//			}
//		}

	        var telemetryTab = new dijit.layout.ContentPane({
	            title: "Telemetry",
	            content: "New telemetry data!"
	        });
	       

	        var gsTab = new dijit.layout.ContentPane({
	            title: "GS",
	            content: "Groundstation online!",
	            closable: "true"
	        });
	       
	        
	        var commandTab = new dijit.layout.ContentPane({
	        	id: "commandTab", // fix error
	        	title: "Command",
	        	content: "Insert Commands"
	        });
    
//	        var tabs = new dijit.layout.TabContainer({
//	        	region: "center",
//	        	id: "testID"
//	        }, "CenterContainer");
//	        
//	        //dojo.place(tabs, "CenterContainer", "first");
//	        
	        var tabs;
	        tabs = dijit.byId("CenterContainerInner");
	        
	        tabs.addChild(telemetryTab);
	        tabs.addChild(gsTab);
	        tabs.addChild(commandTab);
	        tabs.startup();
	        
	        var socket = dojox.socket("ws://localhost:9292/foo/");
	        
			dojo.create("div", {id: "commandArea", region: "center"}, "commandTab");
			console.log("tabs done");
			dojo.create("div", {id: "commandLogArea", region: "center", content: "Here are commands"}, "commandTab");
			
			
			console.log("before TextBox");
			var commandBox = new dijit.form.TextBox({
				id: "commandTextBox",
				value: ""
			}).placeAt("commandArea");
			
			
			console.log("before Button");
			var buttonSend = new dijit.form.Button({
				label: "Send",
				onClick: function() {
					var muutuja = dijit.byId("commandTextBox");
					var split = muutuja.get('value').split(" ");

	    			var params = []; 
	    			console.log(split.length%2);

					if ((split.length - 1) % 2 == 0) {
						for (i = 1; i < split.length - 1; i=i+2) {						
							params.push(new TelemetryParameter(split[i], split[i+1]));
						}
					}
	    		  	
	    			var telemetryCommand = new TelemetryCommand(split[0],params);

	    		  	socket.send(JSON.stringify(telemetryCommand))

	    			socket.on("message", function(message){ 
	    				console.log("Listening: I got ", message.data);
	    				var myObject = JSON.parse(message.data); //make object out of json
	    				//dojo.byId("commandLogArea").innerHTML = myObject.name + "<br>"; 
	    				return   JSON.stringify(message);
	    			});
				}
			}).placeAt("commandArea");
			
			
	    	function TelemetryParameter(name,value) {
	    		this.name=name;
	    		this.value=value;
	    	}
	    	
	    	function TelemetryCommand(name, params) {
	    		this.name = name;
	    		this.params = params;
	    	}

	}
});