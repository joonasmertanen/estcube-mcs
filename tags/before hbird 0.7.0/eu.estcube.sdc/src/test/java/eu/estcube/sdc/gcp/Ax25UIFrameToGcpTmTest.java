package eu.estcube.sdc.gcp;

import static org.mockito.Mockito.*;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.domain.transport.ax25.Ax25UIFrame;

@RunWith(MockitoJUnitRunner.class)
public class Ax25UIFrameToGcpTmTest {

	@Mock
    private Exchange exchange;
    
    @Mock
    private Message in;
    
    @Mock
    private Message out;
    
    @Mock
    private Ax25UIFrame ax25Frame;
    
    @Mock
    private GcpTm gcpTm;
    
    @Mock
    private GcpDecoder decoder;
    
    @InjectMocks
    private Ax25UIFrameToGcpTm toGcp;
    
    private InOrder inOrder;
    
    
    @Before
    public void setUp() throws Exception {
        inOrder = inOrder(exchange, in, out, ax25Frame, gcpTm, decoder);
        when(exchange.getIn()).thenReturn(in);
        when(exchange.getOut()).thenReturn(out);
        when(in.getBody(Ax25UIFrame.class)).thenReturn(ax25Frame);
        when(decoder.decode(eq(ax25Frame))).thenReturn(gcpTm);
    }
    
    
    @Test
    public void testProcess() throws Exception {
        toGcp.process(exchange);
        
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getBody(Ax25UIFrame.class);
        inOrder.verify(decoder, times(1)).decode(ax25Frame);
        inOrder.verify(out, times(1)).setBody(gcpTm);
        inOrder.verifyNoMoreInteractions();
    }
}
