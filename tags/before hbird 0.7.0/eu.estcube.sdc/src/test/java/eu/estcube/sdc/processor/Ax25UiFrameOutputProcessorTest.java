/** 
 *
 */
package eu.estcube.sdc.processor;

import static org.mockito.Mockito.*;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.hbird.exchange.constants.StandardArguments;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.common.Dates;
import eu.estcube.common.Headers;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.sdc.domain.Ax25UiFrameOutput;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class Ax25UiFrameOutputProcessorTest {

    private static final Long NOW = System.currentTimeMillis();
    private static final String ISSUER = "issuer";
    private static final String PORT = "/dev/tty123";
    
    @Mock
    private Exchange exchange;
    
    @Mock
    private Message in;
    
    @Mock
    private Message out;
    
    @Mock
    private Ax25UIFrame frame;
    
    @Mock
    private Ax25UiFrameToOutputConverter converter;
    
    @Mock
    private Ax25UiFrameOutput output;
    
    @InjectMocks
    private Ax25UiFrameOutputProcessor processor;
    
    private InOrder inOrder;
    
    private RuntimeException exception;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        inOrder = inOrder(exchange, in, out, frame, converter, output);
        exception = new RuntimeException("Mutchos proplemos");
        when(exchange.getIn()).thenReturn(in);
        when(exchange.getOut()).thenReturn(out);
        when(in.getBody(Ax25UIFrame.class)).thenReturn(frame);
        when(converter.convert(frame)).thenReturn(output);
        when(in.getHeader(Exchange.CREATED_TIMESTAMP)).thenReturn(NOW);
        when(in.getHeader(Exchange.CREATED_TIMESTAMP, Long.class)).thenReturn(NOW);
        when(in.getHeader(StandardArguments.ISSUED_BY, String.class)).thenReturn(ISSUER);
        when(in.getHeader(Headers.SERIAL_PORT_NAME, String.class)).thenReturn(PORT);
    }

    /**
     * Test method for {@link eu.estcube.sdc.processor.Ax25UiFrameOutputProcessor#process(org.apache.camel.Exchange)}.
     * @throws Exception 
     */
    @Test
    public void testProcess() throws Exception {
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getBody(Ax25UIFrame.class);
        inOrder.verify(converter, times(1)).convert(frame);
        inOrder.verify(in, times(1)).getHeader(Exchange.CREATED_TIMESTAMP);
        inOrder.verify(in, times(1)).getHeader(Exchange.CREATED_TIMESTAMP, Long.class);
        inOrder.verify(out, times(1)).setHeader(StandardArguments.TIMESTAMP, NOW);
        inOrder.verify(output, times(1)).setTimestamp(Dates.toIso8601DateFormat(NOW));
        inOrder.verify(in, times(1)).getHeader(StandardArguments.ISSUED_BY, String.class);
        inOrder.verify(output, times(1)).setServiceId(ISSUER);
        inOrder.verify(in, times(1)).getHeader(Headers.SERIAL_PORT_NAME, String.class);
        inOrder.verify(output, times(1)).setSerialPortName(PORT);
        inOrder.verify(out, times(1)).setBody(output);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.sdc.processor.Ax25UiFrameOutputProcessor#process(org.apache.camel.Exchange)}.
     * @throws Exception 
     */
    @Test
    public void testProcessWithException() throws Exception {
        when(converter.convert(frame)).thenThrow(exception);
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getBody(Ax25UIFrame.class);
        inOrder.verify(converter, times(1)).convert(frame);
        inOrder.verify(exchange, times(1)).setException(exception);
        inOrder.verifyNoMoreInteractions();
    }
}
