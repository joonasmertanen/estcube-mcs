package eu.estcube.sdc.gcp;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.sdc.gcp.GcpCommand;

public class GcpCommandTest {

	@Test
	public void testConstructors1() {
		
		byte[] data = {124,2};
		GcpCommand packet = new GcpCommand( 1, 0, 5, 682, 4, 3, data );
		
		assertEquals( packet.getImmediate(), 1 );
		assertEquals( packet.getPriority(), 0 );
		assertEquals( packet.getDst(), 5 );
		assertEquals( packet.getId(), 682 );
		assertEquals( packet.getSrc(), 4 );
		assertEquals( packet.getIndex(), 3 );
		assertEquals( packet.getLength(), 2 );
		assertEquals( packet.getData(), data );
		
		byte[] bytes = packet.getBytes();
		for( byte s : bytes) {
			System.out.println(s);
		}
		GcpCommand command2 = new GcpCommand( bytes );
		assertEquals( command2.getImmediate(), 1 );
		assertEquals( command2.getPriority(), 0 );
		assertEquals( command2.getDst(), 5 );
		assertEquals( command2.getId(), 682 );
		assertEquals( command2.getSrc(), 4 );
		assertEquals( command2.getIndex(), 3 );
		assertEquals( command2.getLength(), 2 );
		assertEquals( command2.getData()[0], data[0] );
		assertEquals( command2.getData()[1], data[1] );
		assertEquals( command2.getData().length, data.length );
	}
	
	@Test
	public void testConstructors2() {
		
		byte[] data = {124,2};
		GcpCommand packet = new GcpCommand( 1, 0, 5, 341, 4, 3, data );
		
		assertEquals( packet.getImmediate(), 1 );
		assertEquals( packet.getPriority(), 0 );
		assertEquals( packet.getDst(), 5 );
		assertEquals( packet.getId(), 341 );
		assertEquals( packet.getSrc(), 4 );
		assertEquals( packet.getIndex(), 3 );
		assertEquals( packet.getLength(), 2 );
		assertEquals( packet.getData(), data );
		
		byte[] bytes = packet.getBytes();
		
		GcpCommand command2 = new GcpCommand( bytes );
		assertEquals( command2.getImmediate(), 1 );
		assertEquals( command2.getPriority(), 0 );
		assertEquals( command2.getDst(), 5 );
		assertEquals( command2.getId(), 341 );
		assertEquals( command2.getSrc(), 4 );
		assertEquals( command2.getIndex(), 3 );
		assertEquals( command2.getLength(), 2 );
		assertEquals( command2.getData()[0], data[0] );
		assertEquals( command2.getData()[1], data[1] );
		assertEquals( command2.getData().length, data.length );
		
	}
}
