/** 
 *
 */
package eu.estcube.sdc.processor;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.common.ByteUtil;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.sdc.domain.Ax25UiFrameInput;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class InputToAx25UIFrameConverterTest {

    private static final String VALID_BYTES = "0D 0E FF FF 0A 01 00";
    private static final String INVALID_BYTES = "0D 0E FF FF 0A 01";
    private static final String VALID_BYTE = "0C";
    private static final String INVALID_BYTE = "0C 09";
    
    @Mock
    private Ax25UiFrameInput input;
    
    private InputToAx25UIFrameConverter converter;
    
    private InOrder inOrder;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        converter = new InputToAx25UIFrameConverter();
        inOrder = inOrder(input);
        when(input.getDestAddr()).thenReturn(VALID_BYTES);
        when(input.getSrcAddr()).thenReturn(VALID_BYTES);
        when(input.getCtrl()).thenReturn(VALID_BYTE);
        when(input.getPid()).thenReturn(VALID_BYTE);
        when(input.getInfo()).thenReturn(VALID_BYTES);
    }

    /**
     * Test method for {@link eu.estcube.sdc.processor.InputToAx25UIFrameConverter#convert(eu.estcube.sdc.domain.Ax25UiFrameInput)}.
     */
    @Test
    public void testConvert() {
        Ax25UIFrame frame = converter.convert(input);
        assertTrue(Arrays.equals(ByteUtil.toBytesFromHexString(VALID_BYTES), frame.getDestAddr()));
        assertTrue(Arrays.equals(ByteUtil.toBytesFromHexString(VALID_BYTES), frame.getSrcAddr()));
        assertEquals(ByteUtil.toBytesFromHexString(VALID_BYTE)[0], frame.getCtrl());
        assertEquals(ByteUtil.toBytesFromHexString(VALID_BYTE)[0], frame.getPid());
        assertTrue(Arrays.equals(ByteUtil.toBytesFromHexString(VALID_BYTES), frame.getInfo()));
        inOrder.verify(input, times(1)).getDestAddr();
        inOrder.verify(input, times(1)).getSrcAddr();
        inOrder.verify(input, times(1)).getCtrl();
        inOrder.verify(input, times(1)).getPid();
        inOrder.verify(input, times(1)).getInfo();
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.sdc.processor.InputToAx25UIFrameConverter#convert(eu.estcube.sdc.domain.Ax25UiFrameInput)}.
     */
    @Test
    public void testConvertInvalidDestAddr() {
        when(input.getDestAddr()).thenReturn(INVALID_BYTES);
        try {
            converter.convert(input);
            fail("Exception expected");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
        inOrder.verify(input, times(1)).getDestAddr();
        inOrder.verifyNoMoreInteractions();
    }
    
    /**
     * Test method for {@link eu.estcube.sdc.processor.InputToAx25UIFrameConverter#convert(eu.estcube.sdc.domain.Ax25UiFrameInput)}.
     */
    @Test
    public void testConvertInvalidSrcAddr() {
        when(input.getSrcAddr()).thenReturn(INVALID_BYTES);
        try {
            converter.convert(input);
            fail("Exception expected");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
        inOrder.verify(input, times(1)).getDestAddr();
        inOrder.verify(input, times(1)).getSrcAddr();
        inOrder.verifyNoMoreInteractions();
    }
    
    /**
     * Test method for {@link eu.estcube.sdc.processor.InputToAx25UIFrameConverter#convert(eu.estcube.sdc.domain.Ax25UiFrameInput)}.
     */
    @Test
    public void testConvertInvalidCtrl() {
        when(input.getCtrl()).thenReturn(INVALID_BYTE);
        try {
            converter.convert(input);
            fail("Exception expected");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
        inOrder.verify(input, times(1)).getDestAddr();
        inOrder.verify(input, times(1)).getSrcAddr();
        inOrder.verify(input, times(1)).getCtrl();
        inOrder.verifyNoMoreInteractions();
    }
    
    /**
     * Test method for {@link eu.estcube.sdc.processor.InputToAx25UIFrameConverter#convert(eu.estcube.sdc.domain.Ax25UiFrameInput)}.
     */
    @Test
    public void testConvertInvalidPid() {
        when(input.getPid()).thenReturn(INVALID_BYTE);
        try {
            converter.convert(input);
            fail("Exception expected");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
        inOrder.verify(input, times(1)).getDestAddr();
        inOrder.verify(input, times(1)).getSrcAddr();
        inOrder.verify(input, times(1)).getCtrl();
        inOrder.verify(input, times(1)).getPid();
        inOrder.verifyNoMoreInteractions();
    }
    
    /**
     * Test method for {@link eu.estcube.sdc.processor.InputToAx25UIFrameConverter#convert(eu.estcube.sdc.domain.Ax25UiFrameInput)}.
     */
    @Test
    public void testConvertInvalidInfo() {
        Random rnd = new Random();
        byte[] bytes = new byte[Ax25UIFrame.INFO_MAX_SIZE + 1];
        rnd.nextBytes(bytes);
        when(input.getInfo()).thenReturn(ByteUtil.toHexString(bytes));
        try {
            converter.convert(input);
            fail("Exception expected");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
        inOrder.verify(input, times(1)).getDestAddr();
        inOrder.verify(input, times(1)).getSrcAddr();
        inOrder.verify(input, times(1)).getCtrl();
        inOrder.verify(input, times(1)).getPid();
        inOrder.verify(input, times(1)).getInfo();
        inOrder.verifyNoMoreInteractions();
    }
}
