/** 
 *
 */
package eu.estcube.sdc.processor;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.hbird.exchange.constants.StandardArguments;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.common.Dates;
import eu.estcube.sdc.domain.Ax25UiFrameOutput;
import eu.estcube.sdc.xml.Ax25UiFrameOutputSerializer;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class Ax25UiFrameOutputToFileProcessorTest {

    private static final Long NOW = System.currentTimeMillis();
    private static final byte[] SERIALIZED = new byte[] { 0x0D, 0x0E, 0x0A, 0x0D, 0x0C, 0x00, 0x0D, 0x0E };
    
    @Mock
    private Exchange exchange;
    
    @Mock
    private Message in;
    
    @Mock
    private Message out;
    
    @Mock
    private Ax25UiFrameOutput output;
    
    @Mock
    private Ax25UiFrameOutputSerializer serializer;
    
    @InjectMocks
    private Ax25UiFrameOutputToFileProcessor processor;
    
    private InOrder inOrder;
    
    private RuntimeException exception;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        inOrder = inOrder(exchange, in, out, output, serializer);
        exception = new RuntimeException("Mutchos problemos");
        when(exchange.getIn()).thenReturn(in);
        when(exchange.getOut()).thenReturn(out);
        when(in.getBody(Ax25UiFrameOutput.class)).thenReturn(output);
        when(in.getHeader(StandardArguments.TIMESTAMP, Long.class)).thenReturn(NOW);
        when(serializer.serialize(output)).thenReturn(SERIALIZED);
    }

    /**
     * Test method for {@link eu.estcube.sdc.processor.Ax25UiFrameOutputToFileProcessor#process(org.apache.camel.Exchange)}.
     * @throws Exception 
     */
    @Test
    public void testProcess() throws Exception {
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getBody(Ax25UiFrameOutput.class);
        inOrder.verify(serializer, times(1)).serialize(output);
        inOrder.verify(out, times(1)).setBody(SERIALIZED);
        inOrder.verify(in, times(1)).getHeader(StandardArguments.TIMESTAMP, Long.class);
        inOrder.verify(out, times(1)).setHeader(Exchange.FILE_NAME, Dates.toDateInFileNameFormat(NOW) + ".xml");
        inOrder.verifyNoMoreInteractions();
    }
    
    /**
     * Test method for {@link eu.estcube.sdc.processor.Ax25UiFrameOutputToFileProcessor#process(org.apache.camel.Exchange)}.
     * @throws Exception 
     */
    @Test
    public void testProcessWithException() throws Exception {
        when(serializer.serialize(output)).thenThrow(exception);
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getBody(Ax25UiFrameOutput.class);
        inOrder.verify(serializer, times(1)).serialize(output);
        inOrder.verify(exchange, times(1)).setException(exception);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.sdc.processor.Ax25UiFrameOutputToFileProcessor#getFileName(java.text.DateFormat, long)}.
     */
    @Test
    public void testGetFileName() {
        assertEquals(Dates.toDateInFileNameFormat(NOW) + ".xml", processor.getFileName(Dates.toDateInFileNameFormat(NOW)));
    }
}
