/** 
 *
 */
package eu.estcube.sdc.processor;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.common.Headers;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.domain.transport.tnc.TncFrame;
import eu.estcube.sdc.domain.Ax25UiFrameInput;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class Ax25UiFrameInputProcessorTest {

    private static final String PREFIX = "0C 0D";
    
    @Mock
    private Exchange exchange;
    
    @Mock
    private Message in;
    
    @Mock
    private Message out;
    
    @Mock
    private InputToAx25UIFrameConverter converter;
    
    @Mock
    private Ax25UiFrameInput input;
    
    @Mock
    private Ax25UIFrame frame;
    
    @InjectMocks
    private Ax25UiFrameInputProcessor processor;
    
    private InOrder inOrder;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        inOrder =inOrder(exchange, in, out, converter, input, frame);
        when(exchange.getIn()).thenReturn(in);
        when(exchange.getOut()).thenReturn(out);
        when(in.getBody(Ax25UiFrameInput.class)).thenReturn(input);
        when(converter.convert(input)).thenReturn(frame);
        when(input.getTncTargetPort()).thenReturn(TncFrame.TARGET_MAX_VALUE);
        when(input.getPrefix()).thenReturn(PREFIX);
    }

    /**
     * Test method for {@link eu.estcube.sdc.processor.Ax25UiFrameInputProcessor#process(org.apache.camel.Exchange)}.
     * @throws Exception 
     */
    @Test
    public void testProcess() throws Exception {
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getBody(Ax25UiFrameInput.class);
        inOrder.verify(converter, times(1)).convert(input);
        inOrder.verify(input, times(1)).getTncTargetPort();
        inOrder.verify(out, times(1)).setHeader(Headers.TNC_PORT, TncFrame.TARGET_MAX_VALUE);
        inOrder.verify(input, times(1)).getPrefix();
        inOrder.verify(out, times(1)).setHeader(Headers.TNC_PREFIX, PREFIX);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.sdc.processor.Ax25UiFrameInputProcessor#process(org.apache.camel.Exchange)}.
     * @throws Exception 
     */
    @Test
    public void testProcessPortToSmall() throws Exception {
        when(input.getTncTargetPort()).thenReturn(TncFrame.TARGET_MIN_VALUE - 1);
        try {
            processor.process(exchange);
            fail("Exception expected");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getBody(Ax25UiFrameInput.class);
        inOrder.verify(converter, times(1)).convert(input);
        inOrder.verify(input, times(1)).getTncTargetPort();
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.sdc.processor.Ax25UiFrameInputProcessor#process(org.apache.camel.Exchange)}.
     * @throws Exception 
     */
    @Test
    public void testProcessPortToBig() throws Exception {
        when(input.getTncTargetPort()).thenReturn(TncFrame.TARGET_MAX_VALUE + 1);
        try {
            processor.process(exchange);
            fail("Exception expected");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getBody(Ax25UiFrameInput.class);
        inOrder.verify(converter, times(1)).convert(input);
        inOrder.verify(input, times(1)).getTncTargetPort();
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.sdc.processor.Ax25UiFrameInputProcessor#process(org.apache.camel.Exchange)}.
     * @throws Exception 
     */
    @Test
    public void testProcessNullPrefix() throws Exception {
        when(input.getPrefix()).thenReturn(null);
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getBody(Ax25UiFrameInput.class);
        inOrder.verify(converter, times(1)).convert(input);
        inOrder.verify(input, times(1)).getTncTargetPort();
        inOrder.verify(out, times(1)).setHeader(Headers.TNC_PORT, TncFrame.TARGET_MAX_VALUE);
        inOrder.verify(input, times(1)).getPrefix();
        inOrder.verifyNoMoreInteractions();
    }
    
    /**
     * Test method for {@link eu.estcube.sdc.processor.Ax25UiFrameInputProcessor#process(org.apache.camel.Exchange)}.
     * @throws Exception 
     */
    @Test
    public void testProcessBlankPrefix() throws Exception {
        when(input.getPrefix()).thenReturn("");
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getBody(Ax25UiFrameInput.class);
        inOrder.verify(converter, times(1)).convert(input);
        inOrder.verify(input, times(1)).getTncTargetPort();
        inOrder.verify(out, times(1)).setHeader(Headers.TNC_PORT, TncFrame.TARGET_MAX_VALUE);
        inOrder.verify(input, times(1)).getPrefix();
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for {@link eu.estcube.sdc.processor.Ax25UiFrameInputProcessor#process(org.apache.camel.Exchange)}.
     * @throws Exception 
     */
    @Test
    public void testProcessEmptyPrefix() throws Exception {
        when(input.getPrefix()).thenReturn(" ");
        processor.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getBody(Ax25UiFrameInput.class);
        inOrder.verify(converter, times(1)).convert(input);
        inOrder.verify(input, times(1)).getTncTargetPort();
        inOrder.verify(out, times(1)).setHeader(Headers.TNC_PORT, TncFrame.TARGET_MAX_VALUE);
        inOrder.verify(input, times(1)).getPrefix();
        inOrder.verifyNoMoreInteractions();
    }
}
