package eu.estcube.sdc.gcp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class GcpTmTest {

	@Test
	public void testConstructors1() {

		byte[] data = { 0x55, 0x55, 0x55, 0x55, 0x55 };

		GcpTm packet = new GcpTm(data);

		assertEquals(85, packet.getSource());
		assertTrue(packet.getCommand() instanceof GcpCommand);
		assertEquals(341, packet.getCommand().getId());

		byte[] bytes = packet.getBytes();
		assertEquals(bytes[0], data[0]);
		assertEquals(bytes[1], data[1]);
	}
}
