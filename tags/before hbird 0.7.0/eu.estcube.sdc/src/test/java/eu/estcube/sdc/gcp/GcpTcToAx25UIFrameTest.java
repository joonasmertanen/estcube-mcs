package eu.estcube.sdc.gcp;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.domain.transport.ax25.Ax25UIFrame;

@RunWith(MockitoJUnitRunner.class)
public class GcpTcToAx25UIFrameTest {
	
	@Mock
    private Exchange exchange;
    
    @Mock
    private Message in;
    
    @Mock
    private Message out;
    
    @Mock
    private Ax25UIFrame ax25Frame;
    
    @Mock
    private GcpTc gcpTc;
    
    @Mock
    private GcpEncoder encoder;
    
    @InjectMocks
    private GcpTcToAx25UIFrame toAx25Frame;
    
    private InOrder inOrder;
    
    
    @Before
    public void setUp() throws Exception {
        inOrder = inOrder(exchange, in, out, ax25Frame, gcpTc, encoder);
        when(exchange.getIn()).thenReturn(in);
        when(exchange.getOut()).thenReturn(out);
        when(in.getBody(GcpTc.class)).thenReturn(gcpTc);
        when(encoder.encode(eq(gcpTc))).thenReturn(ax25Frame);
    }
    
    
    @Test
    public void testProcess() throws Exception {
    	toAx25Frame.process(exchange);
        
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).copyFrom(in);
        inOrder.verify(in, times(1)).getBody(GcpTc.class);
        inOrder.verify(encoder, times(1)).encode(gcpTc);
        inOrder.verify(out, times(1)).setBody(ax25Frame);
        inOrder.verifyNoMoreInteractions();
    }
}
