/** 
 *
 */
package eu.estcube.sdc.gcp;

import org.apache.camel.Exchange;
import org.apache.camel.Handler;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.TypeConversionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.codec.ax25.impl.Ax25UIFrameKissDecoder;
import eu.estcube.codec.ax25.impl.Ax25UIFrameKissEncoder;
import eu.estcube.common.Headers;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.domain.transport.tnc.TncFrame;

/**
 *
 */
@Component
public class Ax25UIFrameToGcpTm implements Processor {

	@Autowired
	private GcpDecoder decoder;

	/** @{inheritDoc . */
	@Override
	@Handler
	public void process(Exchange exchange) throws Exception {
		Message in = exchange.getIn();
		Message out = exchange.getOut();
		out.copyFrom(in);

		Ax25UIFrame ax25Frame = in.getBody(Ax25UIFrame.class);

		GcpTm result = decoder.decode(ax25Frame);
			
		out.setBody(result);
	}

}
