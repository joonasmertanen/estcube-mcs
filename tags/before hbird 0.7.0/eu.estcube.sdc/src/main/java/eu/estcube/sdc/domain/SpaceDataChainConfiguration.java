/** 
 *
 */
package eu.estcube.sdc.domain;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.estcube.domain.config.ConfigurationBase;

/**
 *
 */
@Component
public class SpaceDataChainConfiguration extends ConfigurationBase {

    @Value("${sdc.use.files}")
    private boolean useFiles;
    
    @Value("${uplink.ax25}")
    private String uplinkAx25;
    
    @Value("${downlink.ax25}")
    private String downlinkAx25;

    /**
     * Returns useFiles.
     *
     * @return the useFiles
     */
    public boolean isUseFiles() {
        return useFiles;
    }

    /**
     * Sets useFiles.
     *
     * @param useFiles the useFiles to set
     */
    public void setUseFiles(boolean useFiles) {
        this.useFiles = useFiles;
    }

    /**
     * Returns uplinkAx25.
     *
     * @return the uplinkAx25
     */
    public String getUplinkAx25() {
        return uplinkAx25;
    }

    /**
     * Sets uplinkAx25.
     *
     * @param uplinkAx25 the uplinkAx25 to set
     */
    public void setUplinkAx25(String uplinkAx25) {
        this.uplinkAx25 = uplinkAx25;
    }

    /**
     * Returns downlinkAx25.
     *
     * @return the downlinkAx25
     */
    public String getDownlinkAx25() {
        return downlinkAx25;
    }

    /**
     * Sets downlinkAx25.
     *
     * @param downlinkAx25 the downlinkAx25 to set
     */
    public void setDownlinkAx25(String downlinkAx25) {
        this.downlinkAx25 = downlinkAx25;
    }
}
