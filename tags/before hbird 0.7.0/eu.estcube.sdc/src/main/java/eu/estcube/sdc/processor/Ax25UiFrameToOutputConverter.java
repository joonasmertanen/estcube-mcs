/** 
 *
 */
package eu.estcube.sdc.processor;

import org.springframework.stereotype.Component;

import eu.estcube.common.ByteUtil;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.sdc.domain.Ax25UiFrameOutput;

/**
 *
 */
@Component
public class Ax25UiFrameToOutputConverter {

    public Ax25UiFrameOutput convert(Ax25UIFrame frame) {
        Ax25UiFrameOutput output = new Ax25UiFrameOutput();
        output.setDestAddr(ByteUtil.toHexString(frame.getDestAddr()));
        output.setSrcAddr(ByteUtil.toHexString(frame.getSrcAddr()));
        output.setCtrl(ByteUtil.toHexString(frame.getCtrl()));
        output.setPid(ByteUtil.toHexString(frame.getPid()));
        output.setInfo(ByteUtil.toHexString(frame.getInfo()));
        return output;
    }
} 
