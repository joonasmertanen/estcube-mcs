/** 
 *
 */
package eu.estcube.sdc.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.hbird.exchange.constants.StandardArguments;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.common.Dates;
import eu.estcube.sdc.domain.Ax25UiFrameOutput;
import eu.estcube.sdc.xml.Ax25UiFrameOutputSerializer;

/**
 *
 */
@Component
public class Ax25UiFrameOutputToFileProcessor implements Processor {
    
    private static final Logger LOG = LoggerFactory.getLogger(Ax25UiFrameOutputToFileProcessor.class);
    
    @Autowired
    private Ax25UiFrameOutputSerializer serializer;
    
    /** @{inheritDoc}. */
    @Override
    public void process(Exchange exchange) throws Exception {
        LOG.trace("Processing Ax25UiFrameOutput to XML");
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        out.copyFrom(in);
        try {
            Ax25UiFrameOutput output = in.getBody(Ax25UiFrameOutput.class);
            byte[] bytes = serializer.serialize(output);
            out.setBody(bytes);
            long timestamp = in.getHeader(StandardArguments.TIMESTAMP, Long.class);
            out.setHeader(Exchange.FILE_NAME, getFileName(Dates.toDateInFileNameFormat(timestamp)));
            LOG.trace(" Ax25UiFrameOutput to XML completed successfully");
        } catch (Exception e) {
            LOG.error("Failed to process Ax25UiFrameOutput to XML", e);
            exchange.setException(e);
        }
    }
    
    String getFileName(String date) {
        StringBuilder sb = new StringBuilder();
        sb.append(date)
          .append(".xml");
        return sb.toString();
    }
}
