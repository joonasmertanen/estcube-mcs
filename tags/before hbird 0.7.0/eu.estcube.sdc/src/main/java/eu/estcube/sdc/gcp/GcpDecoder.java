package eu.estcube.sdc.gcp;

import org.springframework.stereotype.Component;

import eu.estcube.domain.transport.ax25.Ax25UIFrame;

@Component
public class GcpDecoder {

	public GcpTm decode( Ax25UIFrame frame ) {
    	return new GcpTm( frame.getInfo() );
    }
}
