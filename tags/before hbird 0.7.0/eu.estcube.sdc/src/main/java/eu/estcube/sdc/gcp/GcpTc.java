package eu.estcube.sdc.gcp;

public class GcpTc {

	final static int HEADER_LENGTH = 1;

	private int destination;

	private GcpCommand command;

	public GcpTc(byte[] data) {
		setDestination(data[0]);

		byte[] commandData = new byte[data.length - HEADER_LENGTH];
		System.arraycopy(data, 1, commandData, 0, data.length - HEADER_LENGTH);
		this.setCommand(new GcpCommand(commandData));
	}
	
	public byte[] getBytes() {
		byte[] commandBytes = command.getBytes();
		byte[] bytes = new byte[commandBytes.length + HEADER_LENGTH];
		bytes[0] = (byte) destination;
		System.arraycopy(commandBytes, 0, bytes, HEADER_LENGTH, commandBytes.length);
		return bytes;
	}

	public int getDestination() {
		return destination;
	}

	public void setDestination(int destination) {
		this.destination = destination;
	}

	public GcpCommand getCommand() {
		return command;
	}

	public void setCommand(GcpCommand command) {
		this.command = command;
	}

}
