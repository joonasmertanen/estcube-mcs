/** 
 *
 */
package eu.estcube.sdc.processor;

import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import eu.estcube.common.ByteUtil;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.sdc.domain.Ax25UiFrameInput;

/**
 *
 */
@Component
public class InputToAx25UIFrameConverter {

    public Ax25UIFrame convert(Ax25UiFrameInput input) throws IllegalArgumentException {
        Ax25UIFrame frame = new Ax25UIFrame();
        
        byte[] destAddr = ByteUtil.toBytesFromHexString(input.getDestAddr());
        Validate.isTrue(destAddr.length == Ax25UIFrame.DEST_ADDR_LEN, "destAddr length has to be %s bytes", Ax25UIFrame.DEST_ADDR_LEN);
        frame.setDestAddr(destAddr);

        byte[] srcAddr = ByteUtil.toBytesFromHexString(input.getSrcAddr());
        Validate.isTrue(srcAddr.length == Ax25UIFrame.SRC_ADDR_LEN, "srcAddr length has to be %s bytes", Ax25UIFrame.SRC_ADDR_LEN);
        frame.setSrcAddr(srcAddr);
        
        byte[] ctrl = ByteUtil.toBytesFromHexString(input.getCtrl());
        Validate.isTrue(ctrl.length == 1, "ctrl has to be single byte");
        frame.setCtrl(ctrl[0]);
        
        byte[] pid = ByteUtil.toBytesFromHexString(input.getPid());
        Validate.isTrue(pid.length == 1, "pid has to be single byte");
        frame.setPid(pid[0]);
        
        byte[] info = ByteUtil.toBytesFromHexString(input.getInfo());
        Validate.isTrue(info.length <= Ax25UIFrame.INFO_MAX_SIZE, "info max length is %s", Ax25UIFrame.INFO_MAX_SIZE);
        frame.setInfo(info);
        
        return frame;
    }
}
