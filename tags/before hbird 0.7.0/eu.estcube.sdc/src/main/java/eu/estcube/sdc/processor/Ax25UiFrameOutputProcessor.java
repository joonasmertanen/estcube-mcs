/** 
 *
 */
package eu.estcube.sdc.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.hbird.exchange.constants.StandardArguments;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.common.Dates;
import eu.estcube.common.Headers;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.sdc.domain.Ax25UiFrameOutput;

/**
 *
 */
@Component
public class Ax25UiFrameOutputProcessor implements Processor {
    
    private static final Logger LOG = LoggerFactory.getLogger(Ax25UiFrameOutputProcessor.class);
    
    @Autowired
    private Ax25UiFrameToOutputConverter converter;
    
    /** @{inheritDoc}. */
    @Override
    public void process(Exchange exchange) throws Exception {
        LOG.trace("Processing Ax25UIFrame to Ax25UiFrameOutput");
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        out.copyFrom(in);
        
        try {
            Ax25UIFrame frame = in.getBody(Ax25UIFrame.class);
            Ax25UiFrameOutput output = converter.convert(frame);
            
            // XXX - 20.03.2013; kimmell - change this
            long timestamp = System.currentTimeMillis();
            if (in.getHeader(Exchange.CREATED_TIMESTAMP) != null) {
                timestamp = in.getHeader(Exchange.CREATED_TIMESTAMP, Long.class);
            }
            else if (in.getHeader(StandardArguments.TIMESTAMP) != null) {
                timestamp = in.getHeader(StandardArguments.TIMESTAMP, Long.class);
            } else if (in.getHeader("JMSTimestamp") != null) {
                timestamp = in.getHeader("JMSTimestamp", Long.class);
            }
            out.setHeader(StandardArguments.TIMESTAMP, timestamp);
            String timestampAsIso8601String = Dates.toIso8601DateFormat(timestamp);
            output.setTimestamp(timestampAsIso8601String);
            output.setServiceId(in.getHeader(StandardArguments.ISSUED_BY, String.class));
            output.setSerialPortName(in.getHeader(Headers.SERIAL_PORT_NAME, String.class));
            out.setBody(output);
            LOG.trace(" Ax25UIFrame to Ax25UiFrameOutput completed successfully");
        } catch (Throwable e) {
            LOG.error("Failed to process Ax25UIFrame to Ax25UiFrameOutput", e);
            exchange.setException(e);
        }
    }
}
