package eu.estcube.sdc.gcp;

import java.io.Serializable;

public class GcpTm implements Serializable {

    private static final long serialVersionUID = -118624662185695844L;

    final static int HEADER_LENGTH = 1;

	private int source;

	private GcpCommand command;

	public GcpTm(byte[] data) {
		this.setSource(data[0]);

		byte[] commandData = new byte[data.length - HEADER_LENGTH];
		System.arraycopy(data, 1, commandData, 0, data.length - HEADER_LENGTH);
		this.setCommand(new GcpCommand(commandData));
	}
	
	public byte[] getBytes() {
		byte[] commandBytes = command.getBytes();
		byte[] bytes = new byte[commandBytes.length + HEADER_LENGTH];
		bytes[0] = (byte) source;
		System.arraycopy(commandBytes, 0, bytes, HEADER_LENGTH, commandBytes.length);
		return bytes;
	}

	public int getSource() {
		return source;
	}

	public void setSource(int source) {
		this.source = source;
	}

	public GcpCommand getCommand() {
		return command;
	}

	public void setCommand(GcpCommand command) {
		this.command = command;
	}

}
