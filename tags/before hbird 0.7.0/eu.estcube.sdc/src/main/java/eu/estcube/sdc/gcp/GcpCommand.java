package eu.estcube.sdc.gcp;

import java.io.Serializable;


public class GcpCommand implements Serializable {
    
    private static final long serialVersionUID = -4988418304577628037L;

    // final static Logger log = LoggerFactory.getLogger(GcpCommand.class);
	final static int HEADER_LENGTH = 4;

	private byte[] data;

	private int immediate; // 1 bit
	private int priority; // 1 bit
	private int dst; // 4 bits
	private int id; // 10 bits
	private int src; // 4 bits
	private int index; // 4 bits
	private int length; // 8 bits
	
	public GcpCommand(byte[] payload) {
		if (payload.length < HEADER_LENGTH) {
			// log.warn("Payload is too short to parse");
		} else {
			byte[] commandheaderBytes = new byte[HEADER_LENGTH];
			System.arraycopy(payload, 0, commandheaderBytes, 0, HEADER_LENGTH);
			setImmediate((payload[0] & 0x80) >> 7);
			setPriority((payload[0] & 0x40) >> 6);
			setDst((payload[0] & 0x3C) >> 2);
			setId(((payload[0] & 0x03) << 8) + (payload[1] & 0xFF));
			setSrc((payload[2] & 0xf0) >> 4);
			setIndex(payload[2] & 0x0f);
			setLength((payload[3] & 0xFF));
			if (getLength() != payload.length - HEADER_LENGTH) {
				int newLength = payload.length - HEADER_LENGTH;
				// log.error("Erronous ICP payload, promised data length doesn't match with actual");
				// log.error("Using actual length '" + newLength +
				// "', fix your application - promised size: " + getLength());
				setLength(newLength);
			}
		}

		if (getLength() > 0) {
			data = new byte[getLength()];
			System.arraycopy(payload, HEADER_LENGTH, data, 0, getLength());
		} else {
			data = new byte[0];
		}
	}

	public GcpCommand(int immediate, int priority, int dst, int id, int src,
			int index, byte[] data) {
		setImmediate(immediate);
		setPriority(priority);
		setDst(dst);
		setId(id);
		setSrc(src);
		setIndex(index);
		setLength(data.length);
		setData(data);
	}

	public boolean isRequestToResponce(GcpCommand response) {
		// @TODO TODO!
		/*
		 * boolean foundResponse = false; if (response.isResponse() &&
		 * !isResponse()) { foundResponse = (getCommandType().getDst() ==
		 * response.getCommandType().getDst()) &&
		 * ((getCommandType().getId()&0x1FF) ==
		 * (response.getCommandType().getId()&0x1FF)); } return foundResponse;
		 */
		return false;
	}

	private byte[] getHeaderBytes() {
		byte[] bytes = new byte[HEADER_LENGTH];
		bytes[0] = (byte) ((immediate << 7) | (priority << 6)
				| ((dst & 0x0F) << 2) | ((id & 0x300) >>> 8));
		bytes[1] = (byte) (id & 0xff);
		bytes[2] = (byte) ((src << 4) | index);
		bytes[3] = (byte) data.length;
		return bytes;
	}

	public byte[] getBytes() {
		byte[] result = new byte[HEADER_LENGTH + data.length];
		System.arraycopy(getHeaderBytes(), 0, result, 0, HEADER_LENGTH);
		System.arraycopy(data, 0, result, HEADER_LENGTH, data.length);
		return result;
	}

	public int getDst() {
		return dst;
	}

	public void setDst(int dst) {
		this.dst = dst;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getImmediate() {
		return immediate;
	}

	public void setImmediate(int immediate) {
		this.immediate = immediate;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getSrc() {
		return src;
	}

	public void setSrc(int src) {
		this.src = src;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
}
