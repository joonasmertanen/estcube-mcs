/** 
 *
 */
package eu.estcube.sdc.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.hbird.exchange.constants.StandardArguments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.codec.ax25.impl.Ax25UIFrameKissDecoder;
import eu.estcube.common.Headers;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.domain.transport.tnc.TncFrame;

/**
 *
 */
@Component
public class TncFrameToAx25UIFrame implements Processor {

    public static final String TYPE = "AX.25";
    
    @Autowired
    private Ax25UIFrameKissDecoder decoder;
    
    /** @{inheritDoc}. */
    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        out.copyFrom(in);
        TncFrame tncFrame = in.getBody(TncFrame.class);
        // TODO - 20.03.2013; kimmell - tncFrame can be null!
        Ax25UIFrame ax25UIFrame = decoder.decode(tncFrame);
        out.setBody(ax25UIFrame);
        out.setHeader(StandardArguments.CLASS, ax25UIFrame.getClass().getSimpleName());
        out.setHeader(StandardArguments.TYPE, TYPE);
        out.setHeader(Headers.TNC_PORT, tncFrame.getTarget());
    }
}
