/** 
 *
 */
package eu.estcube.domain.transport;

/**
 *
 */
public class Downlink {

    public static final String FROM_TNC = "activemq:topic:estcube.downlink.tnc";

    public static final String AX25_FRAMES = "activemq:topic:estcube.downlink.ax25";

    public static final String GCP_TM_OBJECTS = "activemq:topic:estcube.downlink.gcp_tm";
}
