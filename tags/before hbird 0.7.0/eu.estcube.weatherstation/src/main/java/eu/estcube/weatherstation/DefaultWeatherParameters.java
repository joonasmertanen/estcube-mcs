/** 
 *
 */
package eu.estcube.weatherstation;

import org.hbird.exchange.core.Label;
import org.hbird.exchange.core.Parameter;

/**
 *
 */
public class DefaultWeatherParameters {

    public static final Parameter AIR_TEMPERATURE = new Parameter(null, "air.temperature", "Air Temperature",
        "Air temperature from weather station", 0.0D, "°C");

    public static final Parameter RELATIVE_HUMIDITY = new Parameter(null, "relative.humidity", "Humidity",
        "Relative humidity from weather station", 0.0D, "%");

    public static final Label WIND_DIRECTION = new Label(null, "wind.direction", "Wind direction",
        "Wind direction from weather station", "-");

    public static final Parameter WIND_SPEED = new Parameter(null, "wind.speed", "Wind speed",
        "Wind speed in weather station", 0.0D, "m/s");

    public static final Parameter LUX = new Parameter(null, "lux", "Lux", "Lux in weather station", 0.0D, "?");

    public static final Parameter SOLE = new Parameter(null, "sole", "Sole", "Sole in weather station", 0.0D, "?");

    public static final Parameter GAMMA = new Parameter(null, "gamma", "Gamma", "Gamma in weather station", 0.0D, "?");

    public static final Parameter PRECIPITATIONS = new Parameter(null, "precipitations", "Precipitations",
        "Precipitations from weather station", 0.0D, "?");

    public static final Label PHENOMENON = new Label(null, "phenomenon", "Phenomenon",
        "Phenomenon from weather station", "-");

    public static final Parameter VISIBILITY = new Parameter(null, "visibility", "Visibility",
        "Visibility from weather station", 0.0D, "-");

    public static final Parameter AIR_PRESSURE = new Parameter(null, "air.pressure", "Air pressure",
        "Air pressure in weather station", 0.0D, "?");

    public static final Parameter WIND_SPEED_MAX = new Parameter(null, "wind.speed.max", "Max wind speed",
        "Max wind speed in weather station", 0.0D, "?");

    public static final Label NAME = new Label(null, "name", "Name", "Name of weather station", "N/A");

    public static final Parameter WMO_CODE = new Parameter(null, "wmo.code", "WMO Code", "WMO Code of weather station",
        0, "-");

    public static final Label AIR_TEMPERATURE_TREND = new Label(null, "air.temperature.trend", "Air temperature trend",
        "Air temperature trend from weather station", "-");

    public static final Label AIR_PRESSURE_TREND = new Label(null, "air.pressure.trend", "Air pressure trend",
        "Air pressure trend from weather station", "-");

    public static final Label RELATIVE_HUMIDITY_TREND = new Label(null, "relative.humidity.trend",
        "Relative humidity trend", "Relative humidity trend from weather station", "-");

}
