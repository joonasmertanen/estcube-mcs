package eu.estcube.weatherstation;

import org.hbird.exchange.core.Named;

public interface NamedFactory<T extends Named> {
    T createNew(T base, String value);
}
