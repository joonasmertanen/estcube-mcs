package eu.estcube.weatherstation;

import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_PRESSURE;
import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_PRESSURE_TREND;
import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_TEMPERATURE;
import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_TEMPERATURE_TREND;
import static eu.estcube.weatherstation.DefaultWeatherParameters.PHENOMENON;
import static eu.estcube.weatherstation.DefaultWeatherParameters.RELATIVE_HUMIDITY;
import static eu.estcube.weatherstation.DefaultWeatherParameters.RELATIVE_HUMIDITY_TREND;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WIND_DIRECTION;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WIND_SPEED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.hbird.exchange.core.Label;
import org.hbird.exchange.core.Named;
import org.hbird.exchange.core.Parameter;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import eu.estcube.common.Naming;
import eu.estcube.common.Naming.Base;

public class IlmHandlerImplTest {

    private IlmHandlerImpl handler;
    private List<Named> weatherParams;
    private String dataSetIdPattern;

    @Before
    public void setUp() throws Exception {
        SAXBuilder builder = new SAXBuilder();
        Document document = builder.build(getClass().getResourceAsStream("/ilm.ee.xml"));
        handler = new IlmHandlerImpl();
        weatherParams = handler.parseDocument(document);
        dataSetIdPattern = Base.WEATHER_STATION + "-" + handler.getDataSourceName()
                + "-[0-9]{13,19}";
    }

    @Test
    public void testDataSetIdentifier() {
        String dataSetId = weatherParams.get(0).getDatasetidentifier();
        for (Named n : weatherParams) {
            assertEquals(dataSetId, n.getDatasetidentifier());
            assertTrue(n.getDatasetidentifier().matches(dataSetIdPattern));
        }
    }

    /**
     * Test method for {@link IlmPars#getResult()}.
     * 
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws JDOMException
     */
    @Test
    public final void testParameters() {

        assertEquals(9, weatherParams.size());

        assertNotNull(weatherParams.get(0).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(0).getIssuedBy());
        assertTrue(weatherParams.get(0).getDatasetidentifier().matches(dataSetIdPattern));
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), AIR_TEMPERATURE.getName()), weatherParams.get(0).getName());
        assertEquals(AIR_TEMPERATURE.getDescription(), weatherParams.get(0).getDescription());
        assertEquals(AIR_TEMPERATURE.getType(), weatherParams.get(0).getType());
        assertEquals(AIR_TEMPERATURE.getUnit(), ((Parameter) weatherParams.get(0)).getUnit());
        assertEquals(22.0D, ((Parameter) weatherParams.get(0)).getValue());

        assertNotNull(weatherParams.get(1).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(1).getIssuedBy());
        assertTrue(weatherParams.get(1).getDatasetidentifier().matches(dataSetIdPattern));
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), AIR_TEMPERATURE_TREND.getName()), weatherParams.get(1).getName());
        assertEquals(AIR_TEMPERATURE_TREND.getDescription(), weatherParams.get(1).getDescription());
        assertEquals(AIR_TEMPERATURE_TREND.getType(), weatherParams.get(1).getType());
        assertEquals("up", ((Label) weatherParams.get(1)).getValue());

        assertNotNull(weatherParams.get(2).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(2).getIssuedBy());
        assertTrue(weatherParams.get(2).getDatasetidentifier().matches(dataSetIdPattern));
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), AIR_PRESSURE.getName()), weatherParams.get(2).getName());
        assertEquals(AIR_PRESSURE.getDescription(), weatherParams.get(2).getDescription());
        assertEquals(AIR_PRESSURE.getType(), weatherParams.get(2).getType());
        assertEquals(AIR_PRESSURE.getUnit(), ((Parameter) weatherParams.get(2)).getUnit());
        assertEquals(758.0D, ((Parameter) weatherParams.get(2)).getValue());

        assertNotNull(weatherParams.get(3).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(3).getIssuedBy());
        assertTrue(weatherParams.get(3).getDatasetidentifier().matches(dataSetIdPattern));
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), AIR_PRESSURE_TREND.getName()), weatherParams.get(3).getName());
        assertEquals(AIR_PRESSURE_TREND.getDescription(), weatherParams.get(3).getDescription());
        assertEquals(AIR_PRESSURE_TREND.getType(), weatherParams.get(3).getType());
        assertEquals("down", ((Label) weatherParams.get(3)).getValue());

        assertNotNull(weatherParams.get(4).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(4).getIssuedBy());
        assertTrue(weatherParams.get(4).getDatasetidentifier().matches(dataSetIdPattern));
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), RELATIVE_HUMIDITY.getName()), weatherParams.get(4).getName());
        assertEquals(RELATIVE_HUMIDITY.getDescription(), weatherParams.get(4).getDescription());
        assertEquals(RELATIVE_HUMIDITY.getType(), weatherParams.get(4).getType());
        assertEquals(RELATIVE_HUMIDITY.getUnit(), ((Parameter) weatherParams.get(4)).getUnit());
        assertEquals(67.0D, ((Parameter) weatherParams.get(4)).getValue());

        assertNotNull(weatherParams.get(5).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(5).getIssuedBy());
        assertTrue(weatherParams.get(5).getDatasetidentifier().matches(dataSetIdPattern));
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), RELATIVE_HUMIDITY_TREND.getName()), weatherParams.get(5).getName());
        assertEquals(RELATIVE_HUMIDITY_TREND.getDescription(), weatherParams.get(5).getDescription());
        assertEquals(RELATIVE_HUMIDITY_TREND.getType(), weatherParams.get(5).getType());
        assertEquals("up", ((Label) weatherParams.get(5)).getValue());

        assertNotNull(weatherParams.get(6).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(6).getIssuedBy());
        assertTrue(weatherParams.get(6).getDatasetidentifier().matches(dataSetIdPattern));
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), WIND_SPEED.getName()), weatherParams.get(6).getName());
        assertEquals(WIND_SPEED.getDescription(), weatherParams.get(6).getDescription());
        assertEquals(WIND_SPEED.getType(), weatherParams.get(6).getType());
        assertEquals(WIND_SPEED.getUnit(), ((Parameter) weatherParams.get(6)).getUnit());
        assertEquals(2.2D, ((Parameter) weatherParams.get(6)).getValue());

        assertNotNull(weatherParams.get(7).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(7).getIssuedBy());
        assertTrue(weatherParams.get(7).getDatasetidentifier().matches(dataSetIdPattern));
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), WIND_DIRECTION.getName()), weatherParams.get(7).getName());
        assertEquals(WIND_DIRECTION.getDescription(), weatherParams.get(7).getDescription());
        assertEquals(WIND_DIRECTION.getType(), weatherParams.get(7).getType());
        assertEquals("S", ((Label) weatherParams.get(7)).getValue());

        assertNotNull(weatherParams.get(8).getTimestamp());
        assertEquals(handler.getDataSourceName(), weatherParams.get(8).getIssuedBy());
        assertTrue(weatherParams.get(8).getDatasetidentifier().matches(dataSetIdPattern));
        assertEquals(Naming.createParameterAbsoluteName(Base.WEATHER_STATION,
                handler.getDataSourceName(), PHENOMENON.getName()), weatherParams.get(8).getName());
        assertEquals(PHENOMENON.getDescription(), weatherParams.get(8).getDescription());
        assertEquals(PHENOMENON.getType(), weatherParams.get(8).getType());
        assertEquals("vihm", ((Label) weatherParams.get(8)).getValue());
    }
}
