dojo.provide("webgui.display.MenuDisplay");
dojo.require("dijit.Menu");
dojo.require("dijit.MenuItem");
dojo.require("dijit.layout.ContentPane");
dojo.require("webgui.display.StatusesTableDisplay");
dojo.require("webgui.display.PictureDisplay");
dojo.require("webgui.display.InfoDisplay");
dojo.require("webgui.display.MainTabDisplay");
dojo.require("webgui.display.StatusesInfoDisplay");
dojo.require("webgui.display.StatusesGraphDisplay");
dojo.require("dijit.Dialog");
dojo.require("dijit.form.ComboBox");
dojo.require("dijit.form.DropDownButton");


var dialogVersion;
var versions;

dojo.declare("MenuDisplay", null, {
	
	constructor : function() {
		console.log("MenuDisplay!");

		// This happens on menuItem hover
		function mOver() {
			this.attr('style', 'background-color: #DEDEDE');
			this.attr('style', 'color: #212121');
		}

		// This happens when mouse is not hovering the menuItem anymore
		function mLeave() {
			this.attr('style', 'background-color: #FFFFFF');
			this.attr('style', 'color: #212121');
		}

		// Under construction prompt
		function underConstruction() {
			alert("This page is under construction!");
		}

		var GUIVERSION = webgui.common.Constants.GUIVERSION;
		var activeDevice = "ES5EC";
		
		// Navigation function for activating devices
		function navigate(url) {
    	window.location.href = url;
    	return false;
		}		
		
		// Create a new div for Active Device pop-up
		var n = dojo.create("div", 
			null
		);
		
		// Menu for devices available for activation
		 var activateDevice = new dijit.Menu({
            style: "display: none;"
        }).placeAt(n);
        
		// Menu item for activating ES5EC 
        var activateES5EC = new dijit.MenuItem({
            label: "ES5EC",
            id : "activateES5EC",
            onClick: function() {
            	activeDevice = "ES5EC";
                navigate("ES5EC.html");
            }
        });
        activateDevice.addChild(activateES5EC);
        
        // Menu item for activating EstCube-1
        var activateEC1 = new dijit.MenuItem({
            label: "EstCube-1",
            id : "activateEC1",
            onClick: function() {
            	activeDevice = "EstCube-1";
                navigate("ESTCube-1.html");
            }
        });
        activateDevice.addChild(activateEC1);

        // Drop-down button for activating device
		var buttonActivate = new dijit.form.DropDownButton({
			id : "buttonActivate",
			label : "Choose a device to activate:",
			dropDown :  activateDevice,
		}).placeAt(n);

		
		// Content pane for active device div
		var contentPaneActive = new dijit.layout.ContentPane({
			id : "contentPaneActive",
			content: n
		});
		contentPaneActive.startup();

		// Pop-up dialog for active device content pane
		var dialogActive = new dijit.Dialog({title:"Activate Device"} );
		dialogActive.setContent(contentPaneActive.domNode);
		dialogActive.startup();		
		

		// Show the version dialog
		function showActive() {
			//dijit.byId("dialogActive").show();
			dialogActive.show();
		}

		// Hide the version dialog
		function hideActive() {
			//dijit.byId("dialogActive").hide();
			dialogActive.hide();
		}

		// VERSION NO.
		// Create a "hidden" version Dialog:
		dialogVersion = new dijit.Dialog({
			title : "Mission Control System",
			content : ("Mission Control System <br>" +
					"GUI version: " + GUIVERSION + "<br />"),
			style : "width: 550px; overflow:auto;",
			id : "versionNumber"
		});
		dialogVersion.startup();

		// Show the version dialog
		function showVersion() {
            var webserverComponent = webgui.common.Constants.COMPONENT_WEBSERVER;
            var connectorComponent = webgui.common.Constants.COMPONENT_CONNECTOR;
            var webcameraComponent = webgui.common.Constants.COMPONENT_WEBCAMERA_TRANSMITTER;
            var psComponent = webgui.common.Constants.COMPONENT_PARAMETER_STORAGE;


            var webserverVersion = "";
            var connectorVersions = new Array();
            var webcameraVersions = new Array();
            var psVersions = new Array();

            if(versions != undefined) {
                if(versions[webserverComponent] != undefined) {
                    webserverVersion = versions[webserverComponent];
                }
                for(property in versions) {
                    console.log(property);
                    var regexp = new RegExp(connectorComponent + "*")
                    var match = property.search(regexp);
                    if(match > -1) {
                        connectorVersions.push(property + " : " + versions[property]);
                    }
                    var regexp = new RegExp(webcameraComponent + "*")
                    var match = property.search(regexp);
                    if(match > -1) {
                        webcameraVersions.push(property + " : " + versions[property]);
                    }
                    var regexp = new RegExp(psComponent + "*")
                    var match = property.search(regexp);
                    if(match > -1) {
                        psVersions.push(property + " : " + versions[property]);
                    }
                }
            }

            dialogVersion.set("content","<b>Mission Control System</b> <br>" +
					"<b>GUI version: </b>" + GUIVERSION + "<br />" +
					"<b>Webserver version: </b>" + webserverVersion + "<br />" +
					"<b>Connector version(s): </b>" + connectorVersions + "<br />" +
					"<b>Webcamera transmitter version(s): </b>" + webcameraVersions + "<br />" +
					"<b>Parameter storage version(s): </b>" + psVersions + "<br />"
            );

			dijit.byId("versionNumber").show();
		}

		// Hide the version dialog
		function hideVersion() {
			dijit.byId("versionNumber").hide();
		}

		var menuMain = new dijit.MenuItem({
			onMouseOver : mOver,
			onMouseLeave : mLeave,
			label : "Main View",
			onClick : function() {
				dijit.byId('CenterContainer').selectChild(dijit.byId("mainTabContainer"));
				dijit.byId('InfoView').selectChild(dijit.byId("mainInfoTab"));
				dijit.byId('PictureView').selectChild(dijit.byId("pictureTab"));
			}
		});

		var menuStatuses = new dijit.MenuItem({
			onMouseOver : mOver,
			onMouseLeave : mLeave,
			label : "Status View",
			onClick : function() {
				dijit.byId('CenterContainer').selectChild(dijit.byId("statusesGraphTab"));
				dijit.byId('InfoView').selectChild(dijit.byId("statusesInfoTab"));
				dijit.byId('PictureView').selectChild(dijit.byId("componentTabContainer"));
			}
		});

		var menuCommand = new dijit.MenuItem({
			onMouseOver : mOver,
			onMouseLeave : mLeave,
			label : "Commands View",
			onClick : underConstruction
		});

		var menuLog = new dijit.MenuItem({
			onMouseOver : mOver,
			onMouseLeave : mLeave,
			onClick : underConstruction,
			label : "Log View"
		});

		var menuTimeline = new dijit.MenuItem({
			onMouseOver : mOver,
			onMouseLeave : mLeave,
			onClick : underConstruction,
			label : "Timeline View"
		});

		//Active GS
		var menuGroundstation = new dijit.MenuItem({
			onMouseOver : mOver,
			onMouseLeave : mLeave,
			onClick : showActive,
			label : "Active device: " + activeDevice
		});

		//Version no.
		var menuVersion = new dijit.MenuItem({
			onMouseOver : mOver,
			onMouseLeave : mLeave,
			onClick : showVersion,
			label : "GUI version:<br> " + GUIVERSION
		});

		var mainMenu = dijit.byId("MenuView");
		mainMenu.addChild(menuMain);
		mainMenu.addChild(menuStatuses);
		mainMenu.addChild(menuCommand);
		mainMenu.addChild(menuLog);
		mainMenu.addChild(menuTimeline);
		mainMenu.addChild(menuGroundstation);
		mainMenu.addChild(menuVersion);

	}
});

dojo.subscribe("versions", function(message) {
	versions = JSON.parse(message);
	for (var propertyName in versions) {
		console.log(propertyName + ': ' + versions[propertyName]);
	}
    // versions are in variable "versions"
    // for example versions.webserver
	// connector and webcamera have gsName appended to the property - connector-ES5EC
});

dojo.declare("webgui.display.MenuDisplay", null, {
	constructor : function(args) {
		var menu = new MenuDisplay();
	}
});
