define([
    "dojo/_base/declare",
    "dojo/store/Memory",
    "dojo/store/Observable",
    "../common/Constants",
    "../assembly/AssemblerBase",
    "../display/ParametersView",
    ],

    function(declare, Memory, Observable, Constants, base, view) {
        return [

            declare("ParametersAssembler", AssemblerBase, {

                build: function() {
//                    this.setupCacheRequest({ delay: 1500, channels: ["/hbird.out.parameters"], });
                    this.setupCommunication();

                    var store = new Observable(new Memory({ idProperty : "storeId", }));
                    var view = new ParametersView({ store: store, parentDivId: "CenterContainer" });
                    var controller = ParametersController({ id: "Parameters", channels: ["/hbird.out.parameters"], store: store, filter: { name: ".*/air\.temperature$" } });
                },

            }),

        ];
    }
);