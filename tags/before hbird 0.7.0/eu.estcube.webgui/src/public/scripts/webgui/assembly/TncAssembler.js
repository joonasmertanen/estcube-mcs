define([
    "dojo/_base/declare",
    "dojo/store/Memory",
    "dojo/store/Observable",
    "../common/Constants",
    "../common/DataLoader",
    "../assembly/AssemblerBase",
    "../display/TncView",
    "../display/MenuView",
    ],

    function(declare, Memory, Observable, Constants, DataLoader, base, view, MenuView) {
        return [

            declare("TncAssembler", AssemblerBase, {

                build: function() {
//                    this.setupCacheRequest({ delay: 1500, channels: ["/hbird.out.parameters"], });
                    this.setupCommunication();

                    var pageStore = new Observable(new Memory( { idProperty: "id" }));

                    var menubar = new MenuBarView({
                        parentDivId: "menu",
                        store: pageStore,
                        primaryItems: ["ES5EC", "system", "satellites", "groundStations"],
                        secondaryItems: ["logout"],
                    });

                    var store = new Observable(new Memory({ idProperty : "uuid" }));
                    var view = new TncView({ store: store, parentDivId: "CenterContainer" });
                    var controller = TncController({ id: "TNC Frames", channels: ["/estcube.downlink.dump.tnc"], store: store, limit: 25 });
                    new UrlDataLoader().loadData("../../data/db.json", pageStore);
                },

            }),

        ];
    }
);
