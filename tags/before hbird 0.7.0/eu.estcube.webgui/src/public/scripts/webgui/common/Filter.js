define(["./Constants"], function(Constants) {

    return {

        accept: function(filter, data) {
            for (key in filter) {
                var pattern = filter[key];
                var value = data[key];
                var regex = new RegExp(pattern);
                if (value == null || !regex.test(value)) {
                    return false;
                }
            }
            return true;
        },

        reject: function(filter, data) {
            return !this.accept(filter, data);
        },
    }
});