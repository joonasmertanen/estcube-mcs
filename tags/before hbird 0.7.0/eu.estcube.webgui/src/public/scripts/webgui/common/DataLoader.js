define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/request",
    ],

    function(declare, Arrays, Request) {
        return [
            declare("UrlDataLoader", null, {
                loadData: function(url, store) {
                    Request.get(url, { handleAs: "json"}).then(
                        function(data) {
                            Arrays.forEach(data, function(item, index) {
                                store.put(item);
                            });
                        },
                        function(error) {
                            console.error("Error at loading data from [" + url + "]; " + error);
                        }
                    );
                },
            }),

        ];
    }
);