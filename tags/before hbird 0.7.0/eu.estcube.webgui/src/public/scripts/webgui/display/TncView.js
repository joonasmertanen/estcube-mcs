define([
    "dojo/_base/declare",
    "dgrid/OnDemandGrid",
    "../common/Constants",
    "../common/Utils",
    "../common/LimitingStoreHandler",
    "./ViewBase",
    ],

    function(declare, Grid, Constants, Utils, StoreHandler, ViewBase) {

        return [
            declare("TncController", Controller, {

                limit: 25,
                handler: null,

                constructor: function(args) {
                    this.handler = new LimitingStoreHandler();
                },

                channelHandler: function(message, channel) {
                    this.handler.handle(this.store, message, this.limit);
                },
            }),

            declare("TncView", View, {

                build: function() {
                    var grid = new Grid({
                            store: this.store,
                            columns: [
                                {
                                    label: "Timestamp",
                                    field: "timestamp",
                                    formatter: Utils.formatDate,
                                    className: "field-timestamp",
                                },
                                {
                                    label: "Ground Station",
                                    field: "headers",
                                    formatter: function(headers) {
                                        return headers.location;
                                    },
                                    className: "field-groundStation",
                                },
                                {
                                    label: "Satellite",
                                    field: "headers",
                                    formatter: function(headers) {
                                        return headers.satellite;
                                    },
                                    className: "field-satellite",
                                },
                                {
                                    label: "Contact",
                                    field: "headers",
                                    formatter: function(headers) {
                                        return headers.contact;
                                    },
                                    className: "field-contact",
                                },
                                {
                                    label: "Source",
                                    field: "headers",
                                    formatter: function(headers) {
                                        return headers.issuedBy + " " + headers.serialPortName;
                                    },
                                    className: "field-issuedBy",
                                },
                                {
                                    label: "HEX Dump",
                                    field: "frame",
                                    formatter: function(frame) {
                                        return frame.data;
                                    },
                                    className: "field-value",
                                },
                            ]
                    }, this.parentDivId);
                    grid.set("sort", "timestamp", true);
                    grid.startup();
                },
            }),

        ];
    }
);
