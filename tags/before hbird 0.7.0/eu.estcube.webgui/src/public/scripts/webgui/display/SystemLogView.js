define([
    "dojo/_base/declare",
    "dgrid/OnDemandGrid",
    "../common/Constants",
    "../common/Utils",
    "dojox/uuid/Uuid",
    "dojox/uuid/generateRandomUuid",
    "../common/LimitingStoreHandler",
    "./ViewBase",
    ],

    function(declare, Grid, Constants, Utils, ViewBase, Uuid, StoreHandler, generateRandomUuid) {

        return [
            declare("SystemLogController", Controller, {
            	 limit: 50,
                 handler: null,

                 constructor: function(args) {
                     this.handler = new LimitingStoreHandler();
                 },

                 channelHandler: function(message, channel) {
                     this.handler.handle(this.store, message, this.limit);
                     message.storeId = dojox.uuid.generateRandomUuid();
                     this.store.put(message);
                 },
        
            }),

            declare("SystemLogView", View, {

                build: function() {
                    var grid = new Grid({
                            store: this.store,
                            columns: {
                                categoryName: { label: "Category name" },
                                renderedMessage: { label: "Message" },
                                timeStamp: { label: "Timestamp", formatter: Utils.formatDate },
                            }
                    }, this.parentDivId);
                    grid.set("sort", "timeStamp", true);
                    grid.startup();
                },
            }),

        ];
    }
);