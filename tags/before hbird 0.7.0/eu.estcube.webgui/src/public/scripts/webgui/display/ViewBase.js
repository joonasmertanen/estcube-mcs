define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "../common/Constants",
    ],

    function(declare, Arrays, Constants) {

        return [

            declare("Controller", null, {

                channels: null,
                eventHandlers: null,

                constructor: function(args) {
                    declare.safeMixin(this, args);
                    console.log("[Controller] init " + this.declaredClass + " (id: " + this.id + ")");
                    this.subscribeToChannels();
                    console.log("**** [Controller] init " + this.declaredClass + " (id: " + this.id + ")");
                    this.intiEventHandlers();
                    console.log("[Controller] init " + this.declaredClass + " (id: " + this.id + ")");
                },

                channelHandler: function(message, channel) {
                    console.log("[Controller] Missing channel handler implementation for " + channel + " in " + this.declaredClass);
                },

                eventHandler: function(event) {
                    console.log("[Controller] Missing event handler implementation for " + JSON.stringify(event) + " in " + this.declaredClass);
                },

                subscribeToChannels: function() {
                    if (this.channels == null) {
                        return;
                    }
                    var channelHandler = this.channelHandler;
                    Arrays.forEach(this.channels, function(channel, i) {
                        console.log("[Controller] (id: " + this.id + ") Subscribing to channel[" + i + "]: " + channel);
                        try {
                            dojo.subscribe(channel, dojo.hitch(this, channelHandler));
                            dojo.publish(Constants.TOPIC_CHANNEL_REQUEST, [{ channel: channel, source: this.id }]);
                        } catch (e) {
                            console.error("[Controller] (id: " + this.id + ") Failed to subscribe to channel " + channel + "; " + e);
                        }
                    }, this);
                },

                intiEventHandlers: function() {
                    if (this.eventHandlers == null) {
                        return;
                    }

                    Arrays.forEach(this.eventHandlers, function(h, i) {
                        if (h.event == null || h.handler == null) {
                            console.warn("Skipping invalid event handler " + JSON.stringify(h));
                            return;
                        }
                        dojo.subscribe(h.event, dojo.hitch(this, h.handler));
                    }, this);
                },

            }),

            declare("View", null, {

                divId: null,
                parentDivId: null,

                constructor: function (args) {
                    declare.safeMixin(this, args);
                    this.preBuild();
                    this.build();
                    this.postBuild();
                },

                preBuild: function() {
                },

                build: function() {
                    console.log("[ViewBase] build() unimplemented in " + this.declaredClass);
                },

                postBuild: function() {
                },

            }),
        ];
    }
);
