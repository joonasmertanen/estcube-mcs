define([
    "dojo/_base/declare",
    "dgrid/OnDemandGrid",
    "../common/Constants",
    "../common/Utils",
    "./ViewBase",
    "../common/Filter",
    ],

    function(declare, Grid, Constants, Utils, ViewBase, Filter) {

        return [
            declare("ParametersController", Controller, {
                channelHandler: function(message, channel) {

                    // apply filter if exists
                    if (this.filter != null) {
                        if (Filter.reject(this.filter, message)) {
                            return;
                        }
                    }

                    message.storeId = Utils.getParameterId(message);
                    this.store.put(message);
                },
            }),

            declare("ParametersView", View, {

                build: function() {
                    var grid = new Grid({
                            store: this.store,
                            columns: {
                                issuedBy: { label: "Source" },
                                name: { label: "Name" },
                                value: { label: "Value" },
                                unit: { label: "Unit" },
                                timestamp: { label: "Timestamp", formatter: Utils.formatDate },
                                datasetidentifier: { label: "Data Set" },
                                type: { label: "Type" },
                                description: { label: "Description" },
                            }
                    }, this.parentDivId);
                    grid.startup();
                },
            }),

        ];
    }
);