define([
	"dojo/_base/declare", 
    "dijit/form/Button",
    "dijit/Dialog",
    "dojo/_base/xhr",
	"dojo/on",
    "../common/Constants",
    "./ViewBase", 
    "dijit/form/ValidationTextBox",
    "dijit/form/TextBox",
    "dojox/grid/DataGrid",
    "dojo/data/ItemFileWriteStore",
    "dojo/dom-attr"
    ],

	function(declare, Button, Dialog, xhr, on, Constants, ViewBase, ValidationTextBox, TextBox, DataGrid, ItemFileWriteStore, domAttr) {
	
		return [
	
			declare("RadioBeaconView", View, {
				                
				build : function() {
					
					// not ASCII sysmbol - á
					var radioBeaconTextBox = new dijit.form.ValidationTextBox({
						name : this.radioBeaconMessage,
						value : "",
						placeHolder : "Radio Beacon Message",
						maxLenght : "100",
						style : "width: 400px;",
						regExp : "^[\\u0000-\\u007F]*$",
						invalidMessage : "Message should contain only ASCII symbols!"
					}, this.radioBeaconMessage);
					
			        var beaconMessageAssigneeTextBox = new dijit.form.TextBox({
			            name: this.beaconMessageAssignee,
			            value: "",
			            placeHolder: "Beacon Message Assignee"
			        }, this.beaconMessageAssignee);

					var sendButton = new Button({
						label : "Save",
						onClick : function() {
							if (!checkInputValidity()) {
								showDialog("Error!", "Some field values are incorrect!");
							}
							else {
								var radioBeaconData = {
									radioBeaconMessage: radioBeaconTextBox.get("value"),
									beaconMessageAssignee: beaconMessageAssigneeTextBox.get("value"),
									timestamp: new Date()
								};
							    var xhrArgs = {
							    	url: "/radioBeacon",
							    	postData: dojo.toJson(radioBeaconData),
							    	handleAs: "text",
							    	load: function(data){
							    		if (data == "success") {
							    			showDialog("Info", "Radio beacon sending succeeded.");
							    			radioBeaconTextBox.set("value", "");
							    			beaconMessageAssigneeTextBox.set("value", "");
							    		}
							    		else {
							    			showDialog("Error!", "Failed to send the message. <br /> Server error occured.");
							    		}
							    	},
							    	error: function(error){
							    		showDialog("Error!", "Failed to send the message. <br /> Server error occured.");
							    	}
							    };
							    var deferred = dojo.xhrPost(xhrArgs);

							}
						}
					}, "sendToSatellite");
					
					var checkRadioBeaconMessageButton = new Button({
						label : "Translate Message",
						onClick : function() {
							if (!checkInputValidity()) {
								showDialog("Error!", "Some field values are incorrect!");
							}
							else {
							    var xhrArgs = {
							    	url: "/translateRadioBeacon",
							    	postData: radioBeaconTextBox.get("value"),
							    	handleAs: "text",
							    	load: function(data){
							    		if (data != "error") {
							    			showDialog("Info!", "Radio beacon message was successfully translated.");
							    			dojo.style(dojo.byId('beaconParameters'), "display", "block");
							    			initGrid(data);
							    		}
							    		else {
							    			showDialog("Error!", "Failed to check the message. <br /> Server error occured.");
							    		}
							    	},
							    	error: function(error){
							    		showDialog("Error!", "Failed to check the message. <br /> Server error occured.");
							    	}
							    };
							    var deferred = dojo.xhrPost(xhrArgs);

							}
						}
					}, "checkBeaconMessage");
					
					var getCurrentFromattedTime = function() {
						var date = new Date();
						var currentHour = date.getHours();
						var currentMinutes = date.getMinutes();
						return currentHour + " : " + currentMinutes;
					};
					
					var getCurrentFromattedDate = function() {
						var date = new Date();
						var monthNames = new Array(12);
						monthNames[0] = "Jan";
						monthNames[1] = "Feb";
						monthNames[2] = "Mar";
						monthNames[3] = "Apr";
						monthNames[4] = "May";
						monthNames[5] = "Jun";
						monthNames[6] = "Jul";
						monthNames[7] = "Aug";
						monthNames[8] = "Sep";
						monthNames[9] = "Oct";
						monthNames[10] = "Nov";     
						monthNames[11] = "Dec";
						return monthNames[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear();
					};
					
					var updateTimeButton = new Button({
						label : "Update Time",
						onClick : function() {
							domAttr.set("setBeaconTime", "value", getCurrentFromattedTime());
							domAttr.set("setBeaconDate", "value", getCurrentFromattedDate());
						}
					}, "updateDateTime");
					
					var saveTranslatedBeaconButton = new Button({
						label : "Save",
						onClick : function() {
							// TODO: save beacon function
							alert("not implemented");
						}
					}, "saveTranslatedBeacon");
					
					
					var checkInputValidity = function() {
						if ((beaconMessageAssigneeTextBox.get("value") == null) || 
							(beaconMessageAssigneeTextBox.get("value") == undefined) ||
							!(radioBeaconTextBox.isValid()) || 
							(beaconMessageAssigneeTextBox.get("value") == "")) {
							return false;
						}
						return true;
					};
					
					var showDialog = function(title, message) {
				        var myDialog = new Dialog({
				            title: title,
				            content: message,
				            style: "width: 300px"
				        });
				        myDialog.show();
					};
					
					var initGrid = function(responseData) {
						// transform received string to JSON object
						var responseData = eval("(" + responseData + ')');
						var data = null;
						if (responseData.parameters.beaconMode == "Normal mode") {
						    data = {
						    	items: [
						    	   { parameter: "EPS timestamp", value: (responseData.parameters.EPStimestamp != undefined ? responseData.parameters.EPStimestamp : ""), metric: "Timestamp" },
						    	   { parameter: "Main bus voltage", value: (responseData.parameters.mainBusVoltage != undefined ? responseData.parameters.mainBusVoltage : ""), metric: "V"},
						    	   { parameter: "Average power balance", value: (responseData.parameters.averagePowerBalance != undefined ? responseData.parameters.averagePowerBalance : ""), metric: "W"},
						    	   { parameter: "Battery A voltage", value: (responseData.parameters.batteryAVoltage != undefined ? responseData.parameters.batteryAVoltage : ""), metric: "V" },
						    	   { parameter: "Battery B voltage", value: (responseData.parameters.batteryBVoltage != undefined ? responseData.parameters.batteryBVoltage : ""), metric: "V"},
						    	   { parameter: "Battery A temperature", value: (responseData.parameters.batteryATemperature != undefined ? responseData.parameters.batteryATemperature : ""), metric: "V"},
						    	   { parameter: "Spin rate Z", value: (responseData.parameters.spinRateZ != undefined ? responseData.parameters.spinRateZ : ""), metric: "deg/s" },
						    	   { parameter: "Received signal strength", value: (responseData.parameters.receivedSignalStrength != undefined ? responseData.parameters.receivedSignalStrength : ""), metric: "dBm"},
						    	   { parameter: "Satellite mission phase", value: (responseData.parameters.satelliteMissionPhase != undefined ? responseData.parameters.satelliteMissionPhase : ""), metric: ""},
						    	   { parameter: "Time since last reset", value: (responseData.parameters.timeSinceLastReset != undefined ? responseData.parameters.timeSinceLastReset : ""), metric: "Hours" },
						    	   { parameter: "Tether current", value: (responseData.parameters.tetherCurrent != undefined ? responseData.parameters.tetherCurrent : ""), metric: "mA"},
						    	   { parameter: "Time since last error", value: (responseData.parameters.timeSinceLastError != undefined ? responseData.parameters.timeSinceLastError : ""), metric: "Hours"},
						    	   { parameter: "CDHS System status", value: (responseData.parameters.CDHSSystemStatus != undefined ? responseData.parameters.CDHSSystemStatus : ""), metric: "" },
						    	   { parameter: "EPS System status", value: (responseData.parameters.EPSSystemStatus != undefined ? responseData.parameters.EPSSystemStatus : ""), metric: ""},
						    	   { parameter: "ADCS System status", value: (responseData.parameters.ADCSSystemStatus != undefined ? responseData.parameters.ADCSSystemStatus : ""), metric: ""},
						    	   { parameter: "COM System status", value: (responseData.parameters.COMSystemStatus != undefined ? responseData.parameters.COMSystemStatus : ""), metric: ""},
						    	   { parameter: "Beacon Mode", value: (responseData.parameters.beaconMode != undefined ? responseData.parameters.beaconMode : ""), metric: ""}
						    	]
						    };
						}
						if (responseData.parameters.beaconMode == "Safe mode") {
						    data = {
						    	items: [
						    	   { parameter: "EPS timestamp", value: (responseData.parameters.EPStimestamp != undefined ? responseData.parameters.EPStimestamp : ""), metric: "Timestamp" },
						    	   { parameter: "Error code 1", value: (responseData.parameters.errorCode1 != undefined ? responseData.parameters.errorCode1 : ""), metric: ""},
						    	   { parameter: "Error code 2", value: (responseData.parameters.errorCode2 != undefined ? responseData.parameters.errorCode2 : ""), metric: ""},
						    	   { parameter: "Error code 3", value: (responseData.parameters.errorCode3 != undefined ? responseData.parameters.errorCode3 : ""), metric: "" },
						    	   { parameter: "Time in safe mode", value: (responseData.parameters.timeInSafeMode != undefined ? responseData.parameters.timeInSafeMode : ""), metric: "Minutes"},
						    	   { parameter: "Main bus voltage", value: (responseData.parameters.mainBusVoltage != undefined ? responseData.parameters.mainBusVoltage : ""), metric: "V"},
						    	   { parameter: "Status 1", value: (responseData.parameters.status1 != undefined ? responseData.parameters.status1 : ""), metric: "" },
						    	   { parameter: "Status 2", value: (responseData.parameters.status2 != undefined ? responseData.parameters.status2 : ""), metric: ""},
						    	   { parameter: "Status 3", value: (responseData.parameters.status3 != undefined ? responseData.parameters.status3 : ""), metric: ""},
						    	   { parameter: "Battery A voltage", value: (responseData.parameters.batteryAVoltage != undefined ? responseData.parameters.batteryAVoltage : ""), metric: "V" },
						    	   { parameter: "Battery B voltage", value: (responseData.parameters.batteryBVoltage != undefined ? responseData.parameters.batteryBVoltage : ""), metric: "V"},
						    	   { parameter: "Battery A temperature", value: (responseData.parameters.batteryATemperature != undefined ? responseData.parameters.batteryATemperature : ""), metric: "V"},
						    	   { parameter: "Battery B temperature", value: (responseData.parameters.batteryBTemperature != undefined ? responseData.parameters.batteryBTemperature : ""), metric: "V" },
						    	   { parameter: "Power balance", value: (responseData.parameters.powerBalance != undefined ? responseData.parameters.powerBalance : ""), metric: "W"},
						    	   { parameter: "Firmware version", value: (responseData.parameters.firmwareVersion != undefined ? responseData.parameters.firmwareVersion : ""), metric: "Version number"},
						    	   { parameter: "Crash counter", value: (responseData.parameters.crashCounter != undefined ? responseData.parameters.crashCounter : ""), metric: "Number of crashes"},
						    	   { parameter: "Forwarded RF power", value: (responseData.parameters.forwardedRFPower != undefined ? responseData.parameters.forwardedRFPower : ""), metric: "dBm"},
						    	   { parameter: "Reflected RF power", value: (responseData.parameters.reflectedRFPower != undefined ? responseData.parameters.reflectedRFPower : ""), metric: "dBm"},
						    	   { parameter: "Received signal strength", value: (responseData.parameters.receivedSignalStrength != undefined ? responseData.parameters.receivedSignalStrength : ""), metric: "dBm"},
						    	   { parameter: "Beacon Mode", value: (responseData.parameters.beaconMode != undefined ? responseData.parameters.beaconMode : ""), metric: ""}
						    	]
						    };
						}
						
					    
						var store = new ItemFileWriteStore({data: data});
						
					    var layout = [[   
							{ "name": "Parameter", "field": "parameter", "width": "300px" },        	
							{ "name": "Value", "field": "value", "width": "200px" },
				            { "name": "Metric", "field": "metric", "width": "120px" }
					    ]];
					    
					    var grid = new DataGrid({
					    	id: 'grid',
					    	store: store,
					        structure: layout
					    });

		    			grid.placeAt("gridDiv");
		    			grid.startup();
				    };
				    
				}
	
		})
	
		];
});