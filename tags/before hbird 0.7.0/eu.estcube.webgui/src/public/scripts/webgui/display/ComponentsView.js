define([
    "dojo/_base/declare",
    "dijit/registry",
    "dojox/grid/DataGrid",
    "../common/Constants",
    "../common/Utils",
    "./ViewBase"
    ],

    function(declare, Registry, DataGrid, Constants, Utils, ViewBase) {

        function getStatus(component, now) {
            var timestamp = parseInt(component.timestamp);
            var period = parseInt(component.period);
            var diff = now - (timestamp + period);
            return diff <= 0 ? "OK" : (diff > period ? "Error" : "Warning");
        }

        return [
            declare("ComponentsController", Controller, {

                getId: function(component) {
                    return component.issuedBy;
                },

                channelHandler: function(message, channel) {
//                   console.log(JSON.stringify(message));
                    var componentId = this.getId(message);
                    if (componentId == null) {
                        return;
                    }
                    var now = new Date().getTime();
                    this.store.fetchItemByIdentity({
                        scope: this,
                        identity: componentId,
                        onItem: function(item) {
                            if (item == null) {
                                this.store.newItem({
                                    id: componentId,
                                    timestamp: message.timestamp,
                                    issuedBy: message.issuedBy,
                                    description: message.description,
                                    period: message.period,
                                    status: getStatus(message, now),
                                    host: message.host,
                                });
                            } else {
                                this.store.setValue(item, "timestamp", Utils.normalizeForStorage(message.timestamp));
                                this.store.setValue(item, "issuedBy", Utils.normalizeForStorage(message.issuedBy));
                                this.store.setValue(item, "description", Utils.normalizeForStorage(message.description));
                                this.store.setValue(item, "period", Utils.normalizeForStorage(message.period));
                                this.store.setValue(item, "status", getStatus(item, now));
                                this.store.setValue(item, "host", Utils.normalizeForStorage(message.host));
                            }
                        }
                    });
                }
            }),

            declare("ComponentsView", View, {

                divId: "ComponentsView",
                title: "System Components",
                updateInterval: 3000,

                build: function() {
                    var grid = new DataGrid({
                            id: this.divId,
                            title: this.title,
                            store: this.store,
                            structure: [
                                { name: "Component ID", field: "issuedBy", width: "200px", },
                                { name: "Timestamp", field: "timestamp", width: "200px", formatter: Utils.formatDate, },
                                { name: "Status", field: "status", width: "100px", },
                                { name: "Host", field: "host", width: "200px", },
                                { name: "Period", field: "period", width: "40px", },
                                { name: "Description", field: "description", width: "100%", },
                            ]
                    });


                    var container = Registry.byId(this.parentDivId);
                    container.addChild(grid);
                    grid.startup();

                    setInterval(dojo.hitch(this, function() {
                        var now = new Date().getTime();
                        this.store.fetch({
                            scope: this,
                            query: {},
                            onItem: function(item) {
                                if (item != null) {
                                    this.store.setValue(item, "status", getStatus(item, now));
                                }
                            },
                        });
                    }), this.updateInterval);

                }
            })

        ];
    }
);