define([
    "dojo/_base/declare",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/on",
    "dijit/form/Form",
    "dijit/form/Button",
    "dijit/form/ValidationTextBox",
    "dijit/form/SimpleTextarea",
    "dijit/layout/ContentPane",
    "dojox/layout/TableContainer",
    "dgrid/OnDemandGrid",
    "dojo/data/ItemFileWriteStore", // REMOVE!
    "dojo/json",
    "config/config",
    "dojo/_base/xhr",
    "common/formatter/DateFormatterS",
   ],

    function(declare, DomClass, DomConstruct, On, Form, Button, ValidationTextBox, SimpleTextarea, ContentPane, TableContainer, Grid, ItemFileWriteStore, Json, Config, xhr, DateFormatter ) {

        return declare([], {

            constructor: function(args) {
				
				var self = this;
				
				// ------------------------------------------
				// create form
                this.main = DomConstruct.create("div", { "class": "beacon-container", "style": "width:1000px; margin:10px;" });
                this.formWrapperDiv = DomConstruct.create("div", { "style":"width:700px;"},  this.main);
                this.div = DomConstruct.create("div", {},  this.formWrapperDiv);
                this.form = new Form({ encType: "multipart/form-data", action: "", method: "" }, this.div);
                this.table = new TableContainer({cols: 2, labelWidth: 150 });
				
                this.beaconSourceValue = new ValidationTextBox({ 
                	name: "beaconSource", 
                	value:"", 
                	placeHolder: "Input operator name or callsign",
                	label: "Operator name:", 
                	required:true,
                	onChange: function() {
                		self.hideSubmitButton();
                	}
                });
                this.beaconDateTimeValue = new ValidationTextBox({ 
                	name: "beaconDateTime", 
                	label: "Datetime:", 
                	placeHolder: "YYYY-MM-DD HH:II:SS",
                	value:"", 
                	required:false,
                	regExp: "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}",
                	promptMessage: "Date format is YYYY-MM-DD HH:II:SS",
                	onChange: function() {
                		self.hideSubmitButton();
                		self.hideGrid();
                	}
                });
                this.beaconData = new ValidationTextBox({ 
                	name: "beaconData",  
                	value:"", 
                	label: "CW Message:", 
                	placeHolder: "ASCII string", 
                	required: true, 
                	colspan: 2, 
                	style:"width:528px",
                	promptMessage: "Unknown symbols replace with #",
                	onChange: function() {
                		self.hideSubmitButton();
                		self.hideGrid();
                	}
                });

				this.checkButtonSubmit = new Button({ label: "View parameters", title: "Check beacon message" });
                On(this.checkButtonSubmit, "click", function() {
                    self.checkBeaconData();
                });
                
                this.buttonWrapper = new ContentPane({ style: "margin: 0px; padding: 0px;" });
                this.buttonWrapper.addChild(this.checkButtonSubmit);
                
 				
                this.table.addChild(this.beaconData);
                this.table.addChild(this.beaconSourceValue);
                this.table.addChild(this.beaconDateTimeValue);
                this.table.addChild(this.buttonWrapper);
                this.form.domNode.appendChild(this.table.domNode);
                this.table.startup();


				// ------------------------------------------
				// create response grid and buttons.

				this.responseDiv = DomConstruct.create("div", { style:"border: 1px solid darkgray; padding:5px; margin-top:10px"},  this.main);
				this.responseDiv.innerHTML = "<div style='margin-bottom:5px;'><b style='font-size:15px'>Beacon message parameter table</b></div>";
				
                this.grid = new Grid({
                    columns: [
                        { label: "Name", field: "name" },
                        { label: "Value", field: "value" },
                        { label: "Unit", field: "unit" },
                        { label: "Timestamp", field: "timestamp", formatter: DateFormatter, },
                        { label: "Issued by", field: "issuedBy" },
                        { label: "Description", field: "description" },
                    ]
                });
                
                this.responseDiv.appendChild( this.grid.domNode );
                
                this.buttonSubmit = new Button({ label: "Send", title: "Send", style:"float:right" });
                On(this.buttonSubmit, "click", function() {
                    self.submitBeaconData();
                });
                
                this.buttonCancel = new Button({ label: "Cancel", title: "Cancel", style:"float:left" });
                On(this.buttonCancel, "click", function() {
                     self.hideResponseElements();
                });
                this.hideResponseElements();
                
                this.buttonWrapper2 = new ContentPane({ style: "margin: 0px; padding: 0px;" });
                this.buttonWrapper2.addChild(this.buttonSubmit);   
                this.buttonWrapper2.addChild(this.buttonCancel);   
                this.buttonWrapper2.placeAt( this.responseDiv );
                
                var sampleMessage = "<b>Sample safe mode radio beacon message:</b> ES5E/S T TASAZZZ AT DA TT T6SN ZZ 6B TD 5B AB TC FF UE ZU B U WB NC SW KN<br />"
         		sampleMessage += "<b>Sample normal mode radio beacon message:</b> ES5E/S ETASA ZZZZZ ZUABTCFFHTUS5BSNMFNFCAFE6HK"
     			DomConstruct.create( "div", {style:"color:lightgray; margin-top:10px", innerHTML:sampleMessage}, this.main );
                
               
            },
            
            checkBeaconData: function() {
            	var self = this;
            	if (this.form.validate()) {

                    xhr.post({
                        url: Config.BEACON.CHECK_URL,
                        timeout: 3000,
                        content: {
                            source: this.beaconSourceValue.get('value'),
                            datetime: this.beaconDateTimeValue.get('value'),
                            data: this.beaconData.get('value')
                        },
                        load: function( response, ioArgs ) {
                            var jsonData = Json.parse(response) ;
                            var data = [];
                            for( var i in jsonData ) {
                                data.push( jsonData[i] );
                            }
                            self.grid.refresh();
                            self.grid.renderArray( data);
                            self.showResponseElements();
                        }
                    });
                   
                } else {
                    alert("Invalid data! Fix errors and resubmit!");
                }
            	
            },
            
            submitBeaconData: function() {
            	var self = this;
            	if (this.form.validate()) {
				
                    xhr.post({
                        url: Config.BEACON.SUBMIT_URL,
                        timeout: 3000,
                        content: {
                            source: this.beaconSourceValue.get('value'),
                            datetime: this.beaconDateTimeValue.get('value'),
                            data: this.beaconData.get('value')
                        },
                        load: function( response, ioArgs ) {
                        	response = Json.parse(response) 
                            if( response.status == "ok" ) {
                            	self.form.reset();
                            	self.hideResponseElements();
                            }
                            alert( response.message );
                        }
                    });
                    
                } else {
                    alert("Invalid data! Fix errors and resubmit!");
                }
            	
            },

            placeAt: function(container) {
                DomConstruct.place(this.main, container);

            },
            
            showResponseElements: function() {
            	this.responseDiv.style.display="";
            	//this.grid.domNode.style.display=""
            	//this.buttonSubmit.domNode.style.display=""
            	//this.buttonCancel.domNode.style.display=""
            },
            
            hideResponseElements: function() {
            	this.responseDiv.style.display="none";
            	//this.grid.domNode.style.display="none"
            	//this.buttonSubmit.domNode.style.display="none"
            	//this.buttonCancel.domNode.style.display="none"
            },
            

        });
    }
);
