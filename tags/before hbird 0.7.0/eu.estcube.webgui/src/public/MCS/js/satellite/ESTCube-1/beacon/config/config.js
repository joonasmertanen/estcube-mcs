define([
    "dojo/domReady!"
    ],

    function(ready) {
        return {
            routes: {
                ESTCube1_Beacon: {
                    path: "ESTCube-1/beacon",
                    defaults: {
                        controller: "ESTCube-1.beacon/BeaconController",
                        method: "index",
                    }
                },
            },

            BEACON: {
            	CHECK_URL: "/translateRadioBeacon",	
            	SUBMIT_URL: "/radioBeacon"	
            }

        };
    }
);
