define([
    "dojo/store/Memory",
    "dojo/store/Observable",
    "dojo/topic",
    "config/config",
    "./DataHandler",
    "./StoreIdGenerator",
    "./StoreLimitHandler",
    "./StoreMonitor",
    ],

    function(Memory, Observable, Topic, Config, DataHandler, IdGenerator, StoreLimitHandler, StoreMonitor) {

        var channel = Config.WEBSOCKET_SYSTEM_MESSAGES;
        var storeId = "storeId";

        var store = new Observable(new Memory({ idProperty: storeId }));

        var handler = new DataHandler({ channels: [channel], callback: function(message, channel) {
            if (message.type == "SystemMessage") { // TODO - 20.03.2013, kimmell - fix this
                StoreLimitHandler.put(store, IdGenerator.generate(message, storeId), Config.STORE_LIMIT_SYSTEM_MESSAGES);
            }
        }});

        Topic.subscribe(Config.TOPIC_SYSTEM_MESSAGES, function(message) {
            StoreLimitHandler.put(store, IdGenerator.generate(message, storeId), Config.STORE_LIMIT_SYSTEM_MESSAGES);
        });

        new StoreMonitor({ store: store, storeName: "SystemMessageStore" });
        return store;
    }
);