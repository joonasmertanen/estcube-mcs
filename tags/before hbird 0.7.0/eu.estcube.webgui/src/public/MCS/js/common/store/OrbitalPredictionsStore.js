define([
    "dojo/store/Memory",
    "dojo/store/Observable",
    "config/config",
    "./DataHandler",
    "./StoreMonitor",
    ],

    function(Memory, Observable, Config, DataHandler, StoreMonitor) {

		// Messages: {id, timestamp, satellite_id, lat, lon}
        var channel = Config.WEBSOCKET_ORBITAL_PREDICTIONS;
        var storeId = "id";

        var store = new Observable(new Memory({ idProperty: storeId }));
        new StoreMonitor({ store: store, storeName: "OrbitalPredictionsStore" });
        
        var nextID = 1;
        
        setInterval(function() {
        	msg = {
        			id : nextID,
        			timestamp : Date.now(),
        			satellite_id : "ESTCube-1",
        			lat : 59 + nextID * 0.01,
        			lon : 26.7
        	};
        	
        	store.put(msg);
        	
        	nextID++;
        }, 1000);

        // TODO: Enable
        /*var handler = new DataHandler({ channels: [channel], callback: function(message, channel) {
        	store.put(message)
        }});*/

        return store;
    }
);