define([
    "dojo/_base/declare",
    "dojo/dom-construct",
    "dojo/dom-attr",
    "dijit/layout/ContentPane",
    "./ContentProvider",
    ],

    function(declare, DomConstruct, DomAttr, ContentPane, ContentProvider) {
        return declare(ContentProvider, {

            getContent: function() {
                var img = DomConstruct.create("img", { alt: "Webcam image",  src: this.initialImage });
                var pane = new ContentPane({ content: img });
                this.store.query({ name: this.imageId }).observe(function(value, removedFrom, insertedInto) {
                    if (!/image\//.test(value.type)) {
                        // no type set; return
                        return;
                    }
                    dojo.attr(img, { src : "data:" + value.type + ";base64," + value.rawdata, alt: value.name });
                }, true);

                return pane;
            },

        });
    }
);