define([
    "dojo/_base/declare",
    "dojo/dom-construct",
    "config/config",
    "common/store/WebCamStore",
    "common/display/WebCamContentProvider",
    ],

    function(declare, DomConstruct, Config, store, WebCamContentProvider) {

        return declare([], {

            constructor: function(args) {
                this.provider = new WebCamContentProvider({
                    store: store,
                    initialImage: Config.ES5EC_WEBCAM.initialImage,
                    imageId: Config.ES5EC_WEBCAM.imageId,
                });
            },

            placeAt: function(container) {
                this.provider.getContent().placeAt(container);
            },

        });
    }
);
