define([
    "dojo/_base/declare",
    "dojo/dom-class",
    "dojo/dom-construct",
    "config/config",
    "common/formatter/DateFormatter",
    "common/store/BusinessCardStore",
    "common/store/PageStore",
    "common/store/WebCamStore",
    "common/store/ParameterStore",
    "common/display/Dashboard",
    "common/display/GenericContentProvider",
    "common/display/GridContentProvider",
    "common/display/ListContentProvider",
    "common/display/WebCamContentProvider",
    "common/display/CompositeContentProvider",
    "common/display/GaugeContentProvider",
    "common/display/DGridTooltipSupport",
    ],

    function(declare, DomClass, DomConstruct, Config, DateFormatter,
        BusinessCardStore, PageStore, WebCamStore, ParameterStore,
        Dashboard, GenericContentProvider, GridContentProvider, ListContentProvider, WebCamContentProvider, CompositeContentProvider, GaugeContentProvider, DGridTooltipSupport) {

        return declare([], {

            constructor: function(args) {
                var config = [
                    {
                        title: "Components",
                        settings: "MCS Components.",
                        contentProvider: new GridContentProvider({
                            columns: {
                                issuedBy: { label: "Component ID", className: "field-issuedBy" },
                                host: { label: "Host", className: "field-host" },
                                timestamp: { label: "Time", formatter: DateFormatter, className: "field-timestamp" },
                            },
                            store: BusinessCardStore,
                            query: { type: "BusinessCard" },
                            onStartup: function(provider) {
                                new DGridTooltipSupport(provider.grid, function(entry) {
                                    return entry.description;
                                });
                            },
                        }),
                        col: 0,
                        row: 0,
                    },

                    {
                        title: "ETC",
                        closable: true,
                        contentProvider: new GenericContentProvider({ content: "none" }),
                        col: 0,
                        row: 2,
                    },

                    {
                        title: "MCC stats",
                        settings: "System recources monitoring.",
                        contentProvider: new GridContentProvider({
                            columns: {
                                name: { label: "Name" },
                                value: { label: "Value" },
                                unit: { label: "Unit" },
                                timestamp: { label: "Time", formatter: DateFormatter },
                            },
                            store: ParameterStore,
                            query: function(message) {
                                return /^Host\/mcc\/.*/.test(message.name);
                            },
                            onStartup: function(provider) {
                                new DGridTooltipSupport(provider.grid, function(entry) {
                                    return entry.description;
                                });
                            },
                        }),
                        col: 0,
                        row: 1,
                    },

                    {
                        title: "Satellites",
                        settings: "List of satellites",
                        contentProvider: new ListContentProvider({
                            store: PageStore,
                            query: { parentId: "satellites" },
                            formatter: function(item) {
                                return DomConstruct.create("div", { innerHTML: item.label, style: "padding: 3px; margin: 1px" });
                            },
                        }),
                        col: 2,
                        row: 0,
                    },

                    {
                        title: "ES5EC WebCam",
                        contentProvider: new WebCamContentProvider({
                            store: WebCamStore,
                            imageId: Config.DASHBOARD.imageId,
                            initialImage: Config.DASHBOARD.initialImage,
                        }),
                        col: 1,
                        row: 1,
                    },

                    {
                        title: "Temperatures",
                        settings: "Temperatures from different sources.",
                        contentProvider: new GridContentProvider({
                            columns: {
                                issuedBy: { label: "Source" },
                                value: { label: "°C"},
                                timestamp: { label: "Time", formatter: DateFormatter },
                            },
                            store: ParameterStore,
                            query: function(message) {
                                return /.*\.temperature$/.test(message.name);
                            },
                            onStartup: function(provider) {
                                new DGridTooltipSupport(provider.grid, function(entry) {
                                    return entry.description + " " + entry.issuedBy;
                                });
                            },
                        }),
                        col: 2,
                        row: 1,
                    },

                    {
                        title: "Air Temperature",
                        settings: "This is composite content provider including one or more other providers",
                        contentProvider: new CompositeContentProvider({
                            margin: 6,
                            providers: [

                                new CircularLinearGaugeProvider({
                                    gaugeSettings: {
                                        value: 0,
                                        minimum: -50,
                                        maximum: 50,
                                        majorTickInterval: 10,
                                        minorTickInterval: 1,
                                        interactionArea: "none",
                                        animationDuration: 1000,
                                        style: "width: 180px; height: 200px;",
                                        title: "Meteo",
                                    },
                                    textIndicator: { x: 100, y: 170, value: "°C", },
                                    store: ParameterStore,
                                    parameterName: "WeatherStation/meteo.physic.ut.ee/air.temperature",
                                }),

                                new CircularLinearGaugeProvider({
                                    gaugeSettings: {
                                        value: 0,
                                        minimum: -50,
                                        maximum: 50,
                                        majorTickInterval: 10,
                                        minorTickInterval: 1,
                                        interactionArea: "none",
                                        animationDuration: 1000,
                                        style: "width: 180px; height: 200px;",
                                        title: "EMHI",
                                    },
                                    textIndicator: { x: 100, y: 170, value: "°C", },
                                    store: ParameterStore,
                                    parameterName: "WeatherStation/emhi.ee/air.temperature",
                                }),

                                new CircularLinearGaugeProvider({
                                    gaugeSettings: {
                                        value: 0,
                                        minimum: -50,
                                        maximum: 50,
                                        majorTickInterval: 10,
                                        minorTickInterval: 1,
                                        interactionArea: "none",
                                        animationDuration: 1000,
                                        style: "width: 180px; height: 200px;",
                                        title: "Ilm.ee",
                                    },
                                    textIndicator: { x: 100, y: 170, value: "°C", },
                                    store: ParameterStore,
                                    parameterName: "WeatherStation/ilm.ee/air.temperature",
                                }),
                            ],
                        }),
                        col: 1,
                        row: 2,

                    },
                ];
                this.dashboard = new Dashboard({ config: config, columns: Config.DASHBOARD.numberOfColumns });
                DomClass.add(this.dashboard.getContainer().domNode, "fill");
            },

            placeAt: function(container) {
                this.dashboard.getContainer().placeAt(container);
            },

        });
    }
);
