define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/dom-construct",
    "dijit/layout/BorderContainer",
    "dijit/layout/ContentPane",
    "dijit/form/CheckBox",
    "config/config",
    "common/store/OrbitalPredictionsStore",
    ],

    function(declare, Lang, Arrays, DomConstruct, BorderContainer, ContentPane, CheckBox, config, store) {

        return declare([], {
            constructor: function(args) {
            	this.visible = false;
            	
            	var map = new OpenLayers.Map({
                    controls: [
                        new OpenLayers.Control.Navigation(),
                        new OpenLayers.Control.PanZoomBar(),
                        new OpenLayers.Control.ScaleLine(),
                        new OpenLayers.Control.MousePosition(),
                        new OpenLayers.Control.KeyboardDefaults()
                    ],
                    numZoomLevels: 10
                });
            	
            	var groundStations = config.MAP.groundStations;
            	var renderer = OpenLayers.Layer.Vector.prototype.renderers;
            	
            	var mainLayer = new OpenLayers.Layer.WMS(
            			"Global Imagery",
            			"http://maps.opengeo.org/geowebcache/service/wms",
            			{ layers: "bluemarble" },
            			{ tileOrigin: new OpenLayers.LonLat(-180, -90) }
            	);            	
            	
            	var groundStationLocationLayer = new OpenLayers.Layer.Vector("Ground Station Location Geometry Layer", {
                    styleMap: new OpenLayers.StyleMap({
                    	'default': {
                    		strokeOpacity: 1,
	                        strokeWidth: 2,
	                        fillColor: "#FFFF00",
	                        fillOpacity: 0.5,
	                        pointRadius: 4,
	                        strokeColor: "#000000",
	                        cursor: "pointer",
	                        label : "${label}",
	                        fontSize: "12px",
	                        fontFamily: "Courier New, monospace",
	                        fontColor : "#FFFF00",
	                        labelYOffset: -16
                    	},
                    	'select': {
                    		strokeOpacity: 1,
	                        strokeWidth: 2,
	                        fillColor: "#FFA500",
	                        fillOpacity: 0.5,
	                        pointRadius: 4,
	                        strokeColor: "#000000",
	                        cursor: "pointer"
                    	}
                    }),
                    renderers: renderer
                });
            	
            	var groundStationCoverageAreaLayer = new OpenLayers.Layer.Vector("Ground Station Coverage Area Geometry Layer", {
                    styleMap: new OpenLayers.StyleMap({
                    	'default': {
                    		strokeOpacity: 1,
	                        strokeWidth: 2,
	                        fillColor: "#FFFF00",
	                        fillOpacity: 0.15,
	                        strokeColor: "#FF7F00"
                    	}
                    }),
                    renderers: renderer
                });
            	
            	var satelliteLocationLayer = new OpenLayers.Layer.Vector("Satellite Location Prediction Layer", {
            		styleMap : new OpenLayers.StyleMap({'default': {
                		strokeOpacity: 1,
                        strokeWidth: 2,
                        fillColor: "#FF0000",
                        fillOpacity: 0.5,
                        pointRadius: 10,
                        strokeColor: "#000000",
                        cursor: "pointer",
                        label : "${label}",
                        fontSize: "16px",
                        fontFamily: "Courier New, monospace",
                        fontColor : "#FF0000",
                        labelYOffset: -20
                	}}),
                	renderers : renderer
                });
            	
            	groundStationLocationLayer.addFeatures(Arrays.map(groundStations, function(gs) {
            		var point = new OpenLayers.Geometry.Point(gs.initialLocation.lon, gs.initialLocation.lat);
	                var feature = new OpenLayers.Feature.Vector(point);
	                
	                feature.attributes = { label : gs.abbreviation };
	                
	                return feature;
            	}, this));
            	
            	gsCoverageAreaFeatures = {}
            	
            	groundStationCoverageAreaLayer.addFeatures(Arrays.map(groundStations, function(gs) {
            		gsCoverageAreaFeatures[gs.id] = new OpenLayers.Feature.Vector(OpenLayers.Geometry.fromWKT(gs.coverageArea));
            		return gsCoverageAreaFeatures[gs.id];
            	}, this));            	
            	
            	map.addLayer(mainLayer);
            	map.addLayer(groundStationLocationLayer);
            	map.addLayer(groundStationCoverageAreaLayer);
            	map.addLayer(satelliteLocationLayer);
            	
            	var center = null;
            	if(groundStations.length > 0) {
            		var gs = groundStations[0];
            		center = new OpenLayers.LonLat(gs.initialLocation.lon, gs.initialLocation.lat);
            	}
            	map.setCenter(center, 5);
            	
            	this.map = map;
            	
            	// id => {showing, lastUpdate, data = {lon, lat, feature}}
            	var satellitesTracked = {};
            	
            	Arrays.forEach(config.MAP.satellites, function(sat) {
            		satellitesTracked[sat.id] = {showing : true, lastUpdate : -1, data : null};
            	}, this);  	
            	
            	store.query({}).observe(Lang.hitch(this, function(object) {
            		if(object.satellite_id in satellitesTracked) {
            			var track_data = satellitesTracked[object.satellite_id];
            			
	            		if(track_data.data == null) {
	            			var point = new OpenLayers.Geometry.Point(50, 50);
	            			var feature = new OpenLayers.Feature.Vector(point);
	            			feature.attributes = { label : object.satellite_id }
	            			
	            			track_data.data = {lon : 50, lat : 50, feature : feature};
	            			
	            			satelliteLocationLayer.addFeatures([feature]);
	            			
	            			console.log('[map]: Receiving data for ' + object.satellite_id);
	            		}
	            		
	            		if(track_data.lastUpdate < object.timestamp) {
	            			track_data.data.lon = object.lon;
	            			track_data.data.lat = object.lat;
	            			track_data.data.feature.geometry.x = object.lon;
	            			track_data.data.feature.geometry.y = object.lat;
	            			track_data.data.feature.geometry.clearBounds();
	            			
	            			track_data.lastUpdate = object.timestamp;
	            			
	            			if(track_data.showing && this.visible) {	            			
	            				satelliteLocationLayer.redraw();
	            			}
	            		}
            		} else {
            			console.log('[map]: Received data for untracked satellite');
            		}
            	}));

            	this.createUI(groundStationCoverageAreaLayer, gsCoverageAreaFeatures, satelliteLocationLayer, 
            			satellitesTracked);
            },
            
            createUI: function(gsCoverageLayer, gsCoverageFeatures, satLocationLayer, satellitesTracked) {
				this.mainContainer = DomConstruct.create("div", {style: {width: "100%", height: "100%"}});
            	
            	this.layout = new BorderContainer({}, this.mainContainer);
            	this.mapPane = new ContentPane({region : "center"});
            	this.layout.addChild(this.mapPane);
            	
            	var listPane = new ContentPane({id : "lists", region : "right", style : {width : "200px", height : "100%"}});
            	var gsListPane = new ContentPane({id : "gslist", style : {height : "45%","margin-bottom" : "10px", border : "1px solid gray"}});
            	var satListPane = new ContentPane({id : "satlist", style : {height : "45%", border : "1px solid gray"}});
            	listPane.addChild(gsListPane);
            	listPane.addChild(satListPane);
            	
            	this.layout.addChild(listPane);
            	
            	function CreateCheckboxes(source, parent, callback) {
            		Arrays.forEach(source, function(s) {
                		var checkBox = new CheckBox({id : (s.id + "-cb"), checked : true})
                		checkBox.onChange = function(value) {
                			callback(value, s.id);
                		}
                		
                		parent.addChild(checkBox);
                		DomConstruct.create("label", {"for" : (s.id + "-cb"), innerHTML : s.abbreviation}, parent.domNode);
                		DomConstruct.create("br", {}, parent.domNode)
            		}, this);
            	}
            	
            	CreateCheckboxes(config.MAP.groundStations, gsListPane, Lang.hitch(this, function(value, id) {
            		if(value) {
            			gsCoverageLayer.addFeatures([gsCoverageFeatures[id]]);
            		} else {
            			gsCoverageLayer.removeFeatures([gsCoverageFeatures[id]]);
            		}
            	}));
            	
            	CreateCheckboxes(config.MAP.satellites, satListPane, Lang.hitch(this, function(value, id) {
            		satellitesTracked[id].showing = value;
            		
            		if(value) {
        				satLocationLayer.addFeatures([satellitesTracked[id].data.feature]);
        			} else {
        				satLocationLayer.removeFeatures([satellitesTracked[id].data.feature]);
        			}
            	}));
            },

            placeAt: function(container) {            	
            	DomConstruct.place(this.mainContainer, container);
            	this.layout.startup();
            	
            	this.visible = true;

            	this.map.render(this.mapPane.domNode);
            },
            
            destroy: function() {
            	this.visible = false;
            }

        });
    }
);
