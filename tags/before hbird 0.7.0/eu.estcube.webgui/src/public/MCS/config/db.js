define([
    "dojo/domReady!"
    ],

    function() {

        return [
            // level 1
            { id: "system", parentId: "", priority: "1", label: "System", url: "/MCS", tooltip: "", icon: "/images/transmit.png" },
            { id: "satellites", parentId: "", priority: "2", label: "Satellites", url: "/MCS", tooltip: "Satellites" },
            { id: "groundStations", parentId: "", priority: "3", label: "Ground Stations", url: "/MCS", tooltip: "Ground Stations" },
            { id: "logout", parentId: "", priority: "4", label: "Logout", url: "LOGOUT", tooltip: "Logout user" },

            // level 2: system
            { id: "dashboard", parentId: "system", priority: "1", label: "Dashboard", url: "DASHBOARD", tooltip: "MCS starting page" },
            { id: "map", parentId: "system", priority: "2", label: "Map", url: "MAP", tooltip: "Map view" },
            { id: "contact-planning", parentId: "system", priority: "3", label: "* Contact Planning", url: "/MCS/system/contact-planning.html", tooltip: "Contact Planning" },
            { id: "components", parentId: "system", priority: "4", label: "System Components", url: "SYSTEM_COMPONENTS", tooltip: "Status of system components" },
            { id: "log", parentId: "system", priority: "5", label: "System Log", url: "SYSTEM_LOG", tooltip: "System log" },
            { id: "messages", parentId: "system", priority: "6", label: "System Messages", url: "MESSAGES", tooltip: "System messages" },
            { id: "diagnostics", parentId: "system", priority: "7", label: "UI Diagnostics", url: "DIAGNOSTICS", tooltip: "UI diagnostics" },
            { id: "demo-module", parentId: "system", priority: "8", label: "Demo module", url: "DEMO", tooltip: "Demo module" },

            // level 2: satellites
            { id: "ESTCube-1", parentId: "satellites", priority: "1", label: "ESTCube-1", url: "/MCS/satellites/ESTCube-1/", tooltip: "Satellite ESTCube-1" },
            { id: "ESTCube-1-FS", parentId: "satellites", priority: "2", label: "ESTCube-1-FS", url: "/MCS/satellites/ESTCube-1-FS/", tooltip: "Satellite ESTCube-1-FS" },

            // level 2: ground stations
            { id: "ES5EC", parentId: "groundStations", priority: "1", label: "ES5EC", url: "/MCS/gs/ES5EC/", tooltip: "Ground Station ES5EC" },

            // level 3: satellites -> ESTCube-1
            { id: "ESTCube-1/home", parentId: "ESTCube-1", priority: "1", label: "* Home", url: "/MCS/satellites/ESTCube-1/", tooltip: "ESTCube-1 Dashboard" },
            { id: "ESTCube-1/COM", parentId: "ESTCube-1", priority: "2", label: "* COM", url: "/MCS/satellites/ESTCube-1/com.html", tooltip: "Communication System" },
            { id: "ESTCube-1/TCS", parentId: "ESTCube-1", priority: "3", label: "* TCS", url: "/MCS/satellites/ESTCube-1/tcs.html", tooltip: "Thermal Control System" },
            { id: "ESTCube-1/CDHS", parentId: "ESTCube-1", priority: "4", label: "* CDHS", url: "/MCS/satellites/ESTCube-1/cdhs.html", tooltip: "Command and Data Handling System" },
            { id: "ESTCube-1/CAM", parentId: "ESTCube-1", priority: "5", label: "* CAM", url: "/MCS/satellites/ESTCube-1/cam.html", tooltip: "On Board Camera" },
            { id: "ESTCube-1/EPS", parentId: "ESTCube-1", priority: "6", label: "* EPS", url: "/MCS/satellites/ESTCube-1/eps.html", tooltip: "Electrical Power System" },
            { id: "ESTCube-1/SYS", parentId: "ESTCube-1", priority: "7", label: "* SYS", url: "/MCS/satellites/ESTCube-1/sys.html", tooltip: "Systems Engineering" },
            { id: "ESTCube-1/ADCS", parentId: "ESTCube-1", priority: "8", label: "* ADCS", url: "/MCS/satellites/ESTCube-1/adcs.html", tooltip: "Attitude Determination and Control System" },
            { id: "ESTCube-1/beacon", parentId: "ESTCube-1", priority: "9", label: "Radio Beacon", url: "ESTCube1_Beacon", tooltip: "Radio Beacon Input" },
            { id: "ESTCube-1/TLE", parentId: "ESTCube-1", priority: "10", label: "TLE", url: "ESTCube1_TLE", tooltip: "Two Line Element Input" },
            { id: "ESTCube-1/communication", parentId: "ESTCube-1", priority: "11", label: "* Communication", url: "/MCS/satellites/ESTCube-1/communication.html", tooltip: "Satellite Communication Overview" },

            // level 3: satellites -> ESTCube-1-FS
            { id: "ESTCube-1-FS/home", parentId: "ESTCube-1-FS", priority: "0", label: "* Home", url: "/MCS/satellites/ESTCube-1-FS/", tooltip: "ESTCube-1-FS Dashboard" },
            { id: "ESTCube-1-FS/COM", parentId: "ESTCube-1-FS", priority: "1", label: "* COM", url: "/MCS/satellites/ESTCube-1-FS/com.html", tooltip: "Communication System" },
            { id: "ESTCube-1-FS/TCS", parentId: "ESTCube-1-FS", priority: "2", label: "* TCS", url: "/MCS/satellites/ESTCube-1-FS/tcs.html", tooltip: "Thermal Control System" },
            { id: "ESTCube-1-FS/CDHS", parentId: "ESTCube-1-FS", priority: "3", label: "* CDHS", url: "/MCS/satellites/ESTCube-1-FS/cdhs.html", tooltip: "Command and Data Handling System" },
            { id: "ESTCube-1-FS/CAM", parentId: "ESTCube-1-FS", priority: "4", label: "* CAM", url: "/MCS/satellites/ESTCube-1-FS/cam.html", tooltip: "On Board Camera" },
            { id: "ESTCube-1-FS/EPS", parentId: "ESTCube-1-FS", priority: "5", label: "* EPS", url: "/MCS/satellites/ESTCube-1-FS/eps.html", tooltip: "Electrical Power System" },
            { id: "ESTCube-1-FS/SYS", parentId: "ESTCube-1-FS", priority: "6", label: "* SYS", url: "/MCS/satellites/ESTCube-1-FS/sys.html", tooltip: "Systems Engineering" },
            { id: "ESTCube-1-FS/ADCS", parentId: "ESTCube-1-FS", priority: "7", label: "* ADCS", url: "/MCS/satellites/ESTCube-1-FS/adcs.html", tooltip: "Attitude Determination and Control System" },
            { id: "ESTCube-1-FS/beacon", parentId: "ESTCube-1-FS", priority: "8", label: "* Radio Beacon", url: "/MCS/satellites/ESTCube-1-FS/beacon.html", tooltip: "Radio Beacon Input" },
            { id: "ESTCube-1-FS/TLE", parentId: "ESTCube-1-FS", priority: "9", label: "* TLE", url: "/MCS/satellites/ESTCube-1-FS/tle.html", tooltip: "Two Line Element Input" },
            { id: "ESTCube-1-FS/communication", parentId: "ESTCube-1-FS", priority: "10", label: "* Communication", url: "/MCS/satellites/ESTCube-1-FS/communication.html", tooltip: "Satellite Communication Overview" },

            // level 3: ground stations -> ES5EC
            { id: "ES5EC/home", parentId: "ES5EC", priority: "0", label: "* ES5EC Dashboard", url: "/MCS/gs/ES5EC/", tooltip: "ES5EC Dashboard" },
            { id: "ES5EC/rotator", parentId: "ES5EC", priority: "1", label: "* Rotator", url: "/MCS/gs/ES5EC/rotator.html", tooltip: "Antenna Rotator" },
            { id: "ES5EC/radio", parentId: "ES5EC", priority: "2", label: "* Radio", url: "/MCS/gs/ES5EC/radio.html", tooltip: "Radio Device" },
            { id: "ES5EC/TNC", parentId: "ES5EC", priority: "3", label: "TNC", url: "ES5EC_TNC", tooltip: "Terminal Node Controller" },
            { id: "ES5EC/SDR", parentId: "ES5EC", priority: "4", label: "* SDR", url: "/MCS/gs/ES5EC/sdr.html", tooltip: "Software Defined Radio" },
            { id: "ES5EC/kadi", parentId: "ES5EC", priority: "5", label: "* Kadi", url: "/MCS/gs/ES5EC/kadi.html", tooltip: "Preselector" },
            { id: "ES5EC/webcam", parentId: "ES5EC", priority: "6", label: "Webcam", url: "ES5EC_WEBCAM", tooltip: "Web Camera" },
            { id: "ES5EC/weather", parentId: "ES5EC", priority: "7", label: "Weather", url: "ES5EC_WEATHER", tooltip: "Weather Information" },
        ];
    }
);
