define(["system/Router", "dojo/domReady!"], function( router ) {
	return {
		menu: [
		    { 
		        "label": "Example module",
		        "url": router.getUrl("exampleModule"),
		    },
		    { 
		        "label": "Log",
		        "url": router.getUrl("log"),
		    },
		     { 
		        "label": "Messages",
		        "url": router.getUrl("messages"),
		    },
		    { 
		        "label": "Components",
		        "url": router.getUrl("components"),
		    },
		    { 
		        "label": "ES5EC",
		        "children": [
		        	{
		        		"label": "WebCam",
		        		"url": router.getUrl("ES5EC_webcam"),
		        	},
		        	{
		        		"label": "WebCam tmp",
		        		"url": router.getUrl("ES5EC_webcam_tmp"),
		        	}
		        ]
		    },
		    {
		        "label": "Logout",
		        "url": router.getUrl("messages"),
		        "style": "float:right",
		    },
		]
	};
});
