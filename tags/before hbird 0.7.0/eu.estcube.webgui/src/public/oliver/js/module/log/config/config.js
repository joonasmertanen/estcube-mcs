define([], function() {
	return {
		routes: {
			"log": {
				"path": "log/",
				"defaults": {
					"controller": "log/controller/LogController",
					"method": "display",
				},
				"childRoutes": {
					"item":{
						"path": ":id",
						"defaults": {
							"method": "element"
						}
					},
					"itemX":{
						"path": ":id/xx",
						"defaults": {
							"method": "elementX"
						}
					}
				}
			},
		}
	};
});
