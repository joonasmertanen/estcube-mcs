define([
	"dojo/_base/declare",
	"system/Controller", 
	"./../view/IndexView",
], 
function( 
	declare,
	Controller,
	IndexView
) {
	var s = declare( [Controller], {
		index: function( params ) {
			this.placeWidget( IndexView, true );
		},
		
		item: function( params ) {
			this.placeWidget( new ItemView(), true );
		},
		
		setup: function() {
			console.log("Example module setup");
		},
		
		kill: function() {
			console.log("Example module killed");
		}
	});
	return new s();
})
