define([
	"dojo/_base/declare",
	"system/Router",
	"dojox/grid/DataGrid",
	"dojo/data/ItemFileWriteStore",
	"config/config",
	"system/View"
],
function(declare, router, DataGrid, ItemFileWriteStore, config, View){
	
	this.store = new ItemFileWriteStore({
		data: {
			identifier: "id",
			items: [
				{id: 1, name:"Oliver", age: 26},
				{id: 2, name:"Kalle", age: 27},
				{id: 3, name:"Peeter", age: 29},
			]
		}
	});
	
	this.grid = new DataGrid({
		id: "exampleModuleIndex",
		title: "Components",
		store: this.store,
		height: "500px",
		structure: [
			{ name: "Name", field: "name", width: "200px", },
			{ name: "Age", field: "age", width: "100%", },
		]
	});
	
	return dojo.safeMixin( View, {
		grid: this.grid,
		placeAt: function( container ) {
			this.grid.placeAt( container );
			this.grid.startup();
		},
		destroy: function() {
			this.grid.destroy();
		}
	});
});