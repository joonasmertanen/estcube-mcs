define([
	"dojo/_base/declare",
	"system/Controller", 
	"./../view/ComponentsView",
], 
function( 
	declare,
	Controller,
	ComponentsView
) {
	var s = declare( [Controller], {
		
		index: function( params ) {	
			this.placeWidget( this.view );			
		},
		
		setup: function() {
			if( !this.view ) {
				this.view = new ComponentsView();
			}
		},
		
		destroy: function() {
			//this.view.destroy();
		}
		
	});
	return new s();
})
