define([
	"dojo/_base/declare",
	"system/Controller", 
], 
function(
	declare,
	Controller 
) {
	var s = declare( [Controller], {
		display: function( params ) {
			this.placeHtml("messages");
		},
		
		setup: function() {
			console.log("Messages setup");
		},
		
		destroy: function() {
			console.log("Messages destroy");
		}
	});
	return new s();
});