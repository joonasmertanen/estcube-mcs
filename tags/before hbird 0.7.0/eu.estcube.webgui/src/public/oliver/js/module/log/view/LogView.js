define([
	"dojo/_base/declare",
	"dijit/_WidgetBase", 
	"dijit/_TemplatedMixin",
	"dojo/text!./../template/LogView.html",
	"system/Router"
],
function(declare, WidgetBase, TemplatedMixin, template, router){
	return declare([WidgetBase, TemplatedMixin], {		
		templateString: template,
		variable: "VARIABLE VALUE",
		link: router.getPathJs("log/item", {id:1})			
	});
});