define([
	"dojo/_base/declare",
	"dojox/grid/DataGrid",
	"config/config",
	"dijit/registry",
	"dojo/data/ItemFileWriteStore",
	"system/net/WebSocketProxy",
	"system/formatter/DateFormatter",
	],
	function(declare, DataGrid, config, registry, ItemFileWriteStore, WebSocketProxy,  DateFormatter ){
	
		return declare([], {
			constructor: function( args ) {
				
				this.init();
				
				this.grid = new DataGrid({
					id: "components",
					title: "Components",
					store: this.store,
					height: "500px",
					structure: [
						{ name: "Component ID", field: "issuedBy", width: "200px", },
						{ name: "Timestamp", field: "timestamp", width: "200px", formatter: DateFormatter,},
					   	{ name: "Status", field: "status", width: "100px", },
						{ name: "Host", field: "host", width: "200px", },
						{ name: "Period", field: "period", width: "40px", },
						{ name: "Description", field: "description", width: "100%", },
					]
				});
				
			},
			
			init: function() {
				if( this.initialized == true ) {
					return;
				}		
				this.initialized = true;
			
				var channel = "/hbird.out.businesscards"	
				var proxy = new WebSocketProxy({ delay: 1500, channels: [channel] });
				
				dojo.subscribe(channel, dojo.hitch(this, this.channelHandler));
				dojo.publish(config.TOPIC_CHANNEL_REQUEST, [{ channel: channel, source: this.id }]);
				
				this.store = new ItemFileWriteStore({
					data: {
						identifier: "id",
						items: []
					}
				});
				
			},
			
			placeAt: function( container ) {
				this.grid.placeAt( container );
				this.grid.startup();
			},
			
			destroy: function() {
				this.grid.destroy();
			},
			
			getStatus: function(component, now) {
				var timestamp = parseInt(component.timestamp);
				var period = parseInt(component.period);
				var diff = now - (timestamp + period);
				return diff <= 0 ? "OK" : (diff > period ? "Error" : "Warning");
			},
			
			channelHandler: function(message, channel) {
			 	var now = new Date().getTime();
				this.store.fetchItemByIdentity({
					scope: this,
					identity: message.issuedBy,
					onItem: function( item ) {
						if (item == null) {
							this.store.newItem({
								id: message.issuedBy,
								timestamp: message.timestamp,
								issuedBy: message.issuedBy,
								description: message.description,
								period: message.period,
								status: this.getStatus(message, now),
								host: message.host,
							});
						} else {
							this.store.setValue(item, "timestamp", message.timestamp);
							this.store.setValue(item, "issuedBy", message.issuedBy);
							this.store.setValue(item, "description", message.description);
							this.store.setValue(item, "period", message.period);
							this.store.setValue(item, "status", this.getStatus(message, now));
							this.store.setValue(item, "host", message.host);
						}
					}
				});
			},

		});
	}
);