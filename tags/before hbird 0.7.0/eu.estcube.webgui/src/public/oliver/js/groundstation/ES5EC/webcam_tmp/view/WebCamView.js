define([
	"dojo/_base/declare",
	"dojo/dom-construct",
	"config/config",
	"dijit/layout/ContentPane",
    "ES5ECWebCam/model/WebCamStore"
	],
	function(declare, DomConstruct, config, ContentPane, store ){
	
		return declare([], {
		
			constructor: function( args ) {
				this.init();
			},
			
			init: function() {
				if( this.initialized == true ) {
					return;
				}		
				this.initialized = true;
				
				var img = DomConstruct.create("img", { alt: "Webcam image",  src: "", style: "border: 1px solid red;" });
				
                store.query({ storeId: config.ES5EC.imageId }).observe(function(value, removedFrom, insertedInto) {
                    if (!/image\//.test(value.type)) {
                        // no type set; return
                        return;
                    }
                    dojo.attr(img, { src : "data:" + value.type + ";base64," + value.rawdata, alt: value.name });
                }, true);
                
                this.pane = new ContentPane({ content: img });
				
			},
			
            placeAt: function( container ) {
            	this.pane.placeAt( container );
            },
            
            destroy: function() {
            	//this.pane.destroy();
            }
		});
	}
);