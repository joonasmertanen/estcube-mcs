define([
	"dojo/_base/declare",
	"system/Controller", 
	"./../view/WebCamView",
], 
function( 
	declare,
	Controller,
	WebCamView
) {
	var s = declare( [Controller], {
		
		index: function( params ) {	
			this.view = new WebCamView();
			this.placeWidget( this.view );			
		},
		
		destroy: function( params ) {
			this.view.destroy();
		}
		
	});
	return new s();
})
