define([
	"config/config",
	"./WebCamProxy",
	"dojo/store/Memory",
    "dojo/store/Observable",
	],
	function(config, Proxy, Memory, Observable ){
		
		var imageId = config.ES5EC.imageId;
		var channel = config.ES5EC.webcamChannel;	
		
		var store = new Observable(new Memory({ idProperty: "storeId" }));
		
		dojo.subscribe(channel, function( message ) {
			message.storeId = imageId;
			store.put(message);
		});
		dojo.publish(config.TOPIC_CHANNEL_REQUEST, [{ channel: channel, source: null }]);
		
		return store;
	}
);