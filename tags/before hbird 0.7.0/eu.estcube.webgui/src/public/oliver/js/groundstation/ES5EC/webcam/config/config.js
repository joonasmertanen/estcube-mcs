define(["dojo/dom", "dojo/domReady!"], function( dom ) {
	return {
		routes: {
			"ES5EC_webcam": {
				"path": "webcam/",
				"defaults": {
					"controller": "ES5ECWebCam/controller/WebCamController",
					"method": "index",
				}
			},
		},
		ES5EC: {
			webcamChannel: "/hbird.out.binary", 
			imageId: "GroundStation/ES5EC/webcam",
		}
	};
});
