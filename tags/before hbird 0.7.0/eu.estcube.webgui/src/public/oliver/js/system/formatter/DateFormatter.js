define(["config/config","dojo/date/locale"], function( config, locale ) {
	return function( timestamp ) {	
	 	return locale.format(new Date(timestamp), {
			selector: "date",
			datePattern: config.DEFAULT_DATE_FORMAT,
		});
	}
})