package eu.estcube.gs.hamlib;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HamlibIO {
    private static final Logger LOG = LoggerFactory.getLogger(HamlibIO.class);

    public static String getDeviceDriverUrl(DeviceType deviceType, HamlibDriverConfiguration config) {

        switch (deviceType) {
        case ROTATOR: {
            return getNettyAddress(config);
        }
        case RADIO: {
            return getNettyAddress(config);
        }
        default:
            throw new UnsupportedOperationException("The device of type '" + deviceType == null ? "null" : deviceType
                + "' is not supported.");
        }
    }

    private static String getNettyAddress(HamlibDriverConfiguration config) {
        StringBuilder address = new StringBuilder("netty:tcp://" + config.getAddress());
        LOG.debug("hamlib config.getAd " +config.getAddress());
        
        address.append("?sync=true"); // allow for responses to be forwarded
        address.append("&encoders=#encoderStringToBytes");

        /* the decoders are called in this order, from left to right */
        address
            .append("&decoders=#decoderSplitOnNewline,#decoderBytesToString,#decoderBufferer,#decoderStringLogger,#decoderStringToTelemetryObject");
        return address.toString();
    }
}
