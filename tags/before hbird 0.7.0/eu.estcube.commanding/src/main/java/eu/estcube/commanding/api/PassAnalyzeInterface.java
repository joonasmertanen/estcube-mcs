package eu.estcube.commanding.api;

import java.util.List;

import eu.estcube.commanding.analysis.OverPassType;

public interface PassAnalyzeInterface {
	public OverPassType choosePassCase();

}
