package eu.estcube.commanding.calculation;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.analysis.PassAnalyze;
import eu.estcube.commanding.optimizators.Optimization;
import eu.estcube.commanding.rotator1.Rotator1;

public class GenerateTestData {
	public static List<Double> azimuthList = new ArrayList<Double>();
	public static List<Double> elevationList = new ArrayList<Double>();
	public static double antennaReceivingSector;
	public static double maxAzimuth;
	public static double maxElevation;

	public static void main(String[] args) throws IOException {
		antennaReceivingSector = 7.5;
		chooseCase("3", 360, 180);

		PassAnalyze analyze = new PassAnalyze(azimuthList, elevationList);
		Rotator1 rotator = new Rotator1(antennaReceivingSector);
		System.out.println(azimuthList);
		System.out.println(elevationList);
		process(analyze, rotator);
	}

	public static void process(PassAnalyze analyze, Rotator1 rotator) {
		Map<List<Double>, List<Double>> coordinates = new HashMap<List<Double>, List<Double>>();
		OverPassType type = analyze.choosePassCase();
		Optimization optimizationCase = rotator.getOptimizer(type);
		coordinates = optimizationCase.startOptimization(
				analyze.getAzimuthList(), analyze.getElevationList());

	}

	public static void chooseCase(String caseN, double maxAz, double maxEl)
			throws IOException {
		try {
			maxAzimuth = maxAz;
			maxElevation = maxEl;
			String fileName = new String("src/main/resources/cases/case "
					+ caseN + ".txt");
			FileInputStream fstream = new FileInputStream(fileName);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			int counter = 1;
			String azimuth;
			String elevation;
			String space = new String(" ");
			while ((strLine = br.readLine()) != null) {

				counter++;
				if ((counter > 9) && (counter < 29)) {
					azimuth = strLine.substring(21, 27);
					elevation = strLine.substring(28, 34);

					if (azimuth.charAt(0) == space.charAt(0)) {
						azimuth = azimuth.substring(1, azimuth.length() - 1);

					}
					if (azimuth.charAt(0) == space.charAt(0)) {
						azimuth = azimuth.substring(1, azimuth.length() - 1);

					}
					azimuthList.add(Double.parseDouble(azimuth));
					elevationList.add(Double.parseDouble(elevation));
				}
			}
			in.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

	}
}
