package eu.estcube.commanding.rotator1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import eu.estcube.commanding.api.OptimizeInterface;
import eu.estcube.commanding.optimizators.GlobalFunctions;

public class Rotator1CrossElHigh implements OptimizeInterface {
	double receivingSector;
	double maxAz;
	double maxEl;
	List<Double> azimuthList;
	List<Double> elevationList;

	public Rotator1CrossElHigh(double receivingSector, double maxAz,
			double maxEl) {
		super();
		this.receivingSector = receivingSector;
		this.maxAz = maxAz;
		this.maxEl = maxEl;
	}

	public Map<List<Double>, List<Double>> startOptimization(
			List<Double> azimuthList, List<Double> elevationList) {
		Map<List<Double>, List<Double>> coordinates = new HashMap<List<Double>, List<Double>>();
		coordinates = crossElHigh(azimuthList, elevationList, coordinates);
		return coordinates;
	}

	public Map<List<Double>, List<Double>> crossElHigh(
			List<Double> azimuthList, List<Double> elevationList,
			Map<List<Double>, List<Double>> coordinates) {
		double azLastValue = GlobalFunctions.changeAzimuthTo180(azimuthList
				.get(0).intValue());
		double elLastValue = elevationList.get(0).intValue();
		final int azLastPointSector = GlobalFunctions
				.getDegreeSector(azimuthList.get(azimuthList.size() - 1).intValue());
		final int azFirstPointSector = GlobalFunctions
				.getDegreeSector(azimuthList.get(0).intValue());
		List<Double> azimuthTempList = new ArrayList<Double>();
		List<Double> elevationTempList = new ArrayList<Double>();
		for (int i = 0; i < azimuthList.size(); i++) {
			double currentValueSector = GlobalFunctions
					.getDegreeSector(azimuthList.get(i).intValue());
			double azCurrentValue = azimuthList.get(i).intValue();;
			double elCurrentValue = elevationList.get(i).intValue();;
			
			if (elLastValue <= elCurrentValue) {
				elCurrentValue = 180 - elCurrentValue;
			}


			if (currentValueSector == azFirstPointSector) {
				azCurrentValue = GlobalFunctions
						.changeAzimuthTo180(azCurrentValue);
				if (Math.abs(azCurrentValue - azLastValue) > 5) {
					azCurrentValue = azLastValue;
				}
			}

			if ((currentValueSector != azFirstPointSector)
					&& (currentValueSector != azLastPointSector)) {
				azCurrentValue = azimuthTempList.get(i - 1).intValue();
			}

			elevationTempList.add(elCurrentValue);
			azimuthTempList.add(azCurrentValue);
			azLastValue = azimuthTempList.get(i);
			elLastValue = elevationList.get(i);

		}
		azimuthList = azimuthTempList;
		elevationList = elevationTempList;
		coordinates.put(azimuthList, elevationList);
		System.out.println(azimuthList);
		System.out.println(elevationList);
		return coordinates;

	}

}