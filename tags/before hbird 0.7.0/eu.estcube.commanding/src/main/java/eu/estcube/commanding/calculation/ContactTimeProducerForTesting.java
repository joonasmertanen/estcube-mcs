package eu.estcube.commanding.calculation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.JMSConstants;

class ContactTimeProducerForTesting {

	private static final Logger LOG = LoggerFactory.getLogger(AMQsend.class);
	private static String amqUrl = "tcp://localhost:61616"; 
	public static String TOPIC = JMSConstants.AMQ_COMMANDS_RECEIVE_TIME;
	public static String HEADER = "lunchTime";
	public static String HEADER_START = JMSConstants.CMDG_HEADER_START_TIME;
	public static String HEADER_END = JMSConstants.CMDG_HEADER_END_TIME;

	public static List<Long> contactTimeList = new ArrayList<Long>();



	public static void main(String[] args) throws Exception {

		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
				amqUrl);
		Connection connection = connectionFactory.createConnection();
		connection.start();
		Session session = connection.createSession(false,
				Session.AUTO_ACKNOWLEDGE);
		Destination destination = session.createQueue(TOPIC);
		MessageProducer producer = session.createProducer(destination);
			Date date = new Date();
			long start=date.getTime();
			long end=start + 900000; //current time + 1 minute

			ObjectMessage message = session.createObjectMessage(start);
			message.setStringProperty(HEADER,HEADER_START);
			producer.send(message);
			
			message = session.createObjectMessage(end);
			message.setStringProperty(HEADER,HEADER_END);
			producer.send(message);
			
			LOG.info("Message list sent to Calculator");
			contactTimeList.clear();

		}
		

	}

	