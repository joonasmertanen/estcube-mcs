package eu.estcube.commanding.calculation;

import java.util.ArrayList;
import java.util.List;

import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.log4j.Logger;

import eu.estcube.domain.JMSConstants;

public class ContactTimeConsumer {
	static Logger log = Logger.getLogger(Calculation.class.getName());
	private static String url = "tcp://localhost:61616";
	public static String TOPIC = JMSConstants.AMQ_COMMANDS_CONSUME_TIME;
	public static long start;
	public static long end;
	public static List<Long> contactTimeList = new ArrayList<Long>();

	public static void main(String[] args) throws Exception {
		while(true){
		CamelContext context = new DefaultCamelContext();
		ConnectionFactory connectionFactory =new ActiveMQConnectionFactory(url);
		context.addComponent("activemq", JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
		ConsumerTemplate consumerTemplate = context.createConsumerTemplate();
		context.start();
		Long contactTime = consumerTemplate.receiveBody(TOPIC, Long.class);
		contactTimeList.add(contactTime);
		log.info("Received time for Calculations with value "+contactTime);
		if(contactTimeList.size()==2){
		sendToCalculator(contactTimeList.get(0),contactTimeList.get(1));
		contactTimeList.clear();
		}
		}
	
}
	public static void sendToCalculator(long start,long end) throws Exception{
		Calculation.calculate(start, end);
		
	}
	
}