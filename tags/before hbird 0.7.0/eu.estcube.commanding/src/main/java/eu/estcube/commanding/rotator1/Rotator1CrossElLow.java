package eu.estcube.commanding.rotator1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.estcube.commanding.api.OptimizeInterface;
import eu.estcube.commanding.optimizators.GlobalFunctions;

public class Rotator1CrossElLow implements OptimizeInterface {
	double receivingSector;
	double maxAz;
	double maxEl;

	final class twoDoubleLists {
		List<Double> azimuthList;
		List<Double> elevationList;

		public twoDoubleLists(List<Double> azimuthList,
				List<Double> elevationList) {
			this.azimuthList = azimuthList;
			this.elevationList = elevationList;
		}

		public List<Double> getAzimuth() {
			return azimuthList;
		}

		public List<Double> getElevation() {
			return elevationList;
		}
	}

	public Rotator1CrossElLow(double receivingSector, double maxAz, double maxEl) {
		super();
		this.receivingSector = receivingSector;
		this.maxAz = maxAz;
		this.maxEl = maxEl;
	}

	public Map<List<Double>, List<Double>> startOptimization(
			List<Double> azimuthList, List<Double> elevationList) {
		Map<List<Double>, List<Double>> coordinates = new HashMap<List<Double>, List<Double>>();
		coordinates = crossElLowStartEndMiddle(azimuthList, elevationList,
				coordinates);
		return coordinates;
	}

	public Map<List<Double>, List<Double>> crossElLow(List<Double> azimuthList,
			List<Double> elevationList,
			Map<List<Double>, List<Double>> coordinates) {
		double azFirstValue = azimuthList.get(0);
		azimuthList.set(0, GlobalFunctions.changeAzimuthTo180(azFirstValue));
		elevationList.set(0, 180 - elevationList.get(0));
		for (int i = 1; i < azimuthList.size(); i++) {
			double azCurrentValue = azimuthList.get(i).intValue();
			azCurrentValue = GlobalFunctions.changeAzimuthTo180(azCurrentValue);
			double newValue = azCurrentValue;
			azFirstValue = azimuthList.get(i).intValue();
			azimuthList.set(i, newValue);
			double elevation = 180 - elevationList.get(i).intValue();
			elevationList.set(i, elevation);
		}
		coordinates.put(azimuthList, elevationList);
		return coordinates;

	}

	public Map<List<Double>, List<Double>> crossElLowStartEndMiddle(
			List<Double> azimuthList, List<Double> elevationList,
			Map<List<Double>, List<Double>> coordinates) {
		double lastValue = azimuthList.get(0);
		double startAzimuth = lastValue;
		double endAzimuth = azimuthList.get(azimuthList.size() - 1);
		int crossPoint = 0;
		crossPoint = getCrossPoint(lastValue, crossPoint, azimuthList);
		int startEndMiddle = starEndMiddleCheck(crossPoint, startAzimuth,
				endAzimuth, receivingSector, azimuthList);
		String status = "";
		twoDoubleLists result;
		switch (startEndMiddle) {
		case 1: // START
			result = deleteStartEndValues(1, crossPoint, azimuthList,
					elevationList);
			azimuthList = result.getAzimuth();
			elevationList = result.getElevation();
			status = "contact start";
			break;
		case 2:// END
			result = deleteStartEndValues(2, crossPoint, azimuthList,
					elevationList);
			azimuthList = result.getAzimuth();
			elevationList = result.getElevation();
			status = "contact end";
			break;
		case 3:// MIDDLE
			coordinates = crossElLow(azimuthList, elevationList, coordinates);
			status = "middle";
			break;
		}
		System.out.println(azimuthList);
		System.out.println(elevationList);
		return coordinates;

	}

	public static int starEndMiddleCheck(int crossPoint, double azFirstValue,
			double azLastValue, double antennaReceivingSector,
			List<Double> azimuthList) {
		int listCenter = azimuthList.size() / 2;
		double left;
		double right;
		int caseToChoose = 0;

		left = Math.abs(azimuthList.get(crossPoint) - azFirstValue);
		right = Math.abs(azimuthList.get(crossPoint) - azLastValue);

		if (crossPoint < listCenter) {
			if (left > 300) {
				double change = 360 - left;
				if (change < antennaReceivingSector) {
					caseToChoose = 1;
				} else {
					caseToChoose = 3;
				}

			} else {
				if (left < antennaReceivingSector) {
					caseToChoose = 1;
				} else {
					caseToChoose = 3;
				}

			}
		}

		if (crossPoint > listCenter) {
			if (right > 300) {
				double change = 360 - right;
				if (change < antennaReceivingSector) {
					caseToChoose = 2;
				} else {
					caseToChoose = 3;
				}

			} else {
				if (right < antennaReceivingSector) {
					caseToChoose = 2;
				} else {
					caseToChoose = 3;
				}

			}
		}

		return caseToChoose;
	}

	public static int getCrossPoint(double lastValue, int crossPoint,
			List<Double> azimuthList) {
		for (int i = 0; i < azimuthList.size(); i++) {
			if (Math.abs(lastValue - azimuthList.get(i)) > 180) {
				crossPoint = i;
			}
			lastValue = azimuthList.get(i);
		}

		return crossPoint;
	}

	public twoDoubleLists deleteStartEndValues(int cases, int crossPoint,
			List<Double> azimuthList, List<Double> elevationList) {
		switch (cases) {
		case 1:
			for (int i = 0; i < crossPoint; i++) {
				azimuthList.remove(0);
				elevationList.remove(0);
			}
			break;
		case 2:
			for (int i = crossPoint; i <= azimuthList.size(); i++) {
				azimuthList.remove(azimuthList.size() - 1);
				elevationList.remove(azimuthList.size() - 1);

			}
			break;

		}
		return new twoDoubleLists(azimuthList, elevationList);

	}
}