package eu.estcube.commanding.api;

import java.util.List;
import java.util.Map;

public interface OptimizeInterface {
	Map<List<Double>,List<Double>> startOptimization(List<Double> azimuthList,List<Double> elevationList);
}
