package eu.estcube.commanding.calculation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ScheduledMessage;
import org.hbird.exchange.core.NativeCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.domain.JMSConstants;

class AMQsend {
	public static List<Double> azimuthList = new ArrayList<Double>();
	public static List<Double> elevationList = new ArrayList<Double>();
	/** Logging */
	private static final Logger LOG = LoggerFactory.getLogger(AMQsend.class);
	
	/** AMQ server address*/
	private static String amqUrl = "tcp://localhost:61616"; 
	
	/** TOPIC name where commands will be received*/
	public static String TOPIC = JMSConstants.AMQ_GS_COMMANDS;
	
	/** Message header */
	public static String HEADER = JMSConstants.HEADER_COMPONENT_ID;
	
	/** Header for radio messages */
	public static String HEADER_RADIO = JMSConstants.CMDG_HEADER_RADIO;
	
	/** Header for rotator messages */
	public static String HEADER_ROTATOR = JMSConstants.CMDG_HEADER_ROTATOR;
	
	/** Whether it is upling or downlin */
	public static boolean linkInit;
	
	/** List, where commands are stored */
	public static List<NativeCommand> commandsList = new ArrayList<NativeCommand>();
	
	/** Azimuth parameter for command */
	double azimuth;
	
	/** Elevation parameter for command */
	double elevation;
	
	/** Elevation parameter for command */
	static int messageCounter=0;
	

	/**
	 * Sending commands list to ActiveMQ
	 * @param commandsList
	 * @throws Exception
	 */

	public static void main(List<NativeCommand> commandsList) throws Exception {

		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
				amqUrl);
		Connection connection = connectionFactory.createConnection();
		connection.start();
		Session session = connection.createSession(false,
				Session.AUTO_ACKNOWLEDGE);
		Destination destination = session.createQueue(TOPIC);
		MessageProducer producer = session.createProducer(destination);
		for (int i = 0; i < commandsList.size(); i++) {
			Date date = new Date();
			long release = commandsList.get(i).getTimestamp() - date.getTime();
			ObjectMessage message = session.createObjectMessage(commandsList
					.get(i));
			message.setStringProperty(HEADER, commandsList.get(i)
					.getDestination());
			message.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_DELAY,
					release);
			producer.send(message);
			

		}
		LOG.info("Message list sent to AMQ with size " + commandsList.size());
		commandsList.clear();

	}

	/**
	 * Creating command for Rotator and adding it to List<NativeCommand> commandsList
	 * @param azimuth  - azimuth
	 * @param elevation  -elevation
	 * @param timeStamp - command timestamp
	 * @throws Exception
	 */
	public static void sendToRotator(String azimuth, String elevation,
			long timeStamp) throws Exception {
		String messageCommand = new String("P " + azimuth + " " + elevation);
		addCommand(TOPIC, HEADER_ROTATOR, "command", messageCommand, timeStamp);
		azimuthList.add(Double.parseDouble(azimuth));
		elevationList.add(Double.parseDouble(elevation));
		

	}

	/**
	 * Creating command for Radio and adding it to List<NativeCommand> commandsList
	 * @param doppler1 - radio frequency uplink doppler shift
	 * @param doppler2 - radio frequency downlink doppler shift
	 * @param link - uplink/downlink
	 * @param init  - uplink/downlink
	 * @param timeStamp - timestamp for command
	 * @throws Exception
	 */
	public static void sendToRadio(String doppler1, String doppler2,
			String link, boolean init, long timeStamp) throws Exception {

		if (init == true) {
			linkInit = true;
			addCommand(TOPIC, HEADER_RADIO, "command", "V Main", timeStamp);

		}
		if (init == false) {
			linkInit = false;
			addCommand(TOPIC, HEADER_RADIO, "command", "V Sub", timeStamp);

		}
		if (link.equals("V Main") && linkInit == true) {
			String messageCommand = new String("F " + doppler1);
			addCommand(TOPIC, HEADER_RADIO, "command", messageCommand,
					timeStamp);

		}
		if (link.equals("V Sub") && linkInit == false) {
			String messageCommand = new String("F " + doppler2);
			addCommand(TOPIC, HEADER_RADIO, "command", messageCommand,
					timeStamp);
		}

	}

	/**
	 * Completing List<NativeCommand> commandsList
	 * @param TOPIC - topic, where list elements will be stored
	 * @param destination  - message destination
	 * @param name - Native Command
	 * @param command - command value
	 * @param timeStamp - command timestamp
	 * @throws Exception
	 */
	public static void addCommand(String TOPIC, String destination,
			String name, String command, long timeStamp) throws Exception {
		NativeCommand commandLocal = new NativeCommand("COMMANDING",
				destination, command);
		commandLocal.setTimestamp(timeStamp);
		commandLocal.setDatasetidentifier(TOPIC);
		commandsList.add(commandLocal);
		/*System.out
		.printf("\n %s  /TOPIC %s/ Destination [%s]/ Name %s /MessageValue %s /timeStamp %s  ",messageCounter,
				commandLocal.getDatasetidentifier(),
				commandLocal.getDestination(),commandLocal.getName(),
				commandLocal.getCommandToExecute(),
				commandLocal.getTimestamp());*/
		messageCounter=messageCounter+1;
	}

	public static void endConnection() throws Exception {
		main(commandsList);
		//FlippReactor.main();

	}

}