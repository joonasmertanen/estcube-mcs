package eu.estcube.commanding.analysis;

import java.util.ArrayList;
import java.util.List;

import eu.estcube.commanding.api.PassAnalyzeInterface;

public class PassAnalyze implements PassAnalyzeInterface {
	List<Double> azimuthList = new ArrayList<Double>();
	List<Double> elevationList = new ArrayList<Double>();

	public PassAnalyze(List<Double> azimuthList, List<Double> elevationList) {
		this.azimuthList = azimuthList;
		this.elevationList = elevationList;
	}

	public OverPassType choosePassCase() {
		OverPassType type = OverPassType.NOTHING;
		boolean cross = false;
		boolean high = false;
		cross = checkIfCross(azimuthList, cross);
		high = checkElevationHigh(elevationList, high);
		System.out.println(cross+" "+high);
		if (cross == false && high == false) {
			type = OverPassType.NO_CROSS_ELEVATION_LOW;
		}
		if (cross == true && high == false) {
			type = OverPassType.CROSS_ELEVATION_LOW;

		}
		if (cross == true && high == true) {
			type = OverPassType.CROSS_ELEVATION_HIGH;
		}
		if (cross == false && high == true) {
			type = OverPassType.NO_CROSS_ELEVATION_HIGH;

		}
		System.out.println(type.toString());

		return type;
	}

	public boolean checkIfCross(List<Double> azimuthList, boolean cross) {
		double lastValue = azimuthList.get(0);
		for (int i = 0; i < azimuthList.size(); i++) {
			if (Math.abs(lastValue - azimuthList.get(i)) > 180) {
				cross = true;
			}
			lastValue = azimuthList.get(i);
		}
		return cross;
	}

	public boolean checkElevationHigh(List<Double> elList, boolean high) {
		for (int i = 0; i < elList.size(); i++) {
			if ((elList.get(i) > 75) && (elList.get(i) < 90)) {
				high = true;
			}
		}
		return high;

	}

	public List<Double> getAzimuthList() {
		return azimuthList;
	}

	public List<Double> getElevationList() {
		return elevationList;
	}

}
