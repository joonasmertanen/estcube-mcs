package eu.estcube.commanding.calculation;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

import org.apache.commons.math.geometry.Vector3D;
import org.apache.log4j.Logger;
import org.orekit.bodies.BodyShape;
import org.orekit.bodies.GeodeticPoint;
import org.orekit.bodies.OneAxisEllipsoid;
import org.orekit.errors.OrekitException;
import org.orekit.frames.Frame;
import org.orekit.frames.FramesFactory;
import org.orekit.frames.LocalOrbitalFrame;
import org.orekit.frames.LocalOrbitalFrame.LOFType;
import org.orekit.frames.TopocentricFrame;
import org.orekit.orbits.CartesianOrbit;
import org.orekit.orbits.Orbit;
import org.orekit.propagation.Propagator;
import org.orekit.propagation.analytical.KeplerianPropagator;
import org.orekit.time.AbsoluteDate;
import org.orekit.time.TimeScalesFactory;
import org.orekit.tle.TLE;
import org.orekit.tle.TLEPropagator;
import org.orekit.utils.PVCoordinates;

import scala.actors.threadpool.TimeUnit;
import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.analysis.PassAnalyze;
import eu.estcube.commanding.optimizators.GlobalFunctions;
import eu.estcube.commanding.optimizators.GlobalFunctions.ThreeLists;
import eu.estcube.commanding.optimizators.GlobalFunctions.TwoLists;
import eu.estcube.commanding.optimizators.Optimization;
import eu.estcube.commanding.rotator1.Rotator1;

/**
 * @author Ivar Mahhonin This class makes necessary calculations for antenna
 *         rotator so it could follow satellite position and radio
 * 
 */
public class Calculation {
	static Date now = new Date();
	/** Equatorial radius of Earth */
	public static final double EARTH_RADIUS = 6378137.0;

	/** Flattening */
	public static final double FLATENNING = 1.0 / 298.257223563;

	/** Longitude of the ground station */
	public static final double GS_LONGITUDE = Math.toRadians(26.7330);

	/** Latitude of the ground station */
	public static final double GS_LATITUDE = Math.toRadians(58.3000);

	/** Altitude of the ground station */
	public static final double GS_ALTITUDE = 59.0;

	/** A central attraction coefficient */
	public static final double MU = 3.986004415e+14;

	/** Speed of light */
	public static final double SPD_OF_LIGHT = 299792458.0;

	/** Azimuth angle between ground station and the satellite */
	public static double azimuth = 0;

	/** Satellite elevation */
	public static double elevation = 0;

	/** Boolean variable for waiting of contact with satellite */
	public static boolean waiting = true;

	/** Reading input data from keyboard */
	public static Scanner keyboard = new Scanner(System.in);

	/** Radio uplink frequency */
	public static double uplink_fr;

	/** Radio downling frequenct */
	public static double downlink_fr;

	/** TLE data for satelite */
	public static TLE tle;

	/** For counting 3 seconds and generating new command for radio */
	public static int counter = 0;

	/** New value of azimuth after ▲3 degrees */
	public static double firstAzimuth;

	/** New value of elevation after ▲3 degrees */
	public static double firstElevation;

	/** Radio frequency doppler shift */
	public static double doppler;

	/** Shifting timestamps, for future calculations */
	public static long scheduledShift;

	/** Calculation duration period */
	public static long schPeriod;

	/** Contact time start */
	public static Date start;

	/** Contact time end */
	public static Date end;

	static Logger log = Logger.getLogger(Calculation.class.getName());

	/**
	 * Here we are setting contact time period and running endless loop in
	 * waiting for the contact time to start.
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void calculate(long startTime, long endTime) throws Exception {
		System.out.println(now.toString());
		System.setProperty("orekit.data.path",
				"src/main/resources/orekit-data.zip");
		start = new Date(startTime);
		end = new Date(endTime);
		log.info("Received contact time");
		loadProperties();
		log.info("upLink frequency: " + uplink_fr);
		log.info("downlink frequency: " + downlink_fr);
		calculateSchPeriod();

		GeodeticPoint station = new GeodeticPoint(GS_LATITUDE, GS_LONGITUDE,
				GS_ALTITUDE);

		OneAxisEllipsoid oae = new OneAxisEllipsoid(EARTH_RADIUS, FLATENNING,
				FramesFactory.getITRF2005());
		TopocentricFrame es5ec = new TopocentricFrame(oae, station, "ES5EC");

		AbsoluteDate now = new AbsoluteDate(start, TimeScalesFactory.getUTC());

		TLEPropagator propagator = TLEPropagator.selectExtrapolator(tle);

		printOut(now, es5ec, propagator, station);

	}

	/**
	 * Calculating @param azimuth, @param elevation, @param doppler in loop,
	 * while contact time is continues.
	 * 
	 * @param now
	 *            - current date and time in UTC format
	 * @param es5ec
	 *            -TopocentriFrame class object in order to its methods for
	 *            calculating @param azimuth, @param elevation, @param doppler
	 * @param propagator
	 *            - the models used are SGP4 and SDP4, initially proposed by
	 *            NORAD as the unique convenient
	 * @param station
	 *            -3D vector that defines our ground station position
	 * @param hoursStart
	 *            - setting hours of contact start time point
	 * @param hoursEnd
	 *            - setting minutes of contact start time point
	 * @param minStart
	 *            - setting hours of contact end time point
	 * @param minutesEnd
	 *            - setting minutes of contact end time point
	 * @throws Exception
	 */

	static void printOut(AbsoluteDate now, TopocentricFrame es5ec,
			TLEPropagator propagator, GeodeticPoint station) throws Exception {
		Date timeStamp = new Date(start.getTime());
		List<Double> azimuthList = new ArrayList<Double>();
		List<Double> elevationList = new ArrayList<Double>();
		List<Long> timeStampRotator = new ArrayList<Long>();
		List<Long> timeStampRadio = new ArrayList<Long>();
		List<Double> frUp = new ArrayList<Double>();
		List<Double> frDown = new ArrayList<Double>();
		ThreeLists result;
		ThreeLists result2;
		TwoLists result3;

		Vector3D position = propagator.getPVCoordinates(now).getPosition();
		firstAzimuth = calculateAzimuth(es5ec, position, now);
		firstElevation = calculateElevation(es5ec, position, now);

		for (int second = 0; second < schPeriod * 2; second++) {

			position = propagator.getPVCoordinates(now).getPosition();
			Vector3D velocity = propagator.getPVCoordinates(now).getVelocity();
			double az = calculateAzimuth(es5ec, position, now);
			double el = calculateElevation(es5ec, position, now);
			double doppler1 = calculateDoppler(now, position, velocity,
					station, uplink_fr);
			double doppler2 = calculateDoppler(now, position, velocity,
					station, downlink_fr);

			result2 = sendForRadioAMQ(frUp, frDown, doppler1, doppler2, second,
					timeStamp.getTime(), timeStampRadio);
			result = sendForRotatorAMQ(azimuthList, elevationList, az, el,
					second, timeStampRotator, timeStamp.getTime());
			azimuthList = result.getFirstList();
			elevationList = result.getSecondList();
			timeStampRotator = result.getThirdList();
			frUp = result2.getFirstList();
			frDown = result2.getSecondList();
			timeStampRadio = result2.getThirdList();

			counter++;
			timeStamp = increaseTimeStamp(timeStamp, 500);
			now = now.shiftedBy(0.5 * 1D);
		}
		/*result3=GlobalFunctions.listFinalCheck(elevationList, azimuthList);
		azimuthList = result3.getFirstList();
		elevationList = result3.getSecondList();*/
		System.out.println(azimuthList);
		System.out.println(elevationList);
		PassAnalyze analyze = new PassAnalyze(azimuthList, elevationList);
		Rotator1 rotator = new Rotator1(7.5);
		process(analyze, rotator);
		generateCommands(azimuthList, elevationList, timeStampRotator,
				timeStampRadio, frUp, frDown);
		AMQsend.endConnection();

	}

	public static void generateCommands(List<Double> azimuthList,
			List<Double> elevationList, List<Long> timeStampRotator,
			List<Long> timeStampRadio, List<Double> frUp, List<Double> frDown)
			throws Exception {
		for (int i = 0; i < azimuthList.size(); i++) {
			long timeStamp = timeStampRotator.get(i);
			String azim = String.valueOf(azimuthList.get(i));
			int dotPosAzim = azim.indexOf(".");
			String elev = String.valueOf(elevationList.get(i));
			int dotPosElev = elev.indexOf(".");
			AMQsend.sendToRotator(azim.substring(0, dotPosAzim),
					elev.substring(0, dotPosElev), timeStamp);
		}

		for (int i = 0; i < frUp.size(); i++) {
			double freqUpDouble = frUp.get(i);
			double freqDwnDouble = frDown.get(i);
			int freqUp = (int) freqUpDouble;
			int freqDwn = (int) freqDwnDouble;
			AMQsend.sendToRadio(Integer.toString(freqUp),
					Integer.toString(freqDwn), "V Main", true,
					timeStampRadio.get(i));
			AMQsend.sendToRadio(Integer.toString(freqUp),
					Integer.toString(freqDwn), "V Sub", false,
					timeStampRadio.get(i));
		}

		

	}

	public static void process(PassAnalyze analyze, Rotator1 rotator) {
		Map<List<Double>, List<Double>> coordinates = new HashMap<List<Double>, List<Double>>();
		OverPassType type = analyze.choosePassCase();
		Optimization optimizationCase = rotator.getOptimizer(type);
		coordinates = optimizationCase.startOptimization(
				analyze.getAzimuthList(), analyze.getElevationList());

	}

	/**
	 * Calculating doppler for the satellite
	 * 
	 * @param now
	 *            - current date and time in UTC format
	 * @param position
	 *            -satellite position
	 * @param velocity
	 *            - satellite velocity
	 * @param station
	 *            - 3D vector that defines our ground station position
	 * @return
	 * @throws OrekitException
	 */
	public static double calculateDoppler(AbsoluteDate now, Vector3D position,
			Vector3D velocity, GeodeticPoint station, double frequency)
			throws OrekitException {
		now = now.shiftedBy(0.5 * 1D);
		Frame inertialFrame = FramesFactory.getCIRF2000();
		PVCoordinates pvCoordinates = new PVCoordinates(position, velocity);
		Orbit initialOrbit = new CartesianOrbit(pvCoordinates, inertialFrame,
				now, MU);
		Propagator kepler = new KeplerianPropagator(initialOrbit);
		LocalOrbitalFrame lof = new LocalOrbitalFrame(inertialFrame,
				LOFType.QSW, kepler, "QSW");
		Frame ITRF2005 = FramesFactory.getITRF2005();
		BodyShape earth = new OneAxisEllipsoid(EARTH_RADIUS, FLATENNING,
				ITRF2005);
		TopocentricFrame staF = new TopocentricFrame(earth, station, "station");
		PVCoordinates origin = PVCoordinates.ZERO;
		PVCoordinates pv = staF.getTransformTo(lof, now)
				.transformPVCoordinates(origin);
		double dp = Vector3D.dotProduct(pv.getPosition(), pv.getVelocity())
				/ pv.getPosition().getNorm();
		doppler = ((1 - (dp / SPD_OF_LIGHT)) * frequency) - frequency;
		return doppler;
	}

	/**
	 * Generating commands for Rotator. When azimuth or elevation values are
	 * changing to 3 degrees, then we are generating new command for rotator,
	 * where refreshes only parameter with changed value.
	 * 
	 * @param azimuth
	 *            - current azimuth
	 * @param elevation
	 *            - current elevation
	 * @param i
	 *            - generate command at the very first time
	 * @paran tinmeStamp - time, when command must be released from ActivqMQ
	 */

	static ThreeLists sendForRotatorAMQ(List<Double> azimuthList,
			List<Double> elevationList, double azimuth, double elevation,
			int i, List<Long> timeStamps, long timeStamp) throws Exception {
		if ((Math.abs(Math.abs(firstAzimuth) - Math.abs(azimuth)) >= 3)
				|| i == 1) {
			azimuthList.add(azimuth);
			elevationList.add(elevation);
			System.out.println(azimuth+","+elevation);
			timeStamps.add(timeStamp);
			firstAzimuth = azimuth;
		}

		if ((Math.abs(Math.abs(firstElevation) - Math.abs(elevation)) >= 3)) {
			azimuthList.add(azimuth);
			elevationList.add(elevation);
			timeStamps.add(timeStamp);
			firstElevation = elevation;
		}
		return new ThreeLists(azimuthList, elevationList, timeStamps);

	}

	/**
	 * Generating command for Radio Calculating commands for Radio (for each
	 * band).
	 * 
	 * @param doppler1
	 *            - radio frequency uplink doppler shift
	 * @param doppler2
	 *            - radio frequency downlink doppler shift
	 * @param i
	 *            - generating command at the very first time
	 * @param timeStamp
	 *            - time, when command must be released from ActivqMQ
	 * @throws Exception
	 */

	static ThreeLists sendForRadioAMQ(List<Double> frUp, List<Double> frDown,
			double doppler1, double doppler2, int i, long timeStamp,
			List<Long> timeStamps) throws Exception {
		if (counter == 6 || i == 1) {
			counter = 0;
			double freqUpDouble = uplink_fr + doppler1;
			double freqDwnDouble = downlink_fr + doppler2;
			frUp.add(freqUpDouble);
			frDown.add(freqDwnDouble);
			timeStamps.add(timeStamp);
		}
		return new ThreeLists(frUp, frDown, timeStamps);

	}

	/**
	 * 
	 * @param es5ec
	 *            - TopocentriFrame class object in order to its methods for
	 *            calculating @param azimuth, @param elevation, @param doppler
	 * @param position
	 *            - satellite position
	 * @param now
	 *            - current date and time in UTC format
	 * @throws OrekitException
	 */
	public static double calculateAzimuth(TopocentricFrame es5ec,
			Vector3D position, AbsoluteDate now) throws OrekitException {
		now = now.shiftedBy(0.5 * 1D);
		double azimuthRad = es5ec.getAzimuth(position,
				FramesFactory.getCIRF2000(), now);
		Calculation.azimuth = Math.toDegrees(azimuthRad);
		return azimuth;
	}

	/**
	 * 
	 * @param es5ec
	 *            - TopocentriFrame class object in order to its methods for
	 *            calculating @param azimuth, @param elevation, @param doppler
	 * @param position
	 *            - satellite position
	 * @param now
	 *            - current date and time in UTC format
	 * @throws OrekitException
	 */
	public static double calculateElevation(TopocentricFrame es5ec,
			Vector3D position, AbsoluteDate now) throws OrekitException {
		now = now.shiftedBy(0.5 * 1D);
		double elevationRad = es5ec.getElevation(position,
				FramesFactory.getCIRF2000(), now);
		Calculation.elevation = Math.toDegrees(elevationRad);
		return elevation;
	}

	/**
	 * Loading TLE and frequencies from properties file
	 * 
	 * @throws IOException
	 * @throws OrekitException
	 */

	static void loadProperties() throws IOException, OrekitException {
		InputStream inputStream = Calculation.class.getClassLoader()
				.getResourceAsStream("driver.properties");
		Properties properties = new Properties();
		properties.load(inputStream);
		tle = new TLE(properties.getProperty("TLE1"),
				properties.getProperty("TLE2"));
		uplink_fr = new Double(properties.getProperty("uplink_fr"));
		downlink_fr = new Double(properties.getProperty("downlink_fr"));

	}

	/**
	 * 
	 * @param timeStamp
	 *            - getting timestamp for each command
	 * @param stepp
	 *            - timestamp stepp
	 * @return
	 */

	static Date increaseTimeStamp(Date timeStamp, int stepp) {
		Calendar c = Calendar.getInstance();
		c.setTime(timeStamp);

		c.add(Calendar.MILLISECOND, stepp);
		timeStamp = c.getTime();
		return timeStamp;
	}

	/**
	 * Calculating duration of a calculation period
	 * 
	 * @return
	 */

	static long calculateSchPeriod() {
		long startMilliSec = start.getTime();
		long endMilliSec = end.getTime();
		schPeriod = (endMilliSec - startMilliSec);
		schPeriod = TimeUnit.MILLISECONDS.toSeconds(schPeriod);
		System.out.println("Period  is " + schPeriod + " seconds");
		return schPeriod;

	}

}