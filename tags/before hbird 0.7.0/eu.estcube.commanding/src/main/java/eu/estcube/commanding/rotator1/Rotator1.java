package eu.estcube.commanding.rotator1;

import eu.estcube.commanding.analysis.OptimizationType;
import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.api.RotatorTypeInterface;
import eu.estcube.commanding.optimizators.Optimization;

public class Rotator1 implements RotatorTypeInterface {
	double receivingSector;
	private final double maxAz = 360;
	private final double maxEl = 180;

	public Rotator1(double receivingSector) {
		super();
		this.receivingSector = receivingSector;
	}

	public Optimization getOptimizer(OverPassType type) {
		OptimizationType optType = OptimizationType.NOTHING;
		switch (type) {
		case CROSS_ELEVATION_HIGH:
			optType = OptimizationType.ROT_1_CROSS_EL_HIGH;
			break;
		case NO_CROSS_ELEVATION_HIGH:
			optType = OptimizationType.ROT_1_NO_CROSS_EL_HIGH;
			break;
		case CROSS_ELEVATION_LOW:
			optType = OptimizationType.ROT_1_CROSS_EL_LOW;
			break;
		case NO_CROSS_ELEVATION_LOW:
			optType = OptimizationType.ROT_1_NO_CROSS_EL_LOW;
			System.out.println(1);
			break;
			
		case NOTHING:
			break;
		}
		System.out.println(optType.toString());
		Optimization optimization = new Optimization(optType, maxAz, maxEl,
				receivingSector);

		return optimization;
	}

	public double getReceivingSector() {
		return receivingSector;
	}

	public double getMaxAz() {
		return maxAz;
	}

	public double getMaxEl() {
		return maxEl;
	}

}