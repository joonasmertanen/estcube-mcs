package eu.estcube.commanding.optimizators;

import java.util.ArrayList;
import java.util.List;

public class GlobalFunctions {

	public static final class ThreeLists {
		List<Double> firstList;
		List<Double> secondList;
		List<Long> thirdList;

		public ThreeLists(List<Double> firstList, List<Double> secondList,
				List<Long> thirdList) {
			super();
			this.firstList = firstList;
			this.secondList = secondList;
			this.thirdList = thirdList;
		}

		public List<Double> getFirstList() {
			return firstList;
		}

		public List<Double> getSecondList() {
			return secondList;
		}

		public List<Long> getThirdList() {
			return thirdList;
		}

	}

	public static final class TwoLists {
		List<Double> firstList;
		List<Double> secondList;

		public TwoLists(List<Double> firstList, List<Double> secondList) {
			super();
			this.firstList = firstList;
			this.secondList = secondList;

		}

		public List<Double> getFirstList() {
			return firstList;
		}

		public List<Double> getSecondList() {
			return secondList;
		}

	}

	public static double changeAzimuthTo180(double azimuthValue) {
		if (azimuthValue > 180) {
			azimuthValue = azimuthValue - 180;
		} else {
			azimuthValue = azimuthValue + 180;
		}
		return azimuthValue;
	}

	public static int getDegreeSector(double azimuthValue) {
		int sector = 0;
		if ((azimuthValue >= 0) && (azimuthValue <= 90)) {
			sector = 1;
		}
		if ((azimuthValue > 90) && (azimuthValue <= 180)) {
			sector = 2;
		}
		if ((azimuthValue > 180) && (azimuthValue <= 270)) {
			sector = 3;
		}
		if ((azimuthValue > 270) && (azimuthValue <= 359.99)) {
			sector = 4;
		}

		return sector;

	}

	public static TwoLists listFinalCheck(List<Double> elevationList,
			List<Double> azimuthList) {
		List<Double> azTemp = new ArrayList<Double>();
		List<Double> elTemp = new ArrayList<Double>();

		for (int i = 0; i < elevationList.size(); i++) {
			if (((elevationList.get(i) > 0) && (elevationList.get(i) < 180))
					&& ((azimuthList.get(i) > 0) && (azimuthList.get(i) < 360))) { 
				azTemp.add(azimuthList.get(i));
				elTemp.add(elevationList.get(i));
			}

		}
		azimuthList = azTemp;
		elevationList = elTemp;
		return new TwoLists(azimuthList, elevationList);

	}

}
