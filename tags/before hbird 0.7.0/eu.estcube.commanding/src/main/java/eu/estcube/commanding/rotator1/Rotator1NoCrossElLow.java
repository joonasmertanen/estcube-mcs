package eu.estcube.commanding.rotator1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.estcube.commanding.api.OptimizeInterface;

public class Rotator1NoCrossElLow implements OptimizeInterface {
	double receivingSector;
	double maxAz;
	double maxEl;

	public Rotator1NoCrossElLow(double receivingSector, double maxAz,
			double maxEl) {
		super();
		this.receivingSector = receivingSector;
		this.maxAz = maxAz;
		this.maxEl = maxEl;

	}

	public Map<List<Double>, List<Double>> startOptimization(
			List<Double> azimuthList, List<Double> elevationList) {
		Map<List<Double>, List<Double>> coordinates = new HashMap<List<Double>, List<Double>>();
		coordinates.put(azimuthList, elevationList);
		return coordinates;
	}
}