package eu.estcube.commanding.analysis;

public enum OverPassType {
	NO_CROSS_ELEVATION_HIGH, NO_CROSS_ELEVATION_LOW, CROSS_ELEVATION_HIGH, CROSS_ELEVATION_LOW,NOTHING

}
