package eu.estcube.commanding.optimizators;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.estcube.commanding.analysis.OptimizationType;
import eu.estcube.commanding.api.OptimizeInterface;
import eu.estcube.commanding.rotator1.Rotator1CrossElHigh;
import eu.estcube.commanding.rotator1.Rotator1CrossElLow;
import eu.estcube.commanding.rotator1.Rotator1NoCrossElHigh;
import eu.estcube.commanding.rotator1.Rotator1NoCrossElLow;

public class Optimization implements OptimizeInterface {
	OptimizationType optType;
	double maxAz;
	double maxEl;
	double receivingSector;

	public Optimization(OptimizationType optType, double maxAz, double maxEl,
			double receivingSector) {
		super();
		this.optType = optType;
		this.maxAz = maxAz;
		this.maxEl = maxEl;
		this.receivingSector = receivingSector;
	}

	public Map<List<Double>, List<Double>> startOptimization(
			List<Double> azimuthList, List<Double> elevationList) {
		Map<List<Double>, List<Double>> coordinates = new HashMap<List<Double>, List<Double>>();
		switch (optType) {
		case ROT_1_CROSS_EL_HIGH:
			Rotator1CrossElHigh caseInit1 = new Rotator1CrossElHigh(
					receivingSector, maxAz, maxEl);
			coordinates = caseInit1.startOptimization(azimuthList,
					elevationList);
			break;
		case ROT_1_CROSS_EL_LOW:
			Rotator1CrossElLow caseInit2 = new Rotator1CrossElLow(
					receivingSector, maxAz, maxEl);
			coordinates = caseInit2.startOptimization(azimuthList,
					elevationList);
			break;
		case ROT_1_NO_CROSS_EL_HIGH:
			Rotator1NoCrossElHigh caseInit3 = new Rotator1NoCrossElHigh(
					receivingSector, maxAz, maxEl);
			coordinates = caseInit3.startOptimization(azimuthList,
					elevationList);
			break;
		case ROT_1_NO_CROSS_EL_LOW:
			Rotator1NoCrossElLow caseInit4 = new Rotator1NoCrossElLow(
					receivingSector, maxAz, maxEl);
			coordinates = caseInit4.startOptimization(azimuthList,
					elevationList);
			break;
		case NOTHING:
			break;
		}
		return coordinates;

	}

	public OptimizationType getOptType() {
		return optType;
	}
	
	

}