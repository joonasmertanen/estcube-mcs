package eu.estcube.commanding.api;

import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.optimizators.Optimization;

public interface RotatorTypeInterface {
Optimization getOptimizer(OverPassType rype);
}
