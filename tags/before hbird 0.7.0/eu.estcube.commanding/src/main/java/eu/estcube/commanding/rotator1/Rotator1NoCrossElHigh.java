package eu.estcube.commanding.rotator1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.estcube.commanding.api.OptimizeInterface;
import eu.estcube.commanding.optimizators.GlobalFunctions;

public class Rotator1NoCrossElHigh implements OptimizeInterface {
	double receivingSector;
	double maxAz;
	double maxEl;
	Map<Double, Double> coordinates;

	public Rotator1NoCrossElHigh(double receivingSector, double maxAz,
			double maxEl) {
		super();
		this.receivingSector = receivingSector;
		this.maxAz = maxAz;
		this.maxEl = maxEl;

	}

	public Map<List<Double>, List<Double>> startOptimization(
			List<Double> azimuthList, List<Double> elevationList) {
		Map<List<Double>, List<Double>> coordinates = new HashMap<List<Double>, List<Double>>();
		coordinates = noCrossElHigh(azimuthList, elevationList, coordinates);
		return coordinates;
	}

	public Map<List<Double>, List<Double>> noCrossElHigh(
			List<Double> azimuthList, List<Double> elevationList,
			Map<List<Double>, List<Double>> coordinates) { // CASE 3
		// double elLastValue = 180 - elevationList.get(0);
		double elLastValue = elevationList.get(0);

		final int azLastPointSector = GlobalFunctions
				.getDegreeSector(azimuthList.get(azimuthList.size() - 1));
		final int azFirstPointSector = GlobalFunctions
				.getDegreeSector(azimuthList.get(0));

		List<Double> azimuthTempList = new ArrayList<Double>();
		List<Double> elevationTempList = new ArrayList<Double>();

		for (int i = 0; i < azimuthList.size(); i++) {
			double currentValueSector = GlobalFunctions
					.getDegreeSector(azimuthList.get(i));
			double azCurrentValue = azimuthList.get(i);
			double elCurrentValue = elevationList.get(i);


			if (currentValueSector == azLastPointSector) {
				if (i < azimuthList.size() - 1) {
					double azNextValue = azimuthList.get(i + 1);
					if (Math.abs(azCurrentValue - azNextValue) >= 5) {
						azCurrentValue = azNextValue;
						azCurrentValue = GlobalFunctions
								.changeAzimuthTo180(azCurrentValue);
					} else {
						azCurrentValue = GlobalFunctions
								.changeAzimuthTo180(azCurrentValue);
					}
				} else {
					azCurrentValue = GlobalFunctions
							.changeAzimuthTo180(azCurrentValue);
				}
			}
			if ((currentValueSector != azFirstPointSector)
					&& (currentValueSector != azLastPointSector)) {
				azCurrentValue = azimuthList.get(i - 1);

			}
			if (elCurrentValue < elLastValue) {
				elCurrentValue = 180 - (elCurrentValue);
			}
			elevationTempList.add(elCurrentValue);
			azimuthTempList.add(azCurrentValue);
			elLastValue = elevationList.get(i);
		}
		azimuthList = azimuthTempList;
		elevationList = elevationTempList;
		System.out.println(azimuthList);
		System.out.println(elevationList);
		coordinates.put(azimuthList, elevationList);
		return coordinates;

	}

}