package eu.estcube.commanding;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.google.common.primitives.Doubles;

import eu.estcube.commanding.analysis.OptimizationType;
import eu.estcube.commanding.optimizators.Optimization;
import eu.estcube.commanding.rotator1.Rotator1;

public class Rotator1NoCrossElHighTest {
	public static List<Double> azimuthListResult;
	public static List<Double> elevationListResult;
	public static List<Double> azimuthListExpected;
	public static List<Double> elevationListExpected;
	public static Map<List<Double>, List<Double>> coordinatesResult = new HashMap<List<Double>, List<Double>>();
	public static Map<List<Double>, List<Double>> coordinatesExpect = new HashMap<List<Double>, List<Double>>();
	public static Rotator1 rotator1 = new Rotator1(7.5);

	@Test
	public void NoCrossElHigh() {
		double[] azArrayResult = { 317.82, 316.51, 311.02, 164.43, 146.75,
				144.55, 143.8 };
		double[] elArrayResult = {58.71, 75.77, 83.72, 64.86, 48.82, 36.39,
				19.41, 13.35, 8.28 };
		double[] azArrayExpected = {317.82, 316.51, 311.02, 326.75, 326.75, 324.55, 323.8};
		double[] elArrayExpected = {58.71, 75.77, 83.72, 115.14, 131.18, 143.61, 160.59 };

		fillLists(elArrayResult, azArrayResult, azArrayExpected,
				elArrayExpected);

		Optimization optimizationCase = new Optimization(
				OptimizationType.ROT_1_NO_CROSS_EL_HIGH, rotator1.getMaxAz(),
				rotator1.getMaxEl(), rotator1.getReceivingSector());

		coordinatesResult = optimizationCase.startOptimization(
				elevationListResult, azimuthListResult);

		assertEquals(coordinatesExpect, coordinatesResult);

	}

	public void fillLists(double[] azArrayResult, double[] elArrayResult,
			double[] azArrayExpected, double[] elArrayExpected) {
		azimuthListResult = Doubles.asList(azArrayResult);
		elevationListResult = Doubles.asList(elArrayResult);
		azimuthListExpected = Doubles.asList(azArrayExpected);
		elevationListExpected = Doubles.asList(elArrayExpected);
		coordinatesExpect.put(azimuthListExpected, elevationListExpected);
	}
}
