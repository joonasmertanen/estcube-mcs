package eu.estcube.commanding;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.google.common.primitives.Doubles;

import eu.estcube.commanding.analysis.OptimizationType;
import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.optimizators.Optimization;
import eu.estcube.commanding.rotator1.Rotator1;

public class Rotator1CrossElLowTest {
	public static List<Double> azimuthListResult;
	public static List<Double> elevationListResult;
	public static List<Double> azimuthListExpected;
	public static List<Double> elevationListExpected;
	public static Map<List<Double>, List<Double>> coordinatesResult = new HashMap<List<Double>, List<Double>>();
	public static Map<List<Double>, List<Double>> coordinatesExpect = new HashMap<List<Double>, List<Double>>();
	public static Rotator1 rotator1 = new Rotator1(7.5);

	@Test
	public void crossElLow() {
		double[] azArrayResult =  { 56.1, 38.7, 24.0, 12.9, 4.0, 358.91, 354.43, 350.99};
		double[] elArrayResult = { 35.5, 33.95, 30.08, 25.28, 20.47, 16.06, 12.13, 8.62 };
		double[] azArrayExpected = {236.1, 218.0, 204.0, 192.0, 184.0, 178.0, 174.0, 170.0};
		double[] elArrayExpected = {144.5, 147.0, 150.0, 155.0, 160.0, 164.0, 168.0, 172.0};
	

		fillLists(elArrayResult, azArrayResult, azArrayExpected,
				elArrayExpected);
	
		Optimization optimizationCase = new Optimization(
				OptimizationType.ROT_1_CROSS_EL_LOW, rotator1.getMaxAz(),
				rotator1.getMaxEl(), rotator1.getReceivingSector());
		
		coordinatesResult = optimizationCase.startOptimization(
				elevationListResult,azimuthListResult);
		
		assertEquals(coordinatesExpect, coordinatesResult);

	}
	public void fillLists(double[] azArrayResult, double[] elArrayResult,double[] azArrayExpected,double[] elArrayExpected) {
		azimuthListResult = Doubles.asList(azArrayResult);
		elevationListResult = Doubles.asList(elArrayResult);
		azimuthListExpected=Doubles.asList(azArrayExpected);
		elevationListExpected=Doubles.asList(elArrayExpected);
		coordinatesExpect.put(azimuthListExpected, elevationListExpected);
	}
}
