package eu.estcube.commanding;

import static org.junit.Assert.assertEquals;
import java.util.List;
import org.junit.Test;
import com.google.common.primitives.Doubles;
import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.analysis.PassAnalyze;

public class PassAnalyzeTest {
	public static List<Double> azimuthList;
	public static List<Double> elevationList;

	@Test
	public void noCrossElevationLowTest() {
		double[] azArray = { 20, 30, 40, 45, 55, 67 };
		double[] elArray = { 10, 20, 30, 40, 50, 60 };
		fillLists(elArray, azArray);
		PassAnalyze analyze = new PassAnalyze(azimuthList, elevationList);
		OverPassType type = analyze.choosePassCase();
		assertEquals(OverPassType.NO_CROSS_ELEVATION_LOW, type);

	}

	@Test
	public void crossElevationLowTest() {
		double[] azArray = { 357, 358, 359, 1, 2, 3 };
		double[] elArray = { 20, 30, 40, 45, 55, 67 };
		fillLists(elArray, azArray);
		PassAnalyze analyze = new PassAnalyze(azimuthList, elevationList);
		OverPassType type = analyze.choosePassCase();
		assertEquals(OverPassType.CROSS_ELEVATION_LOW, type);

	}

	@Test
	public void crossElevationHighTest() {
		double[] azArray = { 357, 358, 359, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		double[] elArray = { 20, 30, 40, 45, 55, 67, 75, 80, 90, 80, 75, 60 };
		fillLists(elArray, azArray);
		PassAnalyze analyze = new PassAnalyze(azimuthList, elevationList);
		OverPassType type = analyze.choosePassCase();
		assertEquals(OverPassType.CROSS_ELEVATION_HIGH, type);
	}

	@Test
	public void noCrossElevationHighTest() {
		double[] azArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
		double[] elArray = { 20, 30, 40, 45, 55, 67, 75, 80, 90, 80, 75, 60 };
		fillLists(elArray, azArray);
		PassAnalyze analyze = new PassAnalyze(azimuthList, elevationList);
		OverPassType type = analyze.choosePassCase();
		assertEquals(OverPassType.NO_CROSS_ELEVATION_HIGH, type);
	}

	public void fillLists(double[] elArray, double[] azArray) {
		azimuthList = Doubles.asList(azArray);
		elevationList = Doubles.asList(elArray);
	}

}
