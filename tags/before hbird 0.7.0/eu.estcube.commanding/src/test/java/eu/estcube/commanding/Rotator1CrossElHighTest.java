package eu.estcube.commanding;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.google.common.primitives.Doubles;

import eu.estcube.commanding.analysis.OptimizationType;
import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.optimizators.Optimization;
import eu.estcube.commanding.rotator1.Rotator1;

public class Rotator1CrossElHighTest {
	public static List<Double> azimuthListResult;
	public static List<Double> elevationListResult;
	public static List<Double> azimuthListExpected;
	public static List<Double> elevationListExpected;
	public static Map<List<Double>, List<Double>> coordinatesResult = new HashMap<List<Double>, List<Double>>();
	public static Map<List<Double>, List<Double>> coordinatesExpect = new HashMap<List<Double>, List<Double>>();
	public static Rotator1 rotator1 = new Rotator1(7.5);

	@Test
	public void crossElhigh() {
		double[] azArrayResult = { 160.81, 160.73, 160.52, 159.96, 158.05, 98.6, 347.04, 344.9, 344.31, 344.1, 344.04 };
		double[] elArrayResult = { 25.04, 33.89, 46.54, 64.87, 87.84, 66.47, 47.6, 34.55, 25.45, 18.77, 13.59 };
		double[] azArrayExpected = {340.0, 340.0, 340.0, 339.0, 338.0, 338.0, 347.0, 344.0, 344.0, 344.0, 344.0};
		double[] elArrayExpected = {155.0, 147.0, 134.0, 116.0, 93.0, 66.0, 47.0, 34.0, 25.0, 18.0, 13.0 };

		fillLists(elArrayResult, azArrayResult, azArrayExpected,
				elArrayExpected);
	
		Optimization optimizationCase = new Optimization(
				OptimizationType.ROT_1_CROSS_EL_HIGH, rotator1.getMaxAz(),
				rotator1.getMaxEl(), rotator1.getReceivingSector());
		
		coordinatesResult = optimizationCase.startOptimization(
				elevationListResult,azimuthListResult);
		
		assertEquals(coordinatesExpect, coordinatesResult);

	}
	public void fillLists(double[] azArrayResult, double[] elArrayResult,double[] azArrayExpected,double[] elArrayExpected) {
		azimuthListResult = Doubles.asList(azArrayResult);
		elevationListResult = Doubles.asList(elArrayResult);
		azimuthListExpected=Doubles.asList(azArrayExpected);
		elevationListExpected=Doubles.asList(elArrayExpected);
		coordinatesExpect.put(azimuthListExpected, elevationListExpected);
	}
}
