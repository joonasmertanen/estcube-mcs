package eu.estcube.commanding;

import static org.junit.Assert.*;

import org.junit.Test;

import eu.estcube.commanding.analysis.OptimizationType;
import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.analysis.PassAnalyze;
import eu.estcube.commanding.optimizators.Optimization;
import eu.estcube.commanding.rotator1.Rotator1;

public class Rotator1Test {
	Rotator1 rotator1 = new Rotator1(7.5);
	
	@Test
	public void crossElevationLowTest() {
		
		OverPassType type = OverPassType.CROSS_ELEVATION_LOW;
		Optimization optimizationCase = rotator1.getOptimizer(type);
		assertEquals(OptimizationType.ROT_1_CROSS_EL_LOW, optimizationCase.getOptType());
	}
	
	@Test
	public void crossElevationHighTest() {
		
		OverPassType type = OverPassType.CROSS_ELEVATION_HIGH;
		Optimization optimizationCase = rotator1.getOptimizer(type);
		assertEquals(OptimizationType.ROT_1_CROSS_EL_HIGH, optimizationCase.getOptType());
	}
	
	@Test
	public void noCrossElevationHighTest() {
		
		OverPassType type = OverPassType.NO_CROSS_ELEVATION_HIGH;
		Optimization optimizationCase = rotator1.getOptimizer(type);
		assertEquals(OptimizationType.ROT_1_NO_CROSS_EL_HIGH, optimizationCase.getOptType());
	}
	
	@Test
	public void noCrossElevationLowTest() {
		
		OverPassType type = OverPassType.NO_CROSS_ELEVATION_LOW;
		Optimization optimizationCase = rotator1.getOptimizer(type);
		assertEquals(OptimizationType.ROT_1_NO_CROSS_EL_LOW, optimizationCase.getOptType());
	}
	

}
