package eu.estcube.commanding;
 
import static org.junit.Assert.assertEquals;
 
import java.io.File;
 
import org.apache.commons.math.geometry.Vector3D;
import org.junit.Before;
import org.junit.Test;
import org.orekit.bodies.GeodeticPoint;
import org.orekit.bodies.OneAxisEllipsoid;
import org.orekit.data.DataProvidersManager;
import org.orekit.data.ZipJarCrawler;
import org.orekit.errors.OrekitException;
import org.orekit.frames.FramesFactory;
import org.orekit.frames.TopocentricFrame;
import org.orekit.time.AbsoluteDate;
import org.orekit.time.TimeScalesFactory;
import org.orekit.tle.TLE;
import org.orekit.tle.TLEPropagator;
 
import eu.estcube.commanding.calculation.Calculation;
 
public class CalculationTest {
        DataProvidersManager DataProvidersManager;
        /**MASAT-1 doppler shift according to to 2013-02-24-14-01-46*/
        public double DOPPLER_EXPECTED=-806;
        /**MASAT-1 elevation  according to to 2013-02-24-14-01-46*/
        public double ELEVATION_EXPECTED=-15.94;
        /**MASAT-1 azimuth according to to 2013-02-24-14-01-46*/
        public double AZIMUTH_EXPECTED=6.88;
        /**MASAT-1 frequency according to to 2013-02-24-14-01-46*/
        public double FREQUENCY=100000000;
        /**2013-02-24-14-01-46*/
        public AbsoluteDate now;
        /** the models used are SGP4 and SDP4, initially proposed by NORAD as the unique convenient*/
        public static TLEPropagator propagator;
        /** position -satellite position*/
        public static Vector3D position;
        /**MASAT-1 TLE data according to 2013-02-24-14-01-46*/
        static String tle1="1 38081U 12006E   13054.10096693  .00039570  33082-5  86766-3 0  8907";
        static String tle2="2 38081  69.4788  93.7652 0662838 286.7841  66.1545 14.34813775 53317";
        /** TLE object from tle string*/
        static TLE tle;
        /**satellite velocity*/
        static Vector3D velocity;
        /**3D vector that defines our ground station position*/
        static GeodeticPoint station;
        /** OneAxisEllipsoid*/
        static OneAxisEllipsoid oae;
        /**TopocentriFrame class object in order to its methods for
         * calculating @param azimuth, @param elevation, @param doppler*/
        static TopocentricFrame es5ec;
 
        /** initializing variables*/
        @Before
        public void calculate() throws Exception {
                System.setProperty("orekit.data.path", "src/main/resources/orekit-data.zip");
                now=new AbsoluteDate(2013,02,24,14,01,46,TimeScalesFactory.getUTC());  
                tle=new TLE(tle1,tle2);
                propagator = TLEPropagator.selectExtrapolator(tle);
                position = propagator.getPVCoordinates(now).getPosition();
                velocity = propagator.getPVCoordinates(now)
                                .getVelocity();
                station = new GeodeticPoint(Calculation.GS_LATITUDE, Calculation.GS_LONGITUDE,
                                Calculation.GS_ALTITUDE);
                oae = new OneAxisEllipsoid(Calculation.EARTH_RADIUS, Calculation.FLATENNING,
                                FramesFactory.getITRF2005());
                es5ec = new TopocentricFrame(oae, station, "ES5EC");
                
                
        }
        
        /**Testing doppler calculation*/
        @Test
        public void testSetDoppler() throws OrekitException {
                assertEquals(DOPPLER_EXPECTED, Calculation.calculateDoppler(now, position, velocity, station, FREQUENCY),5);
        }
        
 
        /**Testing azimuth calculation*/
        @Test
        public void testSetAzimuth() throws OrekitException {
                assertEquals(AZIMUTH_EXPECTED, Calculation.calculateAzimuth(es5ec, position, now),5);
        }
        /**Testing elevation calculation*/
        @Test
        public void testSetElevation() throws OrekitException {
                assertEquals(ELEVATION_EXPECTED, Calculation.calculateElevation(es5ec, position, now),5);
        }
        
        
 
}