package eu.estcube.commanding;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.google.common.primitives.Doubles;

import eu.estcube.commanding.analysis.OptimizationType;
import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.optimizators.Optimization;
import eu.estcube.commanding.rotator1.Rotator1;

public class Rotator1NoCrossElLowTest {
	public static List<Double> azimuthListResult;
	public static List<Double> elevationListResult;
	public static List<Double> azimuthListExpected;
	public static List<Double> elevationListExpected;
	public static Map<List<Double>, List<Double>> coordinatesResult = new HashMap<List<Double>, List<Double>>();
	public static Map<List<Double>, List<Double>> coordinatesExpect = new HashMap<List<Double>, List<Double>>();
	public static Rotator1 rotator1 = new Rotator1(7.5);

	@Test
	public void NoCrossElLow() {
		double[] azArrayResult = { 159.96, 158.05, 158.05, 47.04};
		double[] elArrayResult = { 25.04, 33.89, 46.54, 64.87};
		double[] azArrayExpected = { 159.96, 158.05, 158.05, 47.04};
		double[] elArrayExpected = { 25.04, 33.89, 46.54, 64.87};

		fillLists(elArrayResult, azArrayResult, azArrayExpected,
				elArrayExpected);
	
		Optimization optimizationCase = new Optimization(
				OptimizationType.ROT_1_NO_CROSS_EL_LOW, rotator1.getMaxAz(),
				rotator1.getMaxEl(), rotator1.getReceivingSector());
		
		coordinatesResult = optimizationCase.startOptimization(
				elevationListResult,azimuthListResult);
		
		assertEquals(coordinatesExpect, coordinatesResult);

	}
	public void fillLists(double[] azArrayResult, double[] elArrayResult,double[] azArrayExpected,double[] elArrayExpected) {
		azimuthListResult = Doubles.asList(azArrayResult);
		elevationListResult = Doubles.asList(elArrayResult);
		azimuthListExpected=Doubles.asList(azArrayExpected);
		elevationListExpected=Doubles.asList(elArrayExpected);
		coordinatesExpect.put(azimuthListExpected, elevationListExpected);
	}
}
