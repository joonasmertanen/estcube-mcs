#!/bin/bash
quiet=false

# quiet flag
if [[ $1 = "-q" ]]; then
    quiet=true
    shift
fi

# Check argument
if [[ $# < 1 ]]; then
    printf "Missing service. Usage %s [-q] <service>\n" "$0"
    exit -1
fi

service=$1
shift

# Check service
dir=$(pwd)

if [ ! -f "$dir/$service/bin/run" ]; then
    printf "Service %s not found\n" "$service"
    exit -2
fi

if [[ ! -x "$dir/$service/bin/run" ]]; then
    printf "Service %s not executable\n" "$service"
    exit -3
fi


# Check logs
if [ ! -d "$dir/$service/logs" ]; then
    mkdir $dir/$service/logs
fi

logFile=$dir/$service/logs/std.log

# Check PID
pidFile=$dir/$service/bin/service.pid

pid=$(cat $pidFile 2>/dev/null)
if [[ $? = 0 ]]; then
    ps -p $pid -f | grep $service >/dev/null 2>&1
    if [[ $? = 0 ]]; then
        printf "Service %s already running\n" "$service"
        exit -4
    fi
fi

# start service
cd $dir/$service 
nohup bin/run "$@" >> $logFile 2>&1 &

# save pid
pid=$!
echo $pid > $pidFile

# message
message="$(date -u +'%Y-%m-%d %H:%M:%S %Z') - $service started; PID: $pid"
echo $message >> $logFile

if ! $quiet ; then
    printf "%s\n" "$message"
fi

# done
exit 0
