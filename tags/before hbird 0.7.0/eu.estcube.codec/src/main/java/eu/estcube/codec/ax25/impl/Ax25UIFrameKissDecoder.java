/** 
 *
 */
package eu.estcube.codec.ax25.impl;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

import org.springframework.stereotype.Component;

import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.domain.transport.tnc.TncFrame;

/**
 * Decoder to decode {@link TncFrame} to {@link Ax25UIFrame}.
 */
@Component
public class Ax25UIFrameKissDecoder {

    /**
     * Decodes {@link TncFrame} to {@link Ax25UIFrame}.
     * 
     * Data from {@link TncFrame} is decoded to {@link Ax25UIFrame} as fallows:
     * <ol>
     * <li>destination address - 7 bytes</li>
     * <li>source address - 7 bytes</li>
     * <li>ctrl - 1 byte</li>
     * <li>pid - 1 byte</li>
     * <li>info - all remaining bytes</li>
     * </ol>
     * 
     * If there is less than 16 bytes available {@link Ax25UIFrame#isErrorTooShort()} flag is set to <tt>true</tt>.
     * Supports frames with headers only - info part can be missing.
     * 
     * {@link Ax25UIFrame} <tt>FCS</tt> is not set by this decoder 'cos its not available in data from TNC.
     * 
     * @param tncFrame {@link TncFrame} to decode
     * @return {@link Ax25UIFrame} decoded from {@link TncFrame#getData()}
     */
    public Ax25UIFrame decode(TncFrame tncFrame) {
        Ax25UIFrame result = new Ax25UIFrame();
        byte[] bytes = tncFrame.getData();
        
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        
        try {
            // read dest address
            byte[] destAddr = new byte[Ax25UIFrame.DEST_ADDR_LEN];
            buffer.get(destAddr);
            result.setDestAddr(destAddr);
            
            // read src address
            byte[] srcAddr = new byte[Ax25UIFrame.SRC_ADDR_LEN];
            buffer.get(srcAddr);
            result.setSrcAddr(srcAddr);
            
            // read ctrl
            result.setCtrl(buffer.get());

            // read pid
            result.setPid(buffer.get());
            
            // read info
            byte[] info = new byte[buffer.remaining()];
            buffer.get(info);
            result.setInfo(info);
        }
        catch (BufferUnderflowException bue) {
            result.setErrorTooShort(true);
        }
        return result;
    }
}
