package eu.estcube.templating.template.provider;

import java.io.IOException;

import eu.estcube.templating.template.ContactTemplate;

public class ContactTemplateProvider extends GenericFileTemplateProvider<ContactTemplate> {

    private static final String FILE_NAME = "src/main/resources/contact_template.json";

    public ContactTemplateProvider() throws IOException {
        super(FILE_NAME, ContactTemplate.class);
    }

}
