package eu.estcube.templating.processor;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import junit.framework.Assert;

import org.hbird.exchange.core.Command;
import org.hbird.exchange.core.NativeCommand;
import org.hbird.exchange.navigation.PointingData;
import org.junit.Before;
import org.junit.Test;

import eu.estcube.templating.beans.Contact;
import eu.estcube.templating.placeholders.IntegerPlaceholder;
import eu.estcube.templating.placeholders.LongPlaceholder;
import eu.estcube.templating.placeholders.PlaceholderException;
import eu.estcube.templating.placeholders.PlaceholderMap;
import eu.estcube.templating.placeholders.StringPlaceholder;
import eu.estcube.templating.processor.ContactTemplateProcessor;
import eu.estcube.templating.template.provider.ContactTemplateProvider;

public class ContactTemplateProcessorTest {

    private static final String ISSUED_BY = "tEst0";
    private static final String DEST_PREFIX = "GROUNDSTATION/ES5EC/";

    private List<PointingData> contactDataList;

    private PlaceholderMap values;

    private ContactTemplateProvider templateProvider;

    private ContactTemplateProcessor processor;

    @Before
    public void setUp() throws Exception {
        values = new PlaceholderMap();

        contactDataList = new ArrayList<PointingData>();

        templateProvider = new ContactTemplateProvider();

        processor = new ContactTemplateProcessor();
    }

    @Test
    public void testProcess() throws IOException, PlaceholderException {
        values.put(StringPlaceholder.ISSUED_BY, ISSUED_BY);
        values.put(IntegerPlaceholder.RADIO_FREQUENCY, 100);
        values.put(StringPlaceholder.RADIO_MODE, "FM");
        values.put(IntegerPlaceholder.RADIO_PASSBAND, 0);
        values.put(LongPlaceholder.COMMAND_HEADER_TIMESTAMP, 0L);

        values.put(IntegerPlaceholder.ROTATOR_ID, 1);

        values.put(LongPlaceholder.COMMAND_TAIL_TIMESTAMP, 100L);

        PointingData cd1 = mock(PointingData.class);
        when(cd1.getTimestamp()).thenReturn(45L);
        when(cd1.getAzimuth()).thenReturn(10.5);
        when(cd1.getElevation()).thenReturn(12.34);
        contactDataList.add(cd1);

        PointingData cd2 = mock(PointingData.class);
        when(cd2.getTimestamp()).thenReturn(55L);
        when(cd2.getAzimuth()).thenReturn(20.6);
        when(cd2.getElevation()).thenReturn(43.21);
        contactDataList.add(cd2);

        Contact contact = processor.process(templateProvider, values, contactDataList);
        Iterator<Command> it;
        Command cmd;

        assertThat(contact.getHeaderCommands().size(), is(2));
        it = contact.getHeaderCommands().iterator();
        cmd = it.next();
        Assert.assertTrue(cmd instanceof NativeCommand);
        assertThat(cmd.getIssuedBy(), is(ISSUED_BY));
        assertThat(cmd.getDestination(), is(DEST_PREFIX + "RADIO"));
        assertThat(cmd.getExecutionTime(), is(0L));
        assertThat(((NativeCommand) cmd).getCommandToExecute(), is("F 100"));
        cmd = it.next();
        Assert.assertTrue(cmd instanceof NativeCommand);
        assertThat(cmd.getIssuedBy(), is(ISSUED_BY));
        assertThat(cmd.getDestination(), is(DEST_PREFIX + "RADIO"));
        assertThat(cmd.getExecutionTime(), is(0L));
        assertThat(((NativeCommand) cmd).getCommandToExecute(), is("M FM 0"));

        assertThat(contact.getBodyCommands().size(), is(contactDataList.size()));
        it = contact.getBodyCommands().iterator();
        cmd = it.next();
        Assert.assertTrue(cmd instanceof NativeCommand);
        assertThat(cmd.getIssuedBy(), is(ISSUED_BY));
        assertThat(cmd.getDestination(), is(DEST_PREFIX + "ROTATOR1"));
        assertThat(cmd.getExecutionTime(), is(45L));
        assertThat(((NativeCommand) cmd).getCommandToExecute(), is("P 10.5 12.34"));
        cmd = it.next();
        Assert.assertTrue(cmd instanceof NativeCommand);
        assertThat(cmd.getIssuedBy(), is(ISSUED_BY));
        assertThat(cmd.getDestination(), is(DEST_PREFIX + "ROTATOR1"));
        assertThat(cmd.getExecutionTime(), is(55L));
        assertThat(((NativeCommand) cmd).getCommandToExecute(), is("P 20.6 43.21"));

        assertThat(contact.getTailCommands().size(), is(1));
        it = contact.getTailCommands().iterator();
        cmd = it.next();
        Assert.assertTrue(cmd instanceof NativeCommand);
        assertThat(cmd.getIssuedBy(), is(ISSUED_BY));
        assertThat(cmd.getDestination(), is(DEST_PREFIX + "ROTATOR"));
        assertThat(cmd.getExecutionTime(), is(100L));
        assertThat(((NativeCommand) cmd).getCommandToExecute(), is("K"));
    }

    @Test
    public void testProcess2() throws IOException, PlaceholderException {
        values.put(StringPlaceholder.ISSUED_BY, ISSUED_BY);
        values.put(IntegerPlaceholder.RADIO_FREQUENCY, 200);
        values.put(StringPlaceholder.RADIO_MODE, "AM");
        values.put(IntegerPlaceholder.RADIO_PASSBAND, 20);
        values.put(LongPlaceholder.COMMAND_HEADER_TIMESTAMP, 1L);

        values.put(IntegerPlaceholder.ROTATOR_ID, 2);

        values.put(LongPlaceholder.COMMAND_TAIL_TIMESTAMP, 200L);

        PointingData cd1 = mock(PointingData.class);
        when(cd1.getTimestamp()).thenReturn(45L);
        when(cd1.getAzimuth()).thenReturn(10.5);
        when(cd1.getElevation()).thenReturn(12.34);
        contactDataList.add(cd1);

        PointingData cd2 = mock(PointingData.class);
        when(cd2.getTimestamp()).thenReturn(55L);
        when(cd2.getAzimuth()).thenReturn(20.6);
        when(cd2.getElevation()).thenReturn(43.21);
        contactDataList.add(cd2);

        PointingData cd3 = mock(PointingData.class);
        when(cd3.getTimestamp()).thenReturn(566666L);
        when(cd3.getAzimuth()).thenReturn(30.7);
        when(cd3.getElevation()).thenReturn(23.41);
        contactDataList.add(cd3);

        Contact contact = processor.process(templateProvider, values, contactDataList);
        Iterator<Command> it;
        Command cmd;

        assertThat(contact.getHeaderCommands().size(), is(2));
        it = contact.getHeaderCommands().iterator();
        cmd = it.next();
        Assert.assertTrue(cmd instanceof NativeCommand);
        assertThat(cmd.getIssuedBy(), is(ISSUED_BY));
        assertThat(cmd.getDestination(), is(DEST_PREFIX + "RADIO"));
        assertThat(cmd.getExecutionTime(), is(1L));
        assertThat(((NativeCommand) cmd).getCommandToExecute(), is("F 200"));
        cmd = it.next();
        Assert.assertTrue(cmd instanceof NativeCommand);
        assertThat(cmd.getIssuedBy(), is(ISSUED_BY));
        assertThat(cmd.getDestination(), is(DEST_PREFIX + "RADIO"));
        assertThat(cmd.getExecutionTime(), is(1L));
        assertThat(((NativeCommand) cmd).getCommandToExecute(), is("M AM 20"));

        assertThat(contact.getBodyCommands().size(), is(contactDataList.size()));
        it = contact.getBodyCommands().iterator();
        cmd = it.next();
        Assert.assertTrue(cmd instanceof NativeCommand);
        assertThat(cmd.getIssuedBy(), is(ISSUED_BY));
        assertThat(cmd.getDestination(), is(DEST_PREFIX + "ROTATOR2"));
        assertThat(cmd.getExecutionTime(), is(45L));
        assertThat(((NativeCommand) cmd).getCommandToExecute(), is("P 10.5 12.34"));
        cmd = it.next();
        Assert.assertTrue(cmd instanceof NativeCommand);
        assertThat(cmd.getIssuedBy(), is(ISSUED_BY));
        assertThat(cmd.getDestination(), is(DEST_PREFIX + "ROTATOR2"));
        assertThat(cmd.getExecutionTime(), is(55L));
        assertThat(((NativeCommand) cmd).getCommandToExecute(), is("P 20.6 43.21"));
        cmd = it.next();
        Assert.assertTrue(cmd instanceof NativeCommand);
        assertThat(cmd.getIssuedBy(), is(ISSUED_BY));
        assertThat(cmd.getDestination(), is(DEST_PREFIX + "ROTATOR2"));
        assertThat(cmd.getExecutionTime(), is(566666L));
        assertThat(((NativeCommand) cmd).getCommandToExecute(), is("P 30.7 23.41"));

        assertThat(contact.getTailCommands().size(), is(1));
        it = contact.getTailCommands().iterator();
        cmd = it.next();
        Assert.assertTrue(cmd instanceof NativeCommand);
        assertThat(cmd.getIssuedBy(), is(ISSUED_BY));
        assertThat(cmd.getDestination(), is(DEST_PREFIX + "ROTATOR"));
        assertThat(cmd.getExecutionTime(), is(200L));
        assertThat(((NativeCommand) cmd).getCommandToExecute(), is("K"));
    }

}
