package eu.estcube.contact;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.math.geometry.Vector3D;
import org.junit.Test;
import org.orekit.bodies.GeodeticPoint;
import org.orekit.errors.OrekitException;
import org.orekit.time.AbsoluteDate;
import org.orekit.time.TimeScalesFactory;
import org.orekit.tle.TLE;
import org.orekit.utils.PVCoordinates;

public class ContactCalculationTest {
	
	@Test
	public void testGeoStatsSat() throws OrekitException{
		System.setProperty("orekit.data.path", "src/main/resources/orekit-data.zip");
		
		//Geostationary satellite LES 9 TLE
		TLE tle = new TLE("1 08747U 76023B   13091.27084731 -.00000094  00000-0  00000+0 0  9050",
				"2 08747  12.8267 112.8241 0022435  16.8836  53.2651  1.00276232 81296");
		//start date
        AbsoluteDate startDate = new AbsoluteDate(2013, 4, 2, 6, 16, 00, TimeScalesFactory.getUTC());
        //end date 
        AbsoluteDate endDate = new AbsoluteDate(2013, 4, 3, 13, 35, 00, TimeScalesFactory.getUTC());
     // Geolocation of ground station ES5EC - on roof top of the building in TĆ¤he 4, Tartu, Estonia
        GeodeticPoint gsLocation = new GeodeticPoint(Math.toRadians(58.3000D), Math.toRadians(26.7330D), 59.0D);
		double elevation = 0.1;
		String gsName = "GROUNDSTATION";
		double uplinkFreq = 100;
		double downlinkFreq = 100;
		ContactCalculation calc = new ContactCalculation();
		List<ContactEventData> contactEvents = calc.calculateContact(tle, startDate, endDate, gsLocation, elevation, gsName, uplinkFreq, downlinkFreq);
		//Because it's geostationary satellite no contacts are available
		List<ContactEventData> result = new ArrayList<ContactEventData>();
		Assert.assertEquals(result, contactEvents);
	}
}
