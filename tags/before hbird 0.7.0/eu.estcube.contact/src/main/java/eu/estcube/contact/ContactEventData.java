package eu.estcube.contact;

import org.hbird.exchange.core.Named;
import org.orekit.time.AbsoluteDate;

public class ContactEventData extends Named{
	private static final long serialVersionUID = 1L;
	
	private AbsoluteDate startTime; 
	private AbsoluteDate endTime; 
	// öö jääb peale
	private boolean inSunlight; 
	private Double startAzimuth;
	private Double endAzimuth; 
	//nurgad kraadides
	private Double startElevation; 
	private Double endElevation; 
	private Double maxElevation; 

	private Double startDopplerUp; 
	private Double startDopplerDown;
	private Double endDopplerUp;
	private Double endDopplerDown;
	//GS and Sat name tulevad metadatast
	private String groundStation; 
	private String satellite; 
	//sig. loss algus, lõpp, max elev järgi
	private Double startSigLossUp;
	private Double startSigLossDown;
	private Double maxSigLossUp; // maksimaalne ülelennu jooksul
	private Double maxSigLossDown;
	private Double minSigLossUp;
	private Double minSigLossDown;
	private Double endSigLossUp;
	private Double endSigLossDown;
	
	private Double rangeAtStart;
	private Double minRange;
	private Double maxRange;
	private Double rangeAtEnd;
	
	//signal delay
	private Double startSignalDelay;
	private Double maxSignalDelay;
	private Double minSignalDelay;
	private Double endSignalDelay;
	
	//orbit number
	private int orbitNumber;
	
	
	public ContactEventData(){
	
	}

	public AbsoluteDate getStartTime() {
		return startTime;
	}

	public void setStartTime(AbsoluteDate absoluteDate) {
		this.startTime = absoluteDate;
	}

	public AbsoluteDate getEndTime() {
		return endTime;
	}

	public void setEndTime(AbsoluteDate endTime) {
		this.endTime = endTime;
	}

	public Double getStartAzimuth() {
		return startAzimuth;
	}

	public void setStartAzimuth(Double azimuth) {
		this.startAzimuth = azimuth;
	}

	public Double getStartElevation() {
		return startElevation;
	}

	public void setStartElevation(Double elevation) {
		this.startElevation = elevation;
	}

	public Double getEndElevation() {
		return endElevation;
	}

	public void setEndElevation(Double endElevation) {
		this.endElevation = endElevation;
	}

	public Double getMaxElevation() {
		return maxElevation;
	}

	public void setMaxElevation(Double maxElevation) {
		this.maxElevation = maxElevation;
	}

	public Double getEndAzimuth() {
		return endAzimuth;
	}

	public void setEndAzimuth(Double endAzimuth) {
		this.endAzimuth = endAzimuth;
	}

	public String getGroundStation() {
		return groundStation;
	}

	public void setGroundStation(String groundStation) {
		this.groundStation = groundStation;
	}

	public String getSatellite() {
		return satellite;
	}

	public void setSatellite(String satellite) {
		this.satellite = satellite;
	}

	public boolean isInSunlight() {
		return inSunlight;
	}

	public void setInSunlight(boolean inSunlight) {
		this.inSunlight = inSunlight;
	}

	public Double getStartSigLossUp() {
		return startSigLossUp;
	}

	public void setStartSigLossUp(Double startSigLossUp) {
		this.startSigLossUp = startSigLossUp;
	}

	public Double getStartSigLossDown() {
		return startSigLossDown;
	}

	public void setStartSigLossDown(Double startSigLossDown) {
		this.startSigLossDown = startSigLossDown;
	}

	public Double getMaxSigLossUp() {
		return maxSigLossUp;
	}

	public void setMaxSigLossUp(Double maxSigLossUp) {
		this.maxSigLossUp = maxSigLossUp;
	}

	public Double getMaxSigLossDown() {
		return maxSigLossDown;
	}

	public void setMaxSigLossDown(Double maxSigLossDown) {
		this.maxSigLossDown = maxSigLossDown;
	}

	public Double getEndSigLossUp() {
		return endSigLossUp;
	}

	public void setEndSigLossUp(Double endSigLossUp) {
		this.endSigLossUp = endSigLossUp;
	}

	public Double getEndSigLossDown() {
		return endSigLossDown;
	}

	public void setEndSigLossDown(Double endSigLossDown) {
		this.endSigLossDown = endSigLossDown;
	}

	public Double getStartDopplerUp() {
		return startDopplerUp;
	}

	public void setStartDopplerUp(Double startDopplerUp) {
		this.startDopplerUp = startDopplerUp;
	}

	public Double getStartDopplerDown() {
		return startDopplerDown;
	}

	public void setStartDopplerDown(Double startDopplerDown) {
		this.startDopplerDown = startDopplerDown;
	}

	public Double getEndDopplerUp() {
		return endDopplerUp;
	}

	public void setEndDopplerUp(Double endDopplerUp) {
		this.endDopplerUp = endDopplerUp;
	}

	public Double getEndDopplerDown() {
		return endDopplerDown;
	}

	public void setEndDopplerDown(Double endDopplerDown) {
		this.endDopplerDown = endDopplerDown;
	}

	public Double getRangeAtStart() {
		return rangeAtStart;
	}

	public void setRangeAtStart(Double rangeAtStart) {
		this.rangeAtStart = rangeAtStart;
	}

	public Double getMinSigLossUp() {
		return minSigLossUp;
	}

	public void setMinSigLossUp(Double minSigLossUp) {
		this.minSigLossUp = minSigLossUp;
	}

	public Double getMinSigLossDown() {
		return minSigLossDown;
	}

	public void setMinSigLossDown(Double minSigLossDown) {
		this.minSigLossDown = minSigLossDown;
	}

	public Double getMinRange() {
		return minRange;
	}

	public void setMinRange(Double minRange) {
		this.minRange = minRange;
	}

	public Double getMaxRange() {
		return maxRange;
	}

	public void setMaxRange(Double maxRange) {
		this.maxRange = maxRange;
	}

	public Double getRangeAtEnd() {
		return rangeAtEnd;
	}

	public void setRangeAtEnd(Double rangeAtEnd) {
		this.rangeAtEnd = rangeAtEnd;
	}

	public Double getStartSignalDelay() {
		return startSignalDelay;
	}

	public void setStartSignalDelay(Double startSignalDelay) {
		this.startSignalDelay = startSignalDelay;
	}

	public Double getMaxSignalDelay() {
		return maxSignalDelay;
	}

	public void setMaxSignalDelay(Double maxSignalDelay) {
		this.maxSignalDelay = maxSignalDelay;
	}

	public Double getEndSignalDelay() {
		return endSignalDelay;
	}

	public void setEndSignalDelay(Double endSignalDelay) {
		this.endSignalDelay = endSignalDelay;
	}

	public Double getMinSignalDelay() {
		return minSignalDelay;
	}

	public void setMinSignalDelay(Double minSignalDelay) {
		this.minSignalDelay = minSignalDelay;
	}

	public int getOrbitNumber() {
		return orbitNumber;
	}

	public void setOrbitNumber(int orbitNumber) {
		this.orbitNumber = orbitNumber;
	}

}
