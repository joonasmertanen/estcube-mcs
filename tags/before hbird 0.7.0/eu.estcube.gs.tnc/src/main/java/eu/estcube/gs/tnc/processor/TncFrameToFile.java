package eu.estcube.gs.tnc.processor;

import java.io.File;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.hbird.exchange.constants.StandardArguments;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.estcube.common.ByteUtil;
import eu.estcube.common.Dates;
import eu.estcube.domain.transport.tnc.TncFrame;

/**
 *
 */
@Component
public class TncFrameToFile implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(TncFrameToFile.class);
    
    /** @{inheritDoc}. */
    @Override
    public void process(Exchange exchange) throws Exception {
        LOG.trace("Processing TNC frame to HEX string");
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        try {
            TncFrame frame = in.getBody(TncFrame.class);
            long timestamp = in.getHeader(StandardArguments.TIMESTAMP, Long.class);
            String fileName = createName(frame.getTarget(), Dates.toDateInFileNameFormat(timestamp));
            String hex = ByteUtil.toHexString(frame.getData());
            LOG.trace("To file {}: {}", fileName, hex);
            out.setHeader(Exchange.FILE_NAME, fileName);
            out.setBody(hex);
            LOG.trace(" Successfully completed TNC frame to HEX string processing");
        } catch (Exception e) {
            LOG.error("Failed to process TncFrame to HEX string", e);
            exchange.setException(e);
        }
    }
    
    String createName(int target, String date) {
        StringBuilder sb = new StringBuilder();
        sb.append(target)
          .append(File.separator)
          .append(date)
          .append(".txt");
        return sb.toString();
    }
}
