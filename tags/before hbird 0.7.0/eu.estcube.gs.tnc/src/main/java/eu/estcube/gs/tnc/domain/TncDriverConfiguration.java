package eu.estcube.gs.tnc.domain;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.estcube.domain.config.GroundStationConfigurationBase;

/**
 *
 */
@Component
public class TncDriverConfiguration extends GroundStationConfigurationBase {

    @Value("${serial.in.port}")
    private String serialInPort;
    
    @Value("${serial.in.baud}")
    private String serialInBaud;
    
    @Value("${serial.in.dataBits}")
    private String serialInDataBits;
    
    @Value("${serial.in.stopBits}")
    private String serialInStopBits;
    
    @Value("${serial.in.parity}")
    private String serialInParity;
    
    @Value("${serial.in.flowControl}")
    private String serialInFlowControl;
    
    @Value("${serial.in.filters}")
    private String serialInFilters;
    
    @Value("${serial.out.port}")
    private String serialOutPort;
    
    @Value("${serial.out.baud}")
    private String serialOutBaud;
    
    @Value("${serial.out.dataBits}")
    private String serialOutDataBits;
    
    @Value("${serial.out.stopBits}")
    private String serialOutStopBits;
    
    @Value("${serial.out.parity}")
    private String serialOutParity;
    
    @Value("${serial.out.flowControl}")
    private String serialOutFlowControl;

    @Value("${serial.out.filters}")
    private String serialOutFilters;
    
    @Value("${tnc.use.files}")
    private boolean useFiles;
    
    @Value("${satellite.id}")
    private String satelliteId;
    
    @Value("${contact.id}")
    private String contactId;
    
    public String getSerialInPort() {
        return serialInPort;
    }
    
    public String getSerialOutPort() {
        return serialOutPort;
    }

    public String getSerialInBaud() {
        return serialInBaud;
    }

    public String getSerialInDataBits() {
        return serialInDataBits;
    }

    public String getSerialInStopBits() {
        return serialInStopBits;
    }

    public String getSerialInParity() {
        return serialInParity;
    }

    public String getSerialInFlowControl() {
        return serialInFlowControl;
    }

    public String getSerialOutBaud() {
        return serialOutBaud;
    }

    public String getSerialOutDataBits() {
        return serialOutDataBits;
    }

    public String getSerialOutStopBits() {
        return serialOutStopBits;
    }

    public String getSerialOutParity() {
        return serialOutParity;
    }

    public String getSerialOutFlowControl() {
        return serialOutFlowControl;
    }

    public String getSerialInFilters() {
        return serialInFilters;
    }

    public String getSerialOutFilters() {
        return serialOutFilters;
    }
    
    public boolean useFiles() {
        return useFiles;
    }
    
    public String getSatelliteId() {
        return satelliteId;
    }
    
    public void setSatelliteId(String satelliteId) {
        this.satelliteId = satelliteId;
    }
    
    public String getContactId() {
        return contactId;
    }
    
    public void setContactId(String contactId) {
        this.contactId = contactId;
    }
}
