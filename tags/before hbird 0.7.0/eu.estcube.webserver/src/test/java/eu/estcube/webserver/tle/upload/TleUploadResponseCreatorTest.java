package eu.estcube.webserver.tle.upload;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.inOrder;

import org.hbird.exchange.navigation.TleOrbitalParameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.webserver.domain.UIResponse;
import eu.estcube.webserver.domain.UIResponse.Status;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TleUploadResponseCreatorTest {

    private TleUploadResponseCreator creator;

    @Mock
    private TleOrbitalParameters top;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        creator = new TleUploadResponseCreator();
        inOrder = inOrder(top);
    }

    /**
     * Test method for
     * {@link eu.estcube.webserver.tle.upload.TleUploadResponseCreator#create(org.hbird.exchange.navigation.TleOrbitalParameters)}
     * .
     */
    @Test
    public void testCreate() {
        UIResponse response = creator.create(top);
        assertNotNull(response);
        assertEquals(Status.OK, response.getStatus());
        assertEquals(top, response.getValue());
        inOrder.verifyNoMoreInteractions();
    }
}
