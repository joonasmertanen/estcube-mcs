package eu.estcube.webserver.radiobeacon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;

import org.hbird.exchange.core.Label;
import org.hbird.exchange.core.Named;
import org.hbird.exchange.core.Parameter;
import org.junit.Before;
import org.junit.Test;

import eu.estcube.domain.RadioBeacon;
import eu.estcube.webserver.radiobeacon.parsers.RadioBeaconParametersNormalMode;
import eu.estcube.webserver.radiobeacon.parsers.RadioBeaconParametersSafeMode;

public class RadioBeaconTranslatorTest {

    private RadioBeacon beacon1, beacon2, beacon3, beacon4, beacon5, beacon6, beacon7, beacon8, beacon9, beacon10,
            beacon11, beacon12, beacon13, beacon14;
    private RadioBeaconTranslator radioBeaconTranslator;
    private String correctNormalBeacon = "ES5E/SEAAAAAAABBCCDDEEFFGGGHIIJJKKLLMMNNOOK";
    private String correctSafeBeacon1 = "ES5E/STAAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSSKN";
    private String correctSafeBeacon2 = "ES5E/STAA AAAAABBCCD DEEeeFFGGHHIIJJKKLLMMNNOPQQR RSSKN   ";
    private String incorrectLenghtBeacon = "ES5AAAAAAAPRRRRKN";
    private String incorrectBasisBeacon = "QS5R/STAAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSSKN";
    private String nonASCIISafeBeacon = "ES5E/STAAAAAAABBCCDDEEEEFFGGHHáIJJKKLLMMNNOPQQRRSSKN"; //
    private String correctNormalBeaconMode1 = "ES5E/SEAAAAAAABBCCDDEEFFGGGHIIJJKKLLMMNNOOK";
    private String correctNormalBeaconMode2 = "ES5E/S#EAAAAAAABBCCDDEEFFGGGHIIJJKKLLMMNNOOK";
    private String correctNormalBeaconMode3 = "ES5E/SEAAAAAAABBCCDDEEFFGGGHIIJJKKLLMMNNOO#";
    private String correctNormalBeaconMode4 = "ES5E/S#AAAAAAABBCCDDEEFFGGGHIIJJKKLLMMNNOO#";
    private String correctSafeBeaconMode1 = "ES5E/STAAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSSKN";
    private String correctSafeBeaconMode2 = "ES5E/S#AAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSSKN";
    private String correctSafeBeaconMode3 = "ES5E/STAAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSS##";
    private String correctSafeBeaconMode4 = "ES5E/S#AAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSS##";

    private String sampleMessageNormalMode = "ES5E/S ETASA ZZZZZ ZUABTCFFHTUS5BSNMFNFCAFE6HK";
    private String sampleMessageSafeMode = "ES5E/S T TASAZZZ AT DA TT T6SN ZZ 6B TD 5B AB TC FF UE ZU B U WB NC SW KN";
    private String samplePartialMessageNormalMode = "ES5E/SE###################################K";

    @Before
    public void setUp() throws Exception {
        radioBeaconTranslator = new RadioBeaconTranslator();
        beacon1 = new RadioBeacon();
        beacon2 = new RadioBeacon();
        beacon3 = new RadioBeacon();
        beacon4 = new RadioBeacon();
        beacon5 = new RadioBeacon();
        beacon6 = new RadioBeacon();
        beacon7 = new RadioBeacon();
        beacon8 = new RadioBeacon();
        beacon9 = new RadioBeacon();
        beacon10 = new RadioBeacon();
        beacon11 = new RadioBeacon();
        beacon12 = new RadioBeacon();
        beacon13 = new RadioBeacon();
        beacon14 = new RadioBeacon();

        beacon1.setRadioBeacon(correctNormalBeacon);
        beacon2.setRadioBeacon(correctSafeBeacon1);
        beacon3.setRadioBeacon(incorrectLenghtBeacon);
        beacon4.setRadioBeacon(correctSafeBeacon2);
        beacon5.setRadioBeacon(incorrectBasisBeacon);
        beacon6.setRadioBeacon(correctNormalBeaconMode1);
        beacon7.setRadioBeacon(correctNormalBeaconMode2);
        beacon8.setRadioBeacon(correctNormalBeaconMode3);
        beacon9.setRadioBeacon(correctNormalBeaconMode4);
        beacon10.setRadioBeacon(correctSafeBeaconMode1);
        beacon11.setRadioBeacon(correctSafeBeaconMode2);
        beacon12.setRadioBeacon(correctSafeBeaconMode3);
        beacon13.setRadioBeacon(correctSafeBeaconMode4);
        beacon14.setRadioBeacon(nonASCIISafeBeacon);

    }

    @Test
    // @TODO Remove this method, temporary for testing real data.
    public void temporaryTest() throws Exception {
        // String message =
        // "ES5E/STZ6DH5FUS6USZHTTTTCETTTTTTSBSBUSWUTTTATZTZTTKN";
        //
        // HashMap<String, Named> parameters =
        // radioBeaconTranslator.toParameters(message, null);
        //
        // System.out.println("parameters: ");
        // for (String item : parameters.keySet()) {
        // Named dat = parameters.get(item);
        //
        // if (dat instanceof Parameter) {
        // System.out.println(item + ": " + ((Parameter) dat).getValue());
        //
        // } else if (dat instanceof Label) {
        // System.out.println(item + ": " + ((Label) dat).getValue());
        // }
        //
        // }

    }

    @Test
    public void checkBeaconLengthTest() throws Exception {
        assertTrue(radioBeaconTranslator.checkBeaconLength(beacon1.getRadioBeacon()));
        assertTrue(radioBeaconTranslator.checkBeaconLength(beacon2.getRadioBeacon()));
        assertFalse(radioBeaconTranslator.checkBeaconLength(beacon3.getRadioBeacon()));
    }

    @Test
    public void determineRadioBeaconModeTest() throws Exception {
        assertEquals("Normal mode", radioBeaconTranslator.determineRadioBeaconMode(beacon6.getRadioBeacon()));
        assertEquals("Normal mode", radioBeaconTranslator.determineRadioBeaconMode(beacon7.getRadioBeacon()));
        assertEquals("Normal mode", radioBeaconTranslator.determineRadioBeaconMode(beacon8.getRadioBeacon()));
        assertEquals("Undefined mode", radioBeaconTranslator.determineRadioBeaconMode(beacon9.getRadioBeacon()));
        assertEquals("Safe mode", radioBeaconTranslator.determineRadioBeaconMode(beacon10.getRadioBeacon()));
        assertEquals("Safe mode", radioBeaconTranslator.determineRadioBeaconMode(beacon11.getRadioBeacon()));
        assertEquals("Safe mode", radioBeaconTranslator.determineRadioBeaconMode(beacon12.getRadioBeacon()));
        assertEquals("Undefined mode", radioBeaconTranslator.determineRadioBeaconMode(beacon13.getRadioBeacon()));
    }

    @Test
    public void reformBeaconMessageTest() throws Exception {
        assertEquals(correctSafeBeacon1, radioBeaconTranslator.reformBeaconMessage(beacon4.getRadioBeacon()));
    }

    @Test
    public void checkRadioBeaconBasisTest() throws Exception {
        assertTrue(radioBeaconTranslator.checkRadioBeaconBasis(beacon1.getRadioBeacon()));
        assertTrue(radioBeaconTranslator.checkRadioBeaconBasis(beacon2.getRadioBeacon()));
        assertFalse(radioBeaconTranslator.checkRadioBeaconBasis(beacon5.getRadioBeacon()));
    }

    @Test
    public void checkASCIIMessageTest() throws Exception {
        assertTrue(radioBeaconTranslator.checkASCIIMessage(beacon2.getRadioBeacon()));
        assertFalse(radioBeaconTranslator.checkASCIIMessage(beacon14.getRadioBeacon()));
    }

    @Test
    public void toParametersNormalModeTest() throws Exception {

        assertEquals(0, radioBeaconTranslator.toParameters(incorrectLenghtBeacon, null, "oliver").size());
        assertEquals(0, radioBeaconTranslator.toParameters(incorrectBasisBeacon, null, "oliver").size());
        assertEquals(0, radioBeaconTranslator.toParameters(nonASCIISafeBeacon, null, "oliver").size());

        HashMap<String, Named> parameters = radioBeaconTranslator.toParameters(sampleMessageNormalMode, null, "peeter");
        assertEquals("E",
                ((Label) (parameters.get(RadioBeaconParametersNormalMode.OPERATING_MODE.getName()))).getValue());
        assertEquals("peeter",
                ((Label) (parameters.get(RadioBeaconParametersNormalMode.OPERATING_MODE.getName()))).getIssuedBy());
        assertEquals(Long.parseLong("1352902792"),
                ((Label) (parameters.get(RadioBeaconParametersNormalMode.OPERATING_MODE.getName()))).getTimestamp());
        assertEquals(Long.parseLong("1352902792"),
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.TIMESTAMP.getName()))).getValue());
        assertEquals(2.414189978213456, /* 136 */
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.MAIN_BUS_VOLTAGE.getName()))).getValue());
        assertEquals(-126,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.AVERAGE_POWER_BALANCE.getName())))
                        .getValue());
        assertEquals(3.028209702144293/* 171 */,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.BATTERY_A_VOLTAGE.getName()))).getValue());
        assertEquals(0.22542297503644695/* 12 */,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.BATTERY_B_VOLTAGE.getName()))).getValue());
        assertEquals(120.9334 /* 255 */,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.BATTERY_A_TEMPERATURE.getName())))
                        .getValue());
        assertEquals(360.87933561309234,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.SPIN_RATE_Z.getName()))).getValue());
        assertEquals(3,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.RECEIVED_SIGNAL_STRENGTH.getName())))
                        .getValue());

        assertEquals(1,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.SATELLITE_MISSION_PHASE.getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_RESET_CDHS.getName())))
                        .getValue());
        assertEquals(2,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_RESET_COM.getName())))
                        .getValue());
        assertEquals(3,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_RESET_EPS.getName())))
                        .getValue());

        assertEquals(1.1176470588235294,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.TETHER_CURRENT.getName()))).getValue());

        assertEquals(1,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_ADCS.getName())))
                        .getValue());
        assertEquals(3,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_CDHS.getName())))
                        .getValue());
        assertEquals(3,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_COM.getName())))
                        .getValue());
        assertEquals(3,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_EPS.getName())))
                        .getValue());

        assertEquals(39,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.CDHS_SYSTEM_STATUS_CODE.getName())))
                        .getValue());
        assertEquals(3,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.CDHS_SYSTEM_STATUS_VALUE.getName())))
                        .getValue());

        assertEquals(202,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.EPS_STATUS_CODE.getName()))).getValue());

        assertEquals(63,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.ADCS_SYSTEM_STATUS_CODE.getName())))
                        .getValue());
        assertEquals(2,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.ADCS_SYSTEM_STATUS_VALUE.getName())))
                        .getValue());

        assertEquals(25,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.COM_SYSTEM_STATUS_CODE.getName())))
                        .getValue());
        assertEquals(0,
                ((Parameter) (parameters.get(RadioBeaconParametersNormalMode.COM_SYSTEM_STATUS_VALUE.getName())))
                        .getValue());

    }

    @Test
    public void toParametersNormalModePartialMessageTest() throws Exception {

        // Part message test
        HashMap<String, Named> parameters2 = radioBeaconTranslator.toParameters(samplePartialMessageNormalMode,
                (long) 10, "oliver");

        assertEquals("E",
                ((Label) (parameters2.get(RadioBeaconParametersNormalMode.OPERATING_MODE.getName()))).getValue());
        assertEquals("oliver",
                ((Label) (parameters2.get(RadioBeaconParametersNormalMode.OPERATING_MODE.getName()))).getIssuedBy());
        assertEquals(10,
                ((Label) (parameters2.get(RadioBeaconParametersNormalMode.OPERATING_MODE.getName()))).getTimestamp());
        assertEquals(null, ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.TIMESTAMP.getName()))));
        assertEquals(null, ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.MAIN_BUS_VOLTAGE.getName()))));
        assertEquals(null,
                ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.AVERAGE_POWER_BALANCE.getName()))));
        assertEquals(null, ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.BATTERY_A_VOLTAGE.getName()))));
        assertEquals(null, ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.BATTERY_B_VOLTAGE.getName()))));
        assertEquals(null,
                ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.BATTERY_A_TEMPERATURE.getName()))));
        assertEquals(null, ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.SPIN_RATE_Z.getName()))));
        assertEquals(null,
                ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.RECEIVED_SIGNAL_STRENGTH.getName()))));

        assertEquals(null, ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.SATELLITE_MISSION_PHASE.getName()))));
        assertEquals(null,
                ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_RESET_CDHS.getName()))));
        assertEquals(null,
                ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_RESET_COM.getName()))));
        assertEquals(null,
                ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_RESET_EPS.getName()))));

        assertEquals(null, ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.TETHER_CURRENT.getName()))));

        assertEquals(null,
                ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_ADCS.getName()))));
        assertEquals(null,
                ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_CDHS.getName()))));
        assertEquals(null,
                ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_COM.getName()))));
        assertEquals(null,
                ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.TIME_SINCE_LAST_ERROR_EPS.getName()))));

        assertEquals(null,
                ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.CDHS_SYSTEM_STATUS_CODE.getName()))));
        assertEquals(null,
                ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.CDHS_SYSTEM_STATUS_VALUE.getName()))));

        assertEquals(null, ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.EPS_STATUS_CODE.getName()))));

        assertEquals(null,
                ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.ADCS_SYSTEM_STATUS_CODE.getName()))));
        assertEquals(null,
                ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.ADCS_SYSTEM_STATUS_VALUE.getName()))));

        assertEquals(null,
                ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.COM_SYSTEM_STATUS_CODE.getName()))));
        assertEquals(null,
                ((Parameter) (parameters2.get(RadioBeaconParametersNormalMode.COM_SYSTEM_STATUS_VALUE.getName()))));

    }

    @Test
    public void toParametersSafeModeTest() throws Exception {

        // safe mode test
        HashMap<String, Named> parameters3 = radioBeaconTranslator.toParameters(sampleMessageSafeMode, null, "oliver");

        assertEquals("T",
                ((Label) (parameters3.get(RadioBeaconParametersSafeMode.OPERATING_MODE.getName()))).getValue());
        assertEquals("oliver",
                ((Label) (parameters3.get(RadioBeaconParametersNormalMode.OPERATING_MODE.getName()))).getIssuedBy());
        assertEquals(Long.parseLong("1352902792"),
                ((Label) (parameters3.get(RadioBeaconParametersSafeMode.OPERATING_MODE.getName()))).getTimestamp());
        assertEquals(Long.parseLong("1352902792"),
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.TIMESTAMP.getName()))).getValue());
        assertEquals(160,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.ERROR_CODE_1.getName()))).getValue());
        assertEquals(218,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.ERROR_CODE_2.getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.ERROR_CODE_3.getName()))).getValue());

        assertEquals(1593,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.TIME_IN_SAFE_MODE.getName()))).getValue());
        assertEquals(2.414189978213456 /* 136 */,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.MAIN_BUS_VOLTAGE.getName()))).getValue());

        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.CDHS_PROCESSOR_A.getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.CDHS_PROCESSOR_B.getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.CDHS_BUS_SWITCH.getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.COM_3V3_VOLTAGE_LINE.getName())))
                        .getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.COM_5V_VOLTAGE_LINE.getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.PL_3V3_VOLTAGE_LINE.getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.PL_5V_VOLTAGE_LINE.getName()))).getValue());
        assertEquals(1, ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.CAM_VOLTAGE.getName()))).getValue());

        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.ADCS_VOLTAGE.getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.BATTERY_A_CHARGING.getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.BATTERY_A_DISCHARGING.getName())))
                        .getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.BATTERY_B_CHARGING.getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.BATTERY_B_DISCHARGING.getName())))
                        .getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.SECONDARY_POWER_BUS_A.getName())))
                        .getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.SECONDARY_POWER_BUS_B.getName())))
                        .getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.V12_LINE_VOLTAGE.getName()))).getValue());

        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.REGULATOR_A_5V.getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.REGULATOR_B_5V.getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.LINE_VOLTAGE_5V.getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.REGULATOR_A_3V3.getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.REGULATOR_B_3V3.getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.LINE_VOLTAGE_3V3.getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.REGULATOR_A_12V.getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.REGULATOR_B_12V.getName()))).getValue());

        assertEquals(3.028209702144293 /* 171 */,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.BATTERY_A_VOLTAGE.getName()))).getValue());
        assertEquals(0.22542297503644695 /* 12 */,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.BATTERY_B_VOLTAGE.getName()))).getValue());
        assertEquals(120.9334 /* 255 */,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.BATTERY_A_TEMPERATURE.getName())))
                        .getValue());
        assertEquals(-28.271700000000003 /* 46 */,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.BATTERY_B_TEMPERATURE.getName())))
                        .getValue());
        assertEquals(-126,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.AVERAGE_POWER_BALANCE.getName())))
                        .getValue());
        assertEquals(11,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.FIRMWARE_VERSION_NUMBER.getName())))
                        .getValue());
        assertEquals(2,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.NUMBER_OF_CRASHES.getName()))).getValue());

        assertEquals(27,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.FORWARDED_RF_POWER.getName()))).getValue());
        assertEquals(-100,
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.REFLECTED_RF_POWER.getName()))).getValue());
        assertEquals(
                49, // @TODO spec ütleb, et see peaks olema 81, kas asi ei tööta
                    // õigesti?
                ((Parameter) (parameters3.get(RadioBeaconParametersSafeMode.RECEIVED_SIGNAL_STRENGTH.getName())))
                        .getValue());
    }
}
