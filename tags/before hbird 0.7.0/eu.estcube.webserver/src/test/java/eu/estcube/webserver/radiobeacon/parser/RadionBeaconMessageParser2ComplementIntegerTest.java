package eu.estcube.webserver.radiobeacon.parser;

import static org.junit.Assert.assertEquals;

import org.hbird.exchange.core.Parameter;
import org.junit.Before;
import org.junit.Test;

public class RadionBeaconMessageParser2ComplementIntegerTest {

	private RadionBeaconMessageParser2ComplementInteger parser1;
	private RadionBeaconMessageParser2ComplementInteger parser2;

	@Before
	public void setUp() throws Exception {
		parser1 = new RadionBeaconMessageParser2ComplementInteger(0, 2, new Parameter());
		parser2 = new RadionBeaconMessageParser2ComplementInteger(0, 4, new Parameter());

	}

	@Test
	public void parseTest() throws Exception {
		assertEquals(parser1.parse("NC"), "-100");
		assertEquals(parser1.parse("ZU"), "-126");
		assertEquals(parser1.parse("WB"), "27");
	}
}
