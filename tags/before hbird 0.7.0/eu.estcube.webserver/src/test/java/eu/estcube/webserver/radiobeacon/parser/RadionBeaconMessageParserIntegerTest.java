package eu.estcube.webserver.radiobeacon.parser;

import static org.junit.Assert.assertEquals;

import org.hbird.exchange.core.Parameter;
import org.junit.Before;
import org.junit.Test;

public class RadionBeaconMessageParserIntegerTest {

	private RadionBeaconMessageParserInteger parser1;
	private RadionBeaconMessageParserInteger parser2;

	@Before
	public void setUp() throws Exception {
		parser1 = new RadionBeaconMessageParserInteger(0, 2, new Parameter());
		parser2 = new RadionBeaconMessageParserInteger(0, 4, new Parameter());

	}

	@Test
	public void parseTest() throws Exception {
		assertEquals(parser1.parse("AT"), "160");
		assertEquals(parser2.parse("T6SN"), "1593");
	}
}
