package eu.estcube.webserver.json;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.inOrder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.GsonBuilder;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ToJsonProcessorTest {

    private ToJsonProcessor toJson;

    @Mock
    private Object object;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        toJson = new ToJsonProcessor();
        inOrder = inOrder(object);
    }

    /**
     * Test method for
     * {@link eu.estcube.webserver.json.ToJsonProcessor#process(java.lang.Object)}
     * .
     */
    @Test
    public void testProcess() {
        String result = toJson.process(object);
        assertNotNull(result);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.webserver.json.ToJsonProcessor#createBuilder()}.
     */
    @Test
    public void testCreateBuilder() {
        GsonBuilder gson = ToJsonProcessor.createBuilder();
        assertNotNull(gson);
    }
}
