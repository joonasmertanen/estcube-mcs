package eu.estcube.webserver.radiobeacon;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.camel.Produce;
import org.hbird.exchange.core.Named;
import org.springframework.stereotype.Component;

import eu.estcube.webserver.utils.CamelSender;

@SuppressWarnings("serial")
@Component
public class RadioBeaconServlet extends HttpServlet {

    @Produce(uri = "activemq:topic:hbird.monitoring")
    private CamelSender camel;

    class RadioBeacon {
        protected Set<String> radioBeaconMessage = new HashSet<String>();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String datetime = request.getParameter("datetime").trim();
        String source = request.getParameter("source").trim();
        String data = request.getParameter("data").trim();

        RadioBeaconTranslator translator = new RadioBeaconTranslator();
        HashMap<String, Named> parameters = null;

        try {
            parameters = translator.toParameters(data, new RadioBeaconDateInputParser().parse(datetime), source);

            if (camel == null) {
                throw new Exception("Camel object is not set!");
            }

            for (Named key : parameters.values()) {
                camel.sendAsync(key);
            }

            response.getWriter().write("{\"status\":\"ok\",\"message\":\"Data sent!\"}");

        } catch (Exception e) {
            e.printStackTrace();
            // @TODO handle?

            response.getWriter().write(
                    "{\"status\":\"error\",\"message\":\"There were problems with parsing the data! Nothing sent!\"}");
        }

    }
}
