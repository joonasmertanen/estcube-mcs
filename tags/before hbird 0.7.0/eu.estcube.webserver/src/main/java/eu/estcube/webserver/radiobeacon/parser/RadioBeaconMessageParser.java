package eu.estcube.webserver.radiobeacon.parser;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.hbird.exchange.core.Named;

import eu.estcube.domain.JMSConstants;

abstract public class RadioBeaconMessageParser {

	abstract public String parse(String message);

	abstract public Named parseToNamed(String message);

	public String getValidStringPart(String hex, Integer startIndex, Integer endIndex) {

		String part = hex.substring(startIndex, endIndex);
		for (Integer s = 0; s < part.length(); s++) {
			if (part.substring(s, s + 1).compareTo("#") == 0) {
				return null;
			}
		}

		return part;

	}

	public String parseHex(String hex) {
		try {
			return String.valueOf(Integer.parseInt(hex, JMSConstants.HEX_BASE));
		} catch (Exception e) {
			return "0";
		}
	}

	public String cwToHex(String message) {
		Map<String, String> map = new HashMap<String, String>();
		map = getRules();
		StringBuilder result = new StringBuilder();
		for (char c : message.toCharArray()) {
			result.append(map.get(c + ""));
		}
		return result.toString();
	}

	public Map<String, String> getRules() {
		Map<String, String> rulesMap = new HashMap<String, String>();
		rulesMap.put("T", "0");
		rulesMap.put("W", "1");
		rulesMap.put("U", "2");
		rulesMap.put("S", "3");
		rulesMap.put("H", "4");
		rulesMap.put("5", "5");
		rulesMap.put("6", "6");
		rulesMap.put("M", "7");
		rulesMap.put("Z", "8");
		rulesMap.put("N", "9");
		rulesMap.put("A", "A");
		rulesMap.put("B", "B");
		rulesMap.put("C", "C");
		rulesMap.put("D", "D");
		rulesMap.put("E", "E");
		rulesMap.put("F", "F");
		return rulesMap;
	}

	public String unixTimestampToDate(String date) {
		Date d = new Date();
		d.setTime(Long.parseLong(date) * 1000);
		Format formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		String result = formatter.format(d);
		return result;
	}

}
