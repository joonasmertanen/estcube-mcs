package eu.estcube.webserver.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.apache.camel.Exchange;
import org.hbird.exchange.core.Named;
import org.springframework.stereotype.Component;

/**
 * Bean for caching telemetryobjects from messages that are passed through it
 * and returning cached elements by request in the message body.
 * 
 * @author Kaupo Kuresson
 */
@Component
public class CacheMessage {

    static final Comparator<Named> TIME_ORDER = new Comparator<Named>() {
        public int compare(Named t1, Named t2) {
            return Long.valueOf(t1.getTimestamp()).compareTo(t2.getTimestamp());
        }
    };

    private static int cacheLimit;

    /**
     * HashMap containing cached TelemetryObjects as values and their
     * names(String) as keys.
     */
    private static HashMap<String, ArrayList<Named>> objectCache;

    /**
     * Class constructor
     */
    public CacheMessage() {
        objectCache = new HashMap<String, ArrayList<Named>>();
    }

    /**
     * Takes a TelemetryObject from the body of a passing message and adds it to
     * the cache.
     * 
     * @param ex
     *            Camel automatically binds the Exchange to this parameter.
     * @see <a
     *      href="http://camel.apache.org/maven/current/camel-core/apidocs/org/apache/camel/Exchange.html">Exchange</a>
     */
    public void addToCache(Exchange ex) {
        Named named = ex.getIn().getBody(Named.class);
        String key = named.getIssuedBy() + named.getName();
        if (!objectCache.containsKey(key)) {
            ArrayList<Named> newList = new ArrayList<Named>();
            newList.add(named);
            objectCache.put(key, newList);
        } else {
            ArrayList<Named> list = objectCache.get(key);
            list.add(named);
            Collections.sort(list, TIME_ORDER); // list will start with older
                                                // entries, end with newer
            if (list.size() > cacheLimit) {
                list.remove(0); // remove oldest entry
            }
        }
    }

    /**
     * Gets a specific element from the cache and returns it in the message
     * body.
     * 
     * @param ex
     *            Camel automatically binds the Exchange to this parameter.
     * @param param
     *            Source, device, name of the TelemetryObject to get out of the cache.
     * @see <a
     *      href="http://camel.apache.org/maven/current/camel-core/apidocs/org/apache/camel/Exchange.html">Exchange</a>
     */
    public void getCachedElement(Exchange ex, HashMap<String, String> param) {
        String key = param.get("ISSUEDBY") + param.get("NAME");
        ex.getOut().setBody(objectCache.get(key));
    }

    /**
     * Returns the whole cache in the message body.
     * 
     * @param ex
     *            Camel automatically binds the Exchange to this parameter.
     * @see <a
     *      href="http://camel.apache.org/maven/current/camel-core/apidocs/org/apache/camel/Exchange.html">Exchange</a>
     */
    public void getCache(Exchange ex) {
        List<Named> list = new ArrayList<Named>();

        for (String key : objectCache.keySet()) {
            for (Named obj : objectCache.get(key))
                list.add(obj);
        }
        ex.getOut().setBody(list);
    }

    public static int getCacheLimit() {
        return cacheLimit;
    }

    public static void setCacheLimit(int cacheLimit) {
        CacheMessage.cacheLimit = cacheLimit;
    }
}
