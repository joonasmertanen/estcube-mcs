package eu.estcube.webserver.radiobeacon;

import java.util.HashMap;

import org.hbird.exchange.core.Named;

import eu.estcube.domain.JMSConstants;
import eu.estcube.domain.RadioBeacon;
import eu.estcube.webserver.radiobeacon.parser.RadionBeaconMessageParserTimestamp;
import eu.estcube.webserver.radiobeacon.parsers.RadioBeaconParsersNormalMode;
import eu.estcube.webserver.radiobeacon.parsers.RadioBeaconParsers;
import eu.estcube.webserver.radiobeacon.parsers.RadioBeaconParsersSafeMode;

public class RadioBeaconTranslator {

    private static final String MODE_NORMAL = "Normal mode";
    private static final String MODE_SAFE = "Safe mode";
    private static final String MODE_UNDEFINED = "Undefined mode";

    public String determineRadioBeaconMode(String message) {
        String messageModeID = message.substring(6, 7);
        if (messageModeID.equals("E")) {
            return MODE_NORMAL;
        } else if (messageModeID.equals("T")) {
            return MODE_SAFE;
        } else {
            String messageEndCharacter = message.substring(message.length() - 2, message.length());
            if (messageEndCharacter.equals("KN")) {
                return MODE_SAFE;
            } else {
                messageEndCharacter = messageEndCharacter.substring(messageEndCharacter.length() - 1);
                if (messageEndCharacter.equals("K")) {
                    return MODE_NORMAL;
                }
                return MODE_UNDEFINED;
            }
        }
    }

    public String reformBeaconMessage(String message) {
        String newMessage = message.replaceAll("\\s", "");
        newMessage = newMessage.toUpperCase();
        return newMessage;
    }

    public boolean checkRadioBeaconBasis(String message) {
        String messageBasis = message.substring(0, 6);
        if (messageBasis.equals("ES5E/S")) {
            return true;
        }
        return false;
    }

    public boolean checkBeaconLength(String message) {
        int messageLength = message.length();
        if (messageLength == JMSConstants.NORMAL_BEACON_MESSAGE_LENGTH
                || messageLength == JMSConstants.SAFE_BEACON_MESSAGE_LENGTH) {
            return true;
        }
        return false;
    }

    public boolean checkBeaconValidity(String message) {
        if ((determineRadioBeaconMode(message).equals(MODE_NORMAL) || determineRadioBeaconMode(message).equals(
                MODE_SAFE))
                && checkRadioBeaconBasis(message)) {
            return true;
        }
        return false;
    }

    public boolean checkASCIIMessage(String message) {
        if (message.matches("\\A\\p{ASCII}*\\z")) {
            return true;
        }
        return false;
    }

    public boolean checkBeaconMessage(String message) {
        if (checkBeaconLength(message) && checkASCIIMessage(message)) {
            if (checkBeaconValidity(message)) {
                return true;
            }
            return false;
        }
        return false;
    }

    protected RadioBeaconParsers getParsers(String message) throws Exception {

        String beaconMode = determineRadioBeaconMode(message);
        if (beaconMode == MODE_NORMAL) {
            return new RadioBeaconParsersNormalMode();
        } else if (beaconMode == MODE_SAFE) {
            return new RadioBeaconParsersSafeMode();
        } else {
            throw new Exception("Can't parse beacon in mode '" + beaconMode + "'");
        }

    }

    protected HashMap<String, Named> toParameters(String message, Long timestamp, String issuedBy) throws Exception {
        message = reformBeaconMessage(message);
        RadioBeaconParsers parsers = getParsers(message);

        if (checkBeaconMessage(message) == false) {
            return new HashMap<String, Named>();
        }

        if (timestamp == null) {
            RadionBeaconMessageParserTimestamp parser = parsers.getTimestampParser();
            if (parser != null) {
                timestamp = parser.getResult(message);
            }
            if (timestamp == null) {
                timestamp = (long) 0;
            }
        }

        return parsers.parse(message, timestamp, issuedBy);
    }

    public RadioBeacon translateMessage(String message) throws Exception {
        RadioBeacon translatedRadioBeacon = new RadioBeacon();
        // @TODO Fix this
        // HashMap<String, String> parameters = toParameters(message);
        // translatedRadioBeacon.setParameters(parameters);
        return translatedRadioBeacon;
    }

}
