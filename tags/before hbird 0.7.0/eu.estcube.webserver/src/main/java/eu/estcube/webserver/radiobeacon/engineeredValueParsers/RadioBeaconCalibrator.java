package eu.estcube.webserver.radiobeacon.engineeredValueParsers;

import org.hbird.exchange.core.Parameter;

abstract public class RadioBeaconCalibrator {

    abstract public String calibrate(String rawValue);

    abstract public Parameter calibrate(Parameter rawValue);

}
