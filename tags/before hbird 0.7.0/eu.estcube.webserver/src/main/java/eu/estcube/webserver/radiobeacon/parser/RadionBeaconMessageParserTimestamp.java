package eu.estcube.webserver.radiobeacon.parser;

import org.hbird.exchange.core.Parameter;

public class RadionBeaconMessageParserTimestamp extends RadioBeaconMessageParser {

    protected Integer startIndex;

    protected Integer endIndex;

    protected Parameter parameterBase;

    protected Long result;

    public RadionBeaconMessageParserTimestamp(Integer startIndex, Integer length, Parameter parameterBase) {
        this.startIndex = startIndex;
        this.endIndex = startIndex + length;
        this.parameterBase = parameterBase;
    }

    public String parse(String hex) {
        String part = getValidStringPart(hex, this.startIndex, this.endIndex);
        if (part == null) {
            result = null;
            return null;
        }
        result = 1342177280 + Long.parseLong(parseHex(cwToHex(part)));
        return String.valueOf(result);
    }

    public Parameter parseToNamed(String message) {
        parse(message);
        if (result == null) {
            return null;
        }
        Parameter parameter = new Parameter(parameterBase);
        parameter.setValue(result);
        return parameter;
    }

    public Long getResult(String message) {
        parse(message);
        return result;
    }
}
