package eu.estcube.webserver.json;

import java.util.Date;

import org.apache.camel.Body;
import org.apache.log4j.spi.LoggingEvent;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Processor to serialize {@link Object}s to JSON strings.
 */
@Component
public class ToJsonProcessor {

    /** {@link GsonBuilder} instance. */
    private final GsonBuilder builder = createBuilder();

    /**
     * Serializes {@link Object} to JSON string.
     * 
     * @param body Object to serialize
     * @return Object as JSON string
     */
    public String process(@Body Object body) {
        Gson gson = builder.create();
        String json = gson.toJson(body);
        return json;
    }

    /**
     * Creates new {@link GsonBuilder} to use in serialization.
     * 
     * @return new {@link GsonBuilder}
     */
    static GsonBuilder createBuilder() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Date.class, new JsonDateSerializer());
        builder.registerTypeAdapter(byte[].class, new JsonBinaryToBase64Serializer());
        builder.registerTypeAdapter(LoggingEvent.class, new JsonLoggingEventSerializer());
        builder.serializeSpecialFloatingPointValues();
        return builder;
    }
}