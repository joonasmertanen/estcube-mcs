package eu.estcube.webserver;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.servlet.Servlet;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.websocket.WebsocketComponent;
import org.apache.camel.component.websocket.WebsocketConstants;
import org.apache.camel.spring.Main;
import org.apache.log4j.spi.LoggingEvent;
import org.eclipse.jetty.http.security.Constraint;
import org.eclipse.jetty.security.Authenticator;
import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.LoginService;
import org.eclipse.jetty.security.authentication.FormAuthenticator;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.SessionManager;
import org.eclipse.jetty.server.session.HashSessionManager;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.component.LifeCycle;
import org.hbird.exchange.businesscard.BusinessCard;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.core.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.atlassian.crowd.service.soap.client.SecurityServerClientFactory;

import eu.estcube.common.CollectionOfNamedObjectsSplitter;
import eu.estcube.common.PrepareForInjection;
import eu.estcube.domain.JMSConstants;
import eu.estcube.domain.transport.Downlink;
import eu.estcube.domain.transport.Uplink;
import eu.estcube.webserver.auth.LoginServiceListener;
import eu.estcube.webserver.auth.LogoutServlet;
import eu.estcube.webserver.auth.crowd.CrowdLoginService;
import eu.estcube.webserver.cache.Cache;
import eu.estcube.webserver.cache.LogCache;
import eu.estcube.webserver.cache.NamedCache;
import eu.estcube.webserver.cache.guava.GuavaTimeoutStore;
import eu.estcube.webserver.cache.simple.SimpleLimitStore;
import eu.estcube.webserver.json.ToJsonProcessor;
import eu.estcube.webserver.json.TransportFrameToJsonProcessor;
import eu.estcube.webserver.radiobeacon.RadioBeaconServlet;
import eu.estcube.webserver.radiobeacon.RadioBeaconTranslationServlet;
import eu.estcube.webserver.tle.request.TleServlet;
import eu.estcube.webserver.tle.upload.TleSubmitServlet;
import eu.estcube.webserver.tle.upload.TleUploadRequestConverter;
import eu.estcube.webserver.tle.upload.TleUploadRequestValidator;
import eu.estcube.webserver.tle.upload.TleUploadResponseCreator;
import eu.estcube.webserver.userinfo.UserInfoServlet;
import eu.estcube.webserver.utils.ToTransportFrame;
import eu.estcube.webserver.utils.UIErrorHandler;

public class WebServer extends RouteBuilder {

    private static final String[] ROLES = new String[] { "mcs-premium-admin", "mcs-premium-op", "mcs-op" };

    private static final Logger LOG = LoggerFactory.getLogger(WebServer.class);

    @Value("${heart.beat.interval}")
    private int heartBeatInterval = 3000;

    @Value("${service.id}")
    private String serviceId;

    @Value("${service.version}")
    private String webServerVersion;

    @Value("${web.server.host}")
    private String webServerHost;

    @Value("${web.server.port}")
    private int webServerPort;

    @Value("${web.socket.port}")
    private int webSocketPort;

    @Value("${static.resources}")
    private String staticResources;

    // @Value("${webSocketAdress}")
    // private String webSocketAdress;
    //
    // @Value("${webSocketCacheReqAdress}")
    // private String webSocketCacheReqAdress;
    //
    // @Value("${webSocketVersionsAdress}")
    // private String webSocketVersionsAdress;
    //
    // @Value("${webSocketForLog}")
    // private String webSocketForLog;
    //
    // @Value("${webSocketForSystem}")
    // private String webSocketForSystem;
    //
    // @Value("${webSocketForMap}")
    // private String webSocketForMap;
    //
    // @Value("${maxObjectsCached}")
    // private int cacheLimit;
    //
    // @Autowired
    // private JsonToCommand jsonToTelemetryCommand;
    //
    // @Autowired
    // private InputStreamToBase64 inputStreamToBase64;
    //
    // @Autowired
    // private VersionInfoToJson versionInfoToJson;
    //
    // @Autowired
    // private NamedObjectToJson telemetryObjectToJson;
    //
    // @Autowired
    // private NamedCache systemCache;

    @Autowired
    private LoginServiceListener loginServiceListener;

    @Autowired
    private ErrorServlet errorServlet;

    @Autowired
    private LogoutServlet logoutServlet;

    @Autowired
    private ToJsonProcessor toJsonProcessor;

    @Autowired
    private UIErrorHandler uiErrorHandler;

    @Autowired
    private UserInfoServlet userInfoServlet;

    @Autowired
    private TleSubmitServlet tleUploadServlet;

    @Autowired
    private TleServlet tleServlet;

    @Autowired
    private TleUploadRequestValidator tleValidator;

    @Autowired
    private TleUploadRequestConverter tleConverter;

    @Autowired
    private TleUploadResponseCreator tleResponse;

    @Autowired
    private CollectionOfNamedObjectsSplitter collectionSplitter; // TODO -
    // 17.12.2012; kimmell - check if same as Splitter

    @Autowired
    private PrepareForInjection preparator;

    @Autowired
    private NamedObjectPublisher publisher;

    @Autowired
    private ToTransportFrame toTransportFrame;

    @Autowired
    private TransportFrameToJsonProcessor transportFrameToJson;

    @Autowired
    private RadioBeaconServlet radioBeaconServlet;

    @Autowired
    private RadioBeaconTranslationServlet radioBeaconTranslationServlet;

    private HashMap<String, String> versions = new HashMap<String, String>();

    private final Cache<String, BusinessCard> businessCardCache = new NamedCache<BusinessCard>(
            new GuavaTimeoutStore<String, BusinessCard>(1000L * 60));

    private final Cache<String, Named> namedCache = new NamedCache<Named>(new GuavaTimeoutStore<String, Named>(
            1000L * 60 * 10));

    private final Cache<String, LoggingEvent> logCache = new LogCache(new SimpleLimitStore<String, LoggingEvent>(200));

    @Override
    public void configure() throws Exception {
        MDC.put(StandardArguments.ISSUED_BY, serviceId);
        versions.put(JMSConstants.COMPONENT_WEBSERVER, webServerVersion);
        setupJettyWebServer();
        setupCamelRoutes();
    }

    /**
     * 
     */
    void setupCamelRoutes() {
        // configure websocket component
        WebsocketComponent websocketComponent = (WebsocketComponent) getContext().getComponent("websocket");
        websocketComponent.setPort(webSocketPort);
        // websocketComponent.setStaticResources("file:" + staticResources);

        // Endpoint websocket =
        // websocketComponent.createEndpoint(webSocketAdress);
        // Endpoint websocketCacheRequest =
        // websocketComponent.createEndpoint(webSocketCacheReqAdress);
        // Endpoint websocketForLog =
        // websocketComponent.createEndpoint(webSocketForLog);
        // Endpoint websocketForSystem =
        // websocketComponent.createEndpoint(webSocketForSystem);
        // Endpoint websocketForMap =
        // websocketComponent.createEndpoint(webSocketForMap);

        // from("timer://status?repeatCount=1").to(JMSConstants.AMQ_PS_QUEUE);
        // from("timer://versionsRequest?repeatCount=1").to(JMSConstants.AMQ_VERSIONS_REQUEST);
        //
        // from(JMSConstants.AMQ_VERSIONS_RECEIVE)
        // .process(new Processor() {
        // public void process(Exchange ex) throws Exception {
        // String componentId = "";
        // try {
        // componentId = ex.getIn().getHeader(JMSConstants.HEADER_COMPONENT_ID,
        // String.class);
        // } catch (Exception e) {
        // }
        // versions.put(ex.getIn().getHeader(JMSConstants.HEADER_COMPONENT,
        // String.class) + "-" + componentId,
        // ex.getIn().getBody(String.class));
        // }
        // }).process(new Processor() {
        // public void process(Exchange ex) throws Exception {
        // ex.getOut().setBody(versions);
        // }
        // }).process(versionInfoToJson).setHeader(WebsocketConstants.SEND_TO_ALL,
        // constant(true))
        // .to(webSocketVersionsAdress);
        //
        // from(JMSConstants.AMQ_WEBSERVER_FOR_LOG).bean(toJsonProcessor)
        // .setHeader(WebsocketConstants.SEND_TO_ALL,
        // constant(true)).to(websocketForLog);
        //
        // from(JMSConstants.AMQ_WEBSERVER_FOR_MAP).bean(toJsonProcessor)
        // .setHeader(WebsocketConstants.SEND_TO_ALL,
        // constant(true)).to(websocketForMap);
        //
        // from(JMSConstants.AMQ_WEBSERVER_FOR_SYSTEM).choice()
        // //
        // from("activemq:topic:hbird.monitoring?selector=type='BusinessCard'").choice()
        // // cache only Named objects
        // .when(body().isInstanceOf(Named.class)).bean(systemCache,
        // "updateCache").end().bean(toJsonProcessor)
        // .setHeader(WebsocketConstants.SEND_TO_ALL,
        // constant(true)).to(websocketForSystem);
        //
        // from(webSocketVersionsAdress).process(new Processor() {
        // public void process(Exchange ex) throws Exception {
        // ex.getOut().setBody(versions);
        // }
        // }).process(versionInfoToJson).setHeader(WebsocketConstants.SEND_TO_ALL,
        // constant(true))
        // .to(webSocketVersionsAdress);
        //
        // from(websocketCacheRequest).bean(CacheMessage.class,
        // "getCache").split()
        // .method(collectionSplitter,
        // "splitMessage").process(telemetryObjectToJson)
        // .setHeader(WebsocketConstants.SEND_TO_ALL,
        // constant(true)).to(websocketCacheRequest);
        //
        // from(websocket).process(jsonToTelemetryCommand).process(new
        // Processor() {
        // public void process(Exchange exchange) throws Exception {
        // exchange.getIn().setHeader("groundStationID", "ES5EC");
        // exchange.getIn().setHeader("device",
        // exchange.getIn().getBody(Command.class).getDestination());
        // }
        // }).to(JMSConstants.AMQ_GS_RECEIVE);
        //
        // from(websocketForLog).bean(toJsonProcessor).setHeader(WebsocketConstants.SEND_TO_ALL,
        // constant(true))
        // .to(websocketForLog);
        //
        // from(websocketForSystem).transform(body().append("")).bean(systemCache,
        // "getCache").split()
        // .method(Splitter.class,
        // "doSplit").bean(toJsonProcessor).to(websocketForSystem);
        //
        // from(JMSConstants.AMQ_GS_SEND).bean(CacheMessage.class,
        // "addToCache").process(telemetryObjectToJson)
        // .setHeader(WebsocketConstants.SEND_TO_ALL,
        // constant(true)).to(websocket);
        //
        // from(JMSConstants.AMQ_WEBCAM_SEND).process(inputStreamToBase64)
        // .setHeader(WebsocketConstants.SEND_TO_ALL,
        // constant(true)).to("websocket://webcam");

        // configure Camel routes
        // @formatter:off

        from("activemq:topic:hbird.monitoring?selector=type='BusinessCard'")
            .bean(businessCardCache, "putObject")
            .bean(toJsonProcessor)
            .to("log:eu.estcube.webserver.stats-businesCards?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
            .setHeader(WebsocketConstants.SEND_TO_ALL, constant(true))
            .inOnly("websocket://hbird.out.businesscards");

        from("websocket://hbird.out.businesscards")
            .bean(businessCardCache, "getAll")
            .to("log:cacheDump")
            .split(body())
            .bean(toJsonProcessor)
            .inOnly("websocket://hbird.out.businesscards");
        
        from("activemq:topic:hbird.monitoring")
            .bean(namedCache, "putObject")
            .bean(toJsonProcessor)
            .to("log:eu.estcube.webserver.stats-monitoring?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
            .setHeader(WebsocketConstants.SEND_TO_ALL, constant(true))
            .inOnly("websocket://hbird.out.all");
        
        from("websocket://hbird.out.all")
            .bean(namedCache, "getAll")
            .split(body())
            .bean(toJsonProcessor)
            .to("log:eu.estcube.webserver.stats-namedcache?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
            .inOnly("websocket://hbird.out.all");


        // TODO - 12.03.2013; kimmell - check this
        from("activemq:topic:hbird.monitoring?selector=type='Parameter' OR type='State' OR type='Label'")
            .bean(toJsonProcessor)
            .to("log:eu.estcube.webserver.stats-parameters?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
            .setHeader(WebsocketConstants.SEND_TO_ALL, constant(true))
            .inOnly("websocket://hbird.out.parameters");
        
        from("activemq:topic:systemlog")
            .bean(logCache, "putObject")
            .bean(toJsonProcessor)
            .to("log:eu.estcube.webserver.stats-systemlog?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
            .setHeader(WebsocketConstants.SEND_TO_ALL, constant(true))
            .inOnly("websocket://hbird.out.systemlog");
        
        from("websocket://hbird.out.systemlog")
            .bean(logCache, "getAll")
            .split(body())
            .bean(toJsonProcessor)
            .to("log:eu.estcube.webserver.stats-logcache?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
            .inOnly("websocket://hbird.out.systemlog");

        
        from("direct:tle-submit")
            .doTry()
                .bean(tleValidator)
                .to("log:tle-submit")
                .bean(tleConverter)
                .bean(preparator)
                .bean(publisher)
//                .setHeader("destination", constant("OrbitPredictor"))
//                .inOnly("activemq:topic:hbird.commands")
//                .process(new Processor() {
//                    @Override
//                    public void process(Exchange exchange) throws Exception {
//                        // DataRequest dr = new DataRequest(null, null,
//                        // "TlePropagationRequest", null);
//                        TleRequest tleRequest = new TleRequest("ME", "ESTCube-1");
//                        exchange.getOut().setBody(tleRequest);
//                    }
//                })
//                .to("log:request-out")
//                .to("activemq:topic:hbird.commands")
//                .to("log:response?showAll=true")
                .bean(tleResponse)
            .doCatch(Exception.class)
                .bean(uiErrorHandler)
            .end();

        from("activemq:topic:webcamSend")
            .bean(toJsonProcessor)
            .to("log:eu.estcube.webserver.stats-webcam?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
            .setHeader(WebsocketConstants.SEND_TO_ALL, constant(true))
            .inOnly("websocket://hbird.out.binary");

        // XXX - 04.03.2013; kimmell - for TNC frame HEX dump
        from(Downlink.FROM_TNC)
            .to("direct:estcubeDownlinkTransport");
        
        from(Downlink.AX25_FRAMES)
            .to("direct:estcubeDownlinkTransport");
        
        from(Uplink.AX25_FRAMES_LOG)
            .to("direct:estcubeDownlinkTransport");
        
        from(Uplink.TNC_FRAMES_LOG)
            .to("direct:estcubeDownlinkTransport");
        
        from("direct:estcubeDownlinkTransport")
            .bean(toTransportFrame)
            .bean(transportFrameToJson)
            .to("log:eu.estcube.webserver.stats-dumpTnc?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
            .setHeader(WebsocketConstants.SEND_TO_ALL, constant(true))
            .inOnly("websocket://hbird.out.all");
        
        from("direct:beacon-submit")
           .to("log:beacon-submit")
           .end();
        
        BusinessCard card = new BusinessCard(serviceId, heartBeatInterval);
        card.setDescription(String.format("Web server; version: %s", webServerVersion));
        from("timer:heartbeat?fixedRate=true&period=" + heartBeatInterval)
            .bean(card)
            .bean(preparator)
            .to("activemq:topic:hbird.monitoring");

        // @formatter:on
    }

    /**
     * @throws Exception
     */
    void setupJettyWebServer() throws Exception {
        // CacheMessage.setCacheLimit(cacheLimit);
        InetSocketAddress address = new InetSocketAddress(webServerHost, webServerPort);
        Server server = new Server(address);

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

        SessionManager sm = new HashSessionManager();
        SessionHandler sh = new SessionHandler(sm);
        context.setSessionHandler(sh);

        context.setResourceBase(staticResources);

        // Special servlet for static resources
        DefaultServlet defaultServlet = new DefaultServlet();
        ServletHolder holder = new ServletHolder(defaultServlet);

        // - This server prevents .js files from being cached.
        holder.setInitParameter("useFileMappedBuffer", "false");
        holder.setInitParameter("cacheControl", "no-store,no-cache,must-revalidate");
        holder.setInitParameter("maxCachedFiles", "0");
        context.addServlet(holder, "/");

        // Servlets without constraints
        addServlet(context, "/logout", logoutServlet);
        addServlet(context, "/error", errorServlet);

        // Servlets with constraints
        Constraint constraint = createConstraint("auth", ROLES);
        List<ConstraintMapping> mappings = new ArrayList<ConstraintMapping>();

        // - Restrict all static resources under /MCS/*
        mappings.add(createConstraintMapping(constraint, "/MCS/*"));

        // Restricted servlets - available only for authenticated users
        mappings.add(addServlet(context, "/user", userInfoServlet, constraint));
        mappings.add(addServlet(context, "/radioBeacon", radioBeaconServlet, constraint));
        mappings.add(addServlet(context, "/translateRadioBeacon", radioBeaconTranslationServlet, constraint));
        mappings.add(addServlet(context, "/tle/submit", tleUploadServlet, constraint));
        mappings.add(addServlet(context, "/tle", tleServlet, constraint));

        // Login service
        LoginService loginService = createLoginService(loginServiceListener);
        server.addBean(loginService);

        // Authenticator
        Authenticator authenticator = createAutenticator("/MCS/login.html", "/error");

        // Security handler
        ConstraintSecurityHandler security = new ConstraintSecurityHandler();
        security.setConstraintMappings(mappings, new HashSet<String>(Arrays.asList(ROLES)));
        security.setAuthenticator(authenticator);
        security.setLoginService(loginService);
        security.setStrict(false);

        // Disabling next two rows will remove security constraints from the
        // web server
        server.setHandler(security);
        context.setHandler(security);

        // Start the server
        server.setHandler(context);
        server.start();
    }

    Constraint createConstraint(String name, String[] roles) {
        Constraint constraint = new Constraint();
        constraint.setName(name);
        constraint.setRoles(roles);
        constraint.setAuthenticate(true);
        return constraint;
    }

    void addServlet(ServletContextHandler context, String path, Class<? extends Servlet> servletClass) {
        context.addServlet(servletClass, path);
    }

    void addServlet(ServletContextHandler context, String path, Servlet servlet) {
        context.addServlet(new ServletHolder(servlet), path);
    }

    ConstraintMapping addServlet(ServletContextHandler context, String path, Class<? extends Servlet> servletClass,
            Constraint constraint) {
        addServlet(context, path, servletClass);
        return createConstraintMapping(constraint, path);
    }

    ConstraintMapping addServlet(ServletContextHandler context, String path, Servlet servlet, Constraint constraint) {
        addServlet(context, path, servlet);
        return createConstraintMapping(constraint, path);
    }

    ConstraintMapping createConstraintMapping(Constraint constraint, String path) {
        ConstraintMapping mapping = new ConstraintMapping();
        mapping.setConstraint(constraint);
        mapping.setPathSpec(path);
        return mapping;
    }

    LoginService createLoginService(LifeCycle.Listener listener) {
        CrowdLoginService service = new CrowdLoginService(SecurityServerClientFactory.getSecurityServerClient());
        service.addLifeCycleListener(listener);
        return service;
    }

    Authenticator createAutenticator(String loginPage, String errorPage) {
        return new FormAuthenticator(loginPage, errorPage, true);
    }

    public static void main(String[] args) throws Exception {
        LOG.info("Starting WebServer");
        try {
            new Main().run(args);
        } catch (Exception e) {
            LOG.error("Failed to start " + WebServer.class.getName(), e);
        }
    }
}
