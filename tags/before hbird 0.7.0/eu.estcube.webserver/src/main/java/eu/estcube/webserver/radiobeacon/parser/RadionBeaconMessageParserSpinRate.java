package eu.estcube.webserver.radiobeacon.parser;

import org.hbird.exchange.core.Parameter;

public class RadionBeaconMessageParserSpinRate extends RadioBeaconMessageParser {

	protected Integer startIndex;

	protected Integer endIndex;

	protected Parameter parameterBase;

	protected Double result;

	public RadionBeaconMessageParserSpinRate(Integer startIndex, Integer length, Parameter parameterBase) {
		this.startIndex = startIndex;
		this.endIndex = startIndex + length;
		this.parameterBase = parameterBase;
	}

	public String parse(String hex) {
		String part = getValidStringPart(hex, this.startIndex, this.endIndex);
		if (part == null) {
			result = null;
			return null;
		}
		result = (Double.parseDouble(parseHex(cwToHex(part))) * 720) / 2047;
		return String.valueOf(result);
	}

	public Parameter parseToNamed(String message) {
		parse(message);
		if (result == null) {
			return null;
		}
		Parameter parameter = new Parameter(parameterBase);
		parameter.setValue(result);
		return parameter;
	}
}
