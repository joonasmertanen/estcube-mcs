package eu.estcube.webserver.radiobeacon.parser;

import org.hbird.exchange.core.Parameter;

import eu.estcube.domain.JMSConstants;

public class RadionBeaconMessageParser2ComplementInteger extends RadioBeaconMessageParser {

	protected Integer startIndex;

	protected Integer endIndex;

	protected Parameter parameterBase;

	protected Integer result;

	public RadionBeaconMessageParser2ComplementInteger(Integer startIndex, Integer length, Parameter parameterBase) {
		this.startIndex = startIndex;
		this.endIndex = startIndex + length;
		this.parameterBase = parameterBase;
	}

	public String parse(String hex) {
		String part = getValidStringPart(hex, this.startIndex, this.endIndex);
		if (part == null) {
			result = null;
			return null;
		}

		Integer i = Integer.parseInt(cwToHex(part), JMSConstants.HEX_BASE);
		result = i > 128 ? i - 256 : i;
		return String.valueOf(result);
	}

	public Parameter parseToNamed(String message) {
		parse(message);
		if (result == null) {
			return null;
		}
		Parameter parameter = new Parameter(parameterBase);
		parameter.setValue(result);
		return parameter;
	}
}
