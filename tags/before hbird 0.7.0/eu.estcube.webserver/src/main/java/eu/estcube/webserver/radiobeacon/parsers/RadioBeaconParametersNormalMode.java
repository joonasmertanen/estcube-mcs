package eu.estcube.webserver.radiobeacon.parsers;

import org.hbird.exchange.core.Label;
import org.hbird.exchange.core.Parameter;

public class RadioBeaconParametersNormalMode {

    public static final Label OPERATING_MODE = new Label(null, "operating.mode", "Operating mode",
            "Operating mode (E=normal or T=safe)", "");

    public static final Parameter TIMESTAMP = new Parameter(null, "timestamp", "Timestamp", "Timestamp", 0, "sec");

    public static final Parameter MAIN_BUS_VOLTAGE = new Parameter(null, "main.bus.voltage", "Main bus voltage",
            "Main bus voltage", 0, "V");

    public static final Parameter AVERAGE_POWER_BALANCE = new Parameter(null, "average.power.balance",
            "Average power balance", "Average power balance", 0, "W");

    public static final Parameter BATTERY_A_VOLTAGE = new Parameter(null, "battery.A.voltage", "Battery A voltage", "",
            0, "V");

    public static final Parameter BATTERY_B_VOLTAGE = new Parameter(null, "battery.B.voltage", "Battery B voltage", "",
            0, "V");

    public static final Parameter BATTERY_A_TEMPERATURE = new Parameter(null, "battery.A.temperature",
            "Battery A temperature", "", 0, "C");

    public static final Parameter SPIN_RATE_Z = new Parameter(null, "spin.rate.z", "Spin rate Z", "", 0, "deg/s");

    public static final Parameter RECEIVED_SIGNAL_STRENGTH = new Parameter(null, "received.signal.strength",
            "Received signal strength", "", 0, "3dBm");

    public static final Parameter SATELLITE_MISSION_PHASE = new Parameter(null, "satellite.mission.phase",
            "Satellite mission phase",
            "0=Detumbling, 1=Nadir pointing, 2=Tether deployment, 3=E-sail force measurement", 0, "");

    public static final Parameter TIME_SINCE_LAST_RESET_CDHS = new Parameter(null, "time.since.last.reset.CDHS",
            "Time since last reset CDHS", "", 0, "hour(s)");

    public static final Parameter TIME_SINCE_LAST_RESET_COM = new Parameter(null, "time.since.last.reset.COM",
            "Time since last reset COM", "", 0, "hour(s)");

    public static final Parameter TIME_SINCE_LAST_RESET_EPS = new Parameter(null, "time.since.last.reset.EPS",
            "Time since last reset EPS", "", 0, "hour(s)");

    public static final Parameter TETHER_CURRENT = new Parameter(null, "tether.current", "Tether current", "", 0, "mA");

    public static final Parameter TIME_SINCE_LAST_ERROR_ADCS = new Parameter(null, "time.since.last.error.ADCS",
            "Time since last error ADCS", "", 0, "hour(s)");

    public static final Parameter TIME_SINCE_LAST_ERROR_CDHS = new Parameter(null, "time.since.last.error.CDHS",
            "Time since last error CDHS", "", 0, "hour(s)");

    public static final Parameter TIME_SINCE_LAST_ERROR_COM = new Parameter(null, "time.since.last.error.COM",
            "Time since last error COM", "", 0, "hour(s)");

    public static final Parameter TIME_SINCE_LAST_ERROR_EPS = new Parameter(null, "time.since.last.error.EPS",
            "Time since last error EPS", "", 0, "hour(s)");

    public static final Parameter CDHS_SYSTEM_STATUS_CODE = new Parameter(null, "CDHS.system.status.code",
            "CDHS system status code", "", 0, "");

    public static final Parameter CDHS_SYSTEM_STATUS_VALUE = new Parameter(null, "CDHS.system.status.value",
            "CDHS system status value", "", 0, "");

    public static final Parameter EPS_STATUS_CODE = new Parameter(null, "EPS.status.code", "EPS status code", "", 0, "");

    public static final Parameter ADCS_SYSTEM_STATUS_CODE = new Parameter(null, "ADCS.system.status.code",
            "ADCS system status code", "", 0, "");

    public static final Parameter ADCS_SYSTEM_STATUS_VALUE = new Parameter(null, "ADCS.system.status.value",
            "ADCS system status value", "", 0, "");

    public static final Parameter COM_SYSTEM_STATUS_CODE = new Parameter(null, "COM.system.status.code",
            "COM system status code", "", 0, "");

    public static final Parameter COM_SYSTEM_STATUS_VALUE = new Parameter(null, "COM.system.status.value",
            "COM system status value", "", 0, "");

}
