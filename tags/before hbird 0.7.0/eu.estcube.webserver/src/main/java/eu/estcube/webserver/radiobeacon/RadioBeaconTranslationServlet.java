package eu.estcube.webserver.radiobeacon;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hbird.exchange.core.Named;
import org.springframework.stereotype.Component;

import eu.estcube.webserver.json.ToJsonProcessor;

@SuppressWarnings("serial")
@Component
public class RadioBeaconTranslationServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException
    {

        ToJsonProcessor toJson = new ToJsonProcessor();

        String datetime = request.getParameter("datetime");
        String source = request.getParameter("source");
        String data = request.getParameter("data");

        RadioBeaconTranslator translator = new RadioBeaconTranslator();
        HashMap<String, Named> parameters = null;

        try {
            parameters = translator.toParameters(data, new RadioBeaconDateInputParser().parse(datetime), source);
        } catch (Exception e) {
            e.printStackTrace();
            // @TODO handle?
        }

        if (parameters != null) {
            response.getWriter().write(toJson.process(parameters));
        } else {
            response.getWriter().write("Error parsing message to parameters");
        }

    }
}