package eu.estcube.webserver.radiobeacon.parser;

import org.hbird.exchange.core.Parameter;

import eu.estcube.domain.JMSConstants;

public class RadionBeaconMessageParserInteger extends RadioBeaconMessageParser {

	protected Integer startIndex;

	protected Integer endIndex;

	protected Parameter parameterBase;

	private Integer result;

	public RadionBeaconMessageParserInteger(Integer startIndex, Integer length, Parameter parameterBase) {
		this.startIndex = startIndex;
		this.endIndex = startIndex + length;
		this.parameterBase = parameterBase;
	}

	public String parse(String message) {
		String part = getValidStringPart(message, this.startIndex, this.endIndex);
		if (part == null) {
			result = null;
			return null;
		}
		result = Integer.parseInt(cwToHex(part), JMSConstants.HEX_BASE);
		return String.valueOf(result);
	}

	public Parameter parseToNamed(String message) {
		parse(message);
		if (result == null) {
			return null;
		}
		Parameter parameter = new Parameter(parameterBase);
		parameter.setValue(result);
		return parameter;
	}
}
