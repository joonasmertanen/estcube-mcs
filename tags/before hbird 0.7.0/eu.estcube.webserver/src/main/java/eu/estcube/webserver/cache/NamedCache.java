package eu.estcube.webserver.cache;

import org.hbird.exchange.core.Named;

public class NamedCache<T extends Named> extends AbstractCache<String, T> {

    /**
     * Creates new NamedCache.
     * 
     * @param store
     */
    public NamedCache(Cache<String, T> store) {
        super(store);
    }

    /** @{inheritDoc . */
    @Override
    protected boolean shouldReplace(Named oldValue, Named newValue) {
        return oldValue.getTimestamp() <= newValue.getTimestamp();
    }

    /** @{inheritDoc . */
    @Override
    protected String getKey(T named) {
        return named.getName();
    }
}