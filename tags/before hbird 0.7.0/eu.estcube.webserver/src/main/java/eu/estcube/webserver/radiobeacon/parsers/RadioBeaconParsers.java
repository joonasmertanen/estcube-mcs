package eu.estcube.webserver.radiobeacon.parsers;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.hbird.exchange.core.Named;

import eu.estcube.webserver.radiobeacon.parser.RadioBeaconMessageParser;
import eu.estcube.webserver.radiobeacon.parser.RadionBeaconMessageParserTimestamp;

public abstract class RadioBeaconParsers {

    abstract public List<RadioBeaconMessageParser> getParsers();

    abstract public RadionBeaconMessageParserTimestamp getTimestampParser();

    public HashMap<String, Named> parse(String message, Long timestamp, String issuedBy) {

        HashMap<String, Named> list = new LinkedHashMap<String, Named>();
        for (RadioBeaconMessageParser parser : getParsers()) {
            Named named = parser.parseToNamed(message);
            if (named != null) {
                named.setIssuedBy(issuedBy);
                named.setTimestamp(timestamp);

                list.put(named.getName(), named);
            }
        }

        return list;
    }

}
