package eu.estcube.webserver.radiobeacon;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RadioBeaconDateInputParser {

    public Long parse(String datetime) {

        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            date = format.parse(datetime);
        } catch (Exception e) {
            // e.printStackTrace();
        }

        if (date != null) {
            return (long) (date.getTime() / 1000);
        }

        return null;
    }
}
