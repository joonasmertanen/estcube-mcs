package eu.estcube.webserver.radiobeacon.engineeredValueParsers;

import org.hbird.exchange.core.Parameter;

public class RadioBeaconCalibratorLinear extends RadioBeaconCalibrator {

    private double multiplier;

    private double add;

    public RadioBeaconCalibratorLinear(double multiplier, double add) {
        this.multiplier = multiplier;
        this.add = add;
    }

    public Parameter calibrate(Parameter rawValue) {
        Parameter parameter = new Parameter(rawValue);
        double newValue = rawValue.asDouble() * multiplier + add;
        parameter.setValue(newValue);
        return null;
    }

    public String calibrate(String rawValue) {
        double newValue = Double.parseDouble(rawValue) * multiplier + add;
        return String.valueOf(newValue);
    }
}
