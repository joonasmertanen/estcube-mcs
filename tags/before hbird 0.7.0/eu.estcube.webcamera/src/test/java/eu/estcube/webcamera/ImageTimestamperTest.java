package eu.estcube.webcamera;

import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.longThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.common.Headers;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ImageTimestamperTest {

    private static final Logger LOG = LoggerFactory.getLogger(ImageTimestamperTest.class);

    private static final String GS_ID = "ES5EC";

    private static final String FORMAT = "jpeg";

    private ImageTimestamper stamper;

    @Mock
    private Exchange exchange;

    @Mock
    private Message in;

    @Mock
    private Message out;

    private InputStream inputStream;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        stamper = new ImageTimestamper();
        stamper.groundStationId = GS_ID;
        stamper.format = FORMAT;
        inputStream = getClass().getResourceAsStream("/logo.jpg");
        inOrder = inOrder(exchange, in, out);
    }

    /**
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
        if (inputStream != null) {
            inputStream.close();
        }
    }

    /**
     * Test method for
     * {@link eu.estcube.webcamera.ImageTimestamper#process(org.apache.camel.Exchange)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testProcess() throws Exception {
        when(exchange.getIn()).thenReturn(in);
        when(in.getBody(InputStream.class)).thenReturn(inputStream);
        when(exchange.getOut()).thenReturn(out);
        stamper.process(exchange);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(in, times(1)).getBody(InputStream.class);
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).setHeader(eq(Headers.TIMESTAMP), longThat(new ArgumentMatcher<Long>() {
            @Override
            public boolean matches(Object argument) {
                long timestamp = (Long) argument;
                long now = System.currentTimeMillis();
                long limit = 30 * 1000; // 30 seconds should be fine?
                boolean valid = timestamp <= now && timestamp >= now - limit;
                if (!valid) {
                    LOG.warn("Validating timestamp failed! Timestamp: {}; now: {}; limit: {}", new Object[] {
                            timestamp, now, limit });
                }
                return valid;
            }
        }));
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).setHeader(Headers.TYPE, ImageTimestamper.TYPE_PREFIX + FORMAT);
        inOrder.verify(exchange, times(1)).getOut();
        inOrder.verify(out, times(1)).setBody(argThat(new ArgumentMatcher<Object>() {
            @Override
            public boolean matches(Object argument) {
                byte[] data = (byte[]) argument;
                ByteArrayInputStream bais = new ByteArrayInputStream(data);
                try {
                    ImageIO.read(bais);
                } catch (IOException e) {
                    return false;
                } finally {
                    if (bais != null) {
                        try {
                            bais.close();
                        } catch (IOException e) {
                            // ignore
                        }
                    }
                }
                return true;
            }
        }), eq(byte[].class));
        inOrder.verifyNoMoreInteractions();
    }
}
