package eu.estcube.webcamera;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.hbird.exchange.core.Binary;
import org.junit.Before;
import org.junit.Test;

import eu.estcube.common.Naming;
import eu.estcube.common.Naming.Base;

/**
 *
 */
public class ImageMessageCreatorTest {

    private static final String GS_ID = "ES5EC";

    private static final String SERVICE_ID = "webcam";

    private static final Long NOW = System.currentTimeMillis();

    private static final String TYPE = "image/png";

    private static final byte[] DATA = new byte[] { 0x04, 0x04, 0x04, 0x0C, 0x00, 0x01, 0x08, 0x05 };

    private ImageMessageCreator imageMessageCreator;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        imageMessageCreator = new ImageMessageCreator();
        imageMessageCreator.groundStationId = GS_ID;
        imageMessageCreator.service = SERVICE_ID;
    }

    /**
     * Test method for
     * {@link eu.estcube.webcamera.ImageMessageCreator#create(long, java.lang.String, byte[])}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testCreate() {
        Binary binary = imageMessageCreator.create(NOW, TYPE, DATA);
        assertNotNull(binary);
        assertNotNull(binary.getUuid());
        assertEquals(
                Naming.createParameterAbsoluteName(Base.GROUND_STATION, GS_ID, ImageMessageCreator.PARAMETER_NAME),
                binary.getName());
        assertEquals(Naming.createDataSetIdentifier(Base.GROUND_STATION, GS_ID, NOW), binary.getDatasetidentifier());
        assertEquals(TYPE, binary.getType());
        assertEquals(NOW, new Long(binary.getTimestamp()));
        assertEquals(ImageMessageCreator.DESCRIPTION, binary.getDescription());
        assertEquals(SERVICE_ID, binary.getIssuedBy());
        assertTrue(Arrays.equals(DATA, binary.getRawdata()));
    }
}
