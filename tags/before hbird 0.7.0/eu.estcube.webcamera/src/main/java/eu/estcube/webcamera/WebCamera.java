package eu.estcube.webcamera;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.http.HttpMethods;
import org.apache.camel.spring.Main;
import org.apache.commons.lang3.StringUtils;
import org.hbird.exchange.businesscard.BusinessCard;
import org.hbird.exchange.constants.StandardArguments;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import eu.estcube.common.PrepareForInjection;
import eu.estcube.domain.JMSConstants;

public class WebCamera extends RouteBuilder {

    public static final String PROPERTY_SOCKS_PROXY_HOST = "socksProxyHost";
    public static final String PROPERTY_SOCKS_PROXY_PORT = "socksProxyPort";
    public static final String WEB_CAM_REQUEST_BODY = "Image request";

    private static final Logger LOG = LoggerFactory.getLogger(WebCamera.class);

    @Value("${heart.beat.interval}")
    private int heartBeatInterval;

    @Value("${webcamera.url}")
    private String webCameraUrl;

    @Value("${webcamera.interval}")
    private String webCameraInterval;

    @Value("${service.id}")
    private String serviceId;

    @Value("${service.version}")
    private String serviceVersion;

    @Value("${socks.proxy.host}")
    private String proxyHost;

    @Value("${socks.proxy.port}")
    private String proxyPort;

    @Value("${gs.id}")
    private String gsId;

    @Autowired
    private ImageTimestamper timestamper;

    @Autowired
    private PrepareForInjection preparator;

    @Autowired
    private ImageMessageCreator toBinary;

    @Override
    public void configure() throws Exception {

        MDC.put(StandardArguments.ISSUED_BY, serviceId);

        if (StringUtils.isNotBlank(proxyHost) && StringUtils.isNotBlank(proxyPort)) {
            // for usage with SSH tunnel, outside of UT network
            LOG.debug("Using proxy {}: {}; {}:{};", new Object[] { PROPERTY_SOCKS_PROXY_HOST, proxyHost,
                    PROPERTY_SOCKS_PROXY_PORT, proxyPort });
            System.setProperty(PROPERTY_SOCKS_PROXY_HOST, proxyHost);
            System.setProperty(PROPERTY_SOCKS_PROXY_PORT, proxyPort);
        }

        // @formatter:off

        from("timer://foo?fixedRate=true&period=" + webCameraInterval)
            .process(new Processor() {
                public void process(Exchange ex) throws Exception {
                    // no reply when body is null
                    ex.getOut().setBody(WEB_CAM_REQUEST_BODY, String.class);
                }
            })
            .setHeader(Exchange.HTTP_METHOD, HttpMethods.POST)
            .to(webCameraUrl)
            .process(timestamper)
            .bean(toBinary)
            .process(preparator)
            .to("log:eu.estcube.webcam.stats?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
            .to(JMSConstants.AMQ_WEBCAM_SEND);

        BusinessCard card = new BusinessCard(serviceId, heartBeatInterval);
        card.setDescription(String.format("Web camera transmitter for %s; version: %s", gsId, serviceVersion));
        from("timer://heartbeat?fixedRate=true&period=" + heartBeatInterval)
            .bean(card)
            .process(preparator)
            .to("activemq:topic:hbird.monitoring");

        // @formatter:on
    }

    public static void main(String[] args) throws Exception {
        LOG.info("Starting Webcamera transmitter");
        new Main().run(args);
    }
}