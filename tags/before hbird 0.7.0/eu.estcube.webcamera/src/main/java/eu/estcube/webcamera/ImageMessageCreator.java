package eu.estcube.webcamera;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.hbird.exchange.core.Binary;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.estcube.common.Headers;
import eu.estcube.common.Naming;
import eu.estcube.common.Naming.Base;

/**
 *
 */
@Component
public class ImageMessageCreator {

    public static final String PARAMETER_NAME = "webcam.image";
    public static final String DESCRIPTION = "Image from groun station webcam";

    @Value("${service.id}")
    protected String service;

    @Value("${gs.id}")
    protected String groundStationId;

    public Binary create(@Header(Headers.TIMESTAMP) long timestamp, @Header(Headers.TYPE) String type, @Body byte[] data) {
        String name = Naming.createParameterAbsoluteName(Base.GROUND_STATION, groundStationId, PARAMETER_NAME);
        String dataSetId = Naming.createDataSetIdentifier(Base.GROUND_STATION, groundStationId, timestamp);
        Binary result = new Binary(service, name, type, DESCRIPTION, data);
        result.setDatasetidentifier(dataSetId);
        result.setTimestamp(timestamp);
        return result;
    }
}
