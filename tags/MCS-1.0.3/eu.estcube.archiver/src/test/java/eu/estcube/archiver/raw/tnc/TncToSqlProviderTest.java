package eu.estcube.archiver.raw.tnc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.archiver.raw.sql.MessageToSqlFrameDataProvider;
import eu.estcube.domain.transport.Direction;
import eu.estcube.domain.transport.tnc.TncFrame;
import eu.estcube.domain.transport.tnc.TncFrame.TncCommand;

@RunWith(MockitoJUnitRunner.class)
public class TncToSqlProviderTest {
	private static final long TIMESTAMP = System.currentTimeMillis();
	private static final String SAT = "SAT1";
	private static final TncCommand COMMAND = TncCommand.TX_DELAY;
	private static final int TARGET = 654;
	private static final byte[] DATA = new byte[] { 17, 18 };

	private TncToSqlProvider provider;
	private TncFrame frame;
	private Map<String, Object> headers;

	@Mock
	private Message message;

	@Before
	public void setUp() throws Exception {
		provider = new TncToSqlProvider(Direction.DOWN, message);

		headers = new HashMap<String, Object>();
		headers.put(MessageToSqlFrameDataProvider.HDR_RECEPTION_TIME, TIMESTAMP);
		headers.put(MessageToSqlFrameDataProvider.HDR_SATELLITE, SAT);

		frame = new TncFrame(COMMAND, TARGET, DATA);

		Mockito.when(message.getBody(TncFrame.class)).thenReturn(frame);
		Mockito.when(message.getHeaders()).thenReturn(headers);
	}

	@Test
	public void testTncToSqlProviderDirectionMessage() {
		assertSame(message, provider.getMessage());
		assertEquals(Direction.DOWN, provider.getDirection());
	}

	@Test
	public void testTncToSqlProviderDirection() {
		TncToSqlProvider p = new TncToSqlProvider(Direction.DOWN);
		assertEquals(Direction.DOWN, p.getDirection());
	}

	@Test
	public void testGetValue() {
		assertEquals(
				new Long(TIMESTAMP),
				provider.getValue(MessageToSqlFrameDataProvider.COL_RECEPTION_TIME));

		assertEquals(SAT,
				provider.getValue(MessageToSqlFrameDataProvider.COL_SATELLITE));

		assertTrue(Arrays.equals(DATA,
				(byte[]) provider.getValue(TncToSqlProvider.COL_DATA)));

		assertEquals(TARGET, provider.getValue(TncToSqlProvider.COL_TARGET));
		assertEquals(COMMAND.getValue(),
				provider.getValue(TncToSqlProvider.COL_COMMAND));

	}

}
