package eu.estcube.archiver.raw.sql;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.domain.transport.Direction;

@RunWith(MockitoJUnitRunner.class)
public class MessageToSqlFrameDataProviderTest {
	private MessageToSqlFrameDataProvider provider;
	private Map<String, Object> headers;

	@Mock
	private Message message;

	@Before
	public void setUp() throws Exception {
		provider = new MessageToSqlFrameDataProvider(Direction.DOWN, message) {
			@Override
			public Object getValue(String column) {
				return null;
			}
		};

		headers = new HashMap<String, Object>();
		Mockito.when(message.getHeaders()).thenReturn(headers);
	}

	@Test
	public void testMessageToSqlFrameDataProviderDirection() {
		MessageToSqlFrameDataProvider p = new MessageToSqlFrameDataProvider(
				Direction.DOWN) {
			@Override
			public Object getValue(String column) {
				return null;
			}
		};
		assertEquals(Direction.DOWN, p.getDirection());
	}

	@Test
	public void testMessageToSqlFrameDataProviderDirectionMessage() {
		assertSame(message, provider.getMessage());
		assertEquals(Direction.DOWN, provider.getDirection());

	}

	@Test
	public void testSetDirection() {
		assertEquals(Direction.DOWN, provider.getDirection());
		provider.setDirection(Direction.UP);
		assertEquals(Direction.UP, provider.getDirection());
	}

	@Test
	public void testSetMessage() {
		assertSame(message, provider.getMessage());

		Message m = Mockito.mock(Message.class);
		provider.setMessage(m);
		assertSame(m, provider.getMessage());
	}

	@Test
	public void testGetDetails() {
		headers.put("h1", "h1value");
		headers.put("h2", 16);
		headers.put("h3", null);

		Map<String, String> m = provider.getDetails();
		assertEquals(3, m.size());
		assertEquals("h1value", m.get("h1"));
		assertEquals("16", m.get("h2"));
		assertEquals(MessageToSqlFrameDataProvider.NULL, m.get("h3"));
	}
}
