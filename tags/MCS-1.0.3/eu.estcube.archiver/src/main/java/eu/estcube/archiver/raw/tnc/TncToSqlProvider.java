package eu.estcube.archiver.raw.tnc;

import java.util.Map;

import org.apache.camel.Message;

import eu.estcube.archiver.raw.sql.MessageToSqlFrameDataProvider;
import eu.estcube.domain.transport.Direction;
import eu.estcube.domain.transport.tnc.TncFrame;

public class TncToSqlProvider extends MessageToSqlFrameDataProvider {
	public static final String COL_COMMAND = "command";
	public static final String COL_DATA = "data";
	public static final String COL_TARGET = "target";

	public TncToSqlProvider(Direction direction, Message message) {
		super(direction, message);
	}

	public TncToSqlProvider(Direction direction) {
		super(direction);
	}

	@Override
	public Object getValue(String column) {
		Message m = getMessage();
		TncFrame f = m.getBody(TncFrame.class);
		Map<String, Object> h = m.getHeaders();

		if (COL_RECEPTION_TIME.equals(column)) {
			return h.get(HDR_RECEPTION_TIME);
		} else if (COL_SATELLITE.equals(column)) {
			return h.get(HDR_SATELLITE);
		} else if (COL_COMMAND.equals(column)) {
			return f.getCommand().getValue();
		} else if (COL_DATA.equals(column)) {
			return f.getData();
		} else if (COL_TARGET.equals(column)) {
			return f.getTarget();
		}
		return null;
	}
}
