package eu.estcube.archiver.raw.ax25;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import javax.sql.DataSource;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.archiver.raw.ArchivingException;
import eu.estcube.archiver.raw.FrameRetriever;
import eu.estcube.common.ByteUtil;
import eu.estcube.common.queryParameters.AX25Object;
import eu.estcube.common.queryParameters.AX25QueryParameters;


public class Ax25Retriever extends FrameRetriever {

    public Ax25Retriever(DataSource source) {
        super(source);
    }

    private Logger LOG = LoggerFactory.getLogger(Ax25Retriever.class);

    private final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    private final TimeZone utc = TimeZone.getTimeZone("UTC");

    private String createQuery(AX25QueryParameters params) {
        StringBuilder sb = new StringBuilder();
        if (params.getSubsystem() != Integer.MIN_VALUE) {
        	String subSys;
        	if(params.getSubsystem() < 10){
        		subSys = "0"+ params.getSubsystem();
        	}else subSys = params.getSubsystem()+"";
            String direction = params.getDirection();
            if(!direction.isEmpty()){
            	
                if(direction.equals( "UP")){
                    sb.append(" AND info LIKE '06")
                    .append(subSys)
                    .append("%'");
                }else if (direction.equals("DOWN")){
                    sb.append(" AND info LIKE '")
                    .append(subSys)
                    .append("06%'");
                }
                
            }else{
                sb.append(" AND (info LIKE '")
                .append(subSys)
                .append("06%' OR info LIKE '06")
                .append(subSys)
                .append("%')");
            }
        }
       String query = super.createQuery(params);
       query = query.replace("ReplaceInSubclass", sb.toString());
      
        return query;
    }


    @Override
    public void retrieve(Exchange exchange) throws ArchivingException, SQLException {
        try {
			AX25QueryParameters params = (AX25QueryParameters) exchange.getIn().getBody();
			LOG.info("Ax25Retreiver in parameters" + params.toString());
			String query = createQuery(params);
			LOG.info("Ax25Retreiver query" + query);

	    	  df.setTimeZone(utc);
	    	  
			super.retrieve(exchange);
			Statement statement = c.createStatement();
			ResultSet resultSet = statement.executeQuery(query);
			
			//get metadata
			ResultSetMetaData meta = null;
			meta = resultSet.getMetaData();

			//get column names
			int colCount = meta.getColumnCount();
			ArrayList<String> cols = new ArrayList<String>();
			for (int i=1; i<=colCount; i++){
			  cols.add(meta.getColumnName(i));
			}
			//fetch out rows
			ArrayList<AX25Object> rows = new ArrayList<AX25Object>();

			//make new object
			while (resultSet.next()) {
			  AX25Object row = new AX25Object();
			  row.setRequestId(params.getReqId());
			  for (String colName:cols) {
			      if (resultSet.getObject(colName) == null);
			      else if (colName.equals("ID")){
			          row.setId((resultSet.getInt(colName)));
			      }else if (colName.equals("DIRECTION")){
			          row.setDirection(resultSet.getString(colName));
			      }else if (colName.equals("RECEPTION_TIME")){
			    	  Date date = new Date(Long.parseLong(resultSet.getString(colName)));
			          String s = df.format(date);
			          row.setReceptionTime(s); 
			      }else if (colName.equals("SATELLITE")){
			          row.setSatellite(resultSet.getString(colName)); 
			      }else if (colName.equals("DESTADDR")){
			          row.setDestAddr(ByteUtil.toHexString(resultSet.getBytes(colName))); 
			      }else if (colName.equals("SRCADDR")){
			          row.setSrcAddr(ByteUtil.toHexString(resultSet.getBytes(colName))); 
			      }else if (colName.equals("CTRL")){
			          row.setCtrl(Integer.parseInt(resultSet.getString(colName))); 
			      }else if (colName.equals("PID")){
			          row.setPid(Integer.parseInt(resultSet.getString(colName))); 
			      }else if (colName.equals("INFO")){
			          row.setInfo((ByteUtil.toHexString(resultSet.getBytes(colName)))); 
			      }else if (colName.equals("FCS")){
			          row.setFcs(resultSet.getString(colName)); 
			      }else if (colName.equals("ERROR_BITMASK")){
			          row.setErrorBitmask(Integer.parseInt(resultSet.getString(colName))); 
			      }else if (colName.equals("CREATED")){
			          long mili= resultSet.getTimestamp(colName).getTime();
			          String s = df.format(new Date(mili));
			          row.setCreated(s); 
			      }else if (colName.equals("VALUE")){
                  	String[] array = resultSet.getString(colName).split("/");
                    row.setTarget(array[array.length-1]);
			      }
			  }
			  rows.add(row);
			}
			LOG.info("Result: " + rows.size());
			exchange.getOut().setHeader("RequestId", params.getReqId());
			exchange.getOut().setBody(rows);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{

	        c.close();

		}
    }

    @Override
    protected String getTable() {
        return AX25QueryParameters.getTable();
    }



    @Override
    protected String getDetailsTable() {
        return AX25QueryParameters.getDetailsTable();
    }



}
