package eu.estcube.archiver.raw.beacon;

import java.net.UnknownHostException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.sql.DataSource;

import org.apache.camel.Exchange;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import eu.estcube.archiver.raw.ArchivingException;
import eu.estcube.archiver.raw.FrameRetriever;
import eu.estcube.common.queryParameters.BeaconObject;
import eu.estcube.common.queryParameters.BeaconQueryParameters;
import eu.estcube.common.queryParameters.OracleQueryParameters;
import eu.estcube.common.queryParameters.TNCObject;

public class BeaconRetriever extends FrameRetriever {

	 private final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	 private final TimeZone utc = TimeZone.getTimeZone("UTC");
    public BeaconRetriever(DataSource source) {
        super(source);
    }

    @Override
    public void retrieve(Exchange exchange) throws ArchivingException, SQLException {

    	try {
			MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
			DB db = mongoClient.getDB( "hbird" );
		    df.setTimeZone(utc);
			DBCollection coll = db.getCollection(getTable());
			BeaconQueryParameters params = (BeaconQueryParameters)exchange.getIn().getBody();
			DBCursor cursor = coll.find(createQuery(params));
			ArrayList<BeaconObject> rows = new ArrayList<BeaconObject>();
			while(cursor.hasNext()) {
				BeaconObject obj = new BeaconObject();
				DBObject o = cursor.next();
				obj.setRequestId(params.getReqId());
				Date date = new Date((Long)o.get("timestamp"));
			    String s = df.format(date);
				obj.setTimestamp(s);
				obj.setInstertedBy((String)o.get("insertedBy"));
				obj.setIssuedBy((String)o.get("issuedBy"));
				obj.setName((String)o.get("name"));
				obj.setValue((String)o.get("value"));
				obj.setVersion((Long)o.get("version"));
				rows.add(obj);
			}
			exchange.getOut().setHeader("RequestId", params.getReqId());
	        exchange.getOut().setBody(rows);
    	} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    protected String getTable() {
        return BeaconQueryParameters.getTable();
    }

    @Override
    protected String getDetailsTable() {
        return BeaconQueryParameters.getDetailsTable();
    }

    
	protected BasicDBObject createQuery(BeaconQueryParameters params) {
    	Map<String, Object> searchParams = new HashMap<String, Object>();
    	searchParams.put("name", getDetailsTable());
    	searchParams.put("timestamp", new BasicDBObject("$gt", params.getStart().getMillis()).append("$lt", params.getEnd().getMillis()));
    	if(!params.getInstertedBy().isEmpty())
    		searchParams.put("insertedBy", params.getInstertedBy());
    	if(!params.getIssuedBy().isEmpty())
    		searchParams.put("issuedBy", params.getIssuedBy());
    	BasicDBObject query = new BasicDBObject(searchParams);
		
        return query;
    }

}
