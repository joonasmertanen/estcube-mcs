package eu.estcube.archiver.raw.tnc;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import javax.sql.DataSource;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.archiver.raw.ArchivingException;
import eu.estcube.archiver.raw.FrameRetriever;
import eu.estcube.archiver.raw.ax25.Ax25Retriever;
import eu.estcube.common.ByteUtil;
import eu.estcube.common.queryParameters.TNCObject;
import eu.estcube.common.queryParameters.TncQueryParameters;

public class TncRetriever extends FrameRetriever {
    public TncRetriever(DataSource source) {
        super(source);
    }

    private Logger LOG = LoggerFactory.getLogger(Ax25Retriever.class);

    private final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    private final TimeZone utc = TimeZone.getTimeZone("UTC");

    private String createQuery(TncQueryParameters params) {
        StringBuilder sb = new StringBuilder();
        
        if (params.getCommand() != Integer.MIN_VALUE) {
            sb.append(" AND command = ")
                    .append(params.getCommand());
        }
        String query = super.createQuery(params);
        query = query.replace("ReplaceInSubclass", sb.toString());

        return query;
    }

    @Override
    public void retrieve(Exchange exchange) throws ArchivingException, SQLException {
        TncQueryParameters params = (TncQueryParameters) exchange.getIn().getBody();
        LOG.debug("TncRetreiver in parameters" + params.toString());
        String query = createQuery(params);
        LOG.debug("TncRetreiver query" + query);

        super.retrieve(exchange);
        Statement statement = c.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        
        df.setTimeZone(utc);
        
        //get metadata
        ResultSetMetaData meta = null;
        meta = resultSet.getMetaData();
        //get column names
        int colCount = meta.getColumnCount();
        ArrayList<String> cols = new ArrayList<String>();
        for (int i=1; i<=colCount; i++){
          cols.add(meta.getColumnName(i));
        }
        //fetch out rows
        ArrayList<TNCObject> rows = new ArrayList<TNCObject>();

        while (resultSet.next()) {
            TNCObject row = new TNCObject();
			row.setRequestId(params.getReqId());
            for (String colName:cols) {
                try{
                    if (resultSet.getObject(colName) == null);
                    else if (colName.equals("ID")){
                        row.setId((resultSet.getInt(colName)));
                    }else if (colName.equals("DIRECTION")){
                        row.setDirection(resultSet.getString(colName));
                    }else if (colName.equals("RECEPTION_TIME")){
                    	 Date date = new Date(Long.parseLong(resultSet.getString(colName)));
	   			          String s = df.format(date);
	   			          row.setReceptionTime(s);             
	   			    }else if (colName.equals("SATELLITE")){
                        row.setSatellite(resultSet.getString(colName)); 
                    }else if (colName.equals("COMMAND")){
                        row.setCommand((resultSet.getInt(colName)));
                    }else if (colName.equals("DATA")){
                        row.setData((ByteUtil.toHexString(resultSet.getBytes(colName)))); 
                    }else if (colName.equals("VALUE")){
                    	String[] array = resultSet.getString(colName).split("/");
                        row.setTarget(array[array.length-1]);
                    }else if (colName.equals("CREATED")){
  			          long mili= resultSet.getTimestamp(colName).getTime();
  			          String s = df.format(new Date(mili));
  			          row.setCreated(s); 
  			      }
                }finally{
                    
                }
            }
            rows.add(row);
        }
        LOG.info("Result: " + rows.size());

		exchange.getOut().setHeader("RequestId", params.getReqId());
        exchange.getOut().setBody(rows);
        c.close();

    }
    @Override
    protected String getTable() {
        return TncQueryParameters.getTable();
    }

    @Override
    protected String getDetailsTable() {
        return TncQueryParameters.getDetailsTable();
    }

}
