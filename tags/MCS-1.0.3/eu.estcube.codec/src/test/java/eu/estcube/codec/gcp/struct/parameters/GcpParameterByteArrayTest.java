package eu.estcube.codec.gcp.struct.parameters;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilderFactory;

import org.hbird.exchange.core.Binary;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

public class GcpParameterByteArrayTest {

    private final GcpParameterByteArray p = new GcpParameterByteArray("a", "b");

    @Test
    public void GcpParameterBit() throws Exception {

        String xml = "<param><name>test</name><description>a</description><unit>b</unit></param>";
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                .parse(new InputSource(new StringReader(xml)));

        GcpParameterByteArray s = new GcpParameterByteArray((Element) doc.getElementsByTagName("param").item(0));
        assertEquals("test", s.getName());
        assertEquals("a", s.getDescription());
        assertEquals("b", s.getUnit());
    }

    @Test
    public void getLength() {
        assertEquals(-1, p.getLength());
    }

    @Test
    public void toValueStr() {
        assertArrayEquals(new byte[] { 1, 2, 3 }, p.toValue("01    0203"));
        assertArrayEquals(new byte[] { 1, 2, 0x2F }, p.toValue("01022F"));
    }

    @Test
    public void toBytes() {
        assertArrayEquals(new byte[] { 1, 2, 3 }, p.toBytes(new byte[] { 1, 2, 3 }));
    }

    @Test
    public void toValueByte() {
        assertArrayEquals(new byte[] { 1, 2, 3 }, p.toValue(new byte[] { 1, 2, 3 }));
    }

    @Test
    public void getIssued() {
        Binary parameter = p.getIssued(new byte[] { 0x01, -1 }, System.currentTimeMillis());
        assertEquals("a", parameter.getName());
        assertNull(parameter.getID());
        assertEquals("b", parameter.getDescription());
        assertEquals(null, parameter.getIssuedBy());
        assertArrayEquals(new byte[] { 0x01, -1 }, parameter.getRawData());
    }

    @Test
    public void getValueClass() {
        assertEquals(byte[].class, p.getValueClass());
    }
}
