package eu.estcube.codec.gcp.struct;

import org.w3c.dom.Element;

public class GcpSubsystem {

    private String name;

    public GcpSubsystem(String name) {
        setName(name);
    }

    public GcpSubsystem(Element node) {
        setName(node.getTextContent());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
