package eu.estcube.commandautomation.automator.event;

public interface OnCommandHandlerFinished {

    public void execute(String result);

}
