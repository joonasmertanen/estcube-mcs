package eu.estcube.commandautomation.processor;

import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.commandautomation.automator.Automator;

@Component
public class AosEventProcessor extends AbstractTrackingEventProcessor {

    @Autowired
    private Automator automator;

    @Override
    public void process(Exchange exchange) throws Exception {
        automator.start((getTrackingId(exchange)));
    }
}
