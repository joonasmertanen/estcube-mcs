package eu.estcube.gs.tnc.processor;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import eu.estcube.domain.transport.tnc.TncFrame;
import eu.estcube.domain.transport.tnc.TncFrame.TncCommand;

/**
 *
 */
public class TncFrameFromFileCreatorTest {

    private static final byte[] BYTES = new byte[] { 0x03, 0x05, 0x08, 0x0C, 0x0E, 0x00, 0x04, 0x0A };
    
    private TncFrameFromFileCreator processor;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        processor = new TncFrameFromFileCreator();
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.processor.TncFrameFromFileCreator#createFrame(java.lang.String, byte[])}.
     * @throws Exception 
     */
    @Test
    public void testCreateFrame() throws Exception {
        TncFrame frame = processor.createFrame("A", BYTES);
        assertEquals(TncFrameFromFileCreator.DEFAULT_PORT, frame.getTarget());
        assertEquals(TncCommand.DATA, frame.getCommand());
        assertTrue(Arrays.equals(BYTES, frame.getData()));
        
        frame = processor.createFrame("dir" + File.separator + (TncFrameFromFileCreator.MIN_PORT - 1), BYTES);
        assertEquals(TncFrameFromFileCreator.DEFAULT_PORT, frame.getTarget());
        assertEquals(TncCommand.DATA, frame.getCommand());
        assertTrue(Arrays.equals(BYTES, frame.getData()));
        
        frame = processor.createFrame("dir" + File.separator +  (TncFrameFromFileCreator.MAX_PORT + 1), BYTES);
        assertEquals(TncFrameFromFileCreator.DEFAULT_PORT, frame.getTarget());
        assertEquals(TncCommand.DATA, frame.getCommand());
        assertTrue(Arrays.equals(BYTES, frame.getData()));
        
        for (int i = TncFrameFromFileCreator.MIN_PORT; i  < TncFrameFromFileCreator.MAX_PORT; i++) {
            frame = processor.createFrame("dir" + File.separator + i, BYTES);
            assertEquals(i, frame.getTarget());
            assertEquals(TncCommand.DATA, frame.getCommand());
            assertTrue(Arrays.equals(BYTES, frame.getData()));
        }
    }
}
