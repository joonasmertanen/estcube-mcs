package eu.estcube.gs.tnc.codec;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.common.ByteUtil;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TncEscapeEncoderTest {

    @Mock
    private IoSession session;
    
    @Mock
    private ProtocolEncoderOutput out;
    
    private TncEscapeEncoder encoder;
    
    private InOrder inOrder;
    
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        encoder = new TncEscapeEncoder();
        inOrder = inOrder(session, out);
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncEscapeEncoder#encode(org.apache.mina.core.session.IoSession, java.lang.Object, org.apache.mina.filter.codec.ProtocolEncoderOutput)}.
     * @throws Exception 
     */
    @Test(expected = RuntimeException.class)
    public void testEncodeWithExcpetion() throws Exception {
        encoder.encode(session, new Object(), out);
    }
    
    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncEscapeEncoder#encode(org.apache.mina.core.session.IoSession, java.lang.Object, org.apache.mina.filter.codec.ProtocolEncoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testEncodeFend() throws Exception {
        byte[] bytes = new byte[] { TncConstants.FEND };
        encoder.encode(session, bytes, out);
        ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        inOrder.verify(out, times(1)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        byte[] result = captor.getValue();
        assertTrue(ByteUtil.toHexString(result), Arrays.equals(new byte[] { TncConstants.FESC, TncConstants.TFEND }, result));
    }
    
    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncEscapeEncoder#encode(org.apache.mina.core.session.IoSession, java.lang.Object, org.apache.mina.filter.codec.ProtocolEncoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testEncodeFesc() throws Exception {
        byte[] bytes = new byte[] { TncConstants.FESC };
        encoder.encode(session, bytes, out);
        ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        inOrder.verify(out, times(1)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        byte[] result = captor.getValue();
        assertTrue(ByteUtil.toHexString(result), Arrays.equals(new byte[] { TncConstants.FESC, TncConstants.TFESC }, result));
    }

    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncEscapeEncoder#encode(org.apache.mina.core.session.IoSession, java.lang.Object, org.apache.mina.filter.codec.ProtocolEncoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testEncodeTfend() throws Exception {
        byte[] bytes = new byte[] { TncConstants.TFEND };
        encoder.encode(session, bytes, out);
        ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        inOrder.verify(out, times(1)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        byte[] result = captor.getValue();
        assertTrue(ByteUtil.toHexString(result), Arrays.equals(new byte[] { TncConstants.TFEND }, result));
    }
    
    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncEscapeEncoder#encode(org.apache.mina.core.session.IoSession, java.lang.Object, org.apache.mina.filter.codec.ProtocolEncoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testEncodeTfesc() throws Exception {
        byte[] bytes = new byte[] { TncConstants.TFESC };
        encoder.encode(session, bytes, out);
        ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        inOrder.verify(out, times(1)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        byte[] result = captor.getValue();
        assertTrue(ByteUtil.toHexString(result), Arrays.equals(new byte[] { TncConstants.TFESC }, result));
    }
    
    /**
     * Test method for {@link eu.estcube.gs.tnc.codec.TncEscapeEncoder#encode(org.apache.mina.core.session.IoSession, java.lang.Object, org.apache.mina.filter.codec.ProtocolEncoderOutput)}.
     * @throws Exception 
     */
    @Test
    public void testEncode() throws Exception {
        byte[] bytes = new byte[] { 0x01, TncConstants.TFESC, 0x02, 0x03, TncConstants.FESC, 0x04, TncConstants.FEND, 0x05, TncConstants.TFEND, 0x07 };
        encoder.encode(session, bytes, out);
        ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        inOrder.verify(out, times(1)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        byte[] result = captor.getValue();
        assertTrue(ByteUtil.toHexString(result), Arrays.equals(new byte[] { 0x01, TncConstants.TFESC, 0x02, 0x03, TncConstants.FESC, TncConstants.TFESC, 0x04, TncConstants.FESC, TncConstants.TFEND, 0x05, TncConstants.TFEND, 0x07  }, result));
    }
}
