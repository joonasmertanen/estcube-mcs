package eu.estcube.gs.tnc.codec;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 *
 */
public class TncEscapeCodecFactory implements ProtocolCodecFactory {

    private static final Logger LOG = LoggerFactory.getLogger(TncEscapeCodecFactory.class);
    
    private ProtocolEncoder encoder = new TncEscapeEncoder();
    
    private ProtocolDecoder decoder = new TncEscapeDecoder();
    
    /** @{inheritDoc}. */
    @Override
    public ProtocolEncoder getEncoder(IoSession session) throws Exception {
        LOG.trace(" Calling ESCAPE encoder");
        return encoder;
    }

    /** @{inheritDoc}. */
    @Override
    public ProtocolDecoder getDecoder(IoSession session) throws Exception {
        LOG.trace(" Calling ESCAPE decoder");
        return decoder;
    }
}
