/** 
 *
 */
package eu.estcube.limitchecking.processors;

import java.io.File;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.hbird.exchange.core.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.thoughtworks.xstream.XStream;

import eu.estcube.limitchecking.data.ParameterLimits;
import eu.estcube.limitchecking.notifiers.EmailNotifier;

/**
 * Calibrates <code>Parameter</code>s in Camel route
 */
@Component
public class ParameterProcessor implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(ParameterProcessor.class);
    private XStream xstream;
    private File limitsSettings;
    private List<ParameterLimits> parameterLimits;

    @Autowired
    private EmailNotifier notifier;

    /**
     * Constructs a new instance of <code>ParameterCalibrationProcessor</code>,
     * loading the calibration expressions from a file specified in VM arguments
     * ("calibration.values")
     * 
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public ParameterProcessor() throws Exception {
        if (!System.getProperties().containsKey("limitchecking.limits")) {
            LOG.error("Calibration file location is not set with \"limitchecking.limits\" in VM arguments");
            return;
        }
        limitsSettings = new File(System.getProperty("limitchecking.limits"));
        xstream = new XStream();
        xstream.alias("parameters", List.class);
        xstream.processAnnotations(ParameterLimits.class);
        parameterLimits = (List<ParameterLimits>) xstream.fromXML(limitsSettings);
    }

    /** @{inheritDoc . */
    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();

        Parameter input = in.getBody(Parameter.class);
        for (ParameterLimits c : parameterLimits) {
            if (c.appliesTo(input)) {
                int currErrLevel;
                if (c.isInSoft(input)) {
                    // ELSE IN SOFT LIMITS
                    currErrLevel = 0;
                } else if (c.isInHard(input)) {
                    // IN HARD LIMITS
                    currErrLevel = 1;
                } else if (c.isInSanity(input)) {
                    // IN SANITY LIMITS
                    currErrLevel = 2;
                } else {
                    // OUT OF LIMITS
                    currErrLevel = 3;
                }
                int prevErrLevel =
                        c.setErrorLevel(currErrLevel);
                if (prevErrLevel != currErrLevel) {
                    notifier.notify(c, input, prevErrLevel, currErrLevel);
                }

                break;
            }
        }

    }

}
