package eu.estcube.limichecking.domain;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.hbird.exchange.core.Parameter;
import org.junit.Before;
import org.junit.Test;

import eu.estcube.limitchecking.data.ParameterLimits;

public class ParameterLimitsWithSanityLimitsTest {

    private String id;
    private String check;
    private boolean checkCalibrated;
    private boolean checkRaw;
    private double[] soft;
    private double[] hard;
    private double[] sanity;
    private String softStr;
    private String hardStr;
    private String sanityStr;
    private boolean inSoft;
    private boolean inHard;
    private boolean inSanity;
    private ParameterLimits limits;
    private Parameter test;
    private Parameter test2;

    @Before
    public void createInfoContainerWithSanityLimits() {

        id = "test2";

        check = "both";
        checkCalibrated = true;
        checkRaw = true;

        soft = new double[] { 40, 50 };
        softStr = "40;50";
        inSoft = false;

        hard = new double[] { 37.2, 65.37 };
        hardStr = "37.2;65.37";
        inHard = false;

        sanity = new double[] { 2.4, 90 };
        sanityStr = "2.4;90";
        inSanity = false;

        test = new Parameter(id, "test param");
        test2 = new Parameter("test1", "another test param");

        test.setValue(2.399999999999);

        limits = new ParameterLimits(id, check, softStr, hardStr, sanityStr);

    }

    @Test
    public void testInfoContainerId() {
        assertEquals(id, limits.getID());
    }

    @Test
    public void testLimits() {
        assertEquals(Arrays.toString(soft), Arrays.toString(limits.getLimits(0)));
        assertEquals(Arrays.toString(hard), Arrays.toString(limits.getLimits(1)));
        assertEquals(Arrays.toString(sanity), Arrays.toString(limits.getLimits(2)));
    }

    @Test
    public void testCheckingData() {
        assertEquals(checkCalibrated, limits.checkCalibrated());
        assertEquals(checkRaw, limits.checkRaw());
    }

    @Test
    public void testLimitChecking() {
        assertEquals(true, limits.appliesTo(test));
        assertEquals(false, limits.appliesTo(test2));

        assertEquals(inSoft, limits.isInSoft(test));
        assertEquals(inHard, limits.isInHard(test));
        assertEquals(inSanity, limits.isInSanity(test));
    }

}
