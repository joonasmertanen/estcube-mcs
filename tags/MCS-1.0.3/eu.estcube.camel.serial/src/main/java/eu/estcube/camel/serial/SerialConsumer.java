package eu.estcube.camel.serial;

import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.impl.DefaultConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 */
public class SerialConsumer extends DefaultConsumer {

	private static final Logger LOG = LoggerFactory
			.getLogger(SerialConsumer.class);

	public static final String SESSION_ID = "serialSessionId";
	public static final String SERIAL_PORT = "serialPortName";
	public static final String MESSAGE_CLASS = "class";

	/**
	 * Creates new SerialConsumer.
	 * 
	 * @param endpoint
	 * @param processor
	 */
	public SerialConsumer(Endpoint endpoint, Processor processor) {
		super(endpoint, processor);
	}

	public void handle(long sid, String port, Object message) throws Exception {
		Exchange ex = getEndpoint().createExchange();
		Message in = ex.getIn();
		setHeaders(in, sid, port, message);
		in.setBody(message);
		getAsyncProcessor().process(ex);
	}

	public void handleProcessingException(long sid, String port,
			Object message, Exception e) {
		LOG.error(
				"SerialConsumer failed to handle incomming message {}; sid: {}; port: {}",
				new Object[] {
						message != null ? message.getClass().getName() : "null",
						sid, port, e });
		sendErrorMessage(sid, port, e, message);
	}

	public void handleCommunicationException(long sid, String port, Throwable t) {
		LOG.error(
				"Communication exception in serial connection; sid: {}; port: {}",
				new Object[] { sid, port, t });
		sendErrorMessage(sid, port, t, null);
	}

	void sendErrorMessage(long sid, String port, Throwable t, Object message) {
		Exchange ex = getEndpoint().createExchange();
		Message in = ex.getIn();
		setHeaders(in, sid, port, message);
		getExceptionHandler().handleException(
				"Failed to handle message from serial port" + port, ex, t);
	}

	/**
	 * @param sid
	 * @param port
	 * @param bytes
	 * @param message
	 */
	void setHeaders(Message message, long sid, String port, Object msg) {
		message.setHeader(SESSION_ID, sid);
		message.setHeader(SERIAL_PORT, port);
		message.setHeader(Exchange.CREATED_TIMESTAMP,
				System.currentTimeMillis());
		message.setHeader("timestamp", System.currentTimeMillis());
		if (msg != null) {
			message.setHeader(MESSAGE_CLASS, msg.getClass());
		}
	}
}
