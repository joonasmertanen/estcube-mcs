package eu.estcube.camel.serial;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderAdapter;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class DefaultCodecFactory implements ProtocolCodecFactory {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultCodecFactory.class);

    private final ProtocolEncoder encoder = createEncoder();

    private final ProtocolDecoder decoder = createDecoder();

    /** @{inheritDoc . */
    @Override
    public ProtocolEncoder getEncoder(IoSession session) throws Exception {
        return encoder;
    }

    /** @{inheritDoc . */
    @Override
    public ProtocolDecoder getDecoder(IoSession session) throws Exception {
        return decoder;
    }

    ProtocolDecoder createDecoder() {
        return new ProtocolDecoderAdapter() {
            @Override
            public void decode(IoSession session, IoBuffer in, ProtocolDecoderOutput out) throws Exception {
                byte[] bytes = new byte[in.remaining()];
                in.get(bytes);
                out.write(bytes);
            }
        };
    }

    ProtocolEncoder createEncoder() {
        return new ProtocolEncoderAdapter() {
            @Override
            public void encode(IoSession session, Object message, ProtocolEncoderOutput out) throws Exception {
                if (!(message instanceof byte[])) {
                    LOG.error("Unsupported message type in {} - {}. Only supported type is byte[]. Throwing exception",
                            getClass().getSimpleName(), message.getClass().getName());
                    throw new RuntimeException("Unsupported message type in " + getClass().getName() + " - "
                            + message.getClass().getName());
                }
                IoBuffer buffer = IoBuffer.wrap((byte[]) message);
                out.write(buffer);
            }
        };
    }
}
