define([
        "dojo/_base/declare", 
        "common/Controller", 
        "./ContactInfoView",
        ],

function(declare, Controller, ContactInfoView) {
	var s = declare([Controller], {
		constructor : function() {
			this.view = new ContactInfoView("ESTCube-1 (Tartu Observatoorium)", "Maailmaaeg", "Aeg stardist");
		},
		index : function(params) {
			this.placeWidget(this.view);
		},
	});
	return new s();
});
