package eu.estcube.weatherstation;

import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_PRESSURE;
import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_TEMPERATURE;
import static eu.estcube.weatherstation.DefaultWeatherParameters.NAME;
import static eu.estcube.weatherstation.DefaultWeatherParameters.PHENOMENON;
import static eu.estcube.weatherstation.DefaultWeatherParameters.PRECIPITATIONS;
import static eu.estcube.weatherstation.DefaultWeatherParameters.RELATIVE_HUMIDITY;
import static eu.estcube.weatherstation.DefaultWeatherParameters.VISIBILITY;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WIND_DIRECTION_PARAM;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WIND_SPEED;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WIND_SPEED_MAX;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WMO_CODE;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.interfaces.IEntityInstance;
import org.jdom2.Document;
import org.jdom2.Element;

public class EmhiHandlerImpl extends AbstractDataHandler {

    /**
     * Creates new EmhiHandlerImpl.
     * 
     * @param idBuilder
     * @param serviceId
     */
    public EmhiHandlerImpl(IdBuilder idBuilder, String serviceId, String parameterNamespace) {
        super(idBuilder, serviceId, parameterNamespace);
    }

    protected String getDataSourceName() {
        return "emhi.ee";
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    protected Map<String, NamedConfig<? extends IEntityInstance>> getParameterMapping() {
        Map<String, NamedConfig<? extends IEntityInstance>> map = new HashMap<String, NamedConfig<? extends IEntityInstance>>();

        map.put("phenomenon", new NamedConfig(PHENOMENON, new LabelFactory()));
        map.put("visibility", new NamedConfig(VISIBILITY, new DoubleParameterFactory()));
        map.put("precipitations", new NamedConfig(PRECIPITATIONS, new DoubleParameterFactory()));
        map.put("airpressure", new NamedConfig(AIR_PRESSURE, new DoubleParameterFactory()));
        map.put("relativehumidity", new NamedConfig(RELATIVE_HUMIDITY, new DoubleParameterFactory()));
        map.put("airtemperature", new NamedConfig(AIR_TEMPERATURE, new DoubleParameterFactory()));
        map.put("winddirection", new NamedConfig(WIND_DIRECTION_PARAM, new DoubleParameterFactory()));
        map.put("windspeed", new NamedConfig(WIND_SPEED, new DoubleParameterFactory()));
        map.put("windspeedmax", new NamedConfig(WIND_SPEED_MAX, new DoubleParameterFactory()));
        map.put("name", new NamedConfig(NAME, new LabelFactory()));
        map.put("wmocode", new NamedConfig(WMO_CODE, new IntegerParameterFactory()));

        return map;
    }

    /** @{inheritDoc . */
    @Override
    protected Element getParentNodeForWeatherData(Document document) {
        Element rootNode = document.getRootElement();
        List<Element> list = rootNode.getChildren("station");
        for (int u = 0; u < list.size(); u++) {
            Element node = (Element) list.get(u);
            if (node.getChildText("wmocode").equals("26242")) {
                return node;
            }
        }
        throw new IllegalArgumentException("Unable to parse given XML");
    }

    /** @{inheritDoc . */
    @Override
    protected Date parseDate(Document document) throws IllegalArgumentException {
        String timestamp = document.getRootElement().getAttributeValue("timestamp");
        return new Date(Long.parseLong(timestamp) * 1000L);
    }
}
