package eu.estcube.gs;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.hbird.exchange.constants.StandardArguments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.common.TimestampExtractor;
import eu.estcube.gs.contact.LocationContactEventHolder;

/**
 *
 */
@Component
public class GsAddHeaders implements Processor {

    public static final String DEFAULT_CONTACT_ID = "unknown";
    public static final long DEFAULT_ORBIT_NUMBER = -1L;

    @Autowired
    private GsDriverConfiguration config;

    @Autowired
    private LocationContactEventHolder eventHolder;

    @Autowired
    private TimestampExtractor timestampExtractor;

    /** @{inheritDoc . */
    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        out.copyFrom(in);

        long timestamp = timestampExtractor.getTimestamp(in);
        String eventId = eventHolder.getContactId(timestamp);
        long orbitNumber = eventHolder.getOrbitNumber(timestamp);
        if (eventId == null) {
            eventId = DEFAULT_CONTACT_ID;
            orbitNumber = DEFAULT_ORBIT_NUMBER;
        }

        out.setHeader(StandardArguments.TIMESTAMP, timestamp);
        out.setHeader(StandardArguments.ISSUED_BY, config.getServiceId());
        out.setHeader(StandardArguments.SATELLITE_ID, config.getSatelliteId());
        out.setHeader(StandardArguments.GROUND_STATION_ID, config.getGroundstationId());
        out.setHeader(StandardArguments.CONTACT_ID, eventId);
        out.setHeader(StandardArguments.ORBIT_NUMBER, orbitNumber);
        out.setHeader(StandardArguments.CLASS, in.getBody().getClass().getSimpleName());
    }
}
