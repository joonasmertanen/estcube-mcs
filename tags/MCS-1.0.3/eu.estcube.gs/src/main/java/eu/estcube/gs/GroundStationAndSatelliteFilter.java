/**
 *
 */
package eu.estcube.gs;

import org.hbird.exchange.navigation.LocationContactEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class GroundStationAndSatelliteFilter {

    @Autowired
    private GsDriverConfiguration config;

    public boolean matches(LocationContactEvent event) {
        return config.getGroundstationId().equals(event.getGroundStationID())
                && config.getSatelliteId().equals(event.getSatelliteID());
    }
}
