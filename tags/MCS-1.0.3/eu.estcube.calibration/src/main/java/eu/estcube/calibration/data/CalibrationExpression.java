package eu.estcube.calibration.data;

import org.hbird.exchange.core.CalibratedParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.congrace.exp4j.Calculable;
import de.congrace.exp4j.ExpressionBuilder;
import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;

/**
 * Holds calibration expression, unit and decimal format data for specified
 * parameter.
 * 
 * @author Gregor Eesmaa
 * 
 */
public class CalibrationExpression {

    private static final Logger LOG = LoggerFactory.getLogger(CalibrationExpression.class);

    private String id;
    private String expression;
    private String unit;
    private String formatPattern;

    private Calculable calculable;

    /**
     * 
     * @return a <code>Calculable</code> containing the expression for finding
     *         the calibrated value
     * @throws UnknownFunctionException
     * @throws UnparsableExpressionException
     */
    public Calculable getCalculable() throws UnknownFunctionException, UnparsableExpressionException {
        if (calculable == null) {
            String expression = getExpression();
            if (expression == null)
                return null;
            calculable = new ExpressionBuilder(expression).withVariableNames("x").build();
        }
        return calculable;
    }

    /**
     * 
     * @return a <code>String</code> containing <code>Parameter</code>'s ID,
     *         corresponding to this calibration data.
     */
    public String getID() {
        return id.replace("//", "/");
    }

    /**
     * 
     * @return a <code>String</code> containing the equation to find the
     *         calibrated value
     */
    public String getExpression() {
        return expression;
    }

    /**
     * 
     * @return a <code>String</code> containing the unit of the calibrated value
     */
    public String getUnit() {
        return unit;
    }

    /**
     * 
     * @return String containing the decimal format, which will be applied to
     *         the calibrated value;
     */
    public String getFormatPattern() {
        return formatPattern;
    }

    /**
     * Calibrates parameter using given expression and formats it using given
     * decimal format, also adds specified unit
     * to the parameter
     * 
     * @param cp - The parameter to be calibrated
     * @throws UnparsableExpressionException
     * @throws UnknownFunctionException
     */
    public void calibrate(CalibratedParameter cp) throws UnknownFunctionException,
            UnparsableExpressionException {
        Number n = cp.getValue();
        if (n != null) {
            Calculable calculable = getCalculable();
            if (calculable != null) {
                cp.setValue(calculable.calculate(n.doubleValue()));
            }
        }

        String formatPattern = getFormatPattern();
        if (formatPattern != null) {
            cp.setFormatPattern(formatPattern);
        }

        String unit = getUnit();
        if (unit != null) {
            cp.setUnit(unit);
        }
    }
}
