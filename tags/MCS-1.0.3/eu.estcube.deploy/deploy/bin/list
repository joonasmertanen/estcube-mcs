#!/bin/bash

if [[ $1 = "-f" ]]; then
    # we have file input; read all lines into array
    shift

    if [[ $# < 1 ]]; then
        printf "Missing service. Usage: %s -f file | %s service(s)" "$0" "$0"
        exit -1
    fi

    file=$1

    # add new line to the end of the file in case it's missing
    sed -i -e '$a\' $file

    index=0
    while read line; do
        services[$index]="$line"
        index=$(($index+1))
    done < $file
else
    # use input arguments
    index=0
    for service in $@
    do
        services[$index]="$service"
        index=$(($index+1))
    done
fi

# print header
line=$(printf "%0.1s" "-"{1..80})
printf "%s\n" $line
printf " %-30s | %-11s | %-5s | %-23s \n" "Service" "Status" "PID" "Since"
printf "%s\n" $line


dir=$( cd "$( dirname "$0" )" && pwd )

# iterate over the list of services
# modify IFS to skip spaces in for loop
oldIFS=$IFS
IFS=$(echo -en "\n\b")
for service in ${services[@]}
do
    [ -z "$service" ] && continue # skip empty lines
    [ ! -d "$service" ] && continue # skip non directories
    [[ "$service" = "bin" ]] && continue # skip bin/
    [[ "$service" = "conf" ]] && continue # skip conf/
    [[ "$service" == \#* ]] && continue # skip comments
    printf "%s\n" "$($dir/status -t $service)" # print status
done
# restore IFS
IFS=$oldIFS

# print footer
printf "%s\n" $line

# done
exit 0
