#!/bin/bash
quiet=false

# quiet flag
if [[ $1 = "-q" ]]; then
    quiet=true
    shift
fi

if [[ $# < 1 ]]; then
    printf "Missing config file or service name.\n"
    printf " Usage: [-q] %s <file or service>\n" "$0"
    exit -1
else
    if [[ $1 == *services.list ]]; then
        file=$1

        # add new line to the end of the file in case it's missing
        sed -i -e '$a\' $file

        index=0
        while read line; do
            services[$index]="$line"
            index=$(($index+1))
        done < $file
    else
        services[0]=$1
        index=1
    fi
fi

shift

# iterate over the list of services to start them
for service in ${services[@]}
do
    [ -z "$service" ] && continue # skip empty lines
    [ ! -d "$service" ] && continue # skip non directories
    [[ "$service" = "bin" ]] && continue # skip bin/
    [[ "$service" = "conf" ]] && continue # skip conf/
    [[ "$service" == \#* ]] && continue # skip comments

    # Check service
    dir=$(pwd)

    if [ ! -f "$dir/$service/bin/run" ]; then
        printf "Service %s not found\n" "$service"
        continue
    fi

    if [[ ! -x "$dir/$service/bin/run" ]]; then
        printf "Service %s not executable\n" "$service"
        continue
    fi


    # Check logs
    if [ ! -d "$dir/$service/logs" ]; then
        mkdir $dir/$service/logs
    fi

    logFile=$dir/$service/logs/std.log

    # Check PID
    pidFile=$dir/$service/bin/service.pid

    pid=$(cat $pidFile 2>/dev/null)
    if [[ $? = 0 ]]; then
        ps -p $pid -f | grep $service >/dev/null 2>&1
        if [[ $? = 0 ]]; then
            printf "Service %s already running\n" "$service"
            continue
        fi
    fi

    # start service
    cd $dir/$service
    nohup bin/run "$@" >> $logFile 2>&1 &

    # save pid
    pid=$!
    echo $pid > $pidFile

    # message
    message="$(date -u +'%Y-%m-%d %H:%M:%S %Z') - $service started; PID: $pid"
    echo $message >> $logFile

    if ! $quiet ; then
        printf "%s\n" "$message"
    fi
    cd $dir
done
exit 0
