#!/bin/bash
quiet=false

# quiet flag
if [[ $1 = "-q" ]]; then
    quiet=true
    shift
fi

if [[ $# < 1 ]]; then
    printf "Missing config file or service name.\n"
    printf " Usage: [-q] %s <file or service>\n" "$0"
    exit -1
else
    if [[ $1 == *services.list ]]; then
        file=$1

        # add new line to the end of the file in case it's missing
        sed -i -e '$a\' $file

        index=0
        while read line; do
            services[$index]="$line"
            index=$(($index+1))
        done < $file
    else
        services[0]=$1
        index=1
    fi
fi

dir=$( cd "$( dirname "$0" )" && pwd )
workingDir=$(pwd)

# iterate over the list of services to stop them
for service in ${services[@]}
do
    [ -z "$service" ] && continue # skip empty lines
    [ ! -d "$service" ] && continue # skip non directories
    [[ "$service" = "bin" ]] && continue # skip bin/
    [[ "$service" = "conf" ]] && continue # skip conf/
    [[ "$service" == \#* ]] && continue # skip comments
    if ! $quiet; then 
        printf "%s\n" "$($dir/stop $service)"
        printf "%s\n" "$($dir/start $service)"
    else
        $dir/stop -q $service
        $dir/start -q $service
    fi
done

exit 0
