package eu.estcube.proxy.processor;

import java.util.Map.Entry;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Predicate;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.util.Dates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.codec.ax25.impl.Ax25AddressDecoder;
import eu.estcube.common.Headers;
import eu.estcube.common.lookup.SatelliteLookup;
import eu.estcube.domain.transport.Downlink;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.gs.contact.LocationContactEventHolder;
import eu.estcube.proxy.converter.RemoteToAx25UiFrameConverter;
import eu.estcube.proxy.model.Ax25UiFrameRemote;
import eu.estcube.proxy.servlet.RemotePacketServlet;

/**
 * Processes the incoming frames from 3rd party groundstations, as well as
 * implements the Predicate interface. Once the incoming frame is converted to
 * Ax25UiFrame, corresponding satellite name according to the destination and
 * source handles from the frame. If the satellite name is not found, the filter
 * returns false and is omitted from further processing.
 * 
 * @author Kaarel Hanson
 * @since 13.03.2014
 */
@Component
public class Ax25UiFrameRemoteProcessor implements Processor, Predicate {

    public static final String DEFAULT_CONTACT_ID = "unknown";

    public static final String TYPE = "AX.25";

    @Autowired
    private RemoteToAx25UiFrameConverter converter;

    @Autowired
    private LocationContactEventHolder eventHolder;

    @Autowired
    private SatelliteLookup satelliteLookup;

    private boolean matches;

    private Ax25AddressDecoder addressDecoder = new Ax25AddressDecoder();

    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        out.copyFrom(in);

        Ax25UiFrameRemote input = in.getBody(Ax25UiFrameRemote.class);
        Ax25UIFrame frame = converter.convert(input);
        out.setBody(frame);

        String satelliteId;
        if (input.getProperites().containsKey(RemotePacketServlet.POST_PARAMETER_NORAD_ID)) {
            satelliteId = satelliteLookup.getSatelliteByNoradID(input.getProperites().get(
                    RemotePacketServlet.POST_PARAMETER_NORAD_ID));
            // We need to double check, because it could be the case that there
            // is a NORAD ID in the input but not described in the Satellites
            // description XML
            if (satelliteId.equals(SatelliteLookup.DEFAULT_NAME)) {
                satelliteId = satelliteLookup.getSatelliteByDestAndSrc(addressDecoder.toString(frame.getDestAddr()),
                        addressDecoder.toString(frame.getSrcAddr()));
            }
        } else {
            satelliteId = satelliteLookup.getSatelliteByDestAndSrc(addressDecoder.toString(frame.getDestAddr()),
                    addressDecoder.toString(frame.getSrcAddr()));
        }
        if (satelliteId.equals(SatelliteLookup.DEFAULT_NAME)) {
            matches = false;
            return;
        }

        matches = true;
        long timestamp = toTimestamp(input.getTimestamp());
        long orbitNumber = eventHolder.getOrbitNumber(timestamp);
        String contactId = eventHolder.getContactId(timestamp);
        if (contactId == null) {
            contactId = DEFAULT_CONTACT_ID;
        }

        out.setHeader(StandardArguments.TIMESTAMP, timestamp);
        out.setHeader(StandardArguments.ISSUED_BY, input.getSourceHandle());
        out.setHeader(StandardArguments.SATELLITE_ID, satelliteId);
        out.setHeader(StandardArguments.CLASS, frame.getClass().getSimpleName());
        out.setHeader(StandardArguments.CONTACT_ID, contactId);
        out.setHeader(StandardArguments.ORBIT_NUMBER, orbitNumber);
        out.setHeader(StandardArguments.TYPE, TYPE);
        out.setHeader(Headers.COMMUNICATION_LINK_TYPE, Downlink.class.getSimpleName());
        for (Entry<String, String> entry : input.getProperites().entrySet()) {
            out.setHeader(entry.getKey(), entry.getValue());
        }
    }

    private long toTimestamp(String value) {
        if (StringUtils.isBlank(value)) {
            return System.currentTimeMillis();
        }
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException nfe) {
            // ignore
        }
        try {
            return Dates.ISO_8601_DATE_FORMATTER.parseDateTime(value).getMillis();
        } catch (IllegalArgumentException iae) {
            // ignore
        }
        return System.currentTimeMillis();
    }

    @Override
    public boolean matches(Exchange exchange) {
        return matches;
    }
}
