package eu.estcube.proxy.model;

import java.util.Map;

/**
 * Frame representing the data sent from 3rd party groundstations
 * 
 * @author Kaarel Hanson
 * @since 11.03.2014
 */
public class Ax25UiFrameRemote {

    private byte[] data;
    private String sourceHandle;
    private String timestamp;
    private Location location;
    private Map<String, String> properites;

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getSourceHandle() {
        return sourceHandle;
    }

    public void setSourceHandle(String sourceHandle) {
        this.sourceHandle = sourceHandle;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Map<String, String> getProperites() {
        return properites;
    }

    public void setProperites(Map<String, String> properites) {
        this.properites = properites;
    }
}
