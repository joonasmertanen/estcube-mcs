define([
        "dojo/_base/declare", 
        "dojo/_base/lang", 
        "dojo/_base/array",
        "dojo/aspect", 
        "dojo/dom-class", 
        "dgrid/OnDemandGrid",
        "dgrid/util/misc", 
        "./ContentProvider",
        "common/store/ArchiverStore", 
        "common/formatter/DateFormatter",
        "common/formatter/Ax25AddressFormatter", 
        ],
        function(declare, Lang, Arrays, Aspect, DomClass, Grid, Misc, ContentProvider,ArchiverStore, DateFormatter, Ax25AddressFormatter) {
            return declare(ContentProvider, {
                grid : null,
                store : ArchiverStore,
                getContent: function (args) {

                    declare.safeMixin(this, args);
                    queryGrid = new Grid({
                        class: "grid-query",
                        columns: this.columns || {},
                        style: "height:100%",
                        store: this.store
                    });

                    queryGrid.set("sort", "receptionTime", true);

                    setTimeout(function () {
                        queryGrid.set("showHeader", true)
                    }, 500);

                    return queryGrid;
                },

                startup: function() {
                    this.grid.startup();
                },  
            });
        });
