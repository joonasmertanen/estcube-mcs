define([
    "config/config",
    ],

    function(Config) {
        return function (object) {

            if (object.name == "version") {
                var versionNumber = parseInt(object.value);
                var hexString = versionNumber.toString(16).toUpperCase();
                return "0x" + hexString;
            } else {
                return object.value;
            }

        };
    }
);