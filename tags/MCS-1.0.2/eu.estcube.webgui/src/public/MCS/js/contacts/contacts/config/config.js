define([
    "dojo/domReady!"
    ],

    function(ready) {
        return {
            routes: {
                CONTACTS: {
                    path: "contacts/contacts",
                    defaults: {
                        controller: "Contacts/ContactsController",
                        method: "index",
                    }
                },
            },

            CONTACTS: {
            }

        };
    }
);
