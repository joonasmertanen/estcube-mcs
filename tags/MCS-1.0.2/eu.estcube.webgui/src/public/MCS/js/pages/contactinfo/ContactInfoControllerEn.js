define([
        "dojo/_base/declare", 
        "common/Controller", 
        "./ContactInfoView",
        ],

function(declare, Controller, ContactInfoView) {
	var s = declare([Controller], {
		constructor : function() {
			this.view = new ContactInfoView("ESTCube-1 (Tartu Observatory GS)", "UTC", "MET");
		},
		index : function(params) {
			this.placeWidget(this.view);
		},
	});
	return new s();
});
