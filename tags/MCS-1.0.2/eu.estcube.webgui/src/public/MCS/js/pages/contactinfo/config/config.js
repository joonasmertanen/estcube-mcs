define([
    "dojo/domReady!"
    ],

    function(ready) {
        return {
            routes: {
                CONTACTINFO_EN: {
                    path: "pages/contactinfo/en",
                    defaults: {
                        controller: "Contactinfo/ContactInfoControllerEn",
                        method: "index",
                    }
                },
                CONTACTINFO_ET: {
                    path: "pages/contactinfo/et",
                    defaults: {
                        controller: "Contactinfo/ContactInfoControllerEt",
                        method: "index",
                    }
                },
            }
        };
    }
);
