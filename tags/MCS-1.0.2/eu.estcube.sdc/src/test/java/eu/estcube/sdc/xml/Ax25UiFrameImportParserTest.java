/** 
 *
 */
package eu.estcube.sdc.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.thoughtworks.xstream.converters.reflection.AbstractReflectionConverter.UnknownFieldException;

import eu.estcube.sdc.domain.Ax25UiFrameImport;

/**
 *
 */
public class Ax25UiFrameImportParserTest {

    private File input;

    private Ax25UiFrameImportParser parser;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        parser = new Ax25UiFrameImportParser();
    }

    /**
     * Test method for
     * {@link eu.estcube.sdc.xml.Ax25UiFrameImportParser#parse(java.io.File)}.
     */
    @Test
    public void testParseOne() throws Exception {
        input = new File(getClass().getResource("/import-01.xml").toURI());
        List<Ax25UiFrameImport> list = parser.parse(input);
        assertNotNull(list);
        assertEquals(1, list.size());
        assertEquals("00 00 00 00 00 00 01", list.get(0).getDestAddr());
        assertEquals("00 00 00 00 00 00 02", list.get(0).getSrcAddr());
        assertEquals("03", list.get(0).getCtrl());
        assertEquals("0F", list.get(0).getPid());
        assertEquals("00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F", list.get(0).getInfo());
        assertEquals(3, list.get(0).getTncTargetPort());
        assertEquals("/ESTCUBE/Satellites/ESTCube-1", list.get(0).getSatelliteId());
        assertEquals("/ESTCUBE/GroundStations/ES5EC", list.get(0).getGroundStationId());
        assertEquals(449, list.get(0).getOrbitNumber());
        assertEquals("1234567890", list.get(0).getTimestamp());
    }

    /**
     * Test method for
     * {@link eu.estcube.sdc.xml.Ax25UiFrameImportParser#parse(java.io.File)}.
     */
    @Test
    public void testParseNoTimestampNoOrbitNumber() throws Exception {
        input = new File(getClass().getResource("/import-02.xml").toURI());
        List<Ax25UiFrameImport> list = parser.parse(input);
        assertNotNull(list);
        assertEquals(1, list.size());
        assertEquals("00 00 00 00 00 00 01", list.get(0).getDestAddr());
        assertEquals("00 00 00 00 00 00 02", list.get(0).getSrcAddr());
        assertEquals("03", list.get(0).getCtrl());
        assertEquals("0F", list.get(0).getPid());
        assertEquals("00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F", list.get(0).getInfo());
        assertEquals(3, list.get(0).getTncTargetPort());
        assertEquals("/ESTCUBE/Satellites/ESTCube-1", list.get(0).getSatelliteId());
        assertEquals("/ESTCUBE/GroundStations/ES5EC", list.get(0).getGroundStationId());
        assertEquals(Ax25UiFrameImport.DEFAULT_ORBIT_NUMBER, list.get(0).getOrbitNumber());
        assertNull(list.get(0).getTimestamp());
    }

    /**
     * Test method for
     * {@link eu.estcube.sdc.xml.Ax25UiFrameImportParser#parse(java.io.File)}.
     */
    @Test
    public void testParseSeveral() throws Exception {
        input = new File(getClass().getResource("/import-03.xml").toURI());
        List<Ax25UiFrameImport> list = parser.parse(input);
        assertNotNull(list);
        assertEquals(2, list.size());
        assertEquals("00 00 00 00 00 00 01", list.get(0).getDestAddr());
        assertEquals("00 00 00 00 00 00 02", list.get(0).getSrcAddr());
        assertEquals("03", list.get(0).getCtrl());
        assertEquals("0F", list.get(0).getPid());
        assertEquals("00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F", list.get(0).getInfo());
        assertEquals(3, list.get(0).getTncTargetPort());
        assertEquals("/ESTCUBE/Satellites/ESTCube-1", list.get(0).getSatelliteId());
        assertEquals("/ESTCUBE/GroundStations/ES5EC", list.get(0).getGroundStationId());
        assertEquals(Ax25UiFrameImport.DEFAULT_ORBIT_NUMBER, list.get(0).getOrbitNumber());
        assertEquals("2013-05-20T14:45:23.035", list.get(0).getTimestamp());

        assertEquals("01 00 00 00 00 00 01", list.get(1).getDestAddr());
        assertEquals("02 00 00 00 00 00 02", list.get(1).getSrcAddr());
        assertEquals("03", list.get(1).getCtrl());
        assertEquals("0F", list.get(1).getPid());
        assertEquals("04 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F", list.get(1).getInfo());
        assertEquals(6, list.get(1).getTncTargetPort());
        assertEquals("/ESTCUBE/Satellites/ESTCube-1", list.get(1).getSatelliteId());
        assertEquals("/ESTCUBE/GroundStations/ES5EC", list.get(1).getGroundStationId());
        assertEquals(449, list.get(1).getOrbitNumber());
        assertEquals("1234567890", list.get(1).getTimestamp());
    }

    /**
     * Test method for
     * {@link eu.estcube.sdc.xml.Ax25UiFrameImportParser#parse(java.io.File)}.
     */
    @Test(expected = UnknownFieldException.class)
    public void testParseWithException() throws Exception {
        input = new File(getClass().getResource("/import-04.xml").toURI());
        parser.parse(input);
    }
}
