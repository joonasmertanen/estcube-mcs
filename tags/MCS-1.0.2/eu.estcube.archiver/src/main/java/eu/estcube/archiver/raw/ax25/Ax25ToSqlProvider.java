package eu.estcube.archiver.raw.ax25;

import java.util.Map;

import org.apache.camel.Message;

import eu.estcube.archiver.raw.sql.MessageToSqlFrameDataProvider;
import eu.estcube.domain.transport.Direction;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;

public class Ax25ToSqlProvider extends MessageToSqlFrameDataProvider {
	public static final String COL_DESTADDR = "destaddr";
	public static final String COL_SRCADDR = "srcaddr";
	public static final String COL_CTRL = "ctrl";
	public static final String COL_PID = "pid";
	public static final String COL_INFO = "info";
	public static final String COL_FCS = "fcs";
	public static final String COL_ERROR_BITMASK = "error_bitmask";

	public Ax25ToSqlProvider(Direction direction) {
		super(direction);
	}

	public Ax25ToSqlProvider(Direction direction, Message message) {
		super(direction, message);
	}

	@Override
	public Object getValue(String column) {
		Message m = getMessage();
		Ax25UIFrame f = m.getBody(Ax25UIFrame.class);
		Map<String, Object> h = m.getHeaders();

		if (COL_RECEPTION_TIME.equals(column)) {
			return h.get(HDR_RECEPTION_TIME);
		} else if (COL_SATELLITE.equals(column)) {
			return h.get(HDR_SATELLITE);
		} else if (COL_DESTADDR.equals(column)) {
			return f.getDestAddr();
		} else if (COL_SRCADDR.equals(column)) {
			return f.getSrcAddr();
		} else if (COL_CTRL.equals(column)) {
			return f.getCtrl();
		} else if (COL_PID.equals(column)) {
			return f.getPid();
		} else if (COL_INFO.equals(column)) {
			return f.getInfo();
		} else if (COL_FCS.equals(column)) {
			return f.getFcs();
		} else if (COL_ERROR_BITMASK.equals(column)) {
			return calculateErrorBitMask(f);
		}
		return null;
	}

	protected int calculateErrorBitMask(Ax25UIFrame f) {
		int bitMask = 0;

		bitMask += f.isErrorFcs() ? Math.pow(2, 0) : 0;
		bitMask += f.isErrorTooLong() ? Math.pow(2, 1) : 0;
		bitMask += f.isErrorTooShort() ? Math.pow(2, 2) : 0;
		bitMask += f.isErrorUnAligned() ? Math.pow(2, 3) : 0;
		bitMask += f.isErrorUnstuffedBits() ? Math.pow(2, 4) : 0;

		return bitMask;
	}

}
