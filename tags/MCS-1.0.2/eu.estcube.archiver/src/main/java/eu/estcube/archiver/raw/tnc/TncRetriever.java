package eu.estcube.archiver.raw.tnc;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.archiver.raw.ArchivingException;
import eu.estcube.archiver.raw.FrameRetriever;
import eu.estcube.archiver.raw.ax25.Ax25Retriever;
import eu.estcube.common.ByteUtil;
import eu.estcube.common.queryParameters.TNCObject;
import eu.estcube.common.queryParameters.TncQueryParameters;

public class TncRetriever extends FrameRetriever {
    public TncRetriever(DataSource source) {
        super(source);
    }

    private Logger LOG = LoggerFactory.getLogger(Ax25Retriever.class);

    private String createQuery(TncQueryParameters params) {
        StringBuilder sb = new StringBuilder();
        
        if (params.getCommand() != Integer.MIN_VALUE) {
            sb.append(" AND commmand = '")
                    .append(params.getCommand())
                    .append("'");
        }
        String query = super.createQuery(params);
        query = query.replace("ReplaceInSubclass", sb.toString());

        return query;
    }

    @Override
    public void retrieve(Exchange exchange) throws ArchivingException, SQLException {
        TncQueryParameters params = (TncQueryParameters) exchange.getIn().getBody();
        LOG.debug("TncRetreiver in parameters" + params.toString());
        String query = createQuery(params);
        LOG.debug("TncRetreiver query" + query);

        super.retrieve(exchange);
        Statement statement = c.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        
        //get metadata
        ResultSetMetaData meta = null;
        meta = resultSet.getMetaData();
        //get column names
        int colCount = meta.getColumnCount();
        ArrayList<String> cols = new ArrayList<String>();
        for (int i=1; i<=colCount; i++){
          cols.add(meta.getColumnName(i));
        }
        //fetch out rows
        ArrayList<TNCObject> rows = new ArrayList<TNCObject>();

        while (resultSet.next()) {
            TNCObject row = new TNCObject();
            for (String colName:cols) {
                try{
                    if (resultSet.getObject(colName) == null);
                    else if (colName.equals("ID")){
                        row.setId((resultSet.getInt(colName)));
                    }else if (colName.equals("DIRECTION")){
                        row.setDirection(resultSet.getString(colName));
                    }else if (colName.equals("RECEPTION_TIME")){
                        row.setReceptionTime(resultSet.getString(colName)); 
                    }else if (colName.equals("SATELLITE")){
                        row.setSatellite(resultSet.getString(colName)); 
                    }else if (colName.equals("COMMAND")){
                        row.setCommand((resultSet.getInt(colName)));
                    }else if (colName.equals("DATA")){
                        row.setData((ByteUtil.toHexString(resultSet.getBytes(colName)))); 
                    }else if (colName.equals("TARGET")){
                        row.setTarget((resultSet.getInt(colName)));
                    }else if (colName.equals("CREATED")){
                        long mili= resultSet.getTimestamp(colName).getTime();
                        row.setCreated(new Date(mili)); 
                    }
                }finally{
                    
                }
            }
            rows.add(row);
        }
        LOG.info("Result: " + rows.size());
        
        exchange.getOut().setBody(rows);
        c.close();

    }
    @Override
    protected String getTable() {
        return TncQueryParameters.getTable();
    }

    @Override
    protected String getDetailsTable() {
        return TncQueryParameters.getDetailsTable();
    }

}
