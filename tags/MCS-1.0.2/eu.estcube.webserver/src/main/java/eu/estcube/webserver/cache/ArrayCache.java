package eu.estcube.webserver.cache;




public class ArrayCache<T extends Object> extends AbstractCache<Integer, T> {

    /**
     * Creates new NamedCache.
     * 
     * @param store
     */
    public ArrayCache(Cache<Integer, T> store) {
        super(store);
    }

    
    /** @{inheritDoc . */
    @Override
    protected Integer getKey(T named) {
        return named.hashCode();
    }
}