package eu.estcube.commandautomation.simple;

import java.util.List;

import org.hbird.exchange.core.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.commandautomation.automator.CommandHandlerFactory;
import eu.estcube.commandautomation.automator.ContactHandler;
import eu.estcube.commandautomation.db.Contact;

@Component
public class SimpleContactHandler extends ContactHandler {

    public SimpleContactHandler(Contact contact, CommandHandlerFactory commandHandlerFactory) {
        super(contact, commandHandlerFactory);
        // TODO Auto-generated constructor stub
    }

    private List<Command> commands;

    @Autowired
    private SimpleCommandLoader loader;

    private int sentCommandsCounter = -1;

    private int sendAgainTimeout;

    @Override
    public void prepare() {
        // commands = loader.load(getTrackingId());

        // TODO create AMQ listeners
    }

    @Override
    public void start() {
        sendNextCommand();

    }

    @Override
    public void end() {

    }

    private void sendNextCommand() {

        clearTimer();

        sentCommandsCounter++;
        Command command = commands.get(sentCommandsCounter);
        sendCommand(command);

        createTimer();

    }

    private void clearTimer() {

    }

    private void createTimer() {

    }

    private void sendCommand(Command command) {
        // TODO Send to AMQ
    }

    public void receivedResponse() {
        sendNextCommand();
    }

}
