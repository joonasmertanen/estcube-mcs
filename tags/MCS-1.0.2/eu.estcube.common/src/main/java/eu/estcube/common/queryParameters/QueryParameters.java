package eu.estcube.common.queryParameters;

import java.io.Serializable;

import org.joda.time.DateTime;

public class QueryParameters implements Serializable {

    public QueryParameters() {
        // TODO Auto-generated constructor stub
    }

    /**
     * 
     */
    protected static final long serialVersionUID = 1L;
    protected int Id;
    protected DateTime start;
    protected DateTime end;
    protected String satelliteName;
    protected String direction;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public DateTime getStart() {
        return start;
    }

    public void setStart(DateTime start) {
        this.start = start;
    }

    public DateTime getEnd() {
        return end;
    }

    public void setEnd(DateTime end) {
        this.end = end;
    }

    public String getSatellite() {
        return satelliteName;
    }

    public void setSatellite(String satellite) {
        this.satelliteName = satellite;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "QueryParameters [start=" + start + ", end=" + end + ", satelliteName=" + satelliteName
                + ", direction=" + direction + "]";
    }
}