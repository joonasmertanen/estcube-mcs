package eu.estcube.camel.serial;



import org.apache.camel.Component;
import org.apache.camel.Consumer;
import org.apache.camel.Processor;
import org.apache.camel.Producer;
import org.apache.camel.impl.DefaultEndpoint;
import org.apache.mina.core.future.CloseFuture;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.transport.serial.SerialAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class SerialEndpoint extends DefaultEndpoint {

    public static final String ATTRIBUTE_SERIAL_ADDRESS = "serial-address";
    
    private static final Logger LOG = LoggerFactory.getLogger(SerialEndpoint.class);
    
    private final SerialAddress serialAddress;
    private IoSession session;
    private final SerialHandler serialHandler;
    private final SessionFactory sessionFactory;
    
    public SerialEndpoint(String endpointUri, Component component, SerialAddress serialAddress, SerialHandler serialHandler, SessionFactory sessionFactory) {
        super(endpointUri, component);
        this.serialAddress = serialAddress;
        this.serialHandler = serialHandler;
        this.sessionFactory = sessionFactory;
    }
    
    /** @{inheritDoc}. */
    @Override
    public Consumer createConsumer(Processor processor) throws Exception {
        IoSession session = getIoSession(); // to initialize the session ant thus open the connection
        LOG.info("Adding new SerialConsumer to session {}", session.getId());
        SerialConsumer consumer = new SerialConsumer(this, processor);
        serialHandler.addConsumer(consumer);
        return consumer;
    }

    /** @{inheritDoc}. */
    @Override
    public Producer createProducer() throws Exception {
        return new SerialProducer(this, getIoSession());
    }

    /** @{inheritDoc}. */
    @Override
    public boolean isSingleton() {
        return false;
    }
    
    IoSession getIoSession() throws Exception {
        if (session == null || !session.isConnected()) {
            session = sessionFactory.openSession(serialAddress, serialHandler);
        }
        return session;
    }

    /** @{inheritDoc}. */
    @Override
    protected void doStop() throws Exception {
        sessionFactory.close();
        String port = serialAddress.getName();
        if (session == null || !session.isConnected()) {
            LOG.debug("Connetion to {} was not opened or is already closed", port);
            return;
        }
        CloseFuture close = session.close(true);
        close.await();
        LOG.info("Closed serial connection to {}", port);
    }
}
