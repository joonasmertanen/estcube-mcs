package eu.estcube.limitchecking.data;

import org.hbird.exchange.core.CalibratedParameter;
import org.hbird.exchange.core.Parameter;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("parameter")
public class ParameterLimits {
    @XStreamAsAttribute
    private String check;

    private String id;
    private String soft;
    private String hard;
    private String sanity;

    private double[] softLimits;
    private double[] hardLimits;
    private double[] sanityLimits;
    private int errorLevel;

    public ParameterLimits(String id, String check, String soft, String hard, String sanity) {
        this.id = id;
        this.check = check;
        this.soft = soft;
        this.hard = hard;
        this.sanity = sanity;
    }

    // Empty constructor required for XStream
    public ParameterLimits() {

    }

    public boolean appliesTo(Parameter p) {
        if (!getID().equals(p.getID().replace("//", "/")))
            return false;
        if (p instanceof CalibratedParameter) {
            return checkCalibrated();
        } else {
            return checkRaw();
        }
    }

    public boolean checkRaw() {
        return check == null || check.equalsIgnoreCase("raw") || check.equalsIgnoreCase("both");
    }

    public boolean checkCalibrated() {
        return check == null || check.equalsIgnoreCase("calibrated") || check.equalsIgnoreCase("both");
    }

    public String getID() {
        return id.replace("//", "/");
    }

    public double[] getSoft() {
        if (softLimits == null && hasSoft()) {
            softLimits = new double[2];
            parseLimitsToArray(softLimits, soft);
        }
        return softLimits;
    }

    public double[] getHard() {
        if (hardLimits == null && hasHard()) {
            hardLimits = new double[2];
            parseLimitsToArray(hardLimits, hard);
        }
        return hardLimits;
    }

    public double[] getSanity() {
        if (sanityLimits == null && hasSanity()) {
            sanityLimits = new double[2];
            parseLimitsToArray(sanityLimits, sanity);
        }
        return sanityLimits;
    }

    public double[] getLimits(int level) {
        switch (level) {
            case 0:
                return getSoft();
            case 1:
                return getHard();
            default:
                return getSanity();
        }
    }

    public boolean hasSoft() {
        return soft != null;
    }

    public boolean hasHard() {
        return hard != null;
    }

    public boolean hasSanity() {
        return sanity != null;
    }

    private void parseLimitsToArray(double[] arr, String limits) {
        String[] data = limits.split(";");
        double d1 = Double.parseDouble(data[0]);
        double d2 = Double.parseDouble(data[1]);
        arr[0] = Math.min(d1, d2);
        arr[1] = Math.max(d1, d2);
    }

    private boolean isInLimits(double val, double[] arr) {
        return arr[0] <= val && arr[1] >= val;
    }

    public boolean isInSoft(Parameter p) {
        if (!hasSoft())
            return false;
        Number n = p.getValue();
        if (n != null) {
            return isInLimits(n.doubleValue(), getSoft());
        }
        return true;
    }

    public boolean isInHard(Parameter p) {
        if (!hasHard())
            return false;
        Number n = p.getValue();
        if (n != null) {
            return isInLimits(n.doubleValue(), getHard());
        }
        return true;
    }

    public boolean isInSanity(Parameter p) {
        if (!hasSanity())
            return false;
        Number n = p.getValue();
        if (n != null) {
            return isInLimits(n.doubleValue(), getSanity());
        }
        return true;
    }

    public int setErrorLevel(int errorLevel) {
        int currentErrorLevel = this.errorLevel;
        this.errorLevel = errorLevel;
        return currentErrorLevel;
    }

}
