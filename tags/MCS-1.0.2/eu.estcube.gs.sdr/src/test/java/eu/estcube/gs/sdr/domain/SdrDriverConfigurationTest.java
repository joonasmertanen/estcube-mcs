/**
 *
 */
package eu.estcube.gs.sdr.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

/**
 *
 */
public class SdrDriverConfigurationTest {

    private SdrDriverConfiguration config;

    @Before
    public void setUp() throws Exception {
        config = new SdrDriverConfiguration();
    }

    @Test
    public void testGetIpInHost() throws Exception {
        assertNull(config.getIpInHost());
    }

    @Test
    public void testGetIpInPort() throws Exception {
        assertEquals(0, config.getIpInPort());
    }

    @Test
    public void testGetIpOutHost() throws Exception {
        assertNull(config.getIpOutHost());
    }

    @Test
    public void testGetIpOutPort() throws Exception {
        assertEquals(0, config.getIpOutPort());
    }
}
