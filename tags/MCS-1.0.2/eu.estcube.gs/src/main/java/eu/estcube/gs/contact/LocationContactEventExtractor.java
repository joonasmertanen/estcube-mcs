/** 
 *
 */
package eu.estcube.gs.contact;

import org.apache.camel.Handler;
import org.hbird.exchange.groundstation.Track;
import org.hbird.exchange.navigation.LocationContactEvent;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class LocationContactEventExtractor {

    @Handler
    public LocationContactEvent extract(Track track) {
        return track.getLocationContactEvent();
    }
}
