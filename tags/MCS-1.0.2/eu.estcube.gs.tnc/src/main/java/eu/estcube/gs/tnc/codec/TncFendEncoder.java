package eu.estcube.gs.tnc.codec;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 */
public class TncFendEncoder extends ProtocolEncoderAdapter {

    private static final Logger LOG = LoggerFactory.getLogger(TncFendEncoder.class);
    
    /** @{inheritDoc}. */
    @Override
    public void encode(IoSession session, Object message, ProtocolEncoderOutput out) throws Exception {

        if (!(message instanceof byte[])) {
            LOG.error("Unsupported message type in {} - {}. Only supported type is byte[]. Throwing exception", getClass().getSimpleName(), message.getClass().getName());
            throw new RuntimeException("Unsupported message type in " + getClass().getName() + " - " + message.getClass().getName());
        }
        byte[] bytes = (byte[]) message;
        IoBuffer buffer = IoBuffer.allocate(bytes.length + 2);
        // wrap the message with FEND bytes
        buffer.put(TncConstants.FEND)
              .put(bytes)
              .put(TncConstants.FEND)
              .flip();
        LOG.trace("Encoded {} ", buffer.getHexDump());
        // that's it - end of the line; have to use IoBuffer as output here
        // otherwise Mina is not able to cast the message
        out.write(buffer);
    }
}
