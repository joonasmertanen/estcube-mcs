package eu.estcube.codec.radiobeacon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashMap;

import org.hbird.business.api.IdBuilder;
import org.hbird.business.api.impl.DefaultIdBuilder;
import org.hbird.exchange.core.EntityInstance;
import org.hbird.exchange.core.Label;
import org.hbird.exchange.core.Parameter;
import org.junit.Before;
import org.junit.Test;

import eu.estcube.codec.radiobeacon.exceptions.InvalidRadioBeaconException;
import eu.estcube.codec.radiobeacon.parsers.RadioBeaconParametersNormalMode;
import eu.estcube.codec.radiobeacon.parsers.RadioBeaconParametersSafeMode;

public class RadioBeaconTranslatorTest {

    private RadioBeaconTranslator radioBeaconTranslator;
    private RadioBeaconParametersNormalMode paramsNormal;
    private RadioBeaconParametersSafeMode paramsSafe;
    private IdBuilder idBuilder;
    private final String correctNormalBeacon = "ES5E/SEAAAAAAABBCCDDEEFFAAABCCDDEEFFAABBCCK";
    private final String correctSafeBeacon1 = "ES5E/STAAAAAAABBCCDDEEEEFFAABBCCDDEEFFAABBCDEEFFAAKN";
    private final String correctSafeBeacon2 = "ES5E/STAAAAAAABB CCDDEEeeFFAABBCCDDEEFFAABBCD EEFFAAKN  ";
    private final String incorrectLenghtBeacon = "ES5AAAAAAAPRRRRKN";
    private final String incorrectBasisBeacon = "QS5R/STAAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSSKN";
    private final String nonASCIISafeBeacon = "ES5E/STAAAAAAABBCCDDEEEEFFGGHHáIJJKKLLMMNNOPQQRRSSKN"; //
    private final String correctNormalBeaconMode1 = "ES5E/SEAAAAAAABBCCDDEEFFGGGHIIJJKKLLMMNNOOK";
    private final String correctNormalBeaconMode2 = "ES5E/S#EAAAAAAABBCCDDEEFFGGGHIIJJKKLLMMNNOOK";
    private final String correctNormalBeaconMode3 = "ES5E/SEAAAAAAABBCCDDEEFFGGGHIIJJKKLLMMNNOO#";
    private final String correctNormalBeaconMode4 = "ES5E/S#AAAAAAABBCCDDEEFFGGGHIIJJKKLLMMNNOO#";
    private final String correctSafeBeaconMode1 = "ES5E/STAAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSSKN";
    private final String correctSafeBeaconMode2 = "ES5E/S#AAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSSKN";
    private final String correctSafeBeaconMode3 = "ES5E/STAAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSS##";
    private final String correctSafeBeaconMode4 = "ES5E/S#AAAAAAABBCCDDEEEEFFGGHHIIJJKKLLMMNNOPQQRRSS##";

    private final String sampleMessageNormalMode = "ES5E/S ETASA ZZZZZ ZUABTCFFHTUS5BSNMFNFCAFE6HK";
    private final String sampleMessageSafeMode = "ES5E/S T TASAZZZ AT DA TT T6SN ZZ 6B TD 5B AB TC FF UE ZU B U WB NC SW KN";
    private final String samplePartialMessageNormalMode = "ES5E/SE###################################K";
    private final String sampleRandomMessage = "haha";
    private final String sampleInvalidMessage = "ES5E/S T XASAZZZ AT DA TT T6SN ZZ 6B TD 5B AB TC FF UE ZU B U WB NC SW KN";

    @Before
    public void setUp() throws Exception {
        radioBeaconTranslator = new RadioBeaconTranslator();
        paramsNormal = new RadioBeaconParametersNormalMode();
        paramsSafe = new RadioBeaconParametersSafeMode();
        idBuilder = new DefaultIdBuilder();
    }

    @Test
    public void checkBeaconLengthTest() throws Exception {
        assertTrue(radioBeaconTranslator.checkBeaconLength(correctNormalBeacon));
        assertTrue(radioBeaconTranslator.checkBeaconLength(correctSafeBeacon1));
        assertFalse(radioBeaconTranslator.checkBeaconLength(incorrectLenghtBeacon));
    }

    @Test
    public void determineRadioBeaconModeTest() throws Exception {
        assertEquals("Normal mode", radioBeaconTranslator.determineRadioBeaconMode(correctNormalBeaconMode1));
        assertEquals("Normal mode", radioBeaconTranslator.determineRadioBeaconMode(correctNormalBeaconMode2));
        assertEquals("Normal mode", radioBeaconTranslator.determineRadioBeaconMode(correctNormalBeaconMode3));
        assertEquals("Undefined mode", radioBeaconTranslator.determineRadioBeaconMode(correctNormalBeaconMode4));
        assertEquals("Safe mode", radioBeaconTranslator.determineRadioBeaconMode(correctSafeBeaconMode1));
        assertEquals("Safe mode", radioBeaconTranslator.determineRadioBeaconMode(correctSafeBeaconMode2));
        assertEquals("Safe mode", radioBeaconTranslator.determineRadioBeaconMode(correctSafeBeaconMode3));
        assertEquals("Undefined mode", radioBeaconTranslator.determineRadioBeaconMode(correctSafeBeaconMode4));
        assertEquals("Undefined mode", radioBeaconTranslator.determineRadioBeaconMode(sampleRandomMessage));
    }

    @Test
    public void reformBeaconMessageTest() throws Exception {
        assertEquals(correctSafeBeacon1, radioBeaconTranslator.reformBeaconMessage(correctSafeBeacon2));
    }

    @Test
    public void checkRadioBeaconBasisTest() throws Exception {
        assertTrue(radioBeaconTranslator.checkRadioBeaconBasis(correctNormalBeacon));
        assertTrue(radioBeaconTranslator.checkRadioBeaconBasis(correctSafeBeacon1));
        assertFalse(radioBeaconTranslator.checkRadioBeaconBasis(incorrectBasisBeacon));
    }

    @Test
    public void checkASCIIMessageTest() throws Exception {
        assertTrue(radioBeaconTranslator.checkASCIIMessage(correctSafeBeacon1));
        assertFalse(radioBeaconTranslator.checkASCIIMessage(nonASCIISafeBeacon));
    }

    @Test
    public void toParametersRandomMessageTest() throws Exception {
        try {
            radioBeaconTranslator.toParameters(sampleRandomMessage, null, "peeter", "operator", idBuilder);
            fail("Should throw exception!");
        } catch (InvalidRadioBeaconException e) {
            assertTrue(true);
        }
    }

    @Test
    public void toParametersNormalModeTest() throws Exception {

        assertEquals(0, radioBeaconTranslator.toParameters(sampleInvalidMessage, null, "oliver", "operator", idBuilder).size());
        assertEquals(0, radioBeaconTranslator.toParameters(incorrectLenghtBeacon, null, "oliver", "operator", idBuilder).size());
        assertEquals(0, radioBeaconTranslator.toParameters(incorrectBasisBeacon, null, "oliver", "operator", idBuilder).size());
        assertEquals(0, radioBeaconTranslator.toParameters(nonASCIISafeBeacon, null, "oliver", "operator", idBuilder).size());

        HashMap<String, EntityInstance> parameters = radioBeaconTranslator
                .toParameters(sampleMessageNormalMode, null, "peeter", "operator", idBuilder);

        assertEquals("ES5E/SETASAZZZZZZUABTCFFHTUS5BSNMFNFCAFE6HK",
                ((Label) (parameters.get(paramsSafe.getRadioBeacon().getName()))).getValue());
        assertEquals("E",
                ((Label) (parameters.get(paramsNormal.getOperatingMode().getName()))).getValue());
        assertEquals(
                paramsNormal.getEntityId() + "/"
                        + paramsNormal.getOperatingMode().getName(),
                ((Label) (parameters.get(paramsNormal.getOperatingMode().getName()))).getID());
        assertEquals("peeter",
                ((Label) (parameters.get(paramsNormal.getOperatingMode().getName()))).getIssuedBy());
        assertEquals(Long.parseLong("1352902792000"),
                ((Label) (parameters.get(paramsNormal.getOperatingMode().getName()))).getTimestamp());
        assertEquals(Long.parseLong("1352902792000"),
                ((Parameter) (parameters.get(paramsNormal.getTimestamp().getName()))).getValue());
        assertEquals(2.69869855813623, /* 136 */
                ((Parameter) (parameters.get(paramsNormal.getMainBusVoltage().getName()))).getValue());
        assertEquals(-126,
                ((Parameter) (parameters.get(paramsNormal.getAveragePowerBalance().getName())))
                        .getValue());
        assertEquals(3.028209702144293/* 171 */,
                ((Parameter) (parameters.get(paramsNormal.getBatteryAVoltage().getName()))).getValue());
        assertEquals(0.22542297503644695/* 12 */,
                ((Parameter) (parameters.get(paramsNormal.getBatteryBVoltage().getName()))).getValue());
        assertEquals(120.9334 /* 255 */,
                ((Parameter) (parameters.get(paramsNormal.getBatteryATemperature().getName())))
                        .getValue());
        assertEquals(360.87933561309234,
                ((Parameter) (parameters.get(paramsNormal.getSpinRateZ().getName()))).getValue());
        assertEquals(3,
                ((Parameter) (parameters.get(paramsNormal.getReceivedSignalStrength().getName())))
                        .getValue());

        assertEquals(1,
                ((Parameter) (parameters.get(paramsNormal.getSatelliteMissionPhase().getName())))
                        .getValue());
        assertEquals(1,
                ((Parameter) (parameters.get(paramsNormal.getTimeSinceLastResetCDHS().getName())))
                        .getValue());
        assertEquals(2,
                ((Parameter) (parameters.get(paramsNormal.getTimeSinceLastResetCOM().getName())))
                        .getValue());
        assertEquals(3,
                ((Parameter) (parameters.get(paramsNormal.getTimeSinceLastResetEPS().getName())))
                        .getValue());

        assertEquals(1.1176470588235294,
                ((Parameter) (parameters.get(paramsNormal.getTetherCurrent().getName()))).getValue());

        assertEquals(1,
                ((Parameter) (parameters.get(paramsNormal.getTimeSinceLastErrorADCS().getName())))
                        .getValue());
        assertEquals(3,
                ((Parameter) (parameters.get(paramsNormal.getTimeSinceLastErrorCDHS().getName())))
                        .getValue());
        assertEquals(3,
                ((Parameter) (parameters.get(paramsNormal.getTimeSinceLastErrorCOM().getName())))
                        .getValue());
        assertEquals(3,
                ((Parameter) (parameters.get(paramsNormal.getTimeSinceLastErrorEPS().getName())))
                        .getValue());

        assertEquals(39,
                ((Parameter) (parameters.get(paramsNormal.getCDHSSystemStatusCode().getName())))
                        .getValue());
        assertEquals(3,
                ((Parameter) (parameters.get(paramsNormal.getCDHSSystemStatusValue().getName())))
                        .getValue());

        assertEquals(202,
                ((Parameter) (parameters.get(paramsNormal.getEPSStatusCode().getName()))).getValue());

        assertEquals(63,
                ((Parameter) (parameters.get(paramsNormal.getADCSSystemStatusCode().getName())))
                        .getValue());
        assertEquals(2,
                ((Parameter) (parameters.get(paramsNormal.getADCSSystemStatusValue().getName())))
                        .getValue());

        assertEquals(25,
                ((Parameter) (parameters.get(paramsNormal.getCOMSystemStatusCode().getName())))
                        .getValue());
        assertEquals(0,
                ((Parameter) (parameters.get(paramsNormal.getCOMSystemStatusValue().getName())))
                        .getValue());

    }

    @Test
    public void toParametersNormalModePartialMessageTest() throws Exception {

        // Part message test
        HashMap<String, EntityInstance> parameters2 = radioBeaconTranslator.toParameters(
                samplePartialMessageNormalMode,
                (long) 10, "oliver", "operator", idBuilder);

        assertEquals(samplePartialMessageNormalMode,
                ((Label) (parameters2.get(paramsSafe.getRadioBeacon().getName())))
                        .getValue());
        assertEquals("E",
                ((Label) (parameters2.get(paramsNormal.getOperatingMode().getName()))).getValue());
        assertEquals("oliver",
                ((Label) (parameters2.get(paramsNormal.getOperatingMode().getName()))).getIssuedBy());
        assertEquals(10,
                ((Label) (parameters2.get(paramsNormal.getOperatingMode().getName()))).getTimestamp());
        assertEquals(null, ((parameters2.get(paramsNormal.getTimestamp().getName()))));
        assertEquals(null, ((parameters2.get(paramsNormal.getMainBusVoltage().getName()))));
        assertEquals(null,
                ((parameters2.get(paramsNormal.getAveragePowerBalance().getName()))));
        assertEquals(null, ((parameters2.get(paramsNormal.getBatteryAVoltage().getName()))));
        assertEquals(null, ((parameters2.get(paramsNormal.getBatteryBVoltage().getName()))));
        assertEquals(null,
                ((parameters2.get(paramsNormal.getBatteryATemperature().getName()))));
        assertEquals(null, ((parameters2.get(paramsNormal.getSpinRateZ().getName()))));
        assertEquals(null,
                ((parameters2.get(paramsNormal.getReceivedSignalStrength().getName()))));

        assertEquals(null,
                ((parameters2.get(paramsNormal.getSatelliteMissionPhase().getName()))));
        assertEquals(null,
                ((parameters2.get(paramsNormal.getTimeSinceLastResetCDHS().getName()))));
        assertEquals(null,
                ((parameters2.get(paramsNormal.getTimeSinceLastResetCOM().getName()))));
        assertEquals(null,
                ((parameters2.get(paramsNormal.getTimeSinceLastResetEPS().getName()))));

        assertEquals(null, ((parameters2.get(paramsNormal.getTetherCurrent().getName()))));

        assertEquals(null,
                ((parameters2.get(paramsNormal.getTimeSinceLastErrorADCS().getName()))));
        assertEquals(null,
                ((parameters2.get(paramsNormal.getTimeSinceLastErrorCDHS().getName()))));
        assertEquals(null,
                ((parameters2.get(paramsNormal.getTimeSinceLastErrorCOM().getName()))));
        assertEquals(null,
                ((parameters2.get(paramsNormal.getTimeSinceLastErrorEPS().getName()))));

        assertEquals(null,
                ((parameters2.get(paramsNormal.getCDHSSystemStatusCode().getName()))));
        assertEquals(null,
                ((parameters2.get(paramsNormal.getCDHSSystemStatusValue().getName()))));

        assertEquals(null, ((parameters2.get(paramsNormal.getEPSStatusCode().getName()))));

        assertEquals(null,
                ((parameters2.get(paramsNormal.getADCSSystemStatusCode().getName()))));
        assertEquals(null,
                ((parameters2.get(paramsNormal.getADCSSystemStatusValue().getName()))));

        assertEquals(null,
                ((parameters2.get(paramsNormal.getCOMSystemStatusCode().getName()))));
        assertEquals(null,
                ((parameters2.get(paramsNormal.getCOMSystemStatusValue().getName()))));

    }

    @Test
    public void toParametersSafeModeTest() throws Exception {

        // safe mode test
        HashMap<String, EntityInstance> parameters3 = radioBeaconTranslator.toParameters(sampleMessageSafeMode, null,
                "oliver", "operator", idBuilder);

        assertEquals("ES5E/STTASAZZZATDATTT6SNZZ6BTD5BABTCFFUEZUBUWBNCSWKN",
                ((Label) (parameters3.get(paramsSafe.getRadioBeacon().getName())))
                        .getValue());
        assertEquals("T",
                ((Label) (parameters3.get(paramsSafe.getOperatingMode().getName()))).getValue());
        assertEquals("oliver",
                ((Label) (parameters3.get(paramsNormal.getOperatingMode().getName()))).getIssuedBy());
        assertEquals(Long.parseLong("1352902792000"),
                ((Label) (parameters3.get(paramsSafe.getOperatingMode().getName()))).getTimestamp());
        assertEquals(
                paramsSafe.getEntityId() + "/"
                        + paramsNormal.getOperatingMode().getName(),
                ((Label) (parameters3.get(paramsNormal.getOperatingMode().getName()))).getID());
        assertEquals(Long.parseLong("1352902792000"),
                ((Parameter) (parameters3.get(paramsSafe.getTimestamp().getName()))).getValue());
        assertEquals(160,
                ((Parameter) (parameters3.get(paramsSafe.getErrorCode1().getName()))).getValue());
        assertEquals(218,
                ((Parameter) (parameters3.get(paramsSafe.getErrorCode2().getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(paramsSafe.getErrorCode3().getName()))).getValue());

        assertEquals(1593,
                ((Parameter) (parameters3.get(paramsSafe.getTimeInSafeMode().getName()))).getValue());
        assertEquals(2.69869855813623 /* 136 */,
                ((Parameter) (parameters3.get(paramsSafe.getMainBusVoltage().getName()))).getValue());

        assertEquals(0,
                ((Parameter) (parameters3.get(paramsSafe.getCDHSProcessorA().getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(paramsSafe.getCDHSProcessorB().getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(paramsSafe.getCDHSBusSwitch().getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(paramsSafe.getCOM3V3VoltageLine().getName())))
                        .getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(paramsSafe.getCOM5VoltageLine().getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(paramsSafe.getPL3V3VoltageLine().getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(paramsSafe.getPL5VVoltageLine().getName()))).getValue());
        assertEquals(1, ((Parameter) (parameters3.get(paramsSafe.getCAMVoltage().getName()))).getValue());

        assertEquals(0,
                ((Parameter) (parameters3.get(paramsSafe.getADCSVoltage().getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(paramsSafe.getBatteryACharging().getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(paramsSafe.getBatteryADischarging().getName())))
                        .getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(paramsSafe.getBatteryBCharging().getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(paramsSafe.getBatteryBDischarging().getName())))
                        .getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(paramsSafe.getSecondaryPowerBusA().getName())))
                        .getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(paramsSafe.getSecondaryPowerBusB().getName())))
                        .getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(paramsSafe.getV12LineVoltage().getName()))).getValue());

        assertEquals(0,
                ((Parameter) (parameters3.get(paramsSafe.getRegulatorA5V().getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(paramsSafe.getRegulatorB5V().getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(paramsSafe.getLineVoltage5V().getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(paramsSafe.getRegulatorA3V3().getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(paramsSafe.getRegulatorB3V3().getName()))).getValue());
        assertEquals(0,
                ((Parameter) (parameters3.get(paramsSafe.getLineVoltage3V3().getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(paramsSafe.getRegulatorA12V().getName()))).getValue());
        assertEquals(1,
                ((Parameter) (parameters3.get(paramsSafe.getRegulatorB12V().getName()))).getValue());

        assertEquals(3.028209702144293 /* 171 */,
                ((Parameter) (parameters3.get(paramsSafe.getBatteryAVoltage().getName()))).getValue());
        assertEquals(0.22542297503644695 /* 12 */,
                ((Parameter) (parameters3.get(paramsSafe.getBatteryBVoltage().getName()))).getValue());
        assertEquals(120.9334 /* 255 */,
                ((Parameter) (parameters3.get(paramsSafe.getBatteryATemperature().getName())))
                        .getValue());
        assertEquals(-28.271700000000003 /* 46 */,
                ((Parameter) (parameters3.get(paramsSafe.getBatteryBTemperature().getName())))
                        .getValue());
        assertEquals(-126,
                ((Parameter) (parameters3.get(paramsSafe.getAveragePowerBalane().getName())))
                        .getValue());
        assertEquals(11,
                ((Parameter) (parameters3.get(paramsSafe.getFirmwareVersionNumber().getName())))
                        .getValue());
        assertEquals(2,
                ((Parameter) (parameters3.get(paramsSafe.getNumberOfCrashes().getName()))).getValue());

        assertEquals(27,
                ((Parameter) (parameters3.get(paramsSafe.getForwardedRFPower().getName()))).getValue());
        assertEquals(-100,
                ((Parameter) (parameters3.get(paramsSafe.getReflectedRFPower().getName()))).getValue());
        assertEquals(
                49, // @TODO spec ütleb, et see peaks olema 81, kas asi ei tööta
                    // õigesti?
                ((Parameter) (parameters3.get(paramsSafe.getReceivedSignalStrength().getName())))
                        .getValue());
    }
}
