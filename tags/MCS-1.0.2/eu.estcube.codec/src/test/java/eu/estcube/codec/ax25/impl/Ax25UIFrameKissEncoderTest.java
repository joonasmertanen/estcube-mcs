package eu.estcube.codec.ax25.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import java.nio.ByteBuffer;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.domain.transport.tnc.TncFrame;
import eu.estcube.domain.transport.tnc.TncFrame.TncCommand;

@RunWith(MockitoJUnitRunner.class)
public class Ax25UIFrameKissEncoderTest {
    
	private static final byte[] PREFIX = new byte[] { 0x02 };
	private static final byte[] DEST_ADDRESS = new byte[] { 1, 2, 3, 4, 5, 6, 7 };
	private static final byte[] SRC_ADDRESS = new byte[] { 8, 9, 10, 11, 12, 13, 14 };
	private static final byte CTRL = (byte) 15;
	private static final byte PID = (byte) 16;
	private static final byte[] INFO = new byte[] { 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 127 };
	private static final int TNC_TARGET_PORT = 4;
	
	private Ax25UIFrameKissEncoder encoder;
	
	private byte[] expectedDataWithoutPrefix;
	private byte[] expectedDataWithPrefix;
	
	@Mock
	private Ax25UIFrame ax25Frame;

	private InOrder inOrder;
	
	@Before
	public void setUp() throws Exception {
		encoder = new Ax25UIFrameKissEncoder();
		inOrder = inOrder(ax25Frame);
		when(ax25Frame.getDestAddr()).thenReturn(DEST_ADDRESS);
		when(ax25Frame.getSrcAddr()).thenReturn(SRC_ADDRESS);
		when(ax25Frame.getCtrl()).thenReturn(CTRL);
		when(ax25Frame.getPid()).thenReturn(PID);
		when(ax25Frame.getInfo()).thenReturn(INFO);
		
		ByteBuffer buffer = ByteBuffer.allocate(PREFIX.length + DEST_ADDRESS.length + SRC_ADDRESS.length + 1 + 1 + INFO.length);
		buffer.put(PREFIX);
		buffer.put(DEST_ADDRESS);
		buffer.put(SRC_ADDRESS);
		buffer.put(CTRL);
		buffer.put(PID);
		buffer.put(INFO);
		buffer.flip();

		expectedDataWithPrefix = new byte[buffer.remaining()];
		buffer.get(expectedDataWithPrefix);
		buffer.flip();
		buffer.get();
		expectedDataWithoutPrefix = new byte[buffer.remaining()];
		buffer.get(expectedDataWithoutPrefix);
	}


	@Test
	public void testEncodeNoPrefix() throws Exception {
	    TncFrame tncFrame = encoder.encode(ax25Frame, TNC_TARGET_PORT, null);
	    assertEquals(TncCommand.DATA, tncFrame.getCommand());
	    assertEquals(TNC_TARGET_PORT, tncFrame.getTarget());
	    assertTrue(Arrays.equals(expectedDataWithoutPrefix, tncFrame.getData()));
	    
	    inOrder.verify(ax25Frame, times(1)).getDestAddr();
	    inOrder.verify(ax25Frame, times(1)).getSrcAddr();
	    inOrder.verify(ax25Frame, times(1)).getCtrl();
	    inOrder.verify(ax25Frame, times(1)).getPid();
	    inOrder.verify(ax25Frame, times(1)).getInfo();
	    inOrder.verifyNoMoreInteractions();
	}

	@Test
	public void testEncodeWithPrefix() throws Exception {
	    TncFrame tncFrame = encoder.encode(ax25Frame, TNC_TARGET_PORT, PREFIX);
	    assertEquals(TncCommand.DATA, tncFrame.getCommand());
	    assertEquals(TNC_TARGET_PORT, tncFrame.getTarget());
	    assertTrue(Arrays.equals(expectedDataWithPrefix, tncFrame.getData()));
	    
	    inOrder.verify(ax25Frame, times(1)).getDestAddr();
	    inOrder.verify(ax25Frame, times(1)).getSrcAddr();
	    inOrder.verify(ax25Frame, times(1)).getCtrl();
	    inOrder.verify(ax25Frame, times(1)).getPid();
	    inOrder.verify(ax25Frame, times(1)).getInfo();
	    inOrder.verifyNoMoreInteractions();
	}
	
	@Test
	public void testEncodeWithPrefixNoTarget() throws Exception {
	    TncFrame tncFrame = encoder.encode(ax25Frame, Ax25UIFrameKissEncoder.DEFAULT_TNC_TARGET_PORT, PREFIX);
	    assertEquals(TncCommand.DATA, tncFrame.getCommand());
	    assertEquals(Ax25UIFrameKissEncoder.DEFAULT_TNC_TARGET_PORT, tncFrame.getTarget());
	    assertTrue(Arrays.equals(expectedDataWithPrefix, tncFrame.getData()));
	    
	    inOrder.verify(ax25Frame, times(1)).getDestAddr();
	    inOrder.verify(ax25Frame, times(1)).getSrcAddr();
	    inOrder.verify(ax25Frame, times(1)).getCtrl();
	    inOrder.verify(ax25Frame, times(1)).getPid();
	    inOrder.verify(ax25Frame, times(1)).getInfo();
	    inOrder.verifyNoMoreInteractions();
	}
	
	@Test
	public void testEncodeWithoutPrefixNoTarget() throws Exception {
	    TncFrame tncFrame = encoder.encode(ax25Frame);
	    assertEquals(TncCommand.DATA, tncFrame.getCommand());
	    assertEquals(Ax25UIFrameKissEncoder.DEFAULT_TNC_TARGET_PORT, tncFrame.getTarget());
	    assertTrue(Arrays.equals(expectedDataWithoutPrefix, tncFrame.getData()));
	    
	    inOrder.verify(ax25Frame, times(1)).getDestAddr();
	    inOrder.verify(ax25Frame, times(1)).getSrcAddr();
	    inOrder.verify(ax25Frame, times(1)).getCtrl();
	    inOrder.verify(ax25Frame, times(1)).getPid();
	    inOrder.verify(ax25Frame, times(1)).getInfo();
	    inOrder.verifyNoMoreInteractions();
	}
}
