/** 
 *
 */
package eu.estcube.weatherstation;

import org.hbird.exchange.core.Label;
import org.hbird.exchange.core.Parameter;

/**
 *
 */
public class DefaultWeatherParameters {

    public static final Parameter AIR_TEMPERATURE = new Parameter(null, "Air Temperature");
    public static final Parameter RELATIVE_HUMIDITY = new Parameter(null, "Relative Humidity");
    public static final Label WIND_DIRECTION = new Label(null, "Wind Direction");
    public static final Parameter WIND_DIRECTION_PARAM = new Parameter(null, "Wind Direction");
    public static final Parameter WIND_SPEED = new Parameter(null, "Wind Speed");
    public static final Parameter LUX = new Parameter(null, "Lux");
    public static final Parameter SOLE = new Parameter(null, "Sole");
    public static final Parameter GAMMA = new Parameter(null, "Gamma");
    public static final Parameter PRECIPITATIONS = new Parameter(null, "Precipitations");
    public static final Label PHENOMENON = new Label(null, "Phenomenon");
    public static final Parameter VISIBILITY = new Parameter(null, "Visibility");
    public static final Parameter AIR_PRESSURE = new Parameter(null, "Air Pressure");
    public static final Parameter WIND_SPEED_MAX = new Parameter(null, "Maximum Wind Speed");
    public static final Label NAME = new Label(null, "name");
    public static final Parameter WMO_CODE = new Parameter(null, "WMO Code");
    public static final Label AIR_TEMPERATURE_TREND = new Label(null, "Air Temperature Trend");
    public static final Label AIR_PRESSURE_TREND = new Label(null, "Air Pressure Trend");
    public static final Label RELATIVE_HUMIDITY_TREND = new Label(null, "Relative Humidity Trend");

    static {
        AIR_TEMPERATURE.setDescription("Air temperature from weather station");
        AIR_TEMPERATURE.setUnit("°C");
        RELATIVE_HUMIDITY.setDescription("Relative humidity from weather station");
        RELATIVE_HUMIDITY.setUnit("%");
        WIND_DIRECTION.setDescription("Wind direction from weather station");
        WIND_DIRECTION_PARAM.setDescription("Wind direction from weather station");
        WIND_DIRECTION_PARAM.setUnit("°");
        WIND_SPEED.setDescription("Wind speed in weather station");
        WIND_SPEED.setUnit("m/s");
        LUX.setDescription("Lux in weather station");
        LUX.setUnit("lx");
        SOLE.setDescription("Sole in weather station");
        SOLE.setUnit("W/m^2");
        GAMMA.setDescription("Gamma in weather station");
        GAMMA.setUnit("µSv/h");
        PRECIPITATIONS.setDescription("Precipitations from weather station");
        PRECIPITATIONS.setUnit("mm");
        PHENOMENON.setDescription("Phenomenon from weather station");
        VISIBILITY.setDescription("Visibility from weather station");
        VISIBILITY.setUnit("km");
        AIR_PRESSURE.setDescription("Air pressure in weather station");
        AIR_PRESSURE.setUnit("hPa");
        WIND_SPEED_MAX.setDescription("Max wind speed in weather station");
        WIND_SPEED_MAX.setUnit("m/s");
        NAME.setDescription("Name of the weather station");
        WMO_CODE.setDescription("WMO Code of weather station");
        AIR_TEMPERATURE_TREND.setDescription("Air temperature trend from weather station");
        AIR_PRESSURE_TREND.setDescription("Air pressure trend from weather station");
        RELATIVE_HUMIDITY_TREND.setDescription("Relative humidity trend from weather station");
    }
}
