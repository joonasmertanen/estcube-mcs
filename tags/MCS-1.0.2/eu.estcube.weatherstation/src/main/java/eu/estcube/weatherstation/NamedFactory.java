package eu.estcube.weatherstation;

import org.hbird.exchange.core.EntityInstance;

public interface NamedFactory<T extends EntityInstance> {
    T createNew(T base, String value);
}
