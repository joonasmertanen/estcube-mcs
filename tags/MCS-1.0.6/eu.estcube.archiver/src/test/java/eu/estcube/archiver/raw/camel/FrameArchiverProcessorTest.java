package eu.estcube.archiver.raw.camel;

import static org.junit.Assert.assertSame;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.archiver.raw.FrameArchiver;
import eu.estcube.archiver.raw.sql.MessageToSqlFrameDataProvider;

@RunWith(MockitoJUnitRunner.class)
public class FrameArchiverProcessorTest {
	FrameArchiverProcessor processor;

	@Mock
	private FrameArchiver archiver;

	@Mock
	private MessageToSqlFrameDataProvider provider;

	@Mock
	private Exchange exchange;

	@Mock
	private Message message;

	@Before
	public void setUp() throws Exception {
		processor = new FrameArchiverProcessor(archiver, provider);

		Mockito.when(exchange.getIn()).thenReturn(message);

	}

	@Test
	public void testFrameArchiverProcessor() {
		assertSame(archiver, processor.getFrameArchiver());
		assertSame(provider, processor.getMessageToSqlFrameDataProvider());
	}

	@Test
	public void testProcess() throws Exception {
		InOrder io = Mockito.inOrder(exchange, archiver, provider);
		processor.process(exchange);

		io.verify(exchange).getIn();
		io.verify(provider).setMessage(message);
		io.verify(archiver).store(provider);
		io.verifyNoMoreInteractions();

	}

}
