package eu.estcube.dataanalysis;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

public class DataAnalysis extends RouteBuilder {
    private static final Logger LOG = LoggerFactory.getLogger(DataAnalysis.class);

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    AbstractApplicationContext context;

    @Override
    public void configure() throws Exception {
        LOG.info("Configuring WebServer");

        MongoOperations MongoOperations = (MongoOperations) context.getBean("mongoTemplate");

        Query query = new Query();
        query.addCriteria(Criteria.where("key").lt(40));
        int count = MongoOperations.getCollection("parameter").find().count();
        LOG.info("parameters table count: " + count);

    }

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) {
        LOG.info("Starting Data Analysis");
        try {
            new Main().run(args);
        } catch (Exception e) {
            LOG.error("Failed to start " + DataAnalysis.class.getName(), e);
        }
    }

}
