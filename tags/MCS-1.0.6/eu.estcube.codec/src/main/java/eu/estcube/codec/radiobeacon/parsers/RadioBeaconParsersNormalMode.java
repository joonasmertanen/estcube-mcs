package eu.estcube.codec.radiobeacon.parsers;

import java.util.ArrayList;
import java.util.List;

import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.core.Label;

import eu.estcube.codec.radiobeacon.parser.RadioBeaconMessageParser;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParser2ComplementInteger;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserBits;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserCalibratedDouble;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserHex;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserInteger;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserSpinRate;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserTetherCurrent;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserTimestamp;

public class RadioBeaconParsersNormalMode extends RadioBeaconParsers {

    private RadioBeaconParametersNormalMode parameters;

    /**
     * Creates new RadioBeaconParsersNormalMode.
     * 
     * @param idBuilder
     */
    public RadioBeaconParsersNormalMode(IdBuilder idBuilder) {
        super(idBuilder);
        parameters = new RadioBeaconParametersNormalMode();
    }

    @Override
    public List<RadioBeaconMessageParser> getParsers() {
        List<RadioBeaconMessageParser> parsers = new ArrayList<RadioBeaconMessageParser>();
        parsers.add(new RadionBeaconMessageParserHex(6, 1, parameters.getOperatingMode()));
        parsers.add(getTimestampParser());
        parsers.add(new RadionBeaconMessageParserCalibratedDouble(14, 2,
                parameters.getMainBusVoltage(), 0.001240978485204 * 16, -0.001670625667674));

        parsers.add(new RadionBeaconMessageParser2ComplementInteger(16, 2,
                parameters.getAveragePowerBalance()));
        parsers.add(new RadionBeaconMessageParserCalibratedDouble(18, 2,
                parameters.getBatteryAVoltage(), 0.017686154075981, 0.003877355151542));
        parsers.add(new RadionBeaconMessageParserCalibratedDouble(20, 2,
                parameters.getBatteryBVoltage(), 0.017645083640731, 0.013681971347675));
        parsers.add(new RadionBeaconMessageParserCalibratedDouble(22, 2,
                parameters.getBatteryATemperature(), 0.7139, -61.1111));
        parsers.add(new RadionBeaconMessageParserSpinRate(24, 3, parameters.getSpinRateZ()));
        parsers.add(new RadionBeaconMessageParser2ComplementInteger(27, 1,
                parameters.getReceivedSignalStrength()));

        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0xC0, 6,
                parameters.getSatelliteMissionPhase()));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x30, 4,
                parameters.getTimeSinceLastResetCDHS()));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0xC, 2,
                parameters.getTimeSinceLastResetCOM()));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x3, 0,
                parameters.getTimeSinceLastResetEPS()));

        parsers.add(new RadionBeaconMessageParserTetherCurrent(30, 2, parameters.getTetherCurrent()));

        parsers.add(new RadionBeaconMessageParserBits(32, 2, 0xC0, 6,
                parameters.getTimeSinceLastErrorADCS()));
        parsers.add(new RadionBeaconMessageParserBits(32, 2, 0x30, 4,
                parameters.getTimeSinceLastErrorCDHS()));
        parsers.add(new RadionBeaconMessageParserBits(32, 2, 0xC, 2,
                parameters.getTimeSinceLastErrorCOM()));
        parsers.add(new RadionBeaconMessageParserBits(32, 2, 0x3, 0,
                parameters.getTimeSinceLastErrorEPS()));

        parsers.add(new RadionBeaconMessageParserBits(34, 2, 0xFC, 2,
                parameters.getCDHSSystemStatusCode()));
        parsers.add(new RadionBeaconMessageParserBits(34, 2, 0x3, 0,
                parameters.getCDHSSystemStatusValue()));

        parsers.add(new RadionBeaconMessageParserInteger(36, 2, parameters.getEPSStatusCode()));

        parsers.add(new RadionBeaconMessageParserBits(38, 2, 0xFC, 2,
                parameters.getADCSSystemStatusCode()));
        parsers.add(new RadionBeaconMessageParserBits(38, 2, 0x3, 0,
                parameters.getADCSSystemStatusValue()));

        parsers.add(new RadionBeaconMessageParserBits(40, 2, 0xFC, 2,
                parameters.getCOMSystemStatusCode()));
        parsers.add(new RadionBeaconMessageParserBits(40, 2, 0x3, 0,
                parameters.getCOMSystemStatusValue()));

        return parsers;
    }

    @Override
    public Label getRadioBeacon() {
        return parameters.getRadioBeacon();
    }

    @Override
    public String getEntityID() {
        return parameters.getEntityId();
    }

    @Override
    public RadionBeaconMessageParserTimestamp getTimestampParser() {
        return new RadionBeaconMessageParserTimestamp(7, 7, parameters.getTimestamp());
    }

    @Override
    public void updateParameters() {
        parameters.updateParameters();
    }

}
