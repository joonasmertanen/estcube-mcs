package eu.estcube.codec.radiobeacon.parsers;

import org.hbird.exchange.core.Label;
import org.hbird.exchange.core.Parameter;

/**
 * @author hansonk
 *
 */
public class RadioBeaconParametersSafeMode extends RadioBeaconParameters {

    // FIXME Remove hardcoded value @ Kaarel
    private final String ENTITY_ID = "/ESTCUBE/Satellites/ESTCube-1/beacon";

    private Label radioBeacon = new Label(null, "raw");
    private Label operatingMode = new Label(null, "operating.mode");
    private Parameter timestamp = new Parameter(null, "timestamp");
    private Parameter errorCode1 = new Parameter(null, "error.code.1");
    private Parameter errorCode2 = new Parameter(null, "error.code.2");
    private Parameter errorCode3 = new Parameter(null, "error.code.3");
    private Parameter timeInSafeMode = new Parameter(null, "time.in.safe.mode");
    private Parameter mainBusVoltage = new Parameter(null, "main.bus.voltage");
    private Parameter CDHSProcessorA = new Parameter(null, "CDHS.processor.A");
    private Parameter CDHSProcessorB = new Parameter(null, "CDHS.processor.B");
    private Parameter CDHSBusSwitch = new Parameter(null, "CDHS.bus.switch");
    private Parameter COM3V3VoltageLine = new Parameter(null, "COM.3v3.voltage.line");
    private Parameter COM5VoltageLine = new Parameter(null, "COM.5V.voltage.line");
    private Parameter PL3V3VoltageLine = new Parameter(null, "PL.3v3.voltage.line");
    private Parameter PL5VVoltageLine = new Parameter(null, "PL.5V.voltage.line");
    private Parameter CAMVoltage = new Parameter(null, "CAM.voltage");
    private Parameter ADCSVoltage = new Parameter(null, "ADCS.voltage");
    private Parameter batteryACharging = new Parameter(null, "battery.A.charging");
    private Parameter batteryADischarging = new Parameter(null, "battery.A.discharging");
    private Parameter batteryBCharging = new Parameter(null, "battery.B.charging");
    private Parameter batteryBDischarging = new Parameter(null, "battery.B.discharging");
    private Parameter secondaryPowerBusA = new Parameter(null, "secondary.power.bus.A");
    private Parameter secondaryPowerBusB = new Parameter(null, "secondary.power.bus.B");
    private Parameter V12LineVoltage = new Parameter(null, "12V.line.voltage");
    private Parameter regulatorA5V = new Parameter(null, "regulator.A.5V");
    private Parameter regulatorB5V = new Parameter(null, "regulator.B.5V");
    private Parameter lineVoltage5V = new Parameter(null, "line.voltage.5V");
    private Parameter regulatorA3V3 = new Parameter(null, "regulator.A.3V3");
    private Parameter regulatorB3V3 = new Parameter(null, "regulator.B.3V3");
    private Parameter lineVoltage3V3 = new Parameter(null, "line.voltage.3V3");
    private Parameter regulatorA12V = new Parameter(null, "regulator.A.12V");
    private Parameter regulatorB12V = new Parameter(null, "regultator.B.12V");
    private Parameter batteryAVoltage = new Parameter(null, "battery.A.voltage");
    private Parameter batteryBVoltage = new Parameter(null, "battery.B.voltage");
    private Parameter batteryATemperature = new Parameter(null, "battery.A.temperature");
    private Parameter batteryBTemperature = new Parameter(null, "battery.B.temperature");
    private Parameter averagePowerBalane = new Parameter(null, "average.power.balance");
    private Parameter firmwareVersionNumber = new Parameter(null, "firmware.version.number");
    private Parameter numberOfCrashes = new Parameter(null, "number.of.crashes");
    private Parameter forwardedRFPower = new Parameter(null, "forwarded.RF.power");
    private Parameter reflectedRFPower = new Parameter(null, "reflected.RF.power");
    private Parameter receivedSignalStrength = new Parameter(null, "received.signal.strength");

    public RadioBeaconParametersSafeMode() {
        getRadioBeacon().setDescription("Raw radiobeacon string");
        getOperatingMode().setDescription("Operating mode (E=normal or T=safe)");
        getTimestamp().setDescription("Timestamp");
        getTimestamp().setUnit("sec");
        getErrorCode1().setDescription("Error code 1");
        getErrorCode1().setUnit("");
        getErrorCode2().setDescription("Error code 2");
        getErrorCode2().setUnit("");
        getErrorCode3().setDescription("Error code 3");
        getErrorCode3().setUnit("");
        getTimeInSafeMode().setDescription("Time in safe mode");
        getTimeInSafeMode().setUnit("min");
        getMainBusVoltage().setDescription("Main bus voltage");
        getMainBusVoltage().setUnit("V");
        getCDHSProcessorA().setDescription("0=OK, 1=FAULT");
        getCDHSProcessorA().setUnit("");
        getCDHSProcessorB().setDescription("0=OK, 1=FAULT");
        getCDHSProcessorB().setUnit("");
        getCDHSBusSwitch().setDescription("0=OK, 1=FAULT");
        getCDHSBusSwitch().setUnit("");
        getCOM3V3VoltageLine().setDescription("0=OK, 1=FAULT");
        getCOM3V3VoltageLine().setUnit("");
        getCOM5VoltageLine().setDescription("0=OK, 1=FAULT");
        getCOM5VoltageLine().setUnit("");
        getPL3V3VoltageLine().setDescription("0=OK, 1=FAULT");
        getPL3V3VoltageLine().setUnit("");
        getPL5VVoltageLine().setDescription("0=OK, 1=FAULT");
        getPL5VVoltageLine().setUnit("");
        getCAMVoltage().setDescription("0=OK, 1=FAULT");
        getCAMVoltage().setUnit("");
        getADCSVoltage().setDescription("0=OK, 1=FAULT");
        getADCSVoltage().setUnit("");
        getBatteryACharging().setDescription("0=OK, 1=FAULT");
        getBatteryACharging().setUnit("");
        getBatteryADischarging().setDescription("0=OK, 1=FAULT");
        getBatteryADischarging().setUnit("");
        getBatteryBCharging().setDescription("0=OK, 1=FAULT");
        getBatteryBCharging().setUnit("");
        getBatteryBDischarging().setDescription("0=OK, 1=FAULT");
        getBatteryBDischarging().setUnit("");
        getSecondaryPowerBusA().setDescription("0=OK, 1=FAULT");
        getSecondaryPowerBusA().setUnit("");
        getSecondaryPowerBusB().setDescription("0=OK, 1=FAULT");
        getSecondaryPowerBusB().setUnit("");
        getV12LineVoltage().setDescription("0=OK, 1=FAULT");
        getV12LineVoltage().setUnit("");
        getRegulatorA5V().setDescription("0=OK, 1=FAULT");
        getRegulatorA5V().setUnit("");
        getRegulatorB5V().setDescription("0=OK, 1=FAULT");
        getRegulatorB5V().setUnit("");
        getLineVoltage5V().setDescription("0=OK, 1=FAULT");
        getLineVoltage5V().setUnit("");
        getRegulatorA3V3().setDescription("0=OK, 1=FAULT");
        getRegulatorA3V3().setUnit("");
        getRegulatorB3V3().setDescription("0=OK, 1=FAULT");
        getRegulatorB3V3().setUnit("");
        getLineVoltage3V3().setDescription("0=OK, 1=FAULT");
        getLineVoltage3V3().setUnit("");
        getRegulatorA12V().setDescription("0=OK, 1=FAULT");
        getRegulatorA12V().setUnit("");
        getRegulatorB12V().setDescription("0=OK, 1=FAULT");
        getRegulatorB12V().setUnit("");
        getBatteryAVoltage().setDescription("Battery A voltage");
        getBatteryAVoltage().setUnit("V");
        getBatteryBVoltage().setDescription("Battery B voltage");
        getBatteryBVoltage().setUnit("V");
        getBatteryATemperature().setDescription("Battery A Temperature");
        getBatteryATemperature().setUnit("C");
        getBatteryBTemperature().setDescription("Battery B Temperature");
        getBatteryBTemperature().setUnit("C");
        getAveragePowerBalane().setDescription("Average power balance");
        getAveragePowerBalane().setUnit("W");
        getFirmwareVersionNumber().setDescription("Version number");
        getFirmwareVersionNumber().setUnit("");
        getNumberOfCrashes().setDescription("Number of crashes");
        getNumberOfCrashes().setUnit("");
        getForwardedRFPower().setDescription("Forwarded RF power");
        getForwardedRFPower().setUnit("dBm");
        getReflectedRFPower().setDescription("Reflected RF power");
        getReflectedRFPower().setUnit("dBm");
        getReceivedSignalStrength().setDescription("Received signal strength");
        getReceivedSignalStrength().setUnit("dBm");
    }

    /**
     * Updates the varsion and timestamp of the parameters and labels
     */
    public void updateParameters() {
        long now = getCurrentTimeStamp();
        update(radioBeacon, now);
        update(operatingMode, now);
        update(timestamp, now);
        update(errorCode1, now);
        update(errorCode2, now);
        update(errorCode3, now);
        update(timeInSafeMode, now);
        update(mainBusVoltage, now);
        update(CDHSProcessorA, now);
        update(CDHSProcessorB, now);
        update(CDHSBusSwitch, now);
        update(COM3V3VoltageLine, now);
        update(COM5VoltageLine, now);
        update(PL3V3VoltageLine, now);
        update(PL5VVoltageLine, now);
        update(CAMVoltage, now);
        update(ADCSVoltage, now);
        update(batteryACharging, now);
        update(batteryADischarging, now);
        update(batteryBCharging, now);
        update(batteryBDischarging, now);
        update(secondaryPowerBusA, now);
        update(secondaryPowerBusB, now);
        update(V12LineVoltage, now);
        update(regulatorA5V, now);
        update(regulatorB5V, now);
        update(lineVoltage5V, now);
        update(regulatorA3V3, now);
        update(regulatorB3V3, now);
        update(lineVoltage3V3, now);
        update(regulatorA12V, now);
        update(regulatorB12V, now);
        update(batteryAVoltage, now);
        update(batteryBVoltage, now);
        update(batteryATemperature, now);
        update(batteryBTemperature, now);
        update(averagePowerBalane, now);
        update(firmwareVersionNumber, now);
        update(numberOfCrashes, now);
        update(forwardedRFPower, now);
        update(reflectedRFPower, now);
        update(receivedSignalStrength, now);
    }

    public Label getRadioBeacon() {
        return radioBeacon;
    }

    public Label getOperatingMode() {
        return operatingMode;
    }

    public Parameter getTimestamp() {
        return timestamp;
    }

    public Parameter getErrorCode1() {
        return errorCode1;
    }

    public Parameter getErrorCode2() {
        return errorCode2;
    }

    public Parameter getErrorCode3() {
        return errorCode3;
    }

    public Parameter getTimeInSafeMode() {
        return timeInSafeMode;
    }

    public Parameter getMainBusVoltage() {
        return mainBusVoltage;
    }

    public Parameter getCDHSProcessorA() {
        return CDHSProcessorA;
    }

    public Parameter getCDHSProcessorB() {
        return CDHSProcessorB;
    }

    public Parameter getCDHSBusSwitch() {
        return CDHSBusSwitch;
    }

    public Parameter getCOM3V3VoltageLine() {
        return COM3V3VoltageLine;
    }

    public Parameter getCOM5VoltageLine() {
        return COM5VoltageLine;
    }

    public Parameter getPL3V3VoltageLine() {
        return PL3V3VoltageLine;
    }

    public Parameter getPL5VVoltageLine() {
        return PL5VVoltageLine;
    }

    public Parameter getCAMVoltage() {
        return CAMVoltage;
    }

    public Parameter getADCSVoltage() {
        return ADCSVoltage;
    }

    public Parameter getBatteryACharging() {
        return batteryACharging;
    }

    public Parameter getBatteryADischarging() {
        return batteryADischarging;
    }

    public Parameter getBatteryBCharging() {
        return batteryBCharging;
    }

    public Parameter getBatteryBDischarging() {
        return batteryBDischarging;
    }

    public Parameter getSecondaryPowerBusA() {
        return secondaryPowerBusA;
    }

    public Parameter getSecondaryPowerBusB() {
        return secondaryPowerBusB;
    }

    public Parameter getV12LineVoltage() {
        return V12LineVoltage;
    }

    public Parameter getRegulatorA5V() {
        return regulatorA5V;
    }

    public Parameter getRegulatorB5V() {
        return regulatorB5V;
    }

    public Parameter getLineVoltage5V() {
        return lineVoltage5V;
    }

    public Parameter getRegulatorA3V3() {
        return regulatorA3V3;
    }

    public Parameter getRegulatorB3V3() {
        return regulatorB3V3;
    }

    public Parameter getLineVoltage3V3() {
        return lineVoltage3V3;
    }

    public Parameter getRegulatorA12V() {
        return regulatorA12V;
    }

    public Parameter getRegulatorB12V() {
        return regulatorB12V;
    }

    public Parameter getBatteryAVoltage() {
        return batteryAVoltage;
    }

    public Parameter getBatteryBVoltage() {
        return batteryBVoltage;
    }

    public Parameter getBatteryATemperature() {
        return batteryATemperature;
    }

    public Parameter getBatteryBTemperature() {
        return batteryBTemperature;
    }

    public Parameter getAveragePowerBalane() {
        return averagePowerBalane;
    }

    public Parameter getFirmwareVersionNumber() {
        return firmwareVersionNumber;
    }

    public Parameter getNumberOfCrashes() {
        return numberOfCrashes;
    }

    public Parameter getForwardedRFPower() {
        return forwardedRFPower;
    }

    public Parameter getReflectedRFPower() {
        return reflectedRFPower;
    }

    public Parameter getReceivedSignalStrength() {
        return receivedSignalStrength;
    }

    public String getEntityId() {
        return ENTITY_ID;
    }
}
