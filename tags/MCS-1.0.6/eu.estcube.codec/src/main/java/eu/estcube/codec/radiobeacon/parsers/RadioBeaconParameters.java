package eu.estcube.codec.radiobeacon.parsers;

import org.hbird.exchange.core.EntityInstance;

/**
 * Parent class of normal and safe mode parameters. This class keeps only the
 * timestamp that is used as version of the beacon. The timestamp is read in a
 * synchronized method because in the case of multiple beacon input, the
 * parameters might be created at the same time, therefore, the versions match
 * and data is overwritten in MongoDB. In order to avoid this from happening,
 * each time the timestamp is read, it is checked whether the last time
 * timestamp was requested is the same. If this is the case, timestamp is
 * incremented by one, creating a unique version.
 * 
 * @author Kaarel Hanson
 * 
 */
public class RadioBeaconParameters {

    private static volatile long timestamp;

    protected synchronized static long getCurrentTimeStamp() {
        long now = System.currentTimeMillis();
        if (now < timestamp) {
            timestamp++;
        } else {
            if (now == timestamp) {
                now++;
            }
            timestamp = now;
        }
        return timestamp;
    }

    /**
     * Updates the version and timestamp of the entity
     * 
     * @param entity
     * @param timeStamp
     */
    protected void update(EntityInstance entity, long timeStamp) {
        entity.setTimestamp(timeStamp);
        entity.setVersion(timeStamp);
    }
}
