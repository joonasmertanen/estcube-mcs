package eu.estcube.codec.radiobeacon.parsers;

import java.util.ArrayList;
import java.util.List;

import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.core.Label;

import eu.estcube.codec.radiobeacon.parser.RadioBeaconMessageParser;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParser2ComplementInteger;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserBits;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserCalibratedDouble;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserHex;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserInteger;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserTimestamp;

public class RadioBeaconParsersSafeMode extends RadioBeaconParsers {

    
    private RadioBeaconParametersSafeMode parameters;
    
    /**
     * Creates new RadioBeaconParsersSafeMode.
     * 
     * @param idBuilder
     */
    public RadioBeaconParsersSafeMode(IdBuilder idBuilder) {
        super(idBuilder);
        parameters = new RadioBeaconParametersSafeMode();
    }

    @Override
    public List<RadioBeaconMessageParser> getParsers() {
        List<RadioBeaconMessageParser> parsers = new ArrayList<RadioBeaconMessageParser>();

        parsers.add(new RadionBeaconMessageParserHex(6, 1, parameters.getOperatingMode()));
        parsers.add(getTimestampParser());

        parsers.add(new RadionBeaconMessageParserInteger(14, 2, parameters.getErrorCode1()));
        parsers.add(new RadionBeaconMessageParserInteger(16, 2, parameters.getErrorCode2()));
        parsers.add(new RadionBeaconMessageParserInteger(18, 2, parameters.getErrorCode3()));
        parsers.add(new RadionBeaconMessageParserInteger(20, 4, parameters.getTimeInSafeMode()));
        parsers.add(new RadionBeaconMessageParserCalibratedDouble(24, 2,
                parameters.getMainBusVoltage(), 0.001240978485204 * 16, -0.001670625667674));

        parsers.add(new RadionBeaconMessageParserBits(26, 2, 0x80, 7, parameters.getCDHSProcessorA()));
        parsers.add(new RadionBeaconMessageParserBits(26, 2, 0x40, 6, parameters.getCDHSProcessorB()));
        parsers.add(new RadionBeaconMessageParserBits(26, 2, 0x20, 5, parameters.getCDHSBusSwitch()));
        parsers.add(new RadionBeaconMessageParserBits(26, 2, 0x10, 4,
                parameters.getCOM3V3VoltageLine()));
        parsers.add(new RadionBeaconMessageParserBits(26, 2, 0x8, 3, parameters.getCOM5VoltageLine()));
        parsers.add(new RadionBeaconMessageParserBits(26, 2, 0x4, 2, parameters.getPL3V3VoltageLine()));
        parsers.add(new RadionBeaconMessageParserBits(26, 2, 0x2, 1, parameters.getPL5VVoltageLine()));
        parsers.add(new RadionBeaconMessageParserBits(26, 2, 0x1, 0, parameters.getCAMVoltage()));

        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x80, 7, parameters.getADCSVoltage()));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x40, 6, parameters.getBatteryACharging()));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x20, 5,
                parameters.getBatteryADischarging()));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x10, 4, parameters.getBatteryBCharging()));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x8, 3,
                parameters.getBatteryBDischarging()));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x4, 2,
                parameters.getSecondaryPowerBusA()));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x2, 1,
                parameters.getSecondaryPowerBusB()));
        parsers.add(new RadionBeaconMessageParserBits(28, 2, 0x1, 0, parameters.getV12LineVoltage()));

        parsers.add(new RadionBeaconMessageParserBits(30, 2, 0x80, 7, parameters.getRegulatorA5V()));
        parsers.add(new RadionBeaconMessageParserBits(30, 2, 0x40, 6, parameters.getRegulatorB5V()));
        parsers.add(new RadionBeaconMessageParserBits(30, 2, 0x20, 5, parameters.getLineVoltage5V()));
        parsers.add(new RadionBeaconMessageParserBits(30, 2, 0x10, 4, parameters.getRegulatorA3V3()));
        parsers.add(new RadionBeaconMessageParserBits(30, 2, 0x8, 3, parameters.getRegulatorB3V3()));
        parsers.add(new RadionBeaconMessageParserBits(30, 2, 0x4, 2, parameters.getLineVoltage3V3()));
        parsers.add(new RadionBeaconMessageParserBits(30, 2, 0x2, 1, parameters.getRegulatorA12V()));
        parsers.add(new RadionBeaconMessageParserBits(30, 2, 0x1, 0, parameters.getRegulatorB12V()));

        parsers.add(new RadionBeaconMessageParserCalibratedDouble(32, 2,
                parameters.getBatteryAVoltage(),
                0.017686154075981, 0.003877355151542));
        parsers.add(new RadionBeaconMessageParserCalibratedDouble(34, 2,
                parameters.getBatteryBVoltage(),
                0.017645083640731, 0.013681971347675));
        parsers.add(new RadionBeaconMessageParserCalibratedDouble(36, 2,
                parameters.getBatteryATemperature(), 0.7139, -61.1111));
        parsers.add(new RadionBeaconMessageParserCalibratedDouble(38, 2,
                parameters.getBatteryBTemperature(), 0.7139, -61.1111));
        parsers.add(new RadionBeaconMessageParser2ComplementInteger(40, 2,
                parameters.getAveragePowerBalane()));
        parsers.add(new RadionBeaconMessageParserInteger(42, 1, parameters.getFirmwareVersionNumber()));
        parsers.add(new RadionBeaconMessageParserInteger(43, 1, parameters.getNumberOfCrashes()));
        parsers.add(new RadionBeaconMessageParser2ComplementInteger(44, 2,
                parameters.getForwardedRFPower()));
        parsers.add(new RadionBeaconMessageParser2ComplementInteger(46, 2,
                parameters.getReflectedRFPower()));
        parsers.add(new RadionBeaconMessageParser2ComplementInteger(48, 2,
                parameters.getReceivedSignalStrength()));
        return parsers;
    }

    @Override
    public Label getRadioBeacon() {
        return parameters.getRadioBeacon();
    }

    @Override
    public String getEntityID() {
        return parameters.getEntityId();
    }

    @Override
    public RadionBeaconMessageParserTimestamp getTimestampParser() {
        return new RadionBeaconMessageParserTimestamp(7, 7, parameters.getTimestamp());
    }

    @Override
    public void updateParameters() {
        parameters.updateParameters();
    }
}
