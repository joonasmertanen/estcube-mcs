package eu.estcube.commanding.api;

import eu.estcube.commanding.analysis.OverPassType;

/**
 * 
 * @author Ivar Mahhonin
 * 
 */

public interface RotatorTypeInterface {

	OptimizeInterface getOptimizer(OverPassType type);
}
