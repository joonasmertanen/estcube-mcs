package eu.estcube.commanding.optimizators;

import java.util.List;

import org.hbird.exchange.navigation.PointingData;

import eu.estcube.commanding.analysis.OptimizationType;
import eu.estcube.commanding.api.OptimizeInterface;
import eu.estcube.commanding.rotator1.Rotator1CrossElHigh;
import eu.estcube.commanding.rotator1.Rotator1CrossElLow;
import eu.estcube.commanding.rotator1.Rotator1NoCrossElHigh;
import eu.estcube.commanding.rotator1.Rotator1NoCrossElLow;
import eu.estcube.commanding.rotator2.Rotator2CrossElHigh;
import eu.estcube.commanding.rotator2.Rotator2CrossElLow;

/**
 * Optimizing satellite trajectory coordinates in this way,
 * so the rotator could track satellite constantly.
 * 
 * @author Ivar Mahhonin
 * 
 */
public class Optimization implements OptimizeInterface {
	OptimizationType optType;
	double maxAz;
	double maxEl;
	double receivingSector;

	public Optimization(final OptimizationType optType, final double maxAz,
			final double maxEl, final double receivingSector) {
		super();
		this.optType = optType;
		this.maxAz = maxAz;
		this.maxEl = maxEl;
		this.receivingSector = receivingSector;
	}

	public OptimizationType getOptType() {
		return optType;
	}

	@Override
	public List<PointingData> optimize(List<PointingData> coordinates) {
		switch (optType) {
		case ROT_1_CROSS_EL_HIGH:
			final Rotator1CrossElHigh caseInit1 = new Rotator1CrossElHigh(
					receivingSector, maxAz, maxEl);
			coordinates = caseInit1.optimize(coordinates);
			break;
		case ROT_1_CROSS_EL_LOW:
			final Rotator1CrossElLow caseInit2 = new Rotator1CrossElLow(
					receivingSector, maxAz, maxEl);
			coordinates = caseInit2.optimize(coordinates);
			break;
		case ROT_1_NO_CROSS_EL_HIGH:
			final Rotator1NoCrossElHigh caseInit3 = new Rotator1NoCrossElHigh(
					receivingSector, maxAz, maxEl);
			coordinates = caseInit3.optimize(coordinates);
			break;
		case ROT_1_NO_CROSS_EL_LOW:
			final Rotator1NoCrossElLow caseInit4 = new Rotator1NoCrossElLow(
					receivingSector, maxAz, maxEl);
			coordinates = caseInit4.optimize(coordinates);
			break;

		case ROT_2_CROSS_EL_LOW:
			final Rotator2CrossElLow rot2caseInit2 = new Rotator2CrossElLow(
					receivingSector, maxAz, maxEl);
			coordinates = rot2caseInit2.optimize(coordinates);
			break;

		case ROT_2_CROSS_EL_HIGH:
			final Rotator2CrossElHigh rot2caseInit4 = new Rotator2CrossElHigh(
					receivingSector, maxAz, maxEl);
			coordinates = rot2caseInit4.optimize(coordinates);
			break;

		case NOTHING:
			break;
		}
		return coordinates;

	}

}