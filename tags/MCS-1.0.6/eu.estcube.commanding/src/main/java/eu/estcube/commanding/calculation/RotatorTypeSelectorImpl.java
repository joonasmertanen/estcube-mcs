/** 
 *
 */
package eu.estcube.commanding.calculation;

import org.hbird.business.groundstation.configuration.RotatorDriverConfiguration;
import org.springframework.stereotype.Component;

import eu.estcube.commanding.api.RotatorTypeInterface;
import eu.estcube.commanding.api.RotatorTypeSelectorInterface;
import eu.estcube.commanding.rotator1.Rotator1;
import eu.estcube.commanding.rotator2.Rotator2;

@Component
public class RotatorTypeSelectorImpl implements RotatorTypeSelectorInterface {

	/** @{inheritDoc . */
	@Override
	public RotatorTypeInterface selectFor(
			final RotatorDriverConfiguration config) {
		final RotatorTypeInterface rotatorType1 = new Rotator1(7.5);
		final RotatorTypeInterface rotatorType2 = new Rotator2(7.5);

		if (config.getMaxAzimuth() == 360 && config.getMaxElevation() == 180) {
			return rotatorType1;
		} else {
			return rotatorType2;
		}

	}
}
