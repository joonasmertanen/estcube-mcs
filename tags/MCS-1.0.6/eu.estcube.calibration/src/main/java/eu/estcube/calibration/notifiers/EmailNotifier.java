package eu.estcube.calibration.notifiers;

import java.security.GeneralSecurityException;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.hbird.exchange.core.CalibratedParameter;
import org.hbird.exchange.core.CalibratedParameter.Level;
import org.hbird.exchange.core.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.estcube.calibration.data.CalibrationExpression;
import eu.estcube.calibration.data.LimitsData;
import eu.estcube.common.utils.Emailer;

@Component
public class EmailNotifier {

    private static final Logger LOG = LoggerFactory.getLogger(EmailNotifier.class);

    @Value("${email.to}")
    private String to;

    @Autowired
    private Emailer emailer;

    public void notify(CalibrationExpression pl, Parameter parameter, Level levelBefore, Level levelNow) {
        if (!pl.hasLimits() || to == null || to.isEmpty())
            return;
        LimitsData ld = pl.getLimits();
        String format = pl.getFormatPattern();
        boolean increasing = levelNow.value > levelBefore.value;

        // Parameter value has gone to/gone out of all/returned to X limits
        String title = (parameter instanceof CalibratedParameter ? "Calibrated p" : "P") + "arameter \""
                + getComponentName(pl.getID()) + "\" value has "
                + (increasing ? "gone" : "returned") + " "
                + getLimitName(levelNow, (increasing ? "into " : "to ")) + " limits (" + parameter.getValue() + ")";
        StringBuilder sb = new StringBuilder();
        sb.append("---LimitChecking notification---").append("\n");
        sb.append("Parameter: ").append(pl.getID()).append("\n");
        sb.append("Value: ").append(parseValue(parameter.getValue().doubleValue(), format)).append("\n\n");
        sb.append("Sane lower: ").append(parseValue(ld.getSaneLower(), format)).append("\n");
        sb.append("Hard lower: ").append(parseValue(ld.getHardLower(), format)).append("\n");
        sb.append("Soft lower: ").append(parseValue(ld.getSoftLower(), format)).append("\n");
        sb.append("Soft upper: ").append(parseValue(ld.getSoftUpper(), format)).append("\n");
        sb.append("Hard upper: ").append(parseValue(ld.getHardUpper(), format)).append("\n");
        sb.append("Sane upper: ").append(parseValue(ld.getSaneUpper(), format)).append("\n");
        sb.append("Current limit zone: ").append(getLimitName(levelNow, "")).append("\n");
        sb.append("Previous limit zone: ").append(getLimitName(levelBefore, "")).append("\n\n");
        sb.append("Parameter data: ").append(parameter.toString()).append("\n");

        try {
            emailer.sendEmail(to, title, sb.toString());
        } catch (AddressException e) {
            LOG.error("Failed to send email", e);
        } catch (MessagingException e) {
            LOG.error("Failed to send email", e);
        } catch (GeneralSecurityException e) {
            LOG.error("Failed to send email", e);
        }
    }

    private String getComponentName(String id) {
        return id.substring(id.lastIndexOf("/") + 1);
    }

    private String parseValue(Double value, String format) {
        if (value == null)
            return "none";
        String valueStr = value.toString();
        if (format != null && !format.isEmpty()) {
            valueStr += " (formatted: " + String.format(format, value.doubleValue()) + ")";
        }
        return valueStr;
    }

    private String getLimitName(Level level, String s) {
        switch (level) {
            case SOFT:
                return s + "soft";
            case HARD:
                return s + "hard";
            case SANE:
                return s + "sane";
            default:
                return "out of all";
        }
    }
}
