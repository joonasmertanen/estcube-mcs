package eu.estcube.camel.serial;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.util.Arrays;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class DefaultCodecFactoryTest {

    private static final byte[] BYTES = new byte[] { 0x0F, 0x00, 0x00, 0x0D };

    @Mock
    private IoSession session;

    @Mock
    private ProtocolEncoderOutput encoderOut;

    @Mock
    private ProtocolDecoderOutput decoderOut;

    private IoBuffer buffer;

    private DefaultCodecFactory factory;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        factory = new DefaultCodecFactory();
        inOrder = inOrder(session, encoderOut, decoderOut);
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.DefaultCodecFactory#getEncoder(org.apache.mina.core.session.IoSession)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testGetEncoder() throws Exception {
        ProtocolEncoder e1 = factory.getEncoder(session);
        ProtocolEncoder e2 = factory.getEncoder(session);
        assertNotNull(e1);
        assertNotNull(e2);
        assertSame(e1, e2);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.DefaultCodecFactory#getDecoder(org.apache.mina.core.session.IoSession)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testGetDecoder() throws Exception {
        ProtocolDecoder d1 = factory.getDecoder(session);
        ProtocolDecoder d2 = factory.getDecoder(session);
        assertNotNull(d1);
        assertNotNull(d2);
        assertSame(d1, d2);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.DefaultCodecFactory#createDecoder()}.
     * 
     * @throws Exception
     */
    @Test
    public void testCreateDecoder() throws Exception {
        ProtocolDecoder decoder = factory.createDecoder();
        buffer = IoBuffer.wrap(BYTES);
        decoder.decode(session, buffer, decoderOut);
        ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        inOrder.verify(decoderOut, times(1)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        assertTrue(Arrays.equals(BYTES, captor.getValue()));
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.DefaultCodecFactory#createEncoder()}.
     * 
     * @throws Exception
     */
    @Test
    public void testCreateEncoder() throws Exception {
        ProtocolEncoder encoder = factory.createEncoder();
        encoder.encode(session, BYTES, encoderOut);
        ArgumentCaptor<IoBuffer> captor = ArgumentCaptor.forClass(IoBuffer.class);
        inOrder.verify(encoderOut, times(1)).write(captor.capture());
        inOrder.verifyNoMoreInteractions();
        IoBuffer buffer = captor.getValue();
        byte[] result = new byte[buffer.remaining()];
        buffer.get(result);
        assertTrue(Arrays.equals(BYTES, result));
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.DefaultCodecFactory#createEncoder()}.
     * 
     * @throws Exception
     */
    @Test(expected = RuntimeException.class)
    public void testCreateEncoderWrongType() throws Exception {
        ProtocolEncoder encoder = factory.createEncoder();
        encoder.encode(session, new Object(), encoderOut);
    }
}
