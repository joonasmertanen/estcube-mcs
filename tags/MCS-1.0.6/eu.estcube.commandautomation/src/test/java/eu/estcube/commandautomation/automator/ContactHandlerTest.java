package eu.estcube.commandautomation.automator;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.commandautomation.automator.event.OnContactHandlerFinished;
import eu.estcube.commandautomation.automator.helpers.FakeAsyncCommandHandler;
import eu.estcube.commandautomation.db.Command;
import eu.estcube.commandautomation.db.Contact;

@RunWith(MockitoJUnitRunner.class)
public class ContactHandlerTest {

    private static final Logger LOG = LoggerFactory.getLogger(ContactHandler.class);

    Contact contactTwoCommands;
    Contact contactOneCommand;

    private Command command1;
    private Command command2;

    private FakeAsyncCommandHandler commandHandler1;
    private FakeAsyncCommandHandler commandHandler2;

    @Mock
    private OnContactHandlerFinished onContactHandlerFinished;

    @Before
    public void setup() {

        LOG.debug("-------------------------------");

        contactOneCommand = new Contact();
        contactOneCommand.setTrackingId("A1");

        contactTwoCommands = new Contact();
        contactTwoCommands.setTrackingId("A1");

        command1 = new Command();
        command1.setName("C1");
        command1.setMaxExecutionTime(1000);
        command1.setWaitAfterEndTime(1000);
        contactTwoCommands.getCommands().add(command1);
        contactOneCommand.getCommands().add(command1);
        commandHandler1 = new FakeAsyncCommandHandler(command1);

        command2 = new Command();
        command2.setName("C2");
        command2.setMaxExecutionTime(1000);
        command2.setWaitAfterEndTime(1000);
        commandHandler2 = new FakeAsyncCommandHandler(command2);
        contactTwoCommands.getCommands().add(command2);

    }

    @Test(timeout = 1000)
    public void testCommandSuccessfulExecution() throws InterruptedException {
        final ContactHandler contactHandler = new ContactHandler(contactOneCommand, new MockCommandHandlerFactory());
        contactHandler.setOnFinished(onContactHandlerFinished);

        assertEquals(null, contactHandler.getState());
        assertEquals(0, commandHandler1.prepareCalled);

        // ----------------------------
        // PREPARE
        contactHandler.prepare();

        assertEquals(ContactHandler.State.PREPARE_RECEIVED, contactHandler.getState());

        assertEquals(1, commandHandler1.prepareCalled);
        assertEquals(0, commandHandler1.prepareFinishedCalled);

        synchronized (commandHandler1) {
            commandHandler1.wait(200);
        }
        assertEquals(1, commandHandler1.prepareFinishedCalled);

        // ----------------------------
        // START
        contactHandler.start();

        assertEquals(ContactHandler.State.AOS_RECEIVED, contactHandler.getState());

        assertEquals(1, commandHandler1.startCalled);
        assertEquals(0, commandHandler1.startFinishedCalled);
        assertEquals("", commandHandler1.commandResult);

        // 1 startFinished
        synchronized (commandHandler1) {
            commandHandler1.wait(200);
        }
        assertEquals(1, commandHandler1.startFinishedCalled);
        assertEquals("", commandHandler1.commandResult);

        // 1 onEnded
        synchronized (commandHandler1) {
            commandHandler1.wait(200);
        }
        assertEquals("FINISHED", commandHandler1.commandResult);

        // ----------------------------
        // END
        contactHandler.end();

        assertEquals(ContactHandler.State.FINISHED, contactHandler.getState());
        assertEquals(0, commandHandler1.endCalled);
        verify(onContactHandlerFinished, times(1)).execute();
    }

    @Test(timeout = 1000)
    public void testLOSBeforeCommandFinishedExecution() throws InterruptedException {
        final ContactHandler contactHandler = new ContactHandler(contactOneCommand, new MockCommandHandlerFactory());
        contactHandler.setOnFinished(onContactHandlerFinished);

        commandHandler1.startWaitTime = 1000;

        assertEquals(null, contactHandler.getState());
        assertEquals(0, commandHandler1.prepareCalled);

        // ----------------------------
        // PREPARE
        contactHandler.prepare();

        assertEquals(ContactHandler.State.PREPARE_RECEIVED, contactHandler.getState());

        assertEquals(1, commandHandler1.prepareCalled);
        assertEquals(0, commandHandler1.prepareFinishedCalled);

        synchronized (commandHandler1) {
            commandHandler1.wait(200);
        }
        assertEquals(1, commandHandler1.prepareFinishedCalled);

        // ----------------------------
        // START
        contactHandler.start();

        assertEquals(ContactHandler.State.AOS_RECEIVED, contactHandler.getState());

        assertEquals(1, commandHandler1.startCalled);
        assertEquals(0, commandHandler1.startFinishedCalled);
        assertEquals("", commandHandler1.commandResult);
        assertEquals(0, commandHandler1.endCalled);
        assertEquals(ContactHandler.State.AOS_RECEIVED, contactHandler.getState());

        // ----------------------------
        // END
        contactHandler.end();
        assertEquals(1, commandHandler1.endCalled);
        assertEquals("", commandHandler1.commandResult);

        // 1 endFinished
        synchronized (commandHandler1) {
            commandHandler1.wait(200);
        }
        assertEquals(1, commandHandler1.endFinishedCalled);
        assertEquals(0, commandHandler1.startFinishedCalled);
        assertEquals("ENDED", commandHandler1.commandResult);

        assertEquals(ContactHandler.State.FINISHED, contactHandler.getState());
        verify(onContactHandlerFinished, times(1)).execute();
    }

    @Test(timeout = 1000)
    public void testCommandTakingTooLongToExecute() throws InterruptedException {
        final ContactHandler contactHandler = new ContactHandler(contactOneCommand, new MockCommandHandlerFactory());
        contactHandler.setOnFinished(onContactHandlerFinished);

        commandHandler1.startWaitTime = 1000;
        command1.setMaxExecutionTime(1);

        assertEquals(null, contactHandler.getState());
        assertEquals(0, commandHandler1.prepareCalled);

        // ----------------------------
        // PREPARE
        contactHandler.prepare();

        assertEquals(ContactHandler.State.PREPARE_RECEIVED, contactHandler.getState());

        assertEquals(1, commandHandler1.prepareCalled);
        assertEquals(0, commandHandler1.prepareFinishedCalled);

        synchronized (commandHandler1) {
            commandHandler1.wait(200);
        }
        assertEquals(1, commandHandler1.prepareFinishedCalled);

        // ----------------------------
        // START
        contactHandler.start();

        assertEquals(ContactHandler.State.AOS_RECEIVED, contactHandler.getState());

        assertEquals(1, commandHandler1.startCalled);
        assertEquals(0, commandHandler1.startFinishedCalled);
        assertEquals("", commandHandler1.commandResult);
        assertEquals(0, commandHandler1.endCalled);
        assertEquals(ContactHandler.State.AOS_RECEIVED, contactHandler.getState());

        synchronized (commandHandler1) {
            commandHandler1.wait(200);
        }

        assertEquals(0, commandHandler1.startFinishedCalled);
        assertEquals(1, commandHandler1.killCalled);
        assertEquals(1, commandHandler1.killFinishedCalled);

        // ----------------------------
        // END
        contactHandler.end();

        assertEquals(ContactHandler.State.FINISHED, contactHandler.getState());
        assertEquals(0, commandHandler1.endCalled);
        verify(onContactHandlerFinished, times(1)).execute();
    }

    @Test(timeout = 1000)
    public void testCommandEndTakingTooLong() throws InterruptedException {
        final ContactHandler contactHandler = new ContactHandler(contactOneCommand, new MockCommandHandlerFactory());
        contactHandler.setOnFinished(onContactHandlerFinished);

        commandHandler1.endWaitTime = 1000;
        commandHandler1.startWaitTime = 1000;
        command1.setWaitAfterEndTime(1);

        assertEquals(null, contactHandler.getState());
        assertEquals(0, commandHandler1.prepareCalled);

        // ----------------------------
        // PREPARE
        contactHandler.prepare();
        synchronized (commandHandler1) {
            commandHandler1.wait(200);
        }

        // ----------------------------
        // START
        contactHandler.start();
        assertEquals(1, commandHandler1.startCalled);

        // ----------------------------
        // END
        contactHandler.end();

        assertEquals(1, commandHandler1.endCalled);
        assertEquals("", commandHandler1.commandResult);

        // 1 endFinished
        synchronized (commandHandler1) {
            commandHandler1.wait(200);
        }
        assertEquals(0, commandHandler1.endFinishedCalled);
        assertEquals(0, commandHandler1.startFinishedCalled);
        assertEquals(1, commandHandler1.killCalled);
        assertEquals(1, commandHandler1.killFinishedCalled);
        assertEquals("KILLED", commandHandler1.commandResult);

        assertEquals(ContactHandler.State.FINISHED, contactHandler.getState());
        verify(onContactHandlerFinished, times(1)).execute();
    }

    @Test(timeout = 1000)
    public void testTwoCommandSuccessfulExecution() throws InterruptedException {

        final ContactHandler contactHandler = new ContactHandler(contactTwoCommands, new MockCommandHandlerFactory());
        contactHandler.setOnFinished(onContactHandlerFinished);

        assertEquals(null, contactHandler.getState());

        // ----------------------------
        // PREPARE
        contactHandler.prepare();

        assertEquals(ContactHandler.State.PREPARE_RECEIVED, contactHandler.getState());

        assertEquals(1, commandHandler1.prepareCalled);
        assertEquals(0, commandHandler1.prepareFinishedCalled);

        assertEquals(1, commandHandler2.prepareCalled);
        assertEquals(0, commandHandler2.prepareFinishedCalled);

        synchronized (commandHandler1) {
            commandHandler1.wait(200);
        }
        assertEquals(1, commandHandler1.prepareFinishedCalled);

        synchronized (commandHandler2) {
            commandHandler2.wait(200);
        }
        assertEquals(1, commandHandler2.prepareFinishedCalled);

        // ----------------------------
        // START
        contactHandler.start();

        assertEquals(ContactHandler.State.AOS_RECEIVED, contactHandler.getState());

        assertEquals(1, commandHandler1.startCalled);
        assertEquals(0, commandHandler1.startFinishedCalled);
        assertEquals("", commandHandler1.commandResult);

        assertEquals(0, commandHandler2.startCalled);
        assertEquals(0, commandHandler2.startFinishedCalled);
        assertEquals("", commandHandler2.commandResult);

        // 1 startFinished
        synchronized (commandHandler1) {
            commandHandler1.wait(200);
        }
        assertEquals(1, commandHandler1.startFinishedCalled);
        assertEquals("", commandHandler1.commandResult);
        assertEquals(0, commandHandler2.startCalled);

        // 1 onEnded
        synchronized (commandHandler1) {
            commandHandler1.wait(200);
        }
        assertEquals("FINISHED", commandHandler1.commandResult);
        assertEquals(1, commandHandler2.startCalled);

        // 2 startFinished
        synchronized (commandHandler2) {
            commandHandler2.wait(200);
        }
        assertEquals(1, commandHandler2.startFinishedCalled);
        assertEquals("", commandHandler2.commandResult);

        // 2 onEnded
        synchronized (commandHandler2) {
            commandHandler2.wait(200);
        }
        assertEquals("FINISHED", commandHandler2.commandResult);

        // ----------------------------
        // END
        contactHandler.end();

        assertEquals(ContactHandler.State.FINISHED, contactHandler.getState());
        assertEquals(0, commandHandler1.endCalled);
        assertEquals(0, commandHandler2.endCalled);
        verify(onContactHandlerFinished, times(1)).execute();

    }

    private class MockCommandHandlerFactory implements CommandHandlerFactory {

        public CommandHandler create(Command command) {
            if (command.getName().equals("C1")) {
                return commandHandler1;
            } else {
                return commandHandler2;
            }

        }
    }
}
