package eu.estcube.proxy.converter;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

import org.springframework.stereotype.Component;

import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.proxy.model.Ax25UiFrameRemote;

/**
 * Converts remote packet received from 3rd party groundstations to Ax25UIFrame
 * so that it can be handled in MCS
 * 
 * @author Kaarel Hanson
 * @since 13.03.2014
 */
@Component
public class RemoteToAx25UiFrameConverter {

	public Ax25UIFrame convert(Ax25UiFrameRemote remote) {
		Ax25UIFrame result = new Ax25UIFrame();
		byte[] bytes = remote.getData();
		if (bytes == null) {
			throw new IllegalArgumentException("Frame data is null");
		}
		ByteBuffer buffer = ByteBuffer.wrap(bytes);

		try {
			// read dest address
			byte[] destAddr = new byte[Ax25UIFrame.DEST_ADDR_LEN];
			buffer.get(destAddr);
			result.setDestAddr(destAddr);

			// read src address
			byte[] srcAddr = new byte[Ax25UIFrame.SRC_ADDR_LEN];
			buffer.get(srcAddr);
			result.setSrcAddr(srcAddr);

			// read ctrl
			result.setCtrl(buffer.get());

			// read pid
			result.setPid(buffer.get());

			// read info
			byte[] info = new byte[buffer.remaining()];
			buffer.get(info);
			result.setInfo(info);
		} catch (BufferUnderflowException bue) {
			result.setErrorTooShort(true);
		}
		return result;
	}
}
