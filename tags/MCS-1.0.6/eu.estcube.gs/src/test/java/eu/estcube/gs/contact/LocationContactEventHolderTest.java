/** 
 *
 */
package eu.estcube.gs.contact;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import org.hbird.exchange.navigation.LocationContactEvent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import eu.estcube.gs.contact.LocationContactEventHolder;

import eu.estcube.gs.contact.LocationContactEventHolder;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class LocationContactEventHolderTest {

    private static final long NOW = System.currentTimeMillis();
    private static final String CONTACT_ID = "GS:SAT:123";

    @Mock
    private LocationContactEvent event;

    private LocationContactEventHolder holder;

    private InOrder inOrder;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        holder = new LocationContactEventHolder();
        inOrder = inOrder(event);
    }

    @Test
    public void testGetContactIdNullEvent() throws Exception {
        holder.handle(null);
        assertNull(holder.getContactId(NOW));
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void testGetContactIdBeforeEvent() throws Exception {
        holder.handle(event);
        when(event.getStartTime()).thenReturn(NOW + 1);
        assertNull(holder.getContactId(NOW));
        inOrder.verify(event, times(1)).getStartTime();
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void testGetContactIdAfterEvent() throws Exception {
        holder.handle(event);
        when(event.getStartTime()).thenReturn(NOW - 20);
        when(event.getEndTime()).thenReturn(NOW - 10);
        assertNull(holder.getContactId(NOW));
        inOrder.verify(event, times(1)).getStartTime();
        inOrder.verify(event, times(1)).getEndTime();
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void testGetContactId() throws Exception {
        holder.handle(event);
        when(event.getStartTime()).thenReturn(NOW - 1);
        when(event.getEndTime()).thenReturn(NOW + 1);
        when(event.getInstanceID()).thenReturn(CONTACT_ID);
        assertEquals(CONTACT_ID, holder.getContactId(NOW));
        inOrder.verify(event, times(1)).getStartTime();
        inOrder.verify(event, times(1)).getEndTime();
        inOrder.verify(event, times(1)).getInstanceID();
        inOrder.verifyNoMoreInteractions();
    }
}
