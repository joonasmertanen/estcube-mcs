package eu.estcube.gs;

import org.hbird.exchange.groundstation.GroundStationConfigurationBase;
import org.springframework.beans.factory.annotation.Value;

/**
 *
 */
public class GsDriverConfiguration extends GroundStationConfigurationBase {

    private static final long serialVersionUID = 3805727376792864147L;

    @Value("${satellite.id}")
    private String satelliteId;

    // TODO: However, SDR could possibly deal with many satellites at once, also
    // TNC could potentially listen to 2 satellites at once.
    public String getSatelliteId() {
        return satelliteId;
    }

    // TODO: Should this be used to set the satellite the groundstation is
    // currently tracking?
    public void setSatelliteId(String satelliteId) {
        this.satelliteId = satelliteId;
    }
}
