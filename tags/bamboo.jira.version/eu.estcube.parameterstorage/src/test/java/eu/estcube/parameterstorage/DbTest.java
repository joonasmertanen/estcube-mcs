package eu.estcube.parameterstorage;

import static org.junit.Assert.*;

import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.sql.DataSource;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.dbunit.Assertion;
import org.dbunit.DatabaseTestCase;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.DatabaseDataSourceConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;

import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryParameter;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/META-INF/spring/test-context.xml" })
public class DbTest extends DatabaseTestCase {

	
    @Autowired
    private TelemetryObjectToDb toDb;
    @Autowired
    private DbToTelemetryObject fromDb;
    @Autowired
    private DataSource dataSource;
    private IDatabaseConnection connection;
    private String telObjName = "telObjName";
    private String source = "ES5EC";
    private String device = "rotator";
    private TelemetryObject telemetryObject;
    private TelemetryObject telemetryObject2;
    private TelemetryObject telemetryObjectNewer;
    private TelemetryObject telemetryObjectOlder;
    private TelemetryParameter telemetryParameter;
    private TelemetryParameter telemetryParameter2;
    private String telParName = "telParName";
    private String telParName2 = "telParName2";
    private String value = "123";
    private String value2 = "someValue";

    private IDataSet dataSetFromXml;
    private FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();

    private String telObjTableName = "TELEMETRYOBJECTS";
    private String paramTableName = "PARAMETERS";

    private Exchange ex;
    private Message message;

    private Object answer;
    private Vector<Object> headers = new Vector<Object>();

    private URL dataSetDescUrl;
    private int telObjCount;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Before
    public void setUp() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2011, 10, 31, 15, 30); 
        Date date1 = calendar.getTime();
        calendar.set(2011, 10, 31, 15, 32);
        Date date2 = calendar.getTime();
        
        getConnection();
        getDataSet();
        telemetryObject = new TelemetryObject(telObjName, new Date());
        telemetryObject.setSource(source);
        telemetryObject.setDevice(device);

        telemetryObject2 = new TelemetryObject(telObjName, new Date());
        telemetryObject2.setSource(source);
        telemetryObject2.setDevice(device);
        
        telemetryObjectOlder = new TelemetryObject(telObjName, date1);
        telemetryObjectOlder.setSource(source);
        telemetryObjectOlder.setDevice(device);
        telemetryObjectNewer = new TelemetryObject(telObjName, date2);
        telemetryObjectNewer.setSource(source);
        telemetryObjectNewer.setDevice(device);
        
        telemetryParameter = new TelemetryParameter(telParName, value);
        telemetryParameter2 = new TelemetryParameter(telParName2, value2);

        telemetryObject.addParameter(telemetryParameter);
        telemetryObject2.addParameter(telemetryParameter2);
        telemetryObjectNewer.addParameter(telemetryParameter);
        
        ex = Mockito.mock(Exchange.class);
        message = Mockito.mock(Message.class);

        Mockito.when(ex.getIn()).thenReturn(message);
        Mockito.when(ex.getOut()).thenReturn(message);

        Mockito.doAnswer(new Answer<Object>() {
            public Object answer(InvocationOnMock invocation) {
                answer = invocation.getArguments()[0];
                return answer;
            }
        }).when(message).setBody(Mockito.any());
        
        Mockito.doAnswer(new Answer<Object>() {
            public Object answer(InvocationOnMock invocation) {
                for(Object obj:invocation.getArguments())
                    headers.add(obj);
                return headers;
            }
        }).when(message).setHeader(Mockito.anyString(), Mockito.any());
    }
    
    @Test
    public void testReadEmptyDb() throws Exception {
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObject);
        Mockito.when(message.getHeader("groundStationID")).thenReturn("ES5EC");
        Mockito.when(message.getHeader("device")).thenReturn("rotator");
        
        fromDb.process(ex);

        /*Empty ArrayList as answer.*/
        assertEquals(0, ((ArrayList) answer).size());
        /*Header was set.*/
        assertEquals(true, headers.get(0).equals("fromPS"));
        assertEquals(true, headers.get(1).equals("true"));
    }
    
  @SuppressWarnings("unchecked")
  @Test
  public void testAddObject() throws Exception {
      Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObject);
      Mockito.when(message.getHeader("groundStationID")).thenReturn("ES5EC");
      Mockito.when(message.getHeader("device")).thenReturn("rotator");

      /* Populate db with some data. */
      DatabaseOperation.INSERT.execute(connection, dataSetFromXml);
      
      IDataSet databaseDataSet = getConnection().createDataSet();
      ITable actualTelemetryObjectsTable = databaseDataSet.getTable("TELEMETRYOBJECTS");
      telObjCount = actualTelemetryObjectsTable.getRowCount();
      ITable expectedTelemetryObjectsTable = dataSetFromXml.getTable("TELEMETRYOBJECTS");
      ITable actualParametersTable = databaseDataSet.getTable("PARAMETERS");
      ITable expectedParametersTable = dataSetFromXml.getTable("PARAMETERS");

      /* Correct data in db. */
      Assertion.assertEquals(expectedTelemetryObjectsTable, actualTelemetryObjectsTable);
      Assertion.assertEquals(expectedParametersTable, actualParametersTable);

      /* Add a different telemetryObject to db. */
      toDb.process(ex);
      /* Get all telemetryObjects from db. */
      fromDb.process(ex);

      /* Now we have original telemetryObjects + 1 */
      assertEquals(telObjCount + 1, ((ArrayList<TelemetryObject>) answer).size());
      
      /* All telemetryObjects from db. */
      ArrayList<TelemetryObject> telemetryObjects = (ArrayList<TelemetryObject>) answer;

      /* Check that the last telemetryObject is the one we added. */
      TelemetryObject telemetryObjectAnswer = telemetryObjects.get(telemetryObjects.size() - 1);
      assertEquals(telemetryObject.getName(), telemetryObjectAnswer.getName());
      assertEquals(telemetryObject.getSource(), telemetryObjectAnswer.getSource());
      assertEquals(telemetryObject.getDevice(), telemetryObjectAnswer.getDevice());

      List<TelemetryParameter> parameters1 = telemetryObject.getParams();
      List<TelemetryParameter> parameters2 = telemetryObjectAnswer.getParams();

      for (int i = 0; i < parameters1.size(); i++) {
          assertEquals(parameters1.get(i).getName(), parameters2.get(i).getName());
          assertEquals(parameters1.get(i).getValue(), parameters2.get(i).getValue());
      }
      
      /*Header was set.*/
      assertEquals(true, headers.get(0).equals("fromPS"));
      assertEquals(true, headers.get(1).equals("true"));
      
      /* Clear database */
      DatabaseOperation.TRUNCATE_TABLE.execute(connection, dataSetFromXml);
      fromDb.process(ex);
      /*Empty ArrayList as answer.*/
      assertEquals(0, ((ArrayList) answer).size());

  }
    
    
    @SuppressWarnings("unchecked")
    @Test 
    public void testAddDuplicateObject() throws Exception {
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObjectOlder);
        Mockito.when(message.getHeader("groundStationID")).thenReturn("ES5EC");
        Mockito.when(message.getHeader("device")).thenReturn("rotator");
    	/* TelemetryObject to db. */
        toDb.process(ex);
        
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObjectNewer);
        Mockito.when(message.getHeader("groundStationID")).thenReturn("ES5EC");
        Mockito.when(message.getHeader("device")).thenReturn("rotator");

        /* Add a duplicate telemetryObject to db. */
        toDb.process(ex);

        /* Get all telemetryObjects from db. */
        fromDb.process(ex);

        IDataSet databaseDataSet = getConnection().createDataSet();
        ITable actualTelemetryObjectsTable = databaseDataSet.getTable("TELEMETRYOBJECTS");
        telObjCount = actualTelemetryObjectsTable.getRowCount();

        assertEquals(telObjCount, ((ArrayList) answer).size());

        /* All telemetryObjects from db. */
        ArrayList<TelemetryObject> telemetryObjects = (ArrayList<TelemetryObject>) answer;

        /*Answer contains both older and newer version. */
        assertEquals(telemetryObjectOlder.getName(), telemetryObjects.get(0).getName());
        assertEquals(telemetryObjectOlder.getSource(), telemetryObjects.get(0).getSource());
        assertEquals(telemetryObjectOlder.getDevice(), telemetryObjects.get(0).getDevice());
        assertEquals(telemetryObjectNewer.getName(), telemetryObjects.get(1).getName());
        assertEquals(telemetryObjectNewer.getSource(), telemetryObjects.get(1).getSource());
        assertEquals(telemetryObjectNewer.getDevice(), telemetryObjects.get(1).getDevice());
        assertEquals(telParName, telemetryObjects.get(1).getParams().get(0).getName());
        assertEquals(value, telemetryObjects.get(1).getParams().get(0).getValue());
        
        /*Header was set.*/
        assertEquals(true, headers.get(0).equals("fromPS"));
        assertEquals(true, headers.get(1).equals("true"));
        
        /* Clear database */
        DatabaseOperation.TRUNCATE_TABLE.execute(connection, dataSetFromXml);
        fromDb.process(ex);
        /*Empty ArrayList as answer.*/
        assertEquals(0, ((ArrayList) answer).size());
    }
    
    @Override
    protected IDatabaseConnection getConnection() throws Exception {
        connection = new DatabaseDataSourceConnection(dataSource);
        return connection;
    }

    @Override
    protected IDataSet getDataSet() throws Exception {
        dataSetDescUrl = getClass().getClassLoader().getResource("defaultDataset");
        dataSetFromXml = builder.build(dataSetDescUrl);
        return dataSetFromXml;
    }
}
