package eu.estcube.gsconnector.commands.rotator;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRotatorConstants;

public class ResetTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private Reset createMessage = new Reset();

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.RESET);
        telemetryCommand.addParameter("Reset", 1);
        string.append("+R 1\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
