package eu.estcube.gsconnector.commands.radio;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class GetFuncTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private GetFunc createMessage = new GetFunc();
    
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand(TelemetryRadioConstants.GET_FUNC);
        telemetryCommand.addParameter("Func", "FAGC");
        string.append("+u FAGC\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
