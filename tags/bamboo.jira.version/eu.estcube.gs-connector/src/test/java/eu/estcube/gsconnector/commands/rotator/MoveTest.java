package eu.estcube.gsconnector.commands.rotator;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRotatorConstants;

public class MoveTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private Move createMessage = new Move();

    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.MOVE);
        telemetryCommand.addParameter("Direction", 2);
        telemetryCommand.addParameter("Speed", 1);
        string.append("+M 2 1\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
