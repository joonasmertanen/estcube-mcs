package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SetTransmitMode implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+X ");
        messageString.append(telemetryCommand.getParams().get("TX Mode"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("TX Passband"));
        messageString.append("\n");
        return messageString;
    }
}
