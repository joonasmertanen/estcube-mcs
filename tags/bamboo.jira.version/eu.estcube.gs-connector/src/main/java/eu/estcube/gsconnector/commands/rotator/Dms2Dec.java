package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class Dms2Dec implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+D ");
        messageString.append(telemetryCommand.getParams().get("Degrees"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Minutes"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Seconds"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("S/W"));
        messageString.append("\n");
        return messageString;
    }
}
