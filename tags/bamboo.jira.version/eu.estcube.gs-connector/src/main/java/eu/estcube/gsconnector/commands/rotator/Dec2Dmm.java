package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class Dec2Dmm implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+e ");
        messageString.append(telemetryCommand.getParams().get("Dec Deg"));
        messageString.append("\n");
        return messageString;
    }
}
