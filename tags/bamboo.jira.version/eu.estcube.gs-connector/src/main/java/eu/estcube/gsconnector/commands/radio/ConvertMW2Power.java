package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class ConvertMW2Power implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+4 ");
        messageString.append(telemetryCommand.getParams().get("Power mW"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Frequency"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Mode"));
        messageString.append("\n");
        return messageString;
    }
}
