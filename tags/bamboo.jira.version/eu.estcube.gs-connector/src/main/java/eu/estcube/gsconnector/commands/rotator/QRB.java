package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class QRB implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+B ");
        messageString.append(telemetryCommand.getParams().get("Lon 1"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Lat 1"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Lon 2"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Lat 2") + "\n");
        return messageString;
    }
}
