package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SetMode implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+M ");
        messageString.append(telemetryCommand.getParams().get("Mode"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Passband"));
        messageString.append("\n");
        return messageString;
    }
}
