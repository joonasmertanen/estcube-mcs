dojo.provide("webgui.display.StatusesInfoDisplay");

dojo.require("dijit.layout.StackContainer");

dojo.declare("webgui.display.StatusesInfoDisplay", null, {

	constructor : function() {

		console.log("StatusesInfoDisplay activated");

		var statusesInfoDisplay;

		var statusesInfoTab = new dijit.layout.ContentPane({
			id : "statusesInfoTab", // fix error -->> TIMOLE: A unique, opaque ID string that can be assigned by users or by the system. If the developer passes an ID which is known not to be unique, the specified ID is ignored and the system-generated ID is used instead.
			title : "Statuses Info",
			content : "Some raw Info!",
			preventCache: true
		});

		var tempCont;
		tempCont = dijit.byId("InfoView");

		tempCont.addChild(statusesInfoTab);

	}
});