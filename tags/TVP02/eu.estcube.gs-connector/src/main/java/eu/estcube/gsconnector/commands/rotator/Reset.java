package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class Reset implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+R ");
        messageString.append(telemetryCommand.getParams().get("Reset"));
        messageString.append("\n");
        return messageString;
    }
}
