package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class Scan implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+g ");
        messageString.append(telemetryCommand.getParams().get("Scan Fct"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Scan Channel"));
        messageString.append("\n");
        return messageString;
    }
}
