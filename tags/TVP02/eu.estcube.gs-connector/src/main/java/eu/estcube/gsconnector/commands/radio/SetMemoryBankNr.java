package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SetMemoryBankNr implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+B ");
        messageString.append(telemetryCommand.getParams().get("Bank"));
        messageString.append("\n");
        return messageString;
    }
}
