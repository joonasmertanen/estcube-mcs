package eu.estcube.gsconnector.commands.rotator;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRotatorConstants;

public class SetPositionTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private SetPosition createMessage = new SetPosition();
    
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.SET_POSITION);
        telemetryCommand.addParameter("Azimuth", 0);
        telemetryCommand.addParameter("Elevation", 0);
        string.append("+P 0 0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
