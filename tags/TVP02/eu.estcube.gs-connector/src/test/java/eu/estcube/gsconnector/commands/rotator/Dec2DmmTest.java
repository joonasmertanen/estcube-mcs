package eu.estcube.gsconnector.commands.rotator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRotatorConstants;

public class Dec2DmmTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private Dec2Dmm createMessage = new Dec2Dmm();
    
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.DEC2DMMM);
        telemetryCommand.addParameter("Dec Deg", 0.0);
        string.append("+e 0.0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
