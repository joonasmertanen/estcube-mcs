package eu.estcube.gsconnector;


import static org.junit.Assert.assertEquals;

import org.jboss.netty.buffer.ChannelBuffers;
import org.junit.Test;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;

public class EncoderTelemetryObjecRadioTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private EncoderTelemetryObjecRadio encode = new EncoderTelemetryObjecRadio();

    @Test
    public void testEncode() throws Exception
    {
        telemetryCommand = null;
        Object test = encode.encode(null, null, telemetryCommand);
        assertEquals(ChannelBuffers.EMPTY_BUFFER, test);

        telemetryCommand = new TelemetryCommand(TelemetryRadioConstants.GET_MODE);
        string.append("+m\n");
        assertEquals(string.toString(), encode.encode(null, null, telemetryCommand).toString());
    }

}
