package eu.estcube.gsconnector.commands.rotator;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRotatorConstants;

public class Dmm2DecTest {

    private StringBuilder string = new StringBuilder();
    private TelemetryCommand telemetryCommand;
    private Dmm2Dec createMessage = new Dmm2Dec();
    
    @Test
    public void testCreateMessageString() {
        telemetryCommand = new TelemetryCommand(TelemetryRotatorConstants.DMMM2DEC);
        telemetryCommand.addParameter("Degrees", 0);
        telemetryCommand.addParameter("Dec Minutes", 0.0);
        telemetryCommand.addParameter("S/W", 0);
        string.append("+E 0 0.0 0\n");
        assertEquals(string.toString(), createMessage.createMessageString(telemetryCommand).toString());
    }

}
