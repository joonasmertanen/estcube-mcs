package eu.estcube.webcamera;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.http.HttpMethods;
import org.apache.camel.spring.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import eu.estcube.domain.JMSConstants;

public class WebCamera extends RouteBuilder {
    @Value("${webcamera.url}")
    private String webCameraUrl;
    @Value("${gsName}")
    private String gsName;
    @Autowired 
    private TimeStampJpg timeStampJpg;
    private static final Logger LOG = LoggerFactory.getLogger(WebCamera.class);
    @Override
    public void configure() throws Exception {

        if(!System.getProperty("user.name").contains("notsure")) {
            System.setProperty("socksProxyHost", "127.0.0.1"); //for usage with putty tunnel, outside of ut network
            System.setProperty("socksProxyPort", "8910");
        }
        timeStampJpg.setGsName(gsName);
        
        from("timer://foo?fixedRate=true&period=1s")
            .process(new Processor() {
                public void process(Exchange ex) throws Exception { // no reply when body is null
                    ex.getOut().setBody("Image request", String.class);               
                }
            })
            .setHeader(Exchange.HTTP_METHOD, HttpMethods.POST)
                .to(webCameraUrl)
                    .process(timeStampJpg)
                        .to(JMSConstants.AMQ_WEBCAM_SEND);
    }
    
    public static void main(String[] args) throws Exception {
        LOG.info("Starting Webcamera transmitter");
        new Main().run(args);
    }

}
