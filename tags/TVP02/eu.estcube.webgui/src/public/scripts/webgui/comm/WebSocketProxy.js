dojo.provide("webgui.comm.WebSocketProxy");
dojo.require("dojox.socket");

dojo.declare("webgui.comm.WebSocketProxy", null, {

	constructor : function() {
		var webServerCache = dojox.socket("ws://localhost:9292/cache/");
		var socket = dojox.socket("ws://localhost:9292/foo/");
		var webCam = dojox.socket("ws://localhost:9292/webcam/")
		
		webServerCache.on("open", function(event){
			console.log("sent");
			webServerCache.send("Cache request.");
		});
		
		webServerCache.on("message", function(message){
			dojo.publish("fromWS", [message.data]);
		});
		
		dojo.subscribe("toWS", function(message) {
			socket.send(message);
		});
				
		socket.on("message", function(message) {
			dojo.publish("fromWS", [message.data]);
		});
		
		webCam.on("message", function(message) {
			dojo.publish("fromWebCam", [message.data]);
		});
	}
});
