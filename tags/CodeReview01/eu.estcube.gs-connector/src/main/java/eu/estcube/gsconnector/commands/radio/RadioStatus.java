package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class RadioStatus extends CommandStringBuilder {
    private StringBuilder messageString;

    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString = new StringBuilder();
        messageString.append("+");
        messageString.append("y");
        messageString.append("+");
        messageString.append("c");
        messageString.append("+");
        messageString.append("d");
        messageString.append("+");
        messageString.append("f");
        messageString.append("+");
        messageString.append("e");
        messageString.append("+");
        messageString.append("m");
        messageString.append("+");
        messageString.append("t");
        messageString.append("+");
        messageString.append("j");
        messageString.append("+");
        messageString.append("o");
        messageString.append("+");
        messageString.append("r");
        messageString.append("+");
        messageString.append("s");
        messageString.append("+");
        messageString.append("a");
        messageString.append("+");
        messageString.append("i");
        messageString.append("+");
        messageString.append("x");
        messageString.append("+");
        messageString.append("n");
        messageString.append("+");
        messageString.append("v");
        messageString.append("+");
        messageString.append("z");
        messageString.append("\n");
        return messageString;
    }
}
