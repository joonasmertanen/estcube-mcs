package eu.estcube.gsconnector.commands.rotator;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class QRB extends CommandStringBuilder {
    private StringBuilder messageString;

    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString = new StringBuilder();

        messageString.append("+");
        messageString.append("B");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Lon 1"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Lat 1"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Lon 2"));
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Lat 2"));
        messageString.append("\n");

        return messageString;
    }
}
