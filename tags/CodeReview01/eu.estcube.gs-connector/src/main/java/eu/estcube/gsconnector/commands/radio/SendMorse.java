package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRadioConstants;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SendMorse extends CommandStringBuilder {
    private StringBuilder messageString;

    public StringBuilder getMessageString() {
        return messageString;
    }

    public StringBuilder createMessageString(TelemetryCommand telemetryCommand) {
        messageString = new StringBuilder();
        messageString.append("+");
        messageString.append("b");
        messageString.append(" ");
        messageString.append(telemetryCommand.getParams().get("Morse"));
        messageString.append("\n");
        return messageString;
    }
}
