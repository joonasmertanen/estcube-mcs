package eu.estcube.webserver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import eu.estcube.domain.TelemetryObject;

/**
 * Bean for caching telemetryobjects from messages that are passed through it
 * and returning cached elements by request in the message body.
 * @author Kaupo Kuresson
 */
@Component
public class CacheMessage {
    /** HashMap containing cached TelemetryObjects as values and their names(String) as keys. */
    private static HashMap<String, ArrayList<TelemetryObject>> objectCache;
    private static final Logger LOG = LoggerFactory.getLogger(CacheMessage.class);
        
    /**
     * Class constructor
     */
    public CacheMessage() {
        objectCache = new HashMap<String, ArrayList<TelemetryObject>>();
    }
    
    /**
     * Takes a TelemetryObject from the body of a passing message and adds it to the cache.
     * @param ex Camel automatically binds the Exchange to this parameter.
     * @see <a href="http://camel.apache.org/maven/current/camel-core/apidocs/org/apache/camel/Exchange.html">Exchange</a>
     */
    public void addToCache(Exchange ex) {
        TelemetryObject telemetryObject = ex.getIn().getBody(TelemetryObject.class);
        String key = telemetryObject.getSource()+telemetryObject.getDevice()+telemetryObject.getName();
        if(!objectCache.containsKey(key)) {
        	ArrayList<TelemetryObject> newList = new ArrayList<TelemetryObject>();
        	newList.add(telemetryObject);
        	objectCache.put(key,newList);
        } else {
        	objectCache.get(key).add(telemetryObject);
        }        	
    }
    
    /**
     * Gets a specific element from the cache and returns it in the message body.
     * @param ex Camel automatically binds the Exchange to this parameter.
     * @param param Name of the TelemetryObject to get out of the cache.
     * @see <a href="http://camel.apache.org/maven/current/camel-core/apidocs/org/apache/camel/Exchange.html">Exchange</a>
     */
    public void getCachedElement(Exchange ex, HashMap<String,String> param) {
        String key = param.get("SOURCE")+param.get("DEVICE")+param.get("NAME");
        ex.getOut().setBody(objectCache.get(key));
    }
    
    /**
     * Returns the whole cache in the message body.
     * @param ex Camel automatically binds the Exchange to this parameter.
     * @see <a href="http://camel.apache.org/maven/current/camel-core/apidocs/org/apache/camel/Exchange.html">Exchange</a>
     */
    public void getCache(Exchange ex) {
        List<TelemetryObject> list = new ArrayList<TelemetryObject>();
        
        for(String key:objectCache.keySet()) {
        	for(TelemetryObject obj:objectCache.get(key))
        	list.add(obj);
        }                
        ex.getOut().setBody(list);
    }
}
