dojo.provide("webgui.display.StatusesGraphDisplayGS");
dojo.require("dijit.layout.StackContainer");
dojo.require("dojox.gfx");
dojo.require("dojox.gfx.Moveable");

dojo.declare("webgui.display.StatusesGraphDisplayGS", null, {

	constructor : function() {

		console.log("StatusesGraphDisplayGS activated!!!");

		var nodeWidth = 100;
		var nodeHeight = 100;
		var xPos = 200;
		var yPos = 50;
		var nameSize = 12;

		var statusesGraphContent = new dijit.layout.ContentPane({
			id : "statusesGraphTab",
			title : "Graph",
			preventCache : true
		});

		var tempCont;
		tempCont = dijit.byId("CenterContainer");

		tempCont.addChild(statusesGraphContent);
		tempCont.selectChild(statusesGraphContent);

		var surfaceVar = dojo.create("div", {
			id : "surfaceElement",
			region : "center"
		}, "statusesGraphTab");

		var surface = dojox.gfx.createSurface("surfaceElement", 800, 600);

		// RADIO
		var radioNode = surface.createRect({
			x : xPos,
			y : yPos,
			width : nodeWidth,
			height : nodeHeight
		}).setFill("green");

		var radioName = surface.createText({
			x : xPos,
			y : yPos + 2 * nameSize,
			text : "RADIO",
			align : "start"
		}).setFont({
			family : "Arial",
			size : nameSize + "pt",
			weight : "bold"
		}).setFill("black");

		var grpRADIO = surface.createGroup();
		grpRADIO.add(radioNode);
		grpRADIO.add(radioName);
		grpRADIO.connect("onclick", function(e) {
			var tc = dijit.byId("componentTabContainer");
			tc.selectChild(dijit.byId("radioTab"));
		});
		
		// ROTATOR
		var rotatorNode = surface.createRect({
			x : xPos + 2 * nodeWidth,
			y : yPos,
			width : nodeWidth,
			height : nodeHeight
		}).setFill("green");

		var rotatorName = surface.createText({
			x : xPos + 2 * nodeWidth,
			y : yPos + 2 * nameSize,
			text : "ROTATOR",
			align : "start"
		}).setFont({
			family : "Arial",
			size : nameSize + "pt",
			weight : "bold"
		}).setFill("black");

		var grpROTATOR = surface.createGroup();
		grpROTATOR.add(rotatorNode);
		grpROTATOR.add(rotatorName);
		grpROTATOR.connect("onclick", function(e) {
			var tc = dijit.byId("componentTabContainer");
			tc.selectChild(dijit.byId("rotatorTab"));
		});
		
		//----------------------------------------------------
		// CONNECTIONS
		//----------------------------------------------------
		var firstLevelHorizontal = yPos + nodeHeight / 2;
		var secondLevelHorizontal = yPos + 2.5 * nodeHeight;

		var firstLevelVertical = xPos + nodeWidth / 2;
		var secondLevelVertical = xPos + 2.5 * nodeWidth;
		var thirdLevelvertical = xPos + 4.5 * nodeWidth;

		// HORIZONTAL
		var camToCom = surface.createLine({
			x1 : xPos + nodeWidth,
			y1 : firstLevelHorizontal,
			x2 : xPos + 2 * nodeWidth,
			y2 : firstLevelHorizontal
		}).setStroke({
			width : 3,
			color : "black"
		});	
		
	}
});
