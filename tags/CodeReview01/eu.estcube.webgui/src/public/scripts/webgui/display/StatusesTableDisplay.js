dojo.provide("webgui.display.StatusesTableDisplay");
dojo.require("dojox.grid.DataGrid");
dojo.require("dojox.grid.EnhancedGrid");
dojo.require("dojo.store.Memory");
dojo.require("dojo.data.ObjectStore");
dojo.require("dijit.layout.ContentPane");
dojo.require("dijit.layout.StackContainer");
dojo.require("dijit.layout.TabContainer");
dojo.require("dojo.data.ItemFileWriteStore");

dojo.declare("webgui.display.StatusesTableDisplay", null, {

	constructor : function() {
		var someData = [];
		var store = new dojo.data.ItemFileWriteStore({
			data : {
				identifier : 'id',
				items : someData
			}
		});

		console.log("StatusesTableDisplay activated");

		var componentInfoContainer = new dijit.layout.TabContainer({
			id : "componentTabContainer",
			tabPosition : "bottom"
		});
		
		
		var radioContent = new dijit.layout.ContentPane({
			id : "radioTab",
			title : "RADIO",
			preventCache : true
		});
		
		var rotatorContent = new dijit.layout.ContentPane({
			id : "rotatorTab",
			title : "ROTATOR",
			preventCache : true
		});
		
		// var camContent = new dijit.layout.ContentPane({
			// id : "camTab",
			// title : "CAM",
			// preventCache : true
		// });
// 
		// var comContent = new dijit.layout.ContentPane({
			// id : "comTab",
			// title : "COM",
			// preventCache : true
		// });
// 
		// var adcsContent = new dijit.layout.ContentPane({
			// id : "adcsTab",
			// title : "ADCS",
			// preventCache : true
		// });
// 
		// var cdhsContent = new dijit.layout.ContentPane({
			// id : "cdhsTab",
			// title : "CDHS",
			// preventCache : true
		// });
// 
		// var epsContent = new dijit.layout.ContentPane({
			// id : "epsTab",
			// title : "EPS",
			// preventCache : true
		// });
// 
		// var beaconContent = new dijit.layout.ContentPane({
			// id : "beaconTab",
			// title : "BEACON",
			// preventCache : true
		// });
// 
		// var plContent = new dijit.layout.ContentPane({
			// id : "plTab",
			// title : "PL",
			// preventCache : true
		// });

		var tableHeaders = [{
			field : "source",
			name : "Source",
			width : 5
		}, {
			field : "device",
			name : "Device",
			width : 4
		}, {
			field : "name",
			name : "Name",
			width : 8
		}, {
			field : "param",
			name : "Param",
			width : 8
		}, {
			field : "value",
			name : "Value",
			width : 8
		}, {
			field : "time",
			name : "time",
			width : 8
		}];

		// create a new grid:
		var dataField = new dojox.grid.EnhancedGrid({
			id : "statusesTable",
			title : "Component Info",
			store : store,
			structure : tableHeaders
		});

		componentInfoContainer.addChild(dataField);

		var tempCont;
		tempCont = dijit.byId("PictureView");

		// componentInfoContainer.addChild(camContent);
		// componentInfoContainer.addChild(comContent);
		// componentInfoContainer.addChild(adcsContent);
		// componentInfoContainer.addChild(cdhsContent);
		// componentInfoContainer.addChild(epsContent);
		// componentInfoContainer.addChild(beaconContent);
		// componentInfoContainer.addChild(plContent);
		componentInfoContainer.addChild(radioContent);
		componentInfoContainer.addChild(rotatorContent);
		tempCont.addChild(componentInfoContainer);
		dataField.startup();

		var makeKey = function(obj, i) {
			return obj.source + obj.device + obj.name + obj.params[i].name;
		};

		dojo.subscribe("fromWS", function(message) {
			console.log("Tabledisplay got: ", message);

			var myObject = JSON.parse(message);
			if(myObject.name == "GET_POSITION"){
				dojo.publish("GetPositionAzimuth", [myObject]);
			}
			// make object out of json
			for( i = 0; i < myObject.params.length; i++) {
				var identity = makeKey(myObject, i);
				store.fetchItemByIdentity({
					'identity' : identity,
					onItem : function(item) {
						if(item == null) {
							store.newItem({
								id : makeKey(myObject, i),
								source : myObject.source,
								device : myObject.device,
								name : myObject.name,
								param : myObject.params[i].name,
								value : myObject.params[i].value,
                                time: myObject.params[i].time
							});
						} else {
							store.setValue(item, 'source', myObject.source);
							store.setValue(item, 'device', myObject.device);
							store.setValue(item, 'name', myObject.name);
							store.setValue(item, 'param', myObject.params[i].name);
							store.setValue(item, 'value', myObject.params[i].value);
							store.setValue(item, 'time', myObject.params[i].time);
						}
					}
				});
			}
		});
	}
});
