dojo.provide("webgui.display.PictureDisplay");

dojo.require("dijit.form.TextBox");
dojo.require("dijit.form.Button");
dojo.require("dijit.layout.ContentPane");
dojo.require("dijit.layout.StackContainer");
dojo.require("dojox.socket");
dojo.require("dojox.gfx");

dojo.declare("webgui.display.PictureDisplay", null, {
	
	constructor: function() {
		console.log("PictureDisplay!");
		
		var nodeWidth = 533;
		var nodeHeight = 533;
		var xPos = 0;
		var yPos = 0;
		
		var pictureTab = new dijit.layout.ContentPane({
        	id: "pictureTab",
        	title: "Picture",
        	preventCache: true
        		
       });
		
        var tempCont;
        tempCont = dijit.byId("PictureView");        
        tempCont.addChild(pictureTab);
        
		var surfaceVar = dojo.create("div", {
			id : "surfaceElement",
			region : "center"
		}, "pictureTab");
		
		
		var surface = dojox.gfx.createSurface("surfaceElement",nodeWidth,nodeHeight);
		var staticImage = surface.createImage({ 
		    x: xPos, y: yPos, width: nodeWidth, height: nodeHeight, src: "http://nssdc.gsfc.nasa.gov/image/planetary/earth/apollo17_earth.jpg"
		});
//		var camNode = surface.createRect({
//			x : xPos,
//			y : yPos,
//			width : nodeWidth,
//			height : nodeHeight
//		}).setFill("green");
		

		
	}

});