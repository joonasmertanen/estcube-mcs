package eu.estcube.weatherstation;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.core.ApplicableTo;
import org.hbird.exchange.interfaces.IEntityInstance;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractDataHandler {

    protected static final Logger LOG = LoggerFactory.getLogger(AbstractDataHandler.class);

    protected abstract String getDataSourceName();

    protected abstract Element getParentNodeForWeatherData(Document document);

    protected abstract Map<String, NamedConfig<? extends IEntityInstance>> getParameterMapping();

    protected abstract Date parseDate(Document documnet) throws ParseException;

    protected final IdBuilder idBuilder;

    protected final String serviceId;

    protected final String parameterNamespace;

    public AbstractDataHandler(IdBuilder idBuilder, String serviceId, String parameterNamespace) {
        this.idBuilder = idBuilder;
        this.serviceId = serviceId;
        this.parameterNamespace = parameterNamespace;
    }

    public List<IEntityInstance> handle(InputStream xml) throws IOException, JDOMException {
        SAXBuilder builder = new SAXBuilder();
        Document doc = builder.build(xml);
        return parseDocument(doc);
    }

    protected List<IEntityInstance> parseDocument(Document document) {
        String dataSource = getDataSourceName();
        LOG.debug("New data from {}", dataSource);
        List<IEntityInstance> weatherParams = new ArrayList<IEntityInstance>();
        Map<String, NamedConfig<? extends IEntityInstance>> parameterMapping = getParameterMapping();
        try {
            Element parentNode = getParentNode(document);
            Long timestamp = toUtcTimestamp(getDate(document));
            if (parentNode != null) {
                for (Element element : parentNode.getChildren()) {
                    String attribute = getAttribute(element);
                    String value = getValue(element);
                    NamedConfig<? extends IEntityInstance> pm = parameterMapping.get(attribute);
                    if (pm != null) {
                        ApplicableTo n = pm.createNewValue(value);
                        String applicableTo = idBuilder.buildID(parameterNamespace, getDataSourceName());
                        String finalId = idBuilder.buildID(applicableTo, n.getName());
                        n.setID(finalId);
                        n.setIssuedBy(serviceId);
                        n.setApplicableTo(applicableTo);
                        n.setTimestamp(timestamp);
                        weatherParams.add(n);
                    } else {
                        LOG.warn("Unknown attribute \"{}\"", attribute);
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            LOG.error("Failed to parse weather data from {}.", getDataSourceName(), e);
        }
        return weatherParams;
    }

    protected Element getParentNode(Document document) throws IllegalArgumentException {
        try {
            return getParentNodeForWeatherData(document);
        } catch (Exception e) {
            throw new IllegalArgumentException("Unable to parse given XML from " + getDataSourceName(), e);
        }

    }

    protected Date getDate(Document document) {
        Date date;
        try {
            date = parseDate(document);
        } catch (Exception e) {
            LOG.error("Failed to parse date from {}; returning current date", getDataSourceName(), e);
            date = new Date();
        }
        return date;
    }

    protected long toUtcTimestamp(Date date) {
        // TODO - 09.11.2012; kimmell - implement this!
        return date.getTime();
    }

    protected String getAttribute(Element element) {
        return element.getName();
    }

    protected String getValue(Element element) {
        return element.getText();
    }
}
