define([
    "dojo/store/Memory",
    "dojo/store/Observable",
    "config/config",
    "./DataHandler",
    "./StoreMonitor",
    "dojo/_base/array",
    ],

    function (Memory, Observable, Config, DataHandler, StoreMonitor, Arrays) {



        var channel = Config.WEBSOCKET_ARCHIVE;
        var storeId = "ID";
        var store = new Observable(new Memory({ idProperty: storeId }));
        new StoreMonitor({ store: store, storeName: "ArchiveStore" });


        var handler = new DataHandler({ channels: [channel], callback: function (message, channel) {

            if (!(message.valueOf().message == "KeepAlive")) {
            	if (store.get(0).requestId == message.headers.requestId){
	                var array =  message.valueOf().frame;
	                if(array.length == 0){
	                	dijit.byId("Enhanced-grid-query").messagesNode.textContent="No data to display";
	                }
	                Arrays.some(array, function (entry) {
	                	
	                   setTimeout(function() {
	                	   if(entry.requestId != store.get(0).requestId ){
		                		return false;
		                	}else{
		                		entry.class = "row";
		                		store.put(entry)
		                	}
	                    }, 0);
	                });
            	}

            }
        }
        });

        return store;
    }

    );