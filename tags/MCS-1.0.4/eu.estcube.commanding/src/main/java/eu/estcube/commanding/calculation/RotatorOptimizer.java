/** 
 *
 */
package eu.estcube.commanding.calculation;

import java.util.List;

import org.hbird.business.groundstation.configuration.RotatorDriverConfiguration;
import org.hbird.exchange.navigation.PointingData;
import org.springframework.beans.factory.annotation.Autowired;

import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.api.OptimizeInterface;
import eu.estcube.commanding.api.PassAnalyzeInterface;
import eu.estcube.commanding.api.RotatorTypeInterface;
import eu.estcube.commanding.api.RotatorTypeSelectorInterface;

/**
 * Class for optimizing satellite tracking data for given {@link Rotator}.
 */
public class RotatorOptimizer {

	/** Minimum number of {@link PointingData} elements needed for optimization. */
	public static final int INPUT_MIN_LENGTH = 7;

	@Autowired
	private PassAnalyzeInterface passAnalyzer;

	@Autowired
	private RotatorTypeSelectorInterface rotatorTypeSelector;

	/**
	 * Do the optimization.
	 * 
	 * @param input {@link List} satellite trajectory (elevation,azimuth pairs).
	 * @param config {@link RotatorDriverConfiguration} current rotator
	 *            configuration.
	 * @return
	 */

	public List<PointingData> doOptimize(final List<PointingData> input,
			final RotatorDriverConfiguration config) {

		if (input == null) {
			throw new IllegalArgumentException("Pointing data input is null");
		}

		if (input.size() < INPUT_MIN_LENGTH) {
			throw new IllegalArgumentException("Optimizer needs at least "
					+ INPUT_MIN_LENGTH + " for the calculations; input.size="
					+ input.size());
		}

		if (config == null) {
			throw new IllegalArgumentException(
					"RotatorDriverConfiguration is null");
		}

		final OverPassType type = passAnalyzer.calculateType(input);
		final RotatorTypeInterface typeInterface = rotatorTypeSelector
				.selectFor(config);
		final OptimizeInterface optimizer = typeInterface.getOptimizer(type);
		final List<PointingData> result = optimizer.optimize(input);
		return result;
	}
}
