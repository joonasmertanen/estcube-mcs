package eu.estcube.sdc.processor;

import org.apache.camel.Handler;
import org.hbird.business.api.IDataAccess;
import org.hbird.business.api.exceptions.ArchiveException;
import org.hbird.exchange.navigation.Satellite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Component to keep base connection alive by doing a query in Camel route.
 */
@Component
public class BaseConnectionKeeper {

    @Autowired
    private static final Logger LOG = LoggerFactory.getLogger(BaseConnectionKeeper.class);

    @Autowired
    private IDataAccess dao;

    /**
     * Queries the database to keep the connection alive. 2 attempts are made,
     * so if first one times out, the second one shouldn't
     */
    @Handler
    public void keepAlive() {
        // Try twice; if connection was timed out, it would try again and
        // succeed, because connection will be restored with the first attempt.
        for (int i = 0; i < 2; i++) {
            try {
                dao.getAll(Satellite.class);
                break;
            } catch (ArchiveException e) {
                // Log warning on first and error on second attempt
                if (i == 0) {
                    LOG.warn("Keep alive check failed", e);
                } else {
                    LOG.error("Keep alive check failed twice", e);
                }
            }
        }
    }
}
