/** 
 *
 */
package eu.estcube.sdc.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.hbird.exchange.constants.StandardArguments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.common.Headers;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;
import eu.estcube.domain.transport.tnc.TncFrame;
import eu.estcube.sdc.domain.Ax25UiFrameInput;

/**
 *
 */
@Component
public class Ax25UiFrameInputProcessor implements Processor {

    @Autowired
    private InputToAx25UiFrameConverter converter;
    
    /** @{inheritDoc}. */
    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        out.copyFrom(in);
        Ax25UiFrameInput input = in.getBody(Ax25UiFrameInput.class);

        Ax25UIFrame frame = converter.convert(input);
        out.setBody(frame);
        
        int port = input.getTncTargetPort();
        Validate.isTrue(port >= TncFrame.TARGET_MIN_VALUE && port <= TncFrame.TARGET_MAX_VALUE, "Valid range for tncTarget port is [%s ... %s]", TncFrame.TARGET_MIN_VALUE, TncFrame.TARGET_MAX_VALUE);
        out.setHeader(Headers.TNC_PORT, port);
        
        String prefix = input.getPrefix();
        if (StringUtils.isNotBlank(prefix)) {
            out.setHeader(Headers.TNC_PREFIX, prefix);
        }
        if (input.getSatelliteId() != null) {
            out.setHeader(StandardArguments.SATELLITE_ID, input.getSatelliteId());
        } else {
            // FIXME remove this part once ICP sends satelliteId with the command xml
            out.setHeader(StandardArguments.SATELLITE_ID, "/ESTCUBE/Satellites/ESTCube-1");
        }
    }
}
