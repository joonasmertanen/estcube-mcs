package eu.estcube.archiver.raw;

import java.sql.SQLException;

public interface FrameArchiver {

	public void store(FrameDataProvider frameDataProvider)
			throws ArchivingException, SQLException;

}
