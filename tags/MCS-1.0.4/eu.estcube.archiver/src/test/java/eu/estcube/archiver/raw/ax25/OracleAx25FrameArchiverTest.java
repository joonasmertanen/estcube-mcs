package eu.estcube.archiver.raw.ax25;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.camel.Message;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import eu.estcube.archiver.notifier.MailNotifier;
import eu.estcube.archiver.raw.ArchivingException;
import eu.estcube.archiver.raw.FrameDataProvider;
import eu.estcube.archiver.raw.sql.MessageToSqlFrameDataProvider;
import eu.estcube.domain.transport.Direction;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;

public class OracleAx25FrameArchiverTest {
	private static final long TIMESTAMP = System.currentTimeMillis();
	private static final String SAT = "SAT1";

	private static final byte[] DESTADDR = new byte[] { 0, 1 };
	private static final byte[] SRCADDR = new byte[] { 2, 3 };

	private static final byte CTRL = 4;
	private static final byte PID = 5;

	private static final byte[] INFO = new byte[] { 6, 7, 8, 9 };
	private static final byte[] FCS = new byte[] { 10, 11 };

	private DataSource dataSource;
	private OracleAx25FrameArchiver archiver;
	private FrameDataProvider provider;
	private MailNotifier notifier;

	private Message message;
	private Ax25UIFrame frame;
	private Map<String, Object> headers;

	@Before
	public void setUp() throws Exception {
		message = Mockito.mock(Message.class);
		dataSource = Mockito.mock(DataSource.class);
		notifier = Mockito.mock(MailNotifier.class);

		provider = new Ax25ToSqlProvider(Direction.DOWN, message);
		archiver = new OracleAx25FrameArchiver(dataSource, notifier);

		frame = new Ax25UIFrame();
		frame.setDestAddr(DESTADDR);
		frame.setSrcAddr(SRCADDR);
		frame.setCtrl(CTRL);
		frame.setPid(PID);
		frame.setInfo(INFO);
		frame.setFcs(FCS);

		headers = new HashMap<String, Object>();

		headers.put(MessageToSqlFrameDataProvider.HDR_RECEPTION_TIME, TIMESTAMP);
		headers.put(MessageToSqlFrameDataProvider.HDR_SATELLITE, SAT);

		Mockito.when(message.getBody(Ax25UIFrame.class)).thenReturn(frame);
		Mockito.when(message.getHeaders()).thenReturn(headers);
	}

	@Ignore
	public void testDataTypes() throws ArchivingException, SQLException,
			ClassNotFoundException {
		Class.forName("oracle.jdbc.OracleDriver");
		Connection c = DriverManager.getConnection(
				"jdbc:oracle:thin:@localhost:1521:devel", "mcs_user",
				"m1skiMCS");
		Mockito.when(dataSource.getConnection()).thenReturn(c);

		archiver.store(provider);
		c.close();

	}

	@Test
	public void testGetAutoIncrementFunction() {
		assertEquals("MCS.AX25_FRAMES_SEQ.NEXTVAL",
				archiver.getAutoIncrementFunction());
	}

	@Test
	public void testGetDetailsAutoIncrementFunction() {
		assertEquals("MCS.AX25_FRAME_DETAILS_SEQ.NEXTVAL",
				archiver.getDetailsAutoIncrementFunction());
	}

	@Test
	public void testGetColumns() {
		String[] c = archiver.getColumns();
		assertEquals(9, c.length);
		for (int i = 0; i < c.length; i++) {
			assertNotNull(c[i]);
		}
	}

	@Test
	public void testGetTable() {
		assertEquals("MCS.AX25_FRAMES", archiver.getTable());
	}

	@Test
	public void testGetDetailsTable() {
		assertEquals("MCS.AX25_FRAME_DETAILS", archiver.getDetailsTable());
	}

	@Test
	public void testGetTimestampFunction() {
		assertEquals("SYSDATE", archiver.getTimestampFunction());
	}

	@Test
	public void testOracleAx25FrameArchiver() {
		assertSame(dataSource, archiver.getDataSource());
	}

}
