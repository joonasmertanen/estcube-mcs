package eu.estcube.codec.gcp.struct.parameters;

import java.nio.ByteBuffer;

import org.apache.commons.lang3.ArrayUtils;
import org.w3c.dom.Element;

public class GcpParameterFloat extends GcpParameterNumberic {

    public final static long BYTES = 4;

    public GcpParameterFloat(String name, String description, String unit, Boolean isLittleEndian) {
        super(name, description, unit, isLittleEndian);
    }

    public GcpParameterFloat(Element e) {
        super(e);
    }

    @Override
    public int getLength() {
        return 32;
    }

    @Override
    public Float toValue(String input) {
        if (isHexString(input)) {
            return (Float) toValueFromHex(input);
        }
        return Float.parseFloat(input);
    }

    @Override
    public byte[] toBytes(Object value) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putFloat(toValue(String.valueOf(value)));
        byte[] bytes = { buffer.get(0), buffer.get(1), buffer.get(2), buffer.get(3) };
        
        if (getIsLittleEndian()) {
            ArrayUtils.reverse(bytes);
        }
        
        return bytes;
    }

    @Override
    public Float toValue(byte[] bytes) {

        if (bytes.length != BYTES) {
            throw new RuntimeException("byte[] length of the argument must be " + BYTES + "! Was " + bytes.length);
        }

        if (getIsLittleEndian()) {
            ArrayUtils.reverse(bytes);
        }
        
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.position(8 - bytes.length);
        buffer.put(bytes);
        
        return buffer.getFloat(8 - bytes.length);
    }

    @Override
    public Class<?> getValueClass() {
        return Float.class;
    }

}
