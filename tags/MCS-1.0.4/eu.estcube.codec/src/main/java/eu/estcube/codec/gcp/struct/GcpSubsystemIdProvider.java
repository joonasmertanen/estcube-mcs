package eu.estcube.codec.gcp.struct;

import java.util.HashMap;
import java.util.Map;

import eu.estcube.codec.gcp.exceptions.SubsystemNotFoundException;

public class GcpSubsystemIdProvider {

    public static Integer getId(String name) throws SubsystemNotFoundException {
        Map<String, Integer> strToInt = new HashMap<String, Integer>();
        strToInt.put("EPS", 0);
        strToInt.put("COM", 1);
        strToInt.put("CDHS", 2);
        strToInt.put("ADCS", 3);
        strToInt.put("PL", 4);
        strToInt.put("CAM", 5);
        strToInt.put("GS", 6);
        strToInt.put("PC", 7);
        strToInt.put("PC2", 8);

        if (!strToInt.containsKey(name)) {
            throw new SubsystemNotFoundException(name);
        }
        return strToInt.get(name);
    }

    public static String getName(Integer id) throws SubsystemNotFoundException {
        Map<Integer, String> intToStr = new HashMap<Integer, String>();
        intToStr.put(0, "EPS");
        intToStr.put(1, "COM");
        intToStr.put(2, "CDHS");
        intToStr.put(3, "ADCS");
        intToStr.put(4, "PL");
        intToStr.put(5, "CAM");
        intToStr.put(6, "GS");
        intToStr.put(7, "PC");
        intToStr.put(8, "PC2");
        if (!intToStr.containsKey(id)) {
            throw new SubsystemNotFoundException(String.valueOf(id));
        }
        return intToStr.get(id);
    }
}
