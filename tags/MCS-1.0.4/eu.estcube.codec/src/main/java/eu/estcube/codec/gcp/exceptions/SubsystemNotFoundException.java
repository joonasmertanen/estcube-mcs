package eu.estcube.codec.gcp.exceptions;

public class SubsystemNotFoundException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = 929914050153561254L;

    public SubsystemNotFoundException(String subsystem) {
        super("Subsystem '" + subsystem + "' was not found!!! Contact someone in charge!!");
    }
}
