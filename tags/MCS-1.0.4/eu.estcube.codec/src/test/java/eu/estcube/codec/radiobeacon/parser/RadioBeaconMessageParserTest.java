package eu.estcube.codec.radiobeacon.parser;

import static org.junit.Assert.assertEquals;

import org.hbird.exchange.core.EntityInstance;
import org.junit.Before;
import org.junit.Test;

public class RadioBeaconMessageParserTest {

    protected RadioBeaconMessageParser parser;

    private final String cw1 = "5TASAZZZ";
    private final String cw2 = "HTUD";
    private final String cw3 = "";

    private final String hex1A = "2A";
    private final String hex1B = "QW";
    private final String hex2 = "2A4B";
    private final String hex3 = "";

    private final String unixTimestamp1 = "1352902792";

    @Before
    public void setUp() throws Exception {
        parser = new RadioBeaconMessageParser() {
            @Override
            public String parse(String hex) {
                return null;
            }

            @Override
            public EntityInstance parseToIssued(String message) {
                return null;
            }

        };

    }

    @Test
    public void parseHexTest() throws Exception {
        assertEquals(parser.parseHex(hex1A), "42");
        assertEquals(parser.parseHex(hex1B), "0");
        assertEquals(parser.parseHex(hex2), "10827");
        assertEquals(parser.parseHex(hex3), "0");

    }

    @Test
    public void cwToHexTest() throws Exception {
        assertEquals(parser.cwToHex(cw1), "50A3A888");
        assertEquals(parser.cwToHex(cw2), "402D");
        assertEquals(parser.cwToHex(cw3), "");
    }

    @Test
    public void UnixTimestampToDateTest() throws Exception {
        System.out.printf("FIXME - %s.UnixTimestampToDateTest() - this test is working only in EET/EEST time zone!",
                getClass().getSimpleName());
        // HINT: org.hbird.exchange.util.Dates
        assertEquals(parser.unixTimestampToDate(unixTimestamp1), "14.11.2012 16:19:52");
    }
}
