package eu.estcube.camel.serial;

import java.util.ArrayList;
import java.util.List;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.transport.serial.SerialAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class SerialHandler extends IoHandlerAdapter {

    public static final Logger LOG = LoggerFactory.getLogger(SerialHandler.class);

    private final List<SerialConsumer> consumers = new ArrayList<SerialConsumer>();

    public void addConsumer(SerialConsumer consumer) {
        if (!consumers.contains(consumer)) {
            consumers.add(consumer);
        }
    }

    /** @{inheritDoc . */
    @Override
    public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
        long sid = session.getId();
        String port = getSerialPortName(session);
        for (SerialConsumer consumer : consumers) {
            consumer.handleCommunicationException(sid, port, cause);
        }
    }

    /** @{inheritDoc . */
    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
        long sid = session.getId();
        String port = getSerialPortName(session);
        LOG.trace("Received message #{} - {} from serial connection; sid: {}; port: {}", new Object[] {
                session.getReadMessages(), message != null ? message.getClass().getName() : "null", sid, port });
        for (SerialConsumer consumer : consumers) {
            try {
                consumer.handle(sid, port, message);
            } catch (Exception e) {
                consumer.handleProcessingException(sid, port, message, e);
            }
        }
    }

    /** @{inheritDoc . */
    @Override
    public void messageSent(IoSession session, Object message) throws Exception {
        long sentMessages = session.getWrittenMessages();
        LOG.trace("Sent message #{} to serial connection; sid {}; port: {}",
                new Object[] { sentMessages, session.getId(), getSerialPortName(session) });
    }

    /** @{inheritDoc . */
    @Override
    public void sessionClosed(IoSession session) throws Exception {
        LOG.info("Serial session closed; sid: {}; port: {} ", session.getId(), getSerialPortName(session));
    }

    /** @{inheritDoc . */
    @Override
    public void sessionCreated(IoSession session) throws Exception {
        // serial address is not yet attached to session at this point
        LOG.info("Serial Session created; sid: {}; port: {}", session.getId(), getSerialPortName(session));
    }

    /** @{inheritDoc . */
    @Override
    public void sessionOpened(IoSession session) throws Exception {
        // serial address is not yet attached to session at this point
        LOG.info("Serial Session opened; sid: {}; port: {}", session.getId(), getSerialPortName(session));
    }

    String getSerialPortName(IoSession session) {
        SerialAddress address = (SerialAddress) session.getAttribute(SerialEndpoint.ATTRIBUTE_SERIAL_ADDRESS);
        return address == null ? "N/A" : address.getName();
    }
}
