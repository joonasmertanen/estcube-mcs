package eu.estcube.camel.serial;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.spi.ExceptionHandler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SerialConsumerTest {

    private static final long SID = 123413L;

    private static final String PORT = "/dev/ttyS13";

    private static final byte[] BYTES = new byte[] { 0x0D, 0x0E, 0x0A, 0x0D, 0x0C, 0x00, 0x0D, 0x0E };

    @Mock
    private Endpoint endpoint;

    @Mock
    private Processor processor;

    @Mock
    private Exchange exchange;

    @Mock
    private Message in;

    @Mock
    private ExceptionHandler exceptionHandler;

    private Exception exception;

    private InOrder inOrder;

    private SerialConsumer serialConsumer;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        serialConsumer = new SerialConsumer(endpoint, processor);
        serialConsumer.setExceptionHandler(exceptionHandler);
        exception = new Exception("BAD NEWS");
        inOrder = inOrder(endpoint, processor, exchange, in, exceptionHandler);
        when(endpoint.createExchange()).thenReturn(exchange);
        when(exchange.getIn()).thenReturn(in);
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialConsumer#SerialConsumer(org.apache.camel.Endpoint, org.apache.camel.Processor)}
     * .
     */
    @Test
    public void testSerialConsumer() {
        assertNotNull(serialConsumer);
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialConsumer#handle(long, java.lang.String, byte[])}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testHandle() throws Exception {
        serialConsumer.handle(SID, PORT, BYTES);
        inOrder.verify(endpoint, times(1)).createExchange();
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(in, times(1)).setHeader(SerialConsumer.SESSION_ID, SID);
        inOrder.verify(in, times(1)).setHeader(SerialConsumer.SERIAL_PORT, PORT);
        inOrder.verify(in, times(1)).setHeader(SerialConsumer.MESSAGE_CLASS, BYTES.getClass());
        inOrder.verify(in, times(1)).setBody(BYTES);
        inOrder.verify(processor, times(1)).process(exchange);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialConsumer#handleProcessingException(long, java.lang.String, byte[], java.lang.Exception)}
     * .
     */
    @Test
    public void testHandleProcessingException() {
        serialConsumer.handleProcessingException(SID, PORT, BYTES, exception);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(in, times(1)).setHeader(SerialConsumer.SESSION_ID, SID);
        inOrder.verify(in, times(1)).setHeader(SerialConsumer.SERIAL_PORT, PORT);
        inOrder.verify(in, times(1)).setHeader(SerialConsumer.MESSAGE_CLASS, BYTES.getClass());
        inOrder.verify(exceptionHandler, times(1)).handleException(anyString(), eq(exchange), eq(exception));
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialConsumer#handleCommunicationException(long, java.lang.String, java.lang.Throwable)}
     * .
     */
    @Test
    public void testHandleCommunicationException() {
        serialConsumer.handleCommunicationException(SID, PORT, exception);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(in, times(1)).setHeader(SerialConsumer.SESSION_ID, SID);
        inOrder.verify(in, times(1)).setHeader(SerialConsumer.SERIAL_PORT, PORT);
        inOrder.verify(exceptionHandler, times(1)).handleException(anyString(), eq(exchange), eq(exception));
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialConsumer#sendErrorMessage(long, java.lang.String, java.lang.Throwable, byte[])}
     * .
     */
    @Test
    public void testSendErrorMessage() {
        serialConsumer.sendErrorMessage(SID, PORT, exception, BYTES);
        inOrder.verify(exchange, times(1)).getIn();
        inOrder.verify(in, times(1)).setHeader(SerialConsumer.SESSION_ID, SID);
        inOrder.verify(in, times(1)).setHeader(SerialConsumer.SERIAL_PORT, PORT);
        inOrder.verify(in, times(1)).setHeader(SerialConsumer.MESSAGE_CLASS, BYTES.getClass());
        inOrder.verify(exceptionHandler, times(1)).handleException(anyString(), eq(exchange), eq(exception));
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialConsumer#setHeaders(org.apache.camel.Message, long, java.lang.String, byte[])}
     * .
     */
    @Test
    public void testSetHeaders() {
        serialConsumer.setHeaders(in, SID, PORT, BYTES);
        inOrder.verify(in, times(1)).setHeader(SerialConsumer.SESSION_ID, SID);
        inOrder.verify(in, times(1)).setHeader(SerialConsumer.SERIAL_PORT, PORT);
        inOrder.verify(in, times(1)).setHeader(eq(Exchange.CREATED_TIMESTAMP), anyLong());
        inOrder.verify(in, times(1)).setHeader(SerialConsumer.MESSAGE_CLASS, BYTES.getClass());
        inOrder.verifyNoMoreInteractions();
    }
}
