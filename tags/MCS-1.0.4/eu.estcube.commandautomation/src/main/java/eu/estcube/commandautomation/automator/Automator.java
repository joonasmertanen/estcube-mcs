package eu.estcube.commandautomation.automator;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.commandautomation.ContactHandlerNotFoundException;
import eu.estcube.commandautomation.db.Contact;
import eu.estcube.commandautomation.db.DbGateway;

@Component
public class Automator {

    private static final Logger LOG = LoggerFactory.getLogger(Automator.class);

    @Autowired
    private DbGateway db;

    @Autowired
    private CommandHandlerFactory commandHandlerFactory;

    private HashMap<String, ContactHandler> contactHandlers = new HashMap<String, ContactHandler>();

    public void prepare(String trackingId) {

        LOG.info("Prepare event received for tracking with id " + trackingId);

        if (!doesHandlerExist(trackingId)) {
            LOG.warn("Contact handler for tracking with id " + trackingId
                    + " already exists and is being handled. Skipping this prepare event!");
            return;
        }

        Contact contact = db.getContact(trackingId);
        contact.setCommands(db.getCommands(trackingId));

        ContactHandler handler = new ContactHandler(contact, commandHandlerFactory);

        handler.prepare();
        setHandler(trackingId, handler);
    }

    public void start(String trackingId) throws ContactHandlerNotFoundException {
        LOG.info("Start event received for tracking with id " + trackingId);

        ContactHandler handler = getHandler(trackingId);
        if (handler == null) {
            throw new ContactHandlerNotFoundException(trackingId);
        }
        handler.start();
    }

    public void end(String trackingId) throws ContactHandlerNotFoundException {
        LOG.info("End event received for tracking with id " + trackingId);

        ContactHandler handler = getHandler(trackingId);
        if (handler == null) {
            throw new ContactHandlerNotFoundException(trackingId);
        }
        handler.end();
        contactHandlers.remove(trackingId);
    }

    private ContactHandler getHandler(String trackingId) {
        return contactHandlers.get(trackingId);
    }

    private void setHandler(String trackingId, ContactHandler handler) {
        contactHandlers.put(trackingId, handler);
    }

    private boolean doesHandlerExist(String trackingId) {
        return contactHandlers.containsKey(trackingId);
    }

}
