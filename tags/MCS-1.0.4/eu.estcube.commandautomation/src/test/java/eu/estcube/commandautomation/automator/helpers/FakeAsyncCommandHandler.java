package eu.estcube.commandautomation.automator.helpers;

import java.util.Timer;
import java.util.TimerTask;

import eu.estcube.commandautomation.automator.CommandHandler;
import eu.estcube.commandautomation.db.Command;

public class FakeAsyncCommandHandler extends CommandHandler {

    public int prepareWaitTime = 10;
    public int startWaitTime = 10;
    public int commandFinishedWaitTime = 10;
    public int endWaitTime = 10;
    public int killWaitTime = 10;

    public int prepareCalled = 0;
    public int prepareFinishedCalled = 0;
    public int startCalled = 0;
    public int startFinishedCalled = 0;
    public int endCalled = 0;
    public int endFinishedCalled = 0;
    public int killCalled = 0;
    public int killFinishedCalled = 0;
    public String commandResult = "";
    Timer timer;

    public FakeAsyncCommandHandler(Command command) {
        super(command);
    }

    public void resetTimer() {
        if (timer != null) {
            timer.cancel();
        }
        timer = new Timer();
    }

    public void prepare() {
        prepareCalled++;
        resetTimer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                prepareFinishedCalled++;
                synchronized (this) {
                    notifyAll();
                }
            }
        }, prepareWaitTime);
    }

    public void start() {
        startCalled++;
        resetTimer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                startFinished();
            }
        }, startWaitTime);

    }

    private void startFinished() {
        startFinishedCalled++;
        resetTimer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                onEnded("FINISHED");
            }
        }, commandFinishedWaitTime);

        synchronized (this) {
            notifyAll();
        }
    }

    public void onEnded(String result) {
        commandResult = result;
        super.onEnded(result);

        synchronized (this) {
            notifyAll();
        }
    }

    public void end() {
        endCalled++;
        resetTimer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                endFinished();
            }
        }, endWaitTime);
    }

    private void endFinished() {
        endFinishedCalled++;
        onEnded("ENDED");

        synchronized (this) {
            notifyAll();
        }
    }

    public void kill() {
        killCalled++;
        resetTimer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                killFinished();
            }
        }, killWaitTime);
    }

    public void killFinished() {
        killFinishedCalled++;
        onEnded("KILLED");

        synchronized (this) {
            notifyAll();
        }
    }

}