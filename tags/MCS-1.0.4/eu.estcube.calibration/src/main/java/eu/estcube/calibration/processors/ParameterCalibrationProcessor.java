/** 
 *
 */
package eu.estcube.calibration.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.hbird.exchange.core.CalibratedParameter;
import org.hbird.exchange.core.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.estcube.calibration.Calibrator;
import eu.estcube.calibration.data.CalibrationExpression;

/**
 * Calibrates <code>Parameter</code>s in Camel route
 */
@Component
public class ParameterCalibrationProcessor implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(ParameterCalibrationProcessor.class);

    /** @{inheritDoc . */
    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();

        Parameter input = in.getBody(Parameter.class);
        CalibratedParameter output = new CalibratedParameter(input);
        String id = output.getID().replace("//", "/");
        for (CalibrationExpression c : Calibrator.getCalibrationExpressions()) {
            if (c.getID().equals(id)) {
                try {
                    c.calibrate(output);
                } catch (Exception e) {
                    LOG.error("Failed to calibrate Parameter: " + c.getID() + " (current value: " + output.asDouble()
                            + "; expression: " + c.getExpression() + ")", e);
                }
                break;
            }
        }
        out.setBody(output);

    }

}
