/** 
 *
 */
package eu.estcube.calibration;

import java.io.File;
import java.util.List;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.hbird.business.core.AddHeaders;
import org.hbird.exchange.configurator.StandardEndpoints;
import org.hbird.exchange.core.BusinessCard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

import eu.estcube.calibration.data.CalibrationExpression;
import eu.estcube.calibration.data.LimitsData;
import eu.estcube.calibration.processors.ParameterCalibrationProcessor;
import eu.estcube.calibration.processors.ParameterLimitCheckingProcessor;

/**
 * Calibrates <code>Parameters</code>, using a XML data file specified in VM
 * arguments ("calibration.values")
 * 
 * @author Gregor Eesmaa
 * 
 */
public class Calibrator extends RouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(Calibrator.class);

    private static final String FILTER_UNCALIBRATED_PARAMETERS = "${in.header.class} == 'Parameter'";

    @Autowired
    private CalibratorConfig config;

    @Autowired
    private ParameterCalibrationProcessor calibratorProcessor;

    @Autowired
    private ParameterLimitCheckingProcessor limitCheckerProcessor;

    @Autowired
    private AddHeaders addHeaders;

    private static List<CalibrationExpression> calibrationExpressions;

    /** @{inheritDoc . */
    @Override
    public void configure() throws Exception {
        // @formatter:off
        from(StandardEndpoints.MONITORING)
            .filter(simple(FILTER_UNCALIBRATED_PARAMETERS))
            .process(calibratorProcessor)
            .process(limitCheckerProcessor)
            .process(addHeaders)
            .to(StandardEndpoints.MONITORING);
        
        BusinessCard card = new BusinessCard(config.getServiceId(), config.getServiceName());
        card.setPeriod(config.getHeartBeatInterval());
        card.setDescription(String.format("Calibrator; version: %s", config.getServiceVersion()));
        from("timer://heartbeat?fixedRate=true&period=" + config.getHeartBeatInterval())
            .bean(card, "touch")
            .process(addHeaders)
            .to(StandardEndpoints.MONITORING);
        // @formatter:on

    }

    /**
     * 
     * @return the calibrationExpressions
     */
    public static List<CalibrationExpression> getCalibrationExpressions() {
        return calibrationExpressions;
    }

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        LOG.info("Starting Calibrator transmitter");
        // new Main().run(args);
        // TODO - 14.08.2013; kimmell - switch back when upgrading to Camel 2.12

        if (!System.getProperties().containsKey("calibration.values")) {
            LOG.error("Calibration file location is not set with \"calibration.values\" in VM arguments");
            return;
        }
        File calibrationSettings = new File(System.getProperty("calibration.values"));
        XStream xstream = new XStream(new StaxDriver());
        xstream.alias("parameters", List.class);
        xstream.alias("parameter", CalibrationExpression.class);
        xstream.alias("limits", LimitsData.class);
        calibrationExpressions = (List<CalibrationExpression>) xstream.fromXML(calibrationSettings);

        try {
            AbstractApplicationContext context = new ClassPathXmlApplicationContext("META-INF/spring/camel-context.xml");
            Calibrator calibrator = context.getAutowireCapableBeanFactory().createBean(Calibrator.class);

            Main m = new Main();
            m.setApplicationContext(context);
            m.addRouteBuilder(calibrator);
            m.run(args);
        } catch (Exception e) {
            LOG.error("Failed to start " + Calibrator.class.getName(), e);
        }
    }
}
