package eu.estcube.camel.serial;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.transport.serial.SerialAddress;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SerialHandlerTest {

    private static final long SID = 9998712387L;
    private static final String PORT = "/dev/ttyS991";
    private static final byte[] BYTES = new byte[] { 0x0D, 0x0E, 0x0A, 0x0D, 0x0B, 0x00, 0x01 };

    @Mock
    private SerialConsumer consumer1;

    @Mock
    private SerialConsumer consumer2;

    @Mock
    private IoSession session;

    @Mock
    private IoBuffer buffer;

    @Mock
    private SerialAddress address;

    private InOrder inOrder;

    private Exception exception;

    private SerialHandler handler;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        handler = new SerialHandler();
        exception = new Exception("VERY BAD NEWS");
        inOrder = inOrder(consumer1, consumer2, session, buffer, address);
        when(buffer.array()).thenReturn(BYTES);
        when(session.getId()).thenReturn(SID);
        when(session.getAttribute(SerialEndpoint.ATTRIBUTE_SERIAL_ADDRESS)).thenReturn(address);
        when(address.getName()).thenReturn(PORT);
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialHandler#addConsumer(eu.estcube.camel.serial.SerialConsumer)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testAddConsumer() throws Exception {
        handler.addConsumer(consumer1);
        handler.addConsumer(consumer2);
        handler.messageReceived(session, BYTES);
        inOrder.verify(session, times(1)).getId();
        inOrder.verify(session, times(1)).getAttribute(SerialEndpoint.ATTRIBUTE_SERIAL_ADDRESS);
        inOrder.verify(address, times(1)).getName();
        inOrder.verify(consumer1, times(1)).handle(SID, PORT, BYTES);
        inOrder.verify(consumer2, times(1)).handle(SID, PORT, BYTES);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialHandler#addConsumer(eu.estcube.camel.serial.SerialConsumer)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testAddConsumerMultipleTimes() throws Exception {
        handler.addConsumer(consumer1);
        handler.addConsumer(consumer2);
        handler.addConsumer(consumer1);
        handler.addConsumer(consumer2);
        handler.messageReceived(session, BYTES);
        inOrder.verify(session, times(1)).getId();
        inOrder.verify(session, times(1)).getAttribute(SerialEndpoint.ATTRIBUTE_SERIAL_ADDRESS);
        inOrder.verify(address, times(1)).getName();
        inOrder.verify(consumer1, times(1)).handle(SID, PORT, BYTES);
        inOrder.verify(consumer2, times(1)).handle(SID, PORT, BYTES);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialHandler#exceptionCaught(org.apache.mina.core.session.IoSession, java.lang.Throwable)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testExceptionCaught() throws Exception {
        handler.addConsumer(consumer1);
        handler.addConsumer(consumer2);
        handler.exceptionCaught(session, exception);
        inOrder.verify(session, times(1)).getId();
        inOrder.verify(session, times(1)).getAttribute(SerialEndpoint.ATTRIBUTE_SERIAL_ADDRESS);
        inOrder.verify(address, times(1)).getName();
        inOrder.verify(consumer1, times(1)).handleCommunicationException(SID, PORT, exception);
        inOrder.verify(consumer2, times(1)).handleCommunicationException(SID, PORT, exception);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialHandler#messageReceived(org.apache.mina.core.session.IoSession, java.lang.Object)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testMessageReceived() throws Exception {
        handler.addConsumer(consumer1);
        handler.addConsumer(consumer2);
        handler.messageReceived(session, BYTES);
        inOrder.verify(session, times(1)).getId();
        inOrder.verify(session, times(1)).getAttribute(SerialEndpoint.ATTRIBUTE_SERIAL_ADDRESS);
        inOrder.verify(address, times(1)).getName();
        inOrder.verify(consumer1, times(1)).handle(SID, PORT, BYTES);
        inOrder.verify(consumer2, times(1)).handle(SID, PORT, BYTES);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialHandler#messageReceived(org.apache.mina.core.session.IoSession, java.lang.Object)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testMessageReceivedWithExceptionInFirstHandler() throws Exception {
        doThrow(exception).when(consumer1).handle(SID, PORT, BYTES);
        handler.addConsumer(consumer1);
        handler.addConsumer(consumer2);
        handler.messageReceived(session, BYTES);
        inOrder.verify(session, times(1)).getId();
        inOrder.verify(session, times(1)).getAttribute(SerialEndpoint.ATTRIBUTE_SERIAL_ADDRESS);
        inOrder.verify(address, times(1)).getName();
        inOrder.verify(consumer1, times(1)).handle(SID, PORT, BYTES);
        inOrder.verify(consumer1, times(1)).handleProcessingException(SID, PORT, BYTES, exception);
        inOrder.verify(consumer2, times(1)).handle(SID, PORT, BYTES);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialHandler#messageReceived(org.apache.mina.core.session.IoSession, java.lang.Object)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testMessageReceivedWithExceptionInSecondHandler() throws Exception {
        doThrow(exception).when(consumer2).handle(SID, PORT, BYTES);
        handler.addConsumer(consumer1);
        handler.addConsumer(consumer2);
        handler.messageReceived(session, BYTES);
        inOrder.verify(session, times(1)).getId();
        inOrder.verify(session, times(1)).getAttribute(SerialEndpoint.ATTRIBUTE_SERIAL_ADDRESS);
        inOrder.verify(address, times(1)).getName();
        inOrder.verify(consumer1, times(1)).handle(SID, PORT, BYTES);
        inOrder.verify(consumer2, times(1)).handle(SID, PORT, BYTES);
        inOrder.verify(consumer2, times(1)).handleProcessingException(SID, PORT, BYTES, exception);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialHandler#messageReceived(org.apache.mina.core.session.IoSession, java.lang.Object)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testMessageReceivedWithExceptionInBothHandlers() throws Exception {
        doThrow(exception).when(consumer1).handle(SID, PORT, BYTES);
        doThrow(exception).when(consumer2).handle(SID, PORT, BYTES);
        handler.addConsumer(consumer1);
        handler.addConsumer(consumer2);
        handler.messageReceived(session, BYTES);
        inOrder.verify(session, times(1)).getId();
        inOrder.verify(session, times(1)).getAttribute(SerialEndpoint.ATTRIBUTE_SERIAL_ADDRESS);
        inOrder.verify(address, times(1)).getName();
        inOrder.verify(consumer1, times(1)).handle(SID, PORT, BYTES);
        inOrder.verify(consumer1, times(1)).handleProcessingException(SID, PORT, BYTES, exception);
        inOrder.verify(consumer2, times(1)).handle(SID, PORT, BYTES);
        inOrder.verify(consumer2, times(1)).handleProcessingException(SID, PORT, BYTES, exception);
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialHandler#messageSent(org.apache.mina.core.session.IoSession, java.lang.Object)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testMessageSent() throws Exception {
        handler.messageSent(session, buffer);
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialHandler#sessionClosed(org.apache.mina.core.session.IoSession)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testSessionClosed() throws Exception {
        handler.sessionClosed(session);
        inOrder.verify(session, times(1)).getId();
        inOrder.verify(session, times(1)).getAttribute(SerialEndpoint.ATTRIBUTE_SERIAL_ADDRESS);
        inOrder.verify(address, times(1)).getName();
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialHandler#sessionCreated(org.apache.mina.core.session.IoSession)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testSessionCreated() throws Exception {
        handler.sessionCreated(session);
        inOrder.verify(session, times(1)).getId();
        inOrder.verify(session, times(1)).getAttribute(SerialEndpoint.ATTRIBUTE_SERIAL_ADDRESS);
        inOrder.verify(address, times(1)).getName();
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialHandler#sessionOpened(org.apache.mina.core.session.IoSession)}
     * .
     * 
     * @throws Exception
     */
    @Test
    public void testSessionOpened() throws Exception {
        handler.sessionOpened(session);
        inOrder.verify(session, times(1)).getId();
        inOrder.verify(session, times(1)).getAttribute(SerialEndpoint.ATTRIBUTE_SERIAL_ADDRESS);
        inOrder.verify(address, times(1)).getName();
        inOrder.verifyNoMoreInteractions();
    }

    /**
     * Test method for
     * {@link eu.estcube.camel.serial.SerialHandler#getSerialPortName(org.apache.mina.core.session.IoSession)}
     * .
     */
    @Test
    public void testGetSerialPortName() {
        when(session.getAttribute(SerialEndpoint.ATTRIBUTE_SERIAL_ADDRESS)).thenReturn(address, (SerialAddress) null);
        assertEquals(PORT, handler.getSerialPortName(session));
        assertEquals("N/A", handler.getSerialPortName(session));
        inOrder.verify(session, times(1)).getAttribute(SerialEndpoint.ATTRIBUTE_SERIAL_ADDRESS);
        inOrder.verify(address, times(1)).getName();
        inOrder.verify(session, times(1)).getAttribute(SerialEndpoint.ATTRIBUTE_SERIAL_ADDRESS);
        inOrder.verifyNoMoreInteractions();
    }
}
