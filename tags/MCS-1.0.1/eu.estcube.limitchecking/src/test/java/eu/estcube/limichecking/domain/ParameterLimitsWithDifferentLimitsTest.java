package eu.estcube.limichecking.domain;

import static org.junit.Assert.assertEquals;

import org.hbird.exchange.core.Parameter;
import org.junit.Before;
import org.junit.Test;

import eu.estcube.limitchecking.data.ParameterLimits;

public class ParameterLimitsWithDifferentLimitsTest {

    private String id;
    private String check;
    private String softStr;
    private String hardStr;
    private String sanityStr;
    private boolean[] inSoft;
    private boolean[] inHard;
    private boolean[] inSanity;
    private ParameterLimits[] limits;
    private Parameter test;

    @Before
    public void createInfoContainerWithSanityLimits() {

        id = "test2";

        check = "both";

        softStr = "40;50";

        hardStr = "37.2;65.37";

        sanityStr = "2.4;90";

        test = new Parameter(id, "test param");

        test.setValue(38);
        limits = new ParameterLimits[8];
        limits[0] = new ParameterLimits(id, check, softStr, hardStr, sanityStr);
        limits[1] = new ParameterLimits(id, check, null, hardStr, sanityStr);
        limits[2] = new ParameterLimits(id, check, softStr, null, sanityStr);
        limits[3] = new ParameterLimits(id, check, softStr, hardStr, null);
        limits[4] = new ParameterLimits(id, check, null, null, sanityStr);
        limits[5] = new ParameterLimits(id, check, null, hardStr, null);
        limits[6] = new ParameterLimits(id, check, softStr, null, null);
        limits[7] = new ParameterLimits(id, check, null, null, null);

        inSoft = new boolean[] { false, false, false, false, false, false, false, false };

        inHard = new boolean[] { true, true, false, true, false, true, false, false };

        inSanity = new boolean[] { true, true, true, false, true, false, false, false };

    }

    @Test
    public void testLimitChecking() {
        for (int i = 0; i < limits.length; i++) {
            assertEquals(inSoft[i], limits[i].isInSoft(test));
            assertEquals(inHard[i], limits[i].isInHard(test));
            assertEquals(inSanity[i], limits[i].isInSanity(test));
        }
    }

}
