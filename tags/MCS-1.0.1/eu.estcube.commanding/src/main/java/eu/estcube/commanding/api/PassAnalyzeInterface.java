package eu.estcube.commanding.api;

import java.util.List;

import org.hbird.exchange.navigation.PointingData;

import eu.estcube.commanding.analysis.OverPassType;

public interface PassAnalyzeInterface {

	/**
	 * Calculates {@link OverPassType} for given input.
	 * 
	 * Input is "raw" data from the Orekit.
	 * 
	 * @param input {@link List} of unoptimized {@link PointingData}
	 * @return {@link OverPassType} for input
	 */
	public OverPassType calculateType(List<PointingData> input);

}
