package eu.estcube.commanding.calculation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.common.Constants;

class ContactTimeProducerForTesting {

	private static final Logger LOG = LoggerFactory.getLogger(AMQsend.class);
	private static String amqUrl = "tcp://localhost:61616";
	public static String TOPIC = Constants.AMQ_COMMANDS_RECEIVE_TIME;
	public static String HEADER = "lunchTime";
	public static String HEADER_START = Constants.CMDG_HEADER_START_TIME;
	public static String HEADER_END = Constants.CMDG_HEADER_END_TIME;

	public static List<Long> contactTimeList = new ArrayList<Long>();

	public static void main(final String[] args) throws Exception {

		final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
				amqUrl);
		final Connection connection = connectionFactory.createConnection();
		connection.start();
		final Session session = connection.createSession(false,
				Session.AUTO_ACKNOWLEDGE);
		final Destination destination = session.createQueue(TOPIC);
		final MessageProducer producer = session.createProducer(destination);
		final Date date = new Date();
		final long start = (date.getTime()/* +3*60*1000 */);
		final long end = start + 600000; // current time + 10 minutes

		ObjectMessage message = session.createObjectMessage(start);
		message.setStringProperty(HEADER, HEADER_START);
		producer.send(message);

		message = session.createObjectMessage(end);
		message.setStringProperty(HEADER, HEADER_END);
		LOG.info("Sending message");
		producer.send(message);

		LOG.info("Message list sent to Calculator");
		LOG.info(date.toString());
		contactTimeList.clear();

	}

}
