package eu.estcube.commanding;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import org.hbird.business.groundstation.configuration.RotatorDriverConfiguration;
import org.junit.Test;

import eu.estcube.commanding.api.RotatorTypeInterface;
import eu.estcube.commanding.calculation.RotatorTypeSelectorImpl;
import eu.estcube.commanding.rotator1.Rotator1;
import eu.estcube.commanding.rotator2.Rotator2;

public class RotatorTypeSelectorImplTest {

	@Test
	public void test_type1() {
		final RotatorTypeSelectorImpl selector = new RotatorTypeSelectorImpl();
		final RotatorDriverConfiguration config = new RotatorDriverConfiguration();
		config.setMaxAzimuth(360D);
		config.setMaxElevation(180D);
		config.setDeviceName("dummy");
		final RotatorTypeInterface type = selector.selectFor(config);
		assertEquals(type.getClass(), Rotator1.class);

	}

	@Test
	public void test_type2() {
		final RotatorTypeSelectorImpl selector = new RotatorTypeSelectorImpl();
		final RotatorDriverConfiguration config = new RotatorDriverConfiguration();
		config.setMaxAzimuth(360D);
		config.setMaxElevation(450D);
		config.setDeviceName("dummy");
		final RotatorTypeInterface type = selector.selectFor(config);
		assertEquals(type.getClass(), Rotator2.class);

	}

	@Test
	public void test_type2AzFalse() {
		final RotatorTypeSelectorImpl selector = new RotatorTypeSelectorImpl();
		final RotatorDriverConfiguration config = new RotatorDriverConfiguration();
		config.setMaxAzimuth(180D);
		config.setMaxElevation(200D);
		config.setDeviceName("dummy");
		final RotatorTypeInterface type = selector.selectFor(config);
		assertNotSame(type.getClass(), Rotator1.class);

	}

	@Test
	public void test_type2ElFalse() {
		final RotatorTypeSelectorImpl selector = new RotatorTypeSelectorImpl();
		final RotatorDriverConfiguration config = new RotatorDriverConfiguration();
		config.setMaxAzimuth(50D);
		config.setMaxElevation(360D);
		config.setDeviceName("dummy");
		final RotatorTypeInterface type = selector.selectFor(config);
		assertNotSame(type.getClass(), Rotator1.class);

	}

}
