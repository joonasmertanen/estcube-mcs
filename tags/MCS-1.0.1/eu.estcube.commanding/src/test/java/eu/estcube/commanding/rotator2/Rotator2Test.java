package eu.estcube.commanding.rotator2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.estcube.commanding.analysis.OptimizationType;
import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.optimizators.Optimization;

public class Rotator2Test {
	Rotator2 rotator2 = new Rotator2(7.5);

	@Test
	public void crossElevationHighTest() {

		final OverPassType type = OverPassType.CROSS_ELEVATION_HIGH;
		final Optimization optimizationCase = rotator2.getOptimizer(type);
		assertEquals(OptimizationType.ROT_2_CROSS_EL_HIGH,
				optimizationCase.getOptType());
	}

	@Test
	public void crossElevationLowTest() {

		final OverPassType type = OverPassType.CROSS_ELEVATION_LOW;
		final Optimization optimizationCase = rotator2.getOptimizer(type);
		assertEquals(OptimizationType.ROT_2_CROSS_EL_LOW,
				optimizationCase.getOptType());
	}

	@Test
	public void noCrossElevationHighTest() {

		final OverPassType type = OverPassType.NO_CROSS_ELEVATION_HIGH;
		final Optimization optimizationCase = rotator2.getOptimizer(type);
		assertEquals(OptimizationType.ROT_1_NO_CROSS_EL_HIGH,
				optimizationCase.getOptType());
	}

	@Test
	public void noCrossElevationLowTest() {

		final OverPassType type = OverPassType.NO_CROSS_ELEVATION_LOW;
		final Optimization optimizationCase = rotator2.getOptimizer(type);
		assertEquals(OptimizationType.ROT_1_NO_CROSS_EL_LOW,
				optimizationCase.getOptType());
	}

}