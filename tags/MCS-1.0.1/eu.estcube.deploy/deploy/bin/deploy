#!/bin/bash
rxtx=false

if [[ $1 = "-r" ]]; then
    rxtx=true
    shift
fi

if [[ $# < 1 ]]; then
    printf "Missing config file.\n"
    printf "Usage: [-q] [-r] %s <file>\n" "$0"
    exit -1
else
    file=$1

    # add new line to the end of the file in case it's missing
    sed -i -e '$a\' $file

    index=0
    while read line; do
        services[$index]="$line"
        index=$(($index+1))
    done < $file
fi

# unzip the assemblies that are only stated in conf/services.list
zipList=$(ls *.zip)
for z in $zipList; do
    for service in ${services[@]}; do
        s=$(echo $service | sed 's/^#//')
        if [[ "$z" =~ ^"$s".* ]]; then
            echo "Unzipping $z"
            unzip -o -q $z;
            continue
        fi
    done
done

if $rxtx ; then
    service=eu.estcube.gs.tnc
    dir=$(pwd)

    if [ ! -f "$dir/$service/bin/run" ]; then
        printf "Service %s not found\n" "$service"
    else
        # copy new rxtx files
        printf "%s\n" "Copied rxtx lib files to $service from /conf/rxtx"
        cp -f $dir/conf/rxtx/* $dir/$service/lib
    fi
fi

dir=$( cd "$( dirname "$0" )" && pwd )
# iterate over the list of services to start them 
# modify IFS to skip spaces in for loop
oldIFS=$IFS
IFS=$(echo -en "\n\b")

for service in ${services[@]}
do
    s=$(echo $service | sed 's/^#//')
    [ -z "$service" ] && continue # skip empty lines
    [ ! -d "$s" ] && continue # skip non directories
    [[ "$service" = "bin" ]] && continue # skip bin/
    [[ "$service" = "conf" ]] && continue # skip conf/
    chmod +x $s/bin/run # set execution rights
    [[ "$service" == \#* ]] && continue # skip comments
    printf "%s\n" "$($dir/start $service)"
done
# restore IFS
IFS=$oldIFS

exit 0
