package eu.estcube.proxy.servlet;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.estcube.common.ByteUtil;
import eu.estcube.proxy.model.Ax25UiFrameRemote;
import eu.estcube.proxy.model.Location;

/**
 * Servlet for handling POST requests with frames from 3rd party groundstations
 * 
 * @author Kaarel Hanson
 * @since 12.03.2014
 */
@Component
public class RemotePacketServlet extends HttpServlet {

    private static final Logger LOG = LoggerFactory.getLogger(RemotePacketServlet.class);

    private static final long serialVersionUID = -220905184637484559L;

    public static final String POST_PARAMETER_SOURCE = "source";
    public static final String POST_PARAMETER_TIMESTAMP = "timestamp";
    public static final String POST_PARAMETER_FRAME = "frame";
    public static final String POST_PARAMETER_LOCATOR = "locator";
    public static final String POST_PARAMETER_WWL = "wwl";
    public static final String POST_PARAMETER_LONGITUDE = "longitude";
    public static final String POST_PARAMETER_LATITUDE = "latitude";
    public static final String POST_PARAMETER_NORAD_ID = "noradID";

    @EndpointInject(uri = "direct:frame-submit")
    private ProducerTemplate producer;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        for (Enumeration<?> headerNames = req.getHeaderNames(); headerNames.hasMoreElements();) {
            Object nextElement = headerNames.nextElement();
            LOG.debug("Header: " + nextElement + " - " + req.getHeader((String) nextElement));
        }
        Map<String, String> properties = new HashMap<String, String>();
        Map<?, ?> map = req.getParameterMap();
        String[] values;
        for (Entry<?, ?> entry : map.entrySet()) {
            values = (String[]) entry.getValue();
            properties.put(entry.getKey().toString(), values[0]);
            LOG.debug(String.format("%s - %s", entry.getKey(), values[0]));
        }
        Ax25UiFrameRemote input = new Ax25UiFrameRemote();
        input.setSourceHandle(properties.remove(POST_PARAMETER_SOURCE));
        input.setTimestamp(properties.remove(POST_PARAMETER_TIMESTAMP));
        String locator = properties.remove(POST_PARAMETER_LOCATOR);
        if (locator.equalsIgnoreCase("wwl")) {
            input.setLocation(new Location(properties.remove(POST_PARAMETER_WWL)));
        } else if (locator.equalsIgnoreCase("longLat")) {
            input.setLocation(new Location(properties.remove(POST_PARAMETER_LONGITUDE), properties
                    .remove(POST_PARAMETER_LATITUDE)));
        }
        String frame = properties.remove(POST_PARAMETER_FRAME);
        input.setData(ByteUtil.toBytesFromHexString(frame));
        LOG.info(String.format("Received packet. Source - %s, Location - %s, Norad ID - %s", input.getSourceHandle(),
                input.getLocation(), properties.get(POST_PARAMETER_NORAD_ID)));

        input.setProperites(properties);
        producer.sendBody(input);
    }
}
