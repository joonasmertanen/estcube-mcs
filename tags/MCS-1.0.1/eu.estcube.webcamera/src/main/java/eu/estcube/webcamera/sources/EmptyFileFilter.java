package eu.estcube.webcamera.sources;

import java.io.File;

import org.apache.camel.component.file.GenericFile;
import org.apache.camel.component.file.GenericFileFilter;

public class EmptyFileFilter implements GenericFileFilter<File> {

    @Override
    public boolean accept(GenericFile<File> file) {
        return file.getFileLength() > 0;
    }

}
