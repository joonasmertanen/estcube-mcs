/** 
 *
 */
package eu.estcube.sdc.xml;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;


import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

import eu.estcube.sdc.domain.Ax25UiFrameOutput;

/**
 *
 */
public class Ax25UiFrameOutputSerializerTest {

    private static final String DEST_ADDR = "01 02 03 04 05 06 07";
    private static final String SRC_ADDR = "07 06 05 04 03 02 01";
    private static final String CTRL = "0A";
    private static final String PID = "0D";
    private static final String INFO = "0D 0E 0A 0D 0C 00 0D 0E";
    private static final int PORT = 2;
    private static final String DATE = "2013-03-19T23:36:59.002";
    private static final String SERIAL_PORT_NAME = "/dev/ttyS9";
    private static final String SERVICE_ID = "eu.estcube.test";
    
    private Ax25UiFrameOutput frame;
    
    private Ax25UiFrameOutputSerializer serializer;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        frame = new Ax25UiFrameOutput();
        frame.setDestAddr(DEST_ADDR);
        frame.setSrcAddr(SRC_ADDR);
        frame.setCtrl(CTRL);
        frame.setPid(PID);
        frame.setInfo(INFO);
        frame.setTncTargetPort(PORT);
        frame.setTimestamp(DATE);
        frame.setSerialPortName(SERIAL_PORT_NAME);
        frame.setServiceId(SERVICE_ID);
        serializer = new Ax25UiFrameOutputSerializer();
    }

    /**
     * Test method for {@link eu.estcube.sdc.xml.Ax25UiFrameOutputSerializer#serialize(eu.estcube.sdc.domain.Ax25UiFrameOutput)}.
     * @throws Exception 
     */
    @Test
    public void testSerialize() throws Exception {
        byte[] bytes = serializer.serialize(frame);
        String xml = new String(bytes, "UTF-8");
        XStream xstream = new XStream(new StaxDriver());
        xstream.alias("frames", List.class);
        xstream.alias("frame", Ax25UiFrameOutput.class);
        
        @SuppressWarnings("unchecked")
        List<Ax25UiFrameOutput> list = (List<Ax25UiFrameOutput>) xstream.fromXML(xml);
        assertEquals(1, list.size());
        Ax25UiFrameOutput result = list.get(0);
        assertEquals(DEST_ADDR, result.getDestAddr());
        assertEquals(SRC_ADDR, result.getSrcAddr());
        assertEquals(CTRL, result.getCtrl());
        assertEquals(PID, result.getPid());
        assertEquals(INFO, result.getInfo());
        assertEquals(PORT, result.getTncTargetPort());
        assertEquals(DATE, result.getTimestamp());
        assertEquals(SERIAL_PORT_NAME, result.getSerialPortName());
        assertEquals(SERVICE_ID, result.getServiceId());
        assertNull(result.getPrefix());
    }
    
    /**
     * Test method for {@link eu.estcube.sdc.xml.Ax25UiFrameOutputSerializer#serialize(eu.estcube.sdc.domain.Ax25UiFrameOutput)}.
     * @throws Exception 
     */
    @Test
    public void testSerializeAndCompare() throws Exception {
        byte[] bytes = serializer.serialize(frame);
        String xml = new String(bytes, "UTF-8");
        String fromFile = FileUtils.readFileToString(new File(getClass().getResource("/downlink-01.xml").toURI()));
        assertEquals("Should match in case line breaks (all white spaces) are ignored", fromFile.replaceAll("\\s", ""), xml.replaceAll("\\s", ""));
    }    
}
