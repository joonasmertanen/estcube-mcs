package eu.estcube.codec.gcp.struct;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import eu.estcube.codec.gcp.exceptions.SubsystemNotFoundException;

public class GcpStruct {
    private static final Logger LOG = LoggerFactory.getLogger(GcpStruct.class);

    private final List<GcpCommand> commands = new ArrayList<GcpCommand>();

    private final List<GcpReply> replies = new ArrayList<GcpReply>();

    public GcpStruct() {
    }

    public GcpStruct(String commandsXmlPath, String repliesXmlPath) throws Exception {
        try {
            parseCommandsFromXml(loadXml(new FileInputStream(commandsXmlPath)));
            parseRepliesFromXml(loadXml(new FileInputStream(repliesXmlPath)));
        } catch (FileNotFoundException e) {
            LOG.error("Have not found commands/replies configuration files, initializing from fallback XML. Exception: "
                    + e);
            initFromXml();
        }
    }

    public void initFromXml() throws Exception {
        parseCommandsFromXml(loadResourceXml("commands.xml"));
        parseRepliesFromXml(loadResourceXml("replies.xml"));
    }

    private Document loadXml(InputStream source) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(source);

        return doc;
    }

    public Document loadResourceXml(String file) throws Exception {
        InputStream fXmlFile = getClass().getResourceAsStream("/" + file);

        return loadXml(fXmlFile);
    }

    public void parseCommandsFromXml(Document doc) {

        doc.getDocumentElement().normalize();

        NodeList nList = doc.getElementsByTagName("command");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            addCommand(new GcpCommand((Element) nList.item(temp)));
        }

    }

    public void parseRepliesFromXml(Document doc) {

        doc.getDocumentElement().normalize();

        NodeList nList = doc.getElementsByTagName("reply");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            addReply(new GcpReply((Element) nList.item(temp)));
        }

    }

    public GcpCommand getCommand(String name, Integer subSysId) {
        for (GcpCommand command : commands) {
            if (command.getName().equals(name)) {
                for (GcpSubsystem subSys : command.getSubsystems()) {
                    int tempId = -1;
                    try {
                        tempId = GcpSubsystemIdProvider.getId(subSys.getName());
                    } catch (SubsystemNotFoundException e) {
                        return null;
                    }
                    if (tempId == subSysId) {
                        return command;

                    }

                }
            }
        }
        return null;
    }

    public GcpCommand getCommand(Integer id, Integer subSysId) {
        for (GcpCommand command : commands) {
            if (command.getId() == id) {
                for (GcpSubsystem subSys : command.getSubsystems()) {
                    int tempId = -1;
                    try {
                        tempId = GcpSubsystemIdProvider.getId(subSys.getName());
                    } catch (SubsystemNotFoundException e) {
                        return null;
                    }
                    if (tempId == subSysId) {
                        return command;

                    }

                }

            }
        }
        return null;
    }

    public void addCommand(GcpCommand command) {
        commands.add(command);
    }

    public List<GcpCommand> getCommands() {
        return commands;
    }

    public GcpReply getReply(Integer id, String subsystem) {
        for (GcpReply reply : replies) {
            if (reply.getId() == id) {
                for (GcpSubsystem sub : reply.getSubsystems()) {
                    if (sub.getName().equals(subsystem)) {
                        return reply;
                    }
                }
            }
        }
        return null;
    }

    public void addReply(GcpReply reply) {
        replies.add(reply);
    }

    public List<GcpReply> getReplies() {
        return replies;
    }

}
