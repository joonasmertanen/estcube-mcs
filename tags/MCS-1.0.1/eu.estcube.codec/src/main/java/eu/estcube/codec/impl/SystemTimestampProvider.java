package eu.estcube.codec.impl;

import eu.estcube.codec.TimestampProvider;

/**
 * A time stamp provider that uses system time directly.
 */
public class SystemTimestampProvider implements TimestampProvider {

	@Override
	public long getTimestamp() {
		return System.currentTimeMillis() / 1000;
	}

}
