package eu.estcube.codec.pkt.impl;

import eu.estcube.codec.SscProvider;
import eu.estcube.codec.TimestampProvider;
import eu.estcube.codec.pkt.TCPacketiser;
import eu.estcube.domain.transport.pkt.TCPacket;
import eu.estcube.domain.transport.pkt.TCPacketFramePrimaryHeader;
import eu.estcube.domain.transport.pkt.TCPacketFrameSecondaryHeader;
import eu.estcube.domain.transport.pkt.TCPacketPrimaryFrame;
import eu.estcube.domain.transport.pkt.TCPacketSecondaryFrame;

/**
 * Creates ESTCube-1 TC packet structure from the given payload (command).
 */
public class TCPacketiserImpl implements TCPacketiser {
	private SscProvider sscProvider;
	private TimestampProvider timestampProvider;
	private int frameSize;

	public TCPacketiserImpl(SscProvider sscProvider, TimestampProvider timestampProvider, int frameSize) {
		super();
		this.sscProvider = sscProvider;
		this.timestampProvider = timestampProvider;
		this.frameSize = frameSize;
	}

	public SscProvider getSscProvider() {
		return sscProvider;
	}

	public TimestampProvider getTimestampProvider() {
		return timestampProvider;
	}

	public int getFrameSize() {
		return frameSize;
	}

	@Override
	public TCPacket create(int spid, long et, byte[] payload) {
		int primaryHeaderSize = getPrimaryHeaderSize();
		int secondaryHeaderSize = getSecondarHeaderNumBytes();

		int primaryBodySize = frameSize - primaryHeaderSize;
		int secondaryBodySize = frameSize - secondaryHeaderSize;

		int frameCount = calculateFrameCount(frameSize, primaryHeaderSize, secondaryHeaderSize, payload.length);
		int ssc = sscProvider.getNext(frameCount);
		long ts = timestampProvider.getTimestamp();

		TCPacket packet = new TCPacket();

		int bytesToWrite;
		int bytesWritten = 0;
		int framesWritten = 0;
		byte[] body;

		// Primary header
		TCPacketFramePrimaryHeader primaryHeader = new TCPacketFramePrimaryHeader(ssc, frameCount, spid, ts,
				new byte[4], et);

		// Calculate primary body
		bytesToWrite = Math.min(primaryBodySize, (payload.length - bytesWritten));
		body = new byte[bytesToWrite];
		System.arraycopy(payload, bytesWritten, body, 0, bytesToWrite);
		bytesWritten += bytesToWrite;
		framesWritten++;

		// Set primary header and body
		TCPacketPrimaryFrame primaryFrame = new TCPacketPrimaryFrame(primaryHeader, body);
		packet.setPrimaryFrame(primaryFrame);

		// Continue until all payload has been written
		while (bytesWritten < payload.length) {
			// Secondary header
			ssc++;
			TCPacketFrameSecondaryHeader secondaryHeader = new TCPacketFrameSecondaryHeader(ssc, frameCount);

			// Secondary body
			bytesToWrite = Math.min(secondaryBodySize, (payload.length - bytesWritten));
			body = new byte[bytesToWrite];
			System.arraycopy(payload, bytesWritten, body, 0, bytesToWrite);
			bytesWritten += bytesToWrite;
			framesWritten++;

			// Add secondary header and body
			TCPacketSecondaryFrame secondaryFrame = new TCPacketSecondaryFrame(secondaryHeader, body);
			packet.addSecondaryFrame(secondaryFrame);
		}

		// Check if number of frames match, just in case
		if (framesWritten != frameCount)
			throw new RuntimeException("Actual frame count does not match planned frame count");

		return packet;
	}

	protected int getPrimaryHeaderSize() {
		return TCPacketFramePrimaryHeader.SSC_BYTES + TCPacketFramePrimaryHeader.LEN_BYTES
				+ TCPacketFramePrimaryHeader.SPID_BYTES + TCPacketFramePrimaryHeader.TS_BYTES
				+ TCPacketFramePrimaryHeader.MAC_BYTES + TCPacketFramePrimaryHeader.ET_BYTES;
	}

	protected int getSecondarHeaderNumBytes() {
		return TCPacketFrameSecondaryHeader.SSC_BYTES + TCPacketFrameSecondaryHeader.LEN_BYTES;
	}

	protected int calculateFrameCount(int frameSize, int primaryHeaderSize, int secondaryHeaderSize, int bodySize) {

		// Bytes for body in the first frame
		int primaryBodyBytes = frameSize - primaryHeaderSize;
		if (primaryBodyBytes < 1) {
			throw new IllegalArgumentException("Frame size too small, no space left for body in primary frame");
		}

		// Bytes for body in the second frame
		int secondaryBodyBytes = frameSize - secondaryHeaderSize;
		if (secondaryBodyBytes < 1) {
			throw new IllegalArgumentException("Frame size too small, no space left for body in secondary frame(s)");
		}

		int bodyLeft = bodySize - primaryBodyBytes;

		// Only 1 frame needed
		if (bodyLeft <= 0)
			return 1;

		// 1+ frames needed

		return (int) Math.ceil((double) bodyLeft / (double) secondaryBodyBytes) + 1;
	}
}
