package eu.estcube.codec.gcp.exceptions;

public class TooManyCommandArgumentsException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -2786857398353700876L;

    public TooManyCommandArgumentsException(String commandName) {
        super("Command '" + commandName + "' needs less arguments than given. Check the command description below!");
    }
}
