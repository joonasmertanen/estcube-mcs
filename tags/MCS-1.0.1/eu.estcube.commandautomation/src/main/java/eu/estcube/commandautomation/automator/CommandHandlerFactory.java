package eu.estcube.commandautomation.automator;

import eu.estcube.commandautomation.db.Command;

public interface CommandHandlerFactory {

    public CommandHandler create(Command command);

}
