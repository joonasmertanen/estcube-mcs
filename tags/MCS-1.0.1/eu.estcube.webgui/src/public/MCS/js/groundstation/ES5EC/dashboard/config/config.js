define([
    "dojo/domReady!"
    ],

    function(ready) {

        return {
            routes: {
            	ES5EC_DASHBOARD: {
                    path: "ES5EC/dashboard",
                    defaults: {
                        controller: "ES5ECDashboard/DashboardController",
                        method: "index",
                    }
                },
            },


        };
    }
);