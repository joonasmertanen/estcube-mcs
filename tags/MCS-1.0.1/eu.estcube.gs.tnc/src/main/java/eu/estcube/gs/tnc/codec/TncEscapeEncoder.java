package eu.estcube.gs.tnc.codec;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class TncEscapeEncoder extends ProtocolEncoderAdapter {
    
    private static final Logger LOG = LoggerFactory.getLogger(TncEscapeEncoder.class);

    /** @{inheritDoc}. */
    @Override
    public void encode(IoSession session, Object message, ProtocolEncoderOutput out) throws Exception {
        
        if (!(message instanceof byte[])) {
            LOG.error("Unsupported message type in {} - {}. Only supported type is byte[]. Throwing exception", getClass().getSimpleName(), message.getClass().getName());
            throw new RuntimeException("Unsupported message type in " + getClass().getName() + " - " + message.getClass().getName());
        }
        
        byte[] in = (byte[]) message;
        IoBuffer buffer = IoBuffer.allocate(in.length * 2); // worst case scenario - all bytes has to be escaped
        for (byte b : in) {
            if (b == TncConstants.FEND) {
                // found FEND replace with FESC + TFEND
                buffer.put(TncConstants.FESC)
                      .put(TncConstants.TFEND);
            } else if (b == TncConstants.FESC) {
                // found FESC replace with FESC + TFESC
                buffer.put(TncConstants.FESC)
                      .put(TncConstants.TFESC);
            } else {
                buffer.put(b);
            }
        }
        buffer.flip();
        // Can't send bytes as IoBuffer - Mina will skip other downstream codecs in this case
        // sending as byte array
        byte[] result = new byte[buffer.remaining()];
        buffer.get(result);
        out.write(result);
    }
}
