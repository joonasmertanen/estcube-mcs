package eu.estcube.gs.tnc.codec;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoderAdapter;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

import eu.estcube.domain.transport.tnc.TncFrame;

/**
 *
 */
public class TncFrameDecoder extends ProtocolDecoderAdapter {

    /** @{inheritDoc}. */
    @Override
    public void decode(IoSession session, IoBuffer in, ProtocolDecoderOutput out) throws Exception {
        byte[] bytes = new byte[in.remaining()];
        in.get(bytes);
        TncFrame frame = TncFrames.toFrame(bytes);
        out.write(frame);
    }
}
