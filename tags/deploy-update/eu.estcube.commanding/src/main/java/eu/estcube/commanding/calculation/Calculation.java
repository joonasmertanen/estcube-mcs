package eu.estcube.commanding.calculation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.math.geometry.Vector3D;
import org.apache.log4j.Logger;
import org.hbird.business.groundstation.configuration.RotatorDriverConfiguration;
import org.hbird.exchange.navigation.PointingData;
import org.orekit.bodies.GeodeticPoint;
import org.orekit.bodies.OneAxisEllipsoid;
import org.orekit.errors.OrekitException;
import org.orekit.frames.FramesFactory;
import org.orekit.frames.TopocentricFrame;
import org.orekit.time.AbsoluteDate;
import org.orekit.time.TimeScalesFactory;
import org.orekit.tle.TLE;
import org.orekit.tle.TLEPropagator;

import eu.estcube.commanding.analysis.OverPassType;
import eu.estcube.commanding.analysis.PassAnalyze;
import eu.estcube.commanding.api.RotatorTypeInterface;
import eu.estcube.commanding.optimizators.GlobalFunctions;

/**
 * @author Ivar Mahhonin This class makes necessary calculations for antenna
 *         rotator so it could follow satellite position and radio
 * 
 */
public class Calculation {

	public static final double ROT_RECEIVING_TOLERANCE = 7.5;
	private static double firstAzimuth;
	private static double firstElevation;
	/** Equatorial radius of Earth */
	public static final double EARTH_RADIUS = 6378137.0;
	/** Flattening */
	public static final double FLATENNING = 1.0 / 298.257223563;
	/** A central attraction coefficient */
	public static final double MU = 3.986004415e+14;
	/** Speed of light */
	public static final double SPD_OF_LIGHT = 299792458.0;
	/** Longitude of the ground station */
	double GS_LONGITUDE = Math.toRadians(26.7330);
	/** Latitude of the ground station */
	double GS_LATITUDE = Math.toRadians(58.3000);
	/** Altitude of the ground station */
	double GS_ALTITUDE = 59.0;
	/** Radio uplink frequency */
	double uplink_fr;
	/** Radio downling frequenct */
	double downlink_fr;
	/** TLE data for satelite */
	TLE tle;
	/** For counting 3 seconds and generating new command for radio */
	static int counter = 0;
	/**
	 * Tolerance for elevation and azimuth. According to it we calculate this
	 * values with stepp
	 */
	double tolerance;
	/** Contact start time */
	long startTime;
	/** Contact end time */
	long endTime;
	/** Ground station antenna rotator maximum azimuth value */
	int gsMaxAz;
	/** Ground station antenna rotator maximum elevation value */
	int gsMaxEl;
	/** Ground station antenna receiving width */
	double gsReceivingSector;
	/** logger */
	public static Logger log = Logger.getLogger(Calculation.class.getName());

	/** Object constructor */

	public Calculation(final double gS_LONGITUDE, final double gS_LATITUDE,
			final double gS_ALTITUDE, final TLE tle, final double tolerance,
			final long startTime, final long endTime, final int gsMaxAz,
			final int gsMaxEl, final double gsReceivingSector) {
		super();
		GS_LONGITUDE = gS_LONGITUDE;
		GS_LATITUDE = gS_LATITUDE;
		GS_ALTITUDE = gS_ALTITUDE;
		this.tle = tle;
		this.tolerance = tolerance;
		this.startTime = startTime;
		this.endTime = endTime;
		this.gsMaxAz = gsMaxAz;
		this.gsMaxEl = gsMaxEl;
		this.gsReceivingSector = gsReceivingSector;
	}

	/**
	 * 
	 * @param es5ec
	 *            - TopocentriFrame class object in order to its methods for
	 *            calculating @param azimuth, @param elevation, @param doppler
	 * @param position
	 *            - satellite position
	 * @param now
	 *            - current date and time in UTC format
	 * @throws OrekitException
	 */
	public double calculateAzimuth(final TopocentricFrame es5ec,
			final Vector3D position, AbsoluteDate now) throws OrekitException {
		double azimuth = 0;
		now = now.shiftedBy(0.5 * 1D);
		final double azimuthRad = es5ec.getAzimuth(position,
				FramesFactory.getCIRF2000(), now);
		azimuth = (int) (Math.toDegrees(azimuthRad) * 10) / 10;
		return azimuth;
	}

	/**
	 * 
	 * @param es5ec
	 *            - TopocentriFrame class object in order to its methods for
	 *            calculating @param azimuth, @param elevation, @param doppler
	 * @param position
	 *            - satellite position
	 * @param now
	 *            - current date and time in UTC format
	 * @throws OrekitException
	 */
	public double calculateElevation(final TopocentricFrame es5ec,
			final Vector3D position, AbsoluteDate now) throws OrekitException {
		double elevation = 0;
		now = now.shiftedBy(0.5 * 1D);
		final double elevationRad = es5ec.getElevation(position,
				FramesFactory.getCIRF2000(), now);

		elevation = (int) (Math.toDegrees(elevationRad) * 10) / 10;
		return elevation;
	}

	/**
	 * Calculating duration of a calculation period
	 * 
	 * @return
	 */

	public long calculateScheduledPeriod(final Date start, final Date end) {
		long schPeriod;
		final long startMilliSec = start.getTime();
		final long endMilliSec = end.getTime();
		schPeriod = (endMilliSec - startMilliSec);
		schPeriod = TimeUnit.MILLISECONDS.toSeconds(schPeriod);
		log.info("Period  is " + schPeriod + " seconds");
		return schPeriod;

	}

	/**
	 * Calculating @param azimuth, @param elevation, @param doppler in loop,
	 * while contact time is continues.
	 * 
	 * @param now
	 *            - current date and time in UTC format
	 * @param es5ec
	 *            -TopocentriFrame class object in order to its methods for
	 *            calculating @param azimuth, @param elevation, @param doppler
	 * @param propagator
	 *            - the models used are SGP4 and SDP4, initially proposed by
	 *            NORAD as the unique convenient
	 * @param station
	 *            -3D vector that defines our ground station position
	 * @param hoursStart
	 *            - setting hours of contact start time point
	 * @param hoursEnd
	 *            - setting minutes of contact start time point
	 * @param minStart
	 *            - setting hours of contact end time point
	 * @param minutesEnd
	 *            - setting minutes of contact end time point
	 * @throws Exception
	 */

	void calculateTrajectory(AbsoluteDate now, final TopocentricFrame es5ec,
			final TLEPropagator propagator, final GeodeticPoint station,
			final Date start, final long schPeriod) throws Exception {
		Date timeStamp = new Date(start.getTime());
		List<PointingData> coordinates = new ArrayList<PointingData>();

		Vector3D position = propagator.getPVCoordinates(now).getPosition();
		firstAzimuth = calculateAzimuth(es5ec, position, now);
		firstElevation = calculateElevation(es5ec, position, now);

		for (int second = 0; second < schPeriod * 2; second++) {

			position = propagator.getPVCoordinates(now).getPosition();

			final double az = calculateAzimuth(es5ec, position, now);
			final double el = calculateElevation(es5ec, position, now);

			coordinates = sendCommandsForRotatorViaAMQ(coordinates, az, el,
					second, timeStamp.getTime());

			counter++;
			timeStamp = increaseTimeStamp(timeStamp, 500);
			now = now.shiftedBy(0.5 * 1D);
		}

		try {

			coordinates = GlobalFunctions.listFinalCheck(coordinates);
			GlobalFunctions.printValues(coordinates);

			final PassAnalyze analyze = new PassAnalyze();
			final RotatorDriverConfiguration config = new RotatorDriverConfiguration();
			config.setMaxAzimuth(gsMaxAz);
			config.setMaxElevation(gsMaxEl);
			config.setDeviceName("dummy");

			optimizeTrackingTrajectory(analyze, config, coordinates);

		} catch (final IndexOutOfBoundsException e) {
			System.out.println("Elevation is all negative: " + e);
		}

		AMQsend.endConnection();

	}

	public void generateCommandsForRotator(final List<PointingData> coordinates)
			throws Exception {
		for (int i = 0; i < coordinates.size(); i++) {
			final long timeStamp = coordinates.get(i).getTimestamp();
			final String azim = String.valueOf(coordinates.get(i).getAzimuth());
			final int dotPosAzim = azim.indexOf(".");
			final String elev = String.valueOf(coordinates.get(i)
					.getElevation());
			final int dotPosElev = elev.indexOf(".");
			AMQsend.sendToRotator(azim.substring(0, dotPosAzim),
					elev.substring(0, dotPosElev), timeStamp);
		}

	}

	/**
	 * 
	 * @param timeStamp
	 *            - getting timestamp for each command
	 * @param stepp
	 *            - timestamp stepp
	 * @return
	 */

	public Date increaseTimeStamp(Date timeStamp, final int stepp) {
		final Calendar c = Calendar.getInstance();
		c.setTime(timeStamp);

		c.add(Calendar.MILLISECOND, stepp);
		timeStamp = c.getTime();
		return timeStamp;
	}

	/**
	 * Here we are setting contact time period and running endless loop in
	 * waiting for the contact time to start.
	 * 
	 * @param args
	 * @throws Exception
	 */

	public void initCalculate() throws Exception {
		long schPeriod;
		final Date nowDate = new Date();
		log.info(nowDate.toString());
		System.setProperty("orekit.data.path",
				"src/main/resources/orekit-data.zip");
		final Date start = new Date(startTime);
		final Date end = new Date(endTime);
		log.info("Received contact time");
		schPeriod = calculateScheduledPeriod(start, end);

		final GeodeticPoint station = new GeodeticPoint(GS_LATITUDE,
				GS_LONGITUDE, GS_ALTITUDE);

		final OneAxisEllipsoid oae = new OneAxisEllipsoid(EARTH_RADIUS,
				FLATENNING, FramesFactory.getITRF2005());
		final TopocentricFrame es5ec = new TopocentricFrame(oae, station,
				"ES5EC");

		final AbsoluteDate now = new AbsoluteDate(start,
				TimeScalesFactory.getUTC());

		final TLEPropagator propagator = TLEPropagator.selectExtrapolator(tle);

		calculateTrajectory(now, es5ec, propagator, station, start, schPeriod);

	}

	public void optimizeTrackingTrajectory(final PassAnalyze analyze,
			final RotatorDriverConfiguration config,
			List<PointingData> coordinates) throws Exception {
		final OverPassType type = analyze.calculateType(coordinates);
		final RotatorTypeSelectorImpl selector = new RotatorTypeSelectorImpl();
		final RotatorTypeInterface rotatorType = selector.selectFor(config);
		final RotatorOptimizer doOptimize = new RotatorOptimizer();
		coordinates = doOptimize.doOptimize(coordinates, config);

		generateCommandsForRotator(coordinates);

	}

	/**
	 * Generating commands for Rotator. When azimuth or elevation values are
	 * changing to 3 degrees, then we are generating new command for rotator,
	 * where refreshes only parameter with changed value.
	 * 
	 * @param azimuth
	 *            - current azimuth
	 * @param elevation
	 *            - current elevation
	 * @param i
	 *            - generate command at the very first time
	 * @paran tinmeStamp - time, when command must be released from ActivqMQ
	 */

	public List<PointingData> sendCommandsForRotatorViaAMQ(
			final List<PointingData> coordinates, final double azimuth,
			final double elevation, final int i, final long timeStamp)
			throws Exception {

		if ((Math.abs(Math.abs(firstAzimuth) - Math.abs(azimuth)) >= 3)
				|| i == 1) {

			final PointingData element = new PointingData(timeStamp, azimuth,
					elevation, 0.0, "ESTCube-1", "Tartu");
			coordinates.add(element);
			firstAzimuth = azimuth;
		}

		if ((Math.abs(Math.abs(firstElevation) - Math.abs(elevation)) >= 3)) {
			final PointingData element = new PointingData(timeStamp, azimuth,
					elevation, 0.0, "ESTCube-1", "Tartu");
			coordinates.add(element);
			firstElevation = elevation;
		}
		return coordinates;

	}

}