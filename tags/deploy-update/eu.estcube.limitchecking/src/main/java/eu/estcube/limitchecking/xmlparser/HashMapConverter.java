package eu.estcube.limitchecking.xmlparser;

import java.math.BigDecimal;
import java.util.HashMap;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import eu.estcube.limitchecking.constants.LimitConstants;
import eu.estcube.limitchecking.domain.InfoContainer;

public class HashMapConverter implements Converter {

    @Override
    public boolean canConvert(Class c) {

        return c.equals(HashMap.class);
    }

    @Override
    public void marshal(Object arg0, HierarchicalStreamWriter arg1, MarshallingContext arg2) {

        // Not implemented as it is not needed.

    }

    @Override
    public HashMap<String, InfoContainer> unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

        HashMap<String, InfoContainer> map = new HashMap<String, InfoContainer>();

        while (reader.hasMoreChildren()) {

            reader.moveDown();

            String id = "";
            BigDecimal sanityLower = null;
            BigDecimal sanityUpper = null;
            BigDecimal hardLower = null;
            BigDecimal hardUpper = null;
            BigDecimal softLower = null;
            BigDecimal softUpper = null;

            while (reader.hasMoreChildren()) {
                reader.moveDown();

                if (reader.getNodeName().equals(LimitConstants.ID)) {

                    id = reader.getValue();

                } else {

                    while (reader.hasMoreChildren()) {

                        reader.moveDown();

                        if (reader.getNodeName().equals(LimitConstants.SANITY_LOWER)) {

                            sanityLower = new BigDecimal(reader.getValue());

                        } else if (reader.getNodeName().equals(LimitConstants.SANITY_UPPER)) {

                            sanityUpper = new BigDecimal(reader.getValue());

                        } else if (reader.getNodeName().equals(LimitConstants.HARD_LOWER)) {

                            hardLower = new BigDecimal(reader.getValue());

                        } else if (reader.getNodeName().equals(LimitConstants.HARD_UPPER)) {

                            hardUpper = new BigDecimal(reader.getValue());

                        } else if (reader.getNodeName().equals(LimitConstants.SOFT_LOWER)) {

                            hardLower = new BigDecimal(reader.getValue());
                        }

                        reader.moveUp();

                    }

                }
                reader.moveUp();

            }

            InfoContainer info = new InfoContainer(id, sanityLower, sanityUpper, hardLower, hardUpper, softLower,
                    softUpper);

            map.put(id, info);

        }

        return map;
    }

}
