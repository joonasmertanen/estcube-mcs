package eu.estcube.limitchecking.constants;

public class LimitConstants {

    public static final String ID = "id";
    public static final String SANITY_LOWER = "sanityLower";
    public static final String SANITY_UPPER = "sanityUpper";
    public static final String HARD_LOWER = "hardLower";
    public static final String HARD_UPPER = "hardUpper";
    public static final String SOFT_LOWER = "softLower";
    public static final String SOFT_UPPER = "softUpper";
    public static final String PATHPROPERTY = "path";

}
