#!/bin/sh

kill $(ps -u mcs-test -ef | grep java | grep eu.estcube.webserver | awk '{print $2}') 2>/dev/null

cd ~/estcube/
for z in ./*.zip; do unzip -o -qq $z; done

cd ~/estcube/eu.estcube.webserver/
mkdir logs
touch logs/webServer.log
chmod +x bin/run
bin/run >> logs/webServer.log 2>&1 &
