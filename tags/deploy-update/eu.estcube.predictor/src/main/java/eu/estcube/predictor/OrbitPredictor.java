package eu.estcube.predictor;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.hbird.exchange.configurator.StandardEndpoints;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.core.BusinessCard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import eu.estcube.common.PrepareForInjection;

public class OrbitPredictor extends RouteBuilder {

    public static final String DEFAULT_OREKIT_DATA_PATH = "./orekit-data.zip";

    private static final Logger LOG = LoggerFactory.getLogger(OrbitPredictor.class);

    @Value("${heart.beat.interval}")
    private int heartBeatInterval;

    @Value("${service.id}")
    private String serviceId;

    @Value("${service.version}")
    private String serviceVersion;

    @Value("${service.name}")
    private String serviceName;

    @Value("${gs.id}")
    private String gsId;

    @Value("${prediction.interval:60000}")
    private int predictionInterval;

    @Autowired
    private PrepareForInjection preparator;

    @Autowired
    private Predict predict;

    @Override
    public void configure() throws Exception {

        MDC.put(StandardArguments.ISSUED_BY, serviceId);

        from("timer://predict?fixedRate=true&period=" + predictionInterval)
                .bean(predict)
                .split(body())
                .process(preparator)
                .to("log:eu.estcube.prediction.stats?level=DEBUG&groupInterval=60000&groupDelay=60000&groupActiveOnly=false")
                .to(StandardEndpoints.MONITORING);

        BusinessCard card = new BusinessCard(serviceId, serviceName);
        card.setPeriod(heartBeatInterval);
        card.setDescription(String.format("Orbit Predictor; version: %s", gsId, serviceVersion));
        from("timer://heartbeat?fixedRate=true&period=" + heartBeatInterval)
                .bean(card, "touch")
                .process(preparator)
                .to(StandardEndpoints.MONITORING);

        // @formatter:on
    }

    public static void main(String[] args) throws Exception {
        LOG.info("Starting {}", OrbitPredictor.class.getSimpleName());
        if (System.getProperty("orekit.data.path") == null) {
            LOG.warn("System property \"orekit.data.path\" not set; using default value\"{}\"",
                    DEFAULT_OREKIT_DATA_PATH);
            System.setProperty("orekit.data.path", DEFAULT_OREKIT_DATA_PATH);
        } else {
            LOG.info("System property \"orekit.data.path\" is set to \"{}\"", System.getProperty("orekit.data.path"));
        }
        new Main().run(args);
    }
}