/** 
 *
 */
package eu.estcube.predictor;

import java.util.List;

import org.apache.camel.Handler;
import org.hbird.exchange.core.EntityInstance;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class Predict {

//    private static final Logger LOG = LoggerFactory.getLogger(Predict.class);

    @Value("${gs.id}")
    private String groundStationId;

    @Value("${satellite.id}")
    private String satelliteId;

    @Value("${service.id}")
    private String serviceId;

    /** Prediction period in hours */
    @Value("${prediction.period:12}")
    private int predictionPeriod;

    @Handler
    public List<EntityInstance> handle() {
        throw new RuntimeException("eu.estcube.predictor is outdated and should not be used any more!");
        // DataAccess dao = new DataAccess(serviceId);
        // Catalogue catalogue = new Catalogue(serviceId);
        // Publish publisher = new Publish(serviceId);
        // List<EntityInstance> result = Collections.emptyList();
        //
        // try {
        // LOG.info("Starting orbit prediction for the satellite {} in ground station {}",
        // satelliteId,
        // groundStationId);
        //
        // KeplerianOrbitPredictor predictor = new KeplerianOrbitPredictor();
        // TleOrbitalPredictor tlePredictor = new TleOrbitalPredictor(dao,
        // catalogue, publisher, predictor);
        //
        // GroundStation gs = (GroundStation) dao.resolve(groundStationId);
        //
        // long now = System.currentTimeMillis();
        //
        // DateTime start = new DateTime(now,
        // DateTimeZone.UTC).withMinuteOfHour(0).withSecondOfMinute(0)
        // .withMillisOfSecond(0);
        // DateTime end = start.plusHours(predictionPeriod);
        // LOG.info(
        // "Prediction period {}h; from {} to {}",
        // new Object[] { predictionPeriod,
        // start.toString(Dates.ISO_8601_DATE_FORMATTER),
        // end.toString(Dates.ISO_8601_DATE_FORMATTER) });
        //
        // TlePropagationRequest request = new TlePropagationRequest(serviceId,
        // satelliteId, gs.getName(),
        // start.getMillis(), end.getMillis());
        // request.setArgumentValue(StandardArguments.PUBLISH, false);
        //
        // try {
        // result = tlePredictor.predictOrbit(request);
        //
        // for (EntityInstance e : result) {
        // if (e instanceof LocationContactEvent) {
        // // XXXX - 03.05.2013; kimmell - replacing timestamp with
        // // the
        // // orbit number to avoid duplicated events in archive
        // //
        // // Orbital state timestamp is set to state calculation
        // // time
        // // already, thus avoiding duplicates
        // e.setTimestamp(((LocationContactEvent) e).getOrbitNumber());
        // }
        // }
        //
        // } catch (Exception e) {
        // LOG.error("Failed to predict orbit ", e);
        // }
        //
        // LOG.info("Completed orbit prediction for the satellite {} in ground station {}; returning {} entries",
        // new Object[] { satelliteId, groundStationId, result.size() });
        // } catch (Exception e) {
        // LOG.error("Filed to predict orbit for the sattelite {} in the ground station {}",
        // satelliteId,
        // groundStationId);
        // } finally {
        // // XXX - 03.05.2013; kimmell - have to manually destroy them
        // // otherwise camel context and threads will pile up =|
        // HbirdApiHelper.dispose(dao);
        // HbirdApiHelper.dispose(catalogue);
        // HbirdApiHelper.dispose(publisher);
        // }
        // return result;
    }
}
