package eu.estcube.webcamera;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.hbird.exchange.constants.StandardArguments;
import org.hbird.exchange.util.Dates;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Adds ground station name and timestamp to image.
 */
@Component
public class ImageTimestamper implements Processor {

    /** Label offset from top and left on the image. */
    public static final int LABEL_OFFSET = 12;

    /** Text offset on the label from each direction. */
    public static final int TEXT_OFFSET = 3;

    /** Image type prefix. */
    public static final String TYPE_PREFIX = "image/";

    @Value("${gs.id}")
    protected String groundStationId;

    @Value("${output.format}")
    protected String format;

    /**
     * @{inheritDoc .
     */
    @Override
    public void process(Exchange ex) throws Exception {
        InputStream is = ex.getIn().getBody(InputStream.class);

        BufferedImage image;

        try {
            // read image from the input stream
            // IOExcpetion is thrown in case it fails
            image = ImageIO.read(is);
        } finally {
            // close the input stream in any case
            is.close();
        }

        Graphics2D graphics = image.createGraphics();
        Font font = new Font("ARIAL", Font.PLAIN, 18);
        graphics.setFont(font);

        long now = System.currentTimeMillis();
        String dateString = Dates.toDefaultDateFormat(now);

        FontMetrics fm = graphics.getFontMetrics();
        String stamp = groundStationId + " - " + dateString;
        Rectangle2D rect = fm.getStringBounds(stamp, graphics);
        graphics.setColor(Color.black);
        graphics.fillRect(LABEL_OFFSET, LABEL_OFFSET, (int) rect.getWidth() + TEXT_OFFSET * 2, (int) rect.getHeight()
                + TEXT_OFFSET * 2);
        graphics.setColor(Color.white);
        graphics.drawString(stamp, LABEL_OFFSET + TEXT_OFFSET, LABEL_OFFSET + TEXT_OFFSET + fm.getAscent());
        image.flush();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] data;

        try {
            // write image to byte array output stream
            // IOException is thrown in case it fails
            ImageIO.write(image, format, baos);
            // get bytes from byte array output stream
            // IOException is thrown in case it fails
            data = baos.toByteArray();
        } finally {
            // close the byte array output stream in any case
            baos.close();
        }

        // dispose the graphic object to release system resources
        graphics.dispose();

        ex.getOut().setHeader(StandardArguments.TIMESTAMP, now);
        ex.getOut().setHeader(StandardArguments.TYPE, TYPE_PREFIX + format);
        ex.getOut().setBody(data, byte[].class);
    }
}
