package eu.estcube.calibration.utils;

import java.util.HashMap;
import java.util.Map;

import eu.estcube.calibration.domain.CalibrationUnit;
import eu.estcube.calibration.domain.InfoContainer;

public class InitCalibrationUnits {

    public static Map<String, CalibrationUnit> getCalibrationUnits() {

        Map<String, CalibrationUnit> map = new HashMap<String, CalibrationUnit>();

        Map<String, InfoContainer> calibrationInfo = InitCalibrators.getCalibrators();

        for (InfoContainer info : calibrationInfo.values()) {

            CalibrationUnit cu = new CalibrationUnit(info);
            map.put(cu.getId(), cu);

        }

        return map;

    }

}
