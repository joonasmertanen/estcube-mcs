package eu.estcube.calibration.utils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import eu.estcube.calibration.domain.InfoContainer;
import eu.estcube.calibration.xmlparser.Parser;

public class InitCalibrators {

    public static Map<String, InfoContainer> getCalibrators() {
        Map<String, InfoContainer> map = new HashMap<String, InfoContainer>();

        File[] files = FileManager.getFiles();

        for (int i = 0; i < files.length; i++) {

            File file = files[i];

            map.putAll(Parser.getCalibrations(file));

        }

        return map;
    }
}
