package eu.estcube.gs.tnc.processor;

import java.io.File;
import java.io.IOException;

import org.apache.camel.Body;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.estcube.common.ByteUtil;

/**
 * Reads file content to byte array.
 * If file contains HEX dump converts it to byte array otherwise uses file content as is.
 */
@Component
public class FileInputToBinary {

    /** Logger */
    private static final Logger LOG = LoggerFactory.getLogger(FileInputToBinary.class);

    /**
     * Read file content to byte array.
     * 
     * @param file file to read
     * @return byte array with file content
     * @throws IOException
     */
    public byte[] fileContentToBinary(@Body File file) throws IOException {
        byte[] bytes;
        try {
            String content = FileUtils.readFileToString(file, Charsets.US_ASCII);
            bytes = ByteUtil.toBytesFromHexString(content);
        } catch (Exception iae) {
            bytes = FileUtils.readFileToByteArray(file);
        }
        LOG.trace("Got bytes: {}", ByteUtil.toHexString(bytes, 20));
        return bytes;
    }
}
