/** 
 *
 */
package eu.estcube.gs.tnc.processor;

import org.apache.camel.Handler;
import org.hbird.exchange.groundstation.Track;
import org.hbird.exchange.navigation.LocationContactEvent;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class LocationContacEventExtractor {

    @Handler
    public LocationContactEvent extract(Track track) {
        return track.getLocationContactEvent();
    }
}
