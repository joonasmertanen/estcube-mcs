package eu.estcube.codec.radiobeacon.parsers;

import org.hbird.exchange.core.Label;
import org.hbird.exchange.core.Parameter;

public class RadioBeaconParametersNormalMode {

    public static final String ENTITY_ID = "/ESTCUBE/Satellites/ESTCube-1/beacon";

    public static final Label RADIO_BEACON = new Label(null, "raw");
    public static final Label OPERATING_MODE = new Label(null, "operating.mode");
    public static final Parameter TIMESTAMP = new Parameter(null, "timestamp");
    public static final Parameter MAIN_BUS_VOLTAGE = new Parameter(null, "main.bus.voltage");
    public static final Parameter AVERAGE_POWER_BALANCE = new Parameter(null, "average.power.balance");
    public static final Parameter BATTERY_A_VOLTAGE = new Parameter(null, "battery.A.voltage");
    public static final Parameter BATTERY_B_VOLTAGE = new Parameter(null, "battery.B.voltage");
    public static final Parameter BATTERY_A_TEMPERATURE = new Parameter(null, "battery.A.temperature");
    public static final Parameter SPIN_RATE_Z = new Parameter(null, "spin.rate.z");
    public static final Parameter RECEIVED_SIGNAL_STRENGTH = new Parameter(null, "received.signal.strength");
    public static final Parameter SATELLITE_MISSION_PHASE = new Parameter(null, "satellite.mission.phase");
    public static final Parameter TIME_SINCE_LAST_RESET_CDHS = new Parameter(null, "time.since.last.reset.CDHS");
    public static final Parameter TIME_SINCE_LAST_RESET_COM = new Parameter(null, "time.since.last.reset.COM");
    public static final Parameter TIME_SINCE_LAST_RESET_EPS = new Parameter(null, "time.since.last.reset.EPS");
    public static final Parameter TETHER_CURRENT = new Parameter(null, "tether.current");
    public static final Parameter TIME_SINCE_LAST_ERROR_ADCS = new Parameter(null, "time.since.last.error.ADCS");
    public static final Parameter TIME_SINCE_LAST_ERROR_CDHS = new Parameter(null, "time.since.last.error.CDHS");
    public static final Parameter TIME_SINCE_LAST_ERROR_COM = new Parameter(null, "time.since.last.error.COM");
    public static final Parameter TIME_SINCE_LAST_ERROR_EPS = new Parameter(null, "time.since.last.error.EPS");
    public static final Parameter CDHS_SYSTEM_STATUS_CODE = new Parameter(null, "CDHS.system.status.code");
    public static final Parameter CDHS_SYSTEM_STATUS_VALUE = new Parameter(null, "CDHS.system.status.value");
    public static final Parameter EPS_STATUS_CODE = new Parameter(null, "EPS.status.code");
    public static final Parameter ADCS_SYSTEM_STATUS_CODE = new Parameter(null, "ADCS.system.status.code");
    public static final Parameter ADCS_SYSTEM_STATUS_VALUE = new Parameter(null, "ADCS.system.status.value");
    public static final Parameter COM_SYSTEM_STATUS_CODE = new Parameter(null, "COM.system.status.code");
    public static final Parameter COM_SYSTEM_STATUS_VALUE = new Parameter(null, "COM.system.status.value");

    static {
        RADIO_BEACON.setDescription("Raw radiobeacon string");
        OPERATING_MODE.setDescription("Operating mode (E=normal or T=safe)");
        TIMESTAMP.setDescription("Timestamp");
        TIMESTAMP.setUnit("sec");
        MAIN_BUS_VOLTAGE.setDescription("Main bus voltage");
        MAIN_BUS_VOLTAGE.setUnit("V");
        AVERAGE_POWER_BALANCE.setDescription("Average power balance");
        AVERAGE_POWER_BALANCE.setUnit("W");
        BATTERY_A_VOLTAGE.setDescription("Battery A voltage");
        BATTERY_A_VOLTAGE.setUnit("V");
        BATTERY_B_VOLTAGE.setDescription("Battery B voltage");
        BATTERY_B_VOLTAGE.setUnit("V");
        BATTERY_A_TEMPERATURE.setDescription("Battery A temperature");
        BATTERY_A_TEMPERATURE.setUnit("C");
        SPIN_RATE_Z.setDescription("Spin rate Z");
        SPIN_RATE_Z.setUnit("deg/s");
        RECEIVED_SIGNAL_STRENGTH.setDescription("Received signal strength");
        RECEIVED_SIGNAL_STRENGTH.setUnit("3dBm");
        SATELLITE_MISSION_PHASE
                .setDescription("0=Detumbling, 1=Nadir pointing, 2=Tether deployment, 3=E-sail force measurement");
        SATELLITE_MISSION_PHASE.setUnit("");
        TIME_SINCE_LAST_RESET_CDHS.setDescription("Time since last reset CDHS");
        TIME_SINCE_LAST_RESET_CDHS.setUnit("hour(s)");
        TIME_SINCE_LAST_RESET_COM.setDescription("Time since last reset COM");
        TIME_SINCE_LAST_RESET_COM.setUnit("hour(s)");
        TIME_SINCE_LAST_RESET_EPS.setDescription("Time since last reset EPS");
        TIME_SINCE_LAST_RESET_EPS.setUnit("hour(s)");
        TETHER_CURRENT.setDescription("Tether current");
        TETHER_CURRENT.setUnit("mA");
        TIME_SINCE_LAST_ERROR_ADCS.setDescription("Time since last error ADCS");
        TIME_SINCE_LAST_ERROR_ADCS.setUnit("hour(s)");
        TIME_SINCE_LAST_ERROR_CDHS.setDescription("Time since last error CDHS");
        TIME_SINCE_LAST_ERROR_CDHS.setUnit("hour(s)");
        TIME_SINCE_LAST_ERROR_COM.setDescription("Time since last error COM");
        TIME_SINCE_LAST_ERROR_COM.setUnit("hour(s)");
        TIME_SINCE_LAST_ERROR_EPS.setDescription("Time since last error EPS");
        TIME_SINCE_LAST_ERROR_EPS.setUnit("hour(s)");
        CDHS_SYSTEM_STATUS_CODE.setDescription("CDHS system status code");
        CDHS_SYSTEM_STATUS_CODE.setUnit("");
        CDHS_SYSTEM_STATUS_VALUE.setDescription("CDHS system status value");
        CDHS_SYSTEM_STATUS_VALUE.setUnit("");
        EPS_STATUS_CODE.setDescription("EPS status code");
        EPS_STATUS_CODE.setUnit("");
        ADCS_SYSTEM_STATUS_CODE.setDescription("ADCS system status code");
        ADCS_SYSTEM_STATUS_CODE.setUnit("");
        ADCS_SYSTEM_STATUS_VALUE.setDescription("ADCS system status value");
        ADCS_SYSTEM_STATUS_VALUE.setUnit("");
        COM_SYSTEM_STATUS_CODE.setDescription("COM system status code");
        COM_SYSTEM_STATUS_CODE.setUnit("");
        COM_SYSTEM_STATUS_VALUE.setDescription("COM system status value");
        COM_SYSTEM_STATUS_VALUE.setUnit("");
    }
}
