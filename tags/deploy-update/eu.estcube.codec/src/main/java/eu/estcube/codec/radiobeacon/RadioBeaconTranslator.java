package eu.estcube.codec.radiobeacon;

import java.util.HashMap;
import java.util.regex.Pattern;

import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.core.EntityInstance;

import eu.estcube.codec.radiobeacon.exceptions.InvalidRadioBeaconException;
import eu.estcube.codec.radiobeacon.parser.RadionBeaconMessageParserTimestamp;
import eu.estcube.codec.radiobeacon.parsers.RadioBeaconParsers;
import eu.estcube.codec.radiobeacon.parsers.RadioBeaconParsersNormalMode;
import eu.estcube.codec.radiobeacon.parsers.RadioBeaconParsersSafeMode;
import eu.estcube.common.Constants;

public class RadioBeaconTranslator {

    private static final String MODE_NORMAL = "Normal mode";
    private static final String MODE_SAFE = "Safe mode";
    private static final String MODE_UNDEFINED = "Undefined mode";

    public String determineRadioBeaconMode(String message) {
        if (message.length() < 7) {
            return MODE_UNDEFINED;
        }
        String messageModeID = message.substring(6, 7);
        if (messageModeID.equals("E")) {
            return MODE_NORMAL;
        } else if (messageModeID.equals("T")) {
            return MODE_SAFE;
        } else {
            String messageEndCharacter = message.substring(message.length() - 2, message.length());
            if (messageEndCharacter.equals("KN")) {
                return MODE_SAFE;
            } else {
                messageEndCharacter = messageEndCharacter.substring(messageEndCharacter.length() - 1);
                if (messageEndCharacter.equals("K")) {
                    return MODE_NORMAL;
                }
                return MODE_UNDEFINED;
            }
        }
    }

    public String reformBeaconMessage(String message) {
        String newMessage = message.replaceAll("\\s", "");
        newMessage = newMessage.toUpperCase();
        return newMessage;
    }

    public boolean checkRadioBeaconBasis(String message) {
        String messageBasis = message.substring(0, 6);
        if (messageBasis.equals("ES5E/S")) {
            return true;
        }

        return false;
    }

    public boolean checkBeaconLength(String message) {
        int messageLength = message.length();
        String mode = determineRadioBeaconMode(message);

        if ((mode == MODE_NORMAL && messageLength == Constants.NORMAL_BEACON_MESSAGE_LENGTH)
                || (mode == MODE_SAFE && messageLength == Constants.SAFE_BEACON_MESSAGE_LENGTH)) {
            return true;
        }
        return false;
    }

    public boolean checkBeaconValidity(String message) {
        if ((determineRadioBeaconMode(message).equals(MODE_NORMAL) || determineRadioBeaconMode(message).equals(
                MODE_SAFE))
                && checkRadioBeaconBasis(message)) {
            return true;
        }
        return false;
    }

    public boolean checkASCIIMessage(String message) {
        if (Pattern.matches("[TWUSH56MZNABCDEF#KN /]*", message)) {
            return true;
        }
        return false;
    }

    public boolean checkBeaconMessage(String message) {
        if (checkBeaconLength(message) && checkASCIIMessage(message)) {
            if (checkRadioBeaconBasis(message)) {
                return true;
            }
            return false;
        }
        return false;
    }

    protected RadioBeaconParsers getParsers(String message, IdBuilder idBuilder) throws Exception {

        String beaconMode = determineRadioBeaconMode(message);
        if (beaconMode == MODE_NORMAL) {
            return new RadioBeaconParsersNormalMode(idBuilder);
        } else if (beaconMode == MODE_SAFE) {
            return new RadioBeaconParsersSafeMode(idBuilder);
        } else {
            throw new InvalidRadioBeaconException("Invalid beacon mode");
        }

    }

    public HashMap<String, EntityInstance> toParameters(String message, Long timestamp, String issuedBy,
            IdBuilder idBuilder) throws Exception {
        message = reformBeaconMessage(message);
        RadioBeaconParsers parsers = getParsers(message, idBuilder);

        if (checkBeaconMessage(message) == false) {
            return new HashMap<String, EntityInstance>();
        }

        if (timestamp == null) {
            RadionBeaconMessageParserTimestamp parser = parsers.getTimestampParser();
            if (parser != null) {
                timestamp = parser.getResult(message);
            }
            if (timestamp == null) {
                timestamp = (long) 0;
            }
        }

        return parsers.parse(message, timestamp, issuedBy);
    }

}
