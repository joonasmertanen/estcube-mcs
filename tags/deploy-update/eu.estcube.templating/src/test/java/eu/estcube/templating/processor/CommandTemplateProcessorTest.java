package eu.estcube.templating.processor;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.hbird.business.groundstation.hamlib.HamlibNativeCommand;
import org.hbird.exchange.core.CommandBase;
import org.junit.Before;
import org.junit.Test;

import eu.estcube.templating.placeholders.DoublePlaceholder;
import eu.estcube.templating.placeholders.IntegerPlaceholder;
import eu.estcube.templating.placeholders.LongPlaceholder;
import eu.estcube.templating.placeholders.PlaceholderException;
import eu.estcube.templating.placeholders.PlaceholderMap;
import eu.estcube.templating.placeholders.StringPlaceholder;
import eu.estcube.templating.template.CommandTemplate;
import eu.estcube.templating.template.provider.CommandTemplateProvider;

public class CommandTemplateProcessorTest {

    private static final String ISSUED_BY = "test";

    private CommandTemplate cmdTemplate;

    private PlaceholderMap values;

    private CommandTemplateProcessor processor;

    @Before
    public void setUp() throws Exception {
        cmdTemplate = new CommandTemplate("GROUNDSTATION/ES5EC/ROTATOR${rot_id}", "P ${azimuth} ${elevation}");

        values = new PlaceholderMap();

        processor = new CommandTemplateProcessor();
    }

    @Test
    public void testProcess() throws PlaceholderException {

        values.put(StringPlaceholder.ISSUED_BY, ISSUED_BY);
        values.put(IntegerPlaceholder.ROTATOR_ID, 10);
        values.put(DoublePlaceholder.ROTATOR_AZIMUTH, 10.1D);
        values.put(DoublePlaceholder.ROTATOR_ELEVATION, 20.2D);
        values.put(LongPlaceholder.COMMAND_TIMESTAMP, 50L);

        CommandBase answer1 = processor.process(cmdTemplate, values);
        CommandBase answer2 = processor.process(new CommandTemplateProvider(cmdTemplate), values);

        assertTrue(answer1 instanceof HamlibNativeCommand);
        assertTrue(answer2 instanceof HamlibNativeCommand);

        assertThat(answer1.getDestination(), is("GROUNDSTATION/ES5EC/ROTATOR10"));
        assertThat(answer1.getExecutionTime(), is(50L));
        assertThat(((HamlibNativeCommand) answer1).getCommandToExecute(), is("P 10.1 20.2"));

        assertThat(answer2.getDestination(), is("GROUNDSTATION/ES5EC/ROTATOR10"));
        assertThat(answer2.getExecutionTime(), is(50L));
        assertThat(((HamlibNativeCommand) answer2).getCommandToExecute(), is("P 10.1 20.2"));

    }

    @Test(expected = PlaceholderException.class)
    public void testProcessFail1() throws PlaceholderException {

        values.put(StringPlaceholder.ISSUED_BY, ISSUED_BY);
        values.put(IntegerPlaceholder.ROTATOR_ID, 10);
        values.put(DoublePlaceholder.ROTATOR_AZIMUTH, 10D);
        values.put(DoublePlaceholder.ROTATOR_ELEVATION, 20D);

        processor.process(cmdTemplate, values);
    }

    @Test(expected = PlaceholderException.class)
    public void testProcessFail2() throws PlaceholderException {

        values.put(StringPlaceholder.ISSUED_BY, ISSUED_BY);
        values.put(IntegerPlaceholder.ROTATOR_ID, 10);
        values.put(DoublePlaceholder.ROTATOR_AZIMUTH, 10D);
        values.put(LongPlaceholder.COMMAND_TIMESTAMP, 50L);

        processor.process(cmdTemplate, values);
    }

    @Test(expected = PlaceholderException.class)
    public void testProcessFail3() throws PlaceholderException {

        values.put(StringPlaceholder.ISSUED_BY, ISSUED_BY);
        values.put(IntegerPlaceholder.ROTATOR_ID, 10);
        values.put(DoublePlaceholder.ROTATOR_ELEVATION, 20D);
        values.put(LongPlaceholder.COMMAND_TIMESTAMP, 50L);

        processor.process(cmdTemplate, values);
    }

    @Test(expected = PlaceholderException.class)
    public void testProcessFail4() throws PlaceholderException {

        values.put(StringPlaceholder.ISSUED_BY, ISSUED_BY);
        values.put(DoublePlaceholder.ROTATOR_AZIMUTH, 10D);
        values.put(DoublePlaceholder.ROTATOR_ELEVATION, 20D);
        values.put(LongPlaceholder.COMMAND_TIMESTAMP, 50L);

        processor.process(cmdTemplate, values);
    }

    @Test(expected = PlaceholderException.class)
    public void testProcessFail5() throws PlaceholderException {

        values.put(IntegerPlaceholder.ROTATOR_ID, 10);
        values.put(DoublePlaceholder.ROTATOR_AZIMUTH, 10D);
        values.put(DoublePlaceholder.ROTATOR_ELEVATION, 20D);
        values.put(LongPlaceholder.COMMAND_TIMESTAMP, 50L);

        processor.process(cmdTemplate, values);
    }

}
