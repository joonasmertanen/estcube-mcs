package eu.estcube.templating.template;



/**
 * A bean for commands.
 * 
 * @author deiwin
 */
public class CommandTemplate implements Template {

    private String targetID;
    private String value;

    public CommandTemplate(String targetID, String value) throws Exception {

        this.targetID = targetID;
        this.value = value;
    }

    public String getTargetId() {
        return targetID;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Command [targetID=" + targetID + ", value=" + value + "]";
    }

}
