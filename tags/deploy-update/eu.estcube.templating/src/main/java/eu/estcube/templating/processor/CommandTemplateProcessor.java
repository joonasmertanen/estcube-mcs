package eu.estcube.templating.processor;

import org.hbird.business.groundstation.hamlib.HamlibNativeCommand;
import org.hbird.exchange.core.CommandBase;

import eu.estcube.templating.placeholders.LongPlaceholder;
import eu.estcube.templating.placeholders.PlaceholderException;
import eu.estcube.templating.placeholders.PlaceholderMap;
import eu.estcube.templating.placeholders.StringPlaceholder;
import eu.estcube.templating.template.CommandTemplate;
import eu.estcube.templating.template.provider.CommandTemplateProvider;

public class CommandTemplateProcessor extends TemplateProcessor<CommandTemplate, CommandBase, CommandTemplateProvider> {

    @Override
    public CommandBase process(CommandTemplate template, PlaceholderMap values) throws PlaceholderException {
        String dest = processString(template.getTargetId(), values);
        String command = processString(template.getValue(), values);
        String issuedBy = values.get(StringPlaceholder.ISSUED_BY);
        long execTime = values.get(LongPlaceholder.COMMAND_TIMESTAMP);
        HamlibNativeCommand ncmd = new HamlibNativeCommand(command, execTime, null,
                HamlibNativeCommand.STAGE_TRACKING);
        ncmd.setDestination(dest);
        ncmd.setIssuedBy(issuedBy);
        ncmd.setExecutionTime(execTime);
        return ncmd;
    }
}
