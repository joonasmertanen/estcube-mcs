package eu.estcube.templating.processor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hbird.exchange.core.CommandBase;
import org.hbird.exchange.navigation.PointingData;

import eu.estcube.templating.beans.Contact;
import eu.estcube.templating.placeholders.DoublePlaceholder;
import eu.estcube.templating.placeholders.LongPlaceholder;
import eu.estcube.templating.placeholders.PlaceholderException;
import eu.estcube.templating.placeholders.PlaceholderMap;
import eu.estcube.templating.template.CommandTemplate;
import eu.estcube.templating.template.ContactTemplate;
import eu.estcube.templating.template.provider.ContactTemplateProvider;

/**
 * Template processor for the entire contact.
 * 
 * @author deiwin
 */
public class ContactTemplateProcessor extends TemplateProcessor<ContactTemplate, Contact, ContactTemplateProvider> {

    private final CommandTemplateProcessor cmdProcessor = new CommandTemplateProcessor();

    /**
     * Same as {@link #process(ContactTemplate, PlaceholderMap, List)}, but uses
     * a template provider.
     */
    public Contact process(ContactTemplateProvider template, PlaceholderMap placeholderReplacements,
            List<PointingData> contactDataList) throws PlaceholderException {
        return process(template.getTemplate(), placeholderReplacements, contactDataList);
    }

    /**
     * Fills the contact template with data to form the Contact object.
     * 
     * @param template The contact template.
     * @param placeholderReplacements Values to replace the placeholders with.
     *        The {@code LongPlaceholder.COMMAND_HEADER_TIMESTAMP} and
     *        {@code LongPlaceholder.COMMAND_TAIL_TIMESTAMP} are used to fill
     *        the command timestamp for the header and tail commands
     *        accordingly. The body commands get their timestamp from the
     *        contact data.
     * @param contactDataList Contact data list used to fill the body command
     *        templates.
     * @return The contact object
     * @throws PlaceholderException If a required placeholder value is not set.
     */
    public Contact process(ContactTemplate template, PlaceholderMap placeholderReplacements,
            List<PointingData> contactDataList)
            throws PlaceholderException {
        placeholderReplacements.put(LongPlaceholder.COMMAND_TIMESTAMP,
                placeholderReplacements.get(LongPlaceholder.COMMAND_HEADER_TIMESTAMP));
        Collection<CommandBase> headerCmds = processCmdTemplates(template.getHeaderCommands(), placeholderReplacements);

        // Process tail templates before body ones, because body template
        // processing changes some of the placeholder replacement values in the
        // placeholderReplacements map
        placeholderReplacements.put(LongPlaceholder.COMMAND_TIMESTAMP,
                placeholderReplacements.get(LongPlaceholder.COMMAND_TAIL_TIMESTAMP));
        Collection<CommandBase> tailCmds = processCmdTemplates(template.getTailCommands(), placeholderReplacements);

        Collection<CommandBase> bodyCmds = processBodyCmdTemplates(template.getBodyCommands(), placeholderReplacements,
                contactDataList);

        return new Contact(headerCmds, bodyCmds, tailCmds);
    }

    /**
     * Process the body templates for each of the contact data elements
     * 
     * @param bodyCmdTemplates Templates for the body commands.
     * @param placeholderValues Values to replace the placeholders with. Changes
     *        some of these values in the map. If this is not acceptable a clone
     *        should be passed instead.
     * @param contactDataList Contact data list used to fill the templates.
     * @return Processed commands. A list of NativeCommand objects.
     * @throws PlaceholderException If a required placeholder value is not set.
     */
    private Collection<CommandBase> processBodyCmdTemplates(Collection<CommandTemplate> bodyCmdTemplates,
            PlaceholderMap placeholderValues, List<PointingData> contactDataList) throws PlaceholderException {
        Collection<CommandBase> result = new ArrayList<CommandBase>();

        for (PointingData cData : contactDataList) {
            // Update placeholder replacement values
            placeholderValues.put(LongPlaceholder.COMMAND_TIMESTAMP, cData.getTimestamp());
            placeholderValues.put(DoublePlaceholder.ROTATOR_AZIMUTH, cData.getAzimuth());
            placeholderValues.put(DoublePlaceholder.ROTATOR_ELEVATION, cData.getElevation());

            result.addAll(processCmdTemplates(bodyCmdTemplates, placeholderValues));
        }

        return result;
    }

    /**
     * Iterate over the command templates and process every one of them
     * 
     * @param cmdTemplates The command templates to process.
     * @param values The values to use for processing.
     * @return A list of processed objects.
     * @throws PlaceholderException If a required placeholder value is not set.
     */
    private Collection<CommandBase> processCmdTemplates(Collection<CommandTemplate> cmdTemplates, PlaceholderMap values)
            throws PlaceholderException {
        Collection<CommandBase> cmds = new ArrayList<CommandBase>();
        for (CommandTemplate cmdTemplate : cmdTemplates) {
            cmds.add(cmdProcessor.process(cmdTemplate, values));
        }
        return cmds;
    }

    /**
     * Implementation without contact data. Doesn't process any body commands.
     * To include contact data use
     * {@link #process(ContactTemplate, PlaceholderMap, List)}
     * 
     * @see eu.estcube.templating.processor.TemplateProcessor#process(eu.estcube.templating.template.Template,
     *      eu.estcube.templating.placeholders.PlaceholderMap)
     */
    @Override
    public Contact process(ContactTemplate template, PlaceholderMap placeholderReplacements)
            throws PlaceholderException {
        return process(template, placeholderReplacements, new ArrayList<PointingData>());
    }

}
