package eu.estcube.camel.serial;

import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultProducer;
import org.apache.mina.core.session.IoSession;

/**
 * 
 */
public class SerialProducer extends DefaultProducer {

    private final IoSession session;

    /**
     * Creates new SerialProducer.
     * 
     * @param endpoint
     */
    public SerialProducer(Endpoint endpoint, IoSession session) {
        super(endpoint);
        this.session = session;
    }

    /** @{inheritDoc . */
    @Override
    public void process(Exchange exchange) throws Exception {
        Object toSend = exchange.getIn().getBody();
        session.write(toSend);
    }
}
