package eu.estcube.contact;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.math.geometry.Rotation;
import org.apache.commons.math.geometry.Vector3D;
import org.apache.commons.math3.util.FastMath;
import org.junit.Before;
import org.junit.Test;
import org.orekit.attitudes.Attitude;
import org.orekit.bodies.CelestialBodyFactory;
import org.orekit.bodies.GeodeticPoint;
import org.orekit.bodies.OneAxisEllipsoid;
import org.orekit.errors.OrekitException;
import org.orekit.frames.Frame;
import org.orekit.frames.FramesFactory;
import org.orekit.frames.TopocentricFrame;
import org.orekit.orbits.CartesianOrbit;
import org.orekit.orbits.Orbit;
import org.orekit.propagation.SpacecraftState;
import org.orekit.time.AbsoluteDate;
import org.orekit.time.TimeScalesFactory;
import org.orekit.tle.TLE;
import org.orekit.utils.PVCoordinates;
import org.orekit.utils.PVCoordinatesProvider;

public class ContactCalculationTest {
	
	public static final double MU = 3.986004415e+14;                                    // A central attraction coefficient
	TLE compass1;
	private ContactCalculation calc;
	private TopocentricFrame locationOnEarth;
	private Frame inertialFrame;	//an inertial frame.
	
	@Before
	public void initialize() throws OrekitException{
		System.setProperty("orekit.data.path", "src/main/resources/orekit-data.zip");
		GeodeticPoint gsLocation = new GeodeticPoint(Math.toRadians(58.3000D), Math.toRadians(26.7330D), 59.0D);
		compass1 = new TLE("1 32787U 08021E   13105.71801885  .00002263  00000-0  26947-3 0  5963",
				"2 32787  97.7577 165.8927 0016016 145.4217   1.1445 14.85219091268693");
		calc = new ContactCalculation();
		inertialFrame = FramesFactory.getCIRF2000();
		locationOnEarth = new TopocentricFrame(calc.earth, gsLocation, "name");
	}
	//Kui satelliit nähtav on
	@Test
	public void testGeoStatsSat() throws OrekitException{
		
		//Geostationary satellite LES 9 TLE
		TLE tle = new TLE("1 08747U 76023B   13091.27084731 -.00000094  00000-0  00000+0 0  9050",
				"2 08747  12.8267 112.8241 0022435  16.8836  53.2651  1.00276232 81296");
		//start date
        AbsoluteDate startDate = new AbsoluteDate(2013, 4, 2, 6, 16, 00, TimeScalesFactory.getUTC());
        //end date 
        AbsoluteDate endDate = new AbsoluteDate(2013, 4, 3, 13, 35, 00, TimeScalesFactory.getUTC());
        // Geolocation of ground station ES5EC - on roof top of the building in TĆ¤he 4, Tartu, Estonia
        GeodeticPoint gsLocation = new GeodeticPoint(Math.toRadians(58.3000D), Math.toRadians(26.7330D), 59.0D);
		double elevation = 0.1;
		String gsName = "GROUNDSTATION";
		String satName = "SATELLITE";
		double uplinkFreq = 100;
		double downlinkFreq = 100;
		double gsLoss = 0;
		double satLoss = 0;
		
		
		List<ContactEventData> contactEvents = calc.calculateContact(tle, startDate, endDate, gsLocation, elevation, gsName, satName, uplinkFreq, downlinkFreq, gsLoss, satLoss);
		//Because it's geostationary satellite no contacts are available
		Assert.assertEquals(0, contactEvents.size());
	}
	@Test
	public void testGeoStatsSatInRange() throws OrekitException{
		AbsoluteDate startTime = new AbsoluteDate(2013, 4, 27, 6, 16, 00, TimeScalesFactory.getUTC());
		AbsoluteDate endTime = new AbsoluteDate(2013, 4, 27, 13, 35, 00, TimeScalesFactory.getUTC());
	    double elev = 23.75;    
	    double startDoppler = 0.1293;
	    double endDoppler = -0.1345;
	    double endSigLoss = 164.36;
	    double rangeAtEnd = 39179;
	    double endSigDelay = 130.68;
	    boolean inSunlight = true;
	    int orbitNum = 3005;
		//Geostationary satellite XTAR-EUR TLE
		TLE tle = new TLE("1 28542U 05005A   13116.02529027  .00000145  00000-0  00000+0 0  1856",
				"2 28542   0.0544  68.3067 0001704 308.1766 235.8122  1.00273357 30047");
		//start date
        AbsoluteDate startDate = new AbsoluteDate(2013, 4, 27, 6, 16, 00, TimeScalesFactory.getUTC());
        //end date 
        AbsoluteDate endDate = new AbsoluteDate(2013, 4, 27, 13, 35, 00, TimeScalesFactory.getUTC());
        // Geolocation of ground station ES5EC - on roof top of the building in TĆ¤he 4, Tartu, Estonia
        GeodeticPoint gsLocation = new GeodeticPoint(Math.toRadians(58.3000D), Math.toRadians(26.7330D), 59.0D);
		double elevation = 0.1;
		String gsName = "GROUNDSTATION";
		String satName = "SATELLITE";
		double uplinkFreq = 100;
		double downlinkFreq = 100;
		double gsLoss = 0;
		double satLoss = 0;
		
		
		List<ContactEventData> contactEvents = calc.calculateContact(tle, startDate, endDate, gsLocation, elevation, gsName, satName, uplinkFreq, downlinkFreq, gsLoss, satLoss);
		//Because it's geostationary satellite no contacts are available
		Assert.assertEquals(1, contactEvents.size());
		ContactEventData eventData = contactEvents.get(0);
		Assert.assertEquals(startTime, eventData.getStartTime());
		Assert.assertEquals(endTime, eventData.getEndTime());
		Assert.assertTrue((elev > eventData.getEndElevation() && elev - 0.5 < eventData.getEndElevation()) || (elev < eventData.getEndElevation() && elev + 0.5 > eventData.getEndElevation()) || elev == eventData.getEndElevation());
		//Tests that doppler doesn't differ more than 10 units
		Assert.assertTrue((startDoppler > eventData.getStartDopplerUp()*1000000 && startDoppler - 10 < eventData.getStartDopplerUp()*1000000) || (startDoppler < eventData.getStartDopplerUp()*1000000 && startDoppler + 10 > eventData.getStartDopplerUp()*1000000) || startDoppler == eventData.getStartDopplerUp()*1000000);
		Assert.assertTrue((endDoppler > eventData.getEndDopplerUp()*1000000 && endDoppler - 10 < eventData.getEndDopplerUp()*1000000) || (endDoppler < eventData.getEndDopplerUp()*1000000 && endDoppler + 10 > eventData.getEndDopplerUp()*1000000) || endDoppler == eventData.getEndDopplerUp()*1000000);
		//Tests that signal loss doesn't differ more than 0.5 units
		Assert.assertTrue((endSigLoss > eventData.getEndSigLossUp() && endSigLoss - 0.5 < eventData.getEndSigLossUp()) || (endSigLoss < eventData.getEndSigLossUp() && endSigLoss + 0.5 > eventData.getEndSigLossUp()) || endSigLoss == eventData.getEndSigLossUp());
		//Tests that ranges doesn't differ more than 20 units
		Assert.assertTrue((rangeAtEnd > eventData.getRangeAtEnd()/ 1000 && rangeAtEnd - 20 < eventData.getRangeAtEnd()/ 1000) || (rangeAtEnd < eventData.getRangeAtEnd()/ 1000 && rangeAtEnd + 20 > eventData.getRangeAtEnd()/ 1000) || rangeAtEnd == eventData.getRangeAtEnd()/ 1000);
		//Tests that signal delays doesn't differ more than 0.5 units
		Assert.assertTrue((endSigDelay > eventData.getEndSignalDelay()*1000 && endSigDelay - 0.5 < eventData.getEndSignalDelay()*1000) || (endSigDelay < eventData.getEndSignalDelay()*1000 && endSigDelay + 0.5 > eventData.getEndSignalDelay()*1000) || endSigDelay == eventData.getEndSignalDelay()*1000);
		Assert.assertEquals(inSunlight, eventData.isInSunlight());
		
		Assert.assertEquals(orbitNum, eventData.getOrbitNumber());
	}
	@Test
	public void testCubeSat() throws OrekitException{
		
		//Contact start time taken from GPREDICT 12.04.2013
		final AbsoluteDate gpredictStartDate = new AbsoluteDate(2013, 4, 12, 11, 22, 34, TimeScalesFactory.getUTC());
		//Contact end time taken from GPREDICT 12.04.2013
		final AbsoluteDate gpredictEndDate = new AbsoluteDate(2013, 4, 12, 11, 30, 47, TimeScalesFactory.getUTC());
		
		//Compass-1 12.04.2013
		TLE tle = new TLE("1 32787U 08021E   13101.18298337  .00003219  00000-0  38096-3 0  5928",
				"2 32787  97.7581 161.4743 0015690 159.3527 234.4021 14.85196188268020");
		//start date
        AbsoluteDate startDate = new AbsoluteDate(2013, 4, 12, 11, 16, 00, TimeScalesFactory.getUTC());
        //end date 
        AbsoluteDate endDate = new AbsoluteDate(2013, 4, 12, 11, 35, 00, TimeScalesFactory.getUTC());
     // Geolocation of ground station ES5EC - on roof top of the building in TĆ¤he 4, Tartu, Estonia
        GeodeticPoint gsLocation = new GeodeticPoint(Math.toRadians(58.3000D), Math.toRadians(26.7330D), 59.0D);
		double elevation = FastMath.toRadians(0.1);
		String gsName = "GROUNDSTATION";
		String satName = "SATELLITE";
		double uplinkFreq = 100;
		double downlinkFreq = 100;
		double gsLoss = 0;
		double satLoss = 0;
		
		List<ContactEventData> contactEvents = calc.calculateContact(tle, startDate, endDate, gsLocation, elevation, gsName, satName, uplinkFreq, downlinkFreq, gsLoss, satLoss);
		AbsoluteDate calculatedStartDate = contactEvents.get(0).getStartTime();
		AbsoluteDate calculatedEndDate = contactEvents.get(0).getEndTime();
		//check whether calculated start time is +- 30 seconds from gpredictStartDate
		Assert.assertTrue((gpredictStartDate.compareTo(calculatedStartDate) > 0 && gpredictStartDate.shiftedBy(-30).compareTo(calculatedStartDate) < 0 ) || (gpredictStartDate.compareTo(calculatedStartDate) < 0 && gpredictStartDate.shiftedBy(30).compareTo(calculatedStartDate) > 0) );
		
		//check whether calculated end time is +- 30 seconds from gpredictEndDate
		Assert.assertTrue((gpredictEndDate.compareTo(calculatedEndDate) > 0 && gpredictEndDate.shiftedBy(-30).compareTo(calculatedEndDate) < 0 ) || (gpredictEndDate.compareTo(calculatedEndDate) < 0 && gpredictEndDate.shiftedBy(30).compareTo(calculatedEndDate) > 0) );			
	}
	@Test
	public void testContactParams() throws OrekitException{
		
		//Data from OREKIT start time 11:22:34, end time 11:30:47, elevation 0.1 degrees
		double startAz = 6.18; 
		double endAz = 286.10; 
		double maxEl = 6.33;
		double startDoppler = 1483;
		double endDoppler = -1466;
		double startSigLoss = 141.57;
		double endSigLoss = 141.51;
		double rangeAtStart = 2875;
		double rangeAtEnd = 2856;
		double startSigDelay = 9.59;
		double endSigDelay = 9.53;
		boolean inSunlight = true;
		int orbitNum = 26820;
		
		//Compass-1 12.04.2013
		TLE tle = new TLE("1 32787U 08021E   13101.18298337  .00003219  00000-0  38096-3 0  5928",
				"2 32787  97.7581 161.4743 0015690 159.3527 234.4021 14.85196188268020");
		//start date
        AbsoluteDate startDate = new AbsoluteDate(2013, 4, 12, 11, 16, 00, TimeScalesFactory.getUTC());
        //end date 
        AbsoluteDate endDate = new AbsoluteDate(2013, 4, 12, 11, 35, 00, TimeScalesFactory.getUTC());
     // Geolocation of ground station ES5EC - on roof top of the building in TĆ¤he 4, Tartu, Estonia
        GeodeticPoint gsLocation = new GeodeticPoint(Math.toRadians(58.3000D), Math.toRadians(26.7330D), 59.0D);
		double elevation = FastMath.toRadians(0.1);
		String gsName = "GROUNDSTATION";
		String satName = "SATELLITE";
		double uplinkFreq = 100;
		double downlinkFreq = 100;
		double gsLoss = 0;
		double satLoss = 0;

		List<ContactEventData> contactEvents = calc.calculateContact(tle, startDate, endDate, gsLocation, elevation, gsName, satName, uplinkFreq, downlinkFreq, gsLoss, satLoss);
		ContactEventData eventData = contactEvents.get(0);
		//Tests if differences are maximally 0.5 degrees
		Assert.assertTrue((startAz > eventData.getStartAzimuth() && startAz - 0.5 < eventData.getStartAzimuth()) || (startAz < eventData.getStartAzimuth() && startAz + 0.5 > eventData.getStartAzimuth()) || startAz == eventData.getStartAzimuth());
		Assert.assertTrue((endAz > eventData.getEndAzimuth() && endAz - 0.5 < eventData.getEndAzimuth()) || (endAz < eventData.getEndAzimuth() && endAz + 0.5 > eventData.getEndAzimuth()) || endAz == eventData.getEndAzimuth());
		Assert.assertTrue((maxEl > eventData.getMaxElevation() && maxEl - 0.5 < eventData.getMaxElevation()) || (maxEl < eventData.getMaxElevation() && maxEl + 0.5 > eventData.getMaxElevation()) || maxEl == eventData.getMaxElevation());
		//Tests that doppler doesn't differ more than 10 units
		Assert.assertTrue((startDoppler > eventData.getStartDopplerUp()*1000000 && startDoppler - 10 < eventData.getStartDopplerUp()*1000000) || (startDoppler < eventData.getStartDopplerUp()*1000000 && startDoppler + 10 > eventData.getStartDopplerUp()*1000000) || startDoppler == eventData.getStartDopplerUp()*1000000);
		Assert.assertTrue((endDoppler > eventData.getEndDopplerUp()*1000000 && endDoppler - 10 < eventData.getEndDopplerUp()*1000000) || (endDoppler < eventData.getEndDopplerUp()*1000000 && endDoppler + 10 > eventData.getEndDopplerUp()*1000000) || endDoppler == eventData.getEndDopplerUp()*1000000);
		//Tests that signal loss doesn't differ more than 0.5 units
		Assert.assertTrue((startSigLoss > eventData.getStartSigLossUp() && startSigLoss - 0.5 < eventData.getStartSigLossUp()) || (startSigLoss < eventData.getStartSigLossUp() && startSigLoss + 0.5 > eventData.getStartSigLossUp()) || startSigLoss == eventData.getStartSigLossUp());
		Assert.assertTrue((endSigLoss > eventData.getEndSigLossUp() && endSigLoss - 0.5 < eventData.getEndSigLossUp()) || (endSigLoss < eventData.getEndSigLossUp() && endSigLoss + 0.5 > eventData.getEndSigLossUp()) || endSigLoss == eventData.getEndSigLossUp());
		//Tests that ranges doesn't differ more than 20 units
		Assert.assertTrue((rangeAtStart > eventData.getRangeAtStart() / 1000 && rangeAtStart - 20 < eventData.getRangeAtStart()/ 1000) || (rangeAtStart < eventData.getRangeAtStart()/ 1000 && rangeAtStart + 20 > eventData.getRangeAtStart()/ 1000) || rangeAtStart == eventData.getRangeAtStart()/ 1000);
		Assert.assertTrue((rangeAtEnd > eventData.getRangeAtEnd()/ 1000 && rangeAtEnd - 20 < eventData.getRangeAtEnd()/ 1000) || (rangeAtEnd < eventData.getRangeAtEnd()/ 1000 && rangeAtEnd + 20 > eventData.getRangeAtEnd()/ 1000) || rangeAtEnd == eventData.getRangeAtEnd()/ 1000);
		//Tests that signal delays doesn't differ more than 0.5 units
		Assert.assertTrue((startSigDelay > eventData.getStartSignalDelay()*1000 && startSigDelay - 0.5 < eventData.getStartSignalDelay()*1000) || (startSigDelay < eventData.getStartSignalDelay()*1000 && startSigDelay + 0.5 > eventData.getStartSignalDelay()*1000) || startSigDelay == eventData.getStartSignalDelay()*1000);
		Assert.assertTrue((endSigDelay > eventData.getEndSignalDelay()*1000 && endSigDelay - 0.5 < eventData.getEndSignalDelay()*1000) || (endSigDelay < eventData.getEndSignalDelay()*1000 && endSigDelay + 0.5 > eventData.getEndSignalDelay()*1000) || endSigDelay == eventData.getEndSignalDelay()*1000);
		Assert.assertEquals(inSunlight, eventData.isInSunlight());
		
		Assert.assertEquals(orbitNum, eventData.getOrbitNumber());
	}
	//Non-polar orbit satellite ISS
	@Test
	public void testISS() throws OrekitException{
		
		//Data from OREKIT start time 11:27:27, end time 11:35:13, elevation 0.1 degrees
		double startAz = 188.90; 
		double endAz = 96.66; 
		double maxEl = 7.41;
		double startDoppler = 1648;
		double endDoppler = -1653;
		double startSigLoss = 139.75;
		double endSigLoss = 139.76;
		double rangeAtStart = 2331;
		double rangeAtEnd = 2333;
		double startSigDelay = 7.77;
		double endSigDelay = 7.78;
		boolean inSunlight = true;
		int orbitNum = 82651;
		
		//ISS 25.04.2013
		TLE tle = new TLE("1 25544U 98067A   13114.90658572  .00010076  00000-0  16872-3 0  6743",
				"2 25544  51.6475  12.6650 0010173 178.7376 280.9898 15.52472726826439");
		//start date
        AbsoluteDate startDate = new AbsoluteDate(2013, 4, 25, 11, 25, 00, TimeScalesFactory.getUTC());
        //end date 
        AbsoluteDate endDate = new AbsoluteDate(2013, 4, 25, 11, 37, 00, TimeScalesFactory.getUTC());
     // Geolocation of ground station ES5EC - on roof top of the building in TĆ¤he 4, Tartu, Estonia
        GeodeticPoint gsLocation = new GeodeticPoint(Math.toRadians(58.3000D), Math.toRadians(26.7330D), 59.0D);
		double elevation = FastMath.toRadians(0.1);
		String gsName = "GROUNDSTATION";
		String satName = "SATELLITE";
		double uplinkFreq = 100;
		double downlinkFreq = 100;
		double gsLoss = 0;
		double satLoss = 0;

		List<ContactEventData> contactEvents = calc.calculateContact(tle, startDate, endDate, gsLocation, elevation, gsName, satName, uplinkFreq, downlinkFreq, gsLoss, satLoss);
		ContactEventData eventData = contactEvents.get(0);
		//Tests if differences are maximally 0.5 degrees
		Assert.assertTrue((startAz > eventData.getStartAzimuth() && startAz - 0.5 < eventData.getStartAzimuth()) || (startAz < eventData.getStartAzimuth() && startAz + 0.5 > eventData.getStartAzimuth()) || startAz == eventData.getStartAzimuth());
		Assert.assertTrue((endAz > eventData.getEndAzimuth() && endAz - 0.5 < eventData.getEndAzimuth()) || (endAz < eventData.getEndAzimuth() && endAz + 0.5 > eventData.getEndAzimuth()) || endAz == eventData.getEndAzimuth());
		Assert.assertTrue((maxEl > eventData.getMaxElevation() && maxEl - 0.5 < eventData.getMaxElevation()) || (maxEl < eventData.getMaxElevation() && maxEl + 0.5 > eventData.getMaxElevation()) || maxEl == eventData.getMaxElevation());
		//Tests that doppler doesn't differ more than 10 units
		Assert.assertTrue((startDoppler > eventData.getStartDopplerUp()*1000000 && startDoppler - 10 < eventData.getStartDopplerUp()*1000000) || (startDoppler < eventData.getStartDopplerUp()*1000000 && startDoppler + 10 > eventData.getStartDopplerUp()*1000000) || startDoppler == eventData.getStartDopplerUp()*1000000);
		Assert.assertTrue((endDoppler > eventData.getEndDopplerUp()*1000000 && endDoppler - 10 < eventData.getEndDopplerUp()*1000000) || (endDoppler < eventData.getEndDopplerUp()*1000000 && endDoppler + 10 > eventData.getEndDopplerUp()*1000000) || endDoppler == eventData.getEndDopplerUp()*1000000);
		//Tests that signal loss doesn't differ more than 0.5 units
		Assert.assertTrue((startSigLoss > eventData.getStartSigLossUp() && startSigLoss - 0.5 < eventData.getStartSigLossUp()) || (startSigLoss < eventData.getStartSigLossUp() && startSigLoss + 0.5 > eventData.getStartSigLossUp()) || startSigLoss == eventData.getStartSigLossUp());
		Assert.assertTrue((endSigLoss > eventData.getEndSigLossUp() && endSigLoss - 0.5 < eventData.getEndSigLossUp()) || (endSigLoss < eventData.getEndSigLossUp() && endSigLoss + 0.5 > eventData.getEndSigLossUp()) || endSigLoss == eventData.getEndSigLossUp());
		//Tests that ranges doesn't differ more than 20 units
		Assert.assertTrue((rangeAtStart > eventData.getRangeAtStart() / 1000 && rangeAtStart - 20 < eventData.getRangeAtStart()/ 1000) || (rangeAtStart < eventData.getRangeAtStart()/ 1000 && rangeAtStart + 20 > eventData.getRangeAtStart()/ 1000) || rangeAtStart == eventData.getRangeAtStart()/ 1000);
		Assert.assertTrue((rangeAtEnd > eventData.getRangeAtEnd()/ 1000 && rangeAtEnd - 20 < eventData.getRangeAtEnd()/ 1000) || (rangeAtEnd < eventData.getRangeAtEnd()/ 1000 && rangeAtEnd + 20 > eventData.getRangeAtEnd()/ 1000) || rangeAtEnd == eventData.getRangeAtEnd()/ 1000);
		//Tests that signal delays doesn't differ more than 0.5 units
		Assert.assertTrue((startSigDelay > eventData.getStartSignalDelay()*1000 && startSigDelay - 0.5 < eventData.getStartSignalDelay()*1000) || (startSigDelay < eventData.getStartSignalDelay()*1000 && startSigDelay + 0.5 > eventData.getStartSignalDelay()*1000) || startSigDelay == eventData.getStartSignalDelay()*1000);
		Assert.assertTrue((endSigDelay > eventData.getEndSignalDelay()*1000 && endSigDelay - 0.5 < eventData.getEndSignalDelay()*1000) || (endSigDelay < eventData.getEndSignalDelay()*1000 && endSigDelay + 0.5 > eventData.getEndSignalDelay()*1000) || endSigDelay == eventData.getEndSignalDelay()*1000);
		Assert.assertEquals(inSunlight, eventData.isInSunlight());
		
		Assert.assertEquals(orbitNum, eventData.getOrbitNumber());
	}
	@Test
	public void testOrbitNumbers() throws OrekitException{
		List<Integer> gpredictOrbits = new ArrayList<Integer>();
		gpredictOrbits.add(26864);
		gpredictOrbits.add(26865);
		gpredictOrbits.add(26866);
		gpredictOrbits.add(26867);
		gpredictOrbits.add(26868);
		gpredictOrbits.add(26869);
		gpredictOrbits.add(26870);
		gpredictOrbits.add(26871);
		gpredictOrbits.add(26872);
		gpredictOrbits.add(26873);
		
		//Compass-1 15.04.2013
		TLE tle = new TLE("1 32787U 08021E   13104.21450568  .00002836  00000-0  33618-3 0  5953",
						"2 32787  97.7578 164.4282 0015911 150.1582 242.3965 14.85213350268478");
		
		//start date
        AbsoluteDate startDate = new AbsoluteDate(2013, 4, 15, 10, 20, 00, TimeScalesFactory.getUTC());
        //end date 
        AbsoluteDate endDate = new AbsoluteDate(2013, 4, 16, 02, 00, 00, TimeScalesFactory.getUTC());
        // Geolocation of ground station ES5EC - on roof top of the building in TĆ¤he 4, Tartu, Estonia
        GeodeticPoint gsLocation = new GeodeticPoint(Math.toRadians(90.0000D), Math.toRadians(0.0000D), 0.0D);
        
        double elevation = FastMath.toRadians(0.1);
		String gsName = "GROUNDSTATION";
		String satName = "SATELLITE";
		double uplinkFreq = 100;
		double downlinkFreq = 100;
		double gsLoss = 0;
		double satLoss = 0;

		List<ContactEventData> contactEvents = calc.calculateContact(tle, startDate, endDate, gsLocation, elevation, gsName, satName, uplinkFreq, downlinkFreq, gsLoss, satLoss);
		List<Integer> orbits = new ArrayList<Integer>();
		for(ContactEventData data : contactEvents){
			orbits.add(data.getOrbitNumber());
		}
		
		Assert.assertEquals(gpredictOrbits, orbits);
	}
	//Tests for testing OREKIT
	@Test
	public void testCalculateOrbitalState() throws OrekitException{
		new Vector3D(5817271.98,-2582482.14, -2884522.84);
		PVCoordinates testData = new PVCoordinates(new Vector3D(5817271.977140073,-2582482.1444167136, -2884522.8374419734),new Vector3D(-3263.3135917055765, 224.04277614020418, -6811.321791112275));
		//start date
        AbsoluteDate date = new AbsoluteDate(2013, 4, 9, 6, 16, 00, TimeScalesFactory.getUTC());
        PVCoordinates coordinates = calc.calculateOrbitalState(compass1, date);
        //Checks if satellites orbital state is correct
        Assert.assertTrue(coordinates.getPosition().equals(testData.getPosition()));
        Assert.assertTrue(coordinates.getVelocity().equals(testData.getVelocity()));		
	}
	@Test
	public void testCalculateDoppler() throws OrekitException{
		double testData = 0.0021638851054035513;
		//start date
        AbsoluteDate date = new AbsoluteDate(2013, 4, 9, 7, 24, 46, TimeScalesFactory.getUTC());
		final PVCoordinates satellite = calc.calculateOrbitalState(compass1, date);
		final Orbit initalOrbit = new CartesianOrbit(satellite, inertialFrame, date, MU);
		
		double doppler = calc.calculateDoppler(initalOrbit, locationOnEarth, date, 100);
		Assert.assertEquals(testData, doppler);
	}
	@Test 
	public void testCalculateAzimuth() throws OrekitException{
		double testAzimuth = 20.601740156809935;
		PVCoordinates satellite = new PVCoordinates(new Vector3D(1402157.7198109608,470328.23807256925, 6826584.149263805),new Vector3D(6927.844423204071, -2757.0154220637046, -1243.0135434085958));
		
		AbsoluteDate date = new AbsoluteDate(2013, 4, 9, 7, 24, 46, TimeScalesFactory.getUTC());
		double azimuth = calc.calculateAzimuth(satellite, locationOnEarth, inertialFrame, date);
		Assert.assertEquals(testAzimuth, azimuth);
	}
	@Test
	public void testCalculateElevation() throws OrekitException{
		double testElevation = 0.25959421965516055;
		PVCoordinates satellite = new PVCoordinates(new Vector3D(1402157.7198109608,470328.23807256925, 6826584.149263805),new Vector3D(6927.844423204071, -2757.0154220637046, -1243.0135434085958));
		AbsoluteDate date = new AbsoluteDate(2013, 4, 9, 7, 24, 46, TimeScalesFactory.getUTC());
		double elevation = calc.calculateElevation(satellite, locationOnEarth, date);
		
		Assert.assertEquals(testElevation, elevation);
	}
	@Test
	public void testCalculateMaxElevation() throws OrekitException{
		double testMaxElev = 29.663951350922403;
		
		AbsoluteDate date = new AbsoluteDate(2013, 4, 9, 7, 24, 46, TimeScalesFactory.getUTC());
		AbsoluteDate endDate = new AbsoluteDate(2013, 4, 9, 7, 37, 00, TimeScalesFactory.getUTC());
		double maxElev = calc.calculateMaxElevation(getSpacecraftState(), locationOnEarth, date, endDate);
		Assert.assertEquals(testMaxElev, maxElev);
	}
	@Test
	public void testIsEclipsed() throws OrekitException{
		//if isEclipsed < 0 not in sunlight, else in sunlight
		double testIsEclipsed = 0.7624552484902083;
		double isEclipsed = calc.isEclipse(getSpacecraftState());
		Assert.assertEquals(testIsEclipsed, isEclipsed);
		Assert.assertTrue(isEclipsed > 0);
	}
	@Test
	public void testCalculateRange() throws OrekitException{
		double testRange = 2870925.450789055;
		double range = calc.calculateRange(getSpacecraftState(), locationOnEarth, inertialFrame);
		Assert.assertEquals(testRange, range);
	}
	@Test
	public void testCalculateSignalLoss(){
		double testSignalLoss = 72.5;
		double signalLoss = calc.calculateSignalLoss(1000, 100, 0, 0);
		Assert.assertEquals(testSignalLoss, signalLoss);
	}
	@Test
	public void testSignalDelay(){
		double testSignalDelay = 3.3356409519815205E-6;
		double signalDelay = calc.calculateSignalDelay(1000);
		Assert.assertEquals(testSignalDelay, signalDelay);
	}
	@Test
	public void testCalculateMinRange() throws IllegalArgumentException, OrekitException{
		double testMinRange = 1115188.1568161414;
		
		AbsoluteDate startDate = new AbsoluteDate(2013, 4, 9, 7, 24, 46, TimeScalesFactory.getUTC());
		AbsoluteDate endDate = new AbsoluteDate(2013, 4, 9, 7, 37, 00, TimeScalesFactory.getUTC());
		double minRange = calc.calculateMinRange(getSpacecraftState(), locationOnEarth, inertialFrame, startDate, endDate);
		Assert.assertEquals(testMinRange, minRange);
	}
	@Test
	public void testCalculateMaxRange() throws IllegalArgumentException, OrekitException{
		double testMaxRange = 2870925.450789055;
		
		AbsoluteDate startDate = new AbsoluteDate(2013, 4, 9, 7, 24, 46, TimeScalesFactory.getUTC());
		AbsoluteDate endDate = new AbsoluteDate(2013, 4, 9, 7, 37, 00, TimeScalesFactory.getUTC());
		double maxRange = calc.calculateMaxRange(getSpacecraftState(), locationOnEarth, inertialFrame, startDate, endDate);
		Assert.assertEquals(testMaxRange, maxRange);
	}
	@Test
	public void testCalculateOrbitNumber() throws OrekitException{
		int testOrbitNumber = 26772;
		
		AbsoluteDate date = new AbsoluteDate(2013, 4, 9, 7, 24, 46, TimeScalesFactory.getUTC());
		int orbitNum = calc.calculateOrbitNumber(compass1, date);
		Assert.assertEquals(testOrbitNumber, orbitNum);
	}
	public SpacecraftState getSpacecraftState() throws IllegalArgumentException, OrekitException{
		PVCoordinates satellite = new PVCoordinates(new Vector3D(1402157.7198109608,470328.23807256925, 6826584.149263805),new Vector3D(6927.844423204071, -2757.0154220637046, -1243.0135434085958));
		AbsoluteDate date = new AbsoluteDate(2013, 4, 9, 7, 24, 46, TimeScalesFactory.getUTC());
		final Orbit initalOrbit = new CartesianOrbit(satellite, inertialFrame, date, MU);
		Rotation rotation = new Rotation(-0.9999997843647496, 1.4513606379649138E-5, 6.565514520157883E-4, 2.017049663248642E-8, false);
		Attitude attitude = new Attitude(date, inertialFrame, rotation, new Vector3D(0,0,0));
		return new SpacecraftState(initalOrbit, attitude, 1000);
	}
}
