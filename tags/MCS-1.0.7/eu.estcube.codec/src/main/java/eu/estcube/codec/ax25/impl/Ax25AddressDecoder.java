/** 
 *
 */
package eu.estcube.codec.ax25.impl;

import org.apache.commons.lang3.Validate;

/**
 * Converts AX.25 frame address field bytes to ASCII string according to AX.25
 * standard.
 * 
 * http://www.tapr.org/pub_ax25.html#2.2.13
 */
public class Ax25AddressDecoder {

    public static final int NUMBER_OF_BYTES = 7;

    public String toString(byte[] bytes) {
        Validate.notNull(bytes, "AX.25 address can't be null");
        Validate.isTrue(bytes.length == NUMBER_OF_BYTES, "AX.25 address has to be 7 bytes");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            if (i < bytes.length - 1) {
                // 6 first bytes has to be shifted by one
                sb.append((char) ((bytes[i] & 0xFF) >> 1));
            } else {
                // 7th byte is SSID = bits ***SSID* from the byte
                sb.append(String.valueOf((byte) (((bytes[i] & 0xFF) >> 1) & 0x0F)));
            }
        }
        // replace all spaces (one or more) with single '-' (dash).
        String result = sb.toString().replaceAll("[ ]+", "-");
        return result;
    }
}
