package eu.estcube.codec.radiobeacon.parser;

import static org.junit.Assert.assertEquals;

import org.hbird.exchange.core.Parameter;
import org.junit.Before;
import org.junit.Test;

public class RadionBeaconMessageParserIntegerTest {

    private static final String ID = "ID";
    private static final String NAME = "NAME";

    private RadionBeaconMessageParserInteger parser1;
    private RadionBeaconMessageParserInteger parser2;

    @Before
    public void setUp() throws Exception {
        parser1 = new RadionBeaconMessageParserInteger(0, 2, new Parameter(ID, NAME));
        parser2 = new RadionBeaconMessageParserInteger(0, 4, new Parameter(ID, NAME));

    }

    @Test
    public void parseTest() throws Exception {
        assertEquals(parser1.parse("AT"), "160");
        assertEquals(parser2.parse("T6SN"), "1593");
    }
}
