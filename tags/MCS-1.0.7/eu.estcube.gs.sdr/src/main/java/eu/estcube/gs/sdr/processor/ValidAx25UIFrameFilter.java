/**
 *
 */
package eu.estcube.gs.sdr.processor;

import org.springframework.stereotype.Component;

import eu.estcube.domain.transport.ax25.Ax25UIFrame;

/**
 *
 */
@Component
public class ValidAx25UIFrameFilter {

    public boolean valid(Ax25UIFrame frame) {
        return frame.getStatus().isValid();
    }
}
