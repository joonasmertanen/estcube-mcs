/** 
 *
 */
package eu.estcube.gs.contact;

import org.apache.camel.Handler;
import org.hbird.exchange.navigation.LocationContactEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class LocationContactEventHolder {

    private static final Logger LOG = LoggerFactory.getLogger(LocationContactEventHolder.class);

    private LocationContactEvent event;

    @Handler
    public void handle(LocationContactEvent event) {
        synchronized (this) {
            LOG.info("Stored new {}", event);
            this.event = event;
        }
    }

    public String getContactId(long timestamp) {
        synchronized (this) {
            return event != null && event.getStartTime() <= timestamp && event.getEndTime() >= timestamp ? event
                    .getInstanceID() : null;
        }
    }
    
    /**
     * Returns the current orbit number from the LocationContactEvent that has been extracted from the Track command
     * 
     * @param timestamp
     * @return
     */
    public long getOrbitNumber(long timestamp) {
        synchronized (this) {
            return event != null && event.getStartTime() <= timestamp && event.getEndTime() >= timestamp ? event
                     .getOrbitNumber() : -1;
        }
    }

    /**
     * Returns the satellite id from current event
     *
     * @return
     */
    public String getSatelliteId() {
        synchronized (this) {
            return event != null ? event.getSatelliteID() : null;
        }
    }
}
