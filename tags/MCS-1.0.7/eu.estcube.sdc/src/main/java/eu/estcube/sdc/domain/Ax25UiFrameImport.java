/** 
 *
 */
package eu.estcube.sdc.domain;

/**
 *
 */
public class Ax25UiFrameImport extends Ax25UiFrameInput {

    public static final long DEFAULT_ORBIT_NUMBER = 0;

    private String timestamp;
    private long orbitNumber = DEFAULT_ORBIT_NUMBER;

    public Ax25UiFrameImport() {
        super();
    }

    /**
     * Returns timestamp.
     * 
     * @return the timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * Sets timestamp.
     * 
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Returns orbitNumber.
     * 
     * @return the orbitNumber
     */
    public long getOrbitNumber() {
        return orbitNumber;
    }

    /**
     * Sets orbitNumber.
     * 
     * @param orbitNumber the orbitNumber to set
     */
    public void setOrbitNumber(long orbitNumber) {
        this.orbitNumber = orbitNumber;
    }
}
