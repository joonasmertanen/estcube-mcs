package eu.estcube.webserverpublic.radiobeacon;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hbird.business.api.IdBuilder;
import org.hbird.exchange.core.EntityInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.estcube.codec.radiobeacon.RadioBeaconDateInputParser;
import eu.estcube.codec.radiobeacon.RadioBeaconTranslator;
import eu.estcube.common.json.ToJsonProcessor;

@SuppressWarnings("serial")
@Component
public class RadioBeaconTranslationServlet extends HttpServlet {

    @Autowired
    IdBuilder idBuilder;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException
    {

        ToJsonProcessor toJson = new ToJsonProcessor();

        String datetime = request.getParameter("datetime");
        String source = request.getParameter("source");
        String insertedBy = request.getParameter("insertedBy");
        String data = request.getParameter("data").toUpperCase();

        JSONObject json = new JSONObject();
        String result = "";
        try {
            try {
                HashMap<String, EntityInstance> parameters = new RadioBeaconTranslator().toParameters(data, new
                        RadioBeaconDateInputParser().parse(datetime), source, insertedBy, idBuilder);

                if (parameters.values().size() == 0) {

                    json.put("status", "error");
                    json.put("message",
                            "Beacon message could not be parsed. Please check that your message is correct!");
                    result = json.toString();

                } else {

                    HashMap<String, Object> r = new HashMap<String, Object>();
                    r.put("status", "ok");
                    r.put("message", parameters);
                    result = toJson.process(r);
                }

            } catch (Exception e) {
                String errorMessage = "Error parsing message to parameters. Beacon message is probably invalid. Error: ";
                errorMessage += e.getMessage();
                json.put("status", "error");
                json.put("message", errorMessage);
                result = json.toString();
            }

        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        response.getWriter().write(result);

    }
}