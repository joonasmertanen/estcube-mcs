package eu.estcube.webserverpublic.radiobeacon;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.camel.Handler;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.estcube.codec.radiobeacon.RadioBeaconTranslator;
import eu.estcube.common.utils.Emailer;

@SuppressWarnings("serial")
@Component
public class RadioBeaconSendEmailServlet extends HttpServlet {

    @Value("${email.to}")
    String toEmail;

    @Value("${email.checkValue}")
    String checkValue;

    @Autowired
    private Emailer emailer;

    class RadioBeacon {
        protected Set<String> radioBeaconMessage = new HashSet<String>();
    }

    @Override
    @Handler
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String datetime = request.getParameter("datetime").trim();
        String source = request.getParameter("source").trim();
        String data = request.getParameter("data").trim().toUpperCase();
        String check = request.getParameter("check").trim();

        JSONObject json = new JSONObject();

        try {
            RadioBeaconTranslator translator = new RadioBeaconTranslator();

            if (!check.equals(checkValue)) {
                json.put("status", "error");
                json.put("message", "Invalid check value!");

            } else if (!(translator.checkBeaconMessage(translator.reformBeaconMessage(data)))) {
                json.put("status", "error");
                json.put("message", "Invalid beacon!");

            } else {
                try {
                    emailer.sendEmail(toEmail, "Beacon data: " + datetime + " " + source + "",
                            buildMessage(data, datetime, source));

                    json.put("status", "ok");
                    json.put("message", "Message sent!");

                } catch (Exception e) {
                    json.put("status", "error");
                    json.put("message", "Message could not be sent: " + e.getMessage());
                }
            }

        } catch (JSONException e1) {
            throw new ServletException(e1);
        }

        response.getWriter().write(json.toString());

    }

    public String buildMessage(String data, String datetime, String source) throws Exception {
        return data + ";" + source + ";" + datetime;
    }
}
