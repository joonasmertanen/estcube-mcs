package eu.estcube.commanding.analysis;

/**
 * 
 * @author Ivar Mahhonin
 * 
 */
public enum OptimizationType {
	/** Rotator1: Elevation is low, north line is not crossed */
	ROT_1_NO_CROSS_EL_LOW,
	/** Rotator1: Elevation is high, north line is not crossed */
	ROT_1_NO_CROSS_EL_HIGH,
	/** Rotator1: Elevation is low, north line is crossed */
	ROT_1_CROSS_EL_LOW,
	/** Rotator1: Elevation is high, north line is crossed */
	ROT_1_CROSS_EL_HIGH,
	/** Rotator2: Elevation is low, north line is crossed */
	ROT_2_CROSS_EL_LOW,
	/** Rotator2: Elevation is high, north line is crossed */
	ROT_2_CROSS_EL_HIGH,
	/** All Rotators: there is no satellite trajectory currently available */
	NOTHING,
}
