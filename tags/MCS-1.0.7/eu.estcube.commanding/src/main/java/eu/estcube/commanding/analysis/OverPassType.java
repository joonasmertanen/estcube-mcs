package eu.estcube.commanding.analysis;

/**
 * 
 * @author Ivar Mahhonin
 * 
 */
public enum OverPassType {
	/** Elevation is high and norh line is not crossed */
	NO_CROSS_ELEVATION_HIGH,
	/** Elevation is low and north line is not crossed */
	NO_CROSS_ELEVATION_LOW,
	/** Elevation is high and north line is crossed */
	CROSS_ELEVATION_HIGH,
	/** Elevation is low and north line is crossed */
	CROSS_ELEVATION_LOW,
	/** There is no overpass currently */
	NOTHING
}
