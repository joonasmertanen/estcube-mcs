package eu.estcube.commanding.analysis;

import java.util.List;

import org.hbird.exchange.navigation.PointingData;

import eu.estcube.commanding.api.PassAnalyzeInterface;

/**
 * In this class satellite trajectory is analyzed and overpass type is returned.
 * 
 * @author Ivar Mahhonin
 * 
 */

public class PassAnalyze implements PassAnalyzeInterface {

	public PassAnalyze() {
	}

	@Override
	public OverPassType calculateType(final List<PointingData> input) {
		OverPassType type = OverPassType.NOTHING;
		boolean cross = false;
		boolean high = false;
		cross = checkIfCross(input, cross);
		high = checkElevationHigh(input, high);
		if (cross == false && high == false) {
			type = OverPassType.NO_CROSS_ELEVATION_LOW;
		}
		if (cross == true && high == false) {
			type = OverPassType.CROSS_ELEVATION_LOW;

		}
		if (cross == true && high == true) {
			type = OverPassType.CROSS_ELEVATION_HIGH;
		}
		if (cross == false && high == true) {
			type = OverPassType.NO_CROSS_ELEVATION_HIGH;

		}
		System.out.println(type.toString());

		return type;
	}

	/**
	 * Checking whether this is high overpass.
	 * 
	 * @param coordinates - satellite trajectory.
	 * @param high - whether elevation is high.
	 * @return
	 */
	public boolean checkElevationHigh(final List<PointingData> coordinates,
			boolean high) {
		for (int i = 0; i < coordinates.size(); i++) {
			if ((coordinates.get(i).getElevation() > 75)
					&& (coordinates.get(i).getElevation() < 90)) {
				high = true;
			}
		}
		return high;

	}

	/**
	 * Checking if satellite {@link List} trajectory crosses north line.
	 * 
	 * @param coordinates - satellite {@link List} trajectory.
	 * @param cross - whether trajectory crosses north line.
	 * @return
	 */
	public boolean checkIfCross(final List<PointingData> coordinates,
			boolean cross) {

		double lastValue = coordinates.get(0).getAzimuth();
		for (int i = 0; i < coordinates.size(); i++) {
			if (Math.abs(lastValue - coordinates.get(i).getAzimuth()) > 180) {
				cross = true;
			}
			lastValue = coordinates.get(i).getAzimuth();
		}
		return cross;
	}

}
