package eu.estcube.commandautomation.automator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.estcube.commandautomation.automator.event.OnCommandHandlerFinished;
import eu.estcube.commandautomation.automator.event.OnContactHandlerFinished;
import eu.estcube.commandautomation.db.Command;
import eu.estcube.commandautomation.db.Contact;

public class ContactHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ContactHandler.class);

    public enum State {
        PREPARE_RECEIVED,
        AOS_RECEIVED,
        LOS_RECEIVED,
        FINISHED
    }

    private Contact contact;

    private List<CommandHandler> commandHandlers = new ArrayList<CommandHandler>();

    private Integer activeCommandNo;

    private Timer timeoutTimer = new Timer();

    private State state;

    private OnContactHandlerFinished onFinished;

    public ContactHandler(Contact contact, CommandHandlerFactory commandHandlerFactory) {
        this.contact = contact;

        for (Command command : contact.getCommands()) {
            CommandHandler handler = commandHandlerFactory.create(command);
            handler.setOnFinished(new OnCommandHandlerFinished() {
                @Override
                public void execute(String result) {
                    commandEnded(result);
                }
            });
            commandHandlers.add(handler);
        }
    }

    public void prepare() {
        LOG.info("PREPARE");
        state = State.PREPARE_RECEIVED;
        for (CommandHandler handler : commandHandlers) {
            handler.prepare();
        }
    }

    public void start() {
        LOG.info("START");
        state = State.AOS_RECEIVED;
        startCommand(0);
    }

    public void startCommand(final Integer no) {
        LOG.info("Start command " + no);
        if (commandHandlers.size() <= no) {
            allCommandsFinished();
            return;
        }

        activeCommandNo = no;
        CommandHandler commandHandler = commandHandlers.get(activeCommandNo);

        timeoutTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (no == activeCommandNo) {
                    killActiveCommand();
                }
            }
        }, commandHandler.getCommand().getMaxExecutionTime());

        commandHandler.start();
    }

    public void killActiveCommand() {
        LOG.info("Kill active command!");

        CommandHandler commandHandler = commandHandlers.get(activeCommandNo);
        commandHandler.kill();
    }

    public void commandEnded(String result) {
        LOG.info("Command ended!");
        Integer finishedCommandNo = activeCommandNo;
        activeCommandNo = null;

        if (state.equals(State.LOS_RECEIVED) || state.equals(State.FINISHED)) {
            allCommandsFinished();
        } else {
            startCommand(finishedCommandNo + 1);
        }
    }

    public void end() {
        LOG.info("END");

        if (state.equals(State.FINISHED)) {
            return;
        }

        state = State.LOS_RECEIVED;
        endActiveCommand();
    }

    public void endActiveCommand() {
        LOG.info("End active command!");

        final Integer currentActiveCommandNo = activeCommandNo;
        CommandHandler commandHandler = commandHandlers.get(activeCommandNo);

        timeoutTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (currentActiveCommandNo == activeCommandNo) {
                    killActiveCommand();
                }
            }
        }, commandHandler.getCommand().getWaitAfterEndTime());

        commandHandler.end();
    }

    public void allCommandsFinished() {
        LOG.info("All commands finished!");
        state = State.FINISHED;
        onFinished.execute();
    }

    public void setOnFinished(OnContactHandlerFinished event) {
        onFinished = event;
    }

    public Contact getContact() {
        return contact;
    }

    public int getActiveCommandNo() {
        return activeCommandNo;
    }

    public State getState() {
        return state;
    }

}
