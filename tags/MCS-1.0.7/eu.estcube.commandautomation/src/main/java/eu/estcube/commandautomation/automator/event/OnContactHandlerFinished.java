package eu.estcube.commandautomation.automator.event;

public interface OnContactHandlerFinished {

    public void execute();
}
