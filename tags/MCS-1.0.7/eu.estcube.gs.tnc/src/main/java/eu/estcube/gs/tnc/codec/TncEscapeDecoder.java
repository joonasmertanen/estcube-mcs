package eu.estcube.gs.tnc.codec;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoderAdapter;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

/**
 *
 */
public class TncEscapeDecoder extends ProtocolDecoderAdapter {

    /** @{inheritDoc}. */
    @Override
    public void decode(IoSession session, IoBuffer in, ProtocolDecoderOutput out) throws Exception {
        IoBuffer result = IoBuffer.allocate(in.limit());
        boolean escapeMode = false;
        while (in.hasRemaining()) {
            byte b = in.get();
            if (b == TncConstants.FESC) {
                // start the escape mode and skip the byte
                escapeMode = true;
            }
            else if(escapeMode) {
                // in escape mode
                if (b == TncConstants.TFEND) {
                    // replace TFEND with FEND
                    result.put(TncConstants.FEND);
                }
                else if (b == TncConstants.TFESC) {
                    // replace TFESC with FESC
                    result.put(TncConstants.FESC);
                }
                else {
                    // use the byte as is
                    result.put(b);
                }
                // exit the escape mode
                escapeMode = false;
            } 
            else {
                // it's not escape byte and we are not in escape mode;
                // just copy the byte
                result.put(b);
            }
        }
        // flip and write result to the output
        out.write(result.flip());
    }
}
