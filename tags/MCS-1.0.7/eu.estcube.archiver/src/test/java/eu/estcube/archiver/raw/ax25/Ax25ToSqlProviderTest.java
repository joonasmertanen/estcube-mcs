package eu.estcube.archiver.raw.ax25;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Message;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import eu.estcube.archiver.raw.sql.MessageToSqlFrameDataProvider;
import eu.estcube.domain.transport.Direction;
import eu.estcube.domain.transport.ax25.Ax25UIFrame;

public class Ax25ToSqlProviderTest {
	private static final long TIMESTAMP = System.currentTimeMillis();
	private static final String SAT = "SAT1";

	private static final byte[] DESTADDR = new byte[] { 0, 1 };
	private static final byte[] SRCADDR = new byte[] { 2, 3 };

	private static final byte CTRL = 4;
	private static final byte PID = 5;

	private static final byte[] INFO = new byte[] { 6, 7, 8, 9 };
	private static final byte[] FCS = new byte[] { 10, 11 };

	private Message message;
	private Ax25UIFrame frame;
	private Map<String, Object> headers;

	private Ax25ToSqlProvider ax25ToSqlProvider;

	@Before
	public void setUp() {
		message = Mockito.mock(Message.class);
		frame = new Ax25UIFrame();
		frame.setDestAddr(DESTADDR);
		frame.setSrcAddr(SRCADDR);
		frame.setCtrl(CTRL);
		frame.setPid(PID);
		frame.setInfo(INFO);
		frame.setFcs(FCS);

		headers = new HashMap<String, Object>();

		headers.put(MessageToSqlFrameDataProvider.HDR_RECEPTION_TIME, TIMESTAMP);
		headers.put(MessageToSqlFrameDataProvider.HDR_SATELLITE, SAT);

		ax25ToSqlProvider = new Ax25ToSqlProvider(Direction.DOWN, message);

		Mockito.when(message.getBody(Ax25UIFrame.class)).thenReturn(frame);
		Mockito.when(message.getHeaders()).thenReturn(headers);
	}

	@Test
	public void testAx25ToSqlProvider() {
		assertSame(message, ax25ToSqlProvider.getMessage());
	}

	@Test
	public void testGetValue() {
		assertEquals(
				new Long(TIMESTAMP),
				ax25ToSqlProvider
						.getValue(MessageToSqlFrameDataProvider.COL_RECEPTION_TIME));

		assertEquals(SAT,
				ax25ToSqlProvider
						.getValue(MessageToSqlFrameDataProvider.COL_SATELLITE));

		assertTrue(Arrays.equals(DESTADDR, (byte[]) ax25ToSqlProvider
				.getValue(Ax25ToSqlProvider.COL_DESTADDR)));

		assertTrue(Arrays.equals(SRCADDR, (byte[]) ax25ToSqlProvider
				.getValue(Ax25ToSqlProvider.COL_SRCADDR)));

		assertEquals(CTRL,
				ax25ToSqlProvider.getValue(Ax25ToSqlProvider.COL_CTRL));

		assertEquals(PID, ax25ToSqlProvider.getValue(Ax25ToSqlProvider.COL_PID));

		assertTrue(Arrays
				.equals(INFO, (byte[]) ax25ToSqlProvider
						.getValue(Ax25ToSqlProvider.COL_INFO)));

		assertTrue(Arrays.equals(FCS,
				(byte[]) ax25ToSqlProvider.getValue(Ax25ToSqlProvider.COL_FCS)));

		frame.setErrorFcs(true);
		assertEquals(1,
				ax25ToSqlProvider.getValue(Ax25ToSqlProvider.COL_ERROR_BITMASK));

	}

	@Test
	public void testCalculateErrorBitMask() {
		int m;

		m = ax25ToSqlProvider.calculateErrorBitMask(frame);
		assertEquals(0, m);

		frame.setErrorFcs(true);
		m = ax25ToSqlProvider.calculateErrorBitMask(frame);
		assertEquals(1, m);

		frame.setErrorTooLong(true);
		m = ax25ToSqlProvider.calculateErrorBitMask(frame);
		assertEquals(3, m);

		frame.setErrorTooShort(true);
		m = ax25ToSqlProvider.calculateErrorBitMask(frame);
		assertEquals(7, m);

		frame.setErrorUnAligned(true);
		m = ax25ToSqlProvider.calculateErrorBitMask(frame);
		assertEquals(15, m);

		frame.setErrorUnstuffedBits(true);
		m = ax25ToSqlProvider.calculateErrorBitMask(frame);
		assertEquals(31, m);

	}

}
