package eu.estcube.archiver.raw.sql;

import java.util.Map;

import eu.estcube.archiver.raw.FrameDataProvider;

public interface SqlFrameDataProvider extends FrameDataProvider {
	public Object getValue(String column);

	public Map<String, String> getDetails();
}
