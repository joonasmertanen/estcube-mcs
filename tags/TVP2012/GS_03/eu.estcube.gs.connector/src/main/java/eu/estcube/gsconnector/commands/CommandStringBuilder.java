package eu.estcube.gsconnector.commands;

import eu.estcube.domain.TelemetryCommand;

public interface CommandStringBuilder {
    public StringBuilder createMessageString(TelemetryCommand command);
}
