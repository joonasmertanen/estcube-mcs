package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SetTransmitMode implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand command) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+X ");
        messageString.append(command.getParameter("TX Mode"));
        messageString.append(" ");
        messageString.append(command.getParameter("TX Passband"));
        messageString.append("\n");
        return messageString;
    }
}
