package eu.estcube.common;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.camel.Message;
import org.hbird.exchange.core.Label;
import org.junit.Before;
import org.junit.Test;

import eu.estcube.domain.GroundstationLabel;
import eu.estcube.domain.TelemetryObject;

public class TelemetryObjectSplitterTest {
    private TelemetryObjectSplitter telemetryObjectSplitter;
    private ArrayList<TelemetryObject> telemetryObjects;
    private String telObjName = "telObjName";
    private String telObjName2 = "telObjName2";
    private String labelName = "labelName";
    private String labelName2 = "labelName2";
    private String value = "100";
    private String value2 = "101";

    @Before
    public void setUp() {
        telemetryObjectSplitter = new TelemetryObjectSplitter();
        TelemetryObject telemetryObject = new TelemetryObject(telObjName, new Date());
        TelemetryObject telemetryObject2 = new TelemetryObject(telObjName2, new Date());

        Label telemetryParameter = new GroundstationLabel(labelName, value);
        Label telemetryParameter2 = new GroundstationLabel(labelName2, value2);

        telemetryObject.addLabel(telemetryParameter);
        telemetryObject.addLabel(telemetryParameter2);
        telemetryObject2.addLabel(telemetryParameter2);

        telemetryObjects = new ArrayList<TelemetryObject>();
        telemetryObjects.add(telemetryObject);
        telemetryObjects.add(telemetryObject2);
    }

    @Test
    public void testSplitter() {
        List<Message> messages = telemetryObjectSplitter.splitMessage(telemetryObjects);
        assertEquals(messages.size(), telemetryObjects.size());

        for (int i = 0; i < telemetryObjects.size(); i++) {
            assertEquals(messages.get(i).getBody(TelemetryObject.class), telemetryObjects.get(i));
        }
    }
}
