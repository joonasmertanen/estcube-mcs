package eu.estcube.gs.hamlib;

import eu.estcube.gs.config.DriverConfiguration;

public class HamlibIO {

    public static String getDeviceDriverUrl(DeviceType deviceType, DriverConfiguration config) {

        switch (deviceType) {
        case ROTATOR: {
            return getNettyAddress(config);
        }
        case RADIO: {
            return getNettyAddress(config);
        }
        default:
            throw new UnsupportedOperationException("The device of type '" + deviceType == null ? "null" : deviceType
                + "' is not supported.");
        }
    }

    private static String getNettyAddress(DriverConfiguration config) {
        StringBuilder address = new StringBuilder("netty:tcp://" + config.getAddress());
        address.append("?sync=true"); // allow for responses to be forwarded
        address.append("&encoders=#encoderStringToBytes");

        /* the decoders are called in this order, from left to right */
        address
            .append("&decoders=#decoderSplitOnNewline,#decoderBytesToString,#decoderBufferer,#decoderStringLogger,#decoderStringToTelemetryObject");
        return address.toString();
    }
}
