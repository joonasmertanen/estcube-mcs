package eu.estcube.crowd;

import java.io.IOException;
import java.rmi.RemoteException;
import java.security.Principal;

import javax.security.auth.Subject;

import org.codehaus.xfire.XFireRuntimeException;
import org.eclipse.jetty.http.security.Credential;
import org.eclipse.jetty.security.MappedLoginService;
import org.eclipse.jetty.server.UserIdentity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.exception.ApplicationAccessDeniedException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;

public class CrowdLoginService extends MappedLoginService {

    private final SecurityServerClient crowdClient;
    private static final Logger LOG = LoggerFactory.getLogger(CrowdLoginService.class);
    public static String exception;

    public static String getException() {
        return exception;
    }

    public static void setException(String exception) {
        CrowdLoginService.exception = exception;
    }

    public CrowdLoginService(SecurityServerClient crowdClient) {
        this.crowdClient = crowdClient;
    }

    @Override
    protected void doStart() {
        try {
            super.doStart();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
    }

    // TODO
    private void setFailed(Throwable th) {
        for (Listener listener : _listeners)
            listener.lifeCycleFailure(this, th);
    }

    @Override
    // Loads user information from crowd
    protected UserIdentity loadUser(String username) {
        // If user was found, finds the groups it belongs to and creates a user
        // identity using putUser method
        try {
            String[] groups = crowdClient.findGroupMemberships(username);
            return putUser(username, null, groups);
        } catch (UserNotFoundException e) {
            // TODO Auto-generated catch block
            LOG.info("UserNotFoundException {}", e.getCause());
            setException("UserNotFoundException");
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            LOG.info("RemoteException {}", e.getCause());
            setException("RemoteException");
        } catch (InvalidAuthenticationException e) {
            // TODO Auto-generated catch block
            LOG.info("InvalidAuthenticationException {}", e.getCause());
            setException("InvalidAuthenticationException");
        } catch (InvalidAuthorizationTokenException e) {
            // TODO Auto-generated catch block
            LOG.info("InvalidAuthorizationTokenException {}", e.getCause());
            setException("InvalidAuthorizationTokenException");
        } catch (XFireRuntimeException e) {
            LOG.info("XFireRuntimeException {}", e.getCause());
            setException("XFireRuntimeException");
        }
        // Returns null if an exception is caught
        return null;
    }

    @Override
    // Creates a useridentity for the user
    public synchronized UserIdentity putUser(String userName, Credential credential, String[] roles) {
        Principal userPrincipal = new CrowdUser(userName);
        Subject subject = new Subject();
        subject.getPrincipals().add(userPrincipal);
        subject.getPrivateCredentials().add(credential);

        if (roles != null)
            for (String role : roles)
                subject.getPrincipals().add(new RolePrincipal(role));

        subject.setReadOnly();
        UserIdentity identity = _identityService.newUserIdentity(subject, userPrincipal, roles);
        _users.put(userName, identity);
        return identity;
    }

    @Override
    protected void loadUsers() throws IOException {
        // Unimportant
    }

    public class CrowdUser extends KnownUser {

        private String crowdToken;

        public String getCrowdToken() {
            return crowdToken;
        }

        public void setCrowdToken(String crowdToken) {
            this.crowdToken = crowdToken;
        }

        CrowdUser(String username) {
            super(username, null);
        }

        @Override
        /*
         * Tries to authenticate the user with the given credentials. In case of
         * success, returns true. If
         * authentication fails or a an exception is caught, returns null
         */
        public boolean authenticate(Object credentials) {
            try {
                crowdToken = crowdClient.authenticatePrincipalSimple(getName(), String.valueOf(credentials));
                return isAuthenticated();
            } catch (InactiveAccountException e) {
                // TODO Auto-generated catch block
                LOG.info("InactiveAccountException {}", e.getCause());
                setException("InactiveAccountException");
                return false;
            } catch (ExpiredCredentialException e) {
                // TODO Auto-generated catch block
                LOG.info("ExpiredCredentialException {}", e.getCause());
                setException("ExpiredCredentialException");
                return false;
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                LOG.info("RemoteException {}", e.getCause());
                setException("RemoteException");
                return false;
            } catch (InvalidAuthenticationException e) {
                LOG.info("Username used for login:" + getName());
                LOG.info("CrowdUser InvalidAuthenticationException {}", e.getCause().getMessage());
                setException("InvalidAuthenticationException");
                setFailed(e);
                return false;
            } catch (InvalidAuthorizationTokenException e) {
                // TODO Auto-generated catch block
                LOG.info("InvalidAuthorizationTokenException {}", e.getCause());
                setException("InvalidAuthorizationTokenException");
                return false;
            } catch (ApplicationAccessDeniedException e) {
                // TODO Auto-generated catch block
                LOG.info("ApplicationAccessDeniedException {}", e.getCause());
                setException("ApplicationAccessDeniedException");
                return false;
            }

        }

        @Override
        public boolean isAuthenticated() {
            return crowdToken != null;
        }

    }

}
