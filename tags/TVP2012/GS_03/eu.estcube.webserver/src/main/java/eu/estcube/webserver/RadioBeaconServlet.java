package eu.estcube.webserver;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class RadioBeaconServlet extends HttpServlet {

    class RadioBeacon {
        protected Set<String> radioBeaconMessage = new HashSet<String>();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
        IOException {
        RadioBeacon radioBeacon = new RadioBeacon();
        Enumeration<?> paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            String paramName = (String) paramNames.nextElement();
            radioBeacon.radioBeaconMessage.add(paramName);
        }
        try {
            // TODO: Store somewhere radio beacon message.
            response.getWriter().write("success");
        } catch (Exception e) {
            response.getWriter().write("error");
        }
    }
}
