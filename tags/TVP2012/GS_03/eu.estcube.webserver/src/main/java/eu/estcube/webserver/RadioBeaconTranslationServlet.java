package eu.estcube.webserver;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eu.estcube.domain.RadioBeacon;

@SuppressWarnings("serial")
public class RadioBeaconTranslationServlet extends HttpServlet {

    private String radioBeaconMessage;
    private RadioBeacon receivedRadioBeacon;
    private RadioBeacon translatedRadioBeacon;
    private RadioBeaconTranslator radioBeaconTranslator = new RadioBeaconTranslator();
    private ToJsonProcessor toJson = new ToJsonProcessor();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
        IOException {
        Enumeration<?> paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            String paramName = (String) paramNames.nextElement();
            receivedRadioBeacon = new RadioBeacon();
            receivedRadioBeacon.setRadioBeacon(paramName);
        }
        radioBeaconMessage = receivedRadioBeacon.getRadioBeacon();
        radioBeaconMessage = radioBeaconTranslator.reformBeaconMessage(radioBeaconMessage);
        if (radioBeaconTranslator.checkBeaconMessage(radioBeaconMessage)) {
            try {
                translatedRadioBeacon = radioBeaconTranslator.translateMessage(radioBeaconMessage);
                String json = toJson.process(translatedRadioBeacon);
                response.getWriter().write(json);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        } else {
            response.getWriter().write("error");
        }
    }
}