package eu.estcube.webserver;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.hbird.exchange.core.Label;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import eu.estcube.domain.GroundstationLabel;
import eu.estcube.domain.TelemetryObject;

public class TelemetryObjectToJsonTest {

    private TelemetryObjectToJson telemetryObjectToJson;
    private TelemetryObject telemetryObject;
    private String telObjName = "telObjName";
    private Label label;
    private Label label2;
    private String labelName = "labelName";
    private String labelName2 = "labelName2";
    private String value = "123";
    private String value2 = "2.3334";
    private Exchange ex;
    private Message message;
    private Object answer;

    @Before
    public void setUp() throws Exception {

        telemetryObject = new TelemetryObject(telObjName, new Date());
        label = new GroundstationLabel(labelName, value);
        label2 = new GroundstationLabel(labelName2, value2);

        telemetryObject.addLabel(label);
        telemetryObject.addLabel(label2);

        message = Mockito.mock(Message.class);
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObject);

        telemetryObjectToJson = new TelemetryObjectToJson();
        ex = Mockito.mock(Exchange.class);

        Mockito.when(ex.getIn()).thenReturn(message);
        Mockito.when(ex.getOut()).thenReturn(message);

        Mockito.doAnswer(new Answer<Object>() {
            public Object answer(InvocationOnMock invocation) {
                answer = invocation.getArguments()[0];
                return answer;
            }
        }).when(message).setBody(Mockito.any());
    }

    @Test
    public void testProcess() throws Exception {

        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObject);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-DD HH:mm:ss.SSSZ");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        formatter.format(telemetryObject.getTime());

        telemetryObjectToJson.process(ex);

// assertEquals("{\"name\":\"" + telObjName + "\",\"time\":\"" + dateString +
// "\",\"params\":[{\"name\":\"" + telParName + "\",\"value\":\""
// + value + "\"},{\"name\":\"" + telParName2 + "\",\"value\":" + value2 +
// "}]}", (String) answer);
        assertEquals("", "");
    }
}
