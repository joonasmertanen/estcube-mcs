package eu.estcube.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.hbird.exchange.core.Label;
import org.junit.Before;
import org.junit.Test;

public class LabelTest {

    private static final String PARAM_NAME = "A";
    private static final String PARAM_VALUE = "100";

    private static final String NULL_PARAM_NAME = "B";

    private Label l1, l2;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        l1 = new GroundstationLabel(NULL_PARAM_NAME);
        l2 = new GroundstationLabel(PARAM_NAME, PARAM_VALUE);
    }

    /**
     * Test method for {@link eu.estcube.domain.GroundstationLabel#TelemetryParameter(java.lang.String)}.
     */
    @Test
    public void testTelemetryParameterString() {
        assertNotNull(l1);
        assertEquals(null, l1.getValue());
        assertEquals(NULL_PARAM_NAME, l1.getName());
        assertNotNull(l1.toString());
    }

    /**
     * Test method for {@link eu.estcube.domain.GroundstationLabel#TelemetryParameter(java.lang.String, java.lang.Object)}.
     */
    @Test
    public void testTelemetryParameterStringObject() {
        assertNotNull(l2);
        assertNotNull(l2.getValue());
        assertEquals(PARAM_VALUE, l2.getValue());
        assertEquals(PARAM_NAME, l2.getName());
        assertNotNull(l2.toString());
    }

    /**
     * Test method for {@link eu.estcube.domain.GroundstationLabel#getName()}.
     */
    @Test
    public void testGetName() {
        assertEquals(NULL_PARAM_NAME, l1.getName());
        assertEquals(PARAM_NAME, l2.getName());
    }

    /**
     * Test method for {@link eu.estcube.domain.GroundstationLabel#getValue()}.
     */
    @Test
    public void testGetValue() {
        assertEquals(null, l1.getValue());
        assertEquals(PARAM_VALUE, l2.getValue());
    }

    /**
     * Test method for {@link eu.estcube.domain.GroundstationLabel#setValue(java.lang.Object)}.
     */
    @Test
    public void testSetValue() {
        l1.setValue("101");
        assertEquals("101", l1.getValue());
    }

}
