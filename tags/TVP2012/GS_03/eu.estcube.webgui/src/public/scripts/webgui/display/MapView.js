define([
	"dojo/_base/declare",
    "../common/Constants",
    "./ViewBase"
    ],
	
    function(declare, Constants, ViewBase) {
		
		return [
			declare("MapController", Controller, {

				geoLocation: null,
				
				initialLocation: {
					lon: 0,
					lat: 0
				},
				constructor: function(args) {
					this.geoLocation.set("lat", this.initialLocation.lat);
					this.geoLocation.set("lon", this.initialLocation.lon);
				},
				channelHandler: function(message, channel) {
					if(this.geoLocation != null) {
						if(message.name == "Latitude") {
							this.geoLocation.set("lat", message.value);
						}
						if(message.name == "Longitude") {
							this.geoLocation.set("lon", message.value);
						}
					}
				}
				
			}),
			
			declare("MapView", View, {
				
				geoLocation: null,
				
				build: function() {
					
					var self = this;
					var gs = this.groundStationInfo[0];
					var featureTooltip = null;
					
	                var map = new OpenLayers.Map(this.divId , {
	                    controls: [
	                        new OpenLayers.Control.Navigation(),
	                        new OpenLayers.Control.PanZoomBar(),
	                        new OpenLayers.Control.ScaleLine(),
	                        new OpenLayers.Control.MousePosition(),
	                        new OpenLayers.Control.KeyboardDefaults()
	                    ],
	                    numZoomLevels: 10
	                });
	                var layer = new OpenLayers.Layer.WMS(
	                	"Global Imagery",
	                    "http://maps.opengeo.org/geowebcache/service/wms",
	                    { layers: "bluemarble" },
	                    { tileOrigin: new OpenLayers.LonLat(-180, -90) }
	                );
	                map.addLayer(layer);

	                var renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
	                renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;
	                
	                var vectorPointLayer = new OpenLayers.Layer.Vector("Ground Station Location Geometry Layer", {
	                    styleMap: new OpenLayers.StyleMap({
	                    	'default': {
	                    		strokeOpacity: 1,
		                        strokeWidth: 2,
		                        fillColor: "#FF0000",
		                        fillOpacity: 0.5,
		                        pointRadius: 4,
		                        strokeColor: "#000000",
		                        cursor: "pointer"
	                    	},
	                    	'select': {
	                    		strokeOpacity: 1,
		                        strokeWidth: 2,
		                        fillColor: "#FFA500",
		                        fillOpacity: 0.5,
		                        pointRadius: 4,
		                        strokeColor: "#000000",
		                        cursor: "pointer"
	                    	}
	                    }),
	                    renderers: renderer
	                });
	                
	                var vectorPolygonLayer = new OpenLayers.Layer.Vector("Ground Station Coverage Area Geometry Layer", {
	                    styleMap: new OpenLayers.StyleMap({
	                    	'default': {
	                    		strokeOpacity: 1,
		                        strokeWidth: 2,
		                        fillColor: "#FF0000",
		                        fillOpacity: 0.15,
		                        strokeColor: "#FF7F00"
	                    	}
	                    }),
	                    renderers: renderer
	                });

	                var point = new OpenLayers.Geometry.Point(gs.initialLocation.lon, gs.initialLocation.lat);
	                var pointFeature = new OpenLayers.Feature.Vector(point);
	                
	                var polygonFeature = new OpenLayers.Feature.Vector(
	                	OpenLayers.Geometry.fromWKT(gs.coverageArea)
	                );
	                
	                map.addLayer(vectorPointLayer);
	                map.addLayer(vectorPolygonLayer);
	                map.setCenter(new OpenLayers.LonLat(gs.initialLocation.lon, gs.initialLocation.lat), 5);
	                vectorPointLayer.addFeatures([pointFeature]);
	                vectorPolygonLayer.addFeatures([polygonFeature]);
	                addControls();
	                
	                function addControls() {
	                    var selectCtrl = new OpenLayers.Control.SelectFeature(vectorPointLayer, {
	                    	clickout: true,
	                    	toggle: false,
			                multiple: true,
			                hover: false,
			                toggleKey: "ctrlKey",
			                multipleKey: "shiftKey",
			                box: false,
			                onSelect: function(feature) {
			                	// TODO: some feature functionality on click
			                },
		                    callbacks: {
		                    	over : function(feature) {
		                    		onFeatureHoverOver(feature);
		                    	 },
		                    	 out : function(feature) {
		                    		 onFeatureHoverOut(feature);
		                    	 }
		                    }
	                    	
	                    });

	                    map.addControl(selectCtrl);
	                    selectCtrl.activate();
		            };
		            
		            function onFeatureHoverOver(feature) {
		            	if(featureTooltip != null) {
		            		map.removePopup(featureTooltip);
		       			 	featureTooltip.destroy();
		       			 	featureTooltip = null;
		            	}
		       		
			       		if(feature != null) {
			       			var content = 
			       				"Type: " + gs.type + "<br />" + 
			       				"Name: " + gs.abbreviation + "<br />" +
			       				"Location: " + gs.initialLocation.lon + ", " + gs.initialLocation.lat;
	
			       			featureTooltip = new OpenLayers.Popup("activetooltip",
			                           feature.geometry.getBounds().getCenterLonLat(),
			                           new OpenLayers.Size(200, 70),
			                           content,
			                           false);
			       			featureTooltip.contentDiv.style.opacity = 0.5;
			       			featureTooltip.contentDiv.style.backgroundColor='#333300';
			       			featureTooltip.contentDiv.style.fontWeight ="bold";
			       			featureTooltip.contentDiv.style.fontSize ="12px";
			       			featureTooltip.contentDiv.style.overflow='hidden';
			       			featureTooltip.groupDiv.style.height = 'auto';
			       			featureTooltip.div.style.overflow ="visible";
			       			featureTooltip.div.style.margin ="-8px 0 0 8px";
			       			feature.popup = featureTooltip;
			       		    
			       		    map.addPopup(featureTooltip);
			       		}
		            };
		            
		            function onFeatureHoverOut(feature) {
		            	if(featureTooltip != null) {
		            		map.removePopup(featureTooltip);
		       			 	featureTooltip.destroy();
		       			 	featureTooltip = null;
		            	}
		            };
		            
				}
				
			})
		];
	}
);