define(
   ["dojo/_base/declare",
    "../common/Constants",
    "./AssemblerBase",
    "../display/RadioBeaconView"
   ],

   function(declare, Constants, base, view) {
       return [

           declare("RadioBeaconAssembler", AssemblerBase, {

               build: function() {
                  
                   var mapView = new RadioBeaconView({ 
                	   radioBeaconMessage: "beacon", 
                	   beaconMessageAssignee: "beaconAssignee"
                   });
                   
               }

           })

       ];
});