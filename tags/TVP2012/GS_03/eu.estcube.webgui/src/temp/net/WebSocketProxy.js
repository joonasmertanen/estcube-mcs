dojo.provide("webgui.net.WebSocketProxy");
dojo.require("dojox.socket");

dojo.declare("webgui.net.WebSocketProxy", null, {

	constructor : function() {
		var webServerCache = dojox.socket("ws://localhost:9292/cache/");
		var socket = dojox.socket("ws://localhost:9292/main/");
		var webCam = dojox.socket("ws://localhost:9292/webcam/");
		var versions = dojox.socket("ws://localhost:9292/versions/");
		
		webServerCache.on("open", function(event){
			webServerCache.send("Cache request.");
		});
		
		webServerCache.on("message", function(message){
			dojo.publish("fromWS", [message.data]);
		});
		
		dojo.subscribe("toWS", function(message) {
			socket.send(message);
		});
				
		socket.on("message", function(message) {
			dojo.publish("fromWS", [message.data]);
		});
		
		webCam.on("message", function(message) {
			dojo.publish("fromWebCam", [message.data]);
		});
		
		versions.on("open", function(event){
			versions.send("Versions info request.");
		});
		
		versions.on("message", function(message) {
			dojo.publish("versions", [message.data]);
		});
	}
});
