package eu.estcube.gs.radio;

import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import eu.estcube.domain.JMSConstants;
import eu.estcube.gs.config.DriverConfiguration;
import eu.estcube.gs.hamlib.DeviceType;
import eu.estcube.gs.hamlib.HamlibIO;

public class Radio extends RouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(Radio.class);

    @Value("#{config}")
    private DriverConfiguration config;

    @Override
    public void configure() {
        sendMetainfo();
        configureRoutes();
    }

    private void sendMetainfo() {
        // Outgoing to AMQ metadata topic
        from("timer://metaTimer?period=" + config.getTimerFireInterval())
            .pollEnrich("file:src/main/resources?fileName=metainfo.json&noop=true&idempotent=false")
            .log("log:metainfo").to(JMSConstants.AMQ_GS_META_SEND);
    }

    private void configureRoutes() {

        // Incoming from AMQ
        from(JMSConstants.AMQ_GS_RECEIVE).log("log: Received message in AMQ queue").choice()
            .when(header(JMSConstants.HEADER_COMPONENT_ID).isEqualTo(config.getComponentId()))
            .to(JMSConstants.DIRECT_RIG_CTLD_INTERMEDIATE).otherwise().log("log:WrongStation");

        // From AMQ to rigctld
        from(JMSConstants.DIRECT_RIG_CTLD_INTERMEDIATE).log("log: Sending from intermediate to radio").doTry()
            .to(JMSConstants.DIRECT_RIG_CTLD).recipientList(header(JMSConstants.HEADER_FORWARD)).endDoTry()
            .doCatch(Exception.class)
            .log("Log: Sending message from the AMQ receiving queue to the radio driver failed").end();

        // Outgoing to AMQ
        from(JMSConstants.DIRECT_SEND).log(
            "Log: Sending message from " + JMSConstants.DIRECT_SEND + " to " + JMSConstants.AMQ_GS_SEND).to(
            JMSConstants.AMQ_GS_SEND);

        // Outgoing to the radio
        String nettyRotctld = HamlibIO.getDeviceDriverUrl(DeviceType.RADIO, config);
        from(JMSConstants.DIRECT_RIG_CTLD).log("log: Sending message to rigctld").doTry().inOut(nettyRotctld)
            .process(new Processor() {
                /**
                 * Designate an endpoint to which to forward the response from
                 * Hamlib. To enable forwarding, JMSConstants.HEADER_FORWARD needs
                 * to be specified as the recipientList() when sending a message to
                 * JMSConstants.DIRECT_RIG_CTLD. See other routes in this file for
                 * examples on how to do that.
                 */
                public void process(Exchange exchange) {
                    String forwardRoute = JMSConstants.DIRECT_SEND;
                    exchange.getIn().setHeader(JMSConstants.HEADER_FORWARD, forwardRoute);
                    exchange.getIn().setHeader(JMSConstants.HEADER_COMPONENT_ID, config.getComponentId());
                    exchange.getIn().setHeader(JMSConstants.HEADER_DEVICE, JMSConstants.GS_RIG_CTLD);
                }
            }).doCatch(Exception.class).process(new Processor() {
                public void process(Exchange exchange) {
                    LOG.error("Command failed to pass through - exchange properties:");

                    Map<String, Object> properties = exchange.getProperties();
                    for (String propName : properties.keySet()) {
                        LOG.error(propName + ": " + properties.get(propName));
                    }
                }
            }).end();
    }

    public static void main(String... args) {
        LOG.info("Starting radio driver");
        try {
            new Main().run();
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error(e.getMessage());
        }
    }
}
