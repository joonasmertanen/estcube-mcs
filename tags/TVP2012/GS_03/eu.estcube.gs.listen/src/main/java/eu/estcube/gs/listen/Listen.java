package eu.estcube.gs.listen;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.hbird.exchange.core.Label;

import eu.estcube.domain.TelemetryObject;

public class Listen implements MessageListener {

    private static final String BROKER_URL = "tcp://localhost:61616";

    public static void main(String[] args) {
        new Listen().run();
    }

    public void run() {
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(BROKER_URL);
        try {
            Connection connection = factory.createConnection();
            connection.start();

            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            session.createConsumer(session.createTopic("gsSend")).setMessageListener(this);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public void onMessage(Message message) {
        try {
            if (message instanceof ObjectMessage) {
                ObjectMessage obMessage = (ObjectMessage) message;
                TelemetryObject ob = (TelemetryObject) obMessage.getObject();

                System.out.println("Message received:");
                System.out.println("Device: " + ob.getDevice());
                System.out.println("Name: " + ob.getName());

                for (Label label : ob.getLabels()) {
                    System.out.println(label.getName() + ": " + label.getValue());
                }
                System.out.println();

            } else {
                System.out.println("Text message: " + message);
                System.out.println("\n");
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}