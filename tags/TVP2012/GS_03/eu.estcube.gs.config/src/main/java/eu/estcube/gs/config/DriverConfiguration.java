package eu.estcube.gs.config;

import org.springframework.beans.factory.annotation.Value;

public class DriverConfiguration {

  @Value("${device.name}")
  private String deviceName;

  @Value("${device.type}")
  private String deviceType;

  @Value("${gs.id}")
  private String groundstationId;

  @Value("${device.port}")
  private int devicePort;

  @Value("${device.host}")
  private String deviceHost;

  @Value("${timer.fire.interval}")
  private Long timerFireInterval;

  @Value("${poll.delay.interval}")
  private int pollDelayInterval;

  @Value("${poll.round.scale}")
  private int pollRoundingScale;

  @Value("${version}")
  private String driverVersion;

  public String getDeviceName() {
    return deviceName;
  }

  public String getDeviceType() {
    return deviceType;
  }

  public String getGroundstationId() {
    return groundstationId;
  }

  public int getDevicePort() {
    return devicePort;
  }

  public String getDeviceHost() {
    return deviceHost;
  }

  public Long getTimerFireInterval() {
    return timerFireInterval;
  }

  public int getPollDelayInterval() {
    return pollDelayInterval;
  }

  public int getPollRoundingScale() {
    return pollRoundingScale;
  }

  public String getDriverVersion() {
    return driverVersion;
  }

  public String getAddress() {
    return deviceHost + ":" + devicePort;
  }

  public String getComponentId() {
    return deviceType + "_" + groundstationId + "_" + deviceName;
  }

}
