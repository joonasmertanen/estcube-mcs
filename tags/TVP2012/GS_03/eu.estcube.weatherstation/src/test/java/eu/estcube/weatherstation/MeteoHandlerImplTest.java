package eu.estcube.weatherstation;

import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_PRESSURE;
import static eu.estcube.weatherstation.DefaultWeatherParameters.AIR_TEMPERATURE;
import static eu.estcube.weatherstation.DefaultWeatherParameters.GAMMA;
import static eu.estcube.weatherstation.DefaultWeatherParameters.LUX;
import static eu.estcube.weatherstation.DefaultWeatherParameters.PRECIPITATIONS;
import static eu.estcube.weatherstation.DefaultWeatherParameters.RELATIVE_HUMIDITY;
import static eu.estcube.weatherstation.DefaultWeatherParameters.SOLE;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WIND_DIRECTION;
import static eu.estcube.weatherstation.DefaultWeatherParameters.WIND_SPEED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.hbird.exchange.core.Label;
import org.hbird.exchange.core.Named;
import org.hbird.exchange.core.Parameter;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.junit.Test;
import org.xml.sax.SAXException;


public class MeteoHandlerImplTest {

    /**
     * Test method for {@link IlmPars#getResult()}.
     * 
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws JDOMException
     */
    @Test
    public final void testParameters() throws ParserConfigurationException, SAXException, IOException, JDOMException {

        SAXBuilder builder = new SAXBuilder();
        Document document = builder.build(getClass().getResourceAsStream("/meteo.physics.xml"));

        MeteoHandlerImpl test = new MeteoHandlerImpl();
        List<Named> weatherParams = test.parseDocument(document);

        assertEquals(9, weatherParams.size());

        assertNotNull(weatherParams.get(0).getTimestamp());
        assertEquals("meteo.physic.ut.ee", weatherParams.get(0).getIssuedBy());
        assertEquals("meteo.physic.ut.ee", weatherParams.get(0).getDatasetidentifier());
        assertEquals(AIR_TEMPERATURE.getName(), weatherParams.get(0).getName());
        assertEquals(AIR_TEMPERATURE.getDescription(), weatherParams.get(0).getDescription());
        assertEquals(AIR_TEMPERATURE.getType(), weatherParams.get(0).getType());
        assertEquals(AIR_TEMPERATURE.getUnit(), ((Parameter) weatherParams.get(0)).getUnit());
        assertEquals(21.6466262090116D, ((Parameter) weatherParams.get(0)).getValue());

        assertNotNull(weatherParams.get(1).getTimestamp());
        assertEquals("meteo.physic.ut.ee", weatherParams.get(1).getIssuedBy());
        assertEquals("meteo.physic.ut.ee", weatherParams.get(1).getDatasetidentifier());
        assertEquals(RELATIVE_HUMIDITY.getName(), weatherParams.get(1).getName());
        assertEquals(RELATIVE_HUMIDITY.getDescription(), weatherParams.get(1).getDescription());
        assertEquals(RELATIVE_HUMIDITY.getType(), weatherParams.get(1).getType());
        assertEquals(RELATIVE_HUMIDITY.getUnit(), ((Parameter) weatherParams.get(1)).getUnit());
        assertEquals(70.0695341688312D, ((Parameter) weatherParams.get(1)).getValue());

        assertNotNull(weatherParams.get(2).getTimestamp());
        assertEquals("meteo.physic.ut.ee", weatherParams.get(2).getIssuedBy());
        assertEquals("meteo.physic.ut.ee", weatherParams.get(2).getDatasetidentifier());
        assertEquals(AIR_PRESSURE.getName(), weatherParams.get(2).getName());
        assertEquals(AIR_PRESSURE.getDescription(), weatherParams.get(2).getDescription());
        assertEquals(AIR_PRESSURE.getType(), weatherParams.get(2).getType());
        assertEquals(AIR_PRESSURE.getUnit(), ((Parameter) weatherParams.get(2)).getUnit());
        assertEquals(1001.96090655264D, ((Parameter) weatherParams.get(2)).getValue());

        assertNotNull(weatherParams.get(3).getTimestamp());
        assertEquals("meteo.physic.ut.ee", weatherParams.get(3).getIssuedBy());
        assertEquals("meteo.physic.ut.ee", weatherParams.get(3).getDatasetidentifier());
        assertEquals(WIND_DIRECTION.getName(), weatherParams.get(3).getName());
        assertEquals(WIND_DIRECTION.getDescription(), weatherParams.get(3).getDescription());
        assertEquals(WIND_DIRECTION.getType(), weatherParams.get(3).getType());
        assertEquals("-", ((Label) weatherParams.get(3)).getValue());

        assertNotNull(weatherParams.get(4).getTimestamp());
        assertEquals("meteo.physic.ut.ee", weatherParams.get(4).getIssuedBy());
        assertEquals("meteo.physic.ut.ee", weatherParams.get(4).getDatasetidentifier());
        assertEquals(WIND_SPEED.getName(), weatherParams.get(4).getName());
        assertEquals(WIND_SPEED.getDescription(), weatherParams.get(4).getDescription());
        assertEquals(WIND_SPEED.getType(), weatherParams.get(4).getType());
        assertEquals(WIND_SPEED.getUnit(), ((Parameter) weatherParams.get(4)).getUnit());
        assertNull(((Parameter) weatherParams.get(4)).getValue());

        assertNotNull(weatherParams.get(5).getTimestamp());
        assertEquals("meteo.physic.ut.ee", weatherParams.get(5).getIssuedBy());
        assertEquals("meteo.physic.ut.ee", weatherParams.get(5).getDatasetidentifier());
        assertEquals(LUX.getName(), weatherParams.get(5).getName());
        assertEquals(LUX.getDescription(), weatherParams.get(5).getDescription());
        assertEquals(LUX.getType(), weatherParams.get(5).getType());
        assertEquals(LUX.getUnit(), ((Parameter) weatherParams.get(5)).getUnit());
        assertEquals(10056.0792521323D, ((Parameter) weatherParams.get(5)).getValue());

        assertNotNull(weatherParams.get(6).getTimestamp());
        assertEquals("meteo.physic.ut.ee", weatherParams.get(6).getIssuedBy());
        assertEquals("meteo.physic.ut.ee", weatherParams.get(6).getDatasetidentifier());
        assertEquals(SOLE.getName(), weatherParams.get(6).getName());
        assertEquals(SOLE.getDescription(), weatherParams.get(6).getDescription());
        assertEquals(SOLE.getType(), weatherParams.get(6).getType());
        assertEquals(SOLE.getUnit(), ((Parameter) weatherParams.get(6)).getUnit());
        assertNull(((Parameter) weatherParams.get(6)).getValue());

        assertNotNull(weatherParams.get(7).getTimestamp());
        assertEquals("meteo.physic.ut.ee", weatherParams.get(7).getIssuedBy());
        assertEquals("meteo.physic.ut.ee", weatherParams.get(7).getDatasetidentifier());
        assertEquals(GAMMA.getName(), weatherParams.get(7).getName());
        assertEquals(GAMMA.getDescription(), weatherParams.get(7).getDescription());
        assertEquals(GAMMA.getType(), weatherParams.get(7).getType());
        assertEquals(GAMMA.getUnit(), ((Parameter) weatherParams.get(7)).getUnit());
        assertEquals(0.0675110184290355D, ((Parameter) weatherParams.get(7)).getValue());

        assertNotNull(weatherParams.get(8).getTimestamp());
        assertEquals("meteo.physic.ut.ee", weatherParams.get(8).getIssuedBy());
        assertEquals("meteo.physic.ut.ee", weatherParams.get(8).getDatasetidentifier());
        assertEquals(PRECIPITATIONS.getName(), weatherParams.get(8).getName());
        assertEquals(PRECIPITATIONS.getDescription(), weatherParams.get(8).getDescription());
        assertEquals(PRECIPITATIONS.getType(), weatherParams.get(8).getType());
        assertEquals(PRECIPITATIONS.getUnit(), ((Parameter) weatherParams.get(8)).getUnit());
        assertEquals(0D, ((Parameter) weatherParams.get(8)).getValue());
    }
}
