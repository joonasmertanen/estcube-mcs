package eu.estcube.gs.hamlib;

import eu.estcube.gs.config.Configuration;
import eu.estcube.gs.config.RotatorConfiguration;

public class HamlibIO {

	public static String getDeviceDriverUrl(DeviceType deviceType, Configuration config) {
		
		switch (deviceType) {
			case ROTATOR: {
				RotatorConfiguration rotConfig = (RotatorConfiguration) config;
				return "netty:tcp://" + rotConfig.getRotatorAddress() 
						+ "?sync=true" // allow for responses to be forwarded
						+ "&encoders=#encoderStringToBytes"
						// the decoders are called in this order, from left to right
						+ "&decoders=#decoderSplitOnNewline,#decoderBytesToString,#decoderBufferer,#decoderStringToTelemetryObject";
			}
			case RADIO: {
				
			}
			default:
				throw new UnsupportedOperationException("The device of type '" + 
						deviceType == null ? "null" : deviceType + "' is not supported.");
		}
		
	}

}
