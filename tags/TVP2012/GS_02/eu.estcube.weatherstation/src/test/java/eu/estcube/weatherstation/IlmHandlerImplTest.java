package eu.estcube.weatherstation;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.hbird.exchange.core.Parameter;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.junit.Before;
import org.junit.Test;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

public class IlmHandlerImplTest {

  @Before
  public void setUp() throws Exception {
  }

  /**
   * Test method for {@link IlmPars#getResult()}.
   * 
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws IOException
   * @throws JDOMException
   */

  @Test
  public final void testParameters() throws ParserConfigurationException, SAXException, IOException, JDOMException {

    SAXBuilder builder = new SAXBuilder();
    Document document = builder.build(getClass().getResourceAsStream("/ilm.ee.xml"));

    IlmHandlerImpl test = new IlmHandlerImpl();
    List<Parameter> weatherParams = test.parseDocument(document);

    assertEquals(9, weatherParams.size());

    assertEquals("temp", weatherParams.get(0).getName());
    assertEquals("temp description", weatherParams.get(0).getDescription());
    assertEquals("22.0", weatherParams.get(0).getValue());
    assertEquals("", weatherParams.get(0).getUnit());
    assertEquals("ilm.ee", weatherParams.get(0).getIssuedBy());

    assertEquals("temptrend", weatherParams.get(1).getName());
    assertEquals("temptrend description", weatherParams.get(1).getDescription());
    assertEquals("up", weatherParams.get(1).getValue());
    assertEquals("", weatherParams.get(1).getUnit());
    assertEquals("ilm.ee", weatherParams.get(0).getIssuedBy());

    assertEquals("rohk", weatherParams.get(2).getName());
    assertEquals("rohk description", weatherParams.get(2).getDescription());
    assertEquals("758", weatherParams.get(2).getValue());
    assertEquals("", weatherParams.get(2).getUnit());
    assertEquals("ilm.ee", weatherParams.get(0).getIssuedBy());

    assertEquals("rohktrend", weatherParams.get(3).getName());
    assertEquals("rohktrend description", weatherParams.get(3).getDescription());
    assertEquals("down", weatherParams.get(3).getValue());
    assertEquals("", weatherParams.get(3).getUnit());
    assertEquals("ilm.ee", weatherParams.get(0).getIssuedBy());

    assertEquals("niiskus", weatherParams.get(4).getName());
    assertEquals("niiskus description", weatherParams.get(4).getDescription());
    assertEquals("67", weatherParams.get(4).getValue());
    assertEquals("", weatherParams.get(4).getUnit());
    assertEquals("ilm.ee", weatherParams.get(0).getIssuedBy());

    assertEquals("niiskustrend", weatherParams.get(5).getName());
    assertEquals("niiskustrend description", weatherParams.get(5).getDescription());
    assertEquals("up", weatherParams.get(5).getValue());
    assertEquals("", weatherParams.get(5).getUnit());
    assertEquals("ilm.ee", weatherParams.get(0).getIssuedBy());

    assertEquals("tuul", weatherParams.get(6).getName());
    assertEquals("tuul description", weatherParams.get(6).getDescription());
    assertEquals("2.2", weatherParams.get(6).getValue());
    assertEquals("", weatherParams.get(6).getUnit());
    assertEquals("ilm.ee", weatherParams.get(0).getIssuedBy());

    assertEquals("suund", weatherParams.get(7).getName());
    assertEquals("suund description", weatherParams.get(7).getDescription());
    assertEquals("S", weatherParams.get(7).getValue());
    assertEquals("", weatherParams.get(7).getUnit());
    assertEquals("ilm.ee", weatherParams.get(0).getIssuedBy());

    assertEquals("nahtus", weatherParams.get(8).getName());
    assertEquals("nahtus description", weatherParams.get(8).getDescription());
    assertEquals("vihm", weatherParams.get(8).getValue());
    assertEquals("", weatherParams.get(8).getUnit());
    assertEquals("ilm.ee", weatherParams.get(0).getIssuedBy());

  }

}
