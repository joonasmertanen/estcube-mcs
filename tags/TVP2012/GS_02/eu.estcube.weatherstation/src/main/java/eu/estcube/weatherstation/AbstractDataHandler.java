package eu.estcube.weatherstation;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.input.SAXBuilder;

import org.hbird.exchange.core.Parameter;

public abstract class AbstractDataHandler {

  protected abstract String getDataSourceName();

  protected abstract List<Parameter> parseDocument(Document document);

  private Weather weather = new Weather();

  public Weather getWeather() {
    return weather;
  }

  public List<Parameter> handle(InputStream xml) throws IOException, Exception {
    SAXBuilder builder = new SAXBuilder();
    Document doc = builder.build(xml);
    return parseDocument(doc);

  }
}
