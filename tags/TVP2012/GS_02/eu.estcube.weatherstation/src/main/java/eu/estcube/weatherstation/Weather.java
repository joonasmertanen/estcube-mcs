package eu.estcube.weatherstation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Weather {
	double temperature;
	String temperatureTrend;
	
	double airPressure;
	String airPressureTrend;
	
	double humidity;
	String humidityTrend;
	
	double windSpeed;
	String windDirection;
	
	String phenomenon;
	
	double visibility;
	
	double lightness;
	double radiation;
	
    private static final Logger LOG = LoggerFactory.getLogger(Weather.class);

	public double getLightness() {
		return lightness;
	}
	public void setLightness(String lightness) {
		try {
			this.lightness = new Double(lightness);
		} catch(NumberFormatException e) {
			LOG.warn("Lightness cannot be determined");
		}
	}
	public void setLightness(double lightness) {
		this.lightness = lightness;
	}
	public double getRadiation() {
		return radiation;
	}
	public void setRadiation(String radiation) {
		try {
			this.radiation = new Double(radiation);
		} catch(NumberFormatException e) {
			LOG.warn("Radiation cannot be determined");
		}
	}
	public void setRadiation(double radiation) {
		this.radiation = radiation;
	}
	public double getGamma() {
		return gamma;
	}
	public void setGamma(String gamma) {
		try {
			this.gamma = new Double(gamma);
		} catch(NumberFormatException e) {
			LOG.warn("Gamma cannot be determined");
		}
	}
	public void setGamma(double gamma) {
		this.gamma = gamma;
	}
	double gamma;
	
	public double getVisibility() {
		return visibility;
	}
	public void setVisibility(String visibility) {
		try {
			this.visibility = new Double(visibility);
		} catch(NumberFormatException e) {
			LOG.warn("Visibility cannot be determined");
		}
	}
	public void setVisibility(double visibility) {
		this.visibility = visibility;
	}
	public double getPrecipitations() {
		return precipitations;
	}
	public void setPrecipitations(String precipitations) {
		try {
			this.precipitations = new Double(precipitations);
		} catch(NumberFormatException e) {
			LOG.warn("Precipitations cannot be determined");
		}
	}
	public void setPrecipitations(double precipitations) {
		this.precipitations = precipitations;
	}
	double precipitations;
	
	public double getTemperature() {
		return temperature;
	}
	public void setTemperature(String temperature) {
		try {
			this.temperature = new Double(temperature);
		} catch(NumberFormatException e) {
			LOG.warn("Temperature cannot be determined");
		}
	}
	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}
	public String getTemperatureTrend() {
		return temperatureTrend;
	}
	public void setTemperatureTrend(String temperatureTrend) {
		this.temperatureTrend = temperatureTrend;
	}
	public double getAirPressure() {
		return airPressure;
	}
	public void setAirPressure(String airPressure) {
		try {
			this.airPressure = new Double(airPressure);
		} catch(NumberFormatException e) {
			LOG.warn("Air pressure cannot be determined");
		}
	}
	public void setAirPressure(double airPressure) {
		this.airPressure = airPressure;
	}
	public String getAirPressureTrend() {
		return airPressureTrend;
	}
	public void setAirPressureTrend(String airPressureTrend) {
		this.airPressureTrend = airPressureTrend;
	}
	public double getHumidity() {
		return humidity;
	}
	public void setHumidity(String humidity) {
		try {
			this.humidity = new Double(humidity);
		} catch(NumberFormatException e) {
			LOG.warn("Humidity cannot be determined");
		}
	}
	public void setHumidity(double humidity) {
		this.humidity = humidity;
	}
	public String getHumidityTrend() {
		return humidityTrend;
	}
	public void setHumidityTrend(String humidityTrend) {
		this.humidityTrend = humidityTrend;
	}
	public double getWindSpeed() {
		return windSpeed;
	}
	public void setWindSpeed(String windSpeed) {
		try {
			this.windSpeed = new Double(windSpeed);
		} catch(NumberFormatException e) {
			LOG.warn("Wind speed cannot be determined");
		}
	}
	public void setWindSpeed(double windSpeed) {
		this.windSpeed = windSpeed;
	}
	public String getWindDirection() {
		return windDirection;
	}
	public void setWindDirection(String windDirection) {
		this.windDirection = windDirection;
	}
	public String getPhenomenon() {
		return phenomenon;
	}
	public void setPhenomenon(String phenomenon) {
		this.phenomenon = phenomenon;
	}
	double windSpeedMax;
	public double getWindSpeedMax() {
		return windSpeedMax;
	}
	public void setWindSpeedMax(String windSpeedMax) {
		try {
			this.windSpeedMax = new Double(windSpeedMax);
		} catch(NumberFormatException e) {
			LOG.warn("Maximum wind speed cannot be determined");
		}
	}
	public void setWindSpeedMax(double windSpeedMax) {
		this.windSpeedMax = windSpeedMax;
	}
}
