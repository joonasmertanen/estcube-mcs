/**
 * 
 */
package eu.estcube.crowd;

import java.io.IOException;
import java.rmi.RemoteException;
import java.security.Principal;

import javax.security.auth.Subject;

import org.eclipse.jetty.security.MappedLoginService;
import org.eclipse.jetty.security.MappedLoginService.KnownUser;
import org.eclipse.jetty.security.MappedLoginService.RolePrincipal;
import org.eclipse.jetty.server.UserIdentity;
import org.eclipse.jetty.util.component.LifeCycle.Listener;
import org.eclipse.jetty.util.security.Credential;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.exception.ApplicationAccessDeniedException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;

/**
 * @author timo
 *
 */
public class CrowdLoginService extends MappedLoginService {
	
	private final SecurityServerClient crowdClient;
	private static final Logger LOG = LoggerFactory.getLogger(CrowdLoginService.class);
	
	public CrowdLoginService(SecurityServerClient crowdClient) {
		this.crowdClient = crowdClient;
	}

	@Override
	protected void doStart(){
		try {
			super.doStart();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}
	
	// TODO
	private void setFailed(Throwable th) {
		for (Listener listener : _listeners)
            listener.lifeCycleFailure(this, th);
	}

	@Override
	// Loads user information from crowd
	protected UserIdentity loadUser(String username) {
			// If user was found, finds the groups it belongs to and creates a user identity using putUser method
			try {
				String[] groups = crowdClient.findGroupMemberships(username);
				return putUser(username, null, groups);
			} catch (UserNotFoundException e) {
				// TODO Auto-generated catch block
				LOG.info("UserNotFoundException {}", e.getCause());
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				LOG.info("RemoteException {}", e.getCause());
			} catch (InvalidAuthenticationException e) {
				// TODO Auto-generated catch block
				LOG.info("InvalidAuthenticationException {}", e.getCause());
			} catch (InvalidAuthorizationTokenException e) {
				// TODO Auto-generated catch block
				LOG.info("InvalidAuthorizationTokenException {}", e.getCause());
			}
			
		// Returns null if an exception is caught
		return null;
	}

	@Override
	// Creates a useridentity for the user
	public synchronized UserIdentity putUser(String userName, Credential credential, String[] roles)
    {
        Principal userPrincipal = new CrowdUser(userName);
        Subject subject = new Subject();
        subject.getPrincipals().add(userPrincipal);
        subject.getPrivateCredentials().add(credential);
        
        if (roles != null)
            for (String role : roles)
                subject.getPrincipals().add(new RolePrincipal(role));

        subject.setReadOnly();
        UserIdentity identity =_identityService.newUserIdentity(subject,userPrincipal,roles);
        _users.put(userName,identity);
        return identity;
    }


	@Override
	protected void loadUsers() throws IOException {
		// Unimportant
	}
	
	class CrowdUser extends KnownUser {
		
		private String crowdToken;
		
		CrowdUser (String username) {
			super(username, null);
		}

		
		@Override
		/* Tries to authenticate the user with the given credentials. In case of success, returns true. If
		   authentication fails or a an exception is caught, returns null*/
		public boolean authenticate(Object credentials) {
				try {
					crowdToken = crowdClient.authenticatePrincipalSimple(getName(), String.valueOf(credentials));
					return isAuthenticated();
				} catch (InactiveAccountException e) {
					// TODO Auto-generated catch block
					LOG.info("InactiveAccountException {}", e.getCause());
					return false;
				} catch (ExpiredCredentialException e) {
					// TODO Auto-generated catch block
					LOG.info("ExpiredCredentialException {}", e.getCause());
					return false;
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					LOG.info("RemoteException {}", e.getCause());
					return false;
				} catch (InvalidAuthenticationException e) {
					LOG.info("Username used for login:" + getName());
					LOG.info("CrowdUser InvalidAuthenticationException {}", e.getCause().getMessage());
					setFailed(e);
					return false;
				} catch (InvalidAuthorizationTokenException e) {
					// TODO Auto-generated catch block
					LOG.info("InvalidAuthorizationTokenException {}", e.getCause());
					return false;
				} catch (ApplicationAccessDeniedException e) {
					// TODO Auto-generated catch block
					LOG.info("ApplicationAccessDeniedException {}", e.getCause());
					return false;
				}
				
				
		}

		@Override
		public boolean isAuthenticated() {
			return crowdToken != null;
		}
		
		
	}
}
