package eu.estcube.webserver;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.eclipse.jetty.util.log.Log;
import org.hbird.exchange.core.Parameter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryParameter;


public class CacheMessageTest {

    private CacheMessage cacheMessage;
    private String source = "ES5EC";
    private String device = "rotator";
    private TelemetryObject telemetryObject;
    private TelemetryObject telemetryObject2;
    private TelemetryObject telemetryObjectNewer;
    private TelemetryObject telemetryObjectOlder;
    private String telObjName = "telObjName";
    private String telObjName2 = "telObjName2";
    private Parameter telemetryParameter;
    private Parameter telemetryParameter2;
    private String telParName = "telParName";
    private String telParName2 = "telParName2";   
    private String value = "123"; 
    private Double value2 = new Double(2.3334);
    private Exchange ex;
    private Message message;
    private Object answer;
    private HashMap<String,String> key1;
    private HashMap<String,String> key2;
    
    @Before
    public void setUp() throws Exception {
        CacheMessage.setCacheLimit(10);
        cacheMessage = new CacheMessage();
        Calendar calendar = Calendar.getInstance();
        calendar.set(2011, 10, 31, 15, 30); 
        Date date1 = calendar.getTime();
        calendar.set(2011, 10, 31, 15, 32);
        Date date2 = calendar.getTime();
        calendar.set(2011, 10, 31, 11, 45);
        Date date3 = calendar.getTime();
        calendar.set(2011, 10, 31, 16, 45);
        Date date4 = calendar.getTime();
        
        telemetryObject = new TelemetryObject(telObjName, date1); 
        telemetryObject.setSource(source);
        telemetryObject.setDevice(device);
        telemetryObject2 = new TelemetryObject(telObjName2, date2);
        telemetryObject2.setSource(source);
        telemetryObject2.setDevice(device);
        telemetryObjectOlder = new TelemetryObject(telObjName, date3);
        telemetryObjectNewer = new TelemetryObject(telObjName, date4);          
        telemetryObject.setSource(source);
        telemetryObject.setDevice(device);
        
        telemetryParameter = new TelemetryParameter(telParName, value);
        telemetryParameter2 = new TelemetryParameter(telParName2, value2);
        
        telemetryObject.addParameter(telemetryParameter);
        telemetryObject.addParameter(telemetryParameter2);
        telemetryObject2.addParameter(telemetryParameter2);
        
        ex = Mockito.mock(Exchange.class);
        message = Mockito.mock(Message.class);
        
        Mockito.when(ex.getIn()).thenReturn(message);
        Mockito.when(ex.getOut()).thenReturn(message);
        
        Mockito.doAnswer(new Answer<Object>() {
            public Object answer(InvocationOnMock invocation) {
                answer = invocation.getArguments()[0];
                return answer;
            }
        }).when(message).setBody(Mockito.any()); 
        
        key1 = new HashMap<String,String>();
        key1.put("SOURCE", telemetryObject.getSource());
        key1.put("DEVICE", telemetryObject.getDevice());
        key1.put("NAME", telemetryObject.getName());
        key2 = new HashMap<String,String>();
        key2.put("SOURCE", telemetryObject2.getSource());
        key2.put("DEVICE", telemetryObject2.getDevice());
        key2.put("NAME", telemetryObject2.getName());
    }
    
    @SuppressWarnings("unchecked")
	@Test
    public void testAddToCache() {
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObject);       
        cacheMessage.addToCache(ex);  
        cacheMessage.getCachedElement(ex, key1);

        assertEquals(telemetryObject, ((ArrayList<TelemetryObject>) answer).get(0)); 
        assertEquals(telemetryParameter, ((ArrayList<TelemetryObject>) answer).get(0).getParameter(telParName));
    }

    @SuppressWarnings("unchecked")
	@Test
    public void testGetCachedElement() {
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObject);     
        cacheMessage.addToCache(ex);
        cacheMessage.getCachedElement(ex, key1); 
        
        assertEquals(telemetryObject, ((ArrayList<TelemetryObject>) answer).get(0));
        assertEquals(telemetryParameter, ((ArrayList<TelemetryObject>) answer).get(0).getParameter(telParName));
        assertEquals(telemetryParameter2, ((ArrayList<TelemetryObject>) answer).get(0).getParameter(telParName2));
        
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObject2);   
        cacheMessage.addToCache(ex);
        cacheMessage.getCachedElement(ex, key2);
        
        assertEquals(telemetryObject2, ((ArrayList<TelemetryObject>) answer).get(0));
        assertEquals(telemetryParameter2, ((ArrayList<TelemetryObject>) answer).get(0).getParameter(telParName2));
    }

    @Test
    public void testGetCache() {
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObject);
        cacheMessage.addToCache(ex);
      
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObject2);   
        cacheMessage.addToCache(ex);
        
        cacheMessage.getCache(ex);
        
        assertTrue(((ArrayList)answer).contains(telemetryObject));
        assertTrue(((ArrayList)answer).contains(telemetryObject2));

    }
    
    @Test
    public void testAddNewer() {
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObjectOlder);
        cacheMessage.addToCache(ex);
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObjectNewer);
        cacheMessage.addToCache(ex);
        
        cacheMessage.getCache(ex);
        
        /*Answer contains both older and newer version. */
        assertTrue(((ArrayList)answer).contains(telemetryObjectOlder));
        assertTrue(((ArrayList)answer).contains(telemetryObjectNewer));
        /*Newer version is before the older version in the cache list. */
        assertTrue(((ArrayList)answer).indexOf(telemetryObjectNewer) > ((ArrayList)answer).indexOf(telemetryObjectOlder));
    }
    
    @Test
    public void testRemoveOldEntries() {
        /*Cache limit set to 4.*/
        CacheMessage.setCacheLimit(4);
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObjectOlder);
        cacheMessage.addToCache(ex);
        
        /*Insert 4 newer objects of the same kind, old one is pushed out.*/
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObjectNewer);
        cacheMessage.addToCache(ex);
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObjectNewer);
        cacheMessage.addToCache(ex);
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObjectNewer);
        cacheMessage.addToCache(ex);
        
        /*Answer still contains older version of the object, 
         * it is located before the newer versions in the list. */
        cacheMessage.getCache(ex); 
        assertTrue(((ArrayList)answer).contains(telemetryObjectOlder));
        assertTrue(((ArrayList)answer).contains(telemetryObjectNewer));
        System.out.println(((ArrayList)answer).indexOf(telemetryObjectNewer));
        System.out.println(((ArrayList)answer).indexOf(telemetryObjectOlder));
        assertTrue(((ArrayList)answer).indexOf(telemetryObjectNewer) > ((ArrayList)answer).lastIndexOf(telemetryObjectOlder));
        
        /*Now older version is pushed out.*/
        Mockito.when(message.getBody(TelemetryObject.class)).thenReturn(telemetryObjectNewer);
        cacheMessage.addToCache(ex);
        
        cacheMessage.getCache(ex);     
        /*Answer contains only newer versions. */
        assertTrue(!((ArrayList)answer).contains(telemetryObjectOlder));
        assertTrue(((ArrayList)answer).contains(telemetryObjectNewer));
    }
}
