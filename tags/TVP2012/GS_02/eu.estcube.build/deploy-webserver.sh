#!/bin/sh

kill $(ps -u mcs-test -ef | grep java | grep eu.estcube.webserver | awk '{print $2}')
cd /home/mcs-test/estcube/
for z in ./*.zip; do unzip -o -qq $z; done
cd /home/mcs-test/estcube/webserver/bin
touch webServer.txt
chmod +x WebServer
./WebServer > webServer.txt &

