/**
 * 
 */
package eu.estcube.domain;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.hbird.exchange.core.Parameter;
import org.junit.Before;
import org.junit.Test;


public class CommandTest {

    private static final String NAME = "A";
    private static final List<Parameter> PARAMS = new ArrayList<Parameter>();
    private String name = "name";
    private Object value = "value";

    private TelemetryCommand tc, tc2;
    
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        tc = new TelemetryCommand(NAME);
        tc2 = new TelemetryCommand(NAME, PARAMS);
    }

    @Test
    public void testTelemetryCommandString() {
        assertNotNull(tc);
        assertNotNull(tc.getArguments());
        assertEquals(0, tc.getArguments().size());
        assertEquals(NAME, tc.getCommandName());
        assertNotNull(tc.toString());
    }

    @Test
    public void testTelemetryCommandStringMapOfStringObject() {
        assertNotNull(tc2);
        assertNotNull(tc2.getArguments());
        assertEquals(0, tc2.getArguments().size());
        assertEquals(NAME, tc2.getCommandName());
        assertNotNull(tc2.toString());
    }

    @Test
    public void testGetName() {
        assertEquals(NAME, tc.getCommandName());
    }

    @Test
    public void testgetArguments() {
        assertEquals(PARAMS, tc.getArguments());
    }

    @Test
    public void testAddParameter() {
        tc.addParameter(new TelemetryParameter("a", "b"));
        assertEquals("b", tc.getParameter("a"));
    }

    @Test
    public void testGetParameter() {
        tc.addParameter(new TelemetryParameter(name, value));
        assertEquals(value, tc.getParameter(name));
    }

    @Test
    public void getParameters() {
        tc.addParameter(new TelemetryParameter(name, value));
        tc.addParameter(new TelemetryParameter("a", "b"));
        List<Parameter> params = tc.getArguments();
        assertEquals(2, params.size());
        
        Parameter parToGet = null;
        for (Parameter par : params){
        	if (par.getName().equals(name)) {
        		parToGet = par;
        		break;
        	}
        }
        assertEquals(value, parToGet.getValue());
        
        for (Parameter par : params){
        	if (par.getName().equals("a")) {
        		parToGet = par;
        		break;
        	}
        }
        assertEquals("b", parToGet.getValue());      
    }
}
