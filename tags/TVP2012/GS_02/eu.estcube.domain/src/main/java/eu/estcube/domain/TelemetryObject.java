/**
 * 
 */
package eu.estcube.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hbird.exchange.core.Parameter;

public class TelemetryObject implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2092380317315772625L;
    private final String name;
    private String source;
    private String device;
    private Date time;
    private List<Parameter> params;

    /**
     * Creates new TelemetryObject, with only a name.
     * 
     * @param name
     */
    public TelemetryObject(String name, Date time) {
        this.name = name;
        this.time = time;
        params = new ArrayList<Parameter>();
    }

    /**
     * Creates new TelemetryObject.
     * 
     * @param name
     * @param params
     */
    public TelemetryObject(String name, List<Parameter> params) {
        this.name = name;
        this.params = params;
        this.time = new Date();
    }

    /**
     * Returns name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns parameters.
     * 
     * @return the params
     */
    public List<Parameter> getParams() {
        return params;
    }

    /**
     * Returns parameters.
     * 
     * @return a params
     */
    public Parameter getParameter(String name) {
        for (Parameter param : params) {
            if (param.getName().equals(name)) {
                return param;
            }
        }
        return null;
    }

    /**
     * Adds a list of parameters
     * 
     * @param params
     *            the parameters to add
     */
    public void addParams(List<Parameter> params) {
        this.params.addAll(params);
    }

    /**
     * Adds a parameter
     * 
     * @param param
     *            the parameter to add
     */
    public void addParameter(Parameter param) {
        this.params.add(param);
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String toString() {
        String result = "";
        result = result + this.name;
        result = result + "\n";

        result = result + this.source;
        result = result + "\n";
        result = result + this.device;
        result = result + "\n";

        result = result + this.time.toString();
        result = result + "\n";

        for (Parameter param : this.params)
            result = result + param.toString();
        result = result + "\n";
        result = result + "\n";

        return result;

    }

}
