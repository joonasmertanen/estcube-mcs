package eu.estcube.gs.rotator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.camel.Message;
import org.apache.camel.impl.DefaultMessage;
import org.springframework.stereotype.Component;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.domain.TelemetryRotatorConstants;

@Component
public class SplitterRotatorStatus {
    // TODO: what does this class do? do we still need it?
    
    private List<String> commands;

    public SplitterRotatorStatus() {
        this.commands = Arrays.asList(TelemetryRotatorConstants.GET_POSITION);
    }

    public List<Message> splitMessage() {
        final List<Message> answer = new ArrayList<Message>();

        for (String command : commands) {
            DefaultMessage message = new DefaultMessage();
            message.setBody(new TelemetryCommand(command));
            answer.add(message);
        }
        return answer;
    }
}