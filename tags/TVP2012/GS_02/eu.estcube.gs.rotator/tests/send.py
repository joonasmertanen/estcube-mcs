import time
import sys
import logging
import stomp



class MyListener(object):
    def on_error(self, headers, message):
        print('received an error %s' % message)

    def on_message(self, headers, message):
        print('received a message %s' % message)

logging.basicConfig()
conn = stomp.Connection([('localhost', 61613)])
conn.set_listener('somename', MyListener())
#conn.add_listener(MyListener())
conn.start()

#conn.subscribe(destination='/queue/test', ack='auto')

#conn.send('set_pos 10 10', destination='/topic/gsReceive', 
#        headers={
#             'groundStationID':'ES5EC',
#             'device':'rotctld',           
#        })

if __name__ == '__main__':
    conn.connect()
    headers = {
             'groundStationID':'ES5EC',
             'device':'rotctld',
	     'type':'textMessage','MessageNumber':78           
            }
    conn.send(message=' '.join(sys.argv[1:]), destination='/topic/gsReceive', 
        headers=headers, ack="auto")
    conn.disconnect()
