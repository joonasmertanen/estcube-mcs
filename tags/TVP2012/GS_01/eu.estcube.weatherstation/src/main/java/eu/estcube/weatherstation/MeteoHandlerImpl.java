package eu.estcube.weatherstation;

import java.util.ArrayList;
import java.util.List;

import org.hbird.exchange.core.Parameter;
import org.jdom2.Document;
import org.jdom2.Element;

public class MeteoHandlerImpl extends AbstractDataHandler {

	protected String getDataSourceName() {
		return "meteo.physic.ut.ee";
	}

	protected List<Parameter> parseDocument(Document document) {
		List<Parameter> weatherParams = new ArrayList <Parameter>();
		Element rootNode = document.getRootElement();
		List<Element> list = rootNode.getChildren("data");
		
		for(int u = 0; u < list.size(); u++){
			Element node = (Element) list.get(u);
			String attribute = node.getAttribute("id").getValue();
			
			if(attribute.equals("temp")){
				weatherParams.add(new Parameter(getDataSourceName(),"temp","temp description",node.getText(),""));
			}
			else if(attribute.equals("humid")){
				weatherParams.add(new Parameter(getDataSourceName(),"humid","humid description",node.getText(),""));
			}
			else if(attribute.equals("wind_dir")){
				weatherParams.add(new Parameter(getDataSourceName(),"wind_dir","wind_dir description",node.getText(),""));
			}
			else if(attribute.equals("wind_len")){
				weatherParams.add(new Parameter(getDataSourceName(),"wind_len","wind_len description",node.getText(),""));
			}
			else if(attribute.equals("lux")){
				weatherParams.add(new Parameter(getDataSourceName(),"lux","lux description",node.getText(),""));
			}
			else if(attribute.equals("sole")){
				weatherParams.add(new Parameter(getDataSourceName(),"sole","sole description",node.getText(),""));
			}
			else if(attribute.equals("gamma")){
				weatherParams.add(new Parameter(getDataSourceName(),"gamma","gamma description",node.getText(),""));
			}
			else if(attribute.equals("precip")){
				weatherParams.add(new Parameter(getDataSourceName(),"precip","precip description",node.getText(),""));
			}
		}
		// TODO import data to weatherParams
		return weatherParams;
	}
}
