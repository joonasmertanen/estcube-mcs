package eu.estcube.weatherstation;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.hbird.exchange.core.Parameter;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.junit.Before;
import org.junit.Test;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import eu.estcube.weatherstation.EmhiHandlerImpl;

public class EmhiHandlerImplTest {

  @Before
  public void setUp() throws Exception {
  }

  /**
   * Test method for {@link IlmPars#getResult()}.
   * 
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws IOException
   * @throws JDOMException
   */

  @Test
  public final void testParameters() throws ParserConfigurationException, SAXException, IOException, JDOMException {

    SAXBuilder builder = new SAXBuilder();
    Document document = builder.build(getClass().getResourceAsStream("/EMHI.xml"));
    EmhiHandlerImpl test = new EmhiHandlerImpl();
    List<Parameter> weatherParams = test.parseDocument(document);

    assertEquals(9, weatherParams.size());

    assertEquals("emhi.ee", weatherParams.get(0).getIssuedBy());
    assertEquals("phenomen", weatherParams.get(0).getName());
    assertEquals("phenomen description", weatherParams.get(0).getDescription());
    assertEquals("Light shower", weatherParams.get(0).getValue());
    assertEquals("", weatherParams.get(0).getUnit());

    assertEquals("emhi.ee", weatherParams.get(0).getIssuedBy());
    assertEquals("visibility", weatherParams.get(1).getName());
    assertEquals("visibility description", weatherParams.get(1).getDescription());
    assertEquals("26.0", weatherParams.get(1).getValue());
    assertEquals("", weatherParams.get(1).getUnit());

    assertEquals("emhi.ee", weatherParams.get(0).getIssuedBy());
    assertEquals("precipitations", weatherParams.get(2).getName());
    assertEquals("precipitations description", weatherParams.get(2).getDescription());
    assertEquals("0.3", weatherParams.get(2).getValue());
    assertEquals("", weatherParams.get(2).getUnit());

    assertEquals("emhi.ee", weatherParams.get(0).getIssuedBy());
    assertEquals("airpressure", weatherParams.get(3).getName());
    assertEquals("airpressure description", weatherParams.get(3).getDescription());
    assertEquals("1006.4", weatherParams.get(3).getValue());
    assertEquals("", weatherParams.get(3).getUnit());

    assertEquals("emhi.ee", weatherParams.get(0).getIssuedBy());
    assertEquals("relativehumidity", weatherParams.get(4).getName());
    assertEquals("relativehumidity description", weatherParams.get(4).getDescription());
    assertEquals("77", weatherParams.get(4).getValue());
    assertEquals("", weatherParams.get(4).getUnit());

    assertEquals("emhi.ee", weatherParams.get(0).getIssuedBy());
    assertEquals("airtemperature", weatherParams.get(5).getName());
    assertEquals("airtemperature description", weatherParams.get(5).getDescription());
    assertEquals("16.5", weatherParams.get(5).getValue());
    assertEquals("", weatherParams.get(5).getUnit());

    assertEquals("emhi.ee", weatherParams.get(0).getIssuedBy());
    assertEquals("winddirection", weatherParams.get(6).getName());
    assertEquals("winddirection description", weatherParams.get(6).getDescription());
    assertEquals("223", weatherParams.get(6).getValue());
    assertEquals("", weatherParams.get(6).getUnit());

    assertEquals("emhi.ee", weatherParams.get(0).getIssuedBy());
    assertEquals("windspeed", weatherParams.get(7).getName());
    assertEquals("windspeed description", weatherParams.get(7).getDescription());
    assertEquals("2.6", weatherParams.get(7).getValue());
    assertEquals("", weatherParams.get(7).getUnit());

    assertEquals("emhi.ee", weatherParams.get(0).getIssuedBy());
    assertEquals("windspeedmax", weatherParams.get(8).getName());
    assertEquals("windspeedmax description", weatherParams.get(8).getDescription());
    assertEquals("5.7", weatherParams.get(8).getValue());
    assertEquals("", weatherParams.get(8).getUnit());

  }

}
