package eu.estcube.parameterstorage;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.hbird.exchange.core.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;

import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryParameter;

public class DbToTelemetryObject implements Processor {
    @Value("${maxObjectsReturned}")
    private int returnLimit;
    private static final Logger LOG = LoggerFactory.getLogger(DbToTelemetryObject.class);
    private JdbcTemplate jdbcTemplate;
    private Integer id;
    private List<TelemetryObject> list;

    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void process(Exchange ex) throws Exception {
        int rowCount = jdbcTemplate.queryForInt("select count(0) from TELEMETRYOBJECTS");

        if (rowCount > 0) {
            List<Map<String, Object>> distinctObjectsByGs = (List<Map<String, Object>>) jdbcTemplate
                    .queryForList("SELECT DISTINCT NAME, SOURCE, DEVICE FROM TELEMETRYOBJECTS;");

            ArrayList<TelemetryObject> objects = new ArrayList<TelemetryObject>();
            for (Map<String, Object> row : distinctObjectsByGs) {

                List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT * FROM ("
                        + "SELECT TOP ? * FROM TELEMETRYOBJECTS WHERE NAME =? AND SOURCE = ? AND DEVICE=?"
                        + "ORDER BY TIME DESC)" + "ORDER BY TIME ASC;",
                        new Object[] { returnLimit, row.get("NAME"), row.get("SOURCE"), row.get("DEVICE") });

                for (Map<String, Object> rown : rows) {
                    Timestamp ts = (Timestamp) (rown.get("TIME"));
                    Date date = new java.util.Date(ts.getTime());
                    TelemetryObject telObj = new TelemetryObject((String) rown.get("NAME"), date);
                    telObj.setSource((String) rown.get("SOURCE"));
                    telObj.setDevice((String) rown.get("DEVICE"));
                    id = (Integer) rown.get("ID");

                    String sql = "SELECT * FROM PARAMETERS WHERE ID=?";
                    List<Parameter> params = new ArrayList<Parameter>();
                    List<Map<String, Object>> paramRows = jdbcTemplate.queryForList(sql, id);
                    for (Map<String, Object> paramRow : paramRows) {
                        Parameter parameter = new TelemetryParameter((String) paramRow.get("KEY"), (String) paramRow.get("VALUE"));
                        params.add(parameter);
                    }
                    telObj.addParams(new ArrayList<Parameter>(params));
                    objects.add(telObj);
                }
            }
            ex.getOut().setBody(new ArrayList<TelemetryObject>(objects));
            ex.getOut().setHeader("fromPS", "true");
            objects.clear();
        } else {
            // nothing to return from DB
            ex.getOut().setBody(new ArrayList<TelemetryObject>());
            ex.getOut().setHeader("fromPS", "true");
        }
    }
}
