package eu.estcube.parameterstorage;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import eu.estcube.domain.JMSConstants;
import eu.estcube.domain.TelemetryCommand;

public class ParameterStorage extends RouteBuilder {
    
    private static final Logger LOG = LoggerFactory.getLogger(ParameterStorage.class);
    
    @Value("${version}")
    private String parameterStorageVersion;
    
    @Autowired
    private TelemetryObjectToDb telObjToDb;
    
    @Autowired
    private DbToTelemetryObject dbToTelObj;

    @Override
    public void configure() throws Exception {

        from("timer://status?repeatCount=1")
        .process(new Processor() {
            public void process(Exchange exchange) throws Exception {
                exchange.getIn().setHeader("groundStationID", "*"); //wildcard GS ID
                exchange.getIn().setHeader("device", "status");
                TelemetryCommand getStatus = new TelemetryCommand("GET_STATUS");
                exchange.getIn().setBody(getStatus, TelemetryCommand.class);
            }
         })
         .to(JMSConstants.AMQ_GS_RECEIVE).to("log:echo"); 
        
        from(JMSConstants.AMQ_GS_SEND).to("log:echo")
            .process(new Processor() {

                public void process(Exchange ex) throws Exception {
                    boolean fromPs = false;
                    try {
                        fromPs = ex.getIn().getHeader(JMSConstants.HEADER_FROM_PS, Boolean.class);
                    } catch (Exception e) {
                        fromPs = false;
                    }
                    if(!fromPs) {
                        telObjToDb.process(ex);
                    }
                }         
            });
                
        from(JMSConstants.AMQ_PS_QUEUE)
            .process(dbToTelObj)
                .split().method("telemetryObjectSplitter", "splitMessage")
                    .process(new Processor() {
                        public void process(Exchange exchange) throws Exception {
                            exchange.getIn().setHeader(JMSConstants.HEADER_FROM_PS, true);
                        }
                    })
                    .to(JMSConstants.AMQ_GS_SEND);
        
        from(JMSConstants.AMQ_VERSIONS_REQUEST).process(new Processor() {
            public void process(Exchange ex) throws Exception {
                ex.getOut().setHeader(JMSConstants.HEADER_COMPONENT, JMSConstants.COMPONENT_PARAMETER_STORAGE);
                ex.getOut().setBody(parameterStorageVersion, String.class);          
            }            
        }).to(JMSConstants.AMQ_VERSIONS_RECEIVE);
        
        from("timer://sendVersion?repeatCount=1").process(new Processor() {
            public void process(Exchange ex) throws Exception {
                ex.getOut().setHeader(JMSConstants.HEADER_COMPONENT, JMSConstants.COMPONENT_PARAMETER_STORAGE);
                ex.getOut().setBody(parameterStorageVersion, String.class);          
            }            
        }).to(JMSConstants.AMQ_VERSIONS_RECEIVE);
           
    }
    
    public static void main(String[] args) throws Exception { 
        LOG.info("Starting Parameter storage");
        new Main().run(args);
    }
}
