package eu.estcube.gs_rotator;

import java.io.IOException;
import java.nio.channels.ClosedChannelException;

import org.apache.camel.CamelExchangeException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import eu.estcube.domain.JMSConstants;

public class Rotator extends RouteBuilder {

    private static final String CAPABILITIES_COMMAND = "1";
    private static final Logger LOG = LoggerFactory.getLogger(Rotator.class);

    @Autowired
    private SplitterRotatorStatus rotatorStatusSplitter;

    @Value("#{config}")
    private Configuration config;

    private boolean online = true;

    @SuppressWarnings("unchecked")
    @Override
    public void configure() {
        // Incoming from AMQ
        from(JMSConstants.AMQ_GS_RECEIVE).log("log:RecievedMessage").choice()
            .when(header(JMSConstants.HEADER_GROUNDSTATIONID).isEqualTo(config.getGroundStationName()))
            .to(JMSConstants.DIRECT_ROT_CTLD)
            .when(header(JMSConstants.HEADER_GROUNDSTATIONID).isEqualTo(JMSConstants.GS_ALL_NAMES))
            .to(JMSConstants.DIRECT_ROT_CTLD).otherwise().log("log:WrongStation");

        // Outgoing to AMQ
        from(JMSConstants.DIRECT_SEND).log("log:SendMessage").to(JMSConstants.AMQ_GS_SEND);

        // Outgoing to netty
        String nettyRotCtld = "netty:" + config.getRotatorUrl();
        from(JMSConstants.DIRECT_ROT_CTLD).log("log:DebugRotDeviceBeforeResponse").doTry().to(nettyRotCtld)
            .process(new Processor() {
                public void process(Exchange exchange) {
                    String forwardRoute = JMSConstants.DIRECT_SEND;
                    exchange.getIn().setHeader(JMSConstants.HEADER_GROUNDSTATIONID, config.getGroundStationName());
                    exchange.getIn().setHeader(JMSConstants.HEADER_DEVICE, JMSConstants.GS_ROT_CTLD);
                    if (online) {
                        forwardRoute += "," + JMSConstants.DIRECT_ROT_STATUS;
                        online = false;
                    }
                    exchange.getIn().setHeader(JMSConstants.HEADER_FORWARD, forwardRoute);
                }
            }).doCatch(IOException.class, Exception.class, CamelExchangeException.class, ClosedChannelException.class)
            .process(new Processor() {
                public void process(Exchange exchange) {
                    online = true;
                }
            }).end();

        // Periodically check if rotator online and get capabilities
        from("timer://rotTimer?period=" + config.getTimerFireInterval()).process(new Processor() {
            public void process(Exchange exchange) {
                exchange.getIn().setBody(CAPABILITIES_COMMAND);
            }
        }).log("log:DebugRotPeriodicalTimeFire").doTry().to(JMSConstants.DIRECT_ROT_CTLD)
            .recipientList(header(JMSConstants.HEADER_FORWARD)).endDoTry()
            .doCatch(IOException.class, Exception.class, CamelExchangeException.class, ClosedChannelException.class)
            .log("Timerfire failed, probably channel closed").end();
    }

    public static void main(String[] args) throws Exception {
        LOG.info("Starting driver");
        new Main().run(args);
    }
}
