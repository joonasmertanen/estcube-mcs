package eu.estcube.gs_rotator;

import java.util.Map;

import org.apache.camel.Endpoint;
import org.apache.camel.impl.DefaultComponent;
import org.springframework.beans.factory.annotation.Value;

import eu.estcube.domain.JMSConstants;

public class Configuration extends DefaultComponent {

    public String getJmsUrl() {
        return jmsUrl;
    }

    public int getRotatorPort() {
        return rotatorPort;
    }

    public String getRotatorHost() {
        return rotatorHost;
    }

    public String getRotatorAddress() {
        return rotatorAddress;
    }

    public String getRotatorUrl() {
        return "tcp://" + getRotatorAddress()
            + "?sync=true&decoders=#" + "rotatorDecoderNewLine"
            + ",#" + "rotatorDecoderToString"
            + ",#" + "rotatorDecoderHamlib"
            + ",#" + JMSConstants.GS_ROTATOR_PROTOCOL_DECODER
            + "&encoders=#" + JMSConstants.GS_ENCODE_STR_TO_BYTE
            + ",#" + JMSConstants.GS_ROTATOR_PROTOCOL_ENCODER;
    }

    public String getGroundStationName() {
        return groundStationName;
    }

    public String getTimerFireInterval() {
        return timerFireInterval;
    }

    public int getPollDelayInterval() {
        return pollDelayInterval;
    }

    public int getPollRoundingScale() {
        return pollRoundingScale;
    }

    public String getDriverVersion() {
        return driverVersion;
    }

    @Override
    protected Endpoint createEndpoint(String arg0, String arg1, Map<String, Object> arg2) throws Exception {
        return null;
    }

    @Value("${jms.url}")
    private String jmsUrl;

    @Value("${rotctldPort}")
    private int rotatorPort;

    @Value("${rotctldHost}")
    private String rotatorHost;

    @Value("${rotctldAddress}")
    private String rotatorAddress;

    @Value("${gsName}")
    private String groundStationName;

    @Value("${timerFireInterval}")
    private String timerFireInterval;

    @Value("${pollDelayInterval}")
    private int pollDelayInterval;

    @Value("${pollRoundScale}")
    private int pollRoundingScale;

    @Value("${version}")
    private String driverVersion;
}
