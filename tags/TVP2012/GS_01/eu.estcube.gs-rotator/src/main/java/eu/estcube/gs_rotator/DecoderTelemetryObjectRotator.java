package eu.estcube.gs_rotator;

import java.util.Date;
import java.util.Map;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.estcube.domain.JMSConstants;
import eu.estcube.domain.TelemetryObject;
import eu.estcube.domain.TelemetryParameter;

@Component(JMSConstants.GS_ROTATOR_PROTOCOL_DECODER)
public class DecoderTelemetryObjectRotator extends OneToOneDecoder {

    private static final Logger LOG = LoggerFactory.getLogger(DecoderTelemetryObjectRotator.class);

    private Map<String, String> hamlibToProtocolMap;

    private Map<String, String> getHamlibProtocolMap() {
        return hamlibToProtocolMap;
    }

    @Value("#{config}")
    private Configuration config;

    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, Object message) {

        String[] messageSplit = message.toString().split("\n");
        TelemetryObject telemetryObject;
        LOG.debug("Message: {}", message.toString());

        String[] name = messageSplit[0].split(":");
        telemetryObject = new TelemetryObject(hamlibToProtocol(name[0]), new Date());
        addParametersToTelemetryObject(messageSplit, telemetryObject);

        telemetryObject.setSource(config.getGroundStationName());
        telemetryObject.setDevice(JMSConstants.GS_ROT_CTLD);
        return telemetryObject;
    }

    private void addParametersToTelemetryObject(String[] messageSplit, TelemetryObject telemetryObject) {
        String[] messagePiece = { null, null };

        for (int i = 0; i < messageSplit.length; i++) {
            messageSplit[i] = messageSplit[i].replace(":\t\t", ":");
            messageSplit[i] = messageSplit[i].replace(":\t", ":");
            messageSplit[i] = messageSplit[i].replace(": ", ":");
            if (messageSplit[i].contains(":")) {
                messagePiece = messageSplit[i].split(":");
            } else if (messageSplit[i].contains(" ")) {
                messagePiece = messageSplit[i].split(" ");
            } else {
                continue;
            }
            messagePiece[0] = hamlibToProtocol(messagePiece[0]);
            if (messagePiece.length == 1) {
                telemetryObject.addParameter(new TelemetryParameter(messagePiece[0], ""));
                continue;
            }
            telemetryObject.addParameter(new TelemetryParameter(messagePiece[0], messagePiece[1]));
        }
    }

    private String hamlibToProtocol(String hamlibName) {
        String protocolName = getHamlibProtocolMap().get(hamlibName);
        if (protocolName != null) {
            return protocolName;
        }
        return hamlibName;
    }

}
