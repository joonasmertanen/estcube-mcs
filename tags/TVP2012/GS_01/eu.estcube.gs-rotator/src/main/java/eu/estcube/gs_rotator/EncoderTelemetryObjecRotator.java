package eu.estcube.gs_rotator;

import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;
import org.springframework.stereotype.Component;

import eu.estcube.domain.JMSConstants;

@Component(JMSConstants.GS_ROTATOR_PROTOCOL_ENCODER)
public class EncoderTelemetryObjecRotator extends OneToOneEncoder {

    protected Object encode(ChannelHandlerContext ctx, Channel channel, Object message) throws ClassCastException {
        if (message == null) {
            return ChannelBuffers.EMPTY_BUFFER;
        }
        return (String) message;
    }

}
