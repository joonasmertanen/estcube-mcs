package eu.estcube.crowd;

import java.net.InetSocketAddress;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

 

import org.eclipse.jetty.server.Server;

import org.eclipse.jetty.server.SessionManager;

import org.eclipse.jetty.server.session.HashSessionManager;

import org.eclipse.jetty.server.session.SessionHandler;

import org.eclipse.jetty.servlet.DefaultServlet;

import org.eclipse.jetty.servlet.ServletContextHandler;

import org.eclipse.jetty.servlet.ServletHolder;

import org.eclipse.jetty.security.Authenticator;
import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.HashLoginService;
import org.eclipse.jetty.security.LoginService;
import org.eclipse.jetty.security.authentication.BasicAuthenticator;
import org.eclipse.jetty.security.authentication.FormAuthenticator;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.security.Constraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.service.soap.client.SecurityServerClientFactory;

 

public class DemoWebServer {

 

    public static final String SERVER_ADDRES = "0.0.0.0";

    public static final int SERVER_PORT = 8008;

    public static final String SERVER_DATA_HOME = "src/main/resources";
    
    private static final Logger LOG = LoggerFactory.getLogger(DemoWebServer.class);
    

    public static void main(String[] args) throws Exception {

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);

        InetSocketAddress address = new InetSocketAddress(SERVER_ADDRES, SERVER_PORT);

        Server server = new Server(address);


        context.setContextPath("/");

 

        SessionManager sm = new HashSessionManager();

        SessionHandler sh = new SessionHandler(sm);

        context.setSessionHandler(sh);

 

        context.setResourceBase(SERVER_DATA_HOME);

        DefaultServlet defaultServlet = new DefaultServlet();

        ServletHolder holder = new ServletHolder(defaultServlet);

 

        holder.setInitParameter("useFileMappedBuffer", "false");

        holder.setInitParameter("cacheControl", "no-store,no-cache,must-revalidate");

        holder.setInitParameter("maxCachedFiles", "0");

        context.addServlet(holder, "/");
        context.addServlet(LogOutServlet.class, "/logout");
        
        //Setting login service which uses Authenticatior to verify credentials and create a UserIdentity
        //LoginService loginService = new HashLoginService("MyRealm","src/test/resources/realm.properties");
        LoginService loginService = new CrowdLoginService(SecurityServerClientFactory.getSecurityServerClient());
        server.addBean(loginService); 
        
        ConstraintSecurityHandler security = new ConstraintSecurityHandler();
        server.setHandler(security);

        Constraint constraint = new Constraint();
        constraint.setName("auth");
        constraint.setAuthenticate( true );
        constraint.setRoles(new String[]{"mcs-premium-admin", "mcs-admin", "mcs-premium-op", "mcs-op"});
        
        ConstraintMapping mapping = new ConstraintMapping();
        mapping.setPathSpec( "/*" );
        mapping.setConstraint(constraint);

        Set<String> knownRoles = new HashSet<String>();
        knownRoles.add("mcs-premium-admin");
        knownRoles.add("mcs-admin");
        knownRoles.add("mcs-premium-op");
        knownRoles.add("mcs-op");

        security.setConstraintMappings(Collections.singletonList(mapping), knownRoles);
        /* FormAuthentication gets the parameters from the page entered as first argument (login.html)
         * In case of login failure, returns to the page entered in the second argument (login_error.html)*/
        Authenticator authenticator = new FormAuthenticator("/login.html","/login_error.html",true);
        security.setAuthenticator(authenticator);
        security.setLoginService(loginService);
        security.setStrict(false);

        context.setHandler(security);
        server.setHandler(context);

        LOG.info("Starting demo web server");

        server.start();

        LOG.info("Started DEV server on {}", address);

    }

}