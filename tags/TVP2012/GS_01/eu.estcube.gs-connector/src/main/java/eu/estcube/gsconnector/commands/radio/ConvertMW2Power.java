package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class ConvertMW2Power implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand command) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+4 ");
        messageString.append(command.getParameter("Power mW"));
        messageString.append(" ");
        messageString.append(command.getParameter("Frequency"));
        messageString.append(" ");
        messageString.append(command.getParameter("Mode"));
        messageString.append("\n");
        return messageString;
    }
}
