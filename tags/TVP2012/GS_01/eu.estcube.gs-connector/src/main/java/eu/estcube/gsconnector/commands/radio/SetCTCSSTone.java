package eu.estcube.gsconnector.commands.radio;

import eu.estcube.domain.TelemetryCommand;
import eu.estcube.gsconnector.commands.CommandStringBuilder;

public class SetCTCSSTone implements CommandStringBuilder {

    public StringBuilder createMessageString(TelemetryCommand command) {
        StringBuilder messageString = new StringBuilder();
        messageString.append("+C ");
        messageString.append(command.getParameter("CTCSS Tone"));
        messageString.append("\n");
        return messageString;
    }
}
