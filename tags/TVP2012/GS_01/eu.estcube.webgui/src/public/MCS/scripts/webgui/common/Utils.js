define(["dojo/_base/declare",
        "dojo/_base/window",
        "dojo/date/locale",
        "dojo/io-query",
        "dojo/_base/xhr",
        "dojo/dom", 
        "dojo/_base/array",
		"dojo/dom-construct", 
		"dojo/on",
        "./Constants"
       ], function(declare, win, locale, ioQuery, xhr, dom, arrays, domConstruct, on, Constants) {

        function format(date, format) {
            return dojo.date.locale.format(date, {
                selector: "date",
                datePattern: format,
            });
        }

        function createListElements(data, list, proxy) {
			arrays.forEach(data, function(component, index) {
					var item = domConstruct.create("li", {
						innerHTML : component.abbreviation
					}, list);
                var url = window.location.host + "/MCS/" + component.link;
				on(item, "click", dojo.hitch(component, function(event) {
					if(proxy) {
						window.location = "http://" + url + "?proxy=" + proxy;
					}
					else {
						window.location = "http://" + url;
					}
				}));
			});
        }
        
        return {
        	
            formatDate: function(timestamp) {
                return format(new Date(timestamp), Constants.DEFAULT_DATE_FORMAT);
            },

            hashCode: function(str) {
                var hash = 0;
                if (str.length == 0) {
                    return code;
                }
                for (i = 0; i < str.length; i++) {
                    char = str.charCodeAt(i);
                    hash = 31 * hash + char;
                    hash = hash & hash; // Convert to 32bit integer
                }
                return hash.toString(16);
            },
            
            loadJsonData : function(url, list, proxy, successCallback) {
				xhr.get({
                    url: url,
                    handleAs: "json",
                    load: function(data) {
                    	if(!successCallback) {
                    		createListElements(data, list, proxy);
                    	}
                    	else {
                    		successCallback();
                    	}
                    }
				});
            }
        };
});
