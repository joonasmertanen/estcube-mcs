package eu.estcube.webserver;

import java.util.Date;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eu.estcube.domain.TelemetryObject;

@Component
public class TelemetryObjectToJson implements Processor {

    public void process(Exchange ex) throws Exception {
        TelemetryObject telemetryObject = ex.getIn().getBody(TelemetryObject.class);

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class, new JsonDateSerializer());
       
        Gson gson = gsonBuilder.create();      
        String json = gson.toJson(telemetryObject);     

        ex.getOut().setBody(json);
    }
}
